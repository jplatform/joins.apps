﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Xml;

namespace Joins.Apps.Common {
    public class Util {
        public static string DisplayText(string text) {
            string returnValue = "";
            if (string.IsNullOrEmpty(text))
                return returnValue;

            returnValue = System.Web.HttpUtility.HtmlDecode(text);
            returnValue = returnValue.Replace("&amp;", "&").Replace("&amp", "&");
            returnValue = returnValue.Replace("&lt;", "<").Replace("&gt;", ">");
            returnValue = returnValue.Replace("&nbsp;", " ").Replace("&nbsp", " ");
            returnValue = returnValue.Replace("&quot;", "\"").Replace("&quot", "\"");
            returnValue = returnValue.Replace("&lsquo;", "\"").Replace("&lsquo", "\"");
            returnValue = returnValue.Replace("&rsquo;", "\"").Replace("&rsquo", "\"");
            returnValue = returnValue.Replace("&#34;", "\"").Replace("&#34", "\"");
            returnValue = returnValue.Replace("&#35;", "#").Replace("&#35", "#");
            returnValue = returnValue.Replace("&#44;", "`").Replace("&#44", "`");
            returnValue = returnValue.Replace("&#045;", "-").Replace("&#045", "-");
            returnValue = returnValue.Replace("&#40;", "(").Replace("&#41;", ")");
            returnValue = returnValue.Replace("&#92;", "\"").Replace("&#59;", ";");
            returnValue = returnValue.Replace("&#47;", "/");
            returnValue = returnValue.Replace("&#091;", "[").Replace("&#091", "[").Replace("&#91;", "[").Replace("&#91", "[");
            returnValue = returnValue.Replace("&#093;", "]").Replace("&#093", "]").Replace("&#93;", "]").Replace("&#93", "]");
            returnValue = returnValue.Replace("&#039;", "'").Replace("&#039", "'").Replace("&#39;", "'").Replace("&#39", "'");
            returnValue = returnValue.Replace("\"", "“").Replace("\\", "");

            return returnValue;
        }
        public static string InsertText(string text) {
            string returnValue = "";
            if (string.IsNullOrEmpty(text))
                return returnValue;
            
            returnValue = text;
            returnValue = returnValue.Replace(";", "&#59;");
          //  returnValue = returnValue.Replace("&", "&amp;").Replace( "&", "&amp");
            returnValue = returnValue.Replace("<", "&lt;").Replace( ">", "&gt;");
          //  returnValue = returnValue.Replace( " ","&nbsp;").Replace(" ", "&nbsp");
            returnValue = returnValue.Replace("\"", "&quot;").Replace( "\"", "&quot");
            returnValue = returnValue.Replace("\"", "&lsquo;").Replace("\"", "&lsquo");
            returnValue = returnValue.Replace("\"", "&rsquo;").Replace( "\"", "&rsquo");
            returnValue = returnValue.Replace("\"", "&#34;").Replace("\"", "&#34");
            returnValue = returnValue.Replace("#", "&#35;").Replace("#", "&#35;");
            returnValue = returnValue.Replace( "`", "&#44;").Replace( "`", "&#44;");
            returnValue = returnValue.Replace("-", "&#045;").Replace("-", "&#045");
            returnValue = returnValue.Replace( "(", "&#40;").Replace(")", "&#41;");
            returnValue = returnValue.Replace("\"", "&#92;");
            returnValue = returnValue.Replace( "/", "&#47;");
            returnValue = returnValue.Replace( "[", "&#091;").Replace( "[", "&#091").Replace( "[", "&#91;").Replace( "[", "&#91");
            returnValue = returnValue.Replace( "]", "&#093;").Replace("]", "&#093;").Replace("]", "&#093;").Replace("]", "&#093;").Replace("]", "&#093;");
            returnValue = returnValue.Replace("'", "&#039;").Replace("'", "&#039;").Replace("'", "&#039;").Replace("'", "&#039;").Replace("'", "&#039;");

            return returnValue;
        }
        public static string HTMLRemove(string html) {
            string returnValue = "";
            if (string.IsNullOrEmpty(html))
                return returnValue;

            returnValue = Regex.Replace(html, @"<.*?>", String.Empty);
           
            return returnValue;
        }
        public static string HTMLRemoveNotBr(string html) {
            string returnValue = "";
            if (string.IsNullOrEmpty(html))
                return returnValue;
            returnValue = Regex.Replace(html, @"<([^br|BR]).*?>", String.Empty);
            return returnValue;
        }
        public static string DateTimeConvert(string datetime) {
            string returnValue = "";
            if (string.IsNullOrEmpty(datetime))
                return returnValue;

            DateTime datetimeOut;
            DateTime.TryParse(datetime, out datetimeOut);
            returnValue = datetimeOut.ToString("yyyy-MM-dd HH:mm:ss");

            return returnValue;
        }

        public static string ImagePath(string img) {
            string returnValue = "";
            if (string.IsNullOrEmpty(img))
                return returnValue;

            returnValue = "http://pds.joins.com" + img.Replace(".tn_120.jpg", "");

            return returnValue;
        }
        #region [ Cache ]
        private static System.Runtime.Caching.ObjectCache MC = System.Runtime.Caching.MemoryCache.Default;
        public static void SetCache(string key, object val, int chacheMin = 180) { MC.Set(key, val, new DateTimeOffset(DateTime.Now.AddMinutes(chacheMin))); }
        public static object GetCache(string key) { return MC.Get(key); }
        public static void removeCache(string key) { MC.Remove(key); }
        public static void SetCache(string key, object val, DateTime dt) { MC.Set(key, val, new DateTimeOffset(dt)); }
        #endregion

        #region 파일 업로드 관련
        /// <summary>이미지 업로드</summary>
        public static string imgUpload(System.Web.HttpPostedFileBase pf, string saveFileName, bool isContent = false, long maxSize = 2048) {
            if (pf == null) { return "t"; }
            string sd = ("\\" + (DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") + "\\" ));
            /* /I/C/ : 공개 콘텐트용 이미지 파일 : /I/E/ : 공개 서비스용 이미지 파일 */

            string fp = (System.Web.Configuration.WebConfigurationManager.AppSettings["FilePath"] + sd);
            string fpn = (fp + pf.FileName);

            string ext = System.IO.Path.GetExtension(fpn);
            if (string.IsNullOrEmpty(ext) || "|.jpg|.png|.gif".IndexOf(ext.ToLower()) < 0) { return "t"; }
            if (string.IsNullOrEmpty(saveFileName)) { saveFileName = (DateTime.Now.ToString("yyyyMMddHHmmss") + (new Random()).Next(0, 999).ToString("000") + ext); }
            fpn = (fp + saveFileName);
            if (pf.ContentLength > (maxSize * 1024)) { return "s"; }

            try { if (JCube.AF.IO.Dir.CheckAndCreate(fp)) { pf.SaveAs(fpn); }   /*파일 저장*/
              } catch (Exception em) { Joins.Apps.Common.Logger.LogWriteSystemError("파일 업로드 오류", em, "파일 업로드 오류. 파일 경로:" + fp + ",업로드 파일명:" + pf.FileName + ",변경 파일명:" + saveFileName); return "e"; }
           // return (sd.Replace("\\", "/") + saveFileName);
            return (fpn);
        }
        public static string UploadFileToFtp( string filePath) {
            string imgPath = (DateTime.Now.ToString("yyyy") + "/" + DateTime.Now.ToString("MM") + "/");
            string ftpurl = "ftp.images.joins.com:8021/News10/cover/" ;
            var fileName = Path.GetFileName(filePath);
            var request = (FtpWebRequest)WebRequest.Create(ftpurl + fileName);

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential("uximg", "ux0lalwl!@");
           // request.ContentType = "image";
            request.UsePassive = false;
            request.UseBinary = true;
            request.KeepAlive = false;

            using (var fileStream = File.OpenRead(filePath)) {
                using (var requestStream = request.GetRequestStream()) {
                    fileStream.CopyTo(requestStream);
                    requestStream.Close();
                }
            }

            var response = (FtpWebResponse)request.GetResponse();
            Console.WriteLine("Upload done: {0}", response.StatusDescription);
            response.Close();
            return "http://images.joins.com/News10/cover/" + imgPath + fileName;
        }
        public static void MSGShow(string msg, bool preload = false) {
            string str = "alert(\"" + msg + "\");";
            if (preload == true) { RunScriptPreLoad(str); }
            else { RunScriptOnLoad(str); }
        }
        public static void MSGShow(string msg, string url, bool preload = false) {
            StringBuilder sb = new StringBuilder().AppendLine(" alert(\"" + msg + "\");").AppendLine(" document.location.href=\"" + url + "\";");
            if (preload == true) { RunScriptPreLoad(sb.ToString()); }
            else { RunScriptOnLoad(sb.ToString()); }
        }
        protected static void RunScriptPreLoad(string msg) {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<meta http-equiv='Content-Type' content='text/html;charset=utf-8' /><script type='text/javascript'>").AppendLine(msg).AppendLine("</script>");
            System.Web.HttpContext.Current.Response.Clear();
            System.Web.HttpContext.Current.Response.Write(sb.ToString());
            System.Web.HttpContext.Current.Response.End();
        }
        protected static void RunScriptOnLoad(string msg) {
            System.Web.UI.Page page = new System.Web.UI.Page();
            page.ClientScript.RegisterStartupScript(typeof(string), "msg", msg, true);
        }
        #endregion
        #region [TTS관련 치환 메소드]
        public static string GetRexValue(string v) {

            // var Rgx = new Regex(@"\[\w*\]", RegexOptions.IgnoreCase);
            // return Rgx.Replace(v, "");
            string pattern = @"\[\w*\]";
            return Regex.Replace(v, pattern, string.Empty);
        }
        public static List<string[]> GetTTSReplace() {
            List<string[]> LReplaceWordList = new List<string[]>();
            var h = Joins.Apps.Common.Util.GetCache("TTSWordList");
            if (h != null) { LReplaceWordList = (List<string[]>)h; }
            if (LReplaceWordList == null || LReplaceWordList.Count <= 0) {
                string path = HttpContext.Current.Server.MapPath("/Data/TTS_Word.txt");
                StreamReader reader = File.OpenText(path);
                while (reader.Peek() > -1) {
                    string ss = reader.ReadLine();
                    string[] slist = ss.Split("\t".ToArray());
                    if (slist.Length > 1) {
                        LReplaceWordList.Add(slist);
                        //  if (!hReplaceWordList.ContainsKey(slist[0])) { hReplaceWordList.Add(slist[0], slist[1]); }
                    }
                }
                reader.Close();
                Joins.Apps.Common.Util.SetCache("TTSWordList", LReplaceWordList, 10);
            }
            return LReplaceWordList;
        }
        public static char[] HANJA_TO_HANGLE_MAP = new char[65565];
        public static void GetCodeListForHanjatoHangul() {

       //     List<string[]> LReplaceWordList = new List<string[]>();
            var h = Joins.Apps.Common.Util.GetCache("TTSHanjatoHangul");
            if (h != null) {
                char[] hchar = (char[])h;
                //if (!string.IsNullOrEmpty(hchar[48].ToString())&& (hchar[48].ToString()).Equals("0")) {HANJA_TO_HANGLE_MAP = hchar;}
            }
            if (HANJA_TO_HANGLE_MAP == null || string.IsNullOrEmpty(HANJA_TO_HANGLE_MAP[48].ToString()) || !(HANJA_TO_HANGLE_MAP[48].ToString()).Equals("0")) {

                XmlNodeList xmlList = null;
                string code = "";
                try {
                    XmlDocument xml = new XmlDocument();
                    xml.Load(HttpContext.Current.Server.MapPath("/Data/hanjatohangle.xml"));
                    xmlList = xml.SelectNodes("/HanjaToHangle/UnicodeMap");
                    foreach (XmlNode xn in xmlList) {
                        code = xn["Code"].InnerText;
                    }
                } catch {
                }
                string[] codelist = code.Split(',');
                for (int i = 0; i < codelist.Length; i++) {
                    string s = codelist[i].Trim();
                    s = s.Replace("\r\n", "");
                    HANJA_TO_HANGLE_MAP[i] = Convert.ToChar(Convert.ToUInt16(s, 16));
                }
                Joins.Apps.Common.Util.SetCache("TTSHanjatoHangul", HANJA_TO_HANGLE_MAP, 10);
            }
        }
        public static String toKorean(String strChineseText) {

            char unicode =(char)0x0000;
            byte[] hanjaByte;
            String strResult;
            try {
                hanjaByte = Encoding.UTF8.GetBytes(strChineseText);
                for (int i = 0; i < hanjaByte.Length;) {
                    if ((hanjaByte[i] & 0xFF) < 0x80) {
                        i++;
                        continue;
                    }
                    else if ((hanjaByte[i] & 0xFF) < 0xE0) {
                        i += 2;
                        continue;
                    }
                    else if ((hanjaByte[i] & 0xFF) < 0xF0) {
                        unicode = (char)(hanjaByte[i] & 0x0f);
                        i++;
                        unicode = (char)(unicode << 6);
                        unicode = (char)(unicode | (hanjaByte[i] & 0x3f));
                        i++;
                        unicode = (char)(unicode << 6);
                        unicode = (char)(unicode | (hanjaByte[i] & 0x3f));
                        i++;
                    }else {
                        i++;
                    }
                    if (HANJA_TO_HANGLE_MAP[unicode] != unicode) {
                        unicode = HANJA_TO_HANGLE_MAP[unicode];
                        //                    toHexString(new byte[]{(byte) unicode});
                        hanjaByte[i - 1] = (byte)((unicode & 0x3f) | 0x80);
                        hanjaByte[i - 2] = (byte)(((unicode << 2) & 0x3f00 | 0x8000) >> 8);
                        hanjaByte[i - 3] = (byte)(((unicode << 4) & 0x3f0000 | 0xe00000) >> 16);
                        continue;
                    }
                }
                strResult = Encoding.UTF8.GetString(hanjaByte);
            } catch  {
                return strChineseText;
            }
            return strResult;
        }
        #endregion

        #region [문의 메일 전송]
        public static bool SendJoongangMail(string mailto, string mailfrom, string tit, string cont) {
            if (!string.IsNullOrEmpty(Joins.Apps.GLOBAL.JoongangMailServer) && (Joins.Apps.GLOBAL.JoongangMailServer).IndexOf("|") > 0) {
                string[] mailInfo = (Joins.Apps.GLOBAL.JoongangMailServer).Split('|');
                //	string pw = JCube.AF.IO.File.LoadFileText(JCube.JoinsStore.GLOBAL.MailTempPath + "JoinsJoongangMailPw.txt").Trim();
                //	if (string.IsNullOrEmpty(pw)) { pw = mailInfo[3]; }//파일 비번우선
                //0 :서버IP,1:포트,2:인증ID,3:비번,4:보내는메일
                if (string.IsNullOrEmpty(mailfrom)) { mailfrom = mailInfo[3]; }
                if (mailInfo.Length >= 4) {
                    bool r = false;
                    try {
                     //   r = JCube.AF.Module.Mail.SendMailWithCredentials(mailInfo[0], JCube.AF.Util.Converts.ToInt32(mailInfo[1], 25), "", "", mailto, mailfrom, tit, cont);
                        r = JCube.AF.Module.Mail.SendMail(mailInfo[0], mailto, mailfrom, tit, cont);
                    } catch { }
                    return r;
                }
                else { return false; }

            }
            else { return false; }
        }
        #endregion
        public static int IndexOf(byte[] array, byte[] pattern, int offset) {
            int success = 0;
            for (int i = offset; i < array.Length; i++) {
                if (array[i] == pattern[success]) {
                    success++;
                }
                else {
                    success = 0;
                }

                if (pattern.Length == success) {
                    return i - pattern.Length + 1;
                }
            }
            return -1;
        }

        /// <summary>
        /// 클라이언트의 아이피 주소를 가져옵니다.
        /// </summary>
        /// <returns></returns>
        public static string GetClientIPAddress()
        {
            var request = HttpContext.Current.Request;
            var ip = request.Headers["X-Forwarded-For"];

            if (string.IsNullOrWhiteSpace(ip) || "unknown".Equals(ip, StringComparison.OrdinalIgnoreCase))
                ip = request.Headers["Proxy-Client-IP"];

            if (string.IsNullOrWhiteSpace(ip) || "unknown".Equals(ip, StringComparison.OrdinalIgnoreCase))
                ip = request.Headers["WL-Proxy-Client-IP"];

            if (string.IsNullOrWhiteSpace(ip) || "unknown".Equals(ip, StringComparison.OrdinalIgnoreCase))
                ip = request.Headers["HTTP_CLIENT_IP"];

            if (string.IsNullOrWhiteSpace(ip) || "unknown".Equals(ip, StringComparison.OrdinalIgnoreCase))
                ip = request.Headers["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrWhiteSpace(ip) || "unknown".Equals(ip, StringComparison.OrdinalIgnoreCase))
                ip = request.UserHostAddress;

            return ip;
        }
    }
}
