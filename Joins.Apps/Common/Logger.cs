﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common {
    public class Logger {
        public static string LogPath { get { return Joins.Apps.GLOBAL.LogPath; } }
        #region 서비스 로그
        /// <summary>보안관련 로그 기록</summary>
        public static bool LogWriteSecurity(string userid, string msg) { return LogWrite("", userid + "\t$ " + msg.Replace("\t", "  ")); }
        /// <summary>오류 로그 기록</summary>
        public static bool LogWriteError(string userid, string msg) { return LogWrite("", userid + "\t! " + msg.Replace("\t", "  ")); }
        /// <summary>사용자 활동 로그 기록</summary>
        public static bool LogWriteAction(string userid, string msg) { return LogWrite("", userid + "\t@ " + msg.Replace("\t", "  ")); }
        /// <summary>사용자 활동 로그 그룹별 파일 생성</summary>
        public static bool LogWriteGroup(string grouppath, string msg) { return LogWrite(grouppath, msg.Replace("\t", "  ")); }


        protected static bool LogWrite(string path, string msg) {
            msg = (System.Web.HttpContext.Current.Request.UserHostAddress + "\t" + msg);
            if (!string.IsNullOrEmpty(path)) { path = Joins.Apps.Common.Logger.LogPath + path + "\\"; }
            else { path = Joins.Apps.Common.Logger.LogPath; }
            try { (new JCube.AF.Module.LogWriter(path, "", JCube.AF.Module.LogWriter.LogUnit.Day)).WriteLog(msg.Replace("\r", "").Replace("\n", " ")); return true; } catch { return false; }
        }
        public static bool LogWriteDELETEAction(string userid, string msg) { return LogWrite("", userid + "\t@ " + msg.Replace("\t", "  ")); }
        #endregion

        #region 시스템 로그
        public static string LERM = "";
        /// <summary>시스템 장애 로그 기록</summary>
        public static bool LogWriteSystemError(string part, Exception em, string msg = "") {
            string nlerm = (part + em.Message);
            if (LERM == nlerm) { msg = ("시스템 " + part + "\t#[" + msg + "] " + em.Message); }
            else { msg = ("시스템 " + part + "\t#[" + msg + "] " + em.Message + "\r\n" + em.StackTrace); }
            LERM = nlerm;
            return LogWrite("", msg);
        }
        #endregion

        #region 데이터 관리 로그 - 데이터 관리 관련 해서 별도의 로그 구성 (스케줄 / 관리자 / 개발자 처리 항목별로 구성)
        /// <summary>데이터 관리 관련 로그 등록</summary>
        public static bool LogWriteDataControl(string user, string msg) { return LogWrite("", user + "\t " + msg.Replace("\t", "  ")); }
        #endregion

        /// <summary>디버그용 로그 작성</summary>
        public static bool LogWriteDebug(string fn, string msg) { try { (new JCube.AF.Module.LogWriter(Joins.Apps.Common.Logger.LogPath + "Debug\\", fn,JCube. AF.Module.LogWriter.LogUnit.Day)).WriteLog(msg.Replace("\r", "").Replace("\n", " ")); return true; } catch { return false; } }
    }
}
