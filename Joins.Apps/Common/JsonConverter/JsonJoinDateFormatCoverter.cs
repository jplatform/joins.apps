﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.JsonConverter {

    public class JsonJoinDateFormatCoverter : Newtonsoft.Json.JsonConverter {
       private string format;

        public JsonJoinDateFormatCoverter(string format) {
            this.format = format;
        }

        public override bool CanConvert(Type objectType) {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
            string sWriteDate = value.ToString();
            try { sWriteDate = DateTime.ParseExact(value.ToString(), "yyyymmdd", null).ToString(format); } catch { }
            writer.WriteValue(sWriteDate);
        }
    }
}
