﻿using Joins.Apps.Common.Sully;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.JsonConverter
{
    /// <summary>
    /// 경과시간으로 변환하여 JSON에 작성합니다.
    /// </summary>
    public class JsonDateDisplayConverter : Newtonsoft.Json.JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(DateTimeDisplayConverter.ConvertDateDuration(Convert.ToDateTime(value)));
        }
    }
}
