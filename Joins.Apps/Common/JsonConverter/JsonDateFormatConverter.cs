﻿using Newtonsoft.Json;
using System;

namespace Joins.Apps.Common.JsonConverter
{
    /// <summary>
    /// 날짜를 지정한 형태로 JSON에 작성합니다.
    /// </summary>
    public class JsonDateFormatConverter : Newtonsoft.Json.JsonConverter
    {
        private string format;

        public JsonDateFormatConverter(string format)
        {
            this.format = format;
        }

        public override bool CanConvert(Type objectType)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(string.Format(format, value));
        }
    }
}
