﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;

namespace Joins.Apps.Common.Utilities
{
    public class ExcelExportHelper :IDisposable
    {
        ExcelPackage package;

        public ExcelExportHelper()
        {
            package = new ExcelPackage();
        }

        /// <summary>
        /// Export를 하기 위한 Excel 출력형식을 지정하는 어트리뷰트입니다.
        /// </summary>
        public class ExcelHeaderTItle : Attribute
        {
            /// <summary>
            /// 제목
            /// </summary>
            public string Title { get; set; }
            /// <summary>
            /// Export 포함하지 않을지 여부
            /// </summary>
            public bool Ignore { get; set; }
            /// <summary>
            /// 출력형식
            /// </summary>
            public string DisplayFormat { get; set; }
        }

        /// <summary>
        /// 엑셀 Content-Type을 가져옵니다.
        /// </summary>
        public static string ExcelContentType
        {
            get { return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"; }
        }

        /// <summary>
        /// 목록을 데이터 테이블로 변환합니다.
        /// </summary>
        /// <typeparam name="T">형식</typeparam>
        /// <param name="data">데이터</param>
        /// <returns></returns>
        public DataTable ConvertDataTable<T>(IEnumerable<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable dataTable = new DataTable();

            for (int i = 0; i < properties.Count; i++)
            {
                var property = properties[i];
                var headerAttr = property.Attributes.OfType<ExcelHeaderTItle>().FirstOrDefault();
                var columnName = headerAttr == null ? property.Name : headerAttr.Title;
                var ignoreColumn = headerAttr?.Ignore;

                if (!ignoreColumn.HasValue || (ignoreColumn.HasValue && !ignoreColumn.Value))
                {
                    var type = Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType;

                    if (headerAttr != null && !string.IsNullOrWhiteSpace(headerAttr.DisplayFormat))
                        type = typeof(string);

                    dataTable.Columns.Add(columnName, type);
                }
            }

            foreach (T item in data)
            {
                var values = new ArrayList();

                for (int i = 0; i < properties.Count; i++)
                {
                    var property = properties[i];
                    var headerAttr = property.Attributes.OfType<ExcelHeaderTItle>().FirstOrDefault();
                    var ignoreColumn = headerAttr?.Ignore;

                    if (!ignoreColumn.HasValue || (ignoreColumn.HasValue && !ignoreColumn.Value))
                    {
                        var format = headerAttr?.DisplayFormat;
                        var value = properties[i].GetValue(item);

                        values.Add(string.IsNullOrWhiteSpace(format) || value == null ? value : string.Format(format, value));
                    }
                }

                dataTable.Rows.Add(values.ToArray());
            }
            return dataTable;
        }

        /// <summary>
        /// 새로운 워크시트에 데이터를 추가합니다.
        /// </summary>
        /// <param name="dataTable">데이터</param>
        /// <param name="headTitle">헤더 제목</param>
        /// <param name="subTitle">서브 제목</param>
        /// <param name="sheetTitle">시트 제목</param>
        /// <returns></returns>
        public ExcelExportHelper AddWorkSheetData(DataTable dataTable, string headTitle = "", string subTitle = "", string sheetTitle = "")
        {
            var workSheet = package.Workbook.Worksheets.Add(sheetTitle);
            var startRowFrom = string.IsNullOrWhiteSpace(headTitle) ? 2 : 4;

            if (!string.IsNullOrWhiteSpace(subTitle))
                startRowFrom = startRowFrom + 1;

            // add the content into the Excel file  
            workSheet.Cells["A" + startRowFrom].LoadFromDataTable(dataTable, true);

            // autofit width of cells with small content  
            int columnIndex = 1;
            foreach (DataColumn column in dataTable.Columns)
            {
                ExcelRange columnCells = workSheet.Cells[workSheet.Dimension.Start.Row, columnIndex, workSheet.Dimension.End.Row, columnIndex];
                int maxLength = columnCells.Max(cell => Convert.ToString(cell.Value == null ? string.Empty : cell.Value).Count());
                if (maxLength < 150)
                {
                    workSheet.Column(columnIndex).AutoFit();
                }


                columnIndex++;
            }

            // format header - bold, yellow on black  
            using (ExcelRange r = workSheet.Cells[startRowFrom, 1, startRowFrom, dataTable.Columns.Count])
            {
                var borderColor = System.Drawing.ColorTranslator.FromHtml("#147873");
                var backgroundColor = System.Drawing.ColorTranslator.FromHtml("#1fb5ad");

                r.Style.Font.Color.SetColor(System.Drawing.Color.White);
                r.Style.Font.Bold = true;
                r.Style.Fill.PatternType = ExcelFillStyle.Solid;
                r.Style.Fill.BackgroundColor.SetColor(backgroundColor);

                r.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                r.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                r.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                r.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                r.Style.Border.Top.Color.SetColor(borderColor);
                r.Style.Border.Bottom.Color.SetColor(borderColor);
                r.Style.Border.Left.Color.SetColor(borderColor);
                r.Style.Border.Right.Color.SetColor(borderColor);
            }

            // format cells - add borders
            if (dataTable.Rows.Count > 0)
            {
                using (ExcelRange r = workSheet.Cells[startRowFrom + 1, 1, startRowFrom + dataTable.Rows.Count, dataTable.Columns.Count])
                {
                    var borderColor = System.Drawing.Color.DarkGray;

                    r.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    r.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    r.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    r.Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    r.Style.Border.Top.Color.SetColor(borderColor);
                    r.Style.Border.Bottom.Color.SetColor(borderColor);
                    r.Style.Border.Left.Color.SetColor(borderColor);
                    r.Style.Border.Right.Color.SetColor(borderColor);
                }
            }

            if (!string.IsNullOrWhiteSpace(headTitle))
            {
                workSheet.Cells["A2"].Value = headTitle;
                workSheet.Cells["A2"].Style.Font.Size = 20;
                workSheet.Cells["A2"].Style.Font.Bold = true;
            }

            if (!string.IsNullOrWhiteSpace(subTitle))
            {
                workSheet.Cells["A3"].Value = subTitle;
                workSheet.Cells["A3"].Style.Font.Size = 14;
                workSheet.Cells["A3"].Style.Font.Color.SetColor(System.Drawing.Color.DarkGray);
            }

            workSheet.InsertColumn(1, 1);
            workSheet.Column(1).Width = 5;

            return this;
        }

        /// <summary>
        /// 엑셀 데이터를 바이트로 가져옵니다.
        /// </summary>
        /// <returns></returns>
        public byte[] GetExcelBytes()
        {
            return package.GetAsByteArray();
        }

        public void Dispose()
        {
            package.Dispose();
        }
    }
}
