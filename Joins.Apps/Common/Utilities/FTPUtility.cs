﻿using JCube.AF.Module;
using System.Configuration;
using System.IO;
using System.Web;

namespace Joins.Apps.Common.Utilities
{
    public static class FTPUtility
    {
        private static FTPConnectInfo ConfigurationInfo
        {
            get {
                return new FTPConnectInfo
                {
                    Addr = ConfigurationManager.AppSettings["FTPAddress"],
                    BasePath = "/",
                    Port = ConfigurationManager.AppSettings["FTPPort"],
                    UID = ConfigurationManager.AppSettings["FTPID"],
                    UPW = ConfigurationManager.AppSettings["FTPPassword"],
                    UsePassive = true
                };
            }
        }

        private static string FTPBasePath
        {
            get { return ConfigurationManager.AppSettings["FTPBasePath"]; }
        }

        private static string FTPUploadURL
        {
            get { return ConfigurationManager.AppSettings["FTPURL"]; }
        }

        /// <summary>
        /// 파일을 FTP로 업로드 합니다.
        /// </summary>
        /// <param name="localDir">업로드 할 파일 위치</param>
        /// <param name="localFileName">업로드 할 파일명</param>
        /// <param name="ftpDir">업로드 위치</param>
        /// <param name="ftpFileName">업로드시 사용할 파일명</param>
        /// <returns>처리 결과</returns>
        public static FTPUploadResult Upload(string localDir, string localFileName, string ftpDir, string ftpFileName)
        {
            var upload_path = Path.Combine(FTPBasePath, ftpDir);

            return new FTPUploadResult
            {
                IsSuccess = FTP.FTPUploadTo(ConfigurationInfo, localDir, localFileName, upload_path, ftpFileName),
                FTPDomain = FTPUploadURL,
                UploadPath = string.Format("{0}{1}", ftpDir, ftpFileName),
                AccessURL = string.Format("{0}{1}{2}", FTPUploadURL, ftpDir, ftpFileName)
            };
        }

        /// <summary>
        /// 파일을 FTP로 업로드 합니다.
        /// </summary>
        /// <param name="pf">업로드 객체</param>
        /// <param name="ftpDir">업로드 위치</param>
        /// <param name="ftpFileName">업로드시 사용할 파일명</param>
        /// <param name="checkPath">경로 체크 여부</param>
        /// <returns></returns>
        public static FTPUploadResult Upload(HttpPostedFile pf, string ftpDir, string ftpFileName, bool checkPath)
        {
            var upload_path = Path.Combine(FTPBasePath, ftpDir);

            return new FTPUploadResult
            {
                IsSuccess = FTP.FTPUploadTo(ConfigurationInfo, pf, upload_path, ftpFileName, checkPath),
                FTPDomain = FTPUploadURL,
                UploadPath = string.Format("{0}{1}", ftpDir, ftpFileName),
                AccessURL = string.Format("{0}{1}{2}", FTPUploadURL, ftpDir, ftpFileName)
            };
        }

        /// <summary>
        /// 파일을 FTP로 업로드 합니다.
        /// </summary>
        /// <param name="localDir">업로드 할 파일 위치</param>
        /// <param name="localFileName">업로드 할 파일명</param>
        /// <param name="ftpDir">업로드 위치</param>
        /// <param name="ftpFileName">업로드시 사용할 파일명</param>
        /// <param name="checkPath">경로 체크 여부</param>
        /// <returns></returns>
        public static FTPUploadResult Upload(string localDir, string localFileName, string ftpDir, string ftpFileName, bool checkPath)
        {
            var upload_path = Path.Combine(FTPBasePath, ftpDir);

            return new FTPUploadResult
            {
                IsSuccess = FTP.FTPUploadTo(ConfigurationInfo, localDir, localFileName, upload_path, ftpFileName, checkPath),
                FTPDomain = FTPUploadURL,
                UploadPath = string.Format("{0}{1}", ftpDir, ftpFileName),
                AccessURL = string.Format("{0}{1}{2}", FTPUploadURL, ftpDir, ftpFileName)
            };
        }

        public class FTPUploadResult
        {
            /// <summary>
            /// 업로드 처리 여부
            /// </summary>
            public bool IsSuccess { get; internal set; } = false;
            /// <summary>
            /// 업로드 경로
            /// </summary>
            public string UploadPath { get; internal set; }
            /// <summary>
            /// FTP 서비스 주소
            /// </summary>
            public string FTPDomain { get; internal set; }
            /// <summary>
            /// 업로드 파일 접근 URL
            /// </summary>
            public string AccessURL { get; internal set; }
        }
    }
}
