﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Utilities {
    public static class EnumerableExtensions {
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action) {
            if (source == null)
                throw new ArgumentNullException("source");
            if (action == null)
                throw new ArgumentNullException("action");

            foreach (T item in source)
                action(item);
        }

        public static bool IsAny<T>(this IEnumerable<T> data) {
            return data != null && data.Any();
        }
    }
}
