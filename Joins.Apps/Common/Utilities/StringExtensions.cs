﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Joins.Apps.Common.Utilities {
    public static class StringExtensions {
        private static readonly string[] _checkList = { ";", "'", "--", "<", ">", "xp_", "&#059;", "&#59;" };

        private readonly static int[] _defaultWidth =
        {
            // ASCII 32(0x20) ~ 47(0x2f)
            //     !   "   #   $   %   &   '   (   )   *   +   ,   -   .   / 
            40,
            25,
            25,
            50,
            50,
            50,
            50,
            25,
            40,
            40,
            50,
            50,
            25,
            50,
            25,
            50, 

            // ASCII 48(0x30) ~ 57(0x39)
            // 0   1   2   3   4   5   6   7   8   9 
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,

            // ASCII 58(0x3a) ~ 64(0x40)
            // :   ;   <   =   >   ?   @
            25,
            25,
            50,
            50,
            50,
            50,
            50,

            // ASCII 65(0x41) ~ 90(0x5a)
            // A   B   C   D   E   F   G   H   I   J   K   L   M   N   O   P   Q   R   S   T   U   V   W   X   Y   Z
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,

            // ASCII 91(0x5b) ~ 96(0x60)
            // [   \   ]   ^   _   `
            40,
            50,
            40,
            50,
            50,
            25,

            // ASCII 97(0x61) ~ 122(0x7a)   // ijl을 25% 한글기준
            // a   b   c   d   e   f   g   h   i   j   k   l   m   n   o   p   q   r   s   t   u   v   w   x   y   z
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            25,
            25,
            50,
            25,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
                                                
            // ASCII 123(0x7b) ~ 126(0x7e)
            // {   |   }   ~
            40,
            25,
            40,
            50
        };

        // 글줄임에서 사용할 기본 ASCII width (한글 기준 100%) 
        private static readonly int[] _characterWidth = DefaultWidth;

        public static int[] DefaultWidth {
            get { return _defaultWidth; }
        }

        public static int[] CharacterWidth {
            get { return _characterWidth; }
        }


        public static string CatchUpSqlInjection(this string value) {
            if (value == null)
                return null;
            string ret = value;

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (string c in _checkList)
                ret = ret.Replace(c, string.Empty);
            return ret;
        }

        public static string TitleString(this string title) {
            if (string.IsNullOrEmpty(title))
                return string.Empty;

            var array = new char[title.Length];
            int arrayIndex = 0;
            bool inside = false;

            foreach (char @let in title) {
                if (@let == '<') {
                    inside = true;
                    continue;
                }
                if (@let == '>') {
                    inside = false;
                    continue;
                }
                if (!inside) {
                    array[arrayIndex] = @let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }

        public static string Ellipsis(this string str, int length, string cutMessage) {
            if (!string.IsNullOrEmpty(str)) {
                length *= 50;
                const int unicodeHanStart = 0xac00; // 유니코드한글시작위치
                const int unicodeHanEnd = 0xD7A3; // 유니코드한글끝 위치

                string tmpCutMessage = null; // 지정된 길이만큼의 내용
                int tmpCutLength = 0; // 한글, 영문 1자당 길이 수

                for (int i = 0; i < str.Length; i++) {
                    char ascii = Convert.ToChar(str[i]);
                    if (unicodeHanStart <= ascii && unicodeHanEnd >= ascii)
                        tmpCutLength += 100; // 한글일 경우
                    else {
                        if (ascii >= 32 && ascii <= 126)
                            tmpCutLength += CharacterWidth[ascii - 32];
                        else
                            tmpCutLength += 50; // 영문일 경우
                    }

                    if (length >= tmpCutLength)
                        tmpCutMessage += str.Substring(i, 1);
                    else
                        break;
                }

                if (tmpCutMessage != null && str.Length > tmpCutMessage.Length) {
                    tmpCutMessage = tmpCutMessage.Replace("<", "&lt;").Replace(">", "&gt;");
                    return tmpCutMessage + cutMessage;
                }
                str = str.Replace("<", "&lt;").Replace(">", "&gt;");
                return str;
            }
            return str;
        }

        public static DateTime ConvertDateTime(this string str) {
            if (string.IsNullOrEmpty(str))
                return DateTime.Now;

            string[] formats =
            {
                "yyyyMMdd",
                "yyyy-MM-dd",
                "yyyy-MM-dd hh:mm:ss",
                "yyyy-MM-dd HH:mm:ss",
                "yyyy-MM-dd HH",
                "yyyy-MM-dd hh:mm",
                "yyyy-MM-dd HH:mm",
                "yyyyMMddhhmm",
                "yyyyMMddHHmm",
                "yyyyMMddhhmmss",
                "yyyyMMddHHmmss",
                "MM/dd/yyyy hh:mm:ss"
            };

            DateTime value;
            bool result = DateTime.TryParseExact(str, formats, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces, out value);

            return result ? value : DateTime.Now;
        }

        public static DateTime ConvertDateTime(this string str, string format) {
            if (string.IsNullOrEmpty(str))
                return DateTime.Now;

            DateTime value;
            bool result = DateTime.TryParseExact(str, format, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces, out value);

            return result ? value : DateTime.Now;
        }

        public static DateTime ConvertDateTimeyyyyMMddHHmm(this string str) {
            if (string.IsNullOrEmpty(str))
                return DateTime.Now;

            if (str.Length != 12)
                return DateTime.Now;

            int year = Convert.ToInt32(str.Substring(0, 4));
            string month = str.Substring(4, 2);
            int day = Convert.ToInt32(str.Substring(6, 2));
            string hour = str.Substring(8, 2);
            string second = str.Substring(10, 2);
            bool isLeapYear = (year % 4) == 0 && (year % 100) != 0 || (year % 400) == 0;
            if ((month == "04" ||
                 month == "06" ||
                 month == "09" ||
                 month == "11") && day > 30)
                day = 30;
            if (month == "02") {
                if (isLeapYear && day > 29)
                    day = 29;
                else if (!isLeapYear && day > 28)
                    day = 28;
            }

            DateTime value;
            bool result = DateTime.TryParseExact(
                year.ToString("0000") + month + day.ToString("00") + hour + second,
                "yyyyMMddHHmm",
                CultureInfo.InvariantCulture,
                DateTimeStyles.AllowWhiteSpaces,
                out value);

            return result ? value : DateTime.Now;
        }

        public static DateTime ConvertDateTimeyyyyMMddHHmmss(this string str) {
            if (string.IsNullOrEmpty(str))
                return DateTime.Now;

            if (str.Length != 14)
                return DateTime.Now;

            int year = Convert.ToInt32(str.Substring(0, 4));
            string month = str.Substring(4, 2);
            int day = Convert.ToInt32(str.Substring(6, 2));
            string hour = str.Substring(8, 2);
            string minute = str.Substring(10, 2);
            string second = str.Substring(12, 2);
            bool isLeapYear = (year % 4) == 0 && (year % 100) != 0 || (year % 400) == 0;
            if ((month == "04" ||
                 month == "06" ||
                 month == "09" ||
                 month == "11") && day > 30)
                day = 30;
            if (month == "02") {
                if (isLeapYear && day > 29)
                    day = 29;
                else if (!isLeapYear && day > 28)
                    day = 28;
            }

            DateTime value;
            bool result = DateTime.TryParseExact(
                year.ToString("0000") + month + day.ToString("00") + hour + minute + second,
                "yyyyMMddHHmmss",
                CultureInfo.InvariantCulture,
                DateTimeStyles.AllowWhiteSpaces,
                out value);

            return result ? value : DateTime.Now;
        }

        public static int ConvertInt(this string str) {
            if (string.IsNullOrEmpty(str))
                return 0;

            int value;
            bool result = int.TryParse(str, out value);

            return result ? value : 0;
        }

        public static string Right(this string str, int length) {
            if (length < 0)
                return "";

            if (str.Length > length)
                return str.Substring(str.Length - length, length);
            return str;
        }
    }
}
