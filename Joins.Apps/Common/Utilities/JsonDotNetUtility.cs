﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace Joins.Apps.Common.Utilities {
    public static class JsonDotNetUtility {
        public static T GetValue<T>(NullValue obj) {
            return default(T);
        }

        public static T GetValue<T>(JObject obj) {
            if (obj == null)
                return default(T);
            var value = obj["#cdata-section"] as JValue;
            return value == null ? default(T) : GetValue<T>(value);
        }

        public static T GetValue<T>(JValue obj) {
            if (obj == null)
                return default(T);
            try {
                return obj.ToObject<T>();
            } catch {
                return default(T);
            }
        }

        public struct NullValue {
        }
    }
}
