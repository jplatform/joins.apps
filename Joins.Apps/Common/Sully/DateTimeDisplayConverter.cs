﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully
{
    public class DateTimeDisplayConverter
    {
        const int SECOND = 1;
        const int MINUTE = 60 * SECOND;
        const int HOUR = 60 * MINUTE;
        const int DAY = 24 * HOUR;
        const int MONTH = 30 * DAY;

        public static string ConvertDateDuration(DateTime? dt)
        {
            if (!dt.HasValue)
                return string.Empty;

            var ts = new TimeSpan(DateTime.Now.Ticks - dt.Value.Ticks);
            var delta = Math.Abs(ts.TotalSeconds);

            // 1분 이내
            if (delta < MINUTE)
                return "방금 전";

            // 60분 이내
            else if (delta < 60 * MINUTE)
                return ts.Minutes + "분";

            // 24시간 이내
            else if (delta < 24 * HOUR)
                return ts.Hours + "시간";

            // 30일 이내
            else if (delta < 30 * DAY)
                return ts.Days + "일";

            // 그 외
            else
                return Convert.ToInt32(Math.Floor((double)ts.Days / 30)) + "개월";
        }
    }
}
