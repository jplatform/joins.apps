﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace Joins.Apps.Common.Sully
{
    public class APIHeaderUtil
    {
        /// <summary>
        /// 디바이스 고유 아이디를 가져옵니다.
        /// </summary>
        public static string DeviceID { get { return GetHeaderValue("device_id", false); } }
        /// <summary>
        /// 디바이스 종류를 가져옵니다. (I : iOS / A : Android)
        /// </summary>
        public static string DeviceType { get { return GetHeaderValue("device_type", true); } }
        /// <summary>
        /// 회원 로그인 형식을 가져옵니다. (N : 네이버 / F : 페이스북 / K : 카카오 / G : 구글)
        /// </summary>
        public static string MemberLoginType { get { return GetHeaderValue("login_type", true); } }
        ///// <summary>
        ///// 회원 인증 토큰을 가져옵니다.
        ///// </summary>
        //public static string MemberAuthToken { get { return GetHeaderValue("token", false); } }
        ///// <summary>
        ///// 회원 인증 Id토큰을 가져옵니다.
        ///// </summary>
        //public static string MemberAuthIdToken { get { return GetHeaderValue("idToken", false); } }
        /// <summary>
        /// 회원 인증 고유아이디를 가져옵니다.
        /// </summary>
        public static string MemberAuthUniqueID { get { return GetHeaderValue("uid", false); } }

        /// <summary>
        /// 헤더 값을 반환합니다.
        /// </summary>
        /// <param name="key">키</param>
        /// <returns></returns>
        public static string GetHeaderValue(string key, bool isConvertUpperCase)
        {
            var header = HttpContext.Current.Request.Headers;

            if (header.AllKeys.Contains(key))
            {
                var value = header.GetValues(key).FirstOrDefault();
                if (isConvertUpperCase)
                    return value.ToUpper();

                return value;
            }

            return null;
        }
    }
}
