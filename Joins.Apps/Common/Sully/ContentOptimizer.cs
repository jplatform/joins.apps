﻿using Joins.Apps.Common.Sully.ContentOptimizers.Common;
using Joins.Apps.Models.Sully;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace Joins.Apps.Common.Sully
{
    public static class ContentOptimizer
    {
        private static IOptimizer GetContentOptimizer(int cornerType)
        {
            if (cornerType == 4 || cornerType == 6)
                return ContentOptimizers.Optimizer_Type4.Optimizer.Instance;
            else if (cornerType == 5)
                return ContentOptimizers.Optimizer_Type5.Optimizer.Instance;
            else if (cornerType == 7)
                return ContentOptimizers.Optimizer_Type7.Optimizer.Instance;
            else if (cornerType == 8)
                return ContentOptimizers.Optimizer_Type8.Optimizer.Instance;

            return ContentOptimizers.Optimizer_Type1.Optimizer.Instance;
        }
        
        /// <summary>
        /// 기사내용을 최적화하여 객체로 반환합니다.
        /// </summary>
        /// <param name="product">기사정보</param>
        /// <param name="content">기사콘텐트 원본</param>
        /// <param name="recommendProduct">추천 기사</param>
        /// <param name="isDecodeHtmlChar">특수문자 HTML코드를 디코딩 할지 여부</param>
        /// <returns>정제된 객체</returns>
        public static ContentResultBase GetContent(Product product, string content, IEnumerable<Product> recommendProduct, bool isDecodeHtmlChar = false, bool isMobileAPI = false)
        {
            return GetContentOptimizer(product.PD_TYPE).GetContent(product, content, recommendProduct, isDecodeHtmlChar, isMobileAPI);
        }

        /// <summary>
        /// 기사내용을 최적화하여 HTML로 반환합니다.
        /// </summary>
        /// <param name="product">기사정보</param>
        /// <param name="recommendProduct">추천 기사</param>
        /// <param name="content">기사콘텐트 원본</param>
        /// <returns>정제된 HTML코드</returns>
        public static string GetContentHtml(Product product, IEnumerable<Product> recommendProduct, string content)
        {
            return GetContentOptimizer(product.PD_TYPE).GetContentHtml(product, recommendProduct, content);
        }

        public static string GeneratePreviewURL(int product_seq)
        {
            try
            {
                var data = string.Format("{0}", product_seq);
                var enc_data = JCube.AF.Module.Security.JoinsTripleDESEncrypt(data, "!ssUl#ly_pRe%viEw&");

                return string.Format("{0}/_/{1}", ConfigurationManager.AppSettings["SSully_Url"], enc_data);
            }
            catch
            {
                return "미리보기 전용 URL을 생성할 수 없습니다. 문의 주시기 바랍니다.";
            }
        }

        public static int? GetPreviewProductSeq(string key)
        {
            try
            {
                var data = JCube.AF.Module.Security.JoinsTripleDESDecrypt(key, "!ssUl#ly_pRe%viEw&");
                var seq = 0;

                if (int.TryParse(data, out seq))
                    return seq;

                return null;
            }
            catch
            {
                return null;
            }
        }
    }
}