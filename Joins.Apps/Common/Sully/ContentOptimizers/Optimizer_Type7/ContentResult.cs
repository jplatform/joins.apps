﻿using Joins.Apps.Common.Sully.ContentOptimizers.Common;
using Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type7.ResultEntity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type7
{
    /// <summary>
    /// 기사 데이터
    /// </summary>
    public class ContentResult : ContentResultBase
    {
        /// <summary>
        /// 인트로
        /// </summary>
        [JsonProperty("intro")]
        public Intro Intro { get; set; }

        /// <summary>
        /// 페이지별 데이터
        /// </summary>
        [JsonProperty("questions")]
        public IEnumerable<Question> Questions { get; set; }
    }
}
