﻿using Joins.Apps.Common.Sully.ContentOptimizers.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Joins.Apps.Models.Sully;
using Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type7.ResultEntity;
using System.Web;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type7
{
    public class Optimizer : BaseOptimizer, IOptimizer
    {
        private static readonly Lazy<Optimizer> lazy = new Lazy<Optimizer>(() => new Optimizer());

        public static Optimizer Instance
        {
            get { return lazy.Value; }
        }

        private Optimizer() { }

        /// <summary>
        /// 기사내용을 최적화하여 객체로 반환합니다.
        /// </summary>
        /// <param name="product">기사정보</param>
        /// <param name="content">기사콘텐트 원본</param>
        /// <param name="recommendProduct">추천 기사</param>
        /// <param name="isDecodeHtmlChar">특수문자 HTML코드를 디코딩 할지 여부</param>
        /// <returns>정제된 객체</returns>
        public ContentResultBase GetContent(Product product, string content, IEnumerable<Product> recommendProduct, bool isDecodeHtmlChar = false, bool isMobileAPI = false)
        {
            return GetMessageData(product, content, recommendProduct, isDecodeHtmlChar, isMobileAPI);
        }

        /// <summary>
        /// 기사내용을 최적화하여 HTML로 반환합니다.
        /// </summary>
        /// <param name="product">기사정보</param>
        /// <param name="recommentProduct">추천 기사</param>
        /// <param name="content">기사콘텐트 원본</param>
        /// <returns>정제된 HTML코드</returns>
        public string GetContentHtml(Product product, IEnumerable<Product> recommentProduct, string content)
        {
            var result = new StringBuilder();
            var entity = GetMessageData(product, content, recommentProduct);
            var question = entity.Questions.FirstOrDefault();

            result.AppendFormat(@"<div class=""quiz-cont"" data-quiz-seq=""{0}"">", question.QuestionSeq);
            result.AppendFormat(@"    <!-- 문제번호 : 01~20 까지");
            result.AppendFormat(@"        - 해설일땐 flag_explain_01.png");
            result.AppendFormat(@"    -->");
            result.AppendFormat(@"    <div class=""quiz-flag""><img src=""https://images.joins.com/ssully/common/quiz/flag_quiz_01.png"" alt=""""></div>");
            result.AppendFormat(@"    <!-- 스티커");
            result.AppendFormat(@"        [클래스]");
            result.AppendFormat(@"        -기본상태 : default");
            result.AppendFormat(@"        -정답일때 : right1,right2,right3 (랜덤)");
            result.AppendFormat(@"        -오답일때 : wrong");
            result.AppendFormat(@"    -->");
            result.AppendFormat(@"    <span class=""sticker default""></span>");
            result.AppendFormat(@"    <div class=""quiz-board"">");
            result.AppendFormat(@"        <div class=""panel"">");
            result.AppendFormat(@"            <!-- 문제 타이틀 , 해설 문구 -->");
            result.AppendFormat(@"            <p class=""q-title"">{0}</p>", question.Title);
            result.AppendFormat(@"            <!-- 문제 항목 -->");
            result.AppendFormat(@"            <ul class=""q-list"">");

            foreach (var item in question.Items)
                result.AppendFormat(@"                <li data-item-seq=""{0}""><span>{1}</span></li>", item.ItemSeq, item.Title);

            result.AppendFormat(@"            </ul>");
            result.AppendFormat(@"        </div>");
            result.AppendFormat(@"    </div>");
            
            result.AppendFormat(@"    <div class=""quiz-btns"">");
            result.AppendFormat(@"        <!-- 1번문제, 마지막문제일때 텍스트 변경 -->");
            result.AppendFormat(@"        <a href=""javascript:void(0);"" class=""go-prev"">처음으로</a>");
            result.AppendFormat(@"        <a href=""javascript:void(0);"" class=""go-next"">다음문제</a>");
            result.AppendFormat(@"    </div>");
            result.AppendFormat(@"</div>");

            return Convert.ToString(result);
        }

        private ContentResult GetMessageData(Product product, string content, IEnumerable<Product> recommendProduct, bool isDecodeHtmlChar = false, bool isMobileAPI = false)
        {
            var intro = new Intro();
            var questions = new List<Question>();

            if (!string.IsNullOrWhiteSpace(content))
            {
                var htmlDoc = new HtmlAgilityPack.HtmlDocument();
                htmlDoc.LoadHtml(content.Replace("\r\n", string.Empty));

                foreach (var node in htmlDoc.DocumentNode.ChildNodes)
                {
                    try
                    {
                        var nodeType = node.NodeType;
                        var nodeName = node.Name;
                        var ssully_module_type = node.GetAttributeValue("ssully_type", string.Empty);

                        if (nodeType == HtmlAgilityPack.HtmlNodeType.Element)
                        {
                            if ("br".Equals(nodeName))
                                continue;

                            if ("subtitle".Equals(ssully_module_type, StringComparison.OrdinalIgnoreCase))
                            {
                                // 부제
                                intro.Subtitle = DecodeHtmlString(isDecodeHtmlChar, node.InnerHtml);
                            }
                            else if ("page".Equals(ssully_module_type, StringComparison.OrdinalIgnoreCase))
                            {
                                // 문항
                                var title = node.SelectSingleNode("div[contains(@class, 'question_title')]");
                                var items = node.SelectNodes("div[contains(@class, 'question_item')]/ul/li");
                                var explanation = node.SelectSingleNode("div[contains(@class, 'description')]");

                                questions.Add(new Question {
                                    QuestionSeq = title != null ? title.GetAttributeValue("seq", 0) : 0,
                                    Title = DecodeHtmlString(isDecodeHtmlChar, title != null ? title.InnerHtml : null),
                                    Explanation = DecodeHtmlString(isDecodeHtmlChar, explanation != null ? explanation.InnerHtml : null),
                                    Items = items != null && items.Count > 0 ? items.Select(x => {
                                        var correctNode = x.SelectSingleNode("span[contains(@class, 'correct')]");

                                        if (correctNode != null)
                                            x.RemoveChild(correctNode);

                                        return new QuestionItem {
                                            ItemSeq = x.GetAttributeValue("seq", 0),
                                            Correct = correctNode != null ? "Y" : "N",
                                            Title = DecodeHtmlString(isDecodeHtmlChar, x.InnerHtml)
                                        };
                                    }) : null
                                });
                            }
                        }
                    }
                    catch { }
                }
            }

            return new ContentResult
            {
                Corner = CornerType.CODE_7,
                Intro = intro,
                Questions = questions,
                RecommendProduct = recommendProduct == null ? null : recommendProduct.Take(3).Select(x => new ProductInfo
                {
                    Seq = x.PD_SEQ,
                    Category = x.PD_CATEGORY,
                    CornerType = x.PD_TYPE,
                    Thumbnail = x.PD_IMG_URL,
                    Title = x.PD_TITLE,
                    ServiceDate = Convert.ToDateTime(x.PD_SERVICE_DT),
                    IsADProduct = x.IS_AD_PRODUCT
                }),
                Product = new ProductInfo
                {
                    Seq = product.PD_SEQ,
                    Category = product.PD_CATEGORY,
                    CornerType = product.PD_TYPE,
                    Thumbnail = product.PD_IMG_URL,
                    Title = product.PD_TITLE,
                    ServiceDate = Convert.ToDateTime(product.PD_SERVICE_DT),
                    IsADProduct = product.IS_AD_PRODUCT,
                    ProviderUrl = product.PD_PROVIDER_URL,
                    QuizEventPopupImageUrl = product.QUIZ_EVENT_POPUP_IMG_URL,
                    QuizEventPrizeImageUrl = product.QUIZ_EVENT_PRIZE_IMG_URL,
                    QuizEventNoticeDate = product.QUIZ_EVENT_NOTICE_DT,
                    QuizEventEndDate = product.QUIZ_EVENT_END_DT,
                    QuizEventStartDate = product.QUIZ_EVENT_START_DT,
                    QuizEventStatus = Convert.ToString(product.QUIZ_EVENT_STATUS),
                    LikeYN = product.LIKE_YN,
                    TotalLikeCount = product.LIKE_CNT,
                    ScrapYN = product.SCRAP_YN
                }
            };
        }
    }
}
