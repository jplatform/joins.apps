﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type7.ResultEntity
{
    public class Intro
    {
        /// <summary>
        /// 프로덕트 부제
        /// </summary>
        [JsonProperty("subtitle")]
        public string Subtitle { get; set; }
    }
}
