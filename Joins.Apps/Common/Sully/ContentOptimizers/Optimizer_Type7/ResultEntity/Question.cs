﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type7.ResultEntity
{
    public class Question
    {
        [JsonProperty("question_seq")]
        public int QuestionSeq { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("explanation")]
        public string Explanation { get; set; }
        [JsonProperty("items")]
        public IEnumerable<QuestionItem> Items { get; set; }
    }
}
