﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type7.ResultEntity
{
    public class QuestionItem
    {
        [JsonProperty("item_seq")]
        public int ItemSeq { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("correct_yn")]
        public string Correct { get; set; }
    }
}
