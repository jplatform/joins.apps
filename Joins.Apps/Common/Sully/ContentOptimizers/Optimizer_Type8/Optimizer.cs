﻿using Joins.Apps.Common.Sully.ContentOptimizers.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Joins.Apps.Models.Sully;
using System.Net.Http;
using System.Web;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type8
{
    public class Optimizer : BaseOptimizer, IOptimizer
    {
        private static readonly Lazy<Optimizer> lazy = new Lazy<Optimizer>(() => new Optimizer());

        public static Optimizer Instance
        {
            get { return lazy.Value; }
        }

        private Optimizer() { }

        /// <summary>
        /// 기사내용을 최적화하여 객체로 반환합니다.
        /// </summary>
        /// <param name="product">기사정보</param>
        /// <param name="content">기사콘텐트 원본</param>
        /// <param name="recommendProduct">추천 기사</param>
        /// <param name="isDecodeHtmlChar">특수문자 HTML코드를 디코딩 할지 여부</param>
        /// <returns>정제된 객체</returns>
        public ContentResultBase GetContent(Product product, string content, IEnumerable<Product> recommendProduct, bool isDecodeHtmlChar = false, bool isMobileAPI = false)
        {
            return GetMessageData(product, content, recommendProduct, isDecodeHtmlChar, isMobileAPI);
        }

        /// <summary>
        /// 기사내용을 최적화하여 HTML로 반환합니다.
        /// </summary>
        /// <param name="product">기사정보</param>
        /// <param name="recommentProduct">추천 기사</param>
        /// <param name="content">기사콘텐트 원본</param>
        /// <returns>정제된 HTML코드</returns>
        public string GetContentHtml(Product product, IEnumerable<Product> recommentProduct, string content)
        {
            return content;
        }

        private ContentResult GetMessageData(Product product, string content, IEnumerable<Product> recommendProduct, bool isDecodeHtmlChar = false, bool isMobileAPI = false)
        {
            var html = new StringBuilder();

            var like_class = "Y".Equals(product.LIKE_YN, StringComparison.OrdinalIgnoreCase) ? " on" : string.Empty;
            var scrap_class = "Y".Equals(product.SCRAP_YN, StringComparison.OrdinalIgnoreCase) ? " on" : string.Empty;
            
            html.AppendFormat(@"<!doctype html>");
            html.AppendFormat(@"<html lang=""ko"">");
            html.AppendFormat(@"<head>");
            html.AppendFormat(@"    <meta charset=""UTF-8"">");
            html.AppendFormat(@"    <meta http-equiv=""Pragma"" content=""no-cache"">");
            html.AppendFormat(@"    <meta name=""viewport"" content=""width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"">");
            html.AppendFormat(@"    <link rel=""stylesheet"" href=""https://fonts.googleapis.com/earlyaccess/notosanskr.css"">");
            html.AppendFormat(@"    <link rel=""stylesheet"" href=""https://static.joins.com/html/mobile/ssuly/html/css/renewal/ssully_common_test.css"">");
            html.AppendFormat(@"    <link rel=""stylesheet"" href=""https://static.joins.com/html/mobile/ssuly/html/css/renewal/ssully_sub_test.css"">");
            html.AppendFormat(@"    <link rel=""stylesheet"" href=""https://static.joins.com/html/mobile/ssuly/html/css/renewal/style_corner_test.css"">");
            html.AppendFormat(@"    <script type=""text/javascript"" src=""https://code.jquery.com/jquery-1.11.0.min.js""></script>");
            html.AppendFormat(@"    <script src=""https://static.joins.com/html/mobile/ssuly/html/js/ssully_ver3.js""></script>");
            html.AppendFormat(@"</head>");
            html.AppendFormat(@"<body>");
            html.AppendFormat(@"    <div class=""container presso-view webview"">");
            html.AppendFormat(@"        <article class=""view-wrap"">");
            html.AppendFormat(@"            <div class=""presso-article"">");
            html.AppendFormat(@"                {0}", content);
            html.AppendFormat(@"                <div class=""my-btns"">");
            html.AppendFormat(@"                    <a href=""ssully://?action=scrap&seq={0}"" class=""btn-scrap{1}"">스크랩</a>", product.PD_SEQ, scrap_class);
            html.AppendFormat(@"                    <a href=""ssully://?action=like&seq={0}"" class=""btn-favorite{2}""><span>{1}</span></a>", product.PD_SEQ, product.LIKE_CNT, like_class);
            html.AppendFormat(@"                </div>");
            html.AppendFormat(@"            </div>");

            if (recommendProduct?.Count() > 0)
            {
                html.AppendFormat(@"            <div class=""recommend"">");
                html.AppendFormat(@"                <dl class=""cat"">");
                html.AppendFormat(@"                    <dt>이것도 알려줄까? 썰리의 추천</dt>");

                foreach (var item in recommendProduct.Take(3))
                {
                    html.AppendFormat(@"                    <dd>");
                    html.AppendFormat(@"                        <a href=""ssully://?action=product&seq={0}&corner_type={1}&title={2}"">", item.PD_SEQ, item.PD_TYPE, EncodeUrlValue(isMobileAPI, item.PD_TITLE));
                    html.AppendFormat(@"                            <span class=""profile""><img src=""{0}"" alt=""""></span>", item.PD_IMG_URL);
                    html.AppendFormat(@"                            <span class=""txt"">");
                    html.AppendFormat(@"                                <strong>{0}</strong>", item.PD_TITLE);
                    html.AppendFormat(@"                                <span class=""time"">{0:yyyy.MM.dd}</span>", Convert.ToDateTime(item.PD_SERVICE_DT));
                    html.AppendFormat(@"                            </span>");
                    html.AppendFormat(@"                        </a>");
                    html.AppendFormat(@"                    </dd>");
                }

                html.AppendFormat(@"                </dl>");
                html.AppendFormat(@"            </div>");
            }

            html.AppendFormat(@"        </article>");
            html.AppendFormat(@"    </div>");

            html.AppendFormat(@"<script>");
            html.AppendFormat(@"    var fn_scrap = function(active) {{");
            html.AppendFormat(@"        if (active == 'Y')");
            html.AppendFormat(@"            $('div.my-btns a.btn-scrap').addClass('on');");
            html.AppendFormat(@"        else");
            html.AppendFormat(@"            $('div.my-btns a.btn-scrap').removeClass('on');");
            html.AppendFormat(@"    }};");
            html.AppendFormat(@"    var fn_like = function(active, val) {{");
            html.AppendFormat(@"        if (active == 'Y')");
            html.AppendFormat(@"            $('div.my-btns a.btn-favorite').addClass('on').find('span').text(val);");
            html.AppendFormat(@"        else");
            html.AppendFormat(@"            $('div.my-btns a.btn-favorite').removeClass('on').find('span').text(val);");
            html.AppendFormat(@"    }};");
            html.AppendFormat(@"    window.onload = function() {{");
            html.AppendFormat(@"        location.href = 'ssully://?action=load_completed';");
            html.AppendFormat(@"    }};");
            html.AppendFormat(@"</script>");

            html.AppendFormat(@"</body>");
            html.AppendFormat(@"</html>");

            return new ContentResult
            {
                Corner = CornerType.CODE_8,
                Html = Convert.ToString(html),
                RecommendProduct = recommendProduct == null ? null : recommendProduct.Take(3).Select(x => new ProductInfo
                {
                    Seq = x.PD_SEQ,
                    Category = x.PD_CATEGORY,
                    CornerType = x.PD_TYPE,
                    Thumbnail = x.PD_IMG_URL,
                    Title = x.PD_TITLE,
                    ServiceDate = Convert.ToDateTime(x.PD_SERVICE_DT),
                    IsADProduct = x.IS_AD_PRODUCT
                }),
                Product = new ProductInfo
                {
                    Seq = product.PD_SEQ,
                    Category = product.PD_CATEGORY,
                    CornerType = product.PD_TYPE,
                    Thumbnail = product.PD_IMG_URL,
                    Title = product.PD_TITLE,
                    ServiceDate = Convert.ToDateTime(product.PD_SERVICE_DT),
                    IsADProduct = product.IS_AD_PRODUCT,
                    ProviderUrl = product.PD_PROVIDER_URL,
                    LikeYN = product.LIKE_YN,
                    TotalLikeCount = product.LIKE_CNT,
                    ScrapYN = product.SCRAP_YN
                }
            };
        }
    }
}
