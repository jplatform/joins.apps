﻿using Joins.Apps.Common.Sully.ContentOptimizers.Common;
using Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type7.ResultEntity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type8
{
    /// <summary>
    /// 기사 데이터
    /// </summary>
    public class ContentResult : ContentResultBase
    {
        /// <summary>
        /// 본문내용
        /// </summary>
        [JsonProperty("html")]
        public string Html{ get; set; }
    }
}
