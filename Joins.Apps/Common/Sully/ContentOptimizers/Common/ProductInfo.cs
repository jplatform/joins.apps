﻿using Joins.Apps.Common.JsonConverter;
using Newtonsoft.Json;
using System;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Common
{
    /// <summary>
    /// 기사정보
    /// </summary>
    public class ProductInfo
    {
        /// <summary>
        /// 기사번호
        /// </summary>
        [JsonProperty("seq")]
        public int Seq { get; set; }
        /// <summary>
        /// 카테고리 코드
        /// </summary>
        [JsonProperty("category")]
        public string Category { get; set; }
        /// <summary>
        /// 코너 종류 코드
        /// </summary>
        [JsonProperty("corner_type")]
        public int CornerType { get; set; }
        /// <summary>
        /// 썸네일이미지주소
        /// </summary>
        [JsonProperty("thumbnail")]
        public string Thumbnail { get; set; }
        /// <summary>
        /// 기사제목
        /// </summary>
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 서비스 노출 시작일
        /// </summary>
        [JsonProperty("service_date")]
        [JsonConverter(typeof(JsonDateFormatConverter), "{0:yyyy.MM.dd}")]
        public DateTime? ServiceDate { get; set; }
        /// <summary>
        /// 광고 컨텐트 여부
        /// </summary>
        [JsonProperty("is_ad_product")]
        public string IsADProduct { get; set; }
        /// <summary>
        /// 라벨 이미지 주소
        /// </summary>
        [JsonProperty("provider_url")]
        public string ProviderUrl { get; set; }

        /// <summary>
        /// 썰리퀴즈 팝업이미지 주소
        /// </summary>
        [JsonProperty("quiz_event_popup_image_url")]
        public string QuizEventPopupImageUrl { get; set; }
        /// <summary>
        /// 썰리퀴즈 경품이미지 주소
        /// </summary>
        [JsonProperty("quiz_event_prize_image_url")]
        public string QuizEventPrizeImageUrl { get; set; }
        /// <summary>
        /// 썰리퀴즈 이벤트 시작일
        /// </summary>
        [JsonProperty("quiz_event_start_date")]
        [JsonConverter(typeof(JsonDateFormatConverter), "{0:yyyy.MM.dd}")]
        public DateTime? QuizEventStartDate { get; set; }
        /// <summary>
        /// 썰리퀴즈 이벤트 종료일
        /// </summary>
        [JsonProperty("quiz_event_end_date")]
        [JsonConverter(typeof(JsonDateFormatConverter), "{0:yyyy.MM.dd}")]
        public DateTime? QuizEventEndDate { get; set; }
        /// <summary>
        /// 썰리퀴즈 당첨자 발표일
        /// </summary>
        [JsonProperty("quiz_event_notice_date")]
        [JsonConverter(typeof(JsonDateFormatConverter), "{0:yyyy.MM.dd} {0:ddd} (썰리 공지사항)")]
        public DateTime? QuizEventNoticeDate { get; set; }
        /// <summary>
        /// 썰리퀴즈 이벤트 기간 문자열
        /// </summary>
        [JsonProperty("quiz_event_during_date")]
        public string QuizEventDuringDate { get { return string.Format("{0:yyyy.MM.dd} ~ {1:MM.dd}", QuizEventStartDate, QuizEventEndDate); } }
        /// <summary>
        /// 썰리퀴즈 이벤트 상태 ( 0 : 이벤트아님 / 1 : 대기 / 2 : 진행중 / 3 : 종료)
        /// </summary>
        [JsonProperty("quiz_event_status")]
        public string QuizEventStatus { get; set; }
        /// <summary>
        /// 총 좋아요 수
        /// </summary>
        [JsonProperty("total_like_cnt")]
        public int TotalLikeCount { get; set; }
        /// <summary>
        /// 좋아요 여부
        /// </summary>
        [JsonProperty("like_yn")]
        public string LikeYN { get; set; }
        /// <summary>
        /// 스크랩 여부
        /// </summary>
        [JsonProperty("scrap_yn")]
        public string ScrapYN { get; set; }
    }
}
