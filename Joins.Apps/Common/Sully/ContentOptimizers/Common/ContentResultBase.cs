﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Common
{
    public enum CornerType
    {
        /// <summary>
        /// 기본형(대화형)
        /// </summary>
        CODE_1,
        /// <summary>
        /// 썰리피디아
        /// </summary>
        CODE_2,
        /// <summary>
        /// 썰록사무소
        /// </summary>
        CODE_3,
        /// <summary>
        /// 한컷썰리
        /// </summary>
        CODE_4,
        /// <summary>
        /// 스토리썰링
        /// </summary>
        CODE_5,
        /// <summary>
        /// 여기가봤썰
        /// </summary>
        CODE_6,
        /// <summary>
        /// 썰리퀴즈
        /// </summary>
        CODE_7,
        /// <summary>
        /// 썰리프레소
        /// </summary>
        CODE_8
    }

    public class ContentResultBase
    {
        /// <summary>
        /// 추천 기사 정보
        /// </summary>
        [JsonProperty("recommend_products")]
        public IEnumerable<ProductInfo> RecommendProduct { get; set; }
        /// <summary>
        /// 기사 정보
        /// </summary>
        [JsonProperty("product")]
        public ProductInfo Product { get; set; }
        /// <summary>
        /// 공유 URL
        /// </summary>
        [JsonProperty("share_url")]
        public Uri ShareURL { get; set; }
        /// <summary>
        /// 코너 종류
        /// </summary>
        [JsonIgnore]
        public CornerType Corner { get; set; }
    }
}