﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Common
{
    public class BaseOptimizer
    {
        protected string DecodeHtmlString(bool isDecodeHtml, string content)
        {
            if (string.IsNullOrWhiteSpace(content))
                return string.Empty;

            if (isDecodeHtml)
            {
                content = Regex.Replace(content, @"<br\s*/?>", Environment.NewLine, RegexOptions.IgnoreCase);
                content = isDecodeHtml ? HttpContext.Current.Server.HtmlDecode(content) : content;
            }

            return content.Trim();
        }

        protected string EncodeUrlValue(bool isEncode, string content)
        {
            if (string.IsNullOrWhiteSpace(content))
                return string.Empty;

            if (isEncode)
                return HttpContext.Current.Server.UrlEncode(content);

            return content;
        }

        protected string GetExternalImageUrl(string image_url)
        {
            if (string.IsNullOrWhiteSpace(image_url))
                return string.Empty;

            var image_uri = new Uri(image_url);
            var isHttpImageUrl = string.Equals(image_uri.Scheme, Uri.UriSchemeHttp, StringComparison.OrdinalIgnoreCase);
            var isSecureConnection = HttpContext.Current.Request.IsSecureConnection;
            var ssully_url = ConfigurationManager.AppSettings["SSully_Url"];

            // 현재 https이고, 이미지 주소가 http인 경우에만
            if (isSecureConnection && isHttpImageUrl && !string.IsNullOrWhiteSpace(ssully_url))
                return string.Format("{0}/common/image?url={1}", ssully_url, HttpContext.Current.Server.UrlEncode(image_url));

            return image_url;
        }
    }
}
