﻿using Joins.Apps.Common.Sully.ContentOptimizers.Common;
using Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type1.ResultEntity;
using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Sully;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type1
{
    internal class Optimizer : BaseOptimizer, IOptimizer
    {
        private static readonly Lazy<Optimizer> lazy = new Lazy<Optimizer>(() => new Optimizer());

        public static Optimizer Instance
        {
            get { return lazy.Value; }
        }

        private Optimizer() { }

        /// <summary>
        /// 기사내용을 최적화하여 객체로 반환합니다.
        /// </summary>
        /// <param name="product">기사정보</param>
        /// <param name="content">기사콘텐트 원본</param>
        /// <param name="recommendProduct">추천 기사</param>
        /// <param name="isDecodeHtmlChar">특수문자 HTML코드를 디코딩 할지 여부</param>
        /// <returns>정제된 객체</returns>
        public ContentResultBase GetContent(Product product, string content, IEnumerable<Product> recommendProduct, bool isDecodeHtmlChar = false, bool isMobileAPI = false)
        {
            return GetMessageData(product, content, recommendProduct, isDecodeHtmlChar, isMobileAPI);
        }

        /// <summary>
        /// 기사내용을 최적화하여 HTML로 반환합니다.
        /// </summary>
        /// <param name="product">기사정보</param>
        /// <param name="content">기사콘텐트 원본</param>
        /// <returns>정제된 HTML코드</returns>
        public string GetContentHtml(Product product, IEnumerable<Product> recommentProduct, string content)
        {
            var result = new StringBuilder();
            var entity = GetMessageData(product, content, recommentProduct);

            result.AppendFormat(@"<div class=""chat-cont default roll-wrapper"">").AppendLine();

            foreach (var page in entity.Pages)
            {
                result.AppendFormat(@"            <!-- 파트 -->").AppendLine();
                result.AppendFormat(@"            <div class=""part roll-container"">").AppendLine();

                foreach (var block in page.MessageBlock)
                {
                    if (block.Type == MessageTarget.CHAT_A || block.Type == MessageTarget.CHAT_B)
                    {
                        var classname = string.Empty;
                        var thumbnail = string.Empty;

                        if (block.Type == MessageTarget.CHAT_A)
                        {
                            classname = "right";
                            thumbnail = entity.ChatInfo.FirstOrDefault(x => x.Target == MessageTarget.CHAT_A).Thumbnail;
                        }
                        else if (block.Type == MessageTarget.CHAT_B)
                        {
                            classname = "left";
                            thumbnail = entity.ChatInfo.FirstOrDefault(x => x.Target == MessageTarget.CHAT_B).Thumbnail;
                        }

                        result.AppendFormat(@"                <!-- 화자 -->").AppendLine();
                        result.AppendFormat(@"                <div class=""unit {0} roll-block _chat"">", classname).AppendLine();
                        result.AppendFormat(@"                    <span class=""speaker""><img src=""{0}"" alt=""speaker""></span>", thumbnail).AppendLine();

                        #region Message

                        result.AppendFormat(@"                    <ul class=""script"">").AppendLine();

                        foreach (var message in block.Message)
                        {
                            result.AppendFormat(@"                        <li class=""roll-item"">").AppendLine();

                            // 메시지타입
                            switch (message.Type)
                            {
                                case MessageTypeEnum.LINK:
                                    // 링크
                                    result.AppendFormat(@"                            <div class=""box fit link"">").AppendLine();

                                    result.AppendFormat(@"                                <a href=""{0}"" target=""_blank"" rel=""noreferrer"">", message.Url).AppendLine();
                                    result.AppendFormat(@"                                    <div class=""thumb"">").AppendLine();
                                    result.AppendFormat(@"                                        <img src=""{0}"" alt="""">", GetExternalImageUrl(message.Thumbnail)).AppendLine();
                                    result.AppendFormat(@"                                    </div>").AppendLine();
                                    result.AppendFormat(@"                                    <div class=""caption"">").AppendLine();
                                    result.AppendFormat(@"                                        <p>{0}</p>", message.Text).AppendLine();
                                    result.AppendFormat(@"                                        <span class=""source"">{0}</span>", message.Domain).AppendLine();
                                    result.AppendFormat(@"                                    </div>").AppendLine();
                                    result.AppendFormat(@"                                </a>").AppendLine();
                                    result.AppendFormat(@"                            </div>").AppendLine();
                                    break;
                                case MessageTypeEnum.TEXT:
                                    // 메시지
                                    result.AppendFormat(@"                            <div class=""box"">{0}</div>", message.Text).AppendLine();
                                    break;
                                case MessageTypeEnum.STICKER:
                                    // 스티커
                                    result.AppendFormat(@"                            <div class=""box fit"">").AppendLine();
                                    result.AppendFormat(@"                                <img src=""{0}"" title=""{1}"" class=""emotion"">", message.Url, message.Text).AppendLine();
                                    result.AppendFormat(@"                            </div>").AppendLine();
                                    break;
                                case MessageTypeEnum.IMAGE:
                                    // 이미지
                                    result.AppendFormat(@"                            <div class=""box fit"">").AppendLine();
                                    result.AppendFormat(@"                                <span class=""trg-layer"">").AppendLine();
                                    result.AppendFormat(@"                                    <img src=""{0}"" title=""{1}"">", message.Url, message.Text).AppendLine();
                                    result.AppendFormat(@"                                    <i class=""expand"">크게보기</i>").AppendLine();
                                    result.AppendFormat(@"                                </span>").AppendLine();

                                    if (!string.IsNullOrWhiteSpace(message.Text))
                                        result.AppendFormat(@"                                <span class=""txt-caption"">{0}</span>", message.Text).AppendLine();

                                    result.AppendFormat(@"                            </div>").AppendLine();
                                    break;
                                case MessageTypeEnum.VIDEO:
                                    // 동영상
                                    result.AppendFormat(@"                            <div class=""box fit video"">").AppendLine();
                                    result.AppendFormat(@"                                <iframe allow=""encrypted-media"" allowfullscreen="""" frameborder=""0"" gesture=""media"" src=""{0}""></iframe>", message.Url).AppendLine();
                                    result.AppendFormat(@"                            </div>").AppendLine();
                                    break;
                            }

                            result.AppendFormat(@"                        </li>").AppendLine();
                        }

                        result.AppendFormat(@"                    </ul>").AppendLine();

                        #endregion

                        result.AppendFormat(@"                </div>").AppendLine();
                        result.AppendFormat(@"                <!--// 화자 -->").AppendLine();
                    }
                    else if (block.Type == MessageTarget.SUMMARY)
                    {
                        result.AppendFormat(@"                <!-- 요약 -->").AppendLine();
                        result.AppendFormat(@"                <div class=""unit _list summary roll-block"">").AppendLine();
                        result.AppendFormat(@"                    <p class=""title-list"">어렵다고? 더 요약한다!</p>").AppendLine();
                        result.AppendFormat(@"                    <ul class=""summary-list"">").AppendLine();

                        foreach(var item in block.Message)
                            result.AppendFormat(@"                        <li>{0}</li>", item.Text).AppendLine();

                        result.AppendFormat(@"                    </ul>").AppendLine();
                        result.AppendFormat(@"                </div>").AppendLine();
                        result.AppendFormat(@"                <!--// 요약 -->").AppendLine();
                    }
                }

                if (entity.Pages.LastOrDefault() == page)
                {
                    // 페이지 마지막인 경우 추천 컨텐츠 렌더링
                    if (entity.RecommendProduct != null && entity.RecommendProduct.Count() > 0)
                    {
                        result.AppendFormat(@"                <!-- 추천 -->").AppendLine();
                        result.AppendFormat(@"                <div class=""unit _list recommend roll-block"">").AppendLine();
                        result.AppendFormat(@"                    <p class=""title-list"">이것도 알려줄까? 썰리의 추천</p>").AppendLine();
                        result.AppendFormat(@"                    <ul class=""recommend-list"">").AppendLine();

                        foreach (var item in entity.RecommendProduct)
                        {
                            result.AppendFormat(@"                        <li>").AppendLine();
                            result.AppendFormat(@"                            <a href=""{0}"">", item.Seq).AppendLine();
                            result.AppendFormat(@"                                <div class=""thumb""><img src=""{0}"" alt=""""></div>", item.Thumbnail).AppendLine();
                            result.AppendFormat(@"                                <div class=""txt"">").AppendLine();
                            result.AppendFormat(@"                                    <span class=""subject"">{0}</span>", item.Title).AppendLine();
                            result.AppendFormat(@"                                    <span class=""date"">{0:yyyy.MM.dd}</span>", item.ServiceDate).AppendLine();
                            result.AppendFormat(@"                                </div>").AppendLine();
                            result.AppendFormat(@"                            </a>").AppendLine();
                            result.AppendFormat(@"                        </li>").AppendLine();
                        }

                        result.AppendFormat(@"                    </ul>").AppendLine();
                        result.AppendFormat(@"                </div>").AppendLine();
                        result.AppendFormat(@"                <!--// 추천 -->").AppendLine();
                    }
                }
                else
                {
                    // 마지막 페이지가 아닌경우 페이지 버튼 생성
                    result.AppendFormat(@"                <div class=""btn-continue"">").AppendLine();
                    result.AppendFormat(@"                    <button type=""button"" class=""go-next"">").AppendLine();
                    result.AppendFormat(@"                        <span>{0}</span>", page.MoreTitle).AppendLine();
                    result.AppendFormat(@"                    </button>").AppendLine();
                    result.AppendFormat(@"                </div>").AppendLine();
                }

                result.AppendFormat(@"            </div>").AppendLine();
                result.AppendFormat(@"            <!--// 파트 -->").AppendLine();
            }

            result.AppendFormat(@"        </div>").AppendLine();

            return Convert.ToString(result);
        }

        private ContentResult GetMessageData(Product product, string content, IEnumerable<Product> recommendProduct, bool isDecodeHtmlChar = false, bool isMobileAPI = false)
        {
            var pages = new List<Page>();

            if (!string.IsNullOrWhiteSpace(content))
            {
                var htmlDoc = new HtmlAgilityPack.HtmlDocument();
                htmlDoc.LoadHtml(content.Replace("\r\n", string.Empty));

                foreach (var node in htmlDoc.DocumentNode.ChildNodes)
                {
                    try
                    {
                        var nodeType = node.NodeType;
                        var nodeName = node.Name;
                        var page = pages.LastOrDefault();
                        if (page == null)
                        {
                            pages.Add(new Page()
                            {
                                MessageBlock = new List<MessageBlock>()
                            });
                            page = pages.LastOrDefault();
                        }

                        var currentMessageBlock = page.MessageBlock.LastOrDefault();
                        var ssully_module_type = node.GetAttributeValue("ssully_type", string.Empty);

                        if (nodeType == HtmlAgilityPack.HtmlNodeType.Text)
                        {
                            var nodeText = node.InnerText.Trim();

                            // 새로운 대화자 시작될 때 메세지 블럭 추가
                            if (Regex.IsMatch(nodeText, @"(A|B)(\s*)\:", RegexOptions.IgnoreCase))
                            {
                                currentMessageBlock = new MessageBlock()
                                {
                                    Message = new List<Message>(),
                                    Type = Regex.IsMatch(nodeText, @"A(\s*)\:", RegexOptions.IgnoreCase) ? MessageTarget.CHAT_A : MessageTarget.CHAT_B
                                };
                                page.MessageBlock.Add(currentMessageBlock);
                            }

                            nodeText = Regex.Replace(nodeText, @"(A|B)(\s*)\:", string.Empty, RegexOptions.IgnoreCase).Replace("&nbsp;", " ");
                            if (!string.IsNullOrWhiteSpace(nodeText))
                            {
                                var text = (nodeText ?? string.Empty).Trim();
                                if (currentMessageBlock.Type != MessageTarget.SUMMARY)
                                {
                                    currentMessageBlock.Message.Add(new Message
                                    {
                                        Text = DecodeHtmlString(isDecodeHtmlChar, text),
                                        Type = MessageTypeEnum.TEXT
                                    });
                                }
                            }
                        }
                        else if (nodeType == HtmlAgilityPack.HtmlNodeType.Element)
                        {
                            if ("br".Equals(nodeName))
                                continue;

                            // 페이지 구분
                            if ("hr".Equals(nodeName) || node.Attributes.Contains("ssully_page_break"))
                            {
                                var tmpTitle = DecodeHtmlString(true, node.InnerText).Trim();
                                var pageTitle = string.IsNullOrWhiteSpace(tmpTitle) ? "그래서? 그래서?" : node.InnerText.Trim();

                                var prevPage = pages.LastOrDefault();
                                if (prevPage != null && prevPage.MessageBlock.Count > 0)
                                    prevPage.MoreTitle = DecodeHtmlString(isDecodeHtmlChar, pageTitle);

                                if (prevPage.MessageBlock.Count > 0)
                                {
                                    pages.Add(new Page()
                                    {
                                        MessageBlock = new List<MessageBlock>()
                                    });
                                    page = pages.LastOrDefault();
                                }
                            }
                            else if (currentMessageBlock != null)
                            {
                                if (string.IsNullOrWhiteSpace(ssully_module_type))
                                {
                                    #region 기존 메세지 파싱

                                    var message = currentMessageBlock.Message.LastOrDefault();

                                    if (message != null && message.Type == MessageTypeEnum.LINK)
                                    {
                                        var linkTag = node.Descendants("a").Where(x => x.Attributes.Contains("href")).FirstOrDefault();
                                        if (linkTag != null)
                                            message.Url = DecodeHtmlString(true, linkTag.Attributes["href"].Value);

                                        var domainTag = node.Descendants("i").Where(x => x.HasClass("domain")).FirstOrDefault();
                                        if (domainTag != null)
                                            message.Domain = domainTag.InnerText;

                                        var thumbnailWrapperTag = node.Descendants("i").Where(x => x.HasClass("img")).FirstOrDefault();
                                        var thumbnailTag = thumbnailWrapperTag != null ? thumbnailWrapperTag.Descendants("img").Where(x => x.Attributes.Contains("src")).FirstOrDefault() : null;
                                        if (thumbnailTag != null)
                                            message.Thumbnail = DecodeHtmlString(true, thumbnailTag.Attributes["src"].Value);

                                        var titleTag = node.Descendants("em").FirstOrDefault();
                                        if (titleTag != null)
                                            message.Text = DecodeHtmlString(isDecodeHtmlChar, titleTag.InnerText);

                                        // 썰리 내부링크 여부 판단
                                        if (!string.IsNullOrWhiteSpace(message.Url))
                                        {
                                            var pattern = @"https?\:\/\/ssully.joins.com\/view\/(\d+)\?.*";

                                            if (Regex.IsMatch(message.Url, pattern, RegexOptions.IgnoreCase))
                                            {
                                                var seq = Convert.ToInt32(Regex.Match(message.Url, pattern, RegexOptions.IgnoreCase).Groups[1].Value);
                                                var ssully_product = ProductRepository.GetProductInfo(seq);

                                                message.SSully_Seq = seq;

                                                if (ssully_product != null)
                                                {
                                                    message.SSully_Corner_Type = ssully_product.PD_TYPE;
                                                    message.SSully_IsADProduct = ssully_product.IS_AD_PRODUCT;
                                                }
                                            }
                                        }
                                    }
                                    else if (currentMessageBlock.Type == MessageTarget.SUMMARY)
                                    {
                                        if (node.SelectNodes("li") == null)
                                            continue;

                                        foreach (var element in node.SelectNodes("li"))
                                        {
                                            var text = (element.InnerText ?? string.Empty).Trim();
                                            currentMessageBlock.Message.Add(new Message
                                            {
                                                Text = DecodeHtmlString(isDecodeHtmlChar, text),
                                                Type = MessageTypeEnum.TEXT
                                            });
                                        }
                                    }
                                    else if (nodeName == "img")
                                    {
                                        var imageSrc = DecodeHtmlString(true, node.Attributes.Contains("src") ? node.Attributes["src"].Value : null);
                                        var imageTitle = node.Attributes.Contains("title") ? node.Attributes["title"].Value : null;
                                        currentMessageBlock.Message.Add(new Message
                                        {
                                            Url = imageSrc,
                                            Text = imageSrc.IndexOf("sticker") > -1 ? null : DecodeHtmlString(isDecodeHtmlChar, imageTitle).Trim(),
                                            Type = imageSrc.IndexOf("sticker") > -1 ? MessageTypeEnum.STICKER : MessageTypeEnum.IMAGE
                                        });
                                    }
                                    else if (nodeName == "iframe")
                                    {
                                        var videoSrc = DecodeHtmlString(true, node.Attributes.Contains("src") ? node.Attributes["src"].Value : null);
                                        var referrer = node.Attributes.Contains("ssully_video_referrer") ? node.Attributes["ssully_video_referrer"].Value : "YOUTUBE";
                                        var videoThumbnail = string.Empty;

                                        if (isMobileAPI)
                                        {
                                            var uri = new Uri(videoSrc);

                                            if ("youtube".Equals(referrer, StringComparison.OrdinalIgnoreCase))
                                            {
                                                if (!string.IsNullOrWhiteSpace(uri.Query))
                                                    videoSrc = uri.AbsoluteUri.Replace(uri.Query, string.Empty);
                                            }
                                        }

                                        currentMessageBlock.Message.Add(new Message
                                        {
                                            Url = videoSrc,
                                            Type = MessageTypeEnum.VIDEO,
                                            Thumbnail = string.IsNullOrWhiteSpace(videoThumbnail) ? null : videoThumbnail,
                                            VideoSource = referrer.ToUpper()
                                        });
                                    }

                                    #endregion
                                }
                                else
                                {
                                    // 기사 링크
                                    if ("link".Equals(ssully_module_type, StringComparison.OrdinalIgnoreCase))
                                    {
                                        var thumbnail = node.SelectSingleNode("div[contains(@class, 'thumb')]/img");
                                        var title = node.SelectSingleNode("div[contains(@class, 'title')]");
                                        var domain = node.SelectSingleNode("div[contains(@class, 'domain')]");
                                        var url = node.GetAttributeValue("ssully_href", string.Empty);

                                        var message = new Message
                                        {
                                            Url = DecodeHtmlString(true, url),
                                            Thumbnail = DecodeHtmlString(true, thumbnail != null ? thumbnail.GetAttributeValue("src", string.Empty) : string.Empty),
                                            Domain = domain != null ? domain.InnerText : string.Empty,
                                            Text = title != null ? DecodeHtmlString(isDecodeHtmlChar, title.InnerText) : string.Empty
                                        };

                                        // 썰리 내부링크 여부 판단
                                        if (!string.IsNullOrWhiteSpace(message.Url))
                                        {
                                            var pattern = @"https?\:\/\/ssully.joins.com\/view\/(\d+)\?.*";

                                            if (Regex.IsMatch(message.Url, pattern, RegexOptions.IgnoreCase))
                                            {
                                                var seq = Convert.ToInt32(Regex.Match(message.Url, pattern, RegexOptions.IgnoreCase).Groups[1].Value);
                                                var ssully_product = ProductRepository.GetProductInfo(seq);

                                                message.SSully_Seq = seq;

                                                if (ssully_product != null)
                                                {
                                                    message.SSully_Corner_Type = ssully_product.PD_TYPE;
                                                    message.SSully_IsADProduct = ssully_product.IS_AD_PRODUCT;
                                                }
                                            }
                                        }

                                        currentMessageBlock.Message.Add(message);
                                    }

                                    // 비디오 링크
                                    if ("video".Equals(ssully_module_type, StringComparison.OrdinalIgnoreCase))
                                    {
                                        var videoSrc = DecodeHtmlString(true, node.GetAttributeValue("src", string.Empty));
                                        var referrer = node.GetAttributeValue("ssully_video_referrer", "YOUTUBE");
                                        var videoThumbnail = string.Empty;

                                        if (isMobileAPI)
                                        {
                                            var uri = new Uri(videoSrc);

                                            if ("youtube".Equals(referrer, StringComparison.OrdinalIgnoreCase))
                                            {
                                                if (!string.IsNullOrWhiteSpace(uri.Query))
                                                    videoSrc = uri.AbsoluteUri.Replace(uri.Query, string.Empty);
                                            }
                                        }

                                        currentMessageBlock.Message.Add(new Message
                                        {
                                            Url = videoSrc,
                                            Type = MessageTypeEnum.VIDEO,
                                            Thumbnail = string.IsNullOrWhiteSpace(videoThumbnail) ? null : videoThumbnail,
                                            VideoSource = referrer.ToUpper()
                                        });
                                    }
                                    else if ("summary".Equals(ssully_module_type, StringComparison.OrdinalIgnoreCase))
                                    {
                                        // 3줄요약
                                        if (node.SelectNodes("li") == null)
                                            continue;

                                        currentMessageBlock = new MessageBlock
                                        {
                                            Message = new List<Message>(),
                                            Type = MessageTarget.SUMMARY
                                        };

                                        foreach (var item in node.SelectNodes("li"))
                                        {
                                            var text = (item.InnerText ?? string.Empty).Trim();
                                            currentMessageBlock.Message.Add(new Message
                                            {
                                                Text = DecodeHtmlString(isDecodeHtmlChar, text),
                                                Type = MessageTypeEnum.TEXT
                                            });
                                        }

                                        page.MessageBlock.Add(currentMessageBlock);
                                    }
                                }
                            }
                        }
                        else if (nodeType == HtmlAgilityPack.HtmlNodeType.Comment)
                        {
                            if (node.InnerHtml == "<!--data_link_type-->")
                            {
                                // 페이지 연결링크 블럭
                                currentMessageBlock.Message.Add(new Message { Type = MessageTypeEnum.LINK });
                            }
                            else if (node.InnerHtml == "<!--3line_summary_s-->")
                            {
                                // 3줄요약 블럭
                                page.MessageBlock.Add(new MessageBlock { Message = new List<Message>(), Type = MessageTarget.SUMMARY });
                                currentMessageBlock = page.MessageBlock.LastOrDefault();
                            }
                        }
                    }
                    catch { }
                }
            }

            return new ContentResult
            {
                Corner = CornerType.CODE_1,
                Pages = pages,
                ChatInfo = new List<MessageBlockTypeEnum>
                {
                    new MessageBlockTypeEnum { Target = MessageTarget.CHAT_A, Thumbnail = product.CR_IMG_01, Name = product.CR_NAME_01 },
                    new MessageBlockTypeEnum { Target = MessageTarget.CHAT_B, Thumbnail = product.CR_IMG_02, Name = product.CR_NAME_02 }
                },
                RecommendProduct = recommendProduct == null ? null : recommendProduct.Take(3).Select(x => new ProductInfo
                {
                    Seq = x.PD_SEQ,
                    Category = x.PD_CATEGORY,
                    CornerType = x.PD_TYPE,
                    Thumbnail = x.PD_IMG_URL,
                    Title = x.PD_TITLE,
                    ServiceDate = Convert.ToDateTime(x.PD_SERVICE_DT),
                    IsADProduct = x.IS_AD_PRODUCT
                }),
                Product = new ProductInfo
                {
                    Seq = product.PD_SEQ,
                    Category = product.PD_CATEGORY,
                    CornerType = product.PD_TYPE,
                    Thumbnail = product.PD_IMG_URL,
                    Title = product.PD_TITLE,
                    ServiceDate = Convert.ToDateTime(product.PD_SERVICE_DT),
                    IsADProduct = product.IS_AD_PRODUCT,
                    ProviderUrl = product.PD_PROVIDER_URL,
                    LikeYN = product.LIKE_YN,
                    TotalLikeCount = product.LIKE_CNT,
                    ScrapYN = product.SCRAP_YN
                }
            };
        }
    }
}
