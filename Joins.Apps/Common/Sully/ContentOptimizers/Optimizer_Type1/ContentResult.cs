﻿using Joins.Apps.Common.Sully.ContentOptimizers.Common;
using Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type1.ResultEntity;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type1
{
    /// <summary>
    /// 기사 데이터
    /// </summary>
    public class ContentResult : ContentResultBase
    {
        /// <summary>
        /// 페이지별 데이터
        /// </summary>
        [JsonProperty("pages")]
        public List<Page> Pages { get; set; }
        /// <summary>
        /// 대화 캐릭터 정보
        /// </summary>
        [JsonProperty("chat_info")]
        public List<MessageBlockTypeEnum> ChatInfo { get; set; }
    }
}
