﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type1.ResultEntity
{
    /// <summary>
    /// 페이지
    /// </summary>
    public class Page
    {
        /// <summary>
        /// 페이지 분리 제목
        /// </summary>
        [JsonProperty("more_title")]
        public string MoreTitle { get; set; }
        /// <summary>
        /// 메세지 블럭
        /// </summary>
        [JsonProperty("message_block")]
        public List<MessageBlock> MessageBlock { get; set; }
    }
}
