﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type1.ResultEntity
{
    /// <summary>
    /// 메세지 블럭
    /// </summary>
    public class MessageBlock
    {
        /// <summary>
        /// 메세지
        /// </summary>
        [JsonProperty("message")]
        public List<Message> Message { get; set; }
        /// <summary>
        /// 메세지 타입
        /// </summary>
        [JsonProperty("type")]
        public MessageTarget Type { get; set; }
    }
}
