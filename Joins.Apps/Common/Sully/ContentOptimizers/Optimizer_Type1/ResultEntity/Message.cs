﻿using Joins.Apps.Common.JsonConverter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type1.ResultEntity
{
    /// <summary>
    /// 메세지
    /// </summary>
    public class Message
    {
        /// <summary>
        /// 텍스트
        /// </summary>
        [JsonProperty("text")]
        public string Text { get; set; }
        /// <summary>
        /// 링크 주소
        /// </summary>
        [JsonProperty("url")]
        public string Url { get; set; }
        /// <summary>
        /// 링크 썸네일
        /// </summary>
        [JsonProperty("thumbnail")]
        public string Thumbnail { get; set; }
        /// <summary>
        /// 링크 도메인
        /// </summary>
        [JsonProperty("domain")]
        public string Domain { get; set; }
        /// <summary>
        /// 메세지 타입
        /// </summary>
        [JsonProperty("type")]
        public MessageTypeEnum Type { get; set; }
        /// <summary>
        /// 동영상 출처
        /// </summary>
        [JsonProperty("video_source")]
        public string VideoSource { get; set; }
        /// <summary>
        /// 썰리 내부기사 링크시 고유번호
        /// </summary>
        [JsonProperty("ssully_seq")]
        public int? SSully_Seq { get; set; }
        /// <summary>
        /// 썰리 내부기사 링크시 코너형식 코드
        /// </summary>
        [JsonProperty("ssully_corner_type")]
        public int? SSully_Corner_Type { get; set; }
        /// <summary>
        /// 썰리 내부기사 링크시 광고 컨텐트 여부
        /// </summary>
        [JsonProperty("ssully_is_ad_product")]
        public string SSully_IsADProduct { get; set; }
    }
}
