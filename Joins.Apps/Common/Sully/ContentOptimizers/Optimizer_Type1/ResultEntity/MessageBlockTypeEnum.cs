﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type1.ResultEntity
{
    /// <summary>
    /// 대화 참여자 정보
    /// </summary>
    public class MessageBlockTypeEnum
    {
        /// <summary>
        /// 대상자
        /// </summary>
        [JsonProperty("target")]
        public MessageTarget Target { get; set; }
        /// <summary>
        /// 썸네일
        /// </summary>
        [JsonProperty("thumbnail")]
        public string Thumbnail { get; set; }
        /// <summary>
        /// 이름
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
