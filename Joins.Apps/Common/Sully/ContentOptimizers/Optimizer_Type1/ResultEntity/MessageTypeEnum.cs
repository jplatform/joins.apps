﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type1.ResultEntity
{
    /// <summary>
    /// 메세지 형식
    /// </summary>
    public enum MessageTypeEnum
    {
        /// <summary>
        /// 링크
        /// </summary>
        LINK,
        /// <summary>
        /// 일반 텍스트
        /// </summary>
        TEXT,
        /// <summary>
        /// 스티커 주소
        /// </summary>
        STICKER,
        /// <summary>
        /// 이미지 주소
        /// </summary>
        IMAGE,
        /// <summary>
        /// 영상
        /// </summary>
        VIDEO
    }
}
