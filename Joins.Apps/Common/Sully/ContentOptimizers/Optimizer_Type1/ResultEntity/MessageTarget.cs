﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type1.ResultEntity
{
    /// <summary>
    /// 메세지 종류
    /// </summary>
    public enum MessageTarget
    {
        /// <summary>
        /// A캐릭터 (본인)
        /// </summary>
        CHAT_A,
        /// <summary>
        /// B캐릭터 (상대방)
        /// </summary>
        CHAT_B,
        /// <summary>
        /// 3줄요약
        /// </summary>
        SUMMARY
    }
}
