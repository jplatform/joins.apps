﻿using Joins.Apps.Common.Sully.ContentOptimizers.Common;
using Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type5.ResultEntity;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type5
{
    /// <summary>
    /// 기사 데이터
    /// </summary>
    public class ContentResult : ContentResultBase
    {
        /// <summary>
        /// 인트로
        /// </summary>
        [JsonProperty("intro")]
        public Intro Intro { get; set; }
        /// <summary>
        /// 페이지별 데이터
        /// </summary>
        [JsonProperty("pages")]
        public IEnumerable<Page> Pages { get; set; }
    }
}
