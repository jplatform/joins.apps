﻿using Joins.Apps.Common.Sully.ContentOptimizers.Common;
using Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type5.ResultEntity;
using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Sully;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type5
{
    internal class Optimizer : BaseOptimizer, IOptimizer
    {
        private static readonly Lazy<Optimizer> lazy = new Lazy<Optimizer>(() => new Optimizer());

        public static Optimizer Instance
        {
            get { return lazy.Value; }
        }

        private Optimizer() { }

        /// <summary>
        /// 기사내용을 최적화하여 객체로 반환합니다.
        /// </summary>
        /// <param name="product">기사정보</param>
        /// <param name="content">기사콘텐트 원본</param>
        /// <param name="recommendProduct">추천 기사</param>
        /// <param name="isDecodeHtmlChar">특수문자 HTML코드를 디코딩 할지 여부</param>
        /// <returns>정제된 객체</returns>
        public ContentResultBase GetContent(Product product, string content, IEnumerable<Product> recommendProduct, bool isDecodeHtmlChar, bool isMobileAPI = false)
        {
            return GetMessageData(product, content, recommendProduct, isDecodeHtmlChar, isMobileAPI);
        }

        /// <summary>
        /// 기사내용을 최적화하여 HTML로 반환합니다.
        /// </summary>
        /// <param name="product">기사정보</param>
        /// <param name="recommentProduct">추천 기사</param>
        /// <param name="content">기사콘텐트 원본</param>
        /// <returns>정제된 HTML코드</returns>
        public string GetContentHtml(Product product, IEnumerable<Product> recommendProduct, string content)
        {
            var result = new StringBuilder();
            var entity = GetMessageData(product, content, recommendProduct);

            result.AppendFormat(@"<div class=""view-cont"" data-color=""{0}"">", entity.Intro.BackgroundColor);

            // 인트로 화면
            #region 인트로
            
            result.AppendFormat(@"  <div class=""intro page theme-color"">");
            result.AppendFormat(@"      <div class=""_cont"">");
            result.AppendFormat(@"          <div class=""intro-sticker txt-hidden"" data-sticker=""{0}"">Sticker</div>", entity.Intro.BackgroundImage);
            result.AppendFormat(@"          <div class=""panel"">");
            result.AppendFormat(@"              <h2 class=""story-title"">{0}</h2>", entity.Product.Title);
            result.AppendFormat(@"              <p class=""story-subtitle"">{0}</p>", entity.Intro.Subtitle);
            result.AppendFormat(@"              <a href=""#"" id=""btn-intoStory"" class=""txt-hidden"">시작하기</a>");
            result.AppendFormat(@"          </div>");
            result.AppendFormat(@"      </div>");
            result.AppendFormat(@"  </div>");

            #endregion
            //-- 인트로 화면

            foreach (var page in entity.Pages)
            {
                if (page.ParticleType == ParticleTypeEnum.LINK)
                {
                    // 링크
                    var particle = page.Particle as Particle_Link;

                    if (!string.IsNullOrWhiteSpace(page.BackgroundImage))
                        result.AppendFormat(@"  <div class=""page theme-image"" data-bg=""{0}"">", page.BackgroundImage);
                    else
                        result.AppendFormat(@"  <div class=""page theme-color"">");

                    result.AppendFormat(@"      <div class=""_cont"">");
                    result.AppendFormat(@"          <div class=""bg-layer""></div>");
                    result.AppendFormat(@"          <div class=""op-layer""></div>");
                    result.AppendFormat(@"          <div class=""panel"">");

                    if (!string.IsNullOrWhiteSpace(HttpUtility.HtmlDecode(page.LeadTitle)))
                        result.AppendFormat(@"              <h3 class=""reading-txt"">{0}</h3>", page.LeadTitle);

                    result.AppendFormat(@"              <ul class=""story-list"">");

                    foreach (var description in page.Description)
                    {
                        result.AppendFormat(@"                  <li><span class=""box"">{0}</span></li>", description);
                    }

                    result.AppendFormat(@"                  <li class=""items link"">");
                    result.AppendFormat(@"                      <a href=""{0}"" target=""_blank"" rel=""noreferrer"">", particle.LinkUrl);
                    result.AppendFormat(@"                          <figure>");
                    result.AppendFormat(@"                              <img src=""{0}"" alt="""">", GetExternalImageUrl(particle.Thumbnail));
                    result.AppendFormat(@"                              <figcaption>");
                    result.AppendFormat(@"                                  <p>{0}</p>", particle.Title);
                    result.AppendFormat(@"                                  <span>{0}</span>", particle.Domain);
                    result.AppendFormat(@"                              </figcaption>");
                    result.AppendFormat(@"                          </figure>");
                    result.AppendFormat(@"                      </a>");
                    result.AppendFormat(@"                  </li>");
                    result.AppendFormat(@"              </ul>");
                    result.AppendFormat(@"          </div>");
                    result.AppendFormat(@"      </div>");
                    result.AppendFormat(@"  </div>");
                }
                else if (page.ParticleType == ParticleTypeEnum.IMAGE)
                {
                    // 이미지
                    var particle = page.Particle as Particle_Image;

                    if (!string.IsNullOrWhiteSpace(page.BackgroundImage))
                        result.AppendFormat(@"  <div class=""page theme-image"" data-bg=""{0}"">", page.BackgroundImage);
                    else
                        result.AppendFormat(@"  <div class=""page theme-color"">");

                    result.AppendFormat(@"      <div class=""_cont"">");
                    result.AppendFormat(@"          <div class=""bg-layer""></div>");
                    result.AppendFormat(@"          <div class=""op-layer""></div>");
                    result.AppendFormat(@"          <div class=""panel"">");

                    if (!string.IsNullOrWhiteSpace(HttpUtility.HtmlDecode(page.LeadTitle)))
                        result.AppendFormat(@"              <h3 class=""reading-txt"">{0}</h3>", page.LeadTitle);

                    result.AppendFormat(@"              <ul class=""story-list"">");

                    foreach (var description in page.Description)
                    {
                        result.AppendFormat(@"                  <li><span class=""box"">{0}</span></li>", description);
                    }

                    result.AppendFormat(@"                  <li class=""items gallery"">");
                    result.AppendFormat(@"                      <!-- data-img : image src array -->");
                    result.AppendFormat(@"                      <figure class=""trg-gallery"" data-img='{0}'>", string.Format("[{0}]", string.Join(", ", particle.Images.Select(x => string.Format(@"[""{0}"",""{1}""]", x.Thumbnail, HttpContext.Current.Server.HtmlEncode(x.Caption))).ToArray())));
                    result.AppendFormat(@"                          <!-- 대표이미지 -->");
                    result.AppendFormat(@"                          <span class=""thumb"">");
                    result.AppendFormat(@"                              <span class=""ic-library""></span>");
                    result.AppendFormat(@"                              <img src=""{0}"" alt="""">", particle.Images.FirstOrDefault().Thumbnail);
                    result.AppendFormat(@"                          </span>");
                    result.AppendFormat(@"                          <figcaption>{0}</figcaption>", particle.Images.FirstOrDefault().Caption);
                    result.AppendFormat(@"                      </figure>");
                    result.AppendFormat(@"                  </li>");
                    result.AppendFormat(@"              </ul>");
                    result.AppendFormat(@"          </div>");
                    result.AppendFormat(@"      </div>");
                    result.AppendFormat(@"  </div>");
                }
                else if (page.ParticleType == ParticleTypeEnum.VIDEO)
                {
                    // 비디오
                    var particle = page.Particle as Particle_Video;

                    if (!string.IsNullOrWhiteSpace(page.BackgroundImage))
                        result.AppendFormat(@"  <div class=""page theme-image"" data-bg=""{0}"">", page.BackgroundImage);
                    else
                        result.AppendFormat(@"  <div class=""page theme-color"">");

                    result.AppendFormat(@"      <div class=""_cont"">");
                    result.AppendFormat(@"          <div class=""bg-layer""></div>");
                    result.AppendFormat(@"          <div class=""op-layer""></div>");
                    result.AppendFormat(@"          <div class=""panel"">");

                    if (!string.IsNullOrWhiteSpace(HttpUtility.HtmlDecode(page.LeadTitle)))
                        result.AppendFormat(@"              <h3 class=""reading-txt"">{0}</h3>", page.LeadTitle);

                    result.AppendFormat(@"              <ul class=""story-list"">");

                    foreach (var description in page.Description)
                    {
                        result.AppendFormat(@"                  <li><span class=""box"">{0}</span></li>", description);
                    }

                    result.AppendFormat(@"                  <li class=""items video"">");
                    result.AppendFormat(@"                      <iframe allow=""encrypted-media"" allowfullscreen="""" frameborder=""0"" gesture=""media"" src=""{0}"" style=""width:100%;height:100%;""></iframe>", particle.Url);
                    result.AppendFormat(@"                  </li>");
                    result.AppendFormat(@"              </ul>");
                    result.AppendFormat(@"          </div>");
                    result.AppendFormat(@"      </div>");
                    result.AppendFormat(@"  </div>");
                }
                else
                {
                    // 파티클 없음
                    if (!string.IsNullOrWhiteSpace(page.BackgroundImage))
                        result.AppendFormat(@"  <div class=""page theme-image"" data-bg=""{0}"">", page.BackgroundImage);
                    else
                        result.AppendFormat(@"  <div class=""page theme-color"">");

                    result.AppendFormat(@"      <div class=""_cont"">");
                    result.AppendFormat(@"          <div class=""bg-layer""></div>");
                    result.AppendFormat(@"          <div class=""op-layer""></div>");
                    result.AppendFormat(@"          <div class=""panel"">");

                    if (!string.IsNullOrWhiteSpace(HttpUtility.HtmlDecode(page.LeadTitle)))
                        result.AppendFormat(@"              <h3 class=""reading-txt"">{0}</h3>", page.LeadTitle);

                    result.AppendFormat(@"              <ul class=""story-list"">");

                    foreach (var description in page.Description)
                    {
                        result.AppendFormat(@"                  <li><span class=""box"">{0}</span></li>", description);
                    }

                    result.AppendFormat(@"                  <li class=""items emoticon""><img src=""{0}"" alt=""""></li>", page.CharacterImageUrl);
                    result.AppendFormat(@"              </ul>");
                    result.AppendFormat(@"          </div>");
                    result.AppendFormat(@"      </div>");
                    result.AppendFormat(@"  </div>");
                }
            }

            // 추천기사
            if (entity.RecommendProduct != null && entity.RecommendProduct.Count() > 0)
            {
                result.AppendFormat(@"<!-- page : 2018.08 추천기사 추가 -->");
                result.AppendFormat(@"<div class=""page summary theme-color "">");
                result.AppendFormat(@"    <div class=""_cont"">");
                result.AppendFormat(@"        <div class=""panel"">");
                result.AppendFormat(@"            <h3 class=""reading-txt"">");
                result.AppendFormat(@"                <span class=""plain"">이 이야기는 여기까지야. <br>다른</span> 스토리썰링<span class=""plain"">도</span> 읽어볼래?");
                result.AppendFormat(@"            </h3>");
                result.AppendFormat(@"            <ul class=""recommend-list"">");

                foreach (var item in entity.RecommendProduct)
                {
                    result.AppendFormat(@"                <li>");
                    result.AppendFormat(@"                    <a href=""{0}"">", item.Seq);
                    result.AppendFormat(@"                        <figure>");
                    result.AppendFormat(@"                            <div class=""thumb""><img src=""{0}"" alt=""""></div>", item.Thumbnail);
                    result.AppendFormat(@"                            <figcaption>");
                    result.AppendFormat(@"                                <p class=""subject"">{0}</p>", item.Title);
                    result.AppendFormat(@"                                <p class=""date"">{0:yyyy.MM.dd}</p>", item.ServiceDate);
                    result.AppendFormat(@"                            </figcaption>");
                    result.AppendFormat(@"                        </figure>");
                    result.AppendFormat(@"                    </a>");
                    result.AppendFormat(@"                </li>");
                }

                result.AppendFormat(@"            </ul>");
                result.AppendFormat(@"        </div>");
                result.AppendFormat(@"    </div>");
                result.AppendFormat(@"</div>");
                result.AppendFormat(@"<!--// page : 2018.08 추천기사 추가 -->");
            }

            result.AppendFormat(@"</div>");

            return Convert.ToString(result);
        }

        private ContentResult GetMessageData(Product product, string content, IEnumerable<Product> recommendProduct, bool isDecodeHtmlChar = false, bool isMobileAPI = false)
        {
            var intro = new Intro();
            var pages = new List<Page>();

            if (!string.IsNullOrWhiteSpace(content))
            {
                var htmlDoc = new HtmlAgilityPack.HtmlDocument();
                htmlDoc.LoadHtml(content.Replace("\r\n", string.Empty));

                foreach (var node in htmlDoc.DocumentNode.ChildNodes)
                {
                    try
                    {
                        var nodeType = node.NodeType;
                        var nodeName = node.Name;
                        
                        var ssully_module_type = node.GetAttributeValue("ssully_type", string.Empty);

                        if (nodeType == HtmlAgilityPack.HtmlNodeType.Element)
                        {
                            if ("br".Equals(nodeName))
                                continue;

                            if ("intro".Equals(ssully_module_type, StringComparison.OrdinalIgnoreCase))
                            {
                                var background_color = node.GetAttributeValue("ssully_background_color", null);
                                var background_image = DecodeHtmlString(true, node.GetAttributeValue("ssully_background_image", null));

                                intro.Subtitle = DecodeHtmlString(isDecodeHtmlChar, node.InnerHtml);
                                intro.BackgroundColor = background_color;
                                intro.BackgroundImage = background_image;
                            }

                            if ("page".Equals(ssully_module_type, StringComparison.OrdinalIgnoreCase))
                            {
                                var image = node.SelectSingleNode("div[contains(@class, 'image')]/img");
                                var lead = node.SelectSingleNode("div[contains(@class, 'lead_text')]");
                                var desc = node.SelectNodes("div[contains(@class, 'main_text')]");
                                var particle = node.SelectSingleNode("div[contains(@class, 'particle')]");

                                var background_color = node.GetAttributeValue("ssully_background_color", null);
                                var page = new Page { BackgroundColor = background_color };

                                if (image != null)
                                    page.CharacterImageUrl = DecodeHtmlString(true, image.GetAttributeValue("src", string.Empty));

                                if (lead != null)
                                    page.LeadTitle = DecodeHtmlString(isDecodeHtmlChar, lead.InnerHtml);

                                if (desc != null && desc.Count > 0)
                                    page.Description = desc.Select(x => DecodeHtmlString(isDecodeHtmlChar, x.InnerHtml));

                                if (particle != null)
                                {
                                    foreach(var particleNode in particle.ChildNodes)
                                    {
                                        var ssully_particle_type = particleNode.GetAttributeValue("ssully_type", string.Empty);

                                        if ("image".Equals(ssully_particle_type, StringComparison.OrdinalIgnoreCase))
                                        {
                                            var images = particleNode.SelectNodes("img");

                                            if (images != null && images.Count > 0)
                                            {
                                                page.Particle = new Particle_Image
                                                {
                                                    Images = images.Select(x => new Particle_Image.Image {
                                                        Thumbnail = DecodeHtmlString(true, x.GetAttributeValue("src", string.Empty)),
                                                        Caption = DecodeHtmlString(isDecodeHtmlChar, x.GetAttributeValue("title", string.Empty))
                                                    })
                                                };
                                                page.ParticleType = ParticleTypeEnum.IMAGE;
                                                page.BackgroundImage = images.FirstOrDefault() != null ? images.FirstOrDefault().GetAttributeValue("src", string.Empty) : null;
                                            }
                                        }
                                        else if ("link".Equals(ssully_particle_type, StringComparison.OrdinalIgnoreCase))
                                        {
                                            // 기사 링크
                                            var thumbnail = particleNode.SelectSingleNode("div[contains(@class, 'thumb')]/img");
                                            var title = particleNode.SelectSingleNode("div[contains(@class, 'title')]");
                                            var domain = particleNode.SelectSingleNode("div[contains(@class, 'domain')]");
                                            var url = particleNode.GetAttributeValue("ssully_href", string.Empty);

                                            var particle_entity = new Particle_Link
                                            {
                                                LinkUrl = DecodeHtmlString(true, url),
                                                Thumbnail = DecodeHtmlString(true, thumbnail != null ? thumbnail.GetAttributeValue("src", string.Empty) : string.Empty),
                                                Domain = domain != null ? domain.InnerHtml : string.Empty,
                                                Title = DecodeHtmlString(isDecodeHtmlChar, title.InnerHtml)
                                            };

                                            // 썰리 내부링크 여부 판단
                                            if (!string.IsNullOrWhiteSpace(particle_entity.LinkUrl))
                                            {
                                                var pattern = @"https?\:\/\/ssully.joins.com\/view\/(\d+)\?.*";

                                                if (Regex.IsMatch(particle_entity.LinkUrl, pattern, RegexOptions.IgnoreCase))
                                                {
                                                    var seq = Convert.ToInt32(Regex.Match(particle_entity.LinkUrl, pattern, RegexOptions.IgnoreCase).Groups[1].Value);
                                                    var ssully_product = ProductRepository.GetProductInfo(seq);

                                                    particle_entity.SSully_Seq = seq;

                                                    if (ssully_product != null)
                                                    {
                                                        particle_entity.SSully_Corner_Type = ssully_product.PD_TYPE;
                                                        particle_entity.SSully_IsADProduct = ssully_product.IS_AD_PRODUCT;
                                                    }
                                                }
                                            }

                                            page.Particle = particle_entity;
                                            page.ParticleType = ParticleTypeEnum.LINK;
                                        }
                                        else if ("video".Equals(ssully_particle_type, StringComparison.OrdinalIgnoreCase))
                                        {
                                            // 비디오 링크
                                            var videoSrc = DecodeHtmlString(true, particleNode.GetAttributeValue("src", string.Empty));
                                            var referrer = particleNode.GetAttributeValue("ssully_video_referrer", "YOUTUBE");
                                            
                                            if (isMobileAPI)
                                            {
                                                var uri = new Uri(videoSrc);

                                                if ("youtube".Equals(referrer, StringComparison.OrdinalIgnoreCase))
                                                {
                                                    if (!string.IsNullOrWhiteSpace(uri.Query))
                                                        videoSrc = uri.AbsoluteUri.Replace(uri.Query, string.Empty);
                                                }
                                            }

                                            page.Particle = new Particle_Video
                                            {
                                                Url = videoSrc,
                                                VideoSource = referrer.ToUpper()
                                            };
                                            page.ParticleType = ParticleTypeEnum.VIDEO;
                                        }
                                    }
                                }

                                pages.Add(page);
                            }
                        }
                    }
                    catch { }
                }
            }

            return new ContentResult
            {
                Corner = CornerType.CODE_5,
                Intro = intro,
                Pages = pages,
                RecommendProduct = recommendProduct == null ? null : recommendProduct.Take(4).Select(x => new ProductInfo
                {
                    Seq = x.PD_SEQ,
                    Category = x.PD_CATEGORY,
                    CornerType = x.PD_TYPE,
                    Thumbnail = x.PD_IMG_URL,
                    Title = x.PD_TITLE,
                    ServiceDate = Convert.ToDateTime(x.PD_SERVICE_DT),
                    IsADProduct = x.IS_AD_PRODUCT
                }),
                Product = new ProductInfo
                {
                    Seq = product.PD_SEQ,
                    Category = product.PD_CATEGORY,
                    CornerType = product.PD_TYPE,
                    Thumbnail = product.PD_IMG_URL,
                    Title = product.PD_TITLE,
                    ServiceDate = Convert.ToDateTime(product.PD_SERVICE_DT),
                    IsADProduct = product.IS_AD_PRODUCT,
                    ProviderUrl = product.PD_PROVIDER_URL,
                    LikeYN = product.LIKE_YN,
                    TotalLikeCount = product.LIKE_CNT,
                    ScrapYN = product.SCRAP_YN
                }
            };
        }
    }
}
