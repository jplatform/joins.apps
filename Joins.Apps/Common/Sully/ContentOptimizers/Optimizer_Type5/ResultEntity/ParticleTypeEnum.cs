﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type5.ResultEntity
{
    /// <summary>
    /// 메세지 형식
    /// </summary>
    public enum ParticleTypeEnum
    {
        /// <summary>
        /// 링크
        /// </summary>
        LINK,
        /// <summary>
        /// 이미지 주소
        /// </summary>
        IMAGE,
        /// <summary>
        /// 영상
        /// </summary>
        VIDEO
    }
}
