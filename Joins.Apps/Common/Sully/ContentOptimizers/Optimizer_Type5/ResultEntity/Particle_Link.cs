﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type5.ResultEntity
{
    public class Particle_Link
    {
        /// <summary>
        /// 링크 타이틀
        /// </summary>
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 링크 주소
        /// </summary>
        [JsonProperty("link_url")]
        public string LinkUrl { get; set; }
        /// <summary>
        /// 링크 썸네일
        /// </summary>
        [JsonProperty("thumbnail")]
        public string Thumbnail { get; set; }
        /// <summary>
        /// 링크 도메인
        /// </summary>
        [JsonProperty("domain")]
        public string Domain { get; set; }
        /// <summary>
        /// 썰리 내부기사 링크시 고유번호
        /// </summary>
        [JsonProperty("ssully_seq")]
        public int? SSully_Seq { get; set; }
        /// <summary>
        /// 썰리 내부기사 링크시 코너형식 코드
        /// </summary>
        [JsonProperty("ssully_corner_type")]
        public int? SSully_Corner_Type { get; set; }
        /// <summary>
        /// 썰리 내부기사 링크시 광고 컨텐트 여부
        /// </summary>
        [JsonProperty("ssully_is_ad_product")]
        public string SSully_IsADProduct { get; set; }
    }
}
