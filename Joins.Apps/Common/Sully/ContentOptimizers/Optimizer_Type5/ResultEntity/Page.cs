﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type5.ResultEntity
{
    public class Page
    {
        /// <summary>
        /// 캐릭터 프로필 이미지 주소
        /// </summary>
        [JsonProperty("character_url")]
        public string CharacterImageUrl { get; set; }
        /// <summary>
        /// 리드 텍스트
        /// </summary>
        [JsonProperty("lead_title")]
        public string LeadTitle { get; set; }
        /// <summary>
        /// 메인 텍스트
        /// </summary>
        [JsonProperty("descriptions")]
        public IEnumerable<string> Description { get; set; }
        /// <summary>
        /// 파티클 정보
        /// </summary>
        [JsonProperty("particle")]
        public object Particle { get; set; }
        /// <summary>
        /// 파티클 형식
        /// </summary>
        [JsonProperty("particle_type")]
        public ParticleTypeEnum? ParticleType { get; set; }
        /// <summary>
        /// 배경색 컬러 값
        /// </summary>
        [JsonProperty("background_color")]
        public string BackgroundColor { get; set; }
        /// <summary>
        /// 배경 이미지
        /// </summary>
        [JsonProperty("background_image")]
        public string BackgroundImage { get; set; }
    }
}
