﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type5.ResultEntity
{
    public class Particle_Video
    {
        /// <summary>
        /// 링크 주소
        /// </summary>
        [JsonProperty("url")]
        public string Url { get; set; }
        /// <summary>
        /// 동영상 출처
        /// </summary>
        [JsonProperty("video_source")]
        public string VideoSource { get; set; }
    }
}
