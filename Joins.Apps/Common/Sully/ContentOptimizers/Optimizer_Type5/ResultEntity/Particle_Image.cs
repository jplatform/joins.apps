﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type5.ResultEntity
{
    public class Particle_Image
    {
        /// <summary>
        /// 이미지 목록
        /// </summary>
        [JsonProperty("images")]
        public IEnumerable<Image> Images { get; set; }

        public class Image
        {
            /// <summary>
            /// 캡션
            /// </summary>
            [JsonProperty("caption")]
            public string Caption { get; set; }
            /// <summary>
            /// 썸네일
            /// </summary>
            [JsonProperty("thumbnail")]
            public string Thumbnail { get; set; }
        }
    }
}
