﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type5.ResultEntity
{
    public class Intro
    {
        /// <summary>
        /// 프로덕트 부제
        /// </summary>
        [JsonProperty("subtitle")]
        public string Subtitle { get; set; }
        /// <summary>
        /// 배경색 컬러 값
        /// </summary>
        [JsonProperty("background_color")]
        public string BackgroundColor { get; set; }
        /// <summary>
        /// 배경 이미지
        /// </summary>
        [JsonProperty("background_image")]
        public string BackgroundImage { get; set; }
    }
}
