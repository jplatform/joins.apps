﻿using Joins.Apps.Common.Sully.ContentOptimizers.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Joins.Apps.Models.Sully;
using Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type2.ResultEntity;
using System.Web;
using System.Text.RegularExpressions;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type2
{
    internal class Optimizer : IOptimizer
    {
        private static readonly Lazy<Optimizer> lazy = new Lazy<Optimizer>(() => new Optimizer());

        public static Optimizer Instance
        {
            get { return lazy.Value; }
        }

        private Optimizer() { }

        /// <summary>
        /// 기사내용을 최적화하여 객체로 반환합니다.
        /// </summary>
        /// <param name="product">기사정보</param>
        /// <param name="content">기사콘텐트 원본</param>
        /// <param name="recommendProduct">추천 기사</param>
        /// <param name="isDecodeHtmlChar">특수문자 HTML코드를 디코딩 할지 여부</param>
        /// <returns>정제된 객체</returns>
        public ContentResultBase GetContent(Product product, string content, IEnumerable<Product> recommendProduct, bool isDecodeHtmlChar = false, bool isMobileAPI = false)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 기사내용을 최적화하여 HTML로 반환합니다.
        /// </summary>
        /// <param name="product">기사정보</param>
        /// <param name="recommentProduct">추천 기사</param>
        /// <param name="content">기사콘텐트 원본</param>
        /// <returns>정제된 HTML코드</returns>
        public string GetContentHtml(Product product, IEnumerable<Product> recommentProduct, string content)
        {
            throw new NotImplementedException();
        }

        private ContentResult GetMessageData(Product product, string content, IEnumerable<Product> recommendProduct, bool isDecodeHtmlChar = false, bool isMobileAPI = false)
        {
            var message = new List<Message>();

            //if (!string.IsNullOrEmpty(content))
            //{
            //    var htmlDoc = new HtmlAgilityPack.HtmlDocument();
            //    htmlDoc.LoadHtml(content.Replace("\r\n", string.Empty));

            //    foreach (var node in htmlDoc.DocumentNode.ChildNodes)
            //    {
            //        try
            //        {
            //            var nodeType = node.NodeType;
            //            var nodeName = node.Name;

            //            var ssully_module_type = node.GetAttributeValue("ssully_type", string.Empty);

            //            if (nodeType == HtmlAgilityPack.HtmlNodeType.Element)
            //            {
            //                if ("br".Equals(nodeName))
            //                    continue;

            //                if ("image_type4".Equals(ssully_module_type, StringComparison.OrdinalIgnoreCase))
            //                {
            //                    var thumbnail = node.SelectSingleNode("div[contains(@class, 'image')]/img");
            //                    var caption = node.SelectSingleNode("div[contains(@class, 'caption')]");

            //                    var thumbnail_value = thumbnail != null ? thumbnail.GetAttributeValue("src", string.Empty) : string.Empty;
            //                    var caption_value = caption != null ? (isDecodeHtmlChar ? HttpContext.Current.Server.HtmlDecode((caption.InnerHtml ?? string.Empty)) : (caption.InnerHtml ?? string.Empty)).Trim() : string.Empty;

            //                    if (!string.IsNullOrEmpty(caption_value))
            //                        caption_value = Regex.Replace(caption_value, @"<br\s*/?>", Environment.NewLine, RegexOptions.IgnoreCase);

            //                    message.Add(new Message { Type = MessageTypeEnum.IMAGE, Thumbnail = thumbnail_value, Caption = caption_value });
            //                }
            //            }
            //        }
            //        catch { }
            //    }
            //}

            return new ContentResult
            {
                Corner = CornerType.CODE_4,
                Messages = message,
                RecommendProduct = recommendProduct == null ? null : recommendProduct.Select(x => new ProductInfo
                {
                    Seq = x.PD_SEQ,
                    Category = x.PD_CATEGORY,
                    CornerType = x.PD_TYPE,
                    Thumbnail = x.PD_IMG_URL,
                    Title = x.PD_TITLE,
                    ServiceDate = Convert.ToDateTime(x.PD_SERVICE_DT)
                }),
                Product = new ProductInfo
                {
                    Seq = product.PD_SEQ,
                    Category = product.PD_CATEGORY,
                    CornerType = product.PD_TYPE,
                    Thumbnail = product.PD_IMG_URL,
                    Title = product.PD_TITLE,
                    ServiceDate = Convert.ToDateTime(product.PD_SERVICE_DT)
                }
            };
        }
    }
}
