﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type2.ResultEntity
{
    public class Message
    {
        /// <summary>
        /// 질문
        /// </summary>
        [JsonProperty("question")]
        public string QuestionText { get; set; }
        /// <summary>
        /// 프로필 이미지
        /// </summary>
        [JsonProperty("profile_thumbnail_url")]
        public string ProfileThumbnailUrl { get; set; }
        /// <summary>
        /// 설명
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 파티클
        /// </summary>
        [JsonProperty("particles")]
        public IEnumerable<MessageParticle> Particles { get; set; }
    }
}
