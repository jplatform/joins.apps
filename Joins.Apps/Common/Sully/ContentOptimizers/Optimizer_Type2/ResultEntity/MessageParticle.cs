﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type2.ResultEntity
{
    public class MessageParticle
    {
        /// <summary>
        /// 파티클 형식
        /// </summary>
        [JsonProperty("type")]
        public MessageTypeEnum Type { get; set; }
        /// <summary>
        /// 주소
        /// </summary>
        [JsonProperty("url")]
        public string Url { get; set; }
        /// <summary>
        /// 프로덕트 내부링크 고유번호
        /// </summary>
        [JsonProperty("ssully_seq")]
        public int? SSullySeq { get; set; }
        /// <summary>
        /// 프로덕트 내부링크 코너형식
        /// </summary>
        [JsonProperty("ssully_corner_type")]
        public int? SSullyCornerType { get; set; }
    }
}
