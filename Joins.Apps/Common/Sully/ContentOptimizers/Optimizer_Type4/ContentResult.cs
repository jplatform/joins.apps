﻿using Joins.Apps.Common.Sully.ContentOptimizers.Common;
using Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type4.ResultEntity;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type4
{
    /// <summary>
    /// 기사 데이터
    /// </summary>
    public class ContentResult : ContentResultBase
    {
        /// <summary>
        /// 데이터
        /// </summary>
        [JsonProperty("messages")]
        public List<Message> Messages { get; set; }
    }
}
