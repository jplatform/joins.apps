﻿using Joins.Apps.Common.Sully.ContentOptimizers.Common;
using Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type4.ResultEntity;
using Joins.Apps.Models.Sully;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type4
{
    internal class Optimizer : BaseOptimizer, IOptimizer
    {
        private static readonly Lazy<Optimizer> lazy = new Lazy<Optimizer>(() => new Optimizer());

        public static Optimizer Instance
        {
            get { return lazy.Value; }
        }

        private Optimizer() { }

        /// <summary>
        /// 기사내용을 최적화하여 객체로 반환합니다.
        /// </summary>
        /// <param name="product">기사정보</param>
        /// <param name="content">기사콘텐트 원본</param>
        /// <param name="recommendProduct">추천 기사</param>
        /// <param name="isDecodeHtmlChar">특수문자 HTML코드를 디코딩 할지 여부</param>
        /// <param name="isMobileAPI">모바일 API에서 호출하는지 여부</param>
        /// <returns>정제된 객체</returns>
        public ContentResultBase GetContent(Product product, string content, IEnumerable<Product> recommendProduct, bool isDecodeHtmlChar, bool isMobileAPI = false)
        {
            return GetMessageData(product, content, recommendProduct, isDecodeHtmlChar, isMobileAPI);
        }

        /// <summary>
        /// 기사내용을 최적화하여 HTML로 반환합니다.
        /// </summary>
        /// <param name="product">기사정보</param>
        /// <param name="recommentProduct">추천 기사</param>
        /// <param name="content">기사콘텐트 원본</param>
        /// <returns>정제된 HTML코드</returns>
        public string GetContentHtml(Product product, IEnumerable<Product> recommendProduct, string content)
        {
            var result = new StringBuilder();
            var entity = GetMessageData(product, content, recommendProduct);

            // 컨텐츠
            result.Append(@"    <ul class=""onecut-view"">");

            foreach (var item in entity.Messages)
            {
                result.AppendFormat(@"        <li>");
                result.AppendFormat(@"            <figure>");
                result.AppendFormat(@"                <div class=""item trg-layer"">");
                result.AppendFormat(@"                    <img src=""{0}"" alt="""" title=""{1}"">", item.Thumbnail, item.Caption);
                result.AppendFormat(@"                    <i class=""expand""></i>");
                result.AppendFormat(@"                </div>");

                if (!string.IsNullOrWhiteSpace(item.Caption) && !string.IsNullOrWhiteSpace(HttpContext.Current.Server.HtmlDecode(item.Caption).Trim()))
                {
                    result.AppendFormat(@"                <figcaption>");
                    result.AppendFormat(@"                    <span class=""caption"">{0}</span>", item.Caption.Replace(Environment.NewLine, "<br/>"));
                    result.AppendFormat(@"                    <span class=""tag""></span>");
                    result.AppendFormat(@"                </figcaption>");
                }

                result.AppendFormat(@"            </figure>");
                result.AppendFormat(@"        </li>");
            }

            result.Append(@"    </ul>");

            return Convert.ToString(result);
        }

        private ContentResult GetMessageData(Product product, string content, IEnumerable<Product> recommendProduct, bool isDecodeHtmlChar = false, bool isMobileAPI = false)
        {
            var message = new List<Message>();

            if (!string.IsNullOrWhiteSpace(content))
            {
                var htmlDoc = new HtmlAgilityPack.HtmlDocument();
                htmlDoc.LoadHtml(content.Replace("\r\n", string.Empty));

                foreach (var node in htmlDoc.DocumentNode.ChildNodes)
                {
                    try
                    {
                        var nodeType = node.NodeType;
                        var nodeName = node.Name;
                        
                        var ssully_module_type = node.GetAttributeValue("ssully_type", string.Empty);

                        if (nodeType == HtmlAgilityPack.HtmlNodeType.Element)
                        {
                            if ("br".Equals(nodeName))
                                continue;

                            if ("image_type4".Equals(ssully_module_type, StringComparison.OrdinalIgnoreCase))
                            {
                                var thumbnail = node.SelectSingleNode("div[contains(@class, 'image')]/img");
                                var caption = node.SelectSingleNode("div[contains(@class, 'caption')]");

                                var thumbnail_value = DecodeHtmlString(true, thumbnail != null ? thumbnail.GetAttributeValue("src", string.Empty) : string.Empty);
                                var caption_value = caption != null ? DecodeHtmlString(isDecodeHtmlChar, caption.InnerHtml ?? string.Empty).Trim() : string.Empty;
                                
                                message.Add(new Message { Type = MessageTypeEnum.IMAGE, Thumbnail = thumbnail_value, Caption = DecodeHtmlString(isDecodeHtmlChar, caption_value) });
                            }
                        }
                    }
                    catch { }
                }
            }

            return new ContentResult
            {
                Corner = CornerType.CODE_4,
                Messages = message,
                RecommendProduct = recommendProduct == null ? null : recommendProduct.Take(3).Select(x => new ProductInfo
                {
                    Seq = x.PD_SEQ,
                    Category = x.PD_CATEGORY,
                    CornerType = x.PD_TYPE,
                    Thumbnail = x.PD_IMG_URL,
                    Title = x.PD_TITLE,
                    ServiceDate = Convert.ToDateTime(x.PD_SERVICE_DT),
                    IsADProduct = x.IS_AD_PRODUCT
                }),
                Product = new ProductInfo
                {
                    Seq = product.PD_SEQ,
                    Category = product.PD_CATEGORY,
                    CornerType = product.PD_TYPE,
                    Thumbnail = product.PD_IMG_URL,
                    Title = product.PD_TITLE,
                    ServiceDate = Convert.ToDateTime(product.PD_SERVICE_DT),
                    IsADProduct = product.IS_AD_PRODUCT,
                    ProviderUrl = product.PD_PROVIDER_URL,
                    LikeYN = product.LIKE_YN,
                    TotalLikeCount = product.LIKE_CNT,
                    ScrapYN = product.SCRAP_YN
                }
            };
        }
    }
}
