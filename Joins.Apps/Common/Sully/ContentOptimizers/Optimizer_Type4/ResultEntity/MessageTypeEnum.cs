﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type4.ResultEntity
{
    /// <summary>
    /// 메세지 형식
    /// </summary>
    public enum MessageTypeEnum
    {
        /// <summary>
        /// 이미지 주소
        /// </summary>
        IMAGE
    }
}
