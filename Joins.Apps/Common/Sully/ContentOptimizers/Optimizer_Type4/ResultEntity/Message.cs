﻿using Joins.Apps.Common.JsonConverter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully.ContentOptimizers.Optimizer_Type4.ResultEntity
{
    /// <summary>
    /// 메세지
    /// </summary>
    public class Message
    {
        /// <summary>
        /// 설명문구
        /// </summary>
        [JsonProperty("caption")]
        public string Caption { get; set; }
        /// <summary>
        /// 링크 썸네일
        /// </summary>
        [JsonProperty("thumbnail")]
        public string Thumbnail { get; set; }
        /// <summary>
        /// 메세지 타입
        /// </summary>
        [JsonProperty("type")]
        public MessageTypeEnum Type { get; set; }
    }
}
