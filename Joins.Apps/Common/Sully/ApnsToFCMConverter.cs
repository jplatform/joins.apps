﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Common.Sully
{
    public class ApnsToFCMConverter
    {
        public static string ConvertAPNSToFCM(string apns) {
            var url = "https://iid.googleapis.com/iid/v1:batchImport";
            var ios_application = "com.joins.ssully";
            var firebase_authorization = "AAAAujQP2fw:APA91bFktdKJp44L23ijaJVyTtceZeSztSvNR5-qJ-2J7gg-OszTjv92HC2LtIJDjiKjSLp6id5eUhkYHamIzIvMnId24y1ab6m_xezz_mizVx2t6CoBn4RyDnb0AVJhB-Ce7BAdDY-0";

            try
            {
                using (var client = new HttpClient())
                {
                    var param = new HttpRequestMessage
                    {
                        Method = HttpMethod.Post,
                        RequestUri = new Uri(url),
                        Version = HttpVersion.Version11,
                        Content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(new
                        {
                            application = ios_application,
                            sandbox = false,
                            apns_tokens = new[] { apns }
                        }), Encoding.UTF8, "application/json")
                    };
                    param.Headers.TryAddWithoutValidation("Authorization", string.Format("key={0}", firebase_authorization));

                    var result = client.SendAsync(param).GetAwaiter().GetResult().Content.ReadAsStringAsync().GetAwaiter().GetResult();
                    var json = JsonConvert.DeserializeAnonymousType(result, new
                    {
                        results = new[] {
                            new { apns_token = string.Empty, status = string.Empty, registration_token = string.Empty }
                        }
                    });

                    if (json.results != null && json.results.FirstOrDefault() != null && "ok".Equals(json.results.FirstOrDefault().status, StringComparison.OrdinalIgnoreCase))
                        return json.results.FirstOrDefault().registration_token;
                }
            }
            catch (Exception ex) {
                Logger.LogWriteGroup("SSully FCM", ex.ToString());
            }

            return null;
        }
    }
}
