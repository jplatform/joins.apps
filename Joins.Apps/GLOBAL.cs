﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps {
    public class GLOBAL {
        #region [DB연결]
        /// <summary>Joins Store DB [영역별로 사용후 연결종료]</summary>
        public static JCube.AF.DAO.DataHelper GetDataHelper(){
            System.Configuration.ConnectionStringSettings cs = (System.Configuration.ConfigurationManager.ConnectionStrings["TotalDB_R"]);
            if (cs != null) { return new JCube.AF.DAO.DataHelper(JCube.AF.Module.Security.DESDecrypt(cs.ConnectionString, "s23ewr23rerw1")); }
            return null;
        }
        public static JCube.AF.DAO.DataHelper GetSmartBriefDataHelper() {
            System.Configuration.ConnectionStringSettings cs = (System.Configuration.ConfigurationManager.ConnectionStrings["SmartBreiefDB"]);
            if (cs != null) { return new JCube.AF.DAO.DataHelper(JCube.AF.Module.Security.DESDecrypt(cs.ConnectionString, "s23ewr23rerw1")); }
            return null;
        }
        public static JCube.AF.DAO.DataHelper GetJoinsNewsDataHelper() {
            string aaa = JCube.AF.Module.Security.DESEncrypt("server=222.122.208.107;uid=USER_JCUBE_DEV;pwd=tksoroqkfTjqj~!;database=DB_SSULLY;Pooling=false;Connection Lifetime=240", "s23ewr23rerw1");
            System.Configuration.ConnectionStringSettings cs = (System.Configuration.ConfigurationManager.ConnectionStrings["JoinsSSullyDB"]);
            if (cs != null) { return new JCube.AF.DAO.DataHelper(JCube.AF.Module.Security.DESDecrypt(cs.ConnectionString, "s23ewr23rerw1")); }
            return null;
        }
        #endregion
        public static string LogPath { get { return System.Web.Configuration.WebConfigurationManager.AppSettings["LogPath"]; } }
        public static string JAMLoginAPIURL { get { return System.Web.Configuration.WebConfigurationManager.AppSettings["JAMLoginAPIURL"]; } }
        public static string JAMUserInfoAPIURL { get { return System.Web.Configuration.WebConfigurationManager.AppSettings["JAMUserInfoAPIURL"]; } }
        public static string JAMLogoutAPIURL { get { return System.Web.Configuration.WebConfigurationManager.AppSettings["JAMLogoutAPIURL"]; } }
        public static string JoongangMailServer { get { return System.Web.Configuration.WebConfigurationManager.AppSettings["JoongangMailServer"]; } }

        public static string News10JtbcYoutubeListAPI { get { return System.Web.Configuration.WebConfigurationManager.AppSettings["News10JtbcYoutubeListAPI"]; } }
        public static string News10JtbcYoutubeVideoAPI { get { return System.Web.Configuration.WebConfigurationManager.AppSettings["News10JtbcYoutubeVideoAPI"]; } }

        public static string News10AirKoreaAreaAPI { get { return System.Web.Configuration.WebConfigurationManager.AppSettings["News10AirKoreaAreaAPI"]; } }
        public static string News10AirKoreaForecastAPI { get { return System.Web.Configuration.WebConfigurationManager.AppSettings["News10AirKoreaForecastAPI"]; } }
        
#if DEBUG
        public const string FIND_DOMAIN = "http://searchapi.joins.com/";
        public const string FIND_AUTOCOMPLETE_DOMAIN = "http://dev.searchapi.joins.com/";
        public const string FIND_POPULAR_KEYWORD_DOMAIN = "http://dev.searchapi.joins.com/";
        public const string FIND_RELATION_KEYWORD_DOMAIN = "http://dev.searchapi.joins.com/";
#else
        public const string FIND_DOMAIN = "http://searchapi.joins.com/";
        public const string FIND_AUTOCOMPLETE_DOMAIN = "http://searchapi.joins.com/";
        public const string FIND_POPULAR_KEYWORD_DOMAIN = "http://searchapi.joins.com/";
        public const string FIND_RELATION_KEYWORD_DOMAIN = "http://searchapi.joins.com/";
#endif

    }
}
