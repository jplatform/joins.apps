﻿using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Common;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Joins.Apps.Repository.Sully
{
    public class ScrapRepository
    {
        /// <summary>
        /// 스크랩 목록을 검색합니다.
        /// </summary>
        /// <param name="login_type"></param>
        /// <param name="uid"></param>
        /// <param name="page"></param>
        /// <param name="page_size"></param>
        /// <param name="total_count"></param>
        /// <param name="total_page"></param>
        /// <returns></returns>
        public static IEnumerable<ProductScrap> GetScrapList(string login_type, string uid, int page, int page_size, out int total_count, out int total_page)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_USR_GetProductScrapList", new[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@PAGE", Value = page },
                    new SqlParameter { ParameterName = "@PAGE_SIZE", Value = page_size },
                    new SqlParameter { ParameterName = "@TOTAL_COUNT", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output },
                    new SqlParameter { ParameterName = "@TOTAL_PAGE", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output }
                });

                total_count = db.GetOutputParameterValue("@TOTAL_COUNT", 0);
                total_page = db.GetOutputParameterValue("@TOTAL_PAGE", 0);

                if (ds == null)
                    return null;

                return db.ConvertEntity<ProductScrap>(ds.Tables[0]);
            }
        }
    }
}
