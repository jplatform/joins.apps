﻿using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Joins.Apps.Repository.Sully
{
    public class SSullyMainRepository
    {
        #region 쇼케이스

        /// <summary>
        /// 메인 쇼케이스를 등록합니다.
        /// </summary>
        /// <param name="xml"></param>
        public static bool SetCMSShowcase(string xml)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_CMS_SetMainShowcase", new[] {
                    new SqlParameter { ParameterName = "@XML", Value = xml  }
                });
            }

            return true;
        }

        /// <summary>
        /// CMS 메인 쇼케이스 정보를 검색합니다.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<SSullyMainShowcaseCMS> GetCMSShowcase()
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_CMS_GetMainShowcase");

                if (ds == null)
                    return null;

                return db.ConvertEntity<SSullyMainShowcaseCMS>(ds.Tables[0]);
            }
        }

        /// <summary>
        /// 메인 쇼케이스 정보를 검색합니다.
        /// </summary>
        /// <param name="is_debug">디버깅 여부</param>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <returns></returns>
        public static IEnumerable<SSullyMainShowcase> GetServiceShowcase(bool is_debug, string login_type, string uid)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_USR_GetMainShowcase", new[] {
                    new SqlParameter { ParameterName = "@IS_DEBUG", Value = is_debug ? "Y" : "N" },
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid }
                });

                if (ds == null)
                    return null;

                return db.ConvertEntity<SSullyMainShowcase>(ds.Tables[0]);
            }
        }

        #endregion

        /// <summary>
        /// 프로덕트를 검색합니다.
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        public static IEnumerable<ShowcaseSearchProduct> SearchProduct(string title)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_CMS_GetSearchProduct", new[] {
                    new SqlParameter { ParameterName = "@VALUE", Value = title }
                });

                if (ds == null)
                    return null;

                return db.ConvertEntity<ShowcaseSearchProduct>(ds.Tables[0]);
            }
        }
        /// <summary>
        /// 공지사항을 검색합니다.
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        public static IEnumerable<ShowcaseSearchNotice> SearchNotice(string title)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_CMS_GetSearchNotice", new[] {
                    new SqlParameter { ParameterName = "@VALUE", Value = title }
                });

                if (ds == null)
                    return null;

                return db.ConvertEntity<ShowcaseSearchNotice>(ds.Tables[0]);
            }
        }


        #region 레이아웃

        /// <summary>
        /// 메인 레이아웃을 저장합니다.
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static bool SetCMSMainLayout(string xml)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_CMS_SetMainLayout", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@XML", Value = xml }
                });
            }

            return true;
        }
        /// <summary>
        /// 메인 레이아웃을 가져옵니다.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<SSullyMainLayoutCMS> GetCMSMainLayout()
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_CMS_GetMainLayout");

                if (ds == null)
                    return null;

                return db.ConvertEntity<SSullyMainLayoutCMS>(ds.Tables[0]);
            }
        }

        /// <summary>
        /// 메인 레이아웃 배너를 등록합니다.
        /// </summary>
        /// <param name="pd_seq">링크 프로덕트 번호</param>
        /// <param name="background_color">배경색</param>
        /// <param name="image_url">이미지 주소</param>
        /// <param name="reservation_datetime">노출 예약일시</param>
        /// <returns></returns>
        public static bool SetCMSMainLayoutBanner(int pd_seq, string background_color, string image_url, DateTime reservation_start_datetime, DateTime reservation_end_datetime)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_CMS_SetMainLayoutBanner", new[] {
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = pd_seq },
                    new SqlParameter { ParameterName = "@BACKGROUND_COLOR", Value = background_color },
                    new SqlParameter { ParameterName = "@IMAGE_URL", Value = image_url },
                    new SqlParameter { ParameterName = "@RESERVATION_START_DT", Value = reservation_start_datetime },
                    new SqlParameter { ParameterName = "@RESERVATION_END_DT", Value = reservation_end_datetime }
                });

                return true;
            }
        }
        /// <summary>
        /// 등록한 배너목록을 검색합니다.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<SSullyMainLayoutBannerCMS> GetCMSMainLayoutBannerList(int page, int page_size, out int total_count, out int total_page)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_CMS_GetMainLayoutBanner", new[] {
                    new SqlParameter { ParameterName = "@PAGE", Value = page },
                    new SqlParameter { ParameterName = "@PAGE_SIZE", Value = page_size },
                    new SqlParameter { ParameterName = "@TOTAL_COUNT", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output },
                    new SqlParameter { ParameterName = "@TOTAL_PAGE", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output }
                });

                total_count = db.GetOutputParameterValue("@TOTAL_COUNT", 0);
                total_page = db.GetOutputParameterValue("@TOTAL_PAGE", 0);

                if (ds == null)
                    return null;

                return db.ConvertEntity<SSullyMainLayoutBannerCMS>(ds.Tables[0]);
            }
        }
        /// <summary>
        /// 등록한 배너를 삭제합니다.
        /// </summary>
        /// <param name="pd_seq"></param>
        /// <param name="image_url"></param>
        /// <returns></returns>
        public static bool RemoveCMSMainLayoutBanner(int pd_seq, string image_url)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_CMS_RemoveMainLayoutBanner", new[] {
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = pd_seq },
                    new SqlParameter { ParameterName = "@IMAGE_URL", Value = image_url }
                });

                return true;
            }
        }



        /// <summary>
        /// 레이아웃 정보를 가져옵니다.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<SSullyMainLayout> GetMainLayout()
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_USR_GetMainLayout");

                if (ds == null)
                    return null;

                return db.ConvertEntity<SSullyMainLayout>(ds.Tables[0]);
            }
        }

        #endregion

        /// <summary>
        /// 위클리 랭킹 정보를 검색합니다.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<SSullyMainWeeklyRank> GetWeeklyRank()
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_CMT_GetMainWeeklyRank_v2");

                if (ds == null)
                    return null;

                return db.ConvertEntity<SSullyMainWeeklyRank>(ds.Tables[0]);
            }
        }
    }
}