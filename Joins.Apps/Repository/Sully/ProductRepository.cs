﻿using Joins.Apps.Common.Sully;
using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Joins.Apps.Repository.Sully
{
    public class ProductRepository
    {
        #region CMS Repository

        public static ProductPushInfo GetProductPushInfo(int pd_seq)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_CMT_GetProductPushInfo", new[] {
                    new SqlParameter {ParameterName = "@PD_SEQ", Value = pd_seq }
                });

                if (ds == null)
                    return null;

                var result = db.ConvertEntity<ProductPushInfo>(ds.Tables[0].Rows[0]);
                result.CategoryList = db.ConvertEntity<ProductPushInfo.ProductPushCategory>(ds.Tables[1]);

                return result;
            }
        }

        public static void SetCMSCornerListHeader(int pd_type, string link_type, string link_value, string image_url)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_SetCMSProductCornerHeader", new[] {
                    new SqlParameter { ParameterName = "@PD_TYPE", Value = pd_type },
                    new SqlParameter { ParameterName = "@LINK_TYPE", Value = link_type },
                    new SqlParameter { ParameterName = "@LINK_VALUE", Value = link_value },
                    new SqlParameter { ParameterName = "@IMG_URL", Value = image_url }
                });
            }
        }

        public static IEnumerable<Product> GetCMSProductList(ProductListParam param, string editor, out int tot_cnt)
        {
            List<Product> rlist = new List<Product>();
            JCube.AF.DAO.DataHelper helper = GLOBAL.GetJoinsNewsDataHelper();
            helper.ProcParamClear();
            helper.ProcParamAdd("@PageIndex", param.pgi);
            helper.ProcParamAdd("@PageSize", param.ps);
            helper.ProcParamAdd("@PD_TYPE", param.ptype);
            helper.ProcParamAdd("@PD_CATEGORY", param.pcate);
            helper.ProcParamAdd("@PD_TAG", param.ptag);
            helper.ProcParamAdd("@PD_TITLE", param.ptitle);
            helper.ProcParamAdd("@PD_STATUS", param.pst);
            helper.ProcParamAdd("@SERVICE_SDT", param.s_sdt);
            helper.ProcParamAdd("@SERVICE_EDT", param.s_edt);
            helper.ProcParamAdd("@REG_SDT", param.r_sdt);
            helper.ProcParamAdd("@REG_EDT", param.r_edt);
            helper.ProcParamAdd("@EDITOR", editor);
            DataSet ds = helper.ProcExecuteDataSet("USP_PD_GetProductList");
            helper.Close();
            tot_cnt = 0;
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null)
            {
                int cnt = ds.Tables[0].Rows.Count;
                for (int tmpi = 0; tmpi < cnt; tmpi++)
                {
                    DataRow dr = ds.Tables[0].Rows[tmpi];
                    tot_cnt = JCube.AF.Util.Converts.ToInt32(dr["T_COUNT"], 0);
                    rlist.Add(new Models.Sully.Product
                    {
                        ROWNUM = JCube.AF.Util.Converts.ToInt32(dr["ROWNUM"], 0),

                        PD_SEQ = JCube.AF.Util.Converts.ToInt32(dr["PD_SEQ"], 0),
                        PD_USED_YN = JCube.AF.Util.Converts.ToString(dr["PD_USED_YN"], ""),
                        PD_TYPE = JCube.AF.Util.Converts.ToInt32(dr["PD_TYPE"], 1),
                        PD_CATEGORY = JCube.AF.Util.Converts.ToString(dr["PD_CATEGORY"], ""),
                        CATE_NM = JCube.AF.Util.Converts.ToString(dr["CATE_NM"], ""),
                        CORNER_NM = JCube.AF.Util.Converts.ToString(dr["CORNER_NM"], ""),
                        PD_TAG = JCube.AF.Util.Converts.ToString(dr["PD_TAG"], ""),
                        PD_TITLE = JCube.AF.Util.Converts.ToString(dr["PD_TITLE"], ""),
                        PD_IMG_URL = JCube.AF.Util.Converts.ToString(dr["PD_IMG_URL"], ""),
                        PD_SERVICE_STATUS = JCube.AF.Util.Converts.ToString(dr["PD_SERVICE_STATUS"], ""),
                        PD_SERVICE_DT = JCube.AF.Util.Converts.ToString(dr["PD_SERVICE_DT"], ""),
                        PD_EDITOR_NM = JCube.AF.Util.Converts.ToString(dr["PD_EDITOR_NM"], ""),
                        PD_REG_ID = JCube.AF.Util.Converts.ToString(dr["PD_REG_ID"], ""),
                        PD_REG_DT = JCube.AF.Util.Converts.ToString(dr["PD_REG_DT"], ""),
                        PD_MOD_ID = JCube.AF.Util.Converts.ToString(dr["PD_MOD_ID"], ""),
                        PD_MOD_DT = JCube.AF.Util.Converts.ToString(dr["PD_MOD_DT"], ""),
                        PDC_SEQ = JCube.AF.Util.Converts.ToInt32(dr["PDC_SEQ"], 0),
                        PD_PROVIDER_URL = JCube.AF.Util.Converts.ToString(dr["PD_PROVIDER_URL"], ""),
                        QUIZ_EVENT_STATUS = Convert.ToInt32(dr["QUIZ_EVENT_STATUS"])
                    });
                }
            }
            return rlist;
        }

        public static bool SetCMSProductStatus(int pseq, string pd_status, string servicetime, string mid)
        {
            JCube.AF.DAO.DataHelper helper = GLOBAL.GetJoinsNewsDataHelper();
            helper.ProcParamClear();
            helper.ProcParamAdd("@PD_SEQ", pseq);
            helper.ProcParamAdd("@PD_SERVICE_STATUS", pd_status);
            helper.ProcParamAdd("@PD_SERVICE_DT", servicetime);
            helper.ProcParamAdd("@EDITOR", mid);
            bool r = helper.ProcExecuteNonQuery("USP_PD_SetProductStatus") > 0;
            helper.Close();
            return r;
        }

        public static int SetCMSProductEdit(ProductParam param, string mid)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                return db.ProcExecuteScalar("USP_PD_SetProuctEdit", new[] {
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = param.PD_SEQ },
                    new SqlParameter { ParameterName = "@PD_TYPE", Value = param.PD_TYPE },
                    new SqlParameter { ParameterName = "@PD_CATEGORY", Value = param.PD_CATEGORY },
                    new SqlParameter { ParameterName = "@PD_TITLE", Value = param.PD_TITLE },
                    new SqlParameter { ParameterName = "@PD_TAG", Value = param.PD_TAG },
                    new SqlParameter { ParameterName = "@PD_IMG_URL", Value = param.PD_IMG_URL },
                    new SqlParameter { ParameterName = "@PD_SHARE_IMG_URL", Value = param.PD_SHARE_IMG_URL },
                    new SqlParameter { ParameterName = "@PD_SHARE_HONEYSCREEN_IMG_URL", Value = param.PD_SHARE_HONEYSCREEN_IMG_URL },
                    new SqlParameter { ParameterName = "@PD_EDITOR_NM", Value = param.PD_EDITOR_NM },
                    new SqlParameter { ParameterName = "@PD_PROVIDER", Value = param.PD_PROVIDER },
                    new SqlParameter { ParameterName = "@PD_PROVIDER_URL", Value = param.PD_PROVIDER_URL },
                    new SqlParameter { ParameterName = "@CR_SEQ_01", Value = param.CR_SEQ_01 },
                    new SqlParameter { ParameterName = "@CR_SEQ_02", Value = param.CR_SEQ_02 },
                    new SqlParameter { ParameterName = "@CR_NAME_01", Value = param.CR_NAME_01 },
                    new SqlParameter { ParameterName = "@CR_NAME_02", Value = param.CR_NAME_02 },
                    new SqlParameter { ParameterName = "@CR_IMG_01", Value = param.CR_IMG_01 },
                    new SqlParameter { ParameterName = "@CR_IMG_02", Value = param.CR_IMG_02 },
                    new SqlParameter { ParameterName = "@EDITOR", Value = mid },
                    new SqlParameter { ParameterName = "@IS_AD_PRODUCT", Value = param.IS_AD_PRODUCT },
                    new SqlParameter { ParameterName = "@PD_SHARE_DESCRIPTION", Value = param.PD_SHARE_DESCRIPTION },
                    new SqlParameter { ParameterName = "@QUIZ_EVENT_YN", Value = param.QUIZ_EVENT_YN },
                    new SqlParameter { ParameterName = "@QUIZ_EVENT_START_DT", Value = param.QUIZ_EVENT_RANGE_START_DATE},
                    new SqlParameter { ParameterName = "@QUIZ_EVENT_END_DT", Value = param.QUIZ_EVENT_RANGE_END_DATE },
                    new SqlParameter { ParameterName = "@QUIZ_EVENT_NOTICE_DT", Value = param.QUIZ_EVENT_NOTICE_DATE },
                    new SqlParameter { ParameterName = "@QUIZ_EVENT_POPUP_IMG_URL", Value = param.QUIZ_EVENT_POPUP_IMAGE_URL },
                    new SqlParameter { ParameterName = "@QUIZ_EVENT_PRIZE_IMG_URL", Value = param.QUIZ_EVENT_PRIZE_IMAGE_URL },
                    new SqlParameter { ParameterName = "@QUIZ_EVENT_INFO_DESC", Value = param.QUIZ_EVENT_INFO_DESC },

                    new SqlParameter { ParameterName = "@PUSH_YN", Value = param.PUSH_YN },
                    new SqlParameter { ParameterName = "@PUSH_TITLE", Value = param.PUSH_TITLE },
                    new SqlParameter { ParameterName = "@PUSH_CONTENT", Value = param.PUSH_CONTENT },
                    new SqlParameter { ParameterName = "@PUSH_CATEGORY_XML", Value = param.PUSH_CATEGORY_XML },
                }, -1);
            }
        }

        public static ProductContent GetCMSProductContentInfo(int pseq, string mid)
        {
            ProductContent result = new ProductContent();
            JCube.AF.DAO.DataHelper helper = GLOBAL.GetJoinsNewsDataHelper();
            helper.ProcParamClear();
            helper.ProcParamAdd("@PD_SEQ", pseq);
            helper.ProcParamAdd("@REG_ID", mid);
            DataSet ds = helper.ProcExecuteDataSet("USP_PD_GetProductContentInfo");
            helper.Close();
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null)
            {
                DataRow dr = ds.Tables[0].Rows[0];
                result.PDC_SEQ = JCube.AF.Util.Converts.ToInt32(dr["PDC_SEQ"], 0);
                result.PD_SEQ = JCube.AF.Util.Converts.ToInt32(dr["PD_SEQ"], 0);
                result.PD_TITLE = JCube.AF.Util.Converts.ToString(dr["PD_TITLE"], "");
                result.PD_SERVICE_DT = JCube.AF.Util.Converts.ToString(dr["PD_SERVICE_DT"], "");
                result.PD_SERVICE_STATUS = JCube.AF.Util.Converts.ToString(dr["PD_SERVICE_STATUS"], "");
                result.PDC_ORG_CONTENT = JCube.AF.Util.Converts.ToString(dr["PDC_ORG_CONTENT"], "");
                result.PDC_CONTENT = JCube.AF.Util.Converts.ToString(dr["PDC_CONTENT"], "");
                result.REG_ID = JCube.AF.Util.Converts.ToString(dr["REG_ID"], "");
                result.REG_DT = JCube.AF.Util.Converts.ToString(dr["REG_DT"], "");
                result.MOD_ID = JCube.AF.Util.Converts.ToString(dr["MOD_ID"], "");
                result.MOD_DT = JCube.AF.Util.Converts.ToString(dr["MOD_DT"], "");
                //임시저장 정보
                result.TEMP_YN = JCube.AF.Util.Converts.ToString(dr["TEMP_YN"], "");
                result.TEMP_PDC_CONTENT = JCube.AF.Util.Converts.ToString(dr["TEMP_PDC_CONTENT"], "");
                result.TEMP_PDC_ORG_CONTENT = JCube.AF.Util.Converts.ToString(dr["TEMP_PDC_ORG_CONTENT"], "");
                result.TEMP_REG_DT = JCube.AF.Util.Converts.ToString(dr["TEMP_REG_DT"], "");
            }
            return result;
        }

        public static bool SetCMSProductContent(ProductContentParam param, string mid)
        {
            JCube.AF.DAO.DataHelper helper = GLOBAL.GetJoinsNewsDataHelper();
            helper.ProcParamClear();
            helper.ProcParamAdd("@PD_SEQ", param.PD_SEQ);
            helper.ProcParamAdd("@PDC_SEQ", param.PDC_SEQ);
            helper.ProcParamAdd("@PDC_ORG_CONTENT", SqlDbType.NVarChar, param.PDC_ORG_CONTENT);
            helper.ProcParamAdd("@PDC_CONTENT", SqlDbType.NVarChar, param.PDC_CONTENT);
            helper.ProcParamAdd("@EDITOR", mid);
            bool r = helper.ProcExecuteNonQuery("USP_PD_SetProuctContent_v2") > 1;
            helper.Close();
            return r;
        }

        public static bool SetCMSTempProductContent(ProductContentParam param, string mid)
        {
            JCube.AF.DAO.DataHelper helper = GLOBAL.GetJoinsNewsDataHelper();
            helper.ProcParamClear();
            helper.ProcParamAdd("@PD_SEQ", param.PD_SEQ);
            helper.ProcParamAdd("@PDC_SEQ", param.PDC_SEQ);
            helper.ProcParamAdd("@PDC_ORG_CONTENT", SqlDbType.NVarChar, param.PDC_ORG_CONTENT);
            helper.ProcParamAdd("@PDC_CONTENT", SqlDbType.NVarChar, param.PDC_CONTENT);
            helper.ProcParamAdd("@EDITOR", mid);
            bool r = helper.ProcExecuteNonQuery("USP_PD_SetProuctContentTemp") > 0;
            helper.Close();
            return r;
        }

        public static bool SetCMSProductRelateEdit(int pseq, int relateseq, string used_yn, string editor)
        {
            JCube.AF.DAO.DataHelper helper = GLOBAL.GetJoinsNewsDataHelper();
            helper.ProcParamClear();
            helper.ProcParamAdd("@PD_SEQ", pseq);
            helper.ProcParamAdd("@PD_RELATE_SEQ", relateseq);
            helper.ProcParamAdd("@USED_YN", used_yn);
            helper.ProcParamAdd("@EDITOR", editor);
            bool r = helper.ProcExecuteNonQuery("USP_PD_SetProuctRelateEdit_v2") > 0;
            helper.Close();
            return r;
        }

        public static IEnumerable<Product> GetCMSProductRelateList(int pseq, out int tot_cnt1)
        {
            List<Product> rlist = new List<Product>();
            JCube.AF.DAO.DataHelper helper = GLOBAL.GetJoinsNewsDataHelper();
            helper.ProcParamClear();
            helper.ProcParamAdd("@PD_SEQ", pseq);
            DataSet ds = helper.ProcExecuteDataSet("USP_PD_GetProductRelateList_v2");
            helper.Close();
            tot_cnt1 = 0;
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null)
            {
                int cnt = ds.Tables[0].Rows.Count;
                for (int tmpi = 0; tmpi < cnt; tmpi++)
                {
                    DataRow dr = ds.Tables[0].Rows[tmpi];
                    tot_cnt1 = JCube.AF.Util.Converts.ToInt32(dr["T_COUNT"], 0);
                    rlist.Add(new Models.Sully.Product
                    {
                        ROWNUM = JCube.AF.Util.Converts.ToInt32(dr["ROWNUM"], 0),
                        PD_SEQ = JCube.AF.Util.Converts.ToInt32(dr["PD_SEQ"], 0),
                        PD_RELATE_SEQ = JCube.AF.Util.Converts.ToInt32(dr["PD_RELATE_SEQ"], 0),
                        PD_TYPE = JCube.AF.Util.Converts.ToInt32(dr["PD_TYPE"], 1),
                        PD_CATEGORY = JCube.AF.Util.Converts.ToString(dr["PD_CATEGORY"], ""),
                        PD_TITLE = JCube.AF.Util.Converts.ToString(dr["PD_TITLE"], ""),
                        PD_IMG_URL = JCube.AF.Util.Converts.ToString(dr["PD_IMG_URL"], ""),
                        PD_SERVICE_STATUS = JCube.AF.Util.Converts.ToString(dr["PD_SERVICE_STATUS"], ""),
                        PD_SERVICE_DT = JCube.AF.Util.Converts.ToString(dr["PD_SERVICE_DT"], "")
                    });
                }
            }
            return rlist;
        }

        public static IEnumerable<ProductQuestion> SetProductPoll(int pd_seq, string xml)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_CMS_SetProducutQuestionInfo", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@SEQ", Value = pd_seq },
                    new SqlParameter { ParameterName = "@XML", Value = xml }
                });

                if (ds == null)
                    return null;

                return db.ConvertEntity<ProductQuestion>(ds.Tables[0], (x, row) => {
                    var items = ds.Tables[1].AsEnumerable().Where(r => {
                        return Convert.ToInt32(r["QUESTION_SEQ"]) == x.QuestionSeq;
                    }).CopyToDataTable();
                    x.Items = db.ConvertEntity<ProductQuestion.Item>(items);
                });
            }
        }

        public static IEnumerable<SSullyStoryIntroImage> GetCMSProductSSullyStoryIntroImageList(int page, int page_size, out int total_count, out int total_page)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_CMT_GetStoryIntroImageList", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@PAGE", Value = page },
                    new SqlParameter { ParameterName = "@PAGE_SIZE", Value = page_size },
                    new SqlParameter { ParameterName = "@TOTAL_COUNT", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output },
                    new SqlParameter { ParameterName = "@TOTAL_PAGE", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output }
                });

                total_count = db.GetOutputParameterValue("@TOTAL_COUNT", 0);
                total_page = db.GetOutputParameterValue("@TOTAL_PAGE", 0);

                if (ds == null)
                    return null;

                return db.ConvertEntity<SSullyStoryIntroImage>(ds.Tables[0]);
            }
        }

        public static ProductQuizEventApplyAllList GetCMSProductQuizEventApplyAllList(int pd_seq)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_CMS_GetProductQuizEventApplyUserAllList", new[] {
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = pd_seq }
                });

                if (ds == null)
                    return null;

                return new ProductQuizEventApplyAllList
                {
                    ProductInfo = db.ConvertEntity<ProductQuizEventApplyAllList.Product>(ds.Tables[0].Rows[0]),
                    List = db.ConvertEntity<ProductQuizEventApplyAllList.ApplyUser>(ds.Tables[1])
                };
            }
        }

        public static void DestructionProductQuizEventApplyUserInfo()
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
                db.ProcExecuteNonQuery("USP_SVC_DestructionProductQuizEventApplyUserInfo");
        }

        #endregion



        #region Service Repository

        public static bool SetProductQuizEventApply (int pd_seq, string login_type, string uid, string guid, string user_nm, string phone, string email, bool require_agree_checked, bool optional_agree_checked, string position, out string msg)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_USR_SetProductQuizEventApply", new[] {
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = pd_seq },
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@ANSWER_GUID", Value = guid },
                    new SqlParameter { ParameterName = "@USER_NM", Value = user_nm },
                    new SqlParameter { ParameterName = "@PHONE", Value = phone },
                    new SqlParameter { ParameterName = "@EMAIL", Value = email },
                    new SqlParameter { ParameterName = "@REQUIRE_AGREE_YN", Value = require_agree_checked ? "Y" : "N" },
                    new SqlParameter { ParameterName = "@OPTIONAL_AGREE_YN", Value = optional_agree_checked ? "Y" : "N" },
                    new SqlParameter { ParameterName = "@APPLY_POSITION", Value = position },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output }
                });

                if (db.GetOutputParameterValue<int>("@RESULT_CD", -1) != 1)
                {
                    msg = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.");
                    return false;
                }
                else
                {
                    msg = string.Empty;
                    return true;
                }
            }
        }

        /// <summary>
        /// 퀴즈 문항정보를 검색합니다.
        /// </summary>
        /// <param name="pd_seq"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static ProductQuestionInfo GetProductQuizInfo(int pd_seq, int page)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_SC_PD_CheckProductQuizInfo", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = pd_seq },
                    new SqlParameter { ParameterName = "@PAGE", Value = page }
                });

                if (ds == null)
                    return null;

                return db.ConvertEntity<ProductQuestionInfo>(ds.Tables[0].Rows[0], (x, row) => {
                    x.Items = db.ConvertEntity<ProductQuestionInfo.ProductQuestionItem>(ds.Tables[1]);
                });
            }
        }

        /// <summary>
        /// 선택한 문항의 정답여부를 검색합니다.
        /// </summary>
        /// <param name="pd_seq"></param>
        /// <param name="question_seq"></param>
        /// <param name="item_seq"></param>
        /// <returns></returns>
        public static ProductQuestionResult GetProductQuizResult(int pd_seq, int question_seq, int item_seq)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_SC_PD_CheckProductQuizInfoResult", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = pd_seq },
                    new SqlParameter { ParameterName = "@QUESTION_SEQ", Value = question_seq },
                    new SqlParameter { ParameterName = "@ITEM_SEQ", Value = item_seq }
                });

                if (ds == null)
                    return null;

                return db.ConvertEntity<ProductQuestionResult>(ds.Tables[0].Rows[0]);
            }
        }

        public static ProductQuestionApplyResult SetProductDetailQuestionApply(int pd_seq, string login_type, string uid, string xml, string ip_address, out string msg)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_SC_SetProducutQuestionAnswer", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@SEQ", Value = pd_seq },
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@XML", Value = xml },
                    new SqlParameter { ParameterName = "@IP_ADDRESS", Value = ip_address },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                if (db.GetOutputParameterValue<int>("@RESULT_CD", -1) != 1)
                {
                    msg = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.");
                    return null;
                }
                else
                {
                    msg = string.Empty;

                    if (ds == null)
                        return null;

                    return db.ConvertEntity<Models.Sully.ProductQuestionApplyResult>(ds.Tables[0].Rows[0]);
                }
            }
        }

        public static ProductSitemap GetSitemapProductList()
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_SC_PD_GetSitemapProductList");

                if (ds == null)
                    return null;

                return new ProductSitemap
                {
                    url = db.ConvertEntity<ProductSitemapItem>(ds.Tables[0]).ToList()
                };
            }
        }

        public static IEnumerable<ProductRSS> GetRssProductList(int size)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_SC_PD_GetRSSProductList", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@PageSize", Value = size }
                });

                if (ds == null)
                    return null;

                var product = new Product { PD_TYPE = 1 };
                return db.ConvertEntity<ProductRSS>(ds.Tables[0], (x, row) => {
                    // 이미지 사이즈
                    if (!string.IsNullOrWhiteSpace(x.ImageUrl))
                    {
                        try
                        {
                            var req = System.Net.WebRequest.Create(x.ImageUrl);
                            req.Method = System.Net.WebRequestMethods.Http.Head;
                            using (System.Net.WebResponse resp = req.GetResponse())
                            {
                                int contentLength;

                                if (int.TryParse(resp.Headers.Get("Content-Length"), out contentLength))
                                    x.ImageLength = contentLength;
                            }
                        }
                        catch { }
                    }

                    // 대화형의 3줄요약본만 콘텐트에 설정
                    if (x.ProductType != 1)
                        x.Content = string.Empty;

                    if (!string.IsNullOrWhiteSpace(x.Content))
                    {
                        var contentData = ContentOptimizer.GetContent(product, x.Content, null) as Apps.Common.Sully.ContentOptimizers.Optimizer_Type1.ContentResult;

                        x.Content = contentData.Pages.Where(p =>
                        {
                            var msg = p.MessageBlock.Where(m => m.Type == Apps.Common.Sully.ContentOptimizers.Optimizer_Type1.ResultEntity.MessageTarget.SUMMARY).FirstOrDefault();
                            return msg != null;
                        }).Select(p =>
                        {
                            var msg = p.MessageBlock.Where(m => m.Type == Apps.Common.Sully.ContentOptimizers.Optimizer_Type1.ResultEntity.MessageTarget.SUMMARY).FirstOrDefault();

                            if (msg != null)
                                return string.Join("\r\n", msg.Message.Select(m => m.Text).ToArray());

                            return null;
                        }).FirstOrDefault();
                    }
                    else
                    {
                        x.Content = string.Empty;
                    }
                });
            }
        }

        public static IEnumerable<Product> GetServiceProductList(SCProductListParam param, string login_type, string uid, string reg_ip, out int tot_cnt)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_SC_PD_GetProductList", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@PageIndex", Value = param.pgi },
                    new SqlParameter { ParameterName = "@PageSize", Value = param.ps },
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@PD_TYPE", Value = param.ptype },
                    new SqlParameter { ParameterName = "@PD_CATEGORY", Value = param.pcate },
                    new SqlParameter { ParameterName = "@PD_TAG", Value = param.ptag },
                    new SqlParameter { ParameterName = "@PD_TITLE", Value = param.ptitle },
                    new SqlParameter { ParameterName = "@WORD", Value = param.search_word },
                    new SqlParameter { ParameterName = "@ORDER", Value = param.order },
                    new SqlParameter { ParameterName = "@LAST_SERVICE_DT", Value = param.lastdt },
                    new SqlParameter { ParameterName = "@REG_IP", Value = reg_ip },
                    new SqlParameter { ParameterName = "@T_COUNT", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  }
                });

                tot_cnt = 0;
                if (ds == null)
                    return null;

                tot_cnt = db.GetOutputParameterValue("@T_COUNT", 0);
                return db.ConvertEntity<Product>(ds.Tables[0]);
            }
        }

        public static IEnumerable<APIProduct> GetNews10APIProductList(int page, int page_size)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_SC_PD_GetProductListNews10", new[] {
                    new SqlParameter { ParameterName = "@PageIndex", Value = page },
                    new SqlParameter { ParameterName = "@PageSize", Value = page_size }
                });

                if (ds == null)
                    return new List<APIProduct>();

                return db.ConvertEntity<APIProduct>(ds.Tables[0], (entity, row) =>
                {
                    entity.viewurl = string.Format("https://ssully.joins.com/View/Apps/{0}", entity.pseq);
                    entity.shareurl = string.Format("https://ssully.joins.com/View/{0}?share=y", entity.pseq);
                });
            }
        }

        public static ProductContent GetServiceProductContentInfo(int pseq, bool is_debug, string login_type, string uid)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_SC_PD_GetProductContentInfo", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = pseq },
                    new SqlParameter { ParameterName = "@IS_DEBUG", Value = is_debug ? "Y" : "N" },
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid }
                });

                if (ds == null)
                    return new ProductContent();

                return db.ConvertEntity<ProductContent>(ds.Tables[0].Rows[0]);
            }
        }

        public static List<Product> GetServiceProductRelateList(int pseq)
        {
            List<Product> rlist = new List<Product>();
            using (var helper = GLOBAL.GetJoinsNewsDataHelper())
            {
                helper.ProcParamClear();
                helper.ProcParamAdd("@PD_SEQ", pseq);
                DataSet ds = helper.ProcExecuteDataSet("USP_SC_PD_GetProductRelateList");

                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null)
                {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++)
                    {
                        DataRow dr = ds.Tables[0].Rows[tmpi];

                        rlist.Add(new Models.Sully.Product
                        {
                            ROWNUM = JCube.AF.Util.Converts.ToInt32(dr["ROWNUM"], 0),
                            PD_SEQ = JCube.AF.Util.Converts.ToInt32(dr["PD_SEQ"], 0),
                            PD_RELATE_SEQ = JCube.AF.Util.Converts.ToInt32(dr["PD_RELATE_SEQ"], 0),
                            PD_TYPE = JCube.AF.Util.Converts.ToInt32(dr["PD_TYPE"], 1),
                            PD_CATEGORY = JCube.AF.Util.Converts.ToString(dr["PD_CATEGORY"], ""),
                            PD_TITLE = JCube.AF.Util.Converts.ToString(dr["PD_TITLE"], ""),
                            PD_IMG_URL = JCube.AF.Util.Converts.ToString(dr["PD_IMG_URL"], ""),
                            PD_SERVICE_STATUS = JCube.AF.Util.Converts.ToString(dr["PD_SERVICE_STATUS"], ""),
                            PD_SERVICE_DT = JCube.AF.Util.Converts.ToString(dr["PD_SERVICE_DT"], ""),
                            IS_AD_PRODUCT = JCube.AF.Util.Converts.ToString(dr["IS_AD_PRODUCT"], "N"),
                        });
                    }
                }
            }

            return rlist;
        }

        public static ProductQuestionShareDataResult GetQuestionShareData(int seq, string guid)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_SC_GetProducutQuestionAnswerResult", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = seq },
                    new SqlParameter { ParameterName = "@GUID", Value = guid }
                });

                if (ds == null)
                    return null;

                return db.ConvertEntity<ProductQuestionShareDataResult>(ds.Tables[0].Rows[0]);
            }
        }

        public static EventQuizProductInfo GetEventQuizProductData()
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_WEB_GetEventQuizProductInfo");

                if (ds == null)
                    return null;

                return db.ConvertEntity<EventQuizProductInfo>(ds.Tables[0].Rows[0]);
            }
        }

        public static DbExecuteResult SetProductLike(string login_type, string uid, int pd_seq, out string like_yn, out int total_like_cnt)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_USR_SetProductLike", new[] {
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = pd_seq },
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@LIKE_YN", SqlDbType = SqlDbType.Char, Size = 1, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@TOTAL_LIKE_CNT", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  }
                });

                like_yn = db.GetOutputParameterValue("@LIKE_YN", "N");
                total_like_cnt = db.GetOutputParameterValue("@TOTAL_LIKE_CNT", 0);

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        public static DbExecuteResult SetProductScrap(string login_type, string uid, int pd_seq, out string scrap_yn)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_USR_SetProductScrap", new[] {
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = pd_seq },
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@SCRAP_YN", SqlDbType = SqlDbType.Char, Size = 1, Direction = ParameterDirection.Output  }
                });

                scrap_yn = db.GetOutputParameterValue("@SCRAP_YN", "N");

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        #endregion



        #region Common Repository

        public static CornerListHeader GetCornerListHeader (int pd_type)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_GetProductCornerHeader", new[] {
                    new SqlParameter { ParameterName = "@PD_TYPE", Value = pd_type }
                });

                if (ds == null)
                    return null;

                return db.ConvertEntity<CornerListHeader>(ds.Tables[0].Rows[0]);
            }
        }

        public static Product GetProductInfo(int pseq, string login_type = "", string uid = "")
        {
            Product result = new Product();
            JCube.AF.DAO.DataHelper helper = GLOBAL.GetJoinsNewsDataHelper();

            using(var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_PD_GetProductInfo", new[] {
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = pseq },
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid }
                });

                if (ds != null)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    result.PDC_SEQ = JCube.AF.Util.Converts.ToInt32(dr["PDC_SEQ"], 0);
                    result.PD_SEQ = JCube.AF.Util.Converts.ToInt32(dr["PD_SEQ"], 0);
                    result.PD_USED_YN = JCube.AF.Util.Converts.ToString(dr["PD_USED_YN"], "");
                    result.PD_TYPE = JCube.AF.Util.Converts.ToInt32(dr["PD_TYPE"], 1);
                    result.PD_CATEGORY = JCube.AF.Util.Converts.ToString(dr["PD_CATEGORY"], "");
                    result.PD_TAG = JCube.AF.Util.Converts.ToString(dr["PD_TAG"], "");
                    result.PD_TITLE = JCube.AF.Util.Converts.ToString(dr["PD_TITLE"], "");
                    result.PD_IMG_URL = JCube.AF.Util.Converts.ToString(dr["PD_IMG_URL"], "");
                    result.PD_SERVICE_STATUS = JCube.AF.Util.Converts.ToString(dr["PD_SERVICE_STATUS"], "");
                    result.PD_SERVICE_DT = JCube.AF.Util.Converts.ToString(dr["PD_SERVICE_DT"], "");
                    result.PD_EDITOR_NM = JCube.AF.Util.Converts.ToString(dr["PD_EDITOR_NM"], "");
                    result.PD_REG_ID = JCube.AF.Util.Converts.ToString(dr["PD_REG_ID"], "");
                    result.PD_REG_DT = JCube.AF.Util.Converts.ToString(dr["PD_REG_DT"], "");
                    result.PD_MOD_ID = JCube.AF.Util.Converts.ToString(dr["PD_MOD_ID"], "");
                    result.PD_MOD_DT = JCube.AF.Util.Converts.ToString(dr["PD_MOD_DT"], "");
                    result.CR_SEQ_01 = JCube.AF.Util.Converts.ToInt32(dr["CR_SEQ_01"], 0);
                    result.CR_NAME_01 = JCube.AF.Util.Converts.ToString(dr["CR_NAME_01"], "");
                    result.CR_IMG_01 = JCube.AF.Util.Converts.ToString(dr["CR_IMG_01"], "");
                    result.CR_SEQ_02 = JCube.AF.Util.Converts.ToInt32(dr["CR_SEQ_02"], 0);
                    result.CR_NAME_02 = JCube.AF.Util.Converts.ToString(dr["CR_NAME_02"], "");
                    result.CR_IMG_02 = JCube.AF.Util.Converts.ToString(dr["CR_IMG_02"], "");
                    result.PD_SHARE_IMG_URL = JCube.AF.Util.Converts.ToString(dr["PD_SHARE_IMG_URL"], "");
                    result.PD_SUMMARY = JCube.AF.Util.Converts.ToString(dr["PD_SUMMARY"], "");
                    result.PD_PROVIDER = JCube.AF.Util.Converts.ToString(dr["PD_PROVIDER"], "1");
                    result.PD_PROVIDER_URL = JCube.AF.Util.Converts.ToString(dr["PD_PROVIDER_URL"], "");
                    result.IS_AD_PRODUCT = JCube.AF.Util.Converts.ToString(dr["IS_AD_PRODUCT"], "N");
                    result.PUSH_YN = JCube.AF.Util.Converts.ToString(dr["PUSH_YN"], "Y");
                    result.IS_AD_PRODUCT = JCube.AF.Util.Converts.ToString(dr["IS_AD_PRODUCT"], "N");
                    result.PD_SHARE_DESCRIPTION = JCube.AF.Util.Converts.ToString(dr["PD_SHARE_DESCRIPTION"], "");
                    result.PD_SHARE_HONEYSCREEN_IMG_URL = JCube.AF.Util.Converts.ToString(dr["PD_SHARE_HONEYSCREEN_IMG_URL"], "");

                    result.QUIZ_EVENT_YN = JCube.AF.Util.Converts.ToString(dr["QUIZ_EVENT_YN"], "N");
                    result.QUIZ_EVENT_STATUS = Convert.ToInt32(dr["QUIZ_EVENT_STATUS"]);
                    result.QUIZ_EVENT_POPUP_IMG_URL = Convert.ToString(dr["QUIZ_EVENT_POPUP_IMG_URL"]);
                    result.QUIZ_EVENT_PRIZE_IMG_URL = Convert.ToString(dr["QUIZ_EVENT_PRIZE_IMG_URL"]);
                    result.QUIZ_EVENT_START_DT = (dr.IsNull("QUIZ_EVENT_START_DT") ? null : dr["QUIZ_EVENT_START_DT"]) as DateTime?;
                    result.QUIZ_EVENT_END_DT = (dr.IsNull("QUIZ_EVENT_END_DT") ? null : dr["QUIZ_EVENT_END_DT"]) as DateTime?;
                    result.QUIZ_EVENT_NOTICE_DT = (dr.IsNull("QUIZ_EVENT_NOTICE_DT") ? null : dr["QUIZ_EVENT_NOTICE_DT"]) as DateTime?;
                    result.QUIZ_EVENT_INFO_DESC = Convert.ToString(dr["QUIZ_EVENT_INFO_DESC"]);

                    result.LIKE_CNT = Convert.ToInt32(dr["TOTAL_LIKE_CNT"]);
                    result.LIKE_YN = Convert.ToString(dr["LIKE_YN"]);
                    result.SCRAP_YN = Convert.ToString(dr["SCRAP_YN"]);
                }

                return result;
            }
        }

        #endregion


        public static void SetProductViewRanking()
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
                db.ProcExecuteNonQuery("USP_SCD_SetProductViewRanking");
        }
    }
}