﻿using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Repository.Sully
{
    public static class SearchRankingRepository
    {
        /// <summary>
        /// 검색어 랭킹을 검색합니다.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<SearchRanking> GetSearchRanking()
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_GET_SEARCH_RANKING");

                if (ds != null)
                    return db.ConvertEntity<SearchRanking>(ds.Tables[0]);
            }

            return null;
        }
    }
}
