﻿using Joins.Apps.Repository.Common;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Joins.Apps.Repository.Sully
{
    public class BlockWordRepository
    {
        /// <summary>
        /// 금칙어 목록을 검색합니다.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="page_size"></param>
        /// <param name="used_yn"></param>
        /// <param name="search"></param>
        /// <param name="tot_cnt"></param>
        /// <returns></returns>
        public static IEnumerable<Joins.Apps.Models.Sully.BlockWordList> GetBlockWordList(int page, int page_size, string used_yn, string search, out int tot_cnt)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_CMT_GetBlockWordList", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@PageIndex", Value = page },
                    new SqlParameter { ParameterName = "@PageSize", Value = page_size },
                    new SqlParameter { ParameterName = "@USED_YN", Value = used_yn },
                    new SqlParameter { ParameterName = "@SEARCH", Value = search, SqlDbType = SqlDbType.NVarChar },
                    new SqlParameter { ParameterName = "@TOTAL_COUNT", Size = 4, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output },
                });
                tot_cnt = db.GetOutputParameterValue("@TOTAL_COUNT", 0);

                if (ds == null)
                    return null;

                return db.ConvertEntity<Models.Sully.BlockWordList>(ds.Tables[0]);
            }
        }

        /// <summary>
        /// 금칙어 처리
        /// </summary>
        /// <param name="word">금칙어</param>
        /// <param name="desc">사유</param>
        /// <returns></returns>
        public static DbExecuteResult SetBlockWord(string word, string desc)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_CMT_SetBlockWord", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@WORD", Value = word },
                     new SqlParameter { ParameterName = "@BND_DESC", Value = desc },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });
                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 금칙어 상태 변경
        /// </summary>
        /// <param name="seq">고유번호</param>
        /// <param name="status">상태</param>
        /// <returns></returns>
        public static DbExecuteResult SetBlockWordStatus(int seq, string status)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_CMT_SetBlockWordStatus", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@SEQ", Value = seq },
                    new SqlParameter { ParameterName = "@USED_YN", Value = status },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }
    }
}
