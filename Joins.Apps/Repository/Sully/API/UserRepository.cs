﻿using Joins.Apps.Models.Sully.API;
using Joins.Apps.Repository.Common;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Joins.Apps.Repository.Sully.API
{
    public class UserRepository
    {
#if DEBUG
        public static bool RemovePromotion(string uid, string device_id)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("test", new[] {
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@DEVICE_ID", Value = device_id },
                });

                return true;
            }
        }
#endif

        /// <summary>
        /// 탈퇴하지 회원인지 여부를 확인합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <returns></returns>
        public static bool HasActiveUserInfo(string login_type, string uid)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_API_HasUserInfo", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@RESULT", SqlDbType = SqlDbType.Bit, Size = 1, Direction = ParameterDirection.Output }
                });

                return db.GetOutputParameterValue("@RESULT", false);
            }
        }
        /// <summary>
        /// 회원정보를 셋팅하고 정보를 반환합니다
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="email">이메일 주소</param>
        /// <param name="name">SNS 이름</param>
        /// <param name="profile_url">프로필 이미지 주소</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="device_id">디바이스 아이디</param>
        /// <param name="device_push_key">디바이스 푸시 키</param>
        /// <param name="fcm_token">파이어베이스 푸시 토큰</param>
        /// <param name="device_type">디바이스 타입</param>
        /// <param name="ip">아이피 주소</param>
        /// <returns></returns>
        public static DbExecuteResult SetLoginUserInfo(string login_type, string email, string name, string profile_url, string uid, string device_id, string device_push_key, string fcm_token, string device_type, string ip)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_API_SetLoginInfo", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@EMAIL", Value = email },
                    new SqlParameter { ParameterName = "@NAME", Value = name },
                    new SqlParameter { ParameterName = "@PROFILE_URL", Value = profile_url},
                    new SqlParameter { ParameterName = "@DEVICE_ID", Value = device_id },
                    new SqlParameter { ParameterName = "@DEVICE_PUSH_KEY", Value = device_push_key },
                    new SqlParameter { ParameterName = "@FCM_TOKEN", Value = fcm_token },
                    new SqlParameter { ParameterName = "@DEVICE_TYPE", Value = device_type },
                    new SqlParameter { ParameterName = "@IP_ADDRESS", Value = ip },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }
                

        /// <summary>
        /// 회원정보를 검색합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="device_id">디바이스 아이디</param>
        /// <returns></returns>
        public static UserInfo GetUserInfo(string login_type, string uid, string device_id)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_API_GetUserInfo", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@DEVICE_ID", Value = device_id }
                });

                if (ds == null)
                    return null;

                return db.ConvertEntity<UserInfo>(ds.Tables[0].Rows[0]);
            }
        }

        /// <summary>
        /// 회원 관심카테고리 정보를 검색합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <returns></returns>
        public static IEnumerable<UserCategory> GetUserCategory(string login_type, string uid)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_API_GetUserCategory", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid }
                });

                if (ds == null)
                    return null;

                return db.ConvertEntity<UserCategory>(ds.Tables[0]);
            }
        }

        /// <summary>
        /// 회원정보를 변경합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="nick_name">닉네임</param>
        /// <param name="gender">성별</param>
        /// <param name="age">연령대</param>
        /// <param name="image_url">이미지 주소</param>
        /// <param name="category_xml">관심사 카테고리</param>
        /// <returns></returns>
        public static DbExecuteResult SetUserInfo(string login_type, string uid, string nick_name, string gender, int? age, string image_url, string category_xml)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_API_SetUserInfo", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@NICK_NAME", Value = nick_name },
                    new SqlParameter { ParameterName = "@GENDER", Value = gender },
                    new SqlParameter { ParameterName = "@AGE", Value = age },
                    new SqlParameter { ParameterName = "@IMAGE_URL", Value = image_url },
                    new SqlParameter { ParameterName = "@CATEGORY_XML", Value = category_xml },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 로그아웃 처리합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="device_id">디바이스ID</param>
        /// <param name="device_type">디바이스 종류</param>
        /// <returns></returns>
        public static DbExecuteResult SetLogout(string login_type, string uid, string device_id, string device_type)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var result = db.ProcExecuteNonQuery("USP_API_SetLogout", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@DEVICE_ID", Value = device_id },
                    new SqlParameter { ParameterName = "@DEVICE_TYPE", Value = device_type },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 회원탈퇴 처리합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <returns></returns>
        public static DbExecuteResult SetWithdraw(string login_type, string uid)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var result = db.ProcExecuteNonQuery("USP_API_SetWithdraw", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 워크쓰루에서 선택정보를 설정합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="gender">성별</param>
        /// <param name="age">연령대</param>
        /// <param name="push_enable">푸시수신여부</param>
        /// <param name="category_xml">카테고리XML</param>
        /// <returns>처리여부</returns>
        public static DbExecuteResult SetWalkthroughOptionalInfo(string login_type, string uid, string gender, int? age, string push_enable, string category_xml)
        {
            using(var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_API_SetWalkthroughOptionalInfo", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@GENDER", Value = gender },
                    new SqlParameter { ParameterName = "@AGE", Value = age },
                    new SqlParameter { ParameterName = "@PUSH_ENABLE", Value =  push_enable },
                    new SqlParameter { ParameterName = "@CATEGORY_XML", Value = category_xml },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }
        /// <summary>
        /// 워크쓰루에서 선택정보를 설정합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="gender">성별</param>
        /// <param name="age">연령대</param>
        /// <param name="push_enable">푸시수신여부</param>
        /// <param name="category_xml">카테고리XML</param>
        /// <param name="code">추천인 코드</param>
        /// <returns>처리여부</returns>
        public static DbExecuteResult SetWalkthroughOptionalInfoTmp(string login_type, string uid, string gender, int? age, string push_enable, string category_xml, string code, string device_type, string device_id)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_API_SetWalkthroughOptionalInfoTmp", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@GENDER", Value = gender },
                    new SqlParameter { ParameterName = "@AGE", Value = age },
                    new SqlParameter { ParameterName = "@PUSH_ENABLE", Value =  push_enable },
                    new SqlParameter { ParameterName = "@CATEGORY_XML", Value = category_xml },
                    new SqlParameter { ParameterName = "@RECOMMEND_CODE", Value = code },
                    new SqlParameter { ParameterName = "@DEVICE_ID", Value = device_id },
                    new SqlParameter { ParameterName = "@DEVICE_TYPE", Value = device_type },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }
        /// <summary>
        /// 프로모션 코드 사용처리합니다.
        /// </summary>
        /// <param name="login_type"></param>
        /// <param name="uid"></param>
        /// <param name="device_type"></param>
        /// <param name="device_id"></param>
        /// <param name="ip_address"></param>
        /// <returns></returns>
        public static DbExecuteResult SetUSP_API_SetEventMemberRecommentPromotion_201811(string login_type, string uid, string device_type, string device_id, string ip_address)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_API_SetEventMemberRecommentPromotion_201811", new[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@DEVICE_ID", Value = device_id },
                    new SqlParameter { ParameterName = "@DEVICE_TYPE", Value = device_type },
                    new SqlParameter { ParameterName = "@IPADDRESS", Value =  ip_address},
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 워크쓰루에서 필수입력정보를 설정합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="nickname">설정할 닉네임</param>
        /// <returns></returns>
        public static DbExecuteResult SetWalkthroughNickName(string login_type, string uid, string nickname)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_API_SetWalkthroughNickname", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@NICK_NAME", Value = nickname },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 사용가능한 닉네임인지 체크합니다.
        /// </summary>
        /// <param name="nick_name">닉네임</param>
        /// <returns></returns>
        public static DbExecuteResult CheckDeniedNickname(string nick_name)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_API_CheckDeniedNickName", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@NICK_NAME", Value = nick_name },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });
                
                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }
        
        /// <summary>
        /// 프로필 이미지 주소를 변경합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="upload_path">업로드 경로</param>
        /// <returns></returns>
        public static DbExecuteResult SetProfileUrl(string login_type, string uid, string upload_path)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_API_SetMemberProfileUrl", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@UPLOAD_PATH", Value = upload_path },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }
    }
}
