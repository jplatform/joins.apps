﻿using Joins.Apps.Models.Sully.API;
using Joins.Apps.Repository.Common;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace Joins.Apps.Repository.Sully.API
{
    public class NotificationRepository
    {
        public static void SetSystemNotifyData()
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_SCD_SetNotifyData");
            }
        }

        /// <summary>
        /// 확인하지 않은 새로운 알림갯수를 검색합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <returns></returns>
        public static int GetNewNotifyCount(string login_type, string uid)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_GetNotiNewMessageList", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@NEW_NOTI_COUNT", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output }
                });

                return db.GetOutputParameterValue("@NEW_NOTI_COUNT", 0);
            }
        }
        /// <summary>
        /// 알림 목록을 검색합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="page">페이지</param>
        /// <param name="page_size">페이지당 리스트 수</param>
        /// <param name="total_count">총 리스트 수</param>
        /// <param name="total_page">총 페이지 수</param>
        /// <returns></returns>
        public static IEnumerable<NotificationList> GetNotifyList(string login_type, string uid, int page, int page_size, out int total_count, out int total_page)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_API_GetNotiMessageList_v2", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@PAGE", Value = page },
                    new SqlParameter { ParameterName = "@PAGE_SIZE", Value = page_size },
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@TOTAL_COUNT", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output },
                    new SqlParameter { ParameterName = "@TOTAL_PAGE", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output }
                });

                total_count = db.GetOutputParameterValue("@TOTAL_COUNT", 0);
                total_page = db.GetOutputParameterValue("@TOTAL_PAGE", 0);

                if (ds == null)
                    return null;

                return db.ConvertEntity<NotificationList>(ds.Tables[0], (x, row) => {
                    if (!string.IsNullOrWhiteSpace(x.CustomString))
                    {
                        var doc = new XmlDocument();
                        doc.LoadXml(x.CustomString);

                        x.Custom = new Dictionary<string, string>();
                        foreach (XmlNode node in doc.DocumentElement.ChildNodes)
                            x.Custom[node.Name.ToLower()] = node.InnerText;
                    }
                });
            }
        }
    }
}
