﻿using Joins.Apps.Models.Sully.API;
using Joins.Apps.Repository.Common;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Joins.Apps.Repository.Sully.API
{
    public class ProductRepository
    {
        /// <summary>
        /// 메인 기사 목록을 검색합니다.
        /// </summary>
        /// <param name="skip_count">건너뛸 갯수</param>
        /// <param name="count">가져올 갯수</param>
        /// <param name="pd_type">가져올 코너</param>
        /// <param name="is_debug">디버깅 여부</param>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <returns></returns>
        public static IEnumerable<ProductMain> GetProductListToMainV2(int skip_count, int count, int pd_type, bool is_debug, string login_type, string uid)
        {
            var tmp = 0;
            return GetProductListToMainV2(skip_count, count, pd_type, is_debug, login_type, uid, out tmp);
        }
        /// <summary>
        /// 메인 기사 목록을 검색합니다.
        /// </summary>
        /// <param name="skip_count">건너뛸 갯수</param>
        /// <param name="count">가져올 갯수</param>
        /// <param name="pd_type">가져올 코너</param>
        /// <param name="is_debug">디버깅 여부</param>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="total_count">전체 갯수</param>
        /// <returns></returns>
        public static IEnumerable<ProductMain> GetProductListToMainV2(int skip_count, int count, int pd_type, bool is_debug, string login_type, string uid, out int total_count)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_USR_GetMainLayout_Product", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@SKIP_COUNT", Value = skip_count },
                    new SqlParameter { ParameterName = "@COUNT", Value = count },
                    new SqlParameter { ParameterName = "@PD_TYPE", Value = pd_type },
                    new SqlParameter { ParameterName = "@IS_DEBUG", Value = is_debug ? "Y" : "N" },
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@TOTAL_COUNT", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output },
                });

                total_count = db.GetOutputParameterValue("@TOTAL_COUNT", 0);

                if (ds == null)
                    return null;

                var result = db.ConvertEntity<ProductMain>(ds.Tables[0], (p, row) =>
                {
                    if (!string.IsNullOrWhiteSpace(p.TagFromDB))
                        p.TagToJson = p.TagFromDB.Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x));
                });

                return result;
            }
        }

        /// <summary>
        /// 기사 목록을 검색합니다.
        /// </summary>
        /// <param name="page">페이지</param>
        /// <param name="page_size">페이지당 리스트 수</param>
        /// <param name="category">카테고리 코드</param>
        /// <param name="title">제목</param>
        /// <param name="corner_type">코너 종류</param>
        /// <param name="search_xml">검색어</param>
        /// <param name="order">정렬방식 (latest : 최신순 / popularity : 인기순)</param>
        /// <param name="is_debug">디버깅 여부</param>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="total_count">총 리스트 수</param>
        /// <param name="total_page">총 페이지 수</param>
        /// <returns></returns>
        public static IEnumerable<Product> GetProductList(int page, int page_size, string category, string title, int? corner_type, string search_xml, string order, bool is_debug, string login_type, string uid, string ip, out int total_count, out int total_page)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_API_GetProductList", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@PAGE", Value = page },
                    new SqlParameter { ParameterName = "@PAGE_SIZE", Value = page_size },
                    new SqlParameter { ParameterName = "@CATEGORY", Value = category },
                    new SqlParameter { ParameterName = "@TITLE", Value = title },
                    new SqlParameter { ParameterName = "@PD_TYPE", Value = corner_type },
                    new SqlParameter { ParameterName = "@WORD", Value = search_xml },
                    new SqlParameter { ParameterName = "@ORDER", Value = order },
                    new SqlParameter { ParameterName = "@IS_DEBUG", Value = is_debug ? "Y" : "N" },
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@REG_IP", Value = ip },
                    new SqlParameter { ParameterName = "@TOTAL_COUNT", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output },
                    new SqlParameter { ParameterName = "@TOTAL_PAGE", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output }
                });

                total_count = db.GetOutputParameterValue("@TOTAL_COUNT", 0);
                total_page = db.GetOutputParameterValue("@TOTAL_PAGE", 0);

                if (ds == null)
                    return null;

                var result = db.ConvertEntity<Product>(ds.Tables[0], (p, row) =>
                {
                    if (!string.IsNullOrWhiteSpace(p.TagFromDB))
                        p.TagToJson = p.TagFromDB.Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x));
                });

                return result;
            }
        }

        public static Models.Sully.ProductQuestionApplyResult SetProductDetailQuestionApply(string login_type, string uid, int seq, string xml, string ip_address, out string msg)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_API_SetProducutQuestionAnswer", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@SEQ", Value = seq },
                    new SqlParameter { ParameterName = "@XML", Value = xml },
                    new SqlParameter { ParameterName = "@IP_ADDRESS", Value = ip_address },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                if (db.GetOutputParameterValue<int>("@RESULT_CD", -1) != 1)
                {
                    msg = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.");
                    return null;
                }
                else
                {
                    msg = string.Empty;

                    if (ds == null)
                        return null;

                    return db.ConvertEntity<Models.Sully.ProductQuestionApplyResult>(ds.Tables[0].Rows[0]);
                }
            }
        }
    }
}