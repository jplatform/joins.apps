﻿using Joins.Apps.Models.Sully.API;
using Joins.Apps.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Repository.Sully.API
{
    public static class AppVersionRepository
    {
        /// <summary>
        /// 현재 썰리 앱의 업데이트 버전 정보를 검색합니다.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<AppVersion> GetAppVersion()
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_API_APPVERSION");

                if (ds != null)
                    return db.ConvertEntity<AppVersion>(ds.Tables[0]);
            }

            return null;
        }
    }
}
