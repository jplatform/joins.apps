﻿using Joins.Apps.Models.Sully.API;
using Joins.Apps.Repository.Common;
using System.Data;
using System.Data.SqlClient;

namespace Joins.Apps.Repository.Sully.API
{
    public class ConfigRepository
    {
        /// <summary>
        /// 디바이스 푸시 키를 변경합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="device_id">디바이스ID</param>
        /// <param name="device_type">디바이스 종류</param>
        /// <param name="device_push_key">변경할 푸시 키</param>
        /// <param name="fcm_token">파이어베이스 푸시 토큰</param>
        /// <returns>처리 여부</returns>
        public static DbExecuteResult SetDevicePushKey(string login_type, string uid, string device_id, string device_type, string device_push_key, string fcm_token)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var result = db.ProcExecuteNonQuery("USP_API_SetDevicePushKey", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@DEVICE_ID", Value = device_id },
                    new SqlParameter { ParameterName = "@DEVICE_TYPE", Value = device_type },
                    new SqlParameter { ParameterName = "@DEVICE_PUSH_KEY", Value = device_push_key },
                    new SqlParameter { ParameterName = "@FCM_TOKEN", Value = fcm_token },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 푸시 수신여부를 설정합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="enable">푸시수신여부</param>
        /// <returns></returns>
        public static DbExecuteResult SetPushStatus(string login_type, string uid, string enable)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_API_SetAppPush", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@ENABLE", Value = enable },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 콘텐츠 설정을 변경합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="animation">애니메이션 재생속도 단계</param>
        /// <param name="font">글자 크기 단계</param>
        /// <param name="animation_yn">애니메이션 사용여부</param>
        /// <returns></returns>
        public static DbExecuteResult SetContentSetting(string login_type, string uid, int? animation, int? font, string animation_yn)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_API_SetContentsSetting", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@ANIMATION_STEP", Value = animation},
                    new SqlParameter { ParameterName = "@FONT_STEP", Value = font },
                    new SqlParameter { ParameterName = "@ANIMATION_YN", Value = string.IsNullOrWhiteSpace(animation_yn) ? null : animation_yn.Trim() },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 콘텐츠 셋팅 정보를 조회합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <returns></returns>
        public static ContentSetting GetContentSetting(string login_type, string uid)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_API_GetContentsSetting", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid }
                });

                if (ds != null)
                    return db.ConvertEntity<ContentSetting>(ds.Tables[0].Rows[0]);
            }

            return null;
        }
    }
}
