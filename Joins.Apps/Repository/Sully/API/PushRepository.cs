﻿using Joins.Apps.Models.Sully.API;
using Joins.Apps.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Repository.Sully.API
{
    public class PushRepository
    {
        public static IEnumerable<PushSend> GetPushSendList()
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_SCD_GetPushSendList_v2");

                if (ds == null)
                    return null;

                var result = db.ConvertEntity<PushSend>(ds.Tables[0]);

                return result;
            }
        }

        public static void SetPushSendResult(int seq, string xml)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_SCD_SetPushSendResult_v2", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@SEQ", Value = seq },
                    new SqlParameter { ParameterName = "@XML", Value = xml }
                });
            }
        }
    }
}