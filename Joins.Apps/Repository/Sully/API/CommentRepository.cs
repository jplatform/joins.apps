﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Joins.Apps.Repository.Common;

namespace Joins.Apps.Repository.Sully.API
{
    public class CommentRepository
    {
        /// <summary>
        /// 프로덕트의 댓글 갯수를 검색합니다.
        /// </summary>
        /// <param name="pd_seq"></param>
        /// <returns></returns>
        public static int? GetProductCommentCount(int pd_seq)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_USR_GetCommentCount", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = pd_seq },
                    new SqlParameter { ParameterName = "@TOTAL_COUNT", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  }
                });

                return db.GetOutputParameterValue<int?>("@TOTAL_COUNT", null);
            }
        }

        /// <summary>
        /// 댓글 목록을 검색합니다.
        /// </summary>
        /// <param name="login_type">로그인 타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="pdseq">기사고유번호</param>
        /// <param name="last_seq">불러올 댓글 위치 고유번호</param>
        /// <param name="target_seq">검색할 댓글의 고유번호</param>
        /// <param name="page_size">검색할 갯수</param>
        /// <param name="orderby">정렬방식</param>
        /// <param name="total_count">총 댓글 갯수</param>
        /// <returns></returns>
        public static IEnumerable<Joins.Apps.Models.Sully.API.Comment.ItemList> GetCommentList(string login_type, string uid, int pdseq, int last_seq, int? target_seq, int page_size, string orderby, out int total_count, out int total_last_seq)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_API_CMT_GetCommentList", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = pdseq },
                    new SqlParameter { ParameterName = "@LAST_SEQ", Value = last_seq },
                    new SqlParameter { ParameterName = "@TARGET_SEQ", Value = target_seq },
                    new SqlParameter { ParameterName = "@PAGE_SIZE", Value = page_size },
                    new SqlParameter { ParameterName = "@ORDER_BY", Value = orderby },
                    new SqlParameter { ParameterName = "@TOTAL_COUNT", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output },
                    new SqlParameter { ParameterName = "@TOTAL_LAST_SEQ", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output }
                });

                total_count = db.GetOutputParameterValue("@TOTAL_COUNT", 0);
                total_last_seq = db.GetOutputParameterValue("@TOTAL_LAST_SEQ", 0);

                if (ds == null)
                    return null;

                return db.ConvertEntity<Joins.Apps.Models.Sully.API.Comment.ItemList>(ds.Tables[0]);
            }
        }
        /// <summary>
        /// 댓글 입력
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="message">메세지</param>
        /// <returns></returns>
        public static DbExecuteResult SetComment(string login_type, string uid, int pdseq, string content, string ip_address)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_API_CMT_SetCommentInsert", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = pdseq },
                    new SqlParameter { ParameterName = "@CMT_CONTENT", Value = content },
                    new SqlParameter { ParameterName = "@IP_ADDRESS", Value = ip_address },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 댓글 삭제
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="cmtseq">댓글번호</param>
        /// <param name="ip_address">등록IP</param>
        /// <returns></returns>
        public static DbExecuteResult SetDelete(string login_type, string uid, int cmtseq, string ip_address)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_API_CMT_SetCommentDeleteByUser", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@CMT_SEQ", Value = cmtseq },
                    new SqlParameter { ParameterName = "@IP_ADDRESS", Value = ip_address },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }
        /// <summary>
        /// 댓글 공감 입력
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="cmtseq">댓글번호</param>
        /// <param name="like_yn">공감등록/해제</param>
        /// <param name="ip_address">등록IP</param>
        /// <returns></returns>
        public static DbExecuteResult SetLike(string login_type, string uid, int cmtseq, string like_yn, string ip_address)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_API_CMT_SetCommentLikeByUser", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@CMT_SEQ", Value = cmtseq },
                    new SqlParameter { ParameterName = "@LIKE_YN", Value = like_yn },
                    new SqlParameter { ParameterName = "@IP_ADDRESS", Value = ip_address },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 댓글 신고
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="message">메세지</param>
        /// <returns></returns>
        public static DbExecuteResult SetReportByUser(string login_type, string uid, int cmtseq, string code, string content, string ip_address)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_API_CMT_SetReportInsert", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@CMT_SEQ", Value = cmtseq },
                    new SqlParameter { ParameterName = "@RE_CODE", Value = code },
                    new SqlParameter { ParameterName = "@RE_CONTENT", Value = content , SqlDbType = SqlDbType.NVarChar},
                    new SqlParameter { ParameterName = "@IP_ADDRESS", Value = ip_address },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }
    }
}