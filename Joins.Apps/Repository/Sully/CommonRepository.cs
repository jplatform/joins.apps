﻿using Joins.Apps.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Repository.Sully
{
    public class CommonRepository
    {
        /// <summary>
        /// 캐릭터 이미지 검색
        /// </summary>
        /// <param name="pgi">페이지</param>
        /// <param name="ps">페이지당 리스트 수</param>
        /// <param name="uyn">사용여부</param>
        /// <param name="tag">태그</param>
        /// <param name="r_sdt">등록일(시작)</param>
        /// <param name="r_edt">등록일(종료)</param>
        /// <param name="mid">등록자 아이디</param>
        /// <param name="character_type">이미지 종류 (1 : 일반 / 2 : 스토리썰링)</param>
        /// <param name="tot_cnt">총 갯수</param>
        /// <returns></returns>
        public static IEnumerable<Joins.Apps.Models.Sully.ImgData> GetCharacterImgList(int pgi, int ps, string uyn, string tag, string r_sdt, string r_edt, string mid, int? character_type, out int tot_cnt)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_PA_GetCharacterImgList", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@PageIndex", Value = pgi },
                    new SqlParameter { ParameterName = "@PageSize", Value = ps },
                    new SqlParameter { ParameterName = "@USED_YN", Value = uyn },
                    new SqlParameter { ParameterName = "@TAG", Value = tag },
                    new SqlParameter { ParameterName = "@REG_SDT", Value = r_sdt },
                    new SqlParameter { ParameterName = "@REG_EDT", Value = r_edt },
                    new SqlParameter { ParameterName = "@REG_ID", Value = mid },
                    new SqlParameter { ParameterName = "@CHARACTER_TYPE", Value = character_type }
                });

                if (ds == null)
                {
                    tot_cnt = 0;
                    return new List<Joins.Apps.Models.Sully.ImgData>();
                }

                tot_cnt = Convert.ToInt32(ds.Tables[0].Rows[0]["T_COUNT"]);
                return db.ConvertEntity<Joins.Apps.Models.Sully.ImgData>(ds.Tables[0], (x, row) => {
                    switch (x.CHARACTER_TYPE)
                    {
                        case 1:
                            x.CHARACTER_TYPE_NAME = "일반";
                            break;
                        case 2:
                            x.CHARACTER_TYPE_NAME = "스토리썰링";
                            break;
                    }
                });
            }
        }
        /// <summary>
        /// 캐릭터 등록
        /// </summary>
        /// <param name="imginfo">이미지 정보</param>
        /// <param name="characterType">캐릭터이미지 종류 (1 : 기본 / 2 : 스토리썰링)</param>
        /// <returns></returns>
        public static bool SetCharacterFileInfo(Joins.Apps.Models.Sully.ImgFileInfo imginfo, int characterType)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var result = db.ProcExecuteScalar("USP_PD_SetProductCharacterImg", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@IMG_URL", Value = imginfo.url },
                    new SqlParameter { ParameterName = "@IMG_TAG", Value = imginfo.tag },
                    new SqlParameter { ParameterName = "@Editor", Value = imginfo.editor },
                    new SqlParameter { ParameterName = "@CharacterType", Value = characterType },
                }, -1);

                return result > 0;
            }
        }
        /// <summary>
        /// 캐릭터 사용여부 변경
        /// </summary>
        /// <param name="seq">고유번호</param>
        /// <param name="yn">사용여부</param>
        /// <returns></returns>
        public static bool SetProductCharacterStatus(int seq, string yn)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var result = db.ProcExecuteNonQuery("USP_PD_SetParticleCharacterStatus", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@SEQ", Value = seq },
                    new SqlParameter { ParameterName = "@USED_YN", Value = yn }
                });

                return result > 0;
            }
        }

        public static bool SetProductImgStatus(int seq, string yn)
        {
            JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetJoinsNewsDataHelper();
            helper.ProcParamClear();
            helper.ProcParamAdd("@SEQ", seq);
            helper.ProcParamAdd("@USED_YN", yn);
            bool r = helper.ProcExecuteNonQuery("USP_PD_SetParticleImgStatus") > 0;
            helper.Close();
            return r;
        }
        public static bool SetImgFileInfo(Joins.Apps.Models.Sully.ImgFileInfo imginfo)
        {
            JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetJoinsNewsDataHelper();
            helper.ProcParamClear();
            helper.ProcParamAdd("@IMG_URL", imginfo.url);
            helper.ProcParamAdd("@IMG_SIZE", imginfo.size);
            helper.ProcParamAdd("@IMG_H", imginfo.h);
            helper.ProcParamAdd("@IMG_W", imginfo.w);
            helper.ProcParamAdd("@IMG_TAG", imginfo.tag);
            helper.ProcParamAdd("@IMG_DESC", imginfo.desc);
            helper.ProcParamAdd("@Editor", imginfo.editor);
            bool r = helper.ProcExecuteScalar("USP_PD_SetProductContentsImg", -1) > 0;
            helper.Close();
            return r;
        }
        public static IEnumerable<Joins.Apps.Models.Sully.ImgData> GetParticleImgList(int pgi, int ps, string uyn, string tag, string r_sdt, string r_edt, string mid, out int tot_cnt)
        {
            List<Joins.Apps.Models.Sully.ImgData> returnList = new List<Joins.Apps.Models.Sully.ImgData>();
            JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetJoinsNewsDataHelper();
            helper.ProcParamClear();
            helper.ProcParamAdd("@PageIndex", pgi);
            helper.ProcParamAdd("@PageSize", ps);
            helper.ProcParamAdd("@USED_YN", uyn);
            helper.ProcParamAdd("@TAG", tag);
            helper.ProcParamAdd("@REG_SDT", r_sdt);
            helper.ProcParamAdd("@REG_EDT", r_edt);
            helper.ProcParamAdd("@REG_ID", mid);
            DataSet ds = helper.ProcExecuteDataSet("USP_PA_GetParticleImgList");
            helper.Close();
            tot_cnt = 0;
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null)
            {
                int cnt = ds.Tables[0].Rows.Count;
                for (int tmpi = 0; tmpi < cnt; tmpi++)
                {
                    DataRow dr = ds.Tables[0].Rows[tmpi];
                    tot_cnt = JCube.AF.Util.Converts.ToInt32(dr["T_COUNT"], 0);
                    returnList.Add(new Joins.Apps.Models.Sully.ImgData
                    {
                        ROWNUM = JCube.AF.Util.Converts.ToInt32(dr["ROWNUM"], 0),
                        SEQ = JCube.AF.Util.Converts.ToInt32(dr["SEQ"], 0),
                        USED_YN = JCube.AF.Util.Converts.ToString(dr["USED_YN"], ""),
                        IMG_URL = JCube.AF.Util.Converts.ToString(dr["IMG_URL"], ""),
                        IMG_H = JCube.AF.Util.Converts.ToInt32(dr["IMG_H"], 0),
                        IMG_W = JCube.AF.Util.Converts.ToInt32(dr["IMG_W"], 0),
                        IMG_SIZE = JCube.AF.Util.Converts.ToInt32(dr["IMG_SIZE"], 0),
                        IMG_TAG = JCube.AF.Util.Converts.ToString(dr["IMG_TAG"], ""),
                        IMG_DESC = JCube.AF.Util.Converts.ToString(dr["IMG_DESC"], ""),
                        REG_ID = JCube.AF.Util.Converts.ToString(dr["REG_ID"], ""),
                        REG_DT = JCube.AF.Util.Converts.ToString(dr["REG_DT"], "")
                    });
                }
            }
            return returnList;
        }
        public static IEnumerable<Joins.Apps.Models.Sully.StickerData> GetStickerImgList(int pgi, int ps, string t, out int tot_cnt)
        {
            List<Joins.Apps.Models.Sully.StickerData> returnList = new List<Joins.Apps.Models.Sully.StickerData>();
            JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetJoinsNewsDataHelper();
            helper.ProcParamClear();
            helper.ProcParamAdd("@PageIndex", pgi);
            helper.ProcParamAdd("@PageSize", ps);
            helper.ProcParamAdd("@TAG", t);
            DataSet ds = helper.ProcExecuteDataSet("USP_PA_GetParticleStickerList");
            helper.Close();
            tot_cnt = 0;
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null)
            {
                int cnt = ds.Tables[0].Rows.Count;
                for (int tmpi = 0; tmpi < cnt; tmpi++)
                {
                    DataRow dr = ds.Tables[0].Rows[tmpi];
                    tot_cnt = JCube.AF.Util.Converts.ToInt32(dr["T_COUNT"], 0);
                    returnList.Add(new Joins.Apps.Models.Sully.StickerData { Sticker_Name = JCube.AF.Util.Converts.ToString(dr["IMG_TAG"], ""), Sticker_URL = JCube.AF.Util.Converts.ToString(dr["IMG_URL"], "") });
                }
            }
            return returnList;
        }

        #region [로그]
        public static bool SetLog(Joins.Apps.Models.Sully.LogParam param)
        {
            JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetJoinsNewsDataHelper();
            helper.ProcParamClear();
            helper.ProcParamAdd("@DEV_ID", param.devid);
            helper.ProcParamAdd("@SECTION", param.sec);
            helper.ProcParamAdd("@PD_SEQ", param.pseq);
            helper.ProcParamAdd("@SHARE_YN", param.share);
            helper.ProcParamAdd("@REG_IP", param.ip);
            helper.ProcParamAdd("@STEP_TYPE", param.type);
            helper.ProcParamAdd("@STEP_CNT", param.step);
            helper.ProcParamAdd("@REFERER", param.refer);

            bool r = helper.ProcExecuteScalar("USP_PD_SetLog", -1) > 0;
            helper.Close();
            return r;
        }
        #endregion
    }
}