﻿using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Common;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Joins.Apps.Repository.Sully
{
    public class AppRepository
    {
        /// <summary>
        /// 앱 업데이트 버전 정보를 가져옵니다.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<AppVersion> GetVersionInfo()
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_CMS_APP_VERSION");

                if (ds == null)
                    return null;

                return db.ConvertEntity<AppVersion>(ds.Tables[0]);
            }
        }

        /// <summary>
        /// 앱 버전 정보를 반영합니다.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static AppVersion SetVersionInfo(AppVersion version)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_CMS_APP_UPDATE", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@OS_TYPE", Value = version.OS },
                    new SqlParameter { ParameterName = "@VERSION", Value = version.Version },
                    new SqlParameter { ParameterName = "@REQUIRED_YN", Value = version.RequiredYN }
                });

                if (ds == null)
                    return null;

                return db.ConvertEntity<AppVersion>(ds.Tables[0].Rows[0]);
            }
        }
    }
}
