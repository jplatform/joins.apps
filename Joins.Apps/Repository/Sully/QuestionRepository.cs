﻿using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Common;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Joins.Apps.Repository.Sully
{
    public class QuestionRepository
    {
        /// <summary>
        /// 회원의 문의글을 읽음처리합니다.
        /// </summary>
        /// <param name="mem_seq"></param>
        /// <returns></returns>
        public static bool SetQuestionReadFlag(int mem_seq)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var result = db.ProcExecuteNonQuery("USP_SetQuestionRead_v2", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@MEM_SEQ", Value = mem_seq }
                });

                return result >= 1;
            }
        }
        /// <summary>
        /// 질문글 목록을 검색합니다.
        /// </summary>
        /// <param name="page">페이지</param>
        /// <param name="page_size">페이지당 리스트 수</param>
        /// <param name="total_count">총 리스트 수</param>
        /// <param name="total_page">총 페이지 수</param>
        /// <returns></returns>
        public static IEnumerable<QuestionList> GetQuestionList(int page, int page_size, out int total_count, out int total_page)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_GetQuestionList_v2", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@PAGE", Value = page },
                    new SqlParameter { ParameterName = "@PAGE_SIZE", Value = page_size },
                    new SqlParameter { ParameterName = "@TOTAL_COUNT", SqlDbType = SqlDbType.Int, Value = 0, Direction = ParameterDirection.Output },
                    new SqlParameter { ParameterName = "@TOTAL_PAGE", SqlDbType = SqlDbType.Int, Value = 0, Direction = ParameterDirection.Output }
                });

                total_count = db.GetOutputParameterValue<int>("@TOTAL_COUNT", 0);
                total_page = db.GetOutputParameterValue<int>("@TOTAL_PAGE", 0);

                if (ds == null)
                    return null;

                return db.ConvertEntity<QuestionList>(ds.Tables[0]);
            }
        }

        /// <summary>
        /// 질문글 상세목록을 가져옵니다.
        /// </summary>
        /// <param name="mem_seq">회원 고유번호</param>
        /// <param name="page">페이지</param>
        /// <param name="page_size">페이지당 리스트 수</param>
        /// <param name="total_count">총 리스트 수</param>
        /// <param name="total_page">총 페이지 수</param>
        /// <returns></returns>
        public static QuestionDetail GetQuestionDetail(int mem_seq, int page, int page_size, out int total_count, out int total_page)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_GetQuestionDetail_v2", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@MEM_SEQ", Value = mem_seq },
                    new SqlParameter { ParameterName = "@PAGE", Value = page },
                    new SqlParameter { ParameterName = "@PAGE_SIZE", Value = page_size },
                    new SqlParameter { ParameterName = "@TOTAL_COUNT", SqlDbType = SqlDbType.Int, Value = 0, Direction = ParameterDirection.Output },
                    new SqlParameter { ParameterName = "@TOTAL_PAGE", SqlDbType = SqlDbType.Int, Value = 0, Direction = ParameterDirection.Output }
                });

                total_count = db.GetOutputParameterValue<int>("@TOTAL_COUNT", 0);
                total_page = db.GetOutputParameterValue<int>("@TOTAL_PAGE", 0);

                if (ds == null)
                    return null;

                var reply = db.ConvertEntity<QuestionDetailReply>(ds.Tables[2]);
                return new QuestionDetail
                {
                    MemberInfo = db.ConvertEntity<QuestionDetailMember>(ds.Tables[0].Rows[0]),
                    Items = db.ConvertEntity<QuestionDetailItem>(ds.Tables[1], (r, row) =>
                    {
                        r.Reply = reply.Where(x => x.QuestionSeq == r.Seq).OrderBy(x => x.Seq);
                    })
                };
            }
        }

        /// <summary>
        /// 답변을 작성합니다.
        /// </summary>
        /// <param name="question_seq">문의 글 번호</param>
        /// <param name="content">내용</param>
        /// <param name="link_seq">연관 프로덕트 고유번호</param>
        /// <param name="result_msg">처리여부 결과</param>
        /// <returns></returns>
        public static bool SetQuestionReply(int question_seq, int reply_type, string reply_content, int? link_internal_seq, string link_external_url, string link_external_domain, string link_external_thumbnail, string link_external_title, out string result_msg)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_SetQuestionReplyInsert_v2", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@QUESTION_SEQ", Value = question_seq },
                    new SqlParameter { ParameterName = "@REPLY_TYPE", Value = reply_type },
                    new SqlParameter { ParameterName = "@REPLY_CONTENT", Value = reply_content },
                    new SqlParameter { ParameterName = "@LINK_INTERNAL_SEQ", Value = link_internal_seq },
                    new SqlParameter { ParameterName = "@LINK_EXTERNAL_URL", Value = link_external_url },
                    new SqlParameter { ParameterName = "@LINK_EXTERNAL_DOMAIN", Value = link_external_domain },
                    new SqlParameter { ParameterName = "@LINK_EXTERNAL_THUMBNAIL", Value = link_external_thumbnail },
                    new SqlParameter { ParameterName = "@LINK_EXTERNAL_TITLE", Value = link_external_title },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                result_msg = db.GetOutputParameterValue("@RESULT_MSG", string.Empty);

                return db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1;
            }
        }

        /// <summary>
        /// 답변을 삭제합니다.
        /// </summary>
        /// <param name="question_seq">문의 글 번호</param>
        /// <param name="reply_seq">답변 글 번호</param>
        /// <param name="result_msg">처리여부 결과</param>
        /// <returns></returns>
        public static bool RemoveQuestionReply(int question_seq, int reply_seq, out string result_msg)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_SetQuestionReplyRemove_v2", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@QUESTION_SEQ", Value = question_seq },
                    new SqlParameter { ParameterName = "@REPLY_SEQ", Value = reply_seq },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                result_msg = db.GetOutputParameterValue("@RESULT_MSG", string.Empty);

                return db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1;
            }
        }

        /// <summary>
        /// 설정한 답변글을 푸시로 발송합니다.
        /// </summary>
        /// <param name="question_seq">문의글 고유번호</param>
        /// <param name="answer_seq">푸시로 발송할 답변 고유번호</param>
        /// <param name="result_msg">처리 결과</param>
        /// <returns></returns>
        public static bool SetQuestionPush(int question_seq, int answer_seq, out string result_msg)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_SetQuestionReplyPush_v2", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@QUESTION_SEQ", Value = question_seq },
                    new SqlParameter { ParameterName = "@ANSWER_SEQ", Value = answer_seq },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                result_msg = db.GetOutputParameterValue("@RESULT_MSG", string.Empty);

                return db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1;
            }
        }
    }
}
