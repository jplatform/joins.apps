﻿using Joins.Apps.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Repository.Sully
{
    public class LogRepository
    {
        public static void SetProductViewLog(int pd_seq, string ip_address, string http_agent, string referer_domain, string referer_url, string login_type = null, string uid = null)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_PD_SetAccessLog", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = pd_seq },
                    new SqlParameter { ParameterName = "@IP_ADDRESS", Value = ip_address },
                    new SqlParameter { ParameterName = "@HTTP_AGENT", Value = http_agent },
                    new SqlParameter { ParameterName = "@REFERER_DOMAIN", Value = referer_domain },
                    new SqlParameter { ParameterName = "@REFERER_URL", Value = referer_url },
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid }
                });
            }
        }
    }
}
