﻿using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Common;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Joins.Apps.Repository.Sully
{
    public class CommentRepository
    {
        #region [댓글 관리]

        public static IEnumerable<Joins.Apps.Models.Sully.CommentList> GetCommentList(Joins.Apps.Models.Sully.CommentListParam param, out int tot_cnt)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_CMT_GetCommentList", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@PageIndex", Value = param.pgi },
                    new SqlParameter { ParameterName = "@PageSize", Value = param.ps },
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = param.pdseq },
                    new SqlParameter { ParameterName = "@MEM_SEQ", Value = param.mseq },
                    new SqlParameter { ParameterName = "@CMT_STATUS", Value = param.status },
                    new SqlParameter { ParameterName = "@SEARCH", Value = param.search },
                    new SqlParameter { ParameterName = "@SDT", Value = param.sdt },
                    new SqlParameter { ParameterName = "@EDT", Value = param.edt },
                    new SqlParameter { ParameterName = "@ORDERBY", Value = param.order },
                    new SqlParameter { ParameterName = "@TOTAL_COUNT", Size = 4, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output },
                });
                tot_cnt = db.GetOutputParameterValue("@TOTAL_COUNT", 0);

                if (ds == null)
                    return null;

                return db.ConvertEntity<CommentList>(ds.Tables[0]);
            }
        }

        /// <summary>
        /// 댓글 상태 변경
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="cmtseq">댓글번호</param>
        /// <param name="reg_ip">등록IP</param>
        /// <returns></returns>
        public static DbExecuteResult SetCommetStatus(int cmtseq, string status)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_CMT_SetCommentStatus", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@CMT_SEQ", Value = cmtseq },
                    new SqlParameter { ParameterName = "@CMT_STATUS", Value = status },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });
                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        #endregion

        #region [사용자,IP 차단관리]

        /// <summary>
        /// 차단 처리
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="cmtseq">댓글번호</param>
        /// <param name="reg_ip">등록IP</param>
        /// <returns></returns>
        public static DbExecuteResult SetBanned(int mseq, string ip, int days, string desc)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_CMT_SetCommentBanned", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@MEM_SEQ", Value = mseq },
                    new SqlParameter { ParameterName = "@IP", Value = ip },
                    new SqlParameter { ParameterName = "@BND_DAYS", Value = days },
                    new SqlParameter { ParameterName = "@BND_DESC", Value = desc },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });
                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 댓글 사용자,ip 차단 리스트
        /// </summary>
        /// <param name="param"></param>
        /// <param name="tot_cnt"></param>
        /// <returns></returns>
        public static IEnumerable<Joins.Apps.Models.Sully.BannedList> GetBannedList(Joins.Apps.Models.Sully.BannedListParam param, out int tot_cnt)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_CMT_GetBannedList", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@PageIndex", Value = param.pgi },
                    new SqlParameter { ParameterName = "@PageSize", Value = param.ps },
                    new SqlParameter { ParameterName = "@BND_YN", Value = param.status },
                    new SqlParameter { ParameterName = "@SEARCH", Value = param.search },
                    new SqlParameter { ParameterName = "@SDT", Value = param.sdt },
                    new SqlParameter { ParameterName = "@EDT", Value = param.edt },
                    new SqlParameter { ParameterName = "@TOTAL_COUNT", Size = 4, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output },
                });
                tot_cnt = db.GetOutputParameterValue("@TOTAL_COUNT", 0);

                if (ds == null)
                    return null;

                return db.ConvertEntity<BannedList>(ds.Tables[0]);
            }
        }

        /// <summary>
        /// 댓글 사용자,IP 상태 변경
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="cmtseq">댓글번호</param>
        /// <param name="reg_ip">등록IP</param>
        /// <returns></returns>
        public static DbExecuteResult SetBannedStatus(int seq, string status)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_CMT_SetBannedStatus", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@SEQ", Value = seq },
                    new SqlParameter { ParameterName = "@BND_YN", Value = status },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });
                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        #endregion

        /// <summary>
        /// 댓글 신고 목록을 검색합니다.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="page_size"></param>
        /// <param name="total_count"></param>
        /// <returns></returns>
        public static IEnumerable<CommentReportList> GetReportList(CommentReportListParam param)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_CMT_GetCommentReportList", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@PAGE", Value = param.pgi },
                    new SqlParameter { ParameterName = "@PAGE_SIZE", Value = param.ps },
                    new SqlParameter { ParameterName = "@TOTAL_COUNT", Size = 4, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output },
                });

                param.total_count = db.GetOutputParameterValue("@TOTAL_COUNT", 0);

                if (ds == null)
                    return null;

                return db.ConvertEntity<CommentReportList>(ds.Tables[0]);
            }
        }

        /// <summary>
        /// 신고 댓글 상세정보를 검색합니다.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="page_size"></param>
        /// <param name="cmt_seq"></param>
        /// <param name="total_count"></param>
        /// <returns></returns>
        public static CommentReportInfo GetReportInfo(int page, int page_size, int cmt_seq, out int total_count)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_CMT_GetCommentReportCommentInfo", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@CMT_SEQ", Value = cmt_seq },
                    new SqlParameter { ParameterName = "@PAGE", Value = page },
                    new SqlParameter { ParameterName = "@PAGE_SIZE", Value = page_size },
                    new SqlParameter { ParameterName = "@TOTAL_COUNT", Size = 4, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output },
                });

                total_count = db.GetOutputParameterValue("@TOTAL_COUNT", 0);

                if (ds == null)
                    return null;

                return new CommentReportInfo
                {
                    ReportCommentInfo = db.ConvertEntity<CommentReportInfo.Comment>(ds.Tables[0].Rows[0]),
                    ReportList = db.ConvertEntity<CommentReportInfo.Report>(ds.Tables[1])
                };
            }
        }

        /// <summary>
        /// 댓글 신고를 반려합니다.
        /// </summary>
        /// <param name="cmt_seq"></param>
        public static void SetReportReject(int cmt_seq)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_CMT_GetCommentReportCommentReject", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@CMT_SEQ", Value = cmt_seq }
                });
            }
        }

        /// <summary>
        /// 댓글 신고를 수락합니다.
        /// </summary>
        /// <param name="cmt_seq"></param>
        public static void SetReportAccept(int cmt_seq)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_CMT_GetCommentReportCommentAccept", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@CMT_SEQ", Value = cmt_seq }
                });
            }
        }
    }
}