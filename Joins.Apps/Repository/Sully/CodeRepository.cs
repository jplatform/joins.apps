﻿using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Common;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Joins.Apps.Repository.Sully
{
    public class CodeRepository
    {
        /// <summary>
        /// 사용유무와 관계없이 공통 코드 목록을 검색합니다.
        /// </summary>
        /// <param name="group_cd">검색할 그룹 코드명</param>
        /// <returns></returns>
        public static IEnumerable<CommonCode> GetAllCategoryList(string group_cd = null)
        {
            return GetCategoryList(false, group_cd);
        }

        /// <summary>
        /// 사용중인 공통 코드 목록을 검색합니다.
        /// </summary>
        /// <param name="group_cd">검색할 그룹 코드명</param>
        /// <returns></returns>
        public static IEnumerable<CommonCode> GetUseCategoryList(string group_cd = null)
        {
            return GetCategoryList(true, group_cd);
        }

        /// <summary>
        /// 공통 코드 목록을 검색합니다.
        /// </summary>
        /// <param name="search_use">사용중인 코드만 검색할지 여부</param>
        /// <param name="group_cd">검색할 그룹 코드명</param>
        /// <returns></returns>
        public static IEnumerable<CommonCode> GetCategoryList(bool search_use, string group_cd = null)
        {
            using(var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_GetCodeList", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@SEARCH_ALL", Value = search_use ? "N" : "Y" },
                    new SqlParameter { ParameterName = "@SEARCH_GRP_CD", Value = group_cd }
                });

                if (ds == null)
                    return null;

                return db.ConvertEntity<CommonCode>(ds.Tables[0]);
            }
        }

        /// <summary>
        /// 공통코드 정보를 가져옵니다.
        /// </summary>
        /// <param name="group">그룹코드</param>
        /// <param name="code">코드</param>
        /// <returns></returns>
        public static CommonCode GetCodeInfo(string group, string code)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_GetCodeInfo", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@GROUP_CD", Value = group },
                    new SqlParameter { ParameterName = "@CD", Value = code }
                });

                if (ds == null)
                    return null;

                return db.ConvertEntity<CommonCode>(ds.Tables[0].Rows[0]);
            }
        }
    }
}
