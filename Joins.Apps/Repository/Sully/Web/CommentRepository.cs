﻿using Joins.Apps.Models.Sully.Web;
using Joins.Apps.Repository.Common;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.SqlClient;

namespace Joins.Apps.Repository.Sully.Web
{
    public class CommentRepository
    {
        /// <summary>
        /// 코멘트 목록을 검색합니다.
        /// </summary>
        /// <param name="page">페이지</param>
        /// <param name="page_size">페이지당 리스트 수</param>
        /// <param name="pd_seq">프로덕트 고유번호</param>
        /// <param name="comment_seq">한번에 불러올 코멘트 고유번호</param>
        /// <param name="login_type">로그인 타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="order_by">정렬방식</param>
        /// <param name="total_count">총 리스트 수</param>
        /// <param name="total_page">총 페이지 수</param>
        /// <param name="current_page">탐색 페이지</param>
        /// <returns></returns>
        public static IEnumerable<Comment> GetCommentList(int? page, int page_size, int pd_seq, int? comment_seq, string login_type, string uid, string order_by, out int total_count, out int total_page, out int current_page)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_USR_GetCommentList", new[] {
                    new SqlParameter { ParameterName = "@PAGE", Value = page },
                    new SqlParameter { ParameterName = "@PAGE_SIZE", Value = page_size },
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = pd_seq },
                    new SqlParameter { ParameterName = "@COMMENT_SEQ", Value = comment_seq },
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@ORDER_BY", Value = order_by },
                    new SqlParameter { ParameterName = "@TOTAL_COUNT", Size = 4, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output },
                    new SqlParameter { ParameterName = "@TOTAL_PAGE", Size = 4, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output },
                    new SqlParameter { ParameterName = "@CURRENT_PAGE", Size = 4, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output }
                });

                total_count = db.GetOutputParameterValue("@TOTAL_COUNT", 0);
                total_page = db.GetOutputParameterValue("@TOTAL_PAGE", 0);
                current_page = db.GetOutputParameterValue("@CURRENT_PAGE", 0);


                if (ds == null)
                    return null;

                return db.ConvertEntity<Comment>(ds.Tables[0]);
            }
        }

        /// <summary>
        /// 댓글을 입력합니다.
        /// </summary>
        /// <param name="login_type">로그인 타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="pd_seq">프로덕트 고유번호</param>
        /// <param name="content">댓글</param>
        /// <param name="ip_address">아이피 주소</param>
        /// <returns></returns>
        public static DbExecuteResult SetCommentInsert(string login_type, string uid, int pd_seq, string content, string ip_address)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_USR_SetCommentInsert", new[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = pd_seq},
                    new SqlParameter { ParameterName = "@CONTENT", Value = content },
                    new SqlParameter { ParameterName = "@IP_ADDRESS", Value = ip_address },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 댓글을 삭제합니다.
        /// </summary>
        /// <param name="login_type">로그인 타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="pd_seq">프로덕트 고유번호</param>
        /// <param name="comment_seq">댓글 고유번호</param>
        /// <returns></returns>
        public static DbExecuteResult SetCommentRemove(string login_type, string uid, int pd_seq, int comment_seq)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_USR_SetCommentDeleteByUser", new[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = pd_seq},
                    new SqlParameter { ParameterName = "@CMT_SEQ", Value = comment_seq },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 댓글 공감/공감취소 처리 합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="comment_seq">댓글번호</param>
        /// <param name="like_yn">공감등록/해제</param>
        /// <param name="ip_address">등록IP</param>
        /// <param name="like_cnt">처리 후 총 공감 수</param>
        /// <returns></returns>
        public static DbExecuteResult SetCommentLikeToggle(string login_type, string uid, int comment_seq, string like_yn, string ip_address, out int like_cnt)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_USR_SetCommentLikeByUser", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@COMMENT_SEQ", Value = comment_seq },
                    new SqlParameter { ParameterName = "@LIKE_YN", Value = like_yn },
                    new SqlParameter { ParameterName = "@IP_ADDRESS", Value = ip_address },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@LIKE_CNT", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  }
                });

                like_cnt = db.GetOutputParameterValue("@LIKE_CNT", 0);

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 댓글을 신고합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="comment_seq">댓글번호</param>
        /// <param name="code">신고 종류 코드</param>
        /// <param name="msg">신고내용</param>
        /// <param name="ip_address">아이피 주소</param>
        /// <returns></returns>
        public static DbExecuteResult SetReportByUser(string login_type, string uid, int comment_seq, string code, string msg, string ip_address)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_USR_SetReportInsert", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@COMMENT_SEQ", Value = comment_seq },
                    new SqlParameter { ParameterName = "@REPORT_CODE", Value = code },
                    new SqlParameter { ParameterName = "@REPORT_MSG", Value = msg },
                    new SqlParameter { ParameterName = "@IP_ADDRESS", Value = ip_address },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }
    }
}
