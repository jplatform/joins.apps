﻿using Joins.Apps.Models.Sully.Web;
using Joins.Apps.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Repository.Sully.Web
{
    public class QuestionRepository
    {
        /// <summary>
        /// 문의/제보 목록을 검색합니다.
        /// </summary>
        /// <param name="login_type">로그인 타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="page">페이지</param>
        /// <param name="page_size">페이지당 리스트 수</param>
        /// <param name="target_seq">한번에 불러올 문의/제보글 고유번호</param>
        /// <param name="total_count">총 리스트 수</param>
        /// <param name="total_page">총 페이지 수</param>
        /// <param name="current_page">탐색 페이지</param>
        /// <returns></returns>
        public static IEnumerable<Question> GetQuestionList (string login_type, string uid, int? page, int page_size, int? target_seq, out int total_count, out int total_page, out int current_page)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_USR_GetQuestionList", new[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@PAGE", Value = page },
                    new SqlParameter { ParameterName = "@PAGE_SIZE", Value = page_size },
                    new SqlParameter { ParameterName = "@SEQ", Value = target_seq },
                    new SqlParameter { ParameterName = "@TOTAL_COUNT", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output },
                    new SqlParameter { ParameterName = "@TOTAL_PAGE", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output },
                    new SqlParameter { ParameterName = "@CURRENT_PAGE", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output }
                });

                total_count = db.GetOutputParameterValue("@TOTAL_COUNT", 0);
                total_page = db.GetOutputParameterValue("@TOTAL_PAGE", 0);
                current_page = db.GetOutputParameterValue("@CURRENT_PAGE", 0);

                if (ds == null)
                    return null;

                return db.ConvertEntity<Question>(ds.Tables[0]);
            }
        }

        /// <summary>
        /// 문의글을 작성합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="message">메세지</param>
        /// <returns></returns>
        public static DbExecuteResult SetReply(string login_type, string uid, string message)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_USR_SetQuestionInsert", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@MESSAGE", Value = message },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }
    }
}
