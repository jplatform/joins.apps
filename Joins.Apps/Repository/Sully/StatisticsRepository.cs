﻿using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Repository.Sully
{
    public class StatisticsRepository
    {
        public static IEnumerable<ProductCornerViewCount_Statistics> GetProductCornerViewCount()
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_STAT_GetProductCornerViewCount");

                if (ds == null)
                    return null;

                return db.ConvertEntity<ProductCornerViewCount_Statistics>(ds.Tables[0]);
            }
        }

        public static ProductQuestionAnswerCount_Statistics GetQuestionAnswerCount(int pd_seq, DateTime search_date)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_STAT_GetQuestionAnswerCount", new[] {
                    new SqlParameter { ParameterName = "@PD_SEQ", Value = pd_seq },
                    new SqlParameter { ParameterName = "@SEARCH_DATE", Value = search_date }
                });

                if (ds == null)
                    return null;

                return new ProductQuestionAnswerCount_Statistics
                {
                    ProductInfo = db.ConvertEntity<ProductQuestionAnswerCount_Statistics.Product>(ds.Tables[0].Rows[0]),
                    AnswerInfo = db.ConvertEntity<ProductQuestionAnswerCount_Statistics.AnswerCount>(ds.Tables[1])
                };
            }
        }
    }
}
