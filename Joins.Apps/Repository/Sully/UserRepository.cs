﻿using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Common;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Joins.Apps.Repository.Sully
{
    public class UserRepository
    {
        /// <summary>
        /// 회원정보를 셋팅하고 정보를 반환합니다
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="email">이메일 주소</param>
        /// <param name="name">SNS 이름</param>
        /// <param name="profile_url">프로필 이미지 주소</param>
        /// <param name="ip_address">아이피 주소</param>
        /// <returns></returns>
        public static DbExecuteResult SetLoginUserInfo(string login_type, string uid, string email, string name, string profile_url, string ip_address)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_WEB_SetLoginInfo", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@EMAIL", Value = email },
                    new SqlParameter { ParameterName = "@NAME", Value = name },
                    new SqlParameter { ParameterName = "@PROFILE_URL", Value = profile_url},
                    new SqlParameter { ParameterName = "@IP_ADDRESS", Value = ip_address },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 회원정보를 검색합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <returns></returns>
        public static LoginUserInfo GetUserInfo(string login_type, string uid)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_WEB_GetUserInfo", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid }
                });

                if (ds == null)
                    return null;

                var result = db.ConvertEntity<LoginUserInfo>(ds.Tables[0].Rows[0]);
                result.Category = db.ConvertEntity<LoginUserInfo.UserCategory>(ds.Tables[1]);

                return result;
            }
        }

        /// <summary>
        /// 닉네임을 설정합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="nick_name">설정할 닉네임</param>
        /// <returns></returns>
        public static DbExecuteResult SetUserNickName(string login_type, string uid, string nick_name)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_USR_SetNickname", new[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@NICK_NAME", Value = nick_name },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 닉네임 유효성 검증
        /// </summary>
        /// <param name="nick_name"></param>
        /// <returns></returns>
        public static DbExecuteResult GetValidateUserNickName(string nick_name)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_USR_ValidateNickname", new[] {
                    new SqlParameter { ParameterName = "@NICK_NAME", Value = nick_name },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 회원가입시 입력한 정보를 설정합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="gender">성별</param>
        /// <param name="age">연령대</param>
        /// <param name="category_xml">관심카테고리</param>
        /// <returns></returns>
        public static DbExecuteResult SetUserRegistInfo(string login_type, string uid, string gender, int? age, string category_xml)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_USR_SetRegistOptionalInfo", new[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@GENDER", Value = gender },
                    new SqlParameter { ParameterName = "@AGE", Value = age },
                    new SqlParameter { ParameterName = "@CATEGORY_XML", Value = category_xml },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 회원정보를 수정합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="profile_image">새 프로필 이미지 경로</param>
        /// <param name="nick_name">새 닉네임</param>
        /// <param name="gender">새 성별</param>
        /// <param name="age">새 연령대</param>
        /// <param name="category_xml">새 관심사</param>
        /// <returns></returns>
        public static DbExecuteResult SetUserModifyInfo(string login_type, string uid, string profile_image, string nick_name, string gender, int? age, string category_xml)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                db.ProcExecuteNonQuery("USP_USR_SetUserModifyInfo", new[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@PROFILE_IMAGE", Value = profile_image },
                    new SqlParameter { ParameterName = "@NICK_NAME", Value = nick_name },
                    new SqlParameter { ParameterName = "@GENDER", Value = gender },
                    new SqlParameter { ParameterName = "@AGE", Value = age },
                    new SqlParameter { ParameterName = "@CATEGORY_XML", Value = category_xml },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 탈퇴처리 합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <returns></returns>
        public static DbExecuteResult SetUserWithdraw(string login_type, string uid)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var result = db.ProcExecuteNonQuery("USP_USR_SetUserWithdraw", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@RESULT_CD", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output  },
                    new SqlParameter { ParameterName = "@RESULT_MSG", SqlDbType = SqlDbType.NVarChar, Size = 100, Direction = ParameterDirection.Output  }
                });

                return new DbExecuteResult
                {
                    IsSuccess = db.GetOutputParameterValue<int>("@RESULT_CD", -1) == 1,
                    ResultCode = db.GetOutputParameterValue<int>("@RESULT_CD", -1),
                    ResultMessage = db.GetOutputParameterValue("@RESULT_MSG", "처리중 오류가 발생하였습니다.")
                };
            }
        }

        /// <summary>
        /// 알림 목록을 검색합니다.
        /// </summary>
        /// <param name="login_type">로그인타입</param>
        /// <param name="uid">고유아이디</param>
        /// <param name="page">페이지</param>
        /// <param name="page_size">페이지당 리스트 수</param>
        /// <param name="total_count">총 리스트 수</param>
        /// <param name="total_page">총 페이지 수</param>
        /// <returns></returns>
        public static IEnumerable<Notification> GetNotificationList(string login_type, string uid, int page, int page_size, out int total_count, out int total_page)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_USR_GetNotiMessageList", new[] {
                    new SqlParameter { ParameterName = "@PAGE", Value = page },
                    new SqlParameter { ParameterName = "@PAGE_SIZE", Value = page_size },
                    new SqlParameter { ParameterName = "@LOGIN_TYPE", Value = login_type },
                    new SqlParameter { ParameterName = "@UID", Value = uid },
                    new SqlParameter { ParameterName = "@TOTAL_COUNT", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output },
                    new SqlParameter { ParameterName = "@TOTAL_PAGE", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output }
                });

                total_count = db.GetOutputParameterValue("@TOTAL_COUNT", 0);
                total_page = db.GetOutputParameterValue("@TOTAL_PAGE", 0);

                if (ds == null)
                    return null;

                return db.ConvertEntity<Notification>(ds.Tables[0]);
            }
        }
    }
}
