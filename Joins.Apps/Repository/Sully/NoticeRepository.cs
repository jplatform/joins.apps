﻿using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Common;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Joins.Apps.Repository.Sully
{
    public class NoticeRepository
    {
        /// <summary>
        /// 프론트용 공지사항 목록을 검색합니다.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Notice> GetServiceList()
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_SC_GetNoticeList");

                if (ds == null)
                    return null;

                return db.ConvertEntity<Notice>(ds.Tables[0]);
            }
        }

        /// <summary>
        /// 공지사항 목록을 검색합니다.
        /// </summary>
        /// <param name="page">페이지 번호</param>
        /// <param name="page_size">페이지당 리스트 수</param>
        /// <param name="search_type">검색종류 (1 : 제목 / 2 : 내용)</param>
        /// <param name="search_value">검색어</param>
        /// <param name="use_yn">삭제여부(Y/N)</param>
        /// <param name="total_page">총 페이지 수</param>
        /// <param name="total_count">총 리스트 수</param>
        /// <returns></returns>
        public static IEnumerable<NoticeList> GetList(int page, int page_size, string search_type, string search_value, string use_yn, out int total_page, out int total_count)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_GetNoticeList", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@PAGE", Value = page },
                    new SqlParameter { ParameterName = "@PAGE_SIZE", Value = page_size },
                    new SqlParameter { ParameterName = "@SEARCH_TYPE", Value = search_type },
                    new SqlParameter { ParameterName = "@SEARCH_TEXT", Value = search_value },
                    new SqlParameter { ParameterName = "@USE_YN", Value = use_yn },
                    new SqlParameter { ParameterName = "@TOTAL_COUNT", Size = 4, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output },
                    new SqlParameter { ParameterName = "@TOTAL_PAGE", Size = 4, SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output }
                });

                total_page = db.GetOutputParameterValue("@TOTAL_PAGE", 0);
                total_count = db.GetOutputParameterValue("@TOTAL_COUNT", 0);

                if (ds == null)
                    return null;

                return db.ConvertEntity<NoticeList>(ds.Tables[0]);
            }
        }

        /// <summary>
        /// 공지사항 상세정보를 검색합니다.
        /// </summary>
        /// <param name="seq">고유번호</param>
        /// <returns></returns>
        public static NoticeDetail GetDetail(int seq)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var ds = db.ProcExecuteDataSet("USP_GetNoticeDetail", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@SEQ", Value = seq }
                });
                
                if (ds == null)
                    return null;

                return db.ConvertEntity<NoticeDetail>(ds.Tables[0].Rows[0]);
            }
        }

        /// <summary>
        /// 공지사항 정보를 수정하거나 추가합니다.
        /// </summary>
        /// <param name="seq">글 번호</param>
        /// <param name="title">제목</param>
        /// <param name="content">내용</param>
        /// <param name="use_yn">노출여부</param>
        /// <param name="push_yn">푸시 발송여부</param>
        /// <param name="push_title">푸시용 제목</param>
        /// <returns></returns>
        public static bool SetDetail(int? seq, string title, string content, string use_yn, string push_yn, string push_title)
        {
            using (var db = GLOBAL.GetJoinsNewsDataHelper())
            {
                var result = db.ProcExecuteNonQuery("USP_GetNoticeSet", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@SEQ", Value = seq },
                    new SqlParameter { ParameterName = "@TITLE", Value = title },
                    new SqlParameter { ParameterName = "@CONTENT", Value = content },
                    new SqlParameter { ParameterName = "@USE_YN", Value = use_yn },
                    new SqlParameter { ParameterName = "@PUSH_YN", Value = push_yn },
                    new SqlParameter { ParameterName = "@PUSH_TITLE", Value = push_title }
                });

                return result == 1;
            }
        }
    }
}
