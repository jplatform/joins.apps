﻿using Joins.Apps.Repository.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Repository.Sully {
    public class PushRepository {
        #region [푸시 관리]
        public static IEnumerable<Joins.Apps.Models.Sully.Push> GetPushList(Models.Sully.PushParam param, out int total_count) {
            using (var db = GLOBAL.GetJoinsNewsDataHelper()) {
                var ds = db.ProcExecuteDataSet("USP_PH_GetPushList", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@PAGE", Value = param.pgi },
                    new SqlParameter { ParameterName = "@PAGE_SIZE", Value = param.ps },
                    new SqlParameter { ParameterName = "@SEQ", Value =  param.seq },
                     new SqlParameter { ParameterName = "@P_TYPE", Value =  param.type },
                    new SqlParameter { ParameterName = "@P_STATUS", Value =  param.status },
                    new SqlParameter { ParameterName = "@ORDERBY", Value =  param.order },
                    new SqlParameter { ParameterName = "@SDT", Value =  param.sdt },
                    new SqlParameter { ParameterName = "@EDT", Value =  param.edt },
                    new SqlParameter { ParameterName = "@TOTAL_COUNT", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output },
                    new SqlParameter { ParameterName = "@TOTAL_PAGE", SqlDbType = SqlDbType.Int, Size = 4, Direction = ParameterDirection.Output }
                });

                total_count = db.GetOutputParameterValue("@TOTAL_COUNT", 0);
                //total_page = db.GetOutputParameterValue("@TOTAL_PAGE", 0);

                if (ds == null)
                    return null;
                return db.ConvertEntity<Joins.Apps.Models.Sully.Push>(ds.Tables[0]);

            }

        }
        public static bool SetPushEdit(Joins.Apps.Models.Sully.PushEditParam param) {
            using (var db = GLOBAL.GetJoinsNewsDataHelper()) {
                bool bResult = db.ProcExecuteNonQuery("USP_PH_SetPushEdit", new IDbDataParameter[] {
                    new SqlParameter { ParameterName = "@SEQ", Value = param.seq },
                    new SqlParameter { ParameterName = "@P_STATUS", Value = param.p_status },
                    new SqlParameter { ParameterName = "@P_TYPE", Value = param.p_type },
                    new SqlParameter { ParameterName = "@P_TITLE", Value = param.p_title , SqlDbType = SqlDbType.NVarChar},
                    new SqlParameter { ParameterName = "@P_CONTENT", Value = param.p_content, SqlDbType = SqlDbType.NVarChar },
                    new SqlParameter { ParameterName = "@P_IMG", Value = param.p_img },
                    new SqlParameter { ParameterName = "@PUSH_DT", Value = param.push_dt },
                    new SqlParameter { ParameterName = "@P_OS", Value = param.p_os },

                }) >0 ;
                return bResult;
            }
        }
        #endregion
    }
}