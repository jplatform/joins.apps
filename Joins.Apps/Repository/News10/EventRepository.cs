﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Repository.News10 {
   public class EventRepository {
        #region
        public static List<Joins.Apps.Models.News10.EventData> GetEvent(int pgi = 1, int pgs = 5, string syn = "") {
            List<Joins.Apps.Models.News10.EventData> returnList = new List<Joins.Apps.Models.News10.EventData>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@PageIndex", pgi);
                helper.ProcParamAdd("@PageSize", pgs);
                helper.ProcParamAdd("@USED_YN", syn);
                DataSet ds = helper.ProcExecuteDataSet("UPU_NEWS10_EVENT_LIST");
                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        returnList.Add(new Joins.Apps.Models.News10.EventData {
                            title = Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(dr["TITLE"].ToString())),
                            summary = Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(dr["SUMMARY"].ToString())),
                            image = dr["IMG"].ToString(),
                            bg_image = dr["BG_IMG"].ToString(),
                            event_type = dr["EVENT_TYPE"].ToString(),
                            event_cont = dr["EVENT_CONT"].ToString(),
                            event_sdt = dr["EVENT_SDT"].ToString(),
                            event_edt = dr["EVENT_EDT"].ToString()
                        });
                    }
                }
                return returnList;
            }
        }

        public static IEnumerable<Joins.Apps.Models.News10.Event> GetCMSEvent(int pgi , int pgs , int seq , string syn, out int tot_cnt) {
            List<Joins.Apps.Models.News10.Event> rlist = new List<Joins.Apps.Models.News10.Event>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@PageIndex", pgi);
                helper.ProcParamAdd("@PageSize", pgs);
                helper.ProcParamAdd("@SEQ", seq);
                helper.ProcParamAdd("@USED_YN", syn);
                DataSet ds = helper.ProcExecuteDataSet("UPU_NEWS10_EVENT_LIST");
                helper.Close();
                tot_cnt = 0;
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        tot_cnt = JCube.AF.Util.Converts.ToInt32(dr["T_COUNT"], 0);
                        rlist.Add(new Joins.Apps.Models.News10.Event {
                            ROWNUM = JCube.AF.Util.Converts.ToInt32(dr["ROWNUM"], 0),
                            seq = JCube.AF.Util.Converts.ToInt32(dr["SEQ"], 0),
                            used_yn = JCube.AF.Util.Converts.ToString(dr["USED_YN"], ""),
                            title = dr["TITLE"].ToString(),
                            summary = dr["SUMMARY"].ToString(),
                            img = dr["IMG"].ToString(),
                            bg_img = dr["BG_IMG"].ToString(),
                            event_type = dr["EVENT_TYPE"].ToString(),
                            event_cont = dr["EVENT_CONT"].ToString(),
                            event_sdt = dr["EVENT_SDT"].ToString(),
                            event_edt = dr["EVENT_EDT"].ToString(),
                            reg_dt = JCube.AF.Util.Converts.ToString(dr["REG_DT"], ""),
                            mod_dt = JCube.AF.Util.Converts.ToString(dr["MOD_DT"], "")
                        });
                    }
                }
                return rlist;
            }
        }
        public static int SetEventItemEdit(Joins.Apps.Models.News10.Event exc) {
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@SEQ", exc.seq);
                helper.ProcParamAdd("@USED_YN", exc.used_yn);
                helper.ProcParamAdd("@TITLE", exc.title);
                helper.ProcParamAdd("@SUMMARY", exc.summary);
                helper.ProcParamAdd("@IMG", exc.img);
                helper.ProcParamAdd("@BG_IMG", exc.bg_img);
                helper.ProcParamAdd("@EVENT_CONT", exc.event_cont);
                int r = helper.ProcExecuteScalar("UPU_NEWS10_EVENT_EDIT", -1);
                helper.Close();
                return r;
            }
        }
        #endregion
    }
}
