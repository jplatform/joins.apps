﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Repository.News10 {
    public class ShowCaseRepository {
        /// <summary>
        /// 쇼케이스 커버 이미지 관리
        /// </summary>
        /// <param name="pgi"></param>
        /// <param name="ps"></param>
        /// <param name="parameters"></param>
        /// <param name="tot_cnt"></param>
        /// <returns></returns>
        public static IEnumerable<Joins.Apps.Models.News10.ShowCase> GetShowCaseList(int pgi, int ps, Joins.Apps.Models.News10.ShowCase parameters, out int tot_cnt) {
            List<Joins.Apps.Models.News10.ShowCase> rlist = new List<Joins.Apps.Models.News10.ShowCase>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@PageIndex", pgi);
                helper.ProcParamAdd("@PageSize", ps);
                helper.ProcParamAdd("@SEQ", parameters.SEQ);
                helper.ProcParamAdd("@BRIEF_TYPE", parameters.BRIEF_TYPE);
                helper.ProcParamAdd("@USED_YN", parameters.USED_YN);
                helper.ProcParamAdd("@BASIC_YN", parameters.BASIC_YN);
                helper.ProcParamAdd("@WEATHER_TYPE", parameters.WEATHER_TYPE);
                helper.ProcParamAdd("@SPECIAL_DAY", parameters.SPECIAL_DAY);

                DataSet ds = helper.ProcExecuteDataSet("UPU_NEWS10_SHOWCASE_LIST");
                helper.Close();
                tot_cnt = 0;
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;

                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        tot_cnt = JCube.AF.Util.Converts.ToInt32(dr["T_COUNT"], 0);
                        rlist.Add(new Joins.Apps.Models.News10.ShowCase {
                            ROWNUM = JCube.AF.Util.Converts.ToInt32(dr["ROWNUM"], 0),
                            SEQ = JCube.AF.Util.Converts.ToInt32(dr["SEQ"], 0),
                            BRIEF_TYPE = JCube.AF.Util.Converts.ToString(dr["BRIEF_TYPE"], ""),
                            USED_YN = JCube.AF.Util.Converts.ToString(dr["USED_YN"], ""),
                            BASIC_YN = JCube.AF.Util.Converts.ToString(dr["BASIC_YN"], ""),
                            WEATHER_TYPE = JCube.AF.Util.Converts.ToString(dr["WEATHER_TYPE"], ""),
                            SPECIAL_DAY = JCube.AF.Util.Converts.ToString(dr["SPECIAL_DAY"], ""),
                            IMG = JCube.AF.Util.Converts.ToString(dr["IMG"], ""),
                            REG_ID = JCube.AF.Util.Converts.ToString(dr["REG_ID"], ""),
                            REG_DT = JCube.AF.Util.Converts.ToString(dr["REG_DT"], ""),
                            MOD_ID = JCube.AF.Util.Converts.ToString(dr["MOD_ID"], ""),
                            MOD_DT = JCube.AF.Util.Converts.ToString(dr["MOD_DT"], "")
                        });
                    }
                }
                return rlist;
            }
        }
        /// <summary>
        /// 쇼케이스 커버 이미지 관리
        /// </summary>
        /// <param name="pgi"></param>
        /// <param name="ps"></param>
        /// <param name="parameters"></param>
        /// <param name="tot_cnt"></param>
        /// <returns></returns>
        public static List<List<Joins.Apps.Models.News10.ShowCase>> GetShowCaseToday() {
            List<List<Joins.Apps.Models.News10.ShowCase>> rlist = new List<List<Joins.Apps.Models.News10.ShowCase>>();
            List<Joins.Apps.Models.News10.ShowCase> rlist1 = new List<Joins.Apps.Models.News10.ShowCase>();
            List<Joins.Apps.Models.News10.ShowCase> rlist2 = new List<Joins.Apps.Models.News10.ShowCase>();
            List<Joins.Apps.Models.News10.ShowCase> rlist3 = new List<Joins.Apps.Models.News10.ShowCase>();
            List<Joins.Apps.Models.News10.ShowCase> rlist4 = new List<Joins.Apps.Models.News10.ShowCase>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                DataSet ds = helper.ProcExecuteDataSet("UPU_NEWS10_SHOWCASE_TODAY");
                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    bool weather_1 = false;
                    bool weather_2 = false;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        string w = JCube.AF.Util.Converts.ToString(dr["WEATHER_TYPE"], "");
                        if (w == "1") { if (!weather_1) { weather_1 = true; } else { continue; } }
                        if (w == "2") { if (!weather_2) { weather_2 = true; } else { continue; } }
                        rlist1.Add(new Joins.Apps.Models.News10.ShowCase {

                            SEQ = JCube.AF.Util.Converts.ToInt32(dr["SEQ"], 0),
                            BRIEF_TYPE = JCube.AF.Util.Converts.ToString(dr["BRIEF_TYPE"], ""),
                            USED_YN = JCube.AF.Util.Converts.ToString(dr["USED_YN"], ""),
                            BASIC_YN = JCube.AF.Util.Converts.ToString(dr["BASIC_YN"], ""),
                            WEATHER_TYPE = w,
                            SPECIAL_DAY = JCube.AF.Util.Converts.ToString(dr["SPECIAL_DAY"], ""),
                            IMG = JCube.AF.Util.Converts.ToString(dr["IMG"], ""),
                            REG_ID = JCube.AF.Util.Converts.ToString(dr["REG_ID"], ""),
                            REG_DT = JCube.AF.Util.Converts.ToString(dr["REG_DT"], ""),
                            MOD_ID = JCube.AF.Util.Converts.ToString(dr["MOD_ID"], ""),
                            MOD_DT = JCube.AF.Util.Converts.ToString(dr["MOD_DT"], "")
                        });
                    }
                }
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[1].Rows != null) {
                    int cnt = ds.Tables[1].Rows.Count;
                    bool weather_1 = false;
                    bool weather_2 = false;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[1].Rows[tmpi];
                        string w = JCube.AF.Util.Converts.ToString(dr["WEATHER_TYPE"], "");
                        if (w == "1") { if (!weather_1) { weather_1 = true; } else { continue; } }
                        if (w == "2") { if (!weather_2) { weather_2 = true; } else { continue; } }
                        rlist2.Add(new Joins.Apps.Models.News10.ShowCase {

                            SEQ = JCube.AF.Util.Converts.ToInt32(dr["SEQ"], 0),
                            BRIEF_TYPE = JCube.AF.Util.Converts.ToString(dr["BRIEF_TYPE"], ""),
                            USED_YN = JCube.AF.Util.Converts.ToString(dr["USED_YN"], ""),
                            BASIC_YN = JCube.AF.Util.Converts.ToString(dr["BASIC_YN"], ""),
                            WEATHER_TYPE = w,
                            SPECIAL_DAY = JCube.AF.Util.Converts.ToString(dr["SPECIAL_DAY"], ""),
                            IMG = JCube.AF.Util.Converts.ToString(dr["IMG"], ""),
                            REG_ID = JCube.AF.Util.Converts.ToString(dr["REG_ID"], ""),
                            REG_DT = JCube.AF.Util.Converts.ToString(dr["REG_DT"], ""),
                            MOD_ID = JCube.AF.Util.Converts.ToString(dr["MOD_ID"], ""),
                            MOD_DT = JCube.AF.Util.Converts.ToString(dr["MOD_DT"], "")
                        });
                    }
                }
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[2].Rows != null) {
                    int cnt = ds.Tables[2].Rows.Count;
                    bool weather_1 = false;
                    bool weather_2 = false;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[2].Rows[tmpi];
                        string w = JCube.AF.Util.Converts.ToString(dr["WEATHER_TYPE"], "");
                        if (w == "1") { if (!weather_1) { weather_1 = true; } else { continue; } }
                        if (w == "2") { if (!weather_2) { weather_2 = true; } else { continue; } }
                        rlist3.Add(new Joins.Apps.Models.News10.ShowCase {

                            SEQ = JCube.AF.Util.Converts.ToInt32(dr["SEQ"], 0),
                            BRIEF_TYPE = JCube.AF.Util.Converts.ToString(dr["BRIEF_TYPE"], ""),
                            USED_YN = JCube.AF.Util.Converts.ToString(dr["USED_YN"], ""),
                            BASIC_YN = JCube.AF.Util.Converts.ToString(dr["BASIC_YN"], ""),
                            WEATHER_TYPE = w,
                            SPECIAL_DAY = JCube.AF.Util.Converts.ToString(dr["SPECIAL_DAY"], ""),
                            IMG = JCube.AF.Util.Converts.ToString(dr["IMG"], ""),
                            REG_ID = JCube.AF.Util.Converts.ToString(dr["REG_ID"], ""),
                            REG_DT = JCube.AF.Util.Converts.ToString(dr["REG_DT"], ""),
                            MOD_ID = JCube.AF.Util.Converts.ToString(dr["MOD_ID"], ""),
                            MOD_DT = JCube.AF.Util.Converts.ToString(dr["MOD_DT"], "")
                        });
                    }
                }
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[3].Rows != null) {
                    int cnt = ds.Tables[3].Rows.Count;
                    bool weather_1 = false;
                    bool weather_2 = false;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[3].Rows[tmpi];
                        string w = JCube.AF.Util.Converts.ToString(dr["WEATHER_TYPE"], "");
                        if (w == "1") { if (!weather_1) { weather_1 = true; } else { continue; } }
                        if (w == "2") { if (!weather_2) { weather_2 = true; } else { continue; } }
                        rlist4.Add(new Joins.Apps.Models.News10.ShowCase {
                            SEQ = JCube.AF.Util.Converts.ToInt32(dr["SEQ"], 0),
                            BRIEF_TYPE = JCube.AF.Util.Converts.ToString(dr["BRIEF_TYPE"], ""),
                            USED_YN = JCube.AF.Util.Converts.ToString(dr["USED_YN"], ""),
                            BASIC_YN = JCube.AF.Util.Converts.ToString(dr["BASIC_YN"], ""),
                            WEATHER_TYPE = w,
                            SPECIAL_DAY = JCube.AF.Util.Converts.ToString(dr["SPECIAL_DAY"], ""),
                            IMG = JCube.AF.Util.Converts.ToString(dr["IMG"], ""),
                            REG_ID = JCube.AF.Util.Converts.ToString(dr["REG_ID"], ""),
                            REG_DT = JCube.AF.Util.Converts.ToString(dr["REG_DT"], ""),
                            MOD_ID = JCube.AF.Util.Converts.ToString(dr["MOD_ID"], ""),
                            MOD_DT = JCube.AF.Util.Converts.ToString(dr["MOD_DT"], "")
                        });
                    }
                }
                rlist.Add(rlist1);
                rlist.Add(rlist2);
                rlist.Add(rlist3);
                rlist.Add(rlist4);
                return rlist;
            }
        }
        /// <summary>
        /// 쇼케이스 커버 이미지 정보
        /// </summary>
               /// <returns></returns>
        public static List<Joins.Apps.Models.News10.ShowCase> GetShowCaseInfo() {
            List<Joins.Apps.Models.News10.ShowCase> rlist = new List<Joins.Apps.Models.News10.ShowCase>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                DataSet ds = helper.ProcExecuteDataSet("UPU_NEWS10_SHOWCASE_INFO");
                helper.Close();

                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        rlist.Add(new Joins.Apps.Models.News10.ShowCase {
                            //  ROWNUM = JCube.AF.Util.Converts.ToInt32(dr["ROWNUM"], 0),
                            SEQ = JCube.AF.Util.Converts.ToInt32(dr["SEQ"], 0),
                            BRIEF_TYPE = JCube.AF.Util.Converts.ToString(dr["BRIEF_TYPE"], ""),
                            USED_YN = JCube.AF.Util.Converts.ToString(dr["USED_YN"], ""),
                            BASIC_YN = JCube.AF.Util.Converts.ToString(dr["BASIC_YN"], ""),
                            WEATHER_TYPE = JCube.AF.Util.Converts.ToString(dr["WEATHER_TYPE"], ""),
                            SPECIAL_DAY = JCube.AF.Util.Converts.ToString(dr["SPECIAL_DAY"], ""),
                            IMG = JCube.AF.Util.Converts.ToString(dr["IMG"], ""),
                            REG_ID = JCube.AF.Util.Converts.ToString(dr["REG_ID"], ""),
                            REG_DT = JCube.AF.Util.Converts.ToString(dr["REG_DT"], ""),
                            MOD_ID = JCube.AF.Util.Converts.ToString(dr["MOD_ID"], ""),
                            MOD_DT = JCube.AF.Util.Converts.ToString(dr["MOD_DT"], "")
                        });
                    }
                }
                //날씨> 스페셜데이> 기본 순으로 노출 순위 결정
                return rlist;
            }
        }
        public static int SetShowCaseItemEdit(Joins.Apps.Models.News10.ShowCase exc) {
            //  Joins.Apps.Models.News10.ExceptCard itm = new Joins.Apps.Models.News10.ExceptCard();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@SEQ", exc.SEQ);
                helper.ProcParamAdd("@BRIEF_TYPE", exc.BRIEF_TYPE);
                helper.ProcParamAdd("@USED_YN", exc.USED_YN);
                helper.ProcParamAdd("@BASIC_YN", exc.BASIC_YN);
                helper.ProcParamAdd("@WEATHER_TYPE", exc.WEATHER_TYPE);
                helper.ProcParamAdd("@SPECIAL_DAY", exc.SPECIAL_DAY);
                helper.ProcParamAdd("@IMG", exc.IMG);
                helper.ProcParamAdd("@EDITOR", exc.REG_ID);
                int r = helper.ProcExecuteScalar("UPU_NEWS10_SHOWCASE_EDIT", -1);
                helper.Close();
                return r;
            }
        }
    }
}
