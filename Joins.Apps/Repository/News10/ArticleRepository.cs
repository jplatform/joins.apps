﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Repository.News10 {
    public class ArticleRepository {
        #region 조회

        public static Joins.Apps.Models.News10.ArticleData View(Joins.Apps.Models.News10.ArticleParameters parameters) {
            var returnData = new Joins.Apps.Models.News10.ArticleData();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@Total_ID", parameters.totalID);
                DataSet ds = helper.ProcExecuteDataSet("UPU_MYNEWS_ARTICLE_VIEW");
                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        returnData.title = Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(dr["ARTICLE_TITLE"].ToString()));
                        returnData.content = Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(dr["Article_Content"].ToString()));
                        returnData.image = dr["ARTICLE_THUMBNAIL"].ToString();
                    }

                }
                return returnData;
            }
        }

        #endregion
    }
}
