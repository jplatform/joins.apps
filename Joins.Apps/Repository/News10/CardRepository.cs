﻿using Joins.Apps.Models.News10;
using Joins.Apps.Models.Search;
using Joins.Apps.Repository.Search;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Xml;

namespace Joins.Apps.Repository.News10 {
    public class CardRepository {
        private readonly static string[] zodiac = new string[] { "쥐", "소", "호랑이", "토끼", "용", "뱀", "말", "양", "원숭이", "닭", "개", "돼지" };

        #region 카드 리스트

        public static CardInfo GetCardInfo(string card_id) {
            Joins.Apps.Models.News10.CardInfo r = new Joins.Apps.Models.News10.CardInfo();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@CARD_ID", card_id);
                DataSet ds = helper.ProcExecuteDataSet("USP_NEWS10_CARD_INFO");
                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        r.SEQ = JCube.AF.Util.Converts.ToInt32(dr["CARD_ID"], 0);
                        r.CARD_ID = JCube.AF.Util.Converts.ToString(dr["CARD_ID"], "");
                        r.CARD_NAME = JCube.AF.Util.Converts.ToString(dr["CARD_NAME"], "");
                        r.CARD_ENG_NAME = JCube.AF.Util.Converts.ToString(dr["CARD_ENG_NAME"], "");
                        r.VERSION = JCube.AF.Util.Converts.ToString(dr["VERSION"], "");
                        r.MAX_VERSION = JCube.AF.Util.Converts.ToString(dr["MAX_VERSION"], "");
                        r.REG_DT = JCube.AF.Util.Converts.ToString(dr["REG_DT"], "");
                    }
                };
                return r;
            }
        }
        public static List<CardList> CardList(int cardVersion) {
            List<Joins.Apps.Models.News10.CardList> returnList = new List<Joins.Apps.Models.News10.CardList>();
            //   List<Joins.Apps.Models.News10.CardInfo> rlist = new List<Joins.Apps.Models.News10.CardInfo>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@VERSION", cardVersion);
                DataSet ds = helper.ProcExecuteDataSet("UPU_NEWS10_APP_CARDLIST_INFO");
                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        returnList.Add(new CardList {
                            id = dr["CARD_ID"].ToString(),
                            name = dr["CARD_NAME"].ToString()
                        });
                    }
                };
                return returnList;
            }
        }

        public static List<CardList> MyCardList(int cardVersion,string deviceID) {
            List<Joins.Apps.Models.News10.CardList> returnList = new List<Joins.Apps.Models.News10.CardList>();
            //     List<Joins.Apps.Models.News10.CardInfo> rlist = new List<Joins.Apps.Models.News10.CardInfo>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@VERSION", cardVersion);
                helper.ProcParamAdd("@DEV_ID", deviceID);
                DataSet ds = helper.ProcExecuteDataSet("UPU_NEWS10_MY_CARDLIST");
                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        string sid = dr["CARD_ID"].ToString();
                        string yn = dr["yn"].ToString();
                        if (string.IsNullOrEmpty(yn)) { yn = "Y"; }
                        returnList.Add(new CardList {
                            id = sid,
                            name = dr["CARD_NAME"].ToString(),
                            ename = dr["CARD_ENG_NAME"].ToString(),
                            visible_yn = yn,
                            setting_yn = dr["SETTING_YN"].ToString()
                        });
                    }
                };
                return returnList;
            }
        }
        /// <summary>
        /// 카드리스트 상태변경
        /// </summary>
        /// <param name="seq"></param>
        /// <param name="uyn"></param>
        /// <returns></returns>
        public static bool SetMyCardListYN(string deviceId,string cardId, string uyn) {
            Joins.Apps.Models.News10.ExceptCard itm = new Joins.Apps.Models.News10.ExceptCard();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                /*
                 @DEV_ID VARCHAR(100)='',
                @CARD_ID VARCHAR(3)='',
                @USED_YN VARCHAR(1)='N'
                 */
                helper.ProcParamAdd("@DEV_ID", deviceId);
                helper.ProcParamAdd("@CARD_ID", cardId);
                helper.ProcParamAdd("@USED_YN", uyn);
                bool r = helper.ProcExecuteNonQuery("UPU_NEWS10_MY_CARD_PROC") > 0;
                helper.Close();
                return r;
            }
        }
        /// <summary>
        /// 카드리스트(관리툴)
        /// </summary>
        /// <param name="pgi"></param>
        /// <param name="pgs"></param>
        /// <param name="uyn"></param>
        /// <param name="tot_cnt"></param>
        /// <returns></returns>
        public static IEnumerable<Joins.Apps.Models.News10.CardInfo> GetCmsCardList( int pgi,int pgs,string uyn, out int tot_cnt) {
            List<Joins.Apps.Models.News10.CardInfo> rlist = new List<Joins.Apps.Models.News10.CardInfo>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@PageIndex", pgi);
                helper.ProcParamAdd("@PageSize", pgs);
                helper.ProcParamAdd("@used_yn", uyn);
                DataSet ds = helper.ProcExecuteDataSet("UPU_NEWS10_CMS_CARDLIST");
                helper.Close();
                tot_cnt = 0;
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    tot_cnt = cnt;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        tot_cnt = JCube.AF.Util.Converts.ToInt32(dr["T_COUNT"], 0);
                        rlist.Add(new Joins.Apps.Models.News10.CardInfo {
                            ROWNUM = JCube.AF.Util.Converts.ToInt32(dr["ROWNUM"], 0),
                            SEQ = JCube.AF.Util.Converts.ToInt32(dr["SEQ"], 0),
                            CARD_ORDER = JCube.AF.Util.Converts.ToInt32(dr["CARD_ORDER"], 0),
                            USED_YN = JCube.AF.Util.Converts.ToString(dr["USED_YN"], ""),
                            CARD_ID = JCube.AF.Util.Converts.ToString(dr["CARD_ID"], ""),
                            CARD_NAME = JCube.AF.Util.Converts.ToString(dr["CARD_NAME"], ""),
                            CARD_ENG_NAME = JCube.AF.Util.Converts.ToString(dr["CARD_ENG_NAME"], ""),
                            VERSION = JCube.AF.Util.Converts.ToString(dr["VERSION"], ""),
                            MAX_VERSION = JCube.AF.Util.Converts.ToString(dr["MAX_VERSION"], ""),
                            REG_DT = JCube.AF.Util.Converts.ToString(dr["REG_DT"], "")
                        });
                    }
                }
                return rlist;
            }
        }
        /// <summary>
        /// 카드리스트 상태변경
        /// </summary>
        /// <param name="seq"></param>
        /// <param name="uyn"></param>
        /// <returns></returns>
        public static int SetCardListYN(int seq,string uyn) {
            Joins.Apps.Models.News10.ExceptCard itm = new Joins.Apps.Models.News10.ExceptCard();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()){
                helper.ProcParamClear();
                helper.ProcParamAdd("@SEQ", seq);
                helper.ProcParamAdd("@USED_YN", uyn);
                int r = helper.ProcExecuteNonQuery("UPU_NEWS10_APP_CARDLIST_YN");
                helper.Close();
                return r;
            }
        }
        #endregion

        #region 칼럼
        public static List<Joins.Apps.Models.News10.ArticleData> Column() {
            List<Joins.Apps.Models.News10.ArticleData> returnList = new List<Joins.Apps.Models.News10.ArticleData>();

            string xmlPath = WebConfigurationManager.AppSettings["StaticUrl"] + "/scripts/data/home/xml/right_today_column.xml";
            XmlDocument xml = new XmlDocument();
            xml.Load(xmlPath);
            XmlNodeList xmlList = xml.SelectNodes("/article_list/article");

            foreach (XmlNode xn in xmlList) {
                string stitle = Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(xn["title"].InnerText));
                string stotid = xn["total_id"].InnerText;
                if (stitle.IndexOf("[사설]") >= 0) { continue; }
                if (!string.IsNullOrEmpty(stotid) && !string.IsNullOrEmpty(stitle)) {
                    returnList.Add(new Joins.Apps.Models.News10.ArticleData {
                        id = xn["total_id"].InnerText,
                        title = Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(xn["title"].InnerText)),
                        summary = string.Empty,
                        image = xn["image"].InnerText,
                        writer = xn["subtitle"].InnerText,
                        date = string.Empty
                    });
                }
            }
            if (returnList.Any())
                returnList = returnList.OrderBy(x => Guid.NewGuid()).Take(5).ToList();
            return returnList;
        }

        #endregion

        #region 속보
        public static List<Joins.Apps.Models.News10.ArticleData> Newsflash() {
            List<Joins.Apps.Models.News10.ArticleData> returnList = new List<Joins.Apps.Models.News10.ArticleData>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@pageNum", 1);
                helper.ProcParamAdd("@pageSize", 1);
                DataSet ds = helper.ProcExecuteDataSet("UPU_MYNEWS_NEWSFLASH_LIST");
                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        string date = dr["RSV_DAYTIME"].ToString();
                        if (!string.IsNullOrEmpty(date)) {
                            date = string.Format("{0}-{1}-{2} {3}:{4}:00", date.Substring(0, 4), date.Substring(4, 2), date.Substring(6, 2), date.Substring(8, 2), date.Substring(10, 2));
                        }

                        returnList.Add(new Joins.Apps.Models.News10.ArticleData {
                            id = dr["REL_TOTAL_ID"].ToString(),
                            title = Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(dr["TITLE"].ToString())),
                            summary = string.Empty,
                            image = dr["IMG_URL"].ToString(),
                            writer = string.Empty,
                            date = date
                        });
                    }
                }

                return returnList;
            }

        }
        #endregion

        #region 운세
        public static List<Joins.Apps.Models.News10.FortuneData> Fortune() {
            List<Joins.Apps.Models.News10.FortuneData> returnList = new List<Joins.Apps.Models.News10.FortuneData>();

            string xmlPath = WebConfigurationManager.AppSettings["StaticUrl"] + "/scripts/data/mynews/Fortune.xml";
            XmlDocument xml = new XmlDocument();
            xml.Load(xmlPath);
            XmlNodeList xmlList = xml.SelectNodes("/Fortune/zodiac");

            foreach (XmlNode xn in xmlList) {
                returnList.Add(new Joins.Apps.Models.News10.FortuneData {
                    name = xn["name"].InnerText,
                    description = xn["description"].InnerText
                });
            }

            return returnList;
        }
        public static List<FortuneData2> Fortune2() {
            int cYear = DateTime.Now.Year;
            int fYear = cYear - 99;
            int lYear = cYear - 7;
            Hashtable hzodiac = new Hashtable();
            // int ifzodiac = (fYear - 1912) % 12;
            //  string fzodiac = zodiac[ifzodiac];//말띠
            for (int i = 1912; i <= lYear; i++) {
                int izodiac = 0;
                if (fYear <= i && lYear >= i) {
                    izodiac = (i - 1912) % 12; // 년도
                    string szodiac = zodiac[izodiac]; // 해당 년도 띠
                    if (hzodiac.ContainsKey(szodiac)) {
                        List<string> yearList = (List<string>)hzodiac[szodiac];
                        yearList.Add(i.ToString());
                        hzodiac[szodiac] = yearList;
                    }
                    else {
                        List<string> yearList = new List<string>();
                        yearList.Add(i.ToString());
                        hzodiac.Add(szodiac, yearList);
                    }
                }
            }
            //1918 말띠 zodiac[6]
            List<FortuneData2> returnList = new List<FortuneData2>();

            string xmlPath = WebConfigurationManager.AppSettings["StaticUrl"] + "/scripts/data/mynews/Fortune.xml";
            XmlDocument xml = new XmlDocument();
            xml.Load(xmlPath);
            XmlNodeList xmlList = xml.SelectNodes("/Fortune/zodiac");

            foreach (XmlNode xn in xmlList) {
                string sName = xn["name"].InnerText;
                string sDescription = xn["description"].InnerText;
                string[] sDescList = sDescription.Split(new string[] { "<br />-" }, StringSplitOptions.None);
                if (sDescList.Length > 1) {
                    string sTDesc = sDescList[0];
                    if (hzodiac.ContainsKey(sName)) {
                        List<string> zyearList = (List<string>)hzodiac[sName];
                        foreach (string zyear in zyearList) {
                            string sd = "";
                            foreach (string sDesc in sDescList) {
                                if (sDesc.IndexOf(zyear.Substring(2, 2) + "년생 :") >= 0) { sd = sDesc.Replace(zyear.Substring(2, 2) + "년생 :", ""); }
                            }
                            returnList.Add(new FortuneData2 {
                                birthyear = zyear,
                                name = sName,
                                description = (sTDesc.Trim() + " " + sd).Trim()
                            });
                        }
                    }
                }
                else {
                    string sTDesc = sDescription.Substring(0, sDescription.IndexOf(".") + 1);
                    if (hzodiac.ContainsKey(sName)) {
                        List<string> zyearList = (List<string>)hzodiac[sName];
                        foreach (string zyear in zyearList) {
                            string sd = "";
                            int fy = sDescription.IndexOf(zyear.Substring(2, 2) + "년생 :");
                            if (fy >= 0) {
                                int ey = sDescription.IndexOf(".", fy);
                                int ky = sDescription.IndexOf(":", fy + 7);
                                if (ey > fy && (ey < ky || ky < 0)) { sd = sDescription.Substring(fy + 6, ey - (fy + 6) + 1); }
                            }
                            returnList.Add(new FortuneData2 {
                                birthyear = zyear,
                                name = sName,
                                description = (sTDesc.Trim() + " " + sd).Trim()
                            });
                        }
                    }
                }

            }
            returnList.Sort(delegate (FortuneData2 x, FortuneData2 y) {
                if (x.birthyear == null && y.birthyear == null)
                    return 0;
                else if (x.birthyear == null)
                    return -1;
                else if (y.birthyear == null)
                    return 1;
                else
                    return x.birthyear.CompareTo(y.birthyear);
            });
            return returnList;
        }

        #endregion

        #region [News10 카드 제외 기사 리스트]
        public static IEnumerable<Joins.Apps.Models.News10.ExceptCard> GetExceptArticleList(int pgi, int ps,int tid,string uyn,string sdt,string edt, out int tot_cnt) {
            List<Joins.Apps.Models.News10.ExceptCard> rlist = new List<Joins.Apps.Models.News10.ExceptCard>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@PageIndex", pgi);
                helper.ProcParamAdd("@PageSize", ps);
                helper.ProcParamAdd("@tid", tid);
                helper.ProcParamAdd("@used_yn", uyn);
                helper.ProcParamAdd("@sdt", sdt);
                helper.ProcParamAdd("@edt", edt);
                DataSet ds = helper.ProcExecuteDataSet("UPU_NEWS10_APP_EXCEPT_ARTICLE_LIST");
                helper.Close();
                tot_cnt = 0;
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    tot_cnt = cnt;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        tot_cnt = JCube.AF.Util.Converts.ToInt32(dr["T_COUNT"], 0);
                        rlist.Add(new Joins.Apps.Models.News10.ExceptCard {
                            ROWNUM = JCube.AF.Util.Converts.ToInt32(dr["ROWNUM"], 0),
                            seq = JCube.AF.Util.Converts.ToInt32(dr["SEQ"], 0),
                            total_id = JCube.AF.Util.Converts.ToString(dr["TOTAL_ID"], ""),
                            art_title = JCube.AF.Util.Converts.ToString(dr["art_title"], ""),
                            reg_id = JCube.AF.Util.Converts.ToString(dr["REG_ID"], ""),
                            reg_dt = JCube.AF.Util.Converts.ToString(dr["REG_DT"], ""),
                            mod_id = JCube.AF.Util.Converts.ToString(dr["MOD_ID"], ""),
                            mod_dt = JCube.AF.Util.Converts.ToString(dr["MOD_DT"], ""),
                            used_yn = JCube.AF.Util.Converts.ToString(dr["USED_YN"], "")
                        });
                    }
                }
                return rlist;
            }
         }
        public static Joins.Apps.Models.News10.ExceptCard GetExceptArticleItem(int sno) {
            Joins.Apps.Models.News10.ExceptCard itm = new Joins.Apps.Models.News10.ExceptCard();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@SEQ", sno);
                DataSet ds = helper.ProcExecuteDataSet("UPU_NEWS10_APP_EXCEPT_ARTICLE_ITEM");
                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;

                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        itm = (new Joins.Apps.Models.News10.ExceptCard {
                            seq = JCube.AF.Util.Converts.ToInt32(dr["SEQ"], 0),
                            total_id = JCube.AF.Util.Converts.ToString(dr["TOTAL_ID"], ""),
                            art_title = JCube.AF.Util.Converts.ToString(dr["art_title"], ""),
                            reg_id = JCube.AF.Util.Converts.ToString(dr["REG_ID"], ""),
                            reg_dt = JCube.AF.Util.Converts.ToString(dr["REG_DT"], ""),
                            mod_id = JCube.AF.Util.Converts.ToString(dr["MOD_ID"], ""),
                            mod_dt = JCube.AF.Util.Converts.ToString(dr["MOD_DT"], ""),
                            used_yn = JCube.AF.Util.Converts.ToString(dr["USED_YN"], "")
                        });
                    }
                }
                return itm;
            }
        }
        public static int SetExceptArticleItemEdit(Joins.Apps.Models.News10.ExceptCard exc) {
            Joins.Apps.Models.News10.ExceptCard itm = new Joins.Apps.Models.News10.ExceptCard();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@SEQ", exc.seq);
                helper.ProcParamAdd("@TOTAL_ID", exc.total_id);
                helper.ProcParamAdd("@USED_YN", exc.used_yn);
                helper.ProcParamAdd("@EDITOR", exc.reg_id);
                int r = helper.ProcExecuteScalar("UPU_NEWS10_APP_EXCEPT_ARTICLE_EDIT", -1);
                helper.Close();
                return r;
            }
        }

        #endregion
        #region
        public static List<Joins.Apps.Models.News10.Cruz.News10> GetPreviewTodayData() {
            List<Joins.Apps.Models.News10.Cruz.News10> itmList = null;
            JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper();
            helper.ProcParamClear();
            helper.ProcParamAdd("@PageIndex", 1);
            helper.ProcParamAdd("@PageSize", 10);
            helper.ProcParamAdd("@SearchType", "PREVIEWTODAY");
            helper.ProcParamAdd("@SearchWord", "");
            helper.ProcParamAdd("@SearchSDay", "");
            helper.ProcParamAdd("@SearchEDay", "");
            helper.ProcParamAdd("@SourceCode", "");
            
            DataSet ds = helper.ProcExecuteDataSet("USP_NEWS10_ARTICLE_LIST");
            helper.Close();
            int tot_cnt = 0;
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                int cnt = ds.Tables[0].Rows.Count;
                tot_cnt = cnt;
                itmList = new List<Joins.Apps.Models.News10.Cruz.News10>();
                for (int tmpi = 0; tmpi < cnt; tmpi++) {
                    DataRow dr = ds.Tables[0].Rows[tmpi];
                    tot_cnt = JCube.AF.Util.Converts.ToInt32(dr["T_COUNT"], 0);
                    int iTotalID = JCube.AF.Util.Converts.ToInt32(dr["Total_ID"], 0);
                    string sTitle = JCube.AF.Util.Converts.ToString(dr["MOB_TITLE"], "");
                    string sServiceDay = JCube.AF.Util.Converts.ToString(dr["ServiceDay"], "");
                    string sServiceTime = JCube.AF.Util.Converts.ToString(dr["ServiceTime"], "");
                    string sThumbnail = JCube.AF.Util.Converts.ToString(dr["Article_ThumbNail"], "");
                    if (sServiceDay == DateTime.Now.ToString("yyyyMMdd")) {// 오늘 기사만
                    
                       sTitle = Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(sTitle));
                        List<Cruz.Art_imgs> art_imgs1 = new List<Cruz.Art_imgs>();
                        art_imgs1.Add(new Cruz.Art_imgs {
                            h = 0,
                            w = 0,
                            num = 1,
                            img_url = Joins.Apps.Common.Util.ImagePath(sThumbnail)
                        });
                        string sContent = GetContentData(iTotalID);
                        string sSummary = "";
                        if (sContent.IndexOf("━") >= 0) {
                            int istart = sContent.IndexOf("━") + 1;
                            int iEnd = sContent.IndexOf("━", istart);
                            if (istart > 0 && iEnd > 0 && istart < iEnd) {
                                sSummary = (sContent.Substring(istart, iEnd - istart)).Trim();
                            }
                        }
                        if (string.IsNullOrEmpty(sSummary)|| sSummary.Length < 50) {
                            if (sContent.Length > 300) {sSummary = sContent.Substring(0, 300)+"...";}
                            else { sSummary = sContent; }
                        }
                        sSummary = sSummary.Replace("━","");
                        List<Cruz.Summaries> summaries1 = new List<Cruz.Summaries>();
                        summaries1.Add(new Cruz.Summaries {
                            ord = 1,
                            summary = sSummary
                        });
                        itmList.Add(new Joins.Apps.Models.News10.Cruz.News10 {
                            title = sTitle,
                            id = iTotalID.ToString(),
                            image = Joins.Apps.Common.Util.ImagePath(sThumbnail),
                            art_imgs = art_imgs1,
                            content = sContent,
                            summary = sSummary,
                            summaries = summaries1,
                            date = Joins.Apps.Common.Util.DateTimeConvert(sServiceDay + sServiceTime),
                        });
                        break;
                    }
                }
            }
            return itmList;
        }
        public static string GetContentData(int total_id) {
         //   total_id = 22066973;
            string r= null;
            JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper();
            helper.ProcParamClear();
            helper.ProcParamAdd("@Total_ID", total_id);
            DataSet ds = helper.ProcExecuteDataSet("Usp_Article_Full_Content_Select");
            helper.Close();
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                int cnt = ds.Tables[0].Rows.Count;
                for (int tmpi = 0; tmpi < cnt; tmpi++) {
                    DataRow dr = ds.Tables[0].Rows[tmpi];
                    string Article_Content = JCube.AF.Util.Converts.ToString(dr["Article_Content"], "");
                    Article_Content = Joins.Apps.Repository.News10.CruzRepository.GetCotentReFine(Article_Content);
                    Article_Content = Article_Content.Replace("위 재생(▶) 버튼을 누르면 음성으로 들으실 수 있습니다.","");
                    Article_Content = Article_Content.Replace("━", "━━");
                    string pattern = @"(\▶)(.*?)(\━)";
                    Article_Content = Regex.Replace(Article_Content, pattern, string.Empty);
                    Article_Content = Article_Content.Replace("━━", "━");
                    //▶━
                    r = Article_Content;
                    break;
                }
            }
            return r;
        }

        #endregion
        public static List<Joins.Apps.Models.News10.Cruz3.Article_List> GetHotSeriesData(string devid="") {
            List<Joins.Apps.Models.News10.Cruz3.Article_List> r = new List<Joins.Apps.Models.News10.Cruz3.Article_List>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@DEV_ID", devid);
                DataSet ds = helper.ProcExecuteDataSet("USP_NEWS10_LIST_SERIES");

                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        string content1 = JCube.AF.Util.Converts.ToString(dr["ARTICLE_CONTENT"], "");
                        string sTitle = Joins.Apps.Common.Util.DisplayText(JCube.AF.Util.Converts.ToString(dr["ARTICLE_TITLE"], "")).Trim();

                        content1 = Joins.Apps.Repository.News10.CruzRepository.GetCotentReFine(content1);
                        List<Joins.Apps.Models.News10.Cruz.Art_imgs> artimgList = JsonConvert.DeserializeObject<List<Joins.Apps.Models.News10.Cruz.Art_imgs>>(JCube.AF.Util.Converts.ToString(dr["ARTICLE_IMGS"], ""));
                        List<Joins.Apps.Models.News10.Cruz.Summaries> summaries = JsonConvert.DeserializeObject<List<Joins.Apps.Models.News10.Cruz.Summaries>>(JCube.AF.Util.Converts.ToString(dr["ARTICLE_SUMMARIES"], ""));
                        string date1 = "", image1 = "";
                        try {
                            DateTime dt = DateTime.ParseExact(dr["SERVICE_DT"].ToString(), "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                            date1 = dt.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                        } catch { }
                        if (artimgList != null && artimgList.Count > 0) { image1 = artimgList[0].img_url; }
                        if (sTitle.IndexOf("서명수의 노후준비") >= 0) { artimgList = null; image1 = ""; }
                        r.Add(new Joins.Apps.Models.News10.Cruz3.Article_List {
                            art_imgs = artimgList,
                            content = content1.Trim(),
                            date = date1,
                            id = JCube.AF.Util.Converts.ToString(dr["TOTAL_ID"], ""),
                            image = image1,
                            summaries = summaries,
                            summary = JCube.AF.Util.Converts.ToString(dr["ARTICLE_SUMMARY"], ""),
                            title = sTitle
                        });
                    }
                }
                return r;
            }
        }
        public static List<Joins.Apps.Models.News10.Cruz3.Article_List> GetRelativeArticleData(int total_id,string devid = "") {
            List<Joins.Apps.Models.News10.Cruz3.Article_List> r = new List<Joins.Apps.Models.News10.Cruz3.Article_List>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@TOTAL_ID", total_id);
                helper.ProcParamAdd("@DEV_ID", devid);
                DataSet ds = helper.ProcExecuteDataSet("USP_NEWS10_ARTICLE_RELATIVE");

                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        //   string content1 = JCube.AF.Util.Converts.ToString(dr["ARTICLE_CONTENT"], "");
                        string sTitle = Joins.Apps.Common.Util.DisplayText(JCube.AF.Util.Converts.ToString(dr["ARTICLE_TITLE"], "")).Trim();

                        //   content1 = Joins.Apps.Repository.News10.CruzRepository.GetCotentReFine(content1);
                        List<Joins.Apps.Models.News10.Cruz.Art_imgs> artimgList = JsonConvert.DeserializeObject<List<Joins.Apps.Models.News10.Cruz.Art_imgs>>(JCube.AF.Util.Converts.ToString(dr["ARTICLE_IMGS"], ""));
                        List<Joins.Apps.Models.News10.Cruz.Summaries> summaries = JsonConvert.DeserializeObject<List<Joins.Apps.Models.News10.Cruz.Summaries>>(JCube.AF.Util.Converts.ToString(dr["ARTICLE_SUMMARIES"], ""));
                        string date1 = "", image1 = "";
                        try {
                            DateTime dt = DateTime.ParseExact(dr["SERVICE_DT"].ToString(), "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                            date1 = dt.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                        } catch { }
                        if (artimgList != null && artimgList.Count > 0) { image1 = artimgList[0].img_url; }
                        if (sTitle.IndexOf("서명수의 노후준비") >= 0) { artimgList = null; image1 = ""; }
                        r.Add(new Joins.Apps.Models.News10.Cruz3.Article_List {
                            art_imgs = artimgList,
                            /*  content = content1.Trim(),*/
                            date = date1,
                            id = JCube.AF.Util.Converts.ToString(dr["TOTAL_ID"], ""),
                            image = image1,
                            summaries = summaries,
                            summary = JCube.AF.Util.Converts.ToString(dr["ARTICLE_SUMMARY"], ""),
                            title = sTitle
                        });
                    }
                }
                return r;
            }
        }

        public static List<Joins.Apps.Models.News10.Cruz3.Article_List> GetWeekly7Data(string devid = "") {
            List<Joins.Apps.Models.News10.Cruz3.Article_List> r = new List<Joins.Apps.Models.News10.Cruz3.Article_List>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@DEV_ID", devid);
                DataSet ds = helper.ProcExecuteDataSet("USP_NEWS10_WEEKLY_RANKING");

                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        //   string content1 = JCube.AF.Util.Converts.ToString(dr["ARTICLE_CONTENT"], "");
                        string sTitle = Joins.Apps.Common.Util.DisplayText(JCube.AF.Util.Converts.ToString(dr["ARTICLE_TITLE"], "")).Trim();

                        //     content1 = Joins.Apps.Repository.News10.CruzRepository.GetCotentReFine(content1);
                        List<Joins.Apps.Models.News10.Cruz.Art_imgs> artimgList = JsonConvert.DeserializeObject<List<Joins.Apps.Models.News10.Cruz.Art_imgs>>(JCube.AF.Util.Converts.ToString(dr["ARTICLE_IMGS"], ""));
                        List<Joins.Apps.Models.News10.Cruz.Summaries> summaries = JsonConvert.DeserializeObject<List<Joins.Apps.Models.News10.Cruz.Summaries>>(JCube.AF.Util.Converts.ToString(dr["ARTICLE_SUMMARIES"], ""));
                        string date1 = "", image1 = "";
                        try {
                            DateTime dt = DateTime.ParseExact(dr["SERVICE_DT"].ToString(), "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                            date1 = dt.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                        } catch { }
                        if (artimgList != null && artimgList.Count > 0) { image1 = artimgList[0].img_url; }
                        if (sTitle.IndexOf("서명수의 노후준비") >= 0) { artimgList = null; image1 = ""; }
                        r.Add(new Joins.Apps.Models.News10.Cruz3.Article_List {
                            art_imgs = artimgList,
                            content = "",
                            date = date1,
                            id = JCube.AF.Util.Converts.ToString(dr["TOTAL_ID"], ""),
                            image = image1,
                            summaries = summaries,
                            summary = JCube.AF.Util.Converts.ToString(dr["ARTICLE_SUMMARY"], ""),
                            title = sTitle
                        });
                    }
                }
                return r;
            }
        }
    }
}

