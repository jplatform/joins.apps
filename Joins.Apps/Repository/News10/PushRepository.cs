﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Joins.Apps.Models.News10;
using System.Data;

namespace Joins.Apps.Repository.News10 {
    public class PushRepository {

        #region 리스트
        public static List<PushListData> List(PushListParameters parameters) {
            var returnList = new List<PushListData>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@DEV_TYPE", parameters.deviceType);
                helper.ProcParamAdd("@PUSH_TYPE", parameters.pushType);
                DataSet ds = helper.ProcExecuteDataSet("UPU_MYNEWS_APP_DEVICE_PUSH_LIST");
                helper.Close();

                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        returnList.Add(new PushListData {
                            id = dr["DEV_ID"].ToString(),
                            token = dr["DEV_TOKEN"].ToString()
                        });
                    }
                }
                return returnList;
            }
        }
        public static List<PushListData> List_FCM(PushListParameters parameters) {
            var returnList = new List<PushListData>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@DEV_TYPE", parameters.deviceType);
                helper.ProcParamAdd("@PUSH_TYPE", parameters.pushType);
                DataSet ds = helper.ProcExecuteDataSet("UPU_MYNEWS_APP_DEVICE_PUSH_LIST_FCM");
                helper.Close();

                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        returnList.Add(new PushListData {
                            id = dr["DEV_ID"].ToString(),
                            token = dr["DEV_TOKEN"].ToString()
                        });
                    }
                }
                return returnList;
            }
        }
        public static List<NotiPushListData> NotiList(PushListParameters2 parameters) {
            var returnList = new List<NotiPushListData>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@DEV_TYPE", parameters.deviceType);
                helper.ProcParamAdd("@PUSH_TYPE", parameters.pushType);
                DataSet ds = helper.ProcExecuteDataSet("UPU_NEWS10_SEND_PUSH_LIST");
                helper.Close();

                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        returnList.Add(new NotiPushListData {
                            devType = dr["DEV_TYPE"].ToString(),
                            token = dr["DEV_TOKEN"].ToString(),
                            message = dr["PUSH_MSG"].ToString()
                        });
                    }
                }
                return returnList;
            }
        }

        #endregion

        #region 로그 - 리스트

        public static List<PushLogListData> LogList(PushLogListParameters parameters) {
            var returnList = new List<PushLogListData>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@pageNum", parameters.pageNum);
                helper.ProcParamAdd("@pageSize", parameters.pageSize);
                helper.ProcParamAdd("@pushType", parameters.pushType);
                helper.ProcParamAdd("@startDate", parameters.startDate);
                DataSet ds = helper.ProcExecuteDataSet("UPU_MYNEWS_APP_DEVICE_PUSH_LOG_LIST");
                helper.Close();

                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        returnList.Add(new PushLogListData {
                            id = dr["LOG_ID"].ToString(),
                            type = dr["PUSH_TYPE"].ToString(),
                            message = dr["PUSH_MESSAGE"].ToString(),
                            date = dr["LOG_DATE"].ToString()
                        });
                    }
                }
                return returnList;
            }
        }
        public static List<PushLogListData> NotiLogList(PushLogListParameters parameters) {
            var returnList = new List<PushLogListData>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@pageNum", parameters.pageNum);
                helper.ProcParamAdd("@pageSize", parameters.pageSize);
                helper.ProcParamAdd("@pushType", parameters.pushType);
                helper.ProcParamAdd("@startDate", parameters.startDate);
                DataSet ds = helper.ProcExecuteDataSet("UPU_NEWS10_APP_DEVICE_PUSH_LOG_LIST");
                helper.Close();

                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        returnList.Add(new PushLogListData {
                            id = dr["LOG_ID"].ToString(),
                            type = dr["PUSH_TYPE"].ToString(),
                            message = dr["PUSH_MESSAGE"].ToString(),
                            date = dr["LOG_DATE"].ToString()
                        });
                    }
                }
                return returnList;
            }
        }
        #endregion

        #region 로그 - 업데이트

        public static bool LogUpdate(PushLogUpdateParameters parameters) {
            bool returnValue = false;
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@PUSH_TYPE", parameters.pushType);
                helper.ProcParamAdd("@PUSH_MESSAGE", parameters.pushMessage);
                helper.ProcParamAdd("@PUSH_COUNT", parameters.pushCount);
                int r = helper.ProcExecuteScalar("UPU_MYNEWS_APP_DEVICE_PUSH_LOG_PROC", -1);
                if (r == 1) { returnValue = true; }
                helper.Close();
                return returnValue;
            }
        }
        public static bool NotiLogUpdate(PushLogUpdateParameters parameters) {
            bool returnValue = false;
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@PUSH_TYPE", parameters.pushType);
                helper.ProcParamAdd("@PUSH_MESSAGE", parameters.pushMessage);
                helper.ProcParamAdd("@PUSH_COUNT", parameters.pushCount);
                int r = helper.ProcExecuteScalar("UPU_NEWS10_APP_DEVICE_PUSH_LOG_PROC", -1);
                if (r == 1) { returnValue = true; }
                helper.Close();
                return returnValue;
            }
        }
        #endregion
        #region [알림 푸쉬]
        public static IEnumerable<Joins.Apps.Models.News10.NotiPush> GetNotiPushList(int pgi, int ps, int seq,string uyn, out int tot_cnt) {
            List<Joins.Apps.Models.News10.NotiPush> rlist = new List<Joins.Apps.Models.News10.NotiPush>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@PageIndex", pgi);
                helper.ProcParamAdd("@PageSize", ps);
                helper.ProcParamAdd("@SEQ", seq);
                helper.ProcParamAdd("@PUSH_TYPE", "");
                helper.ProcParamAdd("@USED_YN", uyn);
                helper.ProcParamAdd("@OS_DIV", "");
                DataSet ds = helper.ProcExecuteDataSet("UPU_NEWS10_CMS_PUSH_LIST");
                helper.Close();
                tot_cnt = 0;
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    tot_cnt = cnt;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        tot_cnt = JCube.AF.Util.Converts.ToInt32(dr["T_COUNT"], 0);
                        rlist.Add(new Joins.Apps.Models.News10.NotiPush {
                            ROWNUM = JCube.AF.Util.Converts.ToInt32(dr["ROWNUM"], 0),
                            SEQ = JCube.AF.Util.Converts.ToInt32(dr["SEQ"], 0),
                            USED_YN = JCube.AF.Util.Converts.ToString(dr["USED_YN"], ""),
                            PUSH_TYPE = JCube.AF.Util.Converts.ToString(dr["PUSH_TYPE"], ""),
                            OS_DIV = JCube.AF.Util.Converts.ToString(dr["OS_DIV"], ""),
                            PUSH_MSG = JCube.AF.Util.Converts.ToString(dr["PUSH_MSG"], ""),
                            PUSH_TIME = JCube.AF.Util.Converts.ToString(dr["PUSH_TIME"], ""),
                            REG_ID = JCube.AF.Util.Converts.ToString(dr["REG_ID"], ""),
                            REG_DT = JCube.AF.Util.Converts.ToString(dr["REG_DT"], ""),
                            MOD_ID = JCube.AF.Util.Converts.ToString(dr["MOD_ID"], ""),
                            MOD_DT = JCube.AF.Util.Converts.ToString(dr["MOD_DT"], ""),
                        });
                    }
                }
                return rlist;
            }
        }

        public static int SetNotiPushEdit(Joins.Apps.Models.News10.NotiPush exc) {
            Joins.Apps.Models.News10.NotiPush itm = new Joins.Apps.Models.News10.NotiPush();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@SEQ", exc.SEQ);
                helper.ProcParamAdd("@PUSH_TYPE", exc.PUSH_TYPE);
                helper.ProcParamAdd("@USED_YN", exc.USED_YN);
                helper.ProcParamAdd("@OS_DIV", exc.OS_DIV);
                helper.ProcParamAdd("@PUSH_MSG", exc.PUSH_MSG);
                helper.ProcParamAdd("@PUSH_TIME", exc.PUSH_TIME);
                helper.ProcParamAdd("@EDITOR", exc.REG_ID);
                int r = helper.ProcExecuteScalar("UPU_NEWS10_CMS_PUSH_EDIT", -1);
                helper.Close();
                return r;
            }
        }
        #endregion
    }
}
