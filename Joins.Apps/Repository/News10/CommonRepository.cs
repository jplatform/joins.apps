﻿using System;
using System.Collections.Generic;
using System.Web.Configuration;
using System.Xml;
using System.Data;
using Joins.Apps.Models.News10;
using System.Collections;

namespace Joins.Apps.Repository.News10 {
   public class CommonRepository {

        #region 앱 - 정보

        public static List<Joins.Apps.Models.News10.AppInfoData> AppInfo(string deviceType) {
            List<Joins.Apps.Models.News10.AppInfoData> returnList = new List<Joins.Apps.Models.News10.AppInfoData>();

            string xmlPath = WebConfigurationManager.AppSettings["StaticUrl"] + "/scripts/data/mynews/AppInfo2.xml";
            XmlDocument xml = new XmlDocument();
            xml.Load(xmlPath);
            XmlNodeList xmlList = xml.SelectNodes("/AppInfo/app");

            foreach (XmlNode xn in xmlList) {
                string type = xn["type"].InnerText;
                string version = xn["version"].InnerText;

                if (string.IsNullOrEmpty(deviceType) || deviceType == type) {
                    returnList.Add(new Joins.Apps.Models.News10.AppInfoData() {
                        type = xn["type"].InnerText,
                        version = xn["version"].InnerText,
                        //version = "1.1.3",
                        minorVersion = xn["minorVersion"].InnerText,
                       // minorVersion = "1.1.3",
                        ver_notice = xn["ver_notice"].InnerText,
                        update = xn["update"].InnerText,
                        //update = "Y",
                        market_info = xn["market_info"].InnerText
                    });
                }
            }

            return returnList;
        }

        #endregion

        #region 앱 - 디바이스 - 조회

        public static AppDeviceData AppDevice(AppDeviceParameters parameters) {
            var returnList = new AppDeviceData();
            var returnPush = new List<AppDevicePushData>();
            string[] stypelist = new string[] { "morning", "afternoon", "evening", "night","noti" };
            string[] stimelist = new string[] { "06:00", "12:00", "18:00", "00:00","" };
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@DEV_ID", parameters.deviceID);
                DataSet ds = helper.ProcExecuteDataSet("UPU_MYNEWS_APP_DEVICE_VIEW");
                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        returnList.type = dr["DEV_TYPE"].ToString();
                        returnList.token = dr["DEV_TOKEN"].ToString();
                        returnList.gender = dr["DEV_GENDER"].ToString();
                        returnList.birthday = dr["DEV_BIRTHDAY"].ToString();
                    }
                    if (ds.Tables[1].Rows != null) {
                        Hashtable hpushlist = new Hashtable();
                        int cnt1 = ds.Tables[1].Rows.Count;
                        for (int tmpi = 0; tmpi < cnt1; tmpi++) {
                            DataRow dr = ds.Tables[1].Rows[tmpi];
                            string sType = dr["PUSH_TYPE"].ToString();
                            string sTime = "";
                            try { sTime = Convert.ToString(dr["PUSH_TIME"]); } catch { }
                            if (string.IsNullOrEmpty(sTime)) {
                                switch (sType) {
                                    case "morning":
                                        sTime = stimelist[0];
                                        break;
                                    case "afternoon":
                                        sTime = stimelist[1];
                                        break;
                                    case "evening":
                                        sTime = stimelist[2];
                                        break;
                                    case "night":
                                        sTime = stimelist[3];
                                        break;
                                }
                            }
                            if (!hpushlist.ContainsKey(sType)) {
                                hpushlist.Add(sType, new AppDevicePushData {
                                    type = sType,
                                    flag = dr["PUSH_FLAG"].ToString(),
                                    time = sTime
                                });
                            }
                        }

                        for (int i = 0; i < stypelist.Length; i++) {
                            if (hpushlist.ContainsKey(stypelist[i])) { returnPush.Add((AppDevicePushData)hpushlist[stypelist[i]]); }
                            else {   // 없는 경우 기본값을 노출
                                returnPush.Add(new AppDevicePushData {
                                    type = stypelist[i],
                                    flag = "N",
                                    time = stimelist[i]
                                });
                            }
                        }
                        returnList.Push = returnPush;

                    }
                }
                return returnList;
            }
        }

        #endregion

        #region 앱 - 디바이스 - 업데이트

        public static bool AppDeviceUpdate(AppDeviceUpdateParameters parameters) {
            bool returnValue = false;
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@DEV_ID", parameters.deviceID);
                helper.ProcParamAdd("@DEV_TYPE", parameters.deviceType);
                helper.ProcParamAdd("@DEV_TOKEN", parameters.deviceToken);
                helper.ProcParamAdd("@DEV_VER", parameters.deviceVersion);
                helper.ProcParamAdd("@DEV_INFO", parameters.deviceInfo);
                helper.ProcParamAdd("@APP_VER", parameters.AppVersion);
                int r = helper.ProcExecuteScalar("UPU_MYNEWS_APP_DEVICE_PROC", -1);
                if (r == 1) { returnValue = true; }
                helper.Close();
                return returnValue;
            }
        }

        #endregion

        #region 앱 - 디바이스 - 부가정보 - 업데이트

        public static bool AppDeviceAdditionUpdate(AppDeviceUpdateParameters parameters) {
            bool returnValue = false;
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@DEV_ID", parameters.deviceID);
                helper.ProcParamAdd("@DEV_TYPE", parameters.deviceType);
                helper.ProcParamAdd("@DEV_GENDER", parameters.deviceGender);
                helper.ProcParamAdd("@DEV_BIRTHDAY", parameters.deviceBirthday);
                int r = helper.ProcExecuteScalar("UPU_MYNEWS_APP_DEVICE_ADDITION_PROC", -1);
                if (r == 1) { returnValue = true; }
                helper.Close();
                return returnValue;
            }
        }

        #endregion

        #region 앱 - 디바이스 - 푸시 업데이트

        public static bool AppDevicePushUpdate(AppDevicePushUpdateParameters parameters) {
            bool returnValue = false;
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@DEV_ID", parameters.deviceID);
                helper.ProcParamAdd("@PUSH_TYPE", parameters.pushType);
                helper.ProcParamAdd("@PUSH_FLAG", parameters.pushFlag);
                helper.ProcParamAdd("@PUSH_TIME", parameters.pushTime);
                int r = helper.ProcExecuteScalar("UPU_MYNEWS_APP_DEVICE_PUSH_PROC", -1);
                if (r == 1) { returnValue = true; }
                helper.Close();
                return returnValue;
            }
        }

        #endregion

        #region 앱 - api 호출 정보
        public static bool AppApiLogInert(AppApiLogParameters parameters) {
            bool returnValue = false;
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@DEV_ID", parameters.deviceID);
                helper.ProcParamAdd("@API_TYPE", parameters.type);
                helper.ProcParamAdd("@API_URL", parameters.apiurl);
                helper.ProcParamAdd("@RT_MSG", parameters.resultMsg);
                int r = helper.ProcExecuteScalar("UPU_MYNEWS_APP_API_LOG", -1);
                if (r == 1) { returnValue = true; }
                helper.Close();
                return returnValue;
            }
        }

        public static bool ArticleLogInert(ArticleLogParameters parameters) {
            bool returnValue = false;

            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@DEV_ID", parameters.deviceID);
                helper.ProcParamAdd("@LOG_DIV", parameters.div);
                helper.ProcParamAdd("@CARD_ID", parameters.cardID);
                helper.ProcParamAdd("@TOTAL_ID", parameters.totalID);
                int r = helper.ProcExecuteScalar("UPU_NEWS10_ARTICLE_LOG", -1);
                if (r == 1) { returnValue = true; }
                helper.Close();

                return returnValue;
            }
        }
        #endregion

        #region [좋아요]
        public static Joins.Apps.Models.News10.ArticleLikeData GetArticleLikeInfo(int total_id,string devid = "") {
            Joins.Apps.Models.News10.ArticleLikeData r = new Joins.Apps.Models.News10.ArticleLikeData();
            JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper();
            helper.ProcParamClear();
            helper.ProcParamAdd("@TOTAL_ID", total_id);
            helper.ProcParamAdd("@DEV_ID", devid);
            DataSet ds = helper.ProcExecuteDataSet("USP_NEWS10_ARTICLELIKE_INFO");
            helper.Close();
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                int cnt = ds.Tables[0].Rows.Count;
                DataRow dr = ds.Tables[0].Rows[0];
                int iTotLikeCnt = JCube.AF.Util.Converts.ToInt32(dr["TOT_LIKE_CNT"], 0);
                int iLikeCnt = JCube.AF.Util.Converts.ToInt32(dr["LIKE_CNT"], 0);
                r =new Joins.Apps.Models.News10.ArticleLikeData { total_likecnt= iTotLikeCnt,likecnt =iLikeCnt};
            }
            return r;
        }
        public static bool SetArticleLikeUpdate(ArticleLikeParam parameters) {
            bool returnValue = false;
            JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper();
            helper.ProcParamClear();
            helper.ProcParamAdd("@DEV_ID", parameters.deviceID);
            helper.ProcParamAdd("@TOTAL_ID", parameters.totalID);
            helper.ProcParamAdd("@CARD_ID", parameters.cardID);
            helper.ProcParamAdd("@LIKE_CNT", parameters.likeCnt);
            int r = helper.ProcExecuteScalar("USP_NEWS10_ARTICLELIKE_INSERT", -1);
            if (r == 1) { returnValue = true; }
            helper.Close();
            return returnValue;
        }
        #endregion

        #region [브리핑 기사 되돌리기]
        public static Joins.Apps.Models.News10.BriefRestoreData GetBriefRestore() {
            Joins.Apps.Models.News10.BriefRestoreData r = new Joins.Apps.Models.News10.BriefRestoreData();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                DataSet ds = helper.ProcExecuteDataSet("USP_NEWS10_GetBriefRestoreInfo");
                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    DataRow dr = ds.Tables[0].Rows[0];
                    r = new Joins.Apps.Models.News10.BriefRestoreData {
                        used_yn = JCube.AF.Util.Converts.ToString(dr["USED_YN"], ""),
                        stat_dt = JCube.AF.Util.Converts.ToString(dr["STAT_DT"], ""),
                        reg_dt = JCube.AF.Util.Converts.ToString(dr["REG_DT"], "")
                    };
                }
                return r;
            }
        }

        public static bool SetBriefRestore(string yn ="") {
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@USED_YN", yn);
                bool r = helper.ProcExecuteNonQuery("USP_NEWS10_SetBriefRestore") > 0;
                helper.Close();
                return r;
            }
        }
        #endregion
    }
}
