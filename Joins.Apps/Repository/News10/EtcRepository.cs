﻿using System;
using System.Collections.Generic;
using System.Web.Configuration;
using Joins.Apps.Models.News10;
using System.Xml;
using System.Data;

namespace Joins.Apps.Repository.News10 {
    public class EtcRepository {
        #region 날씨

        public static WeatherData Weather(string paramarea) {
            var returnList = new WeatherData();
            var returnForecast = new List<WeatherForecastData>();

            var areaList = new Dictionary<string, string>()
            {
                { "서울", "서울특별시" },
                { "부산", "부산광역시" },
                { "대구", "대구광역시" },
                { "인천", "인천광역시" },
                { "광주", "광주광역시" },
                { "대전", "대전광역시" },
                { "울산", "울산광역시" },
                { "세종", "세종특별자치시" },
                { "경기", "경기도" },
                { "강원", "강원도" },
                { "충북", "충청북도" },
                { "충남", "충청남도" },
                { "전북", "전라북도" },
                { "전남", "전라남도" },
                { "경북", "경상북도" },
                { "경남", "경상남도" },
                { "충청북도", "충청북도" },
                { "충청남도", "충청남도" },
                { "전라북도", "전라북도" },
                { "전라남도", "전라남도" },
                { "경상북도", "경상북도" },
                { "경상남도", "경상남도" },
                { "제주", "제주특별자치도" }
            };

            foreach (KeyValuePair<string, string> area in areaList) {
                if (paramarea.IndexOf(area.Key) > -1) {
                    paramarea = area.Value;
                    break;
                }
            }
            string xmlPath = "";
            XmlNodeList xmlList = null;
            try {
                xmlPath = WebConfigurationManager.AppSettings["StaticUrl"] + "/scripts/data/weather/now.xml";
              //  xmlPath = WebConfigurationManager.AppSettings["StaticUrl"] + "/scripts/data/weather/now1.xml";//테스트를 위해 2017.10.30
                XmlDocument xml = new XmlDocument();
                xml.Load(xmlPath);

                xmlList = xml.SelectNodes("/weather/area");
                foreach (XmlNode xn in xmlList) {
                    string admin = xn["admin"].InnerText;
                    double temp = Convert.ToDouble(xn["temp"].InnerText);
                }
            } catch {
                object oCacheXmlList = Joins.Apps.Common.Util.GetCache("News10_weather");
                if (oCacheXmlList != null) { xmlList = (XmlNodeList)oCacheXmlList; }
            }
            /*
             XmlDocument xml = new XmlDocument();
             xml.Load(xmlPath);
             XmlNodeList xmlList = xml.SelectNodes("/weather/area");
           */
            if (xmlList == null) { return null; }
            foreach (XmlNode xn in xmlList) {
                string admin = xn["admin"].InnerText;

                if (admin == paramarea) {
                    double temp = Convert.ToDouble(xn["temp"].InnerText);
                    double tempDif = 0;
                    if (!string.IsNullOrEmpty(xn["tempBef"].InnerText))
                        tempDif = temp - Convert.ToDouble(xn["tempBef"].InnerText);

                    returnList.admin = admin;
                    returnList.condition = xn["condition"].InnerText;
                    returnList.conditionText = xn["conditionText"].InnerText;
                    returnList.temp = temp.ToString();
                    returnList.tempMax = xn["tempMax"].InnerText;
                    returnList.tempMin = xn["tempMin"].InnerText;
                    returnList.tempDif = tempDif.ToString();
                    returnList.pm10 = xn["pm10"].InnerText;
                    returnList.pm10Grade = xn["pm10Grade"].InnerText;

                    XmlNodeList xmlList2 = xn.SelectNodes("./forecasts/forecast");
                    foreach (XmlNode xn2 in xmlList2) {
                        returnForecast.Add(new WeatherForecastData {
                            time = xn2["time"].InnerText,
                            condition = xn2["condition"].InnerText,
                            conditionText = xn2["conditionText"].InnerText,
                            temp = xn2["temp"].InnerText,
                            rain = xn2["rain"].InnerText
                        });
                    }

                    returnList.Forecast = returnForecast;
                }
            }
            object oCacheDt = Joins.Apps.Common.Util.GetCache("News10_weather_dt");
            if (oCacheDt == null | Convert.ToDateTime(oCacheDt) <= DateTime.Now.AddMinutes(-10)) {
                Joins.Apps.Common.Util.SetCache("News10_weather_dt", DateTime.Now, 10);
                Joins.Apps.Common.Util.SetCache("News10_weather", xmlList, 30);
            }
            return returnList;
        }

        #endregion

        #region 기념일

        public static List<SpecialDayData> SpecialDay(SpecialDayParameters parameters) {
            List<SpecialDayData> returnList = new List<SpecialDayData>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@SPECIAL_DAY", parameters.day);
                //   helper.ProcParamAdd("@service", parameters.service);
                DataSet ds = helper.ProcExecuteDataSet("UPU_NEWS10_SPECIALDAY");
                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        returnList.Add(new SpecialDayData {
                            day = dr["SPECIAL_NAME"].ToString(),
                            special_type = dr["SPECIAL_TYPE_NAME"].ToString()
                            //  day = dr["DAY_TITLE"].ToString(),
                            // service = dr["SP_CARD"].ToString()
                        });
                    }
                }
                return returnList;
            }
        }

        #endregion
    }
}
