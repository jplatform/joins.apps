﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Joins.Apps.Repository.News10 {
    public class CruzRepository {
        public static List<Joins.Apps.Models.News10.Cruz.News10> GetCruzNews10List(int gen,int age,bool bBr=false) {
            List<Joins.Apps.Models.News10.Cruz.News10> r = new List<Joins.Apps.Models.News10.Cruz.News10>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@GEN", gen);
                helper.ProcParamAdd("@AGE", age);
                DataSet ds = helper.ProcExecuteDataSet("USP_NEWS10_LIST_2018");
                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        string content1 = JCube.AF.Util.Converts.ToString(dr["ARTICLE_CONTENT"], "");
                        content1 = GetCotentReFine(content1, bBr);
                        List<Joins.Apps.Models.News10.Cruz.Art_imgs> artimgList = null;
                        List<Joins.Apps.Models.News10.Cruz.Summaries> summaries = null;
                        try {
                            artimgList = JsonConvert.DeserializeObject<List<Joins.Apps.Models.News10.Cruz.Art_imgs>>(JCube.AF.Util.Converts.ToString(dr["ARTICLE_IMGS"], ""));
                            summaries = JsonConvert.DeserializeObject<List<Joins.Apps.Models.News10.Cruz.Summaries>>(JCube.AF.Util.Converts.ToString(dr["ARTICLE_SUMMARIES"], ""));
                        } catch (Exception em) { Joins.Apps.Common.Logger.LogWriteError("이미지,요약 오류", em.Message); }
                        string date1 = "", image1 = "";
                        try {
                            DateTime dt = DateTime.ParseExact(dr["SERVICE_DT"].ToString(), "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                            date1 = dt.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                        } catch { }
                        if (artimgList != null && artimgList.Count > 0) { image1 = artimgList[0].img_url; }
                        r.Add(new Joins.Apps.Models.News10.Cruz.News10 {
                            art_imgs = artimgList,
                            content = content1.Trim(),
                            date = date1,
                            id = JCube.AF.Util.Converts.ToString(dr["TOTAL_ID"], ""),
                            image = image1,
                            summaries = summaries,
                            summary = JCube.AF.Util.Converts.ToString(dr["ARTICLE_SUMMARY"], ""),
                            title = Joins.Apps.Common.Util.DisplayText(JCube.AF.Util.Converts.ToString(dr["ARTICLE_TITLE"], "")).Trim()
                        });
                    }
                }
                return r;
            }
        }
        public static List<Joins.Apps.Models.News10.Cruz.TTS> GetCruzNews10OnlyTTSList(int gen, int age) {
            List<Joins.Apps.Models.News10.Cruz.TTS> r = new List<Joins.Apps.Models.News10.Cruz.TTS>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@GEN", gen);
                helper.ProcParamAdd("@AGE", age);
                DataSet ds = helper.ProcExecuteDataSet("USP_NEWS10_LIST_2018");
                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        string content1 = JCube.AF.Util.Converts.ToString(dr["ARTICLE_CONTENT"], "");
                        content1 = GetCotentReFine(content1, false);
                        string cont1 = Joins.Apps.Common.Util.HTMLRemove(content1);
                        string cont2 = Joins.Apps.Models.News10.Cruz.GetTTSWord(cont1);
                        string title1 = Joins.Apps.Common.Util.GetRexValue(JCube.AF.Util.Converts.ToString(dr["ARTICLE_TITLE"], ""));
                        title1 = Joins.Apps.Common.Util.HTMLRemove(title1);
                        string title2 = Joins.Apps.Models.News10.Cruz.GetTTSWord(title1);
                        string summary1 = Joins.Apps.Common.Util.HTMLRemove(JCube.AF.Util.Converts.ToString(dr["ARTICLE_SUMMARY"], ""));
                        string summary2 = Joins.Apps.Models.News10.Cruz.GetTTSWord(summary1);

                        r.Add(new Joins.Apps.Models.News10.Cruz.TTS {
                            content = cont2,
                            summary = summary2,
                            title = title2
                        });
                    }
                }
                return r;
            }
        }
        public static List<Joins.Apps.Models.News10.Cruz.News10_NOContentTTS> GetCruzNews10NoContentTTSList(int gen, int age, bool bBr = false) {
            List<Joins.Apps.Models.News10.Cruz.News10_NOContentTTS> r = new List<Joins.Apps.Models.News10.Cruz.News10_NOContentTTS>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@GEN", gen);
                helper.ProcParamAdd("@AGE", age);
                DataSet ds = helper.ProcExecuteDataSet("USP_NEWS10_LIST_2018");
                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        List<Joins.Apps.Models.News10.Cruz.Art_imgs> artimgList = null;
                      //  List<Joins.Apps.Models.News10.Cruz.Summaries> summaries = null;
                        try {
                            artimgList = JsonConvert.DeserializeObject<List<Joins.Apps.Models.News10.Cruz.Art_imgs>>(JCube.AF.Util.Converts.ToString(dr["ARTICLE_IMGS"], ""));
                        //    summaries = JsonConvert.DeserializeObject<List<Joins.Apps.Models.News10.Cruz.Summaries>>(JCube.AF.Util.Converts.ToString(dr["ARTICLE_SUMMARIES"], ""));
                        } catch (Exception em) { Joins.Apps.Common.Logger.LogWriteError("이미지,요약 오류", em.Message); }
                        string date1 = "", image1 = "";
                        try {
                            DateTime dt = DateTime.ParseExact(dr["SERVICE_DT"].ToString(), "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                            date1 = dt.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                        } catch { }
                        if (artimgList != null && artimgList.Count > 0) { image1 = artimgList[0].img_url; }
                        r.Add(new Joins.Apps.Models.News10.Cruz.News10_NOContentTTS {
                            art_imgs = artimgList,
                            date = date1,
                            id = JCube.AF.Util.Converts.ToString(dr["TOTAL_ID"], ""),
                            image = image1,
                            /*summaries = summaries,*/
                            summaries = null,
                            summary = JCube.AF.Util.Converts.ToString(dr["ARTICLE_SUMMARY"], ""),
                            title = Joins.Apps.Common.Util.DisplayText(JCube.AF.Util.Converts.ToString(dr["ARTICLE_TITLE"], "")).Trim()
                        });
                    }
                }
                return r;
            }
        }
        public static List<Joins.Apps.Models.News10.Cruz.News10_NOTTS> GetCruzNews10NoTTSList(int gen, int age, bool bBr = false) {
            List<Joins.Apps.Models.News10.Cruz.News10_NOTTS> r = new List<Joins.Apps.Models.News10.Cruz.News10_NOTTS>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@GEN", gen);
                helper.ProcParamAdd("@AGE", age);
                DataSet ds = helper.ProcExecuteDataSet("USP_NEWS10_LIST_2018");
                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        List<Joins.Apps.Models.News10.Cruz.Art_imgs> artimgList = null;
                        List<Joins.Apps.Models.News10.Cruz.Summaries> summaries = null;
                        try {
                            artimgList = JsonConvert.DeserializeObject<List<Joins.Apps.Models.News10.Cruz.Art_imgs>>(JCube.AF.Util.Converts.ToString(dr["ARTICLE_IMGS"], ""));
                            summaries = JsonConvert.DeserializeObject<List<Joins.Apps.Models.News10.Cruz.Summaries>>(JCube.AF.Util.Converts.ToString(dr["ARTICLE_SUMMARIES"], ""));
                        } catch (Exception em) { Joins.Apps.Common.Logger.LogWriteError("이미지,요약 오류", em.Message); }
                        string date1 = "", image1 = "";
                        try {
                            DateTime dt = DateTime.ParseExact(dr["SERVICE_DT"].ToString(), "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                            date1 = dt.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                        } catch { }
                        if (artimgList != null && artimgList.Count > 0) { image1 = artimgList[0].img_url; }
                        r.Add(new Joins.Apps.Models.News10.Cruz.News10_NOTTS {
                            art_imgs = artimgList,
                            date = date1,
                            id = JCube.AF.Util.Converts.ToString(dr["TOTAL_ID"], ""),
                            image = image1,
                            summaries = summaries,
                            summary = JCube.AF.Util.Converts.ToString(dr["ARTICLE_SUMMARY"], ""),
                            title = Joins.Apps.Common.Util.DisplayText(JCube.AF.Util.Converts.ToString(dr["ARTICLE_TITLE"], "")).Trim()
                        });
                    }
                }
                return r;
            }
        }

        public static List<Joins.Apps.Models.News10.Cruz.News10> GetCruzNews10ListTest(int gen, int age) {
            List<Joins.Apps.Models.News10.Cruz.News10> r = new List<Joins.Apps.Models.News10.Cruz.News10>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@GEN", gen);
                helper.ProcParamAdd("@AGE", age);
                DataSet ds = helper.ProcExecuteDataSet("USP_NEWS10_LIST_2018_TEST");

                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        string content1 = JCube.AF.Util.Converts.ToString(dr["ARTICLE_CONTENT"], "");
                        content1 = GetCotentReFine(content1);
                        List<Joins.Apps.Models.News10.Cruz.Art_imgs> artimgList = null;
                        List<Joins.Apps.Models.News10.Cruz.Summaries> summaries = null;
                        try {
                            artimgList = JsonConvert.DeserializeObject<List<Joins.Apps.Models.News10.Cruz.Art_imgs>>(JCube.AF.Util.Converts.ToString(dr["ARTICLE_IMGS"], ""));
                            summaries = JsonConvert.DeserializeObject<List<Joins.Apps.Models.News10.Cruz.Summaries>>(JCube.AF.Util.Converts.ToString(dr["ARTICLE_SUMMARIES"], ""));
                        } catch { }
                        string date1 = "", image1 = "";
                        try {
                            DateTime dt = DateTime.ParseExact(dr["SERVICE_DT"].ToString(), "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                            date1 = dt.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                        } catch { }
                        if (artimgList != null && artimgList.Count > 0) { image1 = artimgList[0].img_url; }
                        r.Add(new Joins.Apps.Models.News10.Cruz.News10 {
                            art_imgs = artimgList,
                            content = content1.Trim(),
                            date = date1,
                            id = JCube.AF.Util.Converts.ToString(dr["TOTAL_ID"], ""),
                            image = image1,
                            summaries = summaries,
                            summary = JCube.AF.Util.Converts.ToString(dr["ARTICLE_SUMMARY"], ""),
                            title = Joins.Apps.Common.Util.DisplayText(JCube.AF.Util.Converts.ToString(dr["ARTICLE_TITLE"], "")).Trim()
                        });
                    }
                }
                return r;
            }
        }
        public static List<Joins.Apps.Models.News10.Cruz2.News10> GetCruzNews10List2(int gen, int age) {
            List<Joins.Apps.Models.News10.Cruz2.News10> r = new List<Joins.Apps.Models.News10.Cruz2.News10>();
            List<Joins.Apps.Models.News10.Cruz.News10> CruzList = Joins.Apps.Repository.News10.CruzRepository.GetCruzNews10List(gen, age);
            foreach (Joins.Apps.Models.News10.Cruz.News10 cruzData in CruzList) {
                r.Add(new Joins.Apps.Models.News10.Cruz2.News10 {
                    art_imgs = cruzData.art_imgs,
                    content = cruzData.content,
                    date = cruzData.date,
                    id = cruzData.id,
                    image = cruzData.image,
                    summaries = cruzData.summaries,
                    summary = cruzData.summary,
                    title = cruzData.title
                });
            }

            return r;
        }
        public static List<Joins.Apps.Models.News10.Cruz.Special> GetCruzSpecialList(int gen, int age,int cardVersion) {
            List<Joins.Apps.Models.News10.Cruz.Special> r = new List<Joins.Apps.Models.News10.Cruz.Special>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@GEN", gen);
                helper.ProcParamAdd("@AGE", age);
                DataSet ds = helper.ProcExecuteDataSet("USP_NEWS10_LIST_CATEGORY_2018");
                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        string content1 = JCube.AF.Util.Converts.ToString(dr["ARTICLE_CONTENT"], "");
                        content1 = GetCotentReFine(content1);
                        List<Joins.Apps.Models.News10.Cruz.Art_imgs> artimgList = null;
                        List<Joins.Apps.Models.News10.Cruz.Summaries> summaries = null;
                        try {
                            artimgList = JsonConvert.DeserializeObject<List<Joins.Apps.Models.News10.Cruz.Art_imgs>>(JCube.AF.Util.Converts.ToString(dr["ARTICLE_IMGS"], ""));
                            summaries = JsonConvert.DeserializeObject<List<Joins.Apps.Models.News10.Cruz.Summaries>>(JCube.AF.Util.Converts.ToString(dr["ARTICLE_SUMMARIES"], ""));
                        } catch (Exception em) { Joins.Apps.Common.Logger.LogWriteError("이미지,요약 오류", em.Message); }
                        string date1 = "", image1 = "";
                        try {
                            DateTime dt = DateTime.ParseExact(dr["SERVICE_DT"].ToString(), "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                            date1 = dt.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                        } catch { }
                        if (artimgList != null && artimgList.Count > 0) { image1 = artimgList[0].img_url; }
                        string sCatNm = "";
                        if (cardVersion >= 10) {
                            switch (JCube.AF.Util.Converts.ToString(dr["CAT_NM"], "")) {
                                case "Sports":
                                    sCatNm = "스포츠";
                                    break;
                                case "Style":
                                    sCatNm = "스타일";
                                    break;
                                case "Travel":
                                    sCatNm = "여행";
                                    break;
                                case "Tech":
                                    sCatNm = "테크";
                                    break;
                                case "Wellbeing":
                                    sCatNm = "웰빙";
                                    break;
                                case "Culture":
                                    sCatNm = "컬처";
                                    break;
                                case "Money":
                                    sCatNm = "머니";
                                    break;
                                case "Movie":
                                    sCatNm = "영화";
                                    break;
                                case "Life":
                                    sCatNm = "라이프";
                                    break;
                                case "Entertainment":
                                    sCatNm = "연예";
                                    break;
                                default:
                                    sCatNm = "색션 모음";
                                    break;
                            }
                        }
                        else { sCatNm = JCube.AF.Util.Converts.ToString(dr["CAT_NM"], ""); }
                        r.Add(new Joins.Apps.Models.News10.Cruz.Special {
                            art_imgs = artimgList,
                            content = content1.Trim(),
                            date = date1,
                            category = sCatNm,
                            id = JCube.AF.Util.Converts.ToString(dr["TOTAL_ID"], ""),
                            image = image1,
                            summaries = summaries,
                            summary = JCube.AF.Util.Converts.ToString(dr["ARTICLE_SUMMARY"], ""),
                            title = Joins.Apps.Common.Util.DisplayText(JCube.AF.Util.Converts.ToString(dr["ARTICLE_TITLE"], "")).Trim()
                        });
                    }
                }
                return r;
            }
        }
        public static List<Joins.Apps.Models.News10.Cruz.HotKwd> GetCruzKeywordList() {
            List<Joins.Apps.Models.News10.Cruz.HotKwd> r = new List<Joins.Apps.Models.News10.Cruz.HotKwd>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                DataSet ds = helper.ProcExecuteDataSet("USP_NEWS10_KEYWORD_LIST");
                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        List<Joins.Apps.Models.News10.Cruz.Art_imgs> artimgList = new List<Joins.Apps.Models.News10.Cruz.Art_imgs>();
                        artimgList.Add(new Joins.Apps.Models.News10.Cruz.Art_imgs {
                            h = 0,
                            w = 0,
                            num = 1,
                            img_url = JCube.AF.Util.Converts.ToString(dr["IMG"], "")
                        });
                        List<Joins.Apps.Models.News10.Cruz.Summaries> summaries = new List<Models.News10.Cruz.Summaries>();
                        string date1 = "", image1 = "";
                        try {
                            DateTime dt = DateTime.ParseExact(dr["SERVICE_DT"].ToString(), "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                            date1 = dt.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                        } catch { }
                        //  if (artimgList != null && artimgList.Count > 0) { image1 = artimgList[0].img_url; }
                        r.Add(new Joins.Apps.Models.News10.Cruz.HotKwd {
                            art_imgs = artimgList,
                            date = date1,
                            kwd = JCube.AF.Util.Converts.ToString(dr["KWD"], ""),
                            id = JCube.AF.Util.Converts.ToString(dr["TOTAL_ID"], ""),
                            image = JCube.AF.Util.Converts.ToString(dr["IMG"], ""),
                            summaries = summaries,
                            summary = "",
                            title = Joins.Apps.Common.Util.DisplayText(JCube.AF.Util.Converts.ToString(dr["ARTICLE_TITLE"], "")).Trim()
                        });
                    }
                }
                return r;
            }
        }
        #region[테스트]
        public static List<Joins.Apps.Models.News10.Cruz.News10> GetCruzNews10List_TEST(int gen, int age) {
            List<Joins.Apps.Models.News10.Cruz.News10> r = new List<Joins.Apps.Models.News10.Cruz.News10>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@GEN", gen);
                helper.ProcParamAdd("@AGE", age);
                DataSet ds = helper.ProcExecuteDataSet("USP_NEW10_LIST_TEST");
                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        string content1 = JCube.AF.Util.Converts.ToString(dr["ARTICLE_CONTENT"], "");
                        content1 = Regex.Replace(content1, @"<p(?:\s *)class=.\bcaption\b[^\>]+(?:.*?)[^<\/]+</p>", string.Empty);

                        //관련기사 삭제
                        int iRelCnt = 0;
                        while (iRelCnt < 10) {
                            if (content1.IndexOf("<div class=\"ab_related_article\">") > 0) {
                                int iRelStart = content1.IndexOf("<div class=\"ab_related_article\">");
                                int iRelEnd = 0;
                                int iTmpEnd = iRelStart;
                                for (int i = 0; i < 3; i++) {
                                    if (iTmpEnd > 0) { iTmpEnd = content1.IndexOf("</div>", iTmpEnd + 6); iRelEnd = iTmpEnd; }
                                }
                                if (iRelEnd > 0 && iRelStart > 0 && (iRelEnd - iRelStart) > 0 && (iRelEnd - iRelStart) < 1000) {
                                    content1 = content1.Remove(iRelStart, iRelEnd - iRelStart + 6);
                                }
                            }
                            else { break; }
                            iRelCnt++;
                        }
                        content1 = Joins.Apps.Common.Util.HTMLRemove(content1);
                        content1 = Joins.Apps.Common.Util.DisplayText(content1);
                        content1 = Regex.Replace(content1, @"(\w+[\w\.]*)@(\w+[\w\.]*)\.([A-Za-z]+)", string.Empty);//메일주소 삭제
                        content1 = Regex.Replace(content1, @"\s+", " ");//공백2개이상 삭제
                        List<Joins.Apps.Models.News10.Cruz.Art_imgs> artimgList = JsonConvert.DeserializeObject<List<Joins.Apps.Models.News10.Cruz.Art_imgs>>(JCube.AF.Util.Converts.ToString(dr["ARTICLE_IMGS"], ""));
                        List<Joins.Apps.Models.News10.Cruz.Summaries> summaries = JsonConvert.DeserializeObject<List<Joins.Apps.Models.News10.Cruz.Summaries>>(JCube.AF.Util.Converts.ToString(dr["ARTICLE_SUMMARIES"], ""));
                        string date1 = "", image1 = "";
                        try {
                            DateTime dt = DateTime.ParseExact(dr["SERVICE_DT"].ToString(), "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                            date1 = dt.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                        } catch { }
                        if (artimgList != null && artimgList.Count > 0) { image1 = artimgList[0].img_url; }
                        r.Add(new Joins.Apps.Models.News10.Cruz.News10 {
                            art_imgs = artimgList,
                            content = content1.Trim(),
                            date = date1,
                            id = JCube.AF.Util.Converts.ToString(dr["TOTAL_ID"], ""),
                            image = image1,
                            summaries = summaries,
                            summary = JCube.AF.Util.Converts.ToString(dr["ARTICLE_SUMMARY"], ""),
                            title = Joins.Apps.Common.Util.DisplayText(JCube.AF.Util.Converts.ToString(dr["ARTICLE_TITLE"], "")).Trim()
                        });
                    }
                }
                return r;
            }
        }

        #endregion
        #region [기사 본문 정제]
        public static string GetCotentReFine(string cont,bool bBr=false) {
            try {
                cont = Regex.Replace(cont, @"<p(?:\s *)class=.\bcaption\b[^\>]+(?:.*?)[^<\/]+</p>", string.Empty);
                //관련기사 삭제
                int iRelCnt = 0;
                while (iRelCnt < 10) {
                    if (cont.IndexOf("<div class=\"ab_related_article\">") > 0) {
                        int iRelStart = cont.IndexOf("<div class=\"ab_related_article\">");
                        int iRelEnd = 0;
                        int iTmpEnd = iRelStart;
                        for (int i = 0; i < 3; i++) {
                            if (iTmpEnd > 0) { iTmpEnd = cont.IndexOf("</div>", iTmpEnd + 6); iRelEnd = iTmpEnd; }
                        }
                        if (iRelEnd > 0 && iRelStart > 0 && (iRelEnd - iRelStart) > 0 && (iRelEnd - iRelStart) < 1000) {
                            cont = cont.Remove(iRelStart, iRelEnd - iRelStart + 6);
                        }
                    }
                    else { break; }
                    iRelCnt++;
                }
                if (bBr) { cont = Joins.Apps.Common.Util.HTMLRemoveNotBr(cont); }
                else {cont = Joins.Apps.Common.Util.HTMLRemove(cont);}

                cont = Joins.Apps.Common.Util.DisplayText(cont);
                cont = Regex.Replace(cont, @"(\w+[\w\.]*)@(\w+[\w\.]*)\.([A-Za-z]+)", string.Empty);//메일주소 삭제
                cont = Regex.Replace(cont, @"\s+", " ");//공백2개이상 삭제
            } catch(Exception em) { Joins.Apps.Common.Logger.LogWriteError("본문 기사정제 오류", em.Message); }
            return cont;
        }
        #endregion
        public static Joins.Apps.Models.News10.Cruz.HotVod SetArticleListData(int pgi, int ps, string stype, string search, string sdt, string edt, string sourcecode) {
            Joins.Apps.Models.News10.Cruz.HotVod itm = null;
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@PageIndex", pgi);
                helper.ProcParamAdd("@PageSize", ps);
                helper.ProcParamAdd("@SearchType", stype);
                helper.ProcParamAdd("@SearchWord", search);
                helper.ProcParamAdd("@SearchSDay", sdt);
                helper.ProcParamAdd("@SearchEDay", edt);
                helper.ProcParamAdd("@SourceCode", sourcecode);

                DataSet ds = helper.ProcExecuteDataSet("USP_NEWS10_ARTICLE_LIST");
                helper.Close();
                int tot_cnt = 0;
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    tot_cnt = cnt;
                    DataRow dr = ds.Tables[0].Rows[0];
                    tot_cnt = JCube.AF.Util.Converts.ToInt32(dr["T_COUNT"], 0);
                    int iTotalID = JCube.AF.Util.Converts.ToInt32(dr["Total_ID"], 0);
                    string sTitle = JCube.AF.Util.Converts.ToString(dr["MOB_TITLE"], "");
                    if (string.IsNullOrEmpty(sTitle)) { sTitle = JCube.AF.Util.Converts.ToString(dr["ARTICLE_TITLE"], ""); }
                    string sServiceDay = JCube.AF.Util.Converts.ToString(dr["ServiceDay"], "");
                    string sServiceTime = JCube.AF.Util.Converts.ToString(dr["ServiceTime"], "");
                    string sThumbnail = JCube.AF.Util.Converts.ToString(dr["Article_ThumbNail"], "");
                    string sVodID = GetVodIDData(iTotalID);
                    if (string.IsNullOrEmpty(sVodID)) { return null; }
                    sThumbnail = string.IsNullOrEmpty(sThumbnail) ? "" : "http://pds.joins.com" + sThumbnail.Replace(".tn_120.jpg", "");
                    ;
                    List<Models.News10.Cruz.Art_imgs> art_img = new List<Models.News10.Cruz.Art_imgs>();
                    List<Models.News10.Cruz.Art_movs> art_movs = new List<Models.News10.Cruz.Art_movs>();
                    List<Joins.Apps.Models.News10.Cruz.Summaries> summaries = new List<Models.News10.Cruz.Summaries>();

                    art_img.Add(new Models.News10.Cruz.Art_imgs() {
                        img_url = sThumbnail,
                        h = 0,
                        w = 0
                    }
                    );
                    art_movs.Add(new Models.News10.Cruz.Art_movs() {
                        movie_type = "MV",
                        movie_url = sVodID
                        // movie_url = item.id
                    });
                    DateTime dt = DateTime.ParseExact(sServiceDay + sServiceTime, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                    string date1 = dt.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                    itm = new Models.News10.Cruz.HotVod() {
                        title = Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(sTitle)),
                        date = date1,
                        image = sThumbnail,
                        art_movs = art_movs,
                        art_imgs = art_img,
                        summary = "",
                        summaries = summaries
                    };

                }
                return itm;
            }
        }

        public static string GetVodIDData(int total_id) {
            //   total_id = 22066973;
            string r = null;
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@Total_ID", total_id);
                DataSet ds = helper.ProcExecuteDataSet("Usp_Article_Full_Content_Select");
                helper.Close();
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    DataRow dr = ds.Tables[0].Rows[0];
                    string Article_Content = JCube.AF.Util.Converts.ToString(dr["Article_Content"], "");
                    MatchCollection ematchList1 = Regex.Matches(Article_Content, @"[d][i][v][_][N][V][0-9]*");
                    foreach (Match match in ematchList1) {
                        r = (match.Value).Replace("div_", "");
                        if (!string.IsNullOrEmpty(r)) { break; }
                    }
                }
                return r;
            }
        }
    }
}
