﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Joins.Apps.Models.Search;
using Newtonsoft.Json.Linq;
using Joins.Apps.Common.Utilities;
using System.Globalization;

namespace Joins.Apps.Repository.Search {
    public interface IConverter<out T> {
        T Convert(dynamic result);
    }
    internal class FindConverter<T> : IConverter<T> where T : new() {
        // 통합 검색
        public T Convert(dynamic result) {
            if (typeof(UnifiedSearchResult) == typeof(T))
                return UnifiedConvert(result);
            if (typeof(NewsResult) == typeof(T))
                return NewsConvert(result);
            if (typeof(DefaultResult) == typeof(T))
                return SearchConvert(result);
            return default(T);
        }

        public UnifiedSearchResult UnifiedConvert(dynamic result) {
            var model = new UnifiedSearchResult();

            if (result == null || result.SearchQueryResult == null || result.SearchQueryResult.Collection == null)
                return model;

            var jarray = result.SearchQueryResult.Collection as JArray;
            if (jarray != null) {
                model.SearchResultDictionary = new Dictionary<SearchCategoryType, IEnumerable<SearchItem>>();
                model.SearchTotalCountDictionary = new Dictionary<SearchCategoryType, int>();
                foreach (dynamic collection in result.SearchQueryResult.Collection) {
                    string id = JsonDotNetUtility.GetValue<string>(collection["Id"] ?? new JsonDotNetUtility.NullValue());
                    switch (id) {
                        case "direct_link":
                            model.ShortCuts = GetShortCuts(collection);
                            break;
                        case "person":
                            model.Peoples = GetPeoples(collection);
                            model.PeopleTotalCount = GetTotalCount(collection);
                            break;
                        case "issue":
                            model.Issues = GetIssues(collection);
                            break;
                        case "reporter":
                            model.Reporters = GetReporters(collection);
                            break;
                        case "news_ja":
                        case "news":
                            model.SearchResultDictionary.Add(SearchCategoryType.News, GetNews(collection));
                            model.SearchTotalCountDictionary.Add(SearchCategoryType.News, GetTotalCount(collection));
                            break;
                        case "news_image":
                            model.SearchResultDictionary.Add(SearchCategoryType.Image, GetImages(collection));
                            model.SearchTotalCountDictionary.Add(SearchCategoryType.Image, GetTotalCount(collection));
                            break;
                        case "news_vod":
                            model.SearchResultDictionary.Add(SearchCategoryType.Video, GetVideos(collection));
                            model.SearchTotalCountDictionary.Add(SearchCategoryType.Video, GetTotalCount(collection));
                            break;
                        case "joins_pdf":
                            model.SearchResultDictionary.Add(SearchCategoryType.Joins, GetJoins(collection));
                            model.SearchTotalCountDictionary.Add(SearchCategoryType.Joins, GetTotalCount(collection));
                            break;
                        case "jplus":
                            model.SearchResultDictionary.Add(SearchCategoryType.Jplus, GetJplus(collection));
                            model.SearchTotalCountDictionary.Add(SearchCategoryType.Jplus, GetTotalCount(collection));
                            break;
                        case "blog":
                            model.SearchResultDictionary.Add(SearchCategoryType.Blog, GetBlogs(collection));
                            model.SearchTotalCountDictionary.Add(SearchCategoryType.Blog, GetTotalCount(collection));
                            break;
                    }
                }
            }

            return model;
        }

        // 뉴스 검색
        public NewsResult NewsConvert(dynamic result) {
            var model = new NewsResult();

            if (result == null || result.SearchQueryResult == null || result.SearchQueryResult.Collection == null)
                return model;

            model.SearchResultDictionary = new Dictionary<SearchCategoryType, IEnumerable<SearchItem>>();
            model.SearchTotalCountDictionary = new Dictionary<SearchCategoryType, int>();

            var sources = new List<Source>();

            var jarray = result.SearchQueryResult.Collection as JArray;
            if (jarray != null) {
                bool isOnlyJoongangSunday = true;
                bool isNotIncludeJoongangSunday = true;

                foreach (dynamic collection in result.SearchQueryResult.Collection) {
                    string id = JsonDotNetUtility.GetValue<string>(collection["Id"] ?? new JsonDotNetUtility.NullValue());
                    switch (id) {
                        case "issue":
                            model.Issues = GetIssues(collection);
                            break;
                        case "news":
                            model.SearchResultDictionary.Add(SearchCategoryType.TotalNews, GetNews(collection));
                            model.SearchTotalCountDictionary.Add(SearchCategoryType.TotalNews, GetTotalCount(collection));
                            break;
                        case "news_ja":
                            model.SearchResultDictionary.Add(SearchCategoryType.JoongangNews, GetNews(collection));
                            model.SearchTotalCountDictionary.Add(SearchCategoryType.JoongangNews, GetTotalCount(collection));
                            break;
                        case "ja_sunday":
                            model.SearchResultDictionary.Add(SearchCategoryType.JoongangSundayNews, GetNews(collection)); //2017-02-02
                            model.SearchTotalCountDictionary.Add(SearchCategoryType.JoongangSundayNews, GetTotalCount(collection));
                            //GetSources(sources, id, collection.MultiGroup);  //2017-02-02
                            break;
                        case "news_group":
                            //model.Sources = GetSources(id, collection.MultiGroup);
                            GetSources(sources, id, collection.MultiGroup);
                            break;
                    }
                }

                model.Sources = sources;

                if ((model.SearchResultDictionary.ContainsKey(SearchCategoryType.TotalNews) && model.SearchResultDictionary[SearchCategoryType.TotalNews].Any())
                    || (model.SearchResultDictionary.ContainsKey(SearchCategoryType.JoongangNews)
                        && model.SearchResultDictionary[SearchCategoryType.JoongangNews].Any()))
                    isOnlyJoongangSunday = false;

                if (!isOnlyJoongangSunday && (model.SearchResultDictionary.ContainsKey(SearchCategoryType.JoongangSundayNews)
                                              && model.SearchResultDictionary[SearchCategoryType.JoongangSundayNews] != null
                                              && model.SearchResultDictionary[SearchCategoryType.JoongangSundayNews].Any()))
                    isNotIncludeJoongangSunday = false;

                foreach (dynamic collection in result.SearchQueryResult.Collection) {
                    string id = JsonDotNetUtility.GetValue<string>(collection["Id"] ?? new JsonDotNetUtility.NullValue());

                    if (isOnlyJoongangSunday && id.Equals("ja_sunday") && collection.MultiGroup != null) {
                        model.Services = GetServices(id, collection.MultiGroup);
                        model.Reporters = GetReporters(id, collection.MultiGroup);
                    }

                    if (isNotIncludeJoongangSunday && id.Equals("news") && collection.MultiGroup != null) {
                        model.Services = GetServices(id, collection.MultiGroup);
                        model.Reporters = GetReporters(id, collection.MultiGroup);
                    }
                }
            }

            var jobj = result.SearchQueryResult.Collection as JObject;
            if (jobj != null) {
                dynamic collection = result.SearchQueryResult.Collection;

                string id = JsonDotNetUtility.GetValue<string>(collection["Id"] ?? new JsonDotNetUtility.NullValue());
                switch (id) {
                    case "issue":
                        model.Issues = GetIssues(collection);
                        break;
                    case "news":
                        model.SearchResultDictionary.Add(SearchCategoryType.TotalNews, GetNews(collection));
                        model.SearchTotalCountDictionary.Add(SearchCategoryType.TotalNews, GetTotalCount(collection));
                        break;
                    case "news_ja":
                        model.SearchResultDictionary.Add(SearchCategoryType.JoongangNews, GetNews(collection));
                        model.SearchTotalCountDictionary.Add(SearchCategoryType.JoongangNews, GetTotalCount(collection));
                        break;
                    case "ja_Sunday":
                        model.SearchResultDictionary.Add(SearchCategoryType.JoongangSundayNews, GetNews(collection)); //2017-02-02
                        model.SearchTotalCountDictionary.Add(SearchCategoryType.JoongangSundayNews, GetTotalCount(collection));
                        break;
                }

                if (collection.MultiGroup != null) {
                    model.Sources = GetSources(id, collection.MultiGroup);
                    model.Services = GetServices(id, collection.MultiGroup);
                    model.Reporters = GetReporters(id, collection.MultiGroup);
                }
            }

            return model;
        }

        // 상세 검색
        public DefaultResult SearchConvert(dynamic result) {
            var model = new DefaultResult { RelationalKeywords = null, PopularSearchWords = null };
            var sources = new List<Source>();

            var jarray = result.SearchQueryResult.Collection as JArray;
            if (jarray != null) {
                foreach (dynamic collection in result.SearchQueryResult.Collection) {
                    string id = JsonDotNetUtility.GetValue<string>(collection["Id"] ?? new JsonDotNetUtility.NullValue());

                    switch (id) {
                        case "issue": // 이슈 검색
                            model.TotalCount = GetTotalCount(collection);
                            model.Issues = GetIssues(collection);
                            break;
                        case "reporter": // 기자 검색      
                            model.TotalCount = GetTotalCount(collection);
                            model.Reporters = GetReporters(collection);
                            break;
                        case "news": // 전체 뉴스        
                            model.TotalCount = GetTotalCount(collection);
                            model.SearchItems = GetNews(collection);
                            if (collection.MultiGroup != null) {
                                //model.Sources = GetSources(id, collection.MultiGroup);
                                model.Services = GetServices(id, collection.MultiGroup);
                                model.Reporters = GetReporters(id, collection.MultiGroup);
                            }
                            break;
                        case "news_ja": // 중앙일보
                            model.TotalCount = GetTotalCount(collection);
                            model.SearchItems = GetNews(collection);
                            if (collection.MultiGroup != null) {
                                //model.Sources = GetSources(id, collection.MultiGroup);
                                model.Services = GetServices(id, collection.MultiGroup);
                                model.Reporters = GetReporters(id, collection.MultiGroup);
                            }
                            break;
                        case "ja_sunday": // 중앙선데이
                            model.TotalCount = GetTotalCount(collection);
                            model.SearchItems = GetNews(collection); //2017-02-02
                            if (collection.MultiGroup != null) {
                                //model.Sources = GetSources(id, collection.MultiGroup);
                                //GetSources(sources, id, collection.MultiGroup); //2017-02-02
                                model.Services = GetServices(id, collection.MultiGroup);
                                model.Reporters = GetReporters(id, collection.MultiGroup);
                            }
                            break;

                        case "news_image": // 이미지 검색
                            model.TotalCount = GetTotalCount(collection);
                            model.SearchItems = GetImages(collection);
                            if (collection.MultiGroup != null) {
                                model.Services = GetServices(id, collection.MultiGroup);
                                model.Reporters = GetReporters(id, collection.MultiGroup);
                            }
                            break;
                        case "news_vod": // 동영상 검색       
                            model.TotalCount = GetTotalCount(collection);
                            model.SearchItems = GetVideos(collection);
                            if (collection.MultiGroup != null) {
                                model.Services = GetServices(id, collection.MultiGroup);
                                model.Reporters = GetReporters(id, collection.MultiGroup);
                            }
                            break;
                        case "joins_pdf": // 조인스 검색     
                            model.TotalCount = GetTotalCount(collection);
                            model.SearchItems = GetJoins(collection);
                            if (collection.MultiGroup != null) {
                                model.Services = GetServices(id, collection.MultiGroup);
                                model.Reporters = GetReporters(id, collection.MultiGroup);
                            }
                            break;
                        case "jplus": // J플러스 검색     
                            model.TotalCount = GetTotalCount(collection);
                            model.SearchItems = GetJplus(collection);
                            break;
                        case "blog": // 블로그 검색     
                            model.TotalCount = GetTotalCount(collection);
                            model.SearchItems = GetBlogs(collection);
                            break;
                        // 매체,분야,기자명 검색 필터 정보
                        case "news_group": // 전체뉴스
                        case "news_image_group": // 이미지
                        case "news_vod_group": // 동영상
                        case "joins_pdf_group": // 조인스
                            if (collection.MultiGroup != null) {
                                GetSources(sources, id, collection.MultiGroup);
                                //model.Sources = GetSources(id, collection.MultiGroup);                                
                            }
                            break;
                    }
                }

                model.Sources = sources;
            }

            var jobj = result.SearchQueryResult.Collection as JObject;
            if (jobj != null) {
                dynamic collection = result.SearchQueryResult.Collection;
                string id = JsonDotNetUtility.GetValue<string>(collection["Id"] ?? new JsonDotNetUtility.NullValue());

                switch (id) {
                    case "issue": // 이슈 검색
                        model.TotalCount = GetTotalCount(collection);
                        model.Issues = GetIssues(collection);
                        break;
                    case "reporter": // 기자 검색      
                        model.TotalCount = GetTotalCount(collection);
                        model.Reporters = GetReporters(collection);
                        break;
                    case "news": // 전체 뉴스        
                        model.TotalCount = GetTotalCount(collection);
                        model.SearchItems = GetNews(collection);
                        if (collection.MultiGroup != null) {
                            //model.Sources = GetSources(id, collection.MultiGroup);
                            model.Services = GetServices(id, collection.MultiGroup);
                            model.Reporters = GetReporters(id, collection.MultiGroup);
                        }
                        break;
                    case "news_ja": // 중앙일보
                        model.TotalCount = GetTotalCount(collection);
                        model.SearchItems = GetNews(collection);
                        if (collection.MultiGroup != null) {
                            //model.Sources = GetSources(id, collection.MultiGroup);
                            model.Services = GetServices(id, collection.MultiGroup);
                            model.Reporters = GetReporters(id, collection.MultiGroup);
                        }
                        break;
                    case "ja_sunday": // 중앙선데이
                        model.TotalCount = GetTotalCount(collection);
                        model.SearchItems = GetNews(collection); //2017-02-02
                        if (collection.MultiGroup != null) {
                            //model.Sources = GetSources(id, collection.MultiGroup); //2017-02-02
                            model.Services = GetServices(id, collection.MultiGroup);
                            model.Reporters = GetReporters(id, collection.MultiGroup);
                        }
                        break;

                    case "news_image": // 이미지 검색
                        model.TotalCount = GetTotalCount(collection);
                        model.SearchItems = GetImages(collection);
                        if (collection.MultiGroup != null) {
                            model.Services = GetServices(id, collection.MultiGroup);
                            model.Reporters = GetReporters(id, collection.MultiGroup);
                        }
                        break;
                    case "news_vod": // 동영상 검색       
                        model.TotalCount = GetTotalCount(collection);
                        model.SearchItems = GetVideos(collection);
                        if (collection.MultiGroup != null) {
                            model.Services = GetServices(id, collection.MultiGroup);
                            model.Reporters = GetReporters(id, collection.MultiGroup);
                        }
                        break;
                    case "joins_pdf": // 조인스 검색     
                        model.TotalCount = GetTotalCount(collection);
                        model.SearchItems = GetJoins(collection);
                        if (collection.MultiGroup != null) {
                            model.Services = GetServices(id, collection.MultiGroup);
                            model.Reporters = GetReporters(id, collection.MultiGroup);
                        }
                        break;
                    case "jplus": // J플러스 검색     
                        model.TotalCount = GetTotalCount(collection);
                        model.SearchItems = GetJplus(collection);
                        break;
                    case "blog": // 블로그 검색     
                        model.TotalCount = GetTotalCount(collection);
                        model.SearchItems = GetBlogs(collection);
                        break;
                    // 매체,분야,기자명 검색 필터 정보
                    case "news_group": // 전체뉴스
                    case "news_image_group": // 이미지
                    case "news_vod_group": // 동영상
                    case "joins_pdf_group": // 조인스
                        if (collection.MultiGroup != null) {
                            model.Sources = GetSources(id, collection.MultiGroup);
                            //model.Services = GetServices(id, collection.MultiGroup);
                            //model.Reporters = GetReporters(id, collection.MultiGroup);
                        }
                        break;
                }
            }

            return model;
        }

        // 중앙 뉴스 검색
        public DefaultResult JoongangNewsConvert(dynamic result) {
            var model = new DefaultResult { RelationalKeywords = null, PopularSearchWords = null };

            foreach (dynamic collection in result.SearchQueryResult.Collection) {
                string id = JsonDotNetUtility.GetValue<string>(collection["Id"] ?? new JsonDotNetUtility.NullValue());
                switch (id) {
                    case "issue":
                        model.Issues = GetIssues(collection);
                        break;
                    case "news":
                        model.TotalCount = GetTotalCount(collection);
                        model.SearchItems = GetNews(collection);
                        break;
                }
            }

            return model;
        }

        // 중앙 선데이 뉴스 검색
        public DefaultResult JoongangSundayNewsConvert(dynamic result) {
            var model = new DefaultResult { RelationalKeywords = null, PopularSearchWords = null };

            foreach (dynamic collection in result.SearchQueryResult.Collection) {
                string id = JsonDotNetUtility.GetValue<string>(collection["Id"] ?? new JsonDotNetUtility.NullValue());
                switch (id) {
                    case "issue":
                        model.Issues = GetIssues(collection);
                        break;
                    case "news":
                        model.TotalCount = GetTotalCount(collection);
                        model.SearchItems = GetNews(collection);
                        break;
                }
            }

            return model;
        }

        // 전체 뉴스 검색
        public DefaultResult TotalNewsConvert(dynamic result) {
            var model = new DefaultResult { RelationalKeywords = null, PopularSearchWords = null };

            foreach (dynamic collection in result.SearchQueryResult.Collection) {
                string id = JsonDotNetUtility.GetValue<string>(collection["Id"] ?? new JsonDotNetUtility.NullValue());
                switch (id) {
                    case "issue":
                        model.Issues = GetIssues(collection);
                        break;
                    case "news":
                        model.TotalCount = GetTotalCount(collection);
                        model.SearchItems = GetNews(collection);
                        break;
                }
            }

            return model;
        }

        // 이미지 검색
        public DefaultResult ImageConvert(dynamic result) {
            var model = new DefaultResult { RelationalKeywords = null, PopularSearchWords = null };

            foreach (dynamic collection in result.SearchQueryResult.Collection) {
                string id = JsonDotNetUtility.GetValue<string>(collection["Id"] ?? new JsonDotNetUtility.NullValue());
                switch (id) {
                    case "news_image":
                        model.TotalCount = GetTotalCount(collection);
                        model.SearchItems = GetImages(collection);
                        break;
                }
            }

            return model;
        }

        // 동영상 검색
        public DefaultResult VideoConvert(dynamic result) {
            var model = new DefaultResult { RelationalKeywords = null, PopularSearchWords = null };

            foreach (dynamic collection in result.SearchQueryResult.Collection) {
                string id = JsonDotNetUtility.GetValue<string>(collection["Id"] ?? new JsonDotNetUtility.NullValue());
                switch (id) {
                    case "news_vod":
                        model.TotalCount = GetTotalCount(collection);
                        model.SearchItems = GetVideos(collection);
                        break;
                }
            }

            return model;
        }

        // 조인스 검색
        public DefaultResult JoinsConvert(dynamic result) {
            var model = new DefaultResult { RelationalKeywords = null, PopularSearchWords = null };

            foreach (dynamic collection in result.SearchQueryResult.Collection) {
                string id = JsonDotNetUtility.GetValue<string>(collection["Id"] ?? new JsonDotNetUtility.NullValue());
                switch (id) {
                    case "joins_pdf":
                        model.TotalCount = GetTotalCount(collection);
                        model.SearchItems = GetJoins(collection);
                        break;
                }
            }

            return model;
        }

        // J플러스 검색
        public DefaultResult JplusConvert(dynamic result) {
            var model = new DefaultResult { RelationalKeywords = null, PopularSearchWords = null };

            foreach (dynamic collection in result.SearchQueryResult.Collection) {
                string id = JsonDotNetUtility.GetValue<string>(collection["Id"] ?? new JsonDotNetUtility.NullValue());
                switch (id) {
                    case "jplus":
                        model.TotalCount = GetTotalCount(collection);
                        model.SearchItems = GetJplus(collection);
                        break;
                }
            }

            return model;
        }

        // 블로그 검색
        public DefaultResult BlogConvert(dynamic result) {
            var model = new DefaultResult { RelationalKeywords = null, PopularSearchWords = null };

            foreach (dynamic collection in result.SearchQueryResult.Collection) {
                string id = JsonDotNetUtility.GetValue<string>(collection["Id"] ?? new JsonDotNetUtility.NullValue());
                switch (id) {
                    case "blog":
                        model.TotalCount = GetTotalCount(collection);
                        model.SearchItems = GetBlogs(collection);
                        break;
                }
            }

            return model;
        }

        // 이슈 검색
        public DefaultResult IssueConvert(dynamic result) {
            var model = new DefaultResult();

            foreach (dynamic collection in result.SearchQueryResult.Collection) {
                string id = JsonDotNetUtility.GetValue<string>(collection["Id"] ?? new JsonDotNetUtility.NullValue());
                switch (id) {
                    case "issue":
                        model.TotalCount = GetTotalCount(collection);
                        model.Issues = GetIssues(collection);
                        break;
                }
            }

            return model;
        }

        // 기자 검색
        public DefaultResult ReporterConvert(dynamic result) {
            var model = new DefaultResult();

            foreach (dynamic collection in result.SearchQueryResult.Collection) {
                string id = JsonDotNetUtility.GetValue<string>(collection["Id"] ?? new JsonDotNetUtility.NullValue());
                switch (id) {
                    case "reporter":
                        model.TotalCount = GetTotalCount(collection);
                        model.Reporters = GetReporters(collection);
                        break;
                }
            }

            return model;
        }

        private int GetTotalCount(dynamic collection) {
            int totalCount = 0;

            if (collection.DocumentSet != null)
                totalCount = JsonDotNetUtility.GetValue<int>(collection.DocumentSet.TotalCount ?? new JsonDotNetUtility.NullValue());

            return totalCount;
        }

        private IEnumerable<ShortCut> GetShortCuts(dynamic collection) {
            var models = new List<ShortCut>();

            var jarray = collection.DocumentSet.Document as JArray;
            if (jarray != null) {
                foreach (dynamic document in collection.DocumentSet.Document) {
                    var item = new ShortCut {
                        Name = JsonDotNetUtility.GetValue<string>(document.Field["LINK_TITLE"] ?? new JsonDotNetUtility.NullValue()),
                        Summary = JsonDotNetUtility.GetValue<string>(document.Field["LINK_CON"] ?? new JsonDotNetUtility.NullValue()),
                        Url = JsonDotNetUtility.GetValue<string>(document.Field["LINK_URL"] ?? new JsonDotNetUtility.NullValue()),
                        Thumbnail = JsonDotNetUtility.GetValue<string>(document.Field["IMG_URL"] ?? new JsonDotNetUtility.NullValue()),
                    };

                    models.Add(item);
                }
            }

            var jobj = collection.DocumentSet.Document as JObject;
            if (jobj != null) {
                dynamic document = collection.DocumentSet.Document;

                var item = new ShortCut {
                    Name = JsonDotNetUtility.GetValue<string>(document.Field["LINK_TITLE"] ?? new JsonDotNetUtility.NullValue()),
                    Summary = JsonDotNetUtility.GetValue<string>(document.Field["LINK_CON"] ?? new JsonDotNetUtility.NullValue()),
                    Url = JsonDotNetUtility.GetValue<string>(document.Field["LINK_URL"] ?? new JsonDotNetUtility.NullValue()),
                    Thumbnail = JsonDotNetUtility.GetValue<string>(document.Field["IMG_URL"] ?? new JsonDotNetUtility.NullValue()),
                };

                models.Add(item);
            }

            if (jarray == null && jobj == null)
                models = null;

            return models;
        }

        private IEnumerable<People> GetPeoples(dynamic collection) {
            var models = new List<People>();

            var jarray = collection.DocumentSet.Document as JArray;
            if (jarray != null) {
                foreach (dynamic document in collection.DocumentSet.Document) {
                    string birth = JsonDotNetUtility.GetValue<string>(document.Field["BIRTH"] ?? new JsonDotNetUtility.NullValue());
                    var item = new People {
                        Id = JsonDotNetUtility.GetValue<int>(document.Field["DOCID"] ?? new JsonDotNetUtility.NullValue()),
                        Name = JsonDotNetUtility.GetValue<string>(document.Field["INMUL_NAME_KR"] ?? new JsonDotNetUtility.NullValue()),
                        NameByChinese = JsonDotNetUtility.GetValue<string>(document.Field["INMUL_NAME_HAN"] ?? new JsonDotNetUtility.NullValue()),
                        NameByEnglish = JsonDotNetUtility.GetValue<string>(document.Field["INMUL_NAME_EN"] ?? new JsonDotNetUtility.NullValue()),
                        Thumbnail = "", // 프로필 이미지는 ID를 가지고 조합
                        Job = JsonDotNetUtility.GetValue<string>(document.Field["JOB_NAME"] ?? new JsonDotNetUtility.NullValue()),
                        BirthDate =
                            !string.IsNullOrEmpty(birth) && birth.Length.Equals(8)
                                ? DateTime.ParseExact(birth, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)
                                : DateTime.MinValue,
                        Height = 0,
                        Weight = JsonDotNetUtility.GetValue<int>(document.Field["INMUL_WEIGHT"] ?? new JsonDotNetUtility.NullValue()),
                        Agency = JsonDotNetUtility.GetValue<string>(document.Field["BELONG_POSITION"] ?? new JsonDotNetUtility.NullValue()),
                        Awards = null,
                        Careers = null,
                    };


                    models.Add(item);
                }
            }

            var jobj = collection.DocumentSet.Document as JObject;
            if (jobj != null) {
                dynamic document = collection.DocumentSet.Document;
                string birth = JsonDotNetUtility.GetValue<string>(document.Field["BIRTH"] ?? new JsonDotNetUtility.NullValue());
                var item = new People {
                    Id = JsonDotNetUtility.GetValue<int>(document.Field["DOCID"] ?? new JsonDotNetUtility.NullValue()),
                    Name = JsonDotNetUtility.GetValue<string>(document.Field["INMUL_NAME_KR"] ?? new JsonDotNetUtility.NullValue()),
                    NameByChinese = JsonDotNetUtility.GetValue<string>(document.Field["INMUL_NAME_HAN"] ?? new JsonDotNetUtility.NullValue()),
                    NameByEnglish = JsonDotNetUtility.GetValue<string>(document.Field["INMUL_NAME_EN"] ?? new JsonDotNetUtility.NullValue()),
                    Thumbnail = "", // 프로필 이미지는 ID를 가지고 조합
                    Job = JsonDotNetUtility.GetValue<string>(document.Field["JOB_NAME"] ?? new JsonDotNetUtility.NullValue()),
                    BirthDate =
                        !string.IsNullOrEmpty(birth) && birth.Length.Equals(8)
                            ? DateTime.ParseExact(birth, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)
                            : DateTime.MinValue,
                    Height = 0,
                    Weight = JsonDotNetUtility.GetValue<int>(document.Field["INMUL_WEIGHT"] ?? new JsonDotNetUtility.NullValue()),
                    Agency = JsonDotNetUtility.GetValue<string>(document.Field["BELONG_POSITION"] ?? new JsonDotNetUtility.NullValue()),
                    Awards = null,
                    Careers = null,
                };

                models.Add(item);
            }

            if (jarray == null && jobj == null)
                models = null;

            return models;
        }

        private IEnumerable<Keyword> GetIssues(dynamic collection) {
            var models = new List<Keyword>();

            var jarray = collection.DocumentSet.Document as JArray;
            if (jarray != null) {
                foreach (dynamic document in collection.DocumentSet.Document) {
                    var item = new Keyword {
                        Id = JsonDotNetUtility.GetValue<int>(document.Field["DOCID"] ?? new JsonDotNetUtility.NullValue()),
                        Word = JsonDotNetUtility.GetValue<string>(document.Field["ISS_TITLE"] ?? new JsonDotNetUtility.NullValue()),
                    };

                    models.Add(item);
                }
            }

            var jobj = collection.DocumentSet.Document as JObject;
            if (jobj != null) {
                dynamic document = collection.DocumentSet.Document;

                var item = new Keyword {
                    Id = JsonDotNetUtility.GetValue<int>(document.Field["DOCID"] ?? new JsonDotNetUtility.NullValue()),
                    Word = JsonDotNetUtility.GetValue<string>(document.Field["ISS_TITLE"] ?? new JsonDotNetUtility.NullValue()),
                };

                models.Add(item);
            }

            if (jarray == null && jobj == null)
                models = null;

            return models;
        }

        private IEnumerable<Writer> GetReporters(dynamic collection) {
            var models = new List<Writer>();

            var jarray = collection.DocumentSet.Document as JArray;
            if (jarray != null) {
                foreach (dynamic document in collection.DocumentSet.Document) {
                    var item = new Writer {
                        Id = JsonDotNetUtility.GetValue<int>(document.Field["DOCID"] ?? new JsonDotNetUtility.NullValue()),
                        Name = JsonDotNetUtility.GetValue<string>(document.Field["REP_NAME"] ?? new JsonDotNetUtility.NullValue()),
                        Department = "",
                        Thumbnail = JsonDotNetUtility.GetValue<string>(document.Field["REP_PHOTO"] ?? new JsonDotNetUtility.NullValue()),
                    };

                    models.Add(item);
                }
            }

            var jobj = collection.DocumentSet.Document as JObject;
            if (jobj != null) {
                dynamic document = collection.DocumentSet.Document;

                var item = new Writer {
                    Id = JsonDotNetUtility.GetValue<int>(document.Field["DOCID"] ?? new JsonDotNetUtility.NullValue()),
                    Name = JsonDotNetUtility.GetValue<string>(document.Field["REP_NAME"] ?? new JsonDotNetUtility.NullValue()),
                    Department = "",
                    Thumbnail = JsonDotNetUtility.GetValue<string>(document.Field["REP_PHOTO"] ?? new JsonDotNetUtility.NullValue()),
                };

                models.Add(item);
            }

            if (jarray == null && jobj == null)
                models = null;

            return models;
        }

        private IEnumerable<SearchItem> GetNews(dynamic collection) {
            var models = new List<SearchItem>();

            var jarray = collection.DocumentSet.Document as JArray;
            if (jarray != null) {
                foreach (dynamic document in collection.DocumentSet.Document) {
                    var item = new SearchItem {
                        Id = JsonDotNetUtility.GetValue<int>(document.Field["DOCID"] ?? new JsonDotNetUtility.NullValue()),
                        RelationId = JsonDotNetUtility.GetValue<int>(document["Uid"] ?? new JsonDotNetUtility.NullValue()),
                        Title = JsonDotNetUtility.GetValue<string>(document.Field["ART_TITLE"] ?? new JsonDotNetUtility.NullValue()),
                        MobileTitle = JsonDotNetUtility.GetValue<string>(document.Field["MOB_TITLE"] ?? new JsonDotNetUtility.NullValue()),
                        Summary = JsonDotNetUtility.GetValue<string>(document.Field["ART_CONTENT"] ?? new JsonDotNetUtility.NullValue()),
                        Thumbnail = JsonDotNetUtility.GetValue<string>(document.Field["ART_THUMB"] ?? new JsonDotNetUtility.NullValue()),
                        Source = JsonDotNetUtility.GetValue<string>(document.Field["SOURCE_NAME"] ?? new JsonDotNetUtility.NullValue()),
                        ArticleType = JsonDotNetUtility.GetValue<string>(document.Field["ART_TYPE"] ?? new JsonDotNetUtility.NullValue()),
                        SourceCode = JsonDotNetUtility.GetValue<string>(document.Field["SOURCE_CODE"] ?? new JsonDotNetUtility.NullValue()),
                        DuplicateCount = JsonDotNetUtility.GetValue<int>(document["DuplicateDocumentCount"] ?? new JsonDotNetUtility.NullValue()),
                        RelationalItems = null,
                        Writer = new Writer {
                            Id = JsonDotNetUtility.GetValue<int>(document.Field["ART_REP_NO"] ?? new JsonDotNetUtility.NullValue()),
                            Name = JsonDotNetUtility.GetValue<string>(document.Field["ART_REPORTER"] ?? new JsonDotNetUtility.NullValue()),
                            Department = JsonDotNetUtility.GetValue<string>(document.Field["ART_REP_JOB"] ?? new JsonDotNetUtility.NullValue()),
                            Thumbnail = JsonDotNetUtility.GetValue<string>(document.Field["ART_REP_IMG"] ?? new JsonDotNetUtility.NullValue()),
                        },
                    };

                    string registedDateTime = JsonDotNetUtility.GetValue<string>(document.Field["Date"] ?? new JsonDotNetUtility.NullValue());
                    string articleKeyword = JsonDotNetUtility.GetValue<string>(document.Field["ART_KWD"] ?? new JsonDotNetUtility.NullValue());
                    if (!string.IsNullOrEmpty(registedDateTime))
                        item.RegistedDateTime = registedDateTime.ConvertDateTimeyyyyMMddHHmmss();
                    if (!string.IsNullOrEmpty(articleKeyword)) {
                        List<Keyword> keywords = articleKeyword.Split(',').Select(keyword => new Keyword { Word = keyword }).ToList();
                        item.Keywords = keywords;
                    }

                    models.Add(item);
                }
            }

            var jobj = collection.DocumentSet.Document as JObject;
            if (jobj != null) {
                dynamic document = collection.DocumentSet.Document;

                var item = new SearchItem {
                    Id = JsonDotNetUtility.GetValue<int>(document.Field["DOCID"] ?? new JsonDotNetUtility.NullValue()),
                    RelationId = JsonDotNetUtility.GetValue<int>(document["Uid"] ?? new JsonDotNetUtility.NullValue()),
                    Title = JsonDotNetUtility.GetValue<string>(document.Field["ART_TITLE"] ?? new JsonDotNetUtility.NullValue()),
                    MobileTitle = JsonDotNetUtility.GetValue<string>(document.Field["MOB_TITLE"] ?? new JsonDotNetUtility.NullValue()),
                    Summary = JsonDotNetUtility.GetValue<string>(document.Field["ART_CONTENT"] ?? new JsonDotNetUtility.NullValue()),
                    Thumbnail = JsonDotNetUtility.GetValue<string>(document.Field["ART_THUMB"] ?? new JsonDotNetUtility.NullValue()),
                    Source = JsonDotNetUtility.GetValue<string>(document.Field["SOURCE_NAME"] ?? new JsonDotNetUtility.NullValue()),
                    ArticleType = JsonDotNetUtility.GetValue<string>(document.Field["ART_TYPE"] ?? new JsonDotNetUtility.NullValue()),
                    SourceCode = JsonDotNetUtility.GetValue<string>(document.Field["SOURCE_CODE"] ?? new JsonDotNetUtility.NullValue()),
                    DuplicateCount = JsonDotNetUtility.GetValue<int>(document["DuplicateDocumentCount"] ?? new JsonDotNetUtility.NullValue()),
                    RelationalItems = null,
                };

                string registedDateTime = JsonDotNetUtility.GetValue<string>(document.Field["Date"] ?? new JsonDotNetUtility.NullValue());
                string articleKeyword = JsonDotNetUtility.GetValue<string>(document.Field["ART_KWD"] ?? new JsonDotNetUtility.NullValue());
                if (!string.IsNullOrEmpty(registedDateTime))
                    item.RegistedDateTime = registedDateTime.ConvertDateTimeyyyyMMddHHmmss();
                if (!string.IsNullOrEmpty(articleKeyword)) {
                    List<Keyword> keywords = articleKeyword.Split(',').Select(keyword => new Keyword { Word = keyword }).ToList();
                    item.Keywords = keywords;
                }

                models.Add(item);
            }

            if (jarray == null && jobj == null)
                models = null;

            return models;
        }

        private IEnumerable<SearchItem> GetJoongangSunday(dynamic collection) {
            var models = new List<SearchItem>();

            var jarray = collection.DocumentSet.Document as JArray;
            if (jarray != null) {
                foreach (dynamic document in collection.DocumentSet.Document) {
                    var item = new SearchItem {
                        Id = JsonDotNetUtility.GetValue<int>(document.Field["DOCID"] ?? new JsonDotNetUtility.NullValue()),
                        Title = JsonDotNetUtility.GetValue<string>(document.Field["post_title"] ?? new JsonDotNetUtility.NullValue()),
                        Summary = JsonDotNetUtility.GetValue<string>(document.Field["post_content"] ?? new JsonDotNetUtility.NullValue()),
                        Thumbnail = "",
                        Source = JsonDotNetUtility.GetValue<string>(document.Field["SRC_GRP_NAME"] ?? new JsonDotNetUtility.NullValue()),
                        OuterUrl = JsonDotNetUtility.GetValue<string>(document.Field["guid"] ?? new JsonDotNetUtility.NullValue()),
                        RelationalItems = null,
                    };

                    string registedDateTime = JsonDotNetUtility.GetValue<string>(document.Field["Date"] ?? new JsonDotNetUtility.NullValue());
                    string articleKeyword = JsonDotNetUtility.GetValue<string>(document.Field["ART_KWD"] ?? new JsonDotNetUtility.NullValue());
                    if (!string.IsNullOrEmpty(registedDateTime))
                        item.RegistedDateTime = registedDateTime.ConvertDateTimeyyyyMMddHHmmss();
                    if (!string.IsNullOrEmpty(articleKeyword)) {
                        List<Keyword> keywords = articleKeyword.Split(',').Select(keyword => new Keyword { Word = keyword }).ToList();
                        item.Keywords = keywords;
                    }

                    models.Add(item);
                }
            }

            var jobj = collection.DocumentSet.Document as JObject;
            if (jobj != null) {
                dynamic document = collection.DocumentSet.Document;
                var item = new SearchItem {
                    Id = JsonDotNetUtility.GetValue<int>(document.Field["DOCID"] ?? new JsonDotNetUtility.NullValue()),
                    Title = JsonDotNetUtility.GetValue<string>(document.Field["post_title"] ?? new JsonDotNetUtility.NullValue()),
                    Summary = JsonDotNetUtility.GetValue<string>(document.Field["post_content"] ?? new JsonDotNetUtility.NullValue()),
                    Thumbnail = "",
                    Source = JsonDotNetUtility.GetValue<string>(document.Field["SRC_GRP_NAME"] ?? new JsonDotNetUtility.NullValue()),
                    RelationalItems = null,
                };

                string registedDateTime = JsonDotNetUtility.GetValue<string>(document.Field["Date"] ?? new JsonDotNetUtility.NullValue());
                string articleKeyword = JsonDotNetUtility.GetValue<string>(document.Field["ART_KWD"] ?? new JsonDotNetUtility.NullValue());
                if (!string.IsNullOrEmpty(registedDateTime))
                    item.RegistedDateTime = registedDateTime.ConvertDateTimeyyyyMMddHHmmss();
                if (!string.IsNullOrEmpty(articleKeyword)) {
                    List<Keyword> keywords = articleKeyword.Split(',').Select(keyword => new Keyword { Word = keyword }).ToList();
                    item.Keywords = keywords;
                }

                models.Add(item);
            }

            if (jarray == null && jobj == null)
                models = null;

            return models;
        }

        private IEnumerable<SearchItem> GetImages(dynamic collection) {
            var models = new List<SearchItem>();

            var jarray = collection.DocumentSet.Document as JArray;
            if (jarray != null) {
                foreach (dynamic document in collection.DocumentSet.Document) {
                    var item = new SearchItem {
                        Id = JsonDotNetUtility.GetValue<int>(document.Field["DOCID"] ?? new JsonDotNetUtility.NullValue()),
                        Title = JsonDotNetUtility.GetValue<string>(document.Field["TITLE"] ?? new JsonDotNetUtility.NullValue()),
                        Summary = JsonDotNetUtility.GetValue<string>(document.Field["CONTENTS"] ?? new JsonDotNetUtility.NullValue()),
                        Thumbnail = JsonDotNetUtility.GetValue<string>(document.Field["img_ThumbNail"] ?? new JsonDotNetUtility.NullValue()),
                        Source = JsonDotNetUtility.GetValue<string>(document.Field["DETAIL_SCH2"] ?? new JsonDotNetUtility.NullValue()),
                        ImageWidth = JsonDotNetUtility.GetValue<int>(document.Field["IMG_WIDTH"] ?? new JsonDotNetUtility.NullValue()),
                        ImageHeight = JsonDotNetUtility.GetValue<int>(document.Field["IMG_HEIGHT"] ?? new JsonDotNetUtility.NullValue()),
                    };

                    string registedDateTime = JsonDotNetUtility.GetValue<string>(document.Field["Date"] ?? new JsonDotNetUtility.NullValue());
                    string articleKeyword = JsonDotNetUtility.GetValue<string>(document.Field["ART_KWD"] ?? new JsonDotNetUtility.NullValue());
                    if (!string.IsNullOrEmpty(registedDateTime))
                        item.RegistedDateTime = registedDateTime.ConvertDateTimeyyyyMMddHHmmss();
                    if (!string.IsNullOrEmpty(articleKeyword)) {
                        List<Keyword> keywords = articleKeyword.Split(',').Select(keyword => new Keyword { Word = keyword }).ToList();
                        item.Keywords = keywords;
                    }

                    models.Add(item);
                }
            }

            var jobj = collection.DocumentSet.Document as JObject;
            if (jobj != null) {
                dynamic document = collection.DocumentSet.Document;

                var item = new SearchItem {
                    Id = JsonDotNetUtility.GetValue<int>(document.Field["DOCID"] ?? new JsonDotNetUtility.NullValue()),
                    Title = JsonDotNetUtility.GetValue<string>(document.Field["TITLE"] ?? new JsonDotNetUtility.NullValue()),
                    Summary = JsonDotNetUtility.GetValue<string>(document.Field["CONTENTS"] ?? new JsonDotNetUtility.NullValue()),
                    Thumbnail = JsonDotNetUtility.GetValue<string>(document.Field["img_ThumbNail"] ?? new JsonDotNetUtility.NullValue()),
                    Source = JsonDotNetUtility.GetValue<string>(document.Field["DETAIL_SCH2"] ?? new JsonDotNetUtility.NullValue()),
                    ImageWidth = JsonDotNetUtility.GetValue<int>(document.Field["IMG_WIDTH"] ?? new JsonDotNetUtility.NullValue()),
                    ImageHeight = JsonDotNetUtility.GetValue<int>(document.Field["IMG_HEIGHT"] ?? new JsonDotNetUtility.NullValue()),
                };

                string registedDateTime = JsonDotNetUtility.GetValue<string>(document.Field["Date"] ?? new JsonDotNetUtility.NullValue());
                string articleKeyword = JsonDotNetUtility.GetValue<string>(document.Field["ART_KWD"] ?? new JsonDotNetUtility.NullValue());
                if (!string.IsNullOrEmpty(registedDateTime))
                    item.RegistedDateTime = registedDateTime.ConvertDateTimeyyyyMMddHHmmss();
                if (!string.IsNullOrEmpty(articleKeyword)) {
                    List<Keyword> keywords = articleKeyword.Split(',').Select(keyword => new Keyword { Word = keyword }).ToList();
                    item.Keywords = keywords;
                }

                models.Add(item);
            }

            if (jarray == null && jobj == null)
                models = null;

            return models;
        }

        private IEnumerable<SearchItem> GetVideos(dynamic collection) {
            var models = new List<SearchItem>();

            var jarray = collection.DocumentSet.Document as JArray;
            if (jarray != null) {
                foreach (dynamic document in collection.DocumentSet.Document) {
                    var item = new SearchItem {
                        Id = JsonDotNetUtility.GetValue<int>(document.Field["TOTAL_ID"] ?? new JsonDotNetUtility.NullValue()),
                        Title = JsonDotNetUtility.GetValue<string>(document.Field["ART_TITLE"] ?? new JsonDotNetUtility.NullValue()),
                        Thumbnail = JsonDotNetUtility.GetValue<string>(document.Field["THUMB_IMG"] ?? new JsonDotNetUtility.NullValue()),
                        Source = JsonDotNetUtility.GetValue<string>(document.Field["SOURCE_NAME"] ?? new JsonDotNetUtility.NullValue()),
                        MultiType = JsonDotNetUtility.GetValue<string>(document.Field["MULTI_TYPE"] ?? new JsonDotNetUtility.NullValue()),
                    };

                    string registedDateTime = JsonDotNetUtility.GetValue<string>(document.Field["Date"] ?? new JsonDotNetUtility.NullValue());
                    if (!string.IsNullOrEmpty(registedDateTime))
                        item.RegistedDateTime = registedDateTime.ConvertDateTimeyyyyMMddHHmmss();

                    models.Add(item);
                }
            }

            var jobj = collection.DocumentSet.Document as JObject;
            if (jobj != null) {
                dynamic document = collection.DocumentSet.Document;

                var item = new SearchItem {
                    Id = JsonDotNetUtility.GetValue<int>(document.Field["TOTAL_ID"] ?? new JsonDotNetUtility.NullValue()),
                    Title = JsonDotNetUtility.GetValue<string>(document.Field["ART_TITLE"] ?? new JsonDotNetUtility.NullValue()),
                    Thumbnail = JsonDotNetUtility.GetValue<string>(document.Field["THUMB_IMG"] ?? new JsonDotNetUtility.NullValue()),
                    Source = JsonDotNetUtility.GetValue<string>(document.Field["SOURCE_NAME"] ?? new JsonDotNetUtility.NullValue()),
                    MultiType = JsonDotNetUtility.GetValue<string>(document.Field["MULTI_TYPE"] ?? new JsonDotNetUtility.NullValue()),
                };

                string registedDateTime = JsonDotNetUtility.GetValue<string>(document.Field["Date"] ?? new JsonDotNetUtility.NullValue());
                if (!string.IsNullOrEmpty(registedDateTime))
                    item.RegistedDateTime = registedDateTime.ConvertDateTimeyyyyMMddHHmmss();

                models.Add(item);
            }

            if (jarray == null && jobj == null)
                models = null;

            return models;
        }

        private IEnumerable<SearchItem> GetJoins(dynamic collection) {
            var models = new List<SearchItem>();

            var jarray = collection.DocumentSet.Document as JArray;
            if (jarray != null) {
                foreach (dynamic document in collection.DocumentSet.Document) {
                    var item = new SearchItem {
                        Id = JsonDotNetUtility.GetValue<int>(document.Field["JI_SEQ"] ?? new JsonDotNetUtility.NullValue()),
                        MediaId = JsonDotNetUtility.GetValue<int>(document.Field["MDA_SEQ"] ?? new JsonDotNetUtility.NullValue()),
                        PublishId = JsonDotNetUtility.GetValue<int>(document.Field["PUB_SEQ"] ?? new JsonDotNetUtility.NullValue()),
                        Title = JsonDotNetUtility.GetValue<string>(document.Field["ART_TITLE"] ?? new JsonDotNetUtility.NullValue()),
                        Summary = JsonDotNetUtility.GetValue<string>(document.Field["ART_CONTENT"] ?? new JsonDotNetUtility.NullValue()),
                        Thumbnail = JsonDotNetUtility.GetValue<string>(document.Field["IMG_URL"] ?? new JsonDotNetUtility.NullValue()),
                        Source = JsonDotNetUtility.GetValue<string>(document.Field["MDA_NM"] ?? new JsonDotNetUtility.NullValue()),
                        IsPaid = true,
                    };

                    string registedDateTime = JsonDotNetUtility.GetValue<string>(document.Field["Date"] ?? new JsonDotNetUtility.NullValue());
                    if (!string.IsNullOrEmpty(registedDateTime))
                        item.RegistedDateTime = registedDateTime.ConvertDateTimeyyyyMMddHHmmss();

                    models.Add(item);
                }
            }

            var jobj = collection.DocumentSet.Document as JObject;
            if (jobj != null) {
                dynamic document = collection.DocumentSet.Document;

                var item = new SearchItem {
                    Id = JsonDotNetUtility.GetValue<int>(document.Field["JI_SEQ"] ?? new JsonDotNetUtility.NullValue()),
                    MediaId = JsonDotNetUtility.GetValue<int>(document.Field["MDA_SEQ"] ?? new JsonDotNetUtility.NullValue()),
                    PublishId = JsonDotNetUtility.GetValue<int>(document.Field["PUB_SEQ"] ?? new JsonDotNetUtility.NullValue()),
                    Title = JsonDotNetUtility.GetValue<string>(document.Field["ART_TITLE"] ?? new JsonDotNetUtility.NullValue()),
                    Summary = JsonDotNetUtility.GetValue<string>(document.Field["ART_CONTENT"] ?? new JsonDotNetUtility.NullValue()),
                    Thumbnail = JsonDotNetUtility.GetValue<string>(document.Field["IMG_URL"] ?? new JsonDotNetUtility.NullValue()),
                    Source = JsonDotNetUtility.GetValue<string>(document.Field["MDA_NM"] ?? new JsonDotNetUtility.NullValue()),
                    IsPaid = true,
                };

                string registedDateTime = JsonDotNetUtility.GetValue<string>(document.Field["Date"] ?? new JsonDotNetUtility.NullValue());
                if (!string.IsNullOrEmpty(registedDateTime))
                    item.RegistedDateTime = registedDateTime.ConvertDateTimeyyyyMMddHHmmss();

                models.Add(item);
            }

            if (jarray == null && jobj == null)
                models = null;

            return models;
        }

        private IEnumerable<SearchItem> GetJplus(dynamic collection) {
            var models = new List<SearchItem>();

            var jarray = collection.DocumentSet.Document as JArray;
            if (jarray != null) {
                foreach (dynamic document in collection.DocumentSet.Document) {
                    var item = new SearchItem {
                        Id = JsonDotNetUtility.GetValue<int>(document.Field["DOCID"] ?? new JsonDotNetUtility.NullValue()),
                        Title = JsonDotNetUtility.GetValue<string>(document.Field["ART_TITLE"] ?? new JsonDotNetUtility.NullValue()),
                        Summary = JsonDotNetUtility.GetValue<string>(document.Field["ART_CONTENT"] ?? new JsonDotNetUtility.NullValue()),
                        Thumbnail = JsonDotNetUtility.GetValue<string>(document.Field["ART_THUMB"] ?? new JsonDotNetUtility.NullValue()),
                        Source = JsonDotNetUtility.GetValue<string>(document.Field["SECTION_NAME"] ?? new JsonDotNetUtility.NullValue()),
                        Writer = new Writer {
                            Id = JsonDotNetUtility.GetValue<int>(document.Field["ART_REP_NO"] ?? new JsonDotNetUtility.NullValue()),
                            Name = JsonDotNetUtility.GetValue<string>(document.Field["ART_REPORTER"] ?? new JsonDotNetUtility.NullValue()),
                            Department = JsonDotNetUtility.GetValue<string>(document.Field["ART_REP_JOB"] ?? new JsonDotNetUtility.NullValue()),
                            Thumbnail = JsonDotNetUtility.GetValue<string>(document.Field["ART_REP_IMG"] ?? new JsonDotNetUtility.NullValue()),
                        },
                    };

                    string registedDateTime = JsonDotNetUtility.GetValue<string>(document.Field["Date"] ?? new JsonDotNetUtility.NullValue());
                    if (!string.IsNullOrEmpty(registedDateTime))
                        item.RegistedDateTime = registedDateTime.ConvertDateTimeyyyyMMddHHmmss();

                    models.Add(item);
                }
            }

            var jobj = collection.DocumentSet.Document as JObject;
            if (jobj != null) {
                dynamic document = collection.DocumentSet.Document;

                var item = new SearchItem {
                    Id = JsonDotNetUtility.GetValue<int>(document.Field["DOCID"] ?? new JsonDotNetUtility.NullValue()),
                    Title = JsonDotNetUtility.GetValue<string>(document.Field["ART_TITLE"] ?? new JsonDotNetUtility.NullValue()),
                    Summary = JsonDotNetUtility.GetValue<string>(document.Field["ART_CONTENT"] ?? new JsonDotNetUtility.NullValue()),
                    Thumbnail = JsonDotNetUtility.GetValue<string>(document.Field["ART_THUMB"] ?? new JsonDotNetUtility.NullValue()),
                    Source = JsonDotNetUtility.GetValue<string>(document.Field["SECTION_NAME"] ?? new JsonDotNetUtility.NullValue()),
                    Writer = new Writer {
                        Id = JsonDotNetUtility.GetValue<int>(document.Field["ART_REP_NO"] ?? new JsonDotNetUtility.NullValue()),
                        Name = JsonDotNetUtility.GetValue<string>(document.Field["ART_REPORTER"] ?? new JsonDotNetUtility.NullValue()),
                        Department = JsonDotNetUtility.GetValue<string>(document.Field["ART_REP_JOB"] ?? new JsonDotNetUtility.NullValue()),
                        Thumbnail = JsonDotNetUtility.GetValue<string>(document.Field["ART_REP_IMG"] ?? new JsonDotNetUtility.NullValue()),
                    },
                };

                string registedDateTime = JsonDotNetUtility.GetValue<string>(document.Field["Date"] ?? new JsonDotNetUtility.NullValue());
                if (!string.IsNullOrEmpty(registedDateTime))
                    item.RegistedDateTime = registedDateTime.ConvertDateTimeyyyyMMddHHmmss();

                models.Add(item);
            }

            if (jarray == null && jobj == null)
                models = null;

            return models;
        }

        private IEnumerable<SearchItem> GetBlogs(dynamic collection) {
            var models = new List<SearchItem>();

            var jarray = collection.DocumentSet.Document as JArray;
            if (jarray != null) {
                foreach (dynamic document in collection.DocumentSet.Document) {
                    var item = new SearchItem {
                        Id = JsonDotNetUtility.GetValue<int>(document.Field["DOCID"] ?? new JsonDotNetUtility.NullValue()),
                        UserId = JsonDotNetUtility.GetValue<string>(document.Field["USER_ID"] ?? new JsonDotNetUtility.NullValue()),
                        Title = JsonDotNetUtility.GetValue<string>(document.Field["TITLE"] ?? new JsonDotNetUtility.NullValue()),
                        Summary = JsonDotNetUtility.GetValue<string>(document.Field["CONTENTS"] ?? new JsonDotNetUtility.NullValue()),
                        Thumbnail = JsonDotNetUtility.GetValue<string>(document.Field["IMAGE"] ?? new JsonDotNetUtility.NullValue()),
                        Source = JsonDotNetUtility.GetValue<string>(document.Field["COMM_NAME"] ?? new JsonDotNetUtility.NullValue()),
                    };

                    string registedDateTime = JsonDotNetUtility.GetValue<string>(document.Field["Date"] ?? new JsonDotNetUtility.NullValue());
                    if (!string.IsNullOrEmpty(registedDateTime))
                        item.RegistedDateTime = registedDateTime.ConvertDateTimeyyyyMMddHHmmss();

                    models.Add(item);
                }
            }

            var jobj = collection.DocumentSet.Document as JObject;
            if (jobj != null) {
                dynamic document = collection.DocumentSet.Document;

                var item = new SearchItem {
                    Id = JsonDotNetUtility.GetValue<int>(document.Field["DOCID"] ?? new JsonDotNetUtility.NullValue()),
                    UserId = JsonDotNetUtility.GetValue<string>(document.Field["USER_ID"] ?? new JsonDotNetUtility.NullValue()),
                    Title = JsonDotNetUtility.GetValue<string>(document.Field["TITLE"] ?? new JsonDotNetUtility.NullValue()),
                    Summary = JsonDotNetUtility.GetValue<string>(document.Field["CONTENTS"] ?? new JsonDotNetUtility.NullValue()),
                    Thumbnail = JsonDotNetUtility.GetValue<string>(document.Field["IMAGE"] ?? new JsonDotNetUtility.NullValue()),
                    Source = JsonDotNetUtility.GetValue<string>(document.Field["COMM_NAME"] ?? new JsonDotNetUtility.NullValue()),
                };

                string registedDateTime = JsonDotNetUtility.GetValue<string>(document.Field["Date"] ?? new JsonDotNetUtility.NullValue());
                if (!string.IsNullOrEmpty(registedDateTime))
                    item.RegistedDateTime = registedDateTime.ConvertDateTimeyyyyMMddHHmmss();

                models.Add(item);
            }

            if (jarray == null && jobj == null)
                models = null;

            return models;
        }


        private IEnumerable<Source> GetSources(string id, dynamic multiGrouop) {
            string sourceCodeName = GetSourceCodeName(id);

            var result = new List<Source>();
            string source = JsonDotNetUtility.GetValue<string>(multiGrouop.Field[sourceCodeName] ?? new JsonDotNetUtility.NullValue());
            if (!string.IsNullOrEmpty(source)) {
                result.AddRange(
                    source.Split('@')
                          .Select(item => new Source { Code = item.Split('^')[0], Name = item.Split('^')[1], Count = System.Convert.ToInt32(item.Split('^')[2]) }));
            }
            return result;
        }

        private void GetSources(List<Source> sources, string id, dynamic multiGrouop) {
            if (multiGrouop != null) {
                string sourceCodeName = GetSourceCodeName(id);

                string source = JsonDotNetUtility.GetValue<string>(multiGrouop.Field[sourceCodeName] ?? new JsonDotNetUtility.NullValue());
                if (!string.IsNullOrEmpty(source)) {
                    sources.AddRange(
                        source.Split('@')
                              .Select(
                                  item => new Source { Code = item.Split('^')[0], Name = item.Split('^')[1], Count = System.Convert.ToInt32(item.Split('^')[2]) }));
                }
            }
        }

        private IEnumerable<Service> GetServices(string id, dynamic multiGroup) {
            string serviceCodeName = GetServiceCodeName(id);

            var result = new List<Service>();
            string service = JsonDotNetUtility.GetValue<string>(multiGroup.Field[serviceCodeName] ?? new JsonDotNetUtility.NullValue());
            if (!string.IsNullOrEmpty(service)) {
                result.AddRange(
                    service.Split('@')
                           .Select(
                               item => new Service { Code = item.Split('^')[0], Name = item.Split('^')[1], Count = System.Convert.ToInt32(item.Split('^')[2]) }));
            }
            return result;
        }

        private IEnumerable<Writer> GetReporters(string id, dynamic multiGroup) {
            string reporterCodeName = GetReporterCodeName(id);

            var result = new List<Writer>();
            string service = JsonDotNetUtility.GetValue<string>(multiGroup.Field[reporterCodeName] ?? new JsonDotNetUtility.NullValue());
            if (!string.IsNullOrEmpty(service)) {
                result.AddRange(
                    service.Split('@').Take(10).Select(
                        item => new Writer {
                            Id = System.Convert.ToInt32(item.Split('^')[0]),
                            Name = item.Split('^')[1],
                            Count = System.Convert.ToInt32(item.Split('^')[2])
                        }));
            }
            return result;
        }

        // 매체 검색 필터 정보 노드 명
        private string GetSourceCodeName(string id) {
            switch (id) {
                case "news":
                case "news_ja":
                case "news_group":
                case "ja_sunday":
                case "news_image_group":
                case "news_vod_group":
                case "joins_pdf_group":
                    return "SRC_GRP_CD";
                case "news_image":
                    return "DETAIL_SCH1";
                case "jons_pdf":
                    return "MDA_SEQ";
                //case "news_vod":
                default:
                    return "SOURCE_CODE";
            }
        }

        private string GetServiceCodeName(string id) {
            switch (id) {
                case "news_image":
                case "news_image_group":
                    return "Service_Code";
                default:
                    return "SERVICE_CODE";
            }
        }

        private string GetReporterCodeName(string id) {
            switch (id) {
                case "ja_sunday":
                case "news_image":
                case "news_image_group":
                    return "ART_REP_NO";
                default:
                    return "REPORTER_GROUP";
            }
        }
    }
}
