﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Repository.Search {
    public abstract class StaticConverter<TKey, TValue> {
        private readonly Dictionary<TKey, TValue> _dictionary;

        protected StaticConverter() {
            _dictionary = new Dictionary<TKey, TValue>();
        }

        protected void Initialize() {
            Regist(_dictionary);
        }

        protected abstract void Regist(Dictionary<TKey, TValue> dictionary);
        protected abstract TValue GetDefaultValue();
        protected abstract TKey GetDefaultKey();
        protected abstract bool IsConvertableKey(TKey key);
        protected abstract bool IsConvertableValue(TValue value);

        public TValue Convert(TKey key) {
            return IsConvertableKey(key) && _dictionary.ContainsKey(key) ? _dictionary[key] : GetDefaultValue();
        }

        public TKey Convert(TValue value) {
            return IsConvertableValue(value) && _dictionary.ContainsValue(value)
                       ? _dictionary.First(kv => kv.Value.Equals(value)).Key
                       : GetDefaultKey();
        }
    }

}
