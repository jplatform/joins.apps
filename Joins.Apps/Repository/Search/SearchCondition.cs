﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Joins.Apps.Models.Search;
using System.Globalization;

namespace Joins.Apps.Repository.Search {
    public class SearchCondition : PaginationCondition, IParameterProvider {
        public SearchCondition() {
            Page = 1;
            PageSize = 10;
            IsDuplicate = true;
        }

        public string Keyword { get; set; }
        public DateTime? StartSearchDate { get; set; }
        public DateTime? EndSearchDate { get; set; }
        public SortType SortType { get; set; }
        public SearchCategoryType SearchCategoryType { get; set; }
        public string MatchKeyword { get; set; }
        public string IncludeKeyword { get; set; }
        public string ExcluedeKeyword { get; set; }
        public PeriodType PeriodType { get; set; }
        public ScopeType ScopeType { get; set; }
        public string ServiceCode { get; set; }
        public string MasterCode { get; set; }
        public string SourceCode { get; set; }
        public string SourceGroupType { get; set; }
        public string ReporterCode { get; set; }
        public ImageType ImageType { get; set; }
        public JplusType JplusType { get; set; }
        public BlogType BlogType { get; set; }
        public ImageSearchType ImageSearchType { get; set; }
        public int TotalCount { get; set; }
        public int StartCount { get; set; }
        public bool IsChosung { get; set; }
        public IssueCategoryType IssueCategoryType { get; set; }
        public string DetailSearch { get; set; }
        public bool IsDuplicate { get; set; }
        public string SpecialPageCdNo { get; set; }
        public string SpecialPageLstYn { get; set; }

        public Dictionary<string, string> ToParameters() {
            var parameter = new Dictionary<string, string>();

            // 검색어
            if (!string.IsNullOrEmpty(Keyword)) {
                var keyword = new StringBuilder();
                keyword.Append(Keyword);

                if (!string.IsNullOrEmpty(IncludeKeyword))
                    keyword.AppendFormat(" {0}", IncludeKeyword);
                if (!string.IsNullOrEmpty(MatchKeyword))
                    keyword.AppendFormat(" \"{0}\"", MatchKeyword);
                if (!string.IsNullOrEmpty(ExcluedeKeyword))
                    keyword.AppendFormat(" !{0}", ExcluedeKeyword);

                parameter.Add("query", keyword.ToString());
            }

            // 검색 카테고리            
            parameter.Add("collection", new SearchCategoryTypeConverter().Convert(SearchCategoryType));

            // 기간
            if (!PeriodType.Equals(PeriodType.All)) {
                switch (PeriodType) {
                    case PeriodType.OneDay:
                        parameter.Add("startDate", DateTime.Now.ToString("yyyy.MM.dd"));
                        break;
                    case PeriodType.OneWeek:
                        parameter.Add("startDate", DateTime.Now.AddDays(-7).ToString("yyyy.MM.dd"));
                        break;
                    case PeriodType.OneMonth:
                        parameter.Add("startDate", DateTime.Now.AddMonths(-1).ToString("yyyy.MM.dd"));
                        break;
                    case PeriodType.DirectInput:
                        if (StartSearchDate != null)
                            parameter.Add("startDate", Convert.ToDateTime(StartSearchDate).ToString("yyyy.MM.dd"));
                        if (EndSearchDate != null)
                            parameter.Add("endDate", Convert.ToDateTime(EndSearchDate).ToString("yyyy.MM.dd"));
                        break;
                }
            }

            // 정렬            
            parameter.Add("sort", new SortTypeConverter().Convert(SortType));

            // 범위
            if (!ScopeType.Equals(ScopeType.All)) {
                switch (SearchCategoryType) {
                    case SearchCategoryType.UnifiedSearch: // 통합검색, 뉴스 검색, 중앙일보 검색, 전체뉴스 검색
                    case SearchCategoryType.News:
                    case SearchCategoryType.JoongangNews:
                    case SearchCategoryType.TotalNews:
                        switch (ScopeType) {
                            case ScopeType.TitleContent:
                                parameter.Add("sfield", "TITLE,CONTENTS");
                                break;
                            case ScopeType.Title:
                                parameter.Add("sfield", "TITLE");
                                break;
                            case ScopeType.Content:
                                parameter.Add("sfield", "CONTENTS");
                                break;
                            case ScopeType.Reporter:
                                parameter.Add("sfield", "REPORTER");
                                break;
                        }
                        break;
                    case SearchCategoryType.IssueNews:
                    case SearchCategoryType.OnlyJoongangNews:
                        switch (ScopeType) {
                            case ScopeType.TitleContent:
                                parameter.Add("sfield", "ART_TITLE,ART_CONTENT");
                                break;
                            case ScopeType.Title:
                                parameter.Add("sfield", "ART_TITLE");
                                break;
                            case ScopeType.Content:
                                parameter.Add("sfield", "ART_CONTENT");
                                break;
                            case ScopeType.Reporter:
                                parameter.Add("sfield", "ART_REPORTER");
                                break;
                            case ScopeType.Keyword:
                                parameter.Add("sfield", "ART_KWD");
                                break;
                            case ScopeType.TitleKeywordReporter:
                                parameter.Add("sfield", "ART_TITLE,ART_KWD,ART_REPORTER");
                                break;
                        }
                        break;
                    case SearchCategoryType.JoongangSundayNews: // 중앙선데이 검색                    
                        switch (ScopeType) {
                            case ScopeType.TitleContent:
                                parameter.Add("sfield", "post_title,post_content");
                                break;
                            case ScopeType.Title:
                                parameter.Add("sfield", "post_title");
                                break;
                            case ScopeType.Content:
                                parameter.Add("sfield", "post_content");
                                break;
                            case ScopeType.Keyword:
                                parameter.Add("sfield", "tag");
                                break;
                            case ScopeType.Reporter:
                                parameter.Add("sfield", "");
                                break;
                        }
                        break;
                    case SearchCategoryType.Image: // 이미지 검색
                        switch (ScopeType) {
                            case ScopeType.TitleContent:
                                parameter.Add("sfield", "TITLE,CONTENTS");
                                break;
                            case ScopeType.Title:
                                parameter.Add("sfield", "TITLE");
                                break;
                            case ScopeType.Content:
                                parameter.Add("sfield", "CONTENTS");
                                break;
                            case ScopeType.Reporter:
                                parameter.Add("sfield", "");
                                break;
                        }
                        break;
                    case SearchCategoryType.Video: // 동영상 검색
                        switch (ScopeType) {
                            case ScopeType.TitleContent:
                                parameter.Add("sfield", "ART_TITLE,ART_CONTENT");
                                break;
                            case ScopeType.Title:
                                parameter.Add("sfield", "ART_TITLE");
                                break;
                            case ScopeType.Content:
                                parameter.Add("sfield", "ART_CONTENT");
                                break;
                            case ScopeType.Reporter:
                                parameter.Add("sfield", "");
                                break;
                            case ScopeType.Keyword:
                                parameter.Add("sfield", "ART_KWD");
                                break;
                        }
                        break;
                    case SearchCategoryType.Joins: // 조인스 검색
                        switch (ScopeType) {
                            case ScopeType.TitleContent:
                                parameter.Add("sfield", "ART_TITLE,ART_CONTENT");
                                break;
                            case ScopeType.Title:
                                parameter.Add("sfield", "ART_TITLE");
                                break;
                            case ScopeType.Content:
                                parameter.Add("sfield", "ART_CONTENT");
                                break;
                            case ScopeType.Reporter:
                                parameter.Add("sfield", "");
                                break;
                        }
                        break;
                    case SearchCategoryType.Jplus: // J플러스 검색
                        switch (ScopeType) {
                            case ScopeType.TitleContent:
                                parameter.Add("sfield", "ART_TITLE,ART_CONTENT");
                                break;
                            case ScopeType.Title:
                                parameter.Add("sfield", "ART_TITLE");
                                break;
                            case ScopeType.Content:
                                parameter.Add("sfield", "ART_CONTENT");
                                break;
                            case ScopeType.Reporter:
                                parameter.Add("sfield", "ART_REPORTER");
                                break;
                        }
                        break;
                    case SearchCategoryType.Blog: // 블로그 검색
                        switch (ScopeType) {
                            case ScopeType.TitleContent:
                                parameter.Add("sfield", "TITLE,CONTENTS");
                                break;
                            case ScopeType.Title:
                                parameter.Add("sfield", "TITLE");
                                break;
                            case ScopeType.Content:
                                parameter.Add("sfield", "CONTENTS");
                                break;
                            case ScopeType.Reporter:
                                parameter.Add("sfield", "");
                                break;
                        }
                        break;
                }
            }

            // 매체 코드
            if (!string.IsNullOrEmpty(SourceCode))
                parameter.Add("sourceCode", SourceCode);

            // 매체 그룹 코드
            if (!string.IsNullOrEmpty(SourceGroupType)) {
                var param = new StringBuilder();
                int i = 0;
                foreach (string item in SourceGroupType.Split('|')) {
                    if (Enum.IsDefined(typeof(SourceGroupType), item)) {
                        var sourceGroupType = (SourceGroupType)Enum.Parse(typeof(SourceGroupType), item);
                        param.AppendFormat("{0}{1}", !i.Equals(0) ? "," : "", new SourceGroupTypeConverter().Convert(sourceGroupType));
                    }
                    i++;
                }

                string srcGrpCodeString = param.ToString();
                if (!string.IsNullOrEmpty(srcGrpCodeString))
                    parameter.Add("srcGrpCode", srcGrpCodeString);
            }

            // 분야
            if (!string.IsNullOrEmpty(ServiceCode))
                parameter.Add("serviceCode", ServiceCode);

            // 분야
            if (!string.IsNullOrEmpty(MasterCode))
                parameter.Add("masterCode", MasterCode);

            // 기자명
            if (!string.IsNullOrEmpty(ReporterCode))
                parameter.Add("artReporterNo", ReporterCode);

            // 이미지형태
            if (!ImageType.Equals(ImageType.All))
                parameter.Add("imgCateCode", new ImageTypeConverter().Convert(ImageType));

            // 블로그
            if (!BlogType.Equals(BlogType.All)) {
                string sFieldValue = string.Empty;
                if (parameter.ContainsKey("sfield"))
                    sFieldValue = parameter["sfield"];
                if (!string.IsNullOrEmpty(sFieldValue))
                    sFieldValue += ",";

                switch (BlogType) {
                    case BlogType.Image:
                        parameter.Add("isBlogImage", "Y");
                        break;
                    case BlogType.NickName:
                        sFieldValue += "USER_NAME,USER_NICKNAME";
                        break;
                    case BlogType.Tag:
                        sFieldValue += "TAG_LIST";
                        break;
                }

                if (!BlogType.Equals(BlogType.Image)) {
                    if (parameter.ContainsKey("sfield"))
                        parameter["sfield"] = sFieldValue;
                    else
                        parameter.Add("sfield", sFieldValue);
                }
            }


            // J플러스
            if (!JplusType.Equals(JplusType.All)) {
                string sFieldValue = string.Empty;
                if (parameter.ContainsKey("sfield"))
                    sFieldValue = parameter["sfield"];
                if (!string.IsNullOrEmpty(sFieldValue))
                    sFieldValue += ",";

                switch (JplusType) {
                    case JplusType.WriterName:
                        sFieldValue += "ART_REPORTER";
                        break;
                    case JplusType.WriterJob:
                        sFieldValue += "ART_REP_JOB";
                        break;
                    case JplusType.Tag:
                        sFieldValue += "ART_KWD";
                        break;
                }

                if (parameter.ContainsKey("sfield"))
                    parameter["sfield"] = sFieldValue;
                else
                    parameter.Add("sfield", sFieldValue);
            }

            // 초성 검색
            if (IsChosung)
                parameter.Add("chosung", "y");

            // 상세 검색
            if (!string.IsNullOrEmpty(DetailSearch)) {
                var detailSearchItem = new StringBuilder();
                foreach (string item in DetailSearch.Split(',')) {
                    var detailSearchFieldType = (DetailSearchFieldType)Enum.Parse(typeof(DetailSearchFieldType), item.Split(':')[0]);

                    detailSearchItem.AppendFormat("{0}:{1},", new DetailSearchFieldTypeConverter().Convert(detailSearchFieldType), item.Split(':')[1]);
                }

                parameter.Add("detailSearch", detailSearchItem.ToString());
            }

            // 중복문서 검출 여부 
            if (!IsDuplicate)
                parameter.Add("duplicate", "n");

            // 검색 시작번호 (페이징 처리)
            if (StartCount.Equals(0) && Page > 1)
                parameter.Add("startCount", ((Page - 1) * PageSize).ToString(CultureInfo.InvariantCulture));

            if (StartCount > 0)
                parameter.Add("startCount", StartCount.ToString(CultureInfo.InvariantCulture));

            // 페이지 사이즈
            if (PageSize > 0)
                parameter.Add("listCount", PageSize.ToString(CultureInfo.InvariantCulture));

            return parameter;
        }
    }
}
