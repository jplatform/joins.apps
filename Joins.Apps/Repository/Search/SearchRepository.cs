﻿using Joins.Apps.Models.Search;
using Joins.Apps.Repository.Search;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Joins.Apps.Repository.Search {
    public class SearchRepository {
       SearchCondition searchcondition= new SearchCondition();
       // string sSearchURL = "";
        public SearchRepository(SearchCondition schcon) {
            this.searchcondition = schcon;
       }
        public DefaultResult GetSearchResult() {

            string xmlString = JCube.AF.Module.PageCall.LoadPage(GetUrl());
            dynamic result = null;
            try {
                int start = xmlString.IndexOf("<?xml", StringComparison.Ordinal);
                if (start > -1)
                    xmlString = xmlString.Remove(0, start);
                xmlString = Filter(xmlString);
                var xml = new XmlDocument();
                xml.LoadXml(xmlString);
                string jsonString = JsonConvert.SerializeXmlNode(xml);
                result = JsonConvert.DeserializeObject(jsonString);
            } catch {}
            try {
                FindConverter<DefaultResult> converter = new FindConverter<DefaultResult>();
                if (converter == null)
                       return  new DefaultResult();
                    return converter.Convert(result);
            } catch {
                   return new DefaultResult();
            }
        }
        protected string GetUrl() {
            string sSearchURL = Joins.Apps.GLOBAL.FIND_DOMAIN + "search_xml.jsp";
            return string.Format("{0}?{1}", sSearchURL, DictToString(searchcondition.ToParameters()));
        }

       
        private string Filter(string xml) {
            if (xml == null)
                return null;

            var newString = new StringBuilder();

            foreach (char c in xml) {
                char ch = c;
                if (XmlConvert.IsXmlChar(ch))
                    newString.Append(ch);
            }
            return newString.ToString();
        }
        private string DictToString(Dictionary<string, string> dictionary) {
            var sb = new StringBuilder();

            foreach (var item in dictionary)
                sb.AppendFormat("{0}={1}&", item.Key, item.Value);

            return sb.ToString();
        }
    }
}
