﻿using Joins.Apps.Models.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Repository.Search {

    internal class SortTypeConverter : StaticConverter<string, SortType> {
        public SortTypeConverter() {
            Initialize();
        }

        protected override void Regist(Dictionary<string, SortType> dictionary) {
            dictionary.Add("DATE/DESC,RANK/DESC", SortType.New);
            dictionary.Add("RANK/DESC,DATE/DESC", SortType.Accuracy);
        }

        protected override SortType GetDefaultValue() {
            return SortType.New;
        }

        protected override string GetDefaultKey() {
            return "DATE/DESC,RANK/DESC";
        }

        protected override bool IsConvertableKey(string key) {
            return !string.IsNullOrEmpty(key);
        }

        protected override bool IsConvertableValue(SortType value) {
            return true;
        }
    }
    internal class SearchCategoryTypeConverter : StaticConverter<string, SearchCategoryType> {
        public SearchCategoryTypeConverter() {
            Initialize();
        }

        protected override void Regist(Dictionary<string, SearchCategoryType> dictionary) {
            dictionary.Add("ALL", SearchCategoryType.UnifiedSearch);
            dictionary.Add("NEWS", SearchCategoryType.News);
            dictionary.Add("issue,ja_sunday,news_ja,news_group", SearchCategoryType.JoongangNews);
            dictionary.Add("issue,ja_sunday,news_group", SearchCategoryType.JoongangSundayNews);
            dictionary.Add("issue,ja_sunday,news,news_group", SearchCategoryType.TotalNews);
            dictionary.Add("news,issue", SearchCategoryType.RelationNews);
            dictionary.Add("IMAGE", SearchCategoryType.Image);
            dictionary.Add("VOD", SearchCategoryType.Video);
            dictionary.Add("PDF", SearchCategoryType.Joins);
            dictionary.Add("person", SearchCategoryType.People);
            dictionary.Add("jplus", SearchCategoryType.Jplus);
            dictionary.Add("blog", SearchCategoryType.Blog);
            dictionary.Add("issue", SearchCategoryType.Issue);
            dictionary.Add("reporter", SearchCategoryType.Reporter);
            dictionary.Add("news", SearchCategoryType.IssueNews);
            dictionary.Add("news_ja", SearchCategoryType.OnlyJoongangNews);
        }

        protected override SearchCategoryType GetDefaultValue() {
            return SearchCategoryType.UnifiedSearch;
        }

        protected override string GetDefaultKey() {
            return "ALL";
        }

        protected override bool IsConvertableKey(string key) {
            return !string.IsNullOrEmpty(key);
        }

        protected override bool IsConvertableValue(SearchCategoryType value) {
            return true;
        }
    }
    internal class SourceGroupTypeConverter : StaticConverter<string, SourceGroupType> {
        public SourceGroupTypeConverter() {
            Initialize();
        }

        protected override void Regist(Dictionary<string, SourceGroupType> dictionary) {
            dictionary.Add("1,2,3,4,5,6,9", SourceGroupType.All);
            dictionary.Add("1", SourceGroupType.Joongang);
            dictionary.Add("2", SourceGroupType.IlganSports);
            dictionary.Add("3", SourceGroupType.Jtbc);
            dictionary.Add("4", SourceGroupType.JoongangSunday);
            dictionary.Add("5", SourceGroupType.JoongangDaily);
            dictionary.Add("6", SourceGroupType.JoongangOnline);
            dictionary.Add("9", SourceGroupType.Etc);
        }

        protected override SourceGroupType GetDefaultValue() {
            return SourceGroupType.All;
        }

        protected override string GetDefaultKey() {
            return "1,2,3,4,5,6,9";
        }

        protected override bool IsConvertableKey(string key) {
            return !string.IsNullOrEmpty(key);
        }

        protected override bool IsConvertableValue(SourceGroupType value) {
            return true;
        }
    }
    internal class ImageTypeConverter : StaticConverter<string, ImageType> {
        public ImageTypeConverter() {
            Initialize();
        }

        protected override void Regist(Dictionary<string, ImageType> dictionary) {
            dictionary.Add("", ImageType.All);
            dictionary.Add("001", ImageType.Article);
            dictionary.Add("002", ImageType.Photo);
            dictionary.Add("003", ImageType.Gallery);
        }

        protected override ImageType GetDefaultValue() {
            return ImageType.All;
        }

        protected override string GetDefaultKey() {
            return "";
        }

        protected override bool IsConvertableKey(string key) {
            return !string.IsNullOrEmpty(key);
        }

        protected override bool IsConvertableValue(ImageType value) {
            return true;
        }
    }
    internal class DetailSearchFieldTypeConverter : StaticConverter<string, DetailSearchFieldType> {
        public DetailSearchFieldTypeConverter() {
            Initialize();
        }

        protected override void Regist(Dictionary<string, DetailSearchFieldType> dictionary) {
            dictionary.Add("ART_TITLE", DetailSearchFieldType.Title);
            dictionary.Add("ART_SUBTITLE", DetailSearchFieldType.SubTitle);
            dictionary.Add("ART_CONTENT", DetailSearchFieldType.Content);
            dictionary.Add("ART_REPORTER", DetailSearchFieldType.Reporter);
            dictionary.Add("ART_KWD", DetailSearchFieldType.Keyword);
        }

        protected override DetailSearchFieldType GetDefaultValue() {
            return DetailSearchFieldType.Title;
        }

        protected override string GetDefaultKey() {
            return "ART_TITLE";
        }

        protected override bool IsConvertableKey(string key) {
            return !string.IsNullOrEmpty(key);
        }

        protected override bool IsConvertableValue(DetailSearchFieldType value) {
            return true;
        }
    }
}
