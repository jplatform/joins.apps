﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Repository.NewsLine {
    public class ArticleRepository {

        public static List<Joins.Apps.Models.NewsLine.TopicData> SetTopicList(int pgi = 1, int ps = 10, int seq = 0, string sw = "") {

            var topicArticleDataList = new List<Joins.Apps.Models.NewsLine.TopicData>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetSmartBriefDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@PageIndex", pgi);
                helper.ProcParamAdd("@PageSize", ps);
                helper.ProcParamAdd("@SEQ", seq);
                helper.ProcParamAdd("@sw", sw);
                DataSet ds = helper.ProcExecuteDataSet("USP_TMP_NEWSLINE_GetTopicList");
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        var rData = new Joins.Apps.Models.NewsLine.TopicData();
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        rData.seq = JCube.AF.Util.Converts.ToInt32(dr["seq"], 0);
                        rData.topic_date = JCube.AF.Util.Converts.ToString(dr["topic_date"]);
                        rData.topic_title = Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(dr["topic_name"].ToString()));
                        rData.topic_naver_article_cnt = JCube.AF.Util.Converts.ToInt32(dr["topic_naver_article_cnt"],0);
                        rData.topic_naver_date = JCube.AF.Util.Converts.ToString(dr["topic_naver_date"]);
                        rData.reg_date = JCube.AF.Util.Converts.ToString(dr["reg_date"]);
                        rData.topic_article_data =(JCube.AF.Util.Converts.ToString(dr["topic_article_data"]));
                        topicArticleDataList.Add(rData);
                    }
                }
                helper.Close();
            }
            return topicArticleDataList;
        }
        public static int SetTopicInput(Joins.Apps.Models.NewsLine.TopicInputData topic_data,string article_xml) {
            var topicArticleDataList = new List<Joins.Apps.Models.NewsLine.TopicArticleData>();
            using (JCube.AF.DAO.DataHelper helper = Joins.Apps.GLOBAL.GetSmartBriefDataHelper()) {
                helper.ProcParamClear();
                helper.ProcParamAdd("@TOPIC_NAME",SqlDbType.NVarChar, topic_data.topic);
                helper.ProcParamAdd("@TOPIC_NAVER_ARTICLE_CNT", topic_data.article_cnt);
                helper.ProcParamAdd("@TOPIC_NAVER_DATE", topic_data.topic_date);
                helper.ProcParamAdd("@ARTICLE_XML", SqlDbType.Xml, article_xml);
                DataSet ds = helper.ProcExecuteDataSet("USP_TMP_NEWSLIE_InsertData");
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows != null) {
                    int cnt = ds.Tables[0].Rows.Count;
                    for (int tmpi = 0; tmpi < cnt; tmpi++) {
                        var rData =new Joins.Apps.Models.NewsLine.TopicArticleData();
                        DataRow dr = ds.Tables[0].Rows[tmpi];
                        rData.topic_seq = JCube.AF.Util.Converts.ToInt32(dr["topic_seq"],0);
                        rData.total_id = JCube.AF.Util.Converts.ToInt32(dr["total_id"], 0);
                        rData.article_title = Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(dr["article_title"].ToString()));
                        rData.article_date = JCube.AF.Util.Converts.ToString(dr["article_date"]);
                        rData.reg_date = JCube.AF.Util.Converts.ToString(dr["reg_date"]);
                        rData.pre_topic_seq = JCube.AF.Util.Converts.ToInt32(dr["PRE_TOPIC_SEQ"], 0);
                        topicArticleDataList.Add(rData);
                    }
                }
                helper.Close();
            }
            
            /*
            {
				"TOPIC_SEQ" :2,
				"TOPIC_NAME":"네이버 토픽 2",
				"TOPIC_NAVER_COUNT":0,
				"TOPIC_NAVER_DATE":"2019.06.18 08:30 ~ 11:30",
				"ARTICLE_DATA":[
						{
							"ARTICLE_ID":234235,
							"ARTICLE_NAME":"기사제목3",
							"ARTICLE_DATE":"2019-06-17 11:54:00",
							"TOPIC_DATA":[
									{
									"TOPIC_SEQ" :1,
									"TOPIC_NAME":"네이버 토픽 1",
									"TOPIC_NAVER_COUNT":0,
									"TOPIC_NAVER_DATE":"2019.06.16 08:30 ~ 11:30",
									"ARTICLE_DATA":[
											{
												"ARTICLE_ID":1842312,
												"ARTICLE_NAME":"기사제목1",
												"ARTICLE_DATE":"2019-06-15 12:54:00",
												"TOPIC_DATA":[
												]
											},
											{
												"ARTICLE_ID":3465467,
												"ARTICLE_NAME":"기사제목2",
												"ARTICLE_DATE":"2019-06-15 12:54:00",
												"TOPIC_DATA":[
												]
											}
										]
									}
								]
						},{
							"ARTICLE_ID":1842312,
							"ARTICLE_NAME":"기사제목2",
							"ARTICLE_DATE":"2019-06-16 10:54:00",
							"TOPIC_DATA":[
							]
						}
				]
			
			}
			*/
            JCube.AF.IO.Dir.CheckAndCreate(Joins.Apps.GLOBAL.LogPath + "newsline\\");
            if (topicArticleDataList != null && topicArticleDataList.Count > 0) {
                int topic_seq = topicArticleDataList[0].topic_seq;
                StringBuilder sb = new StringBuilder();
                sb.Append("{");
                sb.Append("\"SEQ\" :"+ topic_seq + ", ");
                sb.Append("\"NM\":\""+topic_data.topic+"\",");
                sb.Append("\"N_CNT\":" + topic_data.article_cnt + ",");
                sb.Append("\"N_DT\":\""+ topic_data.topic_date + "\",");
                sb.Append("\"ART_DATA\":[");
                int cnt = 0;
                        foreach (Joins.Apps.Models.NewsLine.TopicArticleData topicarticledata in topicArticleDataList) {
                            if(cnt>0)
                                sb.Append(",{");
                            else
                            sb.Append("{");
                            sb.Append("\"A_ID\":" + topicarticledata.total_id + ",");
                            sb.Append("\"A_NM\":\"" + topicarticledata.article_title + "\",");
                            sb.Append("\"A_DT\":\"" + topicarticledata.article_date + "\",");
                            sb.Append("\"TOPIC\":[");
                            sb.Append(LoadFileText(Joins.Apps.GLOBAL.LogPath + "newsline\\" + topicarticledata.pre_topic_seq + ".json", "UTF-8", 99000));
                            sb.Append("]");
                            sb.Append("}");
                            cnt++;
                        }
                sb.Append("]");
                sb.Append("}");
                JCube.AF.IO.File.WriteFileText(Joins.Apps.GLOBAL.LogPath + "newsline\\" + topic_seq + ".json", sb.ToString(),true);
            }
            return 0;
        }

        /// <summary>파일을 텍스트로 읽어옵니다.</summary>
		/// <param name="trgPath">파일경로</param>
		/// <param name="enc">인코딩</param>
		/// <returns>파일내용</returns>
		public static string LoadFileText(string trgPath, string enc = "UTF-8",int maxLength=100000) {
            string rtn = "";
          //  StringBuilder sb = new StringBuilder();
            if (System.IO.File.Exists(trgPath)) {
                try {
                    
                    StringBuilder sb = new StringBuilder(maxLength);
                    using (var reader = System.IO.File.OpenText(trgPath)) {
                        int i;
                        while ((i = reader.Read()) > 0) {
                            char c = (char)i;
                          
                            sb.Append((char)c);
                            if (sb.Length > maxLength) {
                                break;
                            }
                        }
                        
                    }
                    rtn = sb.ToString();
                    //using (System.IO.StreamReader sr = new System.IO.StreamReader(trgPath, System.Text.Encoding.GetEncoding(enc))) {
                    //    rtn = sr.ReadToEnd();
                    //    sr.Close();
                    //}
                } catch { }
            }
            return rtn;
        }
    }
}
