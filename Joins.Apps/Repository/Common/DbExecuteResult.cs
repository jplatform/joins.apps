﻿namespace Joins.Apps.Repository.Common
{
    public class DbExecuteResult
    {
        /// <summary>
        /// 처리 결과 데이터
        /// </summary>
        public bool IsSuccess { get; set; } = false;
        /// <summary>
        /// 처리 결과 코드
        /// </summary>
        public int ResultCode { get; set; } = -1;
        /// <summary>
        /// 처리결과 메세지
        /// </summary>
        public string ResultMessage { get; set; } = "처리 중 오류가 발생하였습니다.";
    }
}
