﻿using Joins.Apps.Repository.Common.Attributes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;

namespace Joins.Apps.Repository.Common
{
    /// <summary>
    /// JCube.AF.DAO.DataHelper 확장클래스 입니다.
    /// </summary>
    public static class DataHelperExtensions
    {
        /// <summary>
        /// 쿼리를 실행하고 쿼리에서 반환된 결과 집합에서 첫 번째 행의 첫 번째 열을 반환합니다. 다른 열이나 행은 무시됩니다.
        /// </summary>
        /// <param name="helper">DataHelper</param>
        /// <param name="procName">프로시저 이름</param>
        /// <param name="parameters">파라메터</param>
        /// <param name="defaultValue">기본값</param>
        /// <returns>실행결과</returns>
        public static int ProcExecuteScalar(this JCube.AF.DAO.DataHelper helper, string procName, IDbDataParameter[] parameters, int defaultValue)
        {
            return AddParametersAll(helper, parameters).ProcExecuteScalar(procName, defaultValue);
        }
        /// <summary>
        /// 쿼리를 실행하고 쿼리에서 반환된 결과 집합에서 첫 번째 행의 첫 번째 열을 반환합니다. 다른 열이나 행은 무시됩니다.
        /// </summary>
        /// <param name="helper">DataHelper</param>
        /// <param name="procName">프로시저 이름</param>
        /// <param name="parameters">파라메터</param>
        /// <param name="defaultValue">기본값</param>
        /// <returns>실행결과</returns>
        public static string ProcExecuteScalar(this JCube.AF.DAO.DataHelper helper, string procName, IDbDataParameter[] parameters, string defaultValue)
        {
            return AddParametersAll(helper, parameters).ProcExecuteScalar(procName, defaultValue);
        }
        /// <summary>
        /// 쿼리를 실행하고 쿼리에서 반환된 결과 집합에서 첫 번째 행의 첫 번째 열을 반환합니다. 다른 열이나 행은 무시됩니다.
        /// </summary>
        /// <param name="helper">DataHelper</param>
        /// <param name="procName">프로시저 이름</param>
        /// <param name="parameters">파라메터</param>
        /// <returns>실행결과</returns>
        public static object ProcExecuteScalar(this JCube.AF.DAO.DataHelper helper, string procName, IDbDataParameter[] parameters)
        {
            return AddParametersAll(helper, parameters).ProcExecuteScalar(procName);
        }
        /// <summary>
        /// 결과 값을 반환하지 않는 쿼리를 실행합니다.
        /// </summary>
        /// <param name="helper">DataHelper</param>
        /// <param name="procName">프로시저 이름</param>
        /// <param name="parameters">파라메터</param>
        /// <returns>실행 결과가 적용된 행 개수</returns>
        public static int ProcExecuteNonQuery(this JCube.AF.DAO.DataHelper helper, string procName, IDbDataParameter[] parameters)
        {
            return AddParametersAll(helper, parameters).ProcExecuteNonQuery(procName);
        }

        /// <summary>
        /// 결과셋을 반환하는 쿼리를 실행합니다.
        /// </summary>
        /// <param name="helper">DataHelper</param>
        /// <param name="procName">프로시저 이름</param>
        /// <param name="parameters">파라메터</param>
        /// <returns>결과 셋</returns>
        public static DataSet ProcExecuteDataSet(this JCube.AF.DAO.DataHelper helper, string procName, IDbDataParameter[] parameters)
        {
            return AddParametersAll(helper, parameters).ProcExecuteDataSet(procName);
        }

        /// <summary>
        /// 파라메터를 추가합니다.
        /// </summary>
        /// <param name="helper">DataHelper</param>
        /// <param name="direction">파라메터 형식</param>
        /// <param name="parameterName">파라메터 이름</param>
        /// <param name="value">값</param>
        /// <returns>DataHelper</returns>
        private static JCube.AF.DAO.DataHelper AddParameter(JCube.AF.DAO.DataHelper helper, IDbDataParameter parameter)
        {
            if (ReferenceEquals(parameter.Value, null))
                parameter.Value = DBNull.Value;

            helper.CommandObj.Parameters.Add(parameter);

            return helper;
        }

        /// <summary>
        /// 파라메터를 모두 추가합니다.
        /// </summary>
        /// <param name="helper">DataHelper</param>
        /// <param name="parameters">파라메터 목록</param>
        /// <returns>DataHelper</returns>
        public static JCube.AF.DAO.DataHelper AddParametersAll(this JCube.AF.DAO.DataHelper helper, IDbDataParameter[] parameters)
        {
            parameters.ToList().ForEach(p => AddParameter(helper, p));
            return helper;
        }

        /// <summary>
        /// 출력용 파라메터에 설정된 값을 가져옵니다.
        /// </summary>
        /// <typeparam name="T">형식</typeparam>
        /// <param name="helper">DataHelper</param>
        /// <param name="parameterName">출력용 파라메터 이름</param>
        /// <param name="defaultValue">기본 값</param>
        /// <returns>값</returns>
        public static T GetOutputParameterValue<T>(this JCube.AF.DAO.DataHelper helper, string parameterName, T defaultValue)
        {
            var parameter = helper.CommandObj.Parameters.Cast<SqlParameter>().Where(p => {
                return (p.Direction == ParameterDirection.Output || p.Direction == ParameterDirection.InputOutput) && p.ParameterName.Equals(parameterName, StringComparison.OrdinalIgnoreCase);
            }).FirstOrDefault();

            if (parameter != null)
                return (T)parameter.Value;

            return defaultValue;
        }

        /// <summary>
        /// 데이터테이블을 지정한 형식으로 변환하여 반환합니다.
        /// </summary>
        /// <typeparam name="T">반환 형식</typeparam>
        /// <param name="helper">DataHelper</param>
        /// <param name="table">데이터 테이블</param>
        /// <param name="action">반환할 각 요소에 대해 수행할 대리자입니다.</param>
        /// <returns></returns>
        public static IEnumerable<T> ConvertEntity<T>(this JCube.AF.DAO.DataHelper helper, DataTable table, Action<T, DataRow> action = null) where T : new()
        {
            var result = new List<T>();
            
            table.AsEnumerable().ToList().ForEach(row =>
            {
                result.Add(ConvertEntity<T>(row, action));
            });

            return result;
        }

        /// <summary>
        /// 데이터테이블을 지정한 형식으로 변환하여 반환합니다.
        /// </summary>
        /// <typeparam name="T">반환 형식</typeparam>
        /// <param name="helper">DataHelper</param>
        /// <param name="row">데이터 행</param>
        /// <param name="action">반환 요소에 대해 수행할 대리자입니다.</param>
        /// <returns></returns>
        public static T ConvertEntity<T>(this JCube.AF.DAO.DataHelper helper, DataRow row, Action<T, DataRow> action = null) where T : new()
        {
            return ConvertEntity<T>(row, action);
        }

        /// <summary>
        /// 데이터테이블을 지정한 형식으로 변환하여 반환합니다.
        /// </summary>
        /// <typeparam name="T">반환 형식</typeparam>
        /// <param name="row">데이터 행</param>
        /// <param name="action">반환할 요소에 대해 수행할 대리자입니다.</param>
        /// <returns></returns>
        private static T ConvertEntity<T>(DataRow row, Action<T, DataRow> action = null) where T : new()
        {
            var property = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => p.CanWrite).ToList();
            var result = new T();

            property.ForEach(p =>
            {
                var columnName = p.Name;

                if (p.IsDefined(typeof(DbColumnAttribute), false))
                {
                    var attr = p.GetCustomAttribute(typeof(DbColumnAttribute), false) as DbColumnAttribute;

                    if (attr.Ignore)
                        return;

                    columnName = attr.ColumnName;
                }

                if (row.Table.Columns.Contains(columnName))
                {
                    var propType = Nullable.GetUnderlyingType(p.PropertyType) ?? p.PropertyType;
                    var safeValue = row[columnName] == DBNull.Value ? null : Convert.ChangeType(row[columnName], propType);

                    p.SetValue(result, safeValue);
                }
            });

            action?.Invoke(result, row);

            return result;
        }
    }
}