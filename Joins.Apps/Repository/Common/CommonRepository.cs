﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Joins.Apps.Repository.Common
{
    public class CommonRepository
    {
        public static bool IsNews10DbLive()
        {
            var fail_cnt = 0;

            for (var i = 0; i < 3; i++)
            {
                JCube.AF.DAO.DataHelper db = null;

                try
                {
                    db = GLOBAL.GetDataHelper();
                    db.ExecuteNonQuery("select 1");
                    break;
                }
                catch
                {
                    fail_cnt++;
                }
                finally
                {
                    if (db != null)
                    {
                        db.Close();
                        db.Dispose();
                    }
                }

                Thread.Sleep(1000);
            }

            return fail_cnt < 3;
        }
    }
}
