﻿using System;

namespace Joins.Apps.Repository.Common.Attributes
{
    /// <summary>
    /// DB에 매핑할 컬럼명을 지정하는 속성입니다.
    /// </summary>
    public class DbColumnAttribute : Attribute
    {
        /// <summary>
        /// 매핑할 컬럼명
        /// </summary>
        public string ColumnName { get; }
        /// <summary>
        /// 매핑하지 않는 컬럼임을 표시하는 속성입니다.
        /// </summary>
        public bool Ignore { get; set; }

        public DbColumnAttribute() { }

        /// <summary>
        /// DB에 매핑할 컬럼명을 지정합니다.
        /// </summary>
        /// <param name="columnName">매핑할 컬럼명</param>
        public DbColumnAttribute(string columnName)
        {
            this.ColumnName = columnName;
        }
    }
}
