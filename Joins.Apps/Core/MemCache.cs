﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Joins.Apps.Core {
    public class MemCache {
        #region
        public static Joins.Apps.Models.News10.CardInfo GetMemCardInfo(string card_id) {
            Joins.Apps.Models.News10.CardInfo returnValue = null;
            var cardinfo1 = Joins.Apps.Common.Util.GetCache("DBCARDINFODATA_" + card_id);
            if (cardinfo1 != null) {
                try { returnValue = (Joins.Apps.Models.News10.CardInfo)cardinfo1; } catch {  }
            }else {
                Joins.Apps.Models.News10.CardInfo cardinfo = Joins.Apps.Repository.News10.CardRepository.GetCardInfo(card_id);
                if (cardinfo != null && !string.IsNullOrEmpty(cardinfo.CARD_ID)) {Joins.Apps.Common.Util.SetCache("DBCARDINFODATA_" + card_id, cardinfo, 5);}
                returnValue = cardinfo;
            }
            return returnValue;
        }
        #endregion
        #region [Cruz API 호출방식]
        public static string GetMemCruzCall(int age, int gen) {
            string r = JCube.AF.Util.Converts.ToString(Joins.Apps.Common.Util.GetCache("NEWS10CRUZCALL_" + age + "_" + gen), "");
            if (string.IsNullOrEmpty(r)) {
                try {
                    r = JCube.AF.Module.PageCall.LoadPage("http://cruz.joins.com/app?gen=" + gen + "&age=" + age + "&div=1");
                } catch { }
             //   r = JCube.AF.Module.PageCall.LoadPage("http://cruz.joins.com/app");
                if (r.IndexOf("News10") >= 0 && r.Length > 100) { Joins.Apps.Common.Util.SetCache("NEWS10CRUZCALL_" + age + "_" + gen, r, 1); }
            }
            return r;
        }
      
        public static Joins.Apps.Models.News10.Cruz GetMemCruzData(int age, int gen) {
            Joins.Apps.Models.News10.Cruz returnValue = null;
            var cruz1 = Joins.Apps.Common.Util.GetCache("NEWS10CRUZDATA_" + age + "_" + gen);
            try {
                if (cruz1 != null) { returnValue = (Joins.Apps.Models.News10.Cruz)cruz1; }
                else {
                    string sCruz = GetMemCruzCall(age, gen);
                    Joins.Apps.Models.News10.Cruz cruz = JsonConvert.DeserializeObject<Joins.Apps.Models.News10.Cruz>(sCruz);
                    Joins.Apps.Common.Util.SetCache("NEWS10CRUZDATA_" + age + "_" + gen, cruz, 1);
                    returnValue = cruz;
                }
            } catch (ArgumentException) { returnValue = null; }
            return returnValue;
        }
        #endregion

        #region [DB 출력방식]
        public static List<Joins.Apps.Models.News10.Cruz.News10> GetMemDBCruzNews10Data(int age, int gen) {
            List<Joins.Apps.Models.News10.Cruz.News10> returnValue = null;
            var cruz1 = Joins.Apps.Common.Util.GetCache("DBCRUZNEWS10DATA_" + age + "_" + gen);
            try {
               
                if (cruz1 != null) { returnValue = (List<Joins.Apps.Models.News10.Cruz.News10>)cruz1; }
                else {
                    List<Joins.Apps.Models.News10.Cruz.News10> CruzNew10DataList = Joins.Apps.Repository.News10.CruzRepository.GetCruzNews10List(gen, age);
                    Joins.Apps.Common.Util.SetCache("DBCRUZNEWS10DATA_" + age + "_" + gen, CruzNew10DataList, 1);
                    returnValue = CruzNew10DataList;
                }
            } catch (ArgumentException) { returnValue = null; }
            return returnValue;
        }
        public static List<Joins.Apps.Models.News10.Cruz.TTS> GetMemDBCruzNews10OnlyTTSData(int age, int gen) {
            List<Joins.Apps.Models.News10.Cruz.TTS> returnValue = null;
            var cruz1 = Joins.Apps.Common.Util.GetCache("DBCRUZNEWS10_TTS_DATA_" + age + "_" + gen);
            try {

                if (cruz1 != null) { returnValue = (List<Joins.Apps.Models.News10.Cruz.TTS>)cruz1; }
                else {
                    List<Joins.Apps.Models.News10.Cruz.TTS> CruzNew10DataList = Joins.Apps.Repository.News10.CruzRepository.GetCruzNews10OnlyTTSList(gen, age);
                    Joins.Apps.Common.Util.SetCache("DBCRUZNEWS10_TTS_DATA_" + age + "_" + gen, CruzNew10DataList, 1);
                    returnValue = CruzNew10DataList;
                }
            } catch (ArgumentException) { returnValue = null; }
            return returnValue;
        }
        public static List<Joins.Apps.Models.News10.Cruz.News10_NOTTS> GetMemDBCruzNews10NoTTSData(int age, int gen) {
            List<Joins.Apps.Models.News10.Cruz.News10_NOTTS> returnValue = null;
            var cruz1 = Joins.Apps.Common.Util.GetCache("DBCRUZNEWS10_NOTTS_DATA_" + age + "_" + gen);
            try {

                if (cruz1 != null) { returnValue = (List<Joins.Apps.Models.News10.Cruz.News10_NOTTS>)cruz1; }
                else {
                    List<Joins.Apps.Models.News10.Cruz.News10_NOTTS> CruzNew10DataList = Joins.Apps.Repository.News10.CruzRepository.GetCruzNews10NoTTSList(gen, age);
                    Joins.Apps.Common.Util.SetCache("DBCRUZNEWS10_NOTTS_DATA_" + age + "_" + gen, CruzNew10DataList, 1);
                    returnValue = CruzNew10DataList;
                }
            } catch (ArgumentException) { returnValue = null; }
            return returnValue;
        }
        public static List<Joins.Apps.Models.News10.Cruz.News10> GetMemDBCruzNews10BixbyData(int age, int gen) {
            List<Joins.Apps.Models.News10.Cruz.News10> returnValue = null;
            var cruz1 = Joins.Apps.Common.Util.GetCache("DBCRUZNEWS10BIXBYDATA_" + age + "_" + gen);
            try {

                if (cruz1 != null) { returnValue = (List<Joins.Apps.Models.News10.Cruz.News10>)cruz1; }
                else {
                    List<Joins.Apps.Models.News10.Cruz.News10> CruzNew10DataList = Joins.Apps.Repository.News10.CruzRepository.GetCruzNews10List(gen, age,true);
                    Joins.Apps.Common.Util.SetCache("DBCRUZNEWS10BIXBYDATA_" + age + "_" + gen, CruzNew10DataList, 1);
                    returnValue = CruzNew10DataList;
                }
            } catch (ArgumentException) { returnValue = null; }
            return returnValue;
        }
        public static List<Joins.Apps.Models.News10.Cruz.News10_NOContentTTS> GetMemDBCruzNews10BixbyData2(int age, int gen) {
            List<Joins.Apps.Models.News10.Cruz.News10_NOContentTTS> returnValue = null;
            var cruz1 = Joins.Apps.Common.Util.GetCache("DBCRUZNEWS10BIXBYDATA2_" + age + "_" + gen);
            try {

                if (cruz1 != null) { returnValue = (List<Joins.Apps.Models.News10.Cruz.News10_NOContentTTS>)cruz1; }
                else {
                    List<Joins.Apps.Models.News10.Cruz.News10_NOContentTTS> CruzNew10DataList = Joins.Apps.Repository.News10.CruzRepository.GetCruzNews10NoContentTTSList(gen, age, true);
                    Joins.Apps.Common.Util.SetCache("DBCRUZNEWS10BIXBYDATA2_" + age + "_" + gen, CruzNew10DataList, 5);
                    returnValue = CruzNew10DataList;
                }
            } catch (ArgumentException) { returnValue = null; }
            return returnValue;
        }
        public static List<Joins.Apps.Models.News10.Cruz2.News10> GetMemDBCruzNews10Data2(int age, int gen) {
            List<Joins.Apps.Models.News10.Cruz2.News10> returnValue = null;
            var cruz1 = Joins.Apps.Common.Util.GetCache("DBCRUZNEWS10DATA2_" + age + "_" + gen);
            try {

                if (cruz1 != null) { returnValue = (List<Joins.Apps.Models.News10.Cruz2.News10>)cruz1; }
                else {
                    List<Joins.Apps.Models.News10.Cruz2.News10> CruzNew10DataList = Joins.Apps.Repository.News10.CruzRepository.GetCruzNews10List2(gen, age);
                    Joins.Apps.Common.Util.SetCache("DBCRUZNEWS10DATA2_" + age + "_" + gen, CruzNew10DataList, 1);
                    returnValue = CruzNew10DataList;
                }
            } catch (ArgumentException) { returnValue = null; }
            return returnValue;
        }
        public static List<Joins.Apps.Models.News10.Cruz.HotKwd> GetMemDBCruzkeywordData() {
            List<Joins.Apps.Models.News10.Cruz.HotKwd> returnValue = null;
            var cruz1 = Joins.Apps.Common.Util.GetCache("DBCRUZNEWS10KEYWORDDATA");
            try {

                if (cruz1 != null) { returnValue = (List<Joins.Apps.Models.News10.Cruz.HotKwd>)cruz1; }
                else {
                    List<Joins.Apps.Models.News10.Cruz.HotKwd> CruzNew10KeywordDataList = Joins.Apps.Repository.News10.CruzRepository.GetCruzKeywordList();
                    Joins.Apps.Common.Util.SetCache("DBCRUZNEWS10KEYWORDDATA" , CruzNew10KeywordDataList, 1);
                    returnValue = CruzNew10KeywordDataList;
                }
            } catch (ArgumentException) { returnValue = null; }
            return returnValue;
        }
        public static List<Joins.Apps.Models.News10.Cruz.Special> GetMemDBCruzSpecialData(int age, int gen,int cardVersion) {
            List<Joins.Apps.Models.News10.Cruz.Special> returnValue = null;
            var cruz1 = Joins.Apps.Common.Util.GetCache("DBCRUZNEWS10SPECIALDATA_" + age + "_" + gen + "_" + cardVersion);
            try {

                if (cruz1 != null) { returnValue = (List<Joins.Apps.Models.News10.Cruz.Special>)cruz1; }
                else {
                    List<Joins.Apps.Models.News10.Cruz.Special> CruzNew10SpecialDataList = Joins.Apps.Repository.News10.CruzRepository.GetCruzSpecialList(gen, age, cardVersion);
                    Joins.Apps.Common.Util.SetCache("DBCRUZNEWS10SPECIALDATA_" + age + "_" + gen + "_" + cardVersion, CruzNew10SpecialDataList, 1);
                    returnValue = CruzNew10SpecialDataList;
                }
            } catch (ArgumentException) { returnValue = null; }
            return returnValue;
        }
        #endregion

        public static string GetMemCruzScoreCall() {
            string r = JCube.AF.Util.Converts.ToString(Joins.Apps.Common.Util.GetCache("NEWS10CRUZSCORECALL"), "");
            if (string.IsNullOrEmpty(r)) {
                try {r = JCube.AF.Module.PageCall.LoadPage("http://crawling.joins.com/sports");} catch { }
                if (r.IndexOf("today") >= 0 && r.Length > 100) { Joins.Apps.Common.Util.SetCache("NEWS10CRUZSCORECALL", r, 1); }
            }
            return r;
        }
        public static string GetMemCruzScoreData() {
            string returnValue = null;
            string cruz1 = JCube.AF.Util.Converts.ToString(Joins.Apps.Common.Util.GetCache("NEWS10CRUZSCOREDATA"), "");
            try {
                if (cruz1 != null && !string.IsNullOrEmpty(cruz1)) { returnValue = cruz1; }
                else {
                    string sCruzScore = GetMemCruzScoreCall();
                    Joins.Apps.Common.Util.SetCache("NEWS10CRUZSCOREDATA", sCruzScore, 1);
                    returnValue = sCruzScore;
                }
            } catch (ArgumentException) { returnValue = null; }
            return returnValue;
        }
        #region [boxoffice]
        public static Joins.Apps.Models.News10.BoxOffice.P351Response GetMemCruzBoxOffice() {
            Joins.Apps.Models.News10.BoxOffice.P351Response returnValue = null;
            object ob =Joins.Apps.Common.Util.GetCache("NEWS10CRUZBOXOFFICEDATA");
            DateTime memTime = JCube.AF.Util.Converts.ToDateTime(Joins.Apps.Common.Util.GetCache("NEWS10CRUZBOXOFFICEDATA_TIME"),DateTime.Now.AddHours(-2));
            if (memTime < DateTime.Now.AddHours(-1)) {
               // Joins.Apps.Models.News10.BoxOffice.P351Response r=GetMegaBoxSOAPData_NEW();
                Joins.Apps.Models.News10.BoxOffice.P351Response r = GetMegaBoxSOAPData();
                if (r != null && r.partnership != null && r.partnership.header.ResultCode.Equals("0000")) {
                    Joins.Apps.Common.Util.SetCache("NEWS10CRUZBOXOFFICEDATA", r, 120);
                    Joins.Apps.Common.Util.SetCache("NEWS10CRUZBOXOFFICEDATA_TIME", DateTime.Now, 120);
                    returnValue = r;
                }else {
                    if (ob != null) {
                        Joins.Apps.Common.Util.SetCache("NEWS10CRUZBOXOFFICEDATA", ob, 120);
                        Joins.Apps.Common.Util.SetCache("NEWS10CRUZBOXOFFICEDATA_TIME", DateTime.Now, 120);
                        returnValue = (Joins.Apps.Models.News10.BoxOffice.P351Response)ob;
                    }
                }
            }else {
                if (ob == null) {
                  //  Joins.Apps.Models.News10.BoxOffice.P351Response r = GetMegaBoxSOAPData_NEW();
                    Joins.Apps.Models.News10.BoxOffice.P351Response r = GetMegaBoxSOAPData();
                    if (r != null && r.partnership != null && r.partnership.header.ResultCode.Equals("0000")) {
                        Joins.Apps.Common.Util.SetCache("NEWS10CRUZBOXOFFICEDATA", r, 120);
                        Joins.Apps.Common.Util.SetCache("NEWS10CRUZBOXOFFICEDATA_TIME", DateTime.Now, 120);
                        returnValue = r;
                    }
                }else {returnValue=(Joins.Apps.Models.News10.BoxOffice.P351Response)ob;}
            }
            /*랭킹 재조정 시작*/
            if (returnValue != null && returnValue.partnership != null && returnValue.partnership.body != null && returnValue.partnership.body.p351Entity != null && returnValue.partnership.body.p351Entity.Count > 0) {
                int iRank = 1;
                List<Joins.Apps.Models.News10.BoxOffice.P351Entity> P351EntityList = new List<Joins.Apps.Models.News10.BoxOffice.P351Entity>();
                foreach (Joins.Apps.Models.News10.BoxOffice.P351Entity data in returnValue.partnership.body.p351Entity) {
                    data.RANK = iRank.ToString();
                    P351EntityList.Add( data);
                    iRank++;
                }
                returnValue.partnership.body.p351Entity = P351EntityList;
            }
            /*랭킹 재조정 끝*/
            return returnValue;
        }
        public static Joins.Apps.Models.News10.BoxOffice.P351Response GetMegaBoxSOAPData_NEW() {
            string soapResult = "";
            Joins.Apps.Models.News10.BoxOffice.P351Response boData = null;
            HttpWebRequest request = CreateWebRequest_NEW();
            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" xmlns:p351=""urn:megaware:/mediate/megabox/Top10/P351/P351.wsdl"">
                <soap:Header/>
              <soap:Body>
                <p351:request>
                  <SERVICEKEY>AAGcPP9Rz9ngcMhUHNPdRlme</SERVICEKEY>
                  <SORTBY>1</SORTBY>
                  <STARTNO>0</STARTNO>
                  <COUNT>12</COUNT>
                </p351:request>
              </soap:Body>
            </soap:Envelope> ");
            using (Stream stream = request.GetRequestStream()) { soapEnvelopeXml.Save(stream); }
            using (WebResponse response = request.GetResponse()) { using (StreamReader rd = new StreamReader(response.GetResponseStream())) { soapResult = rd.ReadToEnd(); } }
            if (!string.IsNullOrEmpty(soapResult)) {
                try {
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(soapResult);
                    var soapBody = xmlDocument.GetElementsByTagName("Body")[0];

                    string innerObject = soapBody.InnerXml;
                    boData = JCube.AF.Serialize.XML.XMLTo<Joins.Apps.Models.News10.BoxOffice.P351Response>(soapResult);
                } catch (Exception e) { }
            }
            return boData;
        }
        public static HttpWebRequest CreateWebRequest_NEW() {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@"https://esb.megabox.co.kr/mediate/megabox/Top10/P351");
            //  webRequest.Headers.Add(@"SOAPAction:http://www.megabox.co.kr/P351");
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }
        public static Joins.Apps.Models.News10.BoxOffice.P351Response GetMegaBoxSOAPData() {
            string soapResult = "";
            Joins.Apps.Models.News10.BoxOffice.P351Response boData = null;
            HttpWebRequest request = CreateWebRequest();
            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
              <soap:Body>
                <P351 xmlns=""http://www.megabox.co.kr/"">
                  <serviceKey>AAGcPP9Rz9ngcMhUHNPdRlme</serviceKey>
                  <SORTBY>1</SORTBY>
                  <STARTNO>0</STARTNO>
                  <COUNT>12</COUNT>
                </P351>
              </soap:Body>
            </soap:Envelope> ");
            using (Stream stream = request.GetRequestStream()) { soapEnvelopeXml.Save(stream); }
            using (WebResponse response = request.GetResponse()) { using (StreamReader rd = new StreamReader(response.GetResponseStream())) { soapResult = rd.ReadToEnd(); } }
            if (!string.IsNullOrEmpty(soapResult)) {
                try {
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(soapResult);
                    var soapBody = xmlDocument.GetElementsByTagName("soap:Body")[0];

                    string innerObject = soapBody.InnerXml;
                    boData = JCube.AF.Serialize.XML.XMLTo<Joins.Apps.Models.News10.BoxOffice.P351Response>(innerObject);
                } catch (Exception e) { }
            }
            return boData;
        }
       
        public static HttpWebRequest CreateWebRequest() {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@"https://icewine.megabox.co.kr/McPartnerShipService/OnlinePartnerShipService.asmx");
            webRequest.Headers.Add(@"SOAPAction:http://www.megabox.co.kr/P351");
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }
        #endregion

        #region [평창 2018 스페셜 ]
        public static Joins.Apps.Models.News10.PyeongchangInput.article_list GetMemCruzPyeongchangCall() {
            Joins.Apps.Models.News10.PyeongchangInput.article_list boData = null;
            string sResult = "";
            try { sResult = JCube.AF.Module.PageCall.LoadPage("http://static.joins.com/joongang_15re/scripts/data/pyeongchang2018/xml/pyeongchang_showcase.xml"); } catch { }
            if (!string.IsNullOrEmpty(sResult)) {
                try {
                    boData = JCube.AF.Serialize.XML.XMLTo<Joins.Apps.Models.News10.PyeongchangInput.article_list>(sResult);
                } catch (Exception e) { }
            }
            return boData;
        }
        public static Joins.Apps.Models.News10.PyeongchangOutput GetMemCruzPyeongchang() {
            Joins.Apps.Models.News10.PyeongchangOutput returnValue = null;
            object ob = Joins.Apps.Common.Util.GetCache("NEWS10CRUZPYEONGCHANGDATA");
            DateTime memTime = JCube.AF.Util.Converts.ToDateTime(Joins.Apps.Common.Util.GetCache("NEWS10CRUZPYEONGCHANGDATA_TIME"), DateTime.Now.AddHours(-2));
            if (memTime < DateTime.Now.AddHours(-1)) {
                Joins.Apps.Models.News10.PyeongchangInput.article_list r = GetMemCruzPyeongchangCall();
                if (r != null && r.article != null) {
                    Joins.Apps.Common.Util.SetCache("NEWS10CRUZPYEONGCHANGDATA", r, 120);
                    Joins.Apps.Common.Util.SetCache("NEWS10CRUZPYEONGCHANGDATA_TIME", DateTime.Now, 120);
                    returnValue = GetCruzPyeongChangOutput(r);
                }
                else {
                    if (ob != null) {
                        Joins.Apps.Common.Util.SetCache("NEWS10CRUZPYEONGCHANGDATA", ob, 120);
                        Joins.Apps.Common.Util.SetCache("NEWS10CRUZPYEONGCHANGDATA_TIME", DateTime.Now, 120);
                        returnValue = GetCruzPyeongChangOutput((Joins.Apps.Models.News10.PyeongchangInput.article_list)ob);
                    }
                }
            }
            else {
                if (ob == null) {
                    Joins.Apps.Models.News10.PyeongchangInput.article_list r = GetMemCruzPyeongchangCall();
                    if (r != null && r.article != null) {
                        Joins.Apps.Common.Util.SetCache("NEWS10CRUZPYEONGCHANGDATA", r, 120);
                        Joins.Apps.Common.Util.SetCache("NEWS10CRUZPYEONGCHANGDATA_TIME", DateTime.Now, 120);
                        returnValue = GetCruzPyeongChangOutput(r);
                    }
                }
                else { returnValue = GetCruzPyeongChangOutput((Joins.Apps.Models.News10.PyeongchangInput.article_list)ob); }
            }
            return returnValue;
        }
        private static Joins.Apps.Models.News10.PyeongchangOutput GetCruzPyeongChangOutput(Joins.Apps.Models.News10.PyeongchangInput.article_list r) {
            Joins.Apps.Models.News10.PyeongchangOutput returnValue = null;
            if (r != null && r.article != null) {
                if (r.article.Count > 0) {
                    returnValue = new Models.News10.PyeongchangOutput();
                    returnValue.dt = DateTime.Now.ToString();
                    returnValue.data = new List<Models.News10.PyeongchangOutput.Data>();
                    foreach (Joins.Apps.Models.News10.PyeongchangInput.article1 article in r.article) {
                        returnValue.data.Add(new Models.News10.PyeongchangOutput.Data() {
                            //title = Joins.Apps.Common.Util.HTMLRemove(article.title),
                            title = article.title,
                            id = article.total_id,
                            image = article.image_mobile
                        });
                    }
                }
            }
            return returnValue;
        }

        public static Joins.Apps.Models.News10.PyeongchangOutput GetMemPyeongchangBriefCall() {
            Joins.Apps.Models.News10.PyeongchangOutput returnValue = null;
            string sResult = "";
            try { sResult = JCube.AF.Module.PageCall.LoadPage("http://static.joins.com/joongang_15re/scripts/data/pyeongchang2018/js/pyeongchang_briefing.json"); } catch { }
            if (!string.IsNullOrEmpty(sResult)) {
                try {
                    List<Joins.Apps.Models.News10.PyeongchangInput.briefinput> boData1 = JCube.AF.Serialize.JSON.JSONTo<List<Joins.Apps.Models.News10.PyeongchangInput.briefinput>>(sResult);
                    if (boData1 != null && boData1.Count > 0) {
                        returnValue = new Models.News10.PyeongchangOutput();
                        returnValue.dt = DateTime.Now.ToString();
                        returnValue.data = new List<Models.News10.PyeongchangOutput.Data>();
                        foreach (Joins.Apps.Models.News10.PyeongchangInput.briefinput article in boData1) {
                            string sImage = "";
                            int total_id = JCube.AF.Util.Converts.ToInt32((article.link.url.Replace("?","")).Replace("http://news.joins.com/article/", ""), 0);
                            Models.News10.ArticleData articleContent = Joins.Apps.Repository.News10.ArticleRepository.View(new Models.News10.ArticleParameters { totalID = total_id });
                            if (articleContent != null && !string.IsNullOrEmpty(articleContent.image)) {sImage = "http://pds.joins.com" + (articleContent.image).Replace(".tn_120.jpg", "");}
                            string sTitle = Joins.Apps.Common.Util.HTMLRemove(article.brief);
                            string TmpTitle = sTitle.Replace(" ", "");
                            byte[] sByteSpace = Encoding.Default.GetBytes(" ");
                            byte[] sByteTitle = Encoding.Default.GetBytes(sTitle);
                            if (sByteTitle.Length > 28) {
                                int iBr = Joins.Apps.Common.Util.IndexOf(sByteTitle, sByteSpace, 24);
                                if (iBr > 0) { sTitle = Encoding.Default.GetString(sByteTitle, 0, iBr) + "<br/>" + Encoding.Default.GetString(sByteTitle, iBr + 1, sByteTitle.Length - (iBr + 1)); }
                            }
                            returnValue.data.Add(new Models.News10.PyeongchangOutput.Data() {
                                title = sTitle,
                                id = total_id.ToString(),
                                image = sImage
                            });
                        }
                    }
                } catch (Exception e) { }
            }
            return returnValue;
        }
        public static Joins.Apps.Models.News10.PyeongchangOutput GetMemPyeongchangBrief() {
            Joins.Apps.Models.News10.PyeongchangOutput returnValue = null;
            object ob = Joins.Apps.Common.Util.GetCache("NEWS10CRUZPYEONGCHANGDATA2");
            DateTime memTime = JCube.AF.Util.Converts.ToDateTime(Joins.Apps.Common.Util.GetCache("NEWS10CRUZPYEONGCHANGDATA2_TIME"), DateTime.Now.AddHours(-2));
            if (memTime < DateTime.Now.AddHours(-1)) {
                Joins.Apps.Models.News10.PyeongchangOutput r = GetMemPyeongchangBriefCall();
                if (r != null && r.data!=null&&r.data.Count>0) {
                    Joins.Apps.Common.Util.SetCache("NEWS10CRUZPYEONGCHANGDATA2", r, 120);
                    Joins.Apps.Common.Util.SetCache("NEWS10CRUZPYEONGCHANGDATA2_TIME", DateTime.Now, 120);
                    returnValue = r;
                }else {
                    if (ob != null) {
                        Joins.Apps.Models.News10.PyeongchangOutput oldData = null;
                        try { oldData = (Joins.Apps.Models.News10.PyeongchangOutput)ob;} catch { }

                        if (oldData != null && oldData.data != null && oldData.data.Count > 0) {
                            Joins.Apps.Common.Util.SetCache("NEWS10CRUZPYEONGCHANGDATA2", oldData, 120);
                            Joins.Apps.Common.Util.SetCache("NEWS10CRUZPYEONGCHANGDATA2_TIME", DateTime.Now, 120);
                            returnValue = oldData;
                        }else {Joins.Apps.Common.Util.SetCache("NEWS10CRUZPYEONGCHANGDATA2_TIME", DateTime.Now, 120);}
                    }else { Joins.Apps.Common.Util.SetCache("NEWS10CRUZPYEONGCHANGDATA2_TIME", DateTime.Now, 120); }
                }
            }else {
                Joins.Apps.Models.News10.PyeongchangOutput nData = null;
                try { nData = (Joins.Apps.Models.News10.PyeongchangOutput)ob;} catch { }
                if (nData != null && nData.data != null && nData.data.Count > 0) {returnValue = nData;}
                else {//데이터가 없는 경우
                    if (memTime < DateTime.Now.AddMinutes(-5)) {//데이터가 없더라도 5분마다 호출
                        Joins.Apps.Models.News10.PyeongchangOutput r = GetMemPyeongchangBriefCall();
                        if (r != null && r.data != null && r.data.Count > 0) {
                            Joins.Apps.Common.Util.SetCache("NEWS10CRUZPYEONGCHANGDATA2", r, 120);
                            Joins.Apps.Common.Util.SetCache("NEWS10CRUZPYEONGCHANGDATA2_TIME", DateTime.Now, 120);
                            returnValue = r;
                        }else { Joins.Apps.Common.Util.SetCache("NEWS10CRUZPYEONGCHANGDATA2_TIME", DateTime.Now, 120); }
                    }
                }
            }
            return returnValue;
        }
        #endregion
        #region [JTBC VOD]

        public static Joins.Apps.Models.News10.JtbcVodInput GetMemCruzJtbcVodCall() {
            Joins.Apps.Models.News10.JtbcVodInput boData = null;
            string sListResult="", sListResult2 = "";
         
            string sSrchTime1 = DateTime.Now.AddHours(-22).ToString("yyyy-MM-dd" + "T" + "HH:mm:ss"+"Z");
            try { sListResult = JCube.AF.Module.PageCall.LoadPage(Joins.Apps.GLOBAL.News10JtbcYoutubeListAPI.Replace("||", "&") + sSrchTime1, "UTF-8", "", 5000); } catch { }
            if (!string.IsNullOrEmpty(sListResult)) {
                string videoidList = "";
                Joins.Apps.Models.News10.JtbcVodListInput DataList = null;
                try {  DataList = JCube.AF.Serialize.JSON.JSONTo<Joins.Apps.Models.News10.JtbcVodListInput>(sListResult); } catch { }
                int iVideoCnt = 0;
                if (DataList != null && DataList.items != null && DataList.items.Count > 0) {
                    foreach(Joins.Apps.Models.News10.JtbcVodListInput.items1 item in DataList.items) {
                        if (item.id != null) {if (string.IsNullOrEmpty(videoidList)) {videoidList = item.id.videoId;}else { videoidList += "," + item.id.videoId; iVideoCnt++; } }
                    }
                }
                #region [13시간 동안 동영상이 5개가 안될 경우]
                if (iVideoCnt < 5) {
                    string sSrchTime2 = DateTime.Now.AddHours(-33).ToString("yyyy-MM-dd" + "T" + "HH:mm:ss" + "Z");
                    try { sListResult2 = JCube.AF.Module.PageCall.LoadPage(Joins.Apps.GLOBAL.News10JtbcYoutubeListAPI.Replace("||", "&") + sSrchTime2, "UTF-8", "", 5000); } catch { }
                    Joins.Apps.Models.News10.JtbcVodListInput DataList2 = null;
                    try { DataList2 = JCube.AF.Serialize.JSON.JSONTo<Joins.Apps.Models.News10.JtbcVodListInput>(sListResult2); } catch { }
                    if (DataList2 != null && DataList2.items != null && DataList2.items.Count > 0) { }
                    foreach (Joins.Apps.Models.News10.JtbcVodListInput.items1 item in DataList2.items) {
                        if (item.id != null) {
                            if (string.IsNullOrEmpty(videoidList)) { videoidList = item.id.videoId; }
                            else { if (videoidList.IndexOf(item.id.videoId) <= 0) { videoidList += "," + item.id.videoId; iVideoCnt++; } }
                        }
                        if (iVideoCnt > 10) { break; }
                    }
                }
                #endregion
                try { 
                    if (!string.IsNullOrEmpty(videoidList)) {
                        DateTime nowDate = DateTime.Now;
                        string sResult = JCube.AF.Module.PageCall.LoadPage(Joins.Apps.GLOBAL.News10JtbcYoutubeVideoAPI.Replace("||","&") + videoidList);
                        Joins.Apps.Models.News10.JtbcVodInput boData1 = JCube.AF.Serialize.JSON.JSONTo<Joins.Apps.Models.News10.JtbcVodInput>(sResult);
                        if (boData1 != null && boData1.items != null && boData1.items.Count > 0) {
                            boData = new Models.News10.JtbcVodInput();
                            boData.kind = boData1.kind;
                            boData.items = new List<Models.News10.JtbcVodInput.items1>();
                            foreach (Joins.Apps.Models.News10.JtbcVodInput.items1 item in boData1.items) {
                                if (boData.items.Count >= 5) { break;}
                                if (JCube.AF.Util.Converts.ToDateTime(item.snippet.publishedAt) >= nowDate.AddHours(-12)) {
                                    boData.items.Add(item);
                                }else {

                                }
                            }
                            if (boData.items.Count < 5) {
                                foreach (Joins.Apps.Models.News10.JtbcVodInput.items1 item in boData1.items) {
                                    if (boData.items.Count >= 5) { break; }
                                    if (JCube.AF.Util.Converts.ToDateTime(item.snippet.publishedAt) < nowDate.AddHours(-12)) {
                                        boData.items.Add(item);
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) { }
            }
            return boData;
        }
        public static List<Joins.Apps.Models.News10.Cruz.HotVod> GetMemJtbcVod() {
            List<Joins.Apps.Models.News10.Cruz.HotVod> returnValue = null;
            object ob = Joins.Apps.Common.Util.GetCache("NEWS10CRUZJTBCVODDATA");
            DateTime memTime = JCube.AF.Util.Converts.ToDateTime(Joins.Apps.Common.Util.GetCache("NEWS10CRUZJTVCVODDATA_TIME"), DateTime.Now.AddHours(-2));
            if (memTime < DateTime.Now.AddHours(-1)){// 한시간이 넘었을 경우
                Joins.Apps.Models.News10.JtbcVodInput r = GetMemCruzJtbcVodCall();
                if(r != null && r.items != null&& r.items.Count>0){//데이터가 있는 경우
                    Joins.Apps.Common.Util.SetCache("NEWS10CRUZJTBCVODDATA", r, 120);
                    Joins.Apps.Common.Util.SetCache("NEWS10CRUZJTVCVODDATA_TIME", DateTime.Now, 120);
                    returnValue = GetCruzJtbcVodOutput(r);
                }else {//데이터가 없는 경우
                    if (ob != null ) {//기존 데이터를 다시 넣는다
                        Joins.Apps.Models.News10.JtbcVodInput oldData = null;
                        try { oldData = (Joins.Apps.Models.News10.JtbcVodInput)ob; } catch  { }
                        if (oldData!=null&& oldData.items != null && oldData.items.Count > 0) {
                            Joins.Apps.Common.Util.SetCache("NEWS10CRUZJTBCVODDATA", ob, 120);
                            Joins.Apps.Common.Util.SetCache("NEWS10CRUZJTVCVODDATA_TIME", DateTime.Now, 120);
                            returnValue = GetCruzJtbcVodOutput((Joins.Apps.Models.News10.JtbcVodInput)ob);
                        }else {Joins.Apps.Common.Util.SetCache("NEWS10CRUZJTVCVODDATA_TIME", DateTime.Now, 120);}//기존 데이터도 없는 경우
                    }else {Joins.Apps.Common.Util.SetCache("NEWS10CRUZJTVCVODDATA_TIME", DateTime.Now, 120);}//기존 데이터도 없는 경우
                }
            }else {//한시간 이내인 경우
                Joins.Apps.Models.News10.JtbcVodInput nData = null;
                try { nData = (Joins.Apps.Models.News10.JtbcVodInput)ob; } catch { }
                if (nData != null && nData.items != null && nData.items.Count > 0) {
                    returnValue = GetCruzJtbcVodOutput((Joins.Apps.Models.News10.JtbcVodInput)ob);
                }else{//데이터가 없는 경우
                    if (memTime < DateTime.Now.AddMinutes(-5)) {//데이터가 없더라도 5분마다 호출
                        Joins.Apps.Models.News10.JtbcVodInput r = GetMemCruzJtbcVodCall();
                        if (r != null && r.items != null && r.items.Count > 0) {
                            Joins.Apps.Common.Util.SetCache("NEWS10CRUZJTBCVODDATA", r, 120);
                            Joins.Apps.Common.Util.SetCache("NEWS10CRUZJTVCVODDATA_TIME", DateTime.Now, 120);
                            returnValue = GetCruzJtbcVodOutput(r);
                        }else { Joins.Apps.Common.Util.SetCache("NEWS10CRUZJTVCVODDATA_TIME", DateTime.Now, 120); }
                    }
                }
            }
            return returnValue;
        }
        private static List<Joins.Apps.Models.News10.Cruz.HotVod> GetCruzJtbcVodOutput(Joins.Apps.Models.News10.JtbcVodInput r) {
            List<Joins.Apps.Models.News10.Cruz.HotVod> returnValue = null;
            if (r != null && r.items != null) {
                if (r.items.Count > 0) {
                    returnValue = new List<Models.News10.Cruz.HotVod>();
                    foreach (Joins.Apps.Models.News10.JtbcVodInput.items1 item in r.items) {
                        string sTitle = "",sDate="",sImage="";
                        List<Models.News10.Cruz.Art_imgs> art_img = new List<Models.News10.Cruz.Art_imgs>();
                        List<Models.News10.Cruz.Art_movs> art_movs = new List<Models.News10.Cruz.Art_movs>();
                        List<Joins.Apps.Models.News10.Cruz.Summaries> summaries = new List<Models.News10.Cruz.Summaries>();
                        if (item.snippet != null) {
                            sTitle=item.snippet.title;
                            sDate = JCube.AF.Util.Converts.ToDateTime(item.snippet.publishedAt).ToString("yyyy-MM-dd HH:mm:ss");
                            sImage = item.snippet.thumbnails.medium.url;
                            art_img.Add(new Models.News10.Cruz.Art_imgs(){
                                img_url = item.snippet.thumbnails.medium.url,
                                h = item.snippet.thumbnails.medium.height,
                                w = item.snippet.thumbnails.medium.width
                              }
                            );
                            art_movs.Add(new Models.News10.Cruz.Art_movs() {
                                movie_type="OL",
                                movie_url ="https://youtu.be/"+item.id
                               // movie_url = item.id
                            });
                        }
                       
                        returnValue.Add(new Models.News10.Cruz.HotVod() {
                            title = Joins.Apps.Common.Util.HTMLRemove(sTitle),
                            date = sDate,
                            image = sImage,
                            art_movs= art_movs,
                            art_imgs = art_img,
                            summary = "",
                            summaries = summaries
                        });
                    }
                }
            }
            return returnValue;
        }
        #endregion
        #region [미리보는 오늘]
        public static List<Joins.Apps.Models.News10.Cruz.News10> GetMemPreviewTodayData() {
            List < Joins.Apps.Models.News10.Cruz.News10> returnValue = new List<Models.News10.Cruz.News10>();
            var cruz1 = Joins.Apps.Common.Util.GetCache("PREVIEWTODAYDATA");
            DateTime memTime = JCube.AF.Util.Converts.ToDateTime(Joins.Apps.Common.Util.GetCache("PREVIEWTODAYDATA_TIME"), DateTime.Now.AddMinutes(-20));
            List<Joins.Apps.Models.News10.Cruz.News10> cc = null;
            try { cc = (List<Joins.Apps.Models.News10.Cruz.News10>)cruz1; } catch { }
            if (cc != null &&cc.Count>0 && !string.IsNullOrEmpty(cc[0].id)) {returnValue =(cc);}
            else {
                if (memTime < DateTime.Now.AddMinutes(-10)) {//데이터가 없더라도 1분마다 호출
                    List<Joins.Apps.Models.News10.Cruz.News10> previewData = Joins.Apps.Repository.News10.CardRepository.GetPreviewTodayData();
                    if (previewData != null) {Joins.Apps.Common.Util.SetCache("PREVIEWTODAYDATA", previewData, 10);}
                    Joins.Apps.Common.Util.SetCache("PREVIEWTODAYDATA_TIME", DateTime.Now, 10);
                    returnValue = previewData;
                }/*else {Joins.Apps.Common.Util.SetCache("PREVIEWTODAYDATA_TIME", DateTime.Now, 10);}*/
            }
            return returnValue;
        }
        #endregion
        #region [1분뉴스]
        public static Joins.Apps.Models.News10.Cruz.HotVod GetMemJTBC1MinData() {
            Joins.Apps.Models.News10.Cruz.HotVod returnValue = new Models.News10.Cruz.HotVod();
            var cruz1 = Joins.Apps.Common.Util.GetCache("JTBC1MINDATA");
            DateTime memTime = JCube.AF.Util.Converts.ToDateTime(Joins.Apps.Common.Util.GetCache("JTBC1MINDATA_TIME"), DateTime.Now.AddMinutes(-20));
            Joins.Apps.Models.News10.Cruz.HotVod cc = null;
            try { cc = (Joins.Apps.Models.News10.Cruz.HotVod)cruz1; } catch { }
            if (cc != null  && !string.IsNullOrEmpty(cc.title)) { returnValue = (cc); }
            else {

                if (memTime < DateTime.Now.AddMinutes(-10) ) {//데이터가 없더라도 10분마다 호출
                    string sDT = DateTime.Now.ToString("yyyyMMdd");
                    if (DateTime.Now.Hour < 2) { sDT = DateTime.Now.AddDays(-1).ToString("yyyyMMdd"); }
                    Joins.Apps.Models.News10.Cruz.HotVod vodData = Joins.Apps.Repository.News10.CruzRepository.SetArticleListData(1,1, "JTBC1MIN", "1분뉴스", sDT, DateTime.Now.ToString("yyyyMMdd"),"d9");
                    //  Joins.Apps.Models.News10.Cruz.HotVod previewData = Joins.Apps.Repository.News10.CruzRepository.SetArticleListData(1, 1, "", "1분뉴스", "20180301", "20180301", "d9");
                    if (vodData != null) {Joins.Apps.Common.Util.SetCache("JTBC1MINDATA", vodData, 10);}
                    Joins.Apps.Common.Util.SetCache("JTBC1MINDATA_TIME", DateTime.Now, 10);
                    returnValue = vodData;
                }/*else { Joins.Apps.Common.Util.SetCache("JTBC1MINDATA_TIME", DateTime.Now, 10); }*/
            }
            return returnValue;
        }
        #endregion

        #region [핫 연재(시리즈)]

        public static List<Joins.Apps.Models.News10.Cruz3.Article_List> GetMemHotSeriesData() {
            List<Joins.Apps.Models.News10.Cruz3.Article_List> returnValue = new List<Models.News10.Cruz3.Article_List>();
            var cruz1 = Joins.Apps.Common.Util.GetCache("HOTSERIESDATA");
            DateTime memTime = JCube.AF.Util.Converts.ToDateTime(Joins.Apps.Common.Util.GetCache("HOTSERIESDATA_TIME"), DateTime.Now.AddMinutes(-2));
            List<Joins.Apps.Models.News10.Cruz3.Article_List> cc = null;
            try { cc = (List<Joins.Apps.Models.News10.Cruz3.Article_List>)cruz1; } catch { }
            if (cc != null && cc.Count > 0 && !string.IsNullOrEmpty(cc[0].id)) { returnValue = (cc); }
            else {
                if (memTime < DateTime.Now.AddMinutes(-1)) {//데이터가 없더라도 1분마다 호출
                    List<Joins.Apps.Models.News10.Cruz3.Article_List> hotData = Joins.Apps.Repository.News10.CardRepository.GetHotSeriesData();
                    if (hotData != null) { Joins.Apps.Common.Util.SetCache("HOTSERIESDATA", hotData, 1); }
                    Joins.Apps.Common.Util.SetCache("HOTSERIESDATA_TIME", DateTime.Now, 1);
                    returnValue = hotData;
                }
               /* else { Joins.Apps.Common.Util.SetCache("HOTSERIESDATA_TIME", DateTime.Now, 1); }*/
            }
            return returnValue;
        }
        #endregion
        #region [이슈패키지]
        private static List<Joins.Apps.Models.News10.Cruz3.issue> GetMemIssuePackCall() {
            List<Joins.Apps.Models.News10.Cruz3.issue> r = null;
            List<Joins.Apps.Models.News10.IssuePackList> boData = null;
            string sResult = "";
            try { sResult = JCube.AF.Module.PageCall.LoadPage("http://static.joins.com/joongang_15re/scripts/data/issue/js/tagnews_article_list_news10.json"); } catch { }
            if (!string.IsNullOrEmpty(sResult)) {
                try {
                    boData = JCube.AF.Serialize.JSON.JSONTo<List<Joins.Apps.Models.News10.IssuePackList>>(sResult);
                } catch (Exception e) { }
                if (boData != null && boData.Count > 0) {
                    r = new List<Joins.Apps.Models.News10.Cruz3.issue>();
                    // boData = boData.OrderBy(a => Guid.NewGuid()).ToList();
                    DateTime LastIssueDt = DateTime.Now.AddMonths(-1);
                    bool bLastIssue = false;
                    foreach (Joins.Apps.Models.News10.IssuePackList item in boData) {
                        Joins.Apps.Models.News10.Cruz3.issue iss = new Joins.Apps.Models.News10.Cruz3.issue();
                        iss.iss_seq = item.iss_seq;
                        iss.iss_name = item.edit_iss_title;
                        iss.iss_img = item.edit_iss_img;
                        if (item.article_list != null && item.article_list.Count > 0) {
                            iss.article_cnt = item.article_list.Count;
                            List < Joins.Apps.Models.News10.Cruz3.Article_List > iss_art_list = new List<Joins.Apps.Models.News10.Cruz3.Article_List>();
                            foreach (Joins.Apps.Models.News10.IssuePackList.article_list1 art_item in item.article_list) {
                                List<Joins.Apps.Models.News10.Cruz.Art_imgs> artimgList = new List<Models.News10.Cruz.Art_imgs>();

                                string date1 = "", image1 = "";
                                DateTime dt = DateTime.Now.AddMonths(-2);
                                try {
                                    dt = DateTime.ParseExact(art_item.service_day + art_item.serivce_time, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                                    date1 = dt.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                                } catch { }
                                if (dt > LastIssueDt) {LastIssueDt = dt; bLastIssue = true; }
                                artimgList.Add(new Models.News10.Cruz.Art_imgs { h=0,w=0,num=0,img_url=art_item.thumbnail.Replace(".tn_250.jpg", "")});
                                List<Joins.Apps.Models.News10.Cruz.Summaries> summaries =new List<Joins.Apps.Models.News10.Cruz.Summaries>();
                                summaries.Add(new Joins.Apps.Models.News10.Cruz.Summaries {  ord=1,summary=art_item.summary});
                                string sTitle = Joins.Apps.Common.Util.DisplayText(art_item.title).Trim();                               
                                if (artimgList != null && artimgList.Count > 0) { image1 = artimgList[0].img_url; }
                                Joins.Apps.Models.News10.Cruz3.Article_List a_item = new Joins.Apps.Models.News10.Cruz3.Article_List();
                                a_item.art_imgs = artimgList;
                                a_item.date = date1;
                                a_item.id = art_item.total_id;
                                a_item.image = image1;
                                a_item.summaries = summaries;
                                a_item.summary = art_item.summary;
                                a_item.title = sTitle;
                                iss_art_list.Add(a_item); 
                            }
                            iss.article_list = iss_art_list;
                        }
                        if (bLastIssue) { r = new List<Joins.Apps.Models.News10.Cruz3.issue>(); r.Add(iss); bLastIssue = false; }
                       // if (r.Count > 0) { break; }
                    }
                }
            }
            return r;
        }
        public static List<Joins.Apps.Models.News10.Cruz3.issue> GetMemIssuePackData() {
            List<Joins.Apps.Models.News10.Cruz3.issue> returnValue = new List<Models.News10.Cruz3.issue>();
            var cruz1 = Joins.Apps.Common.Util.GetCache("ISSUEPACKDATA");
            DateTime memTime = JCube.AF.Util.Converts.ToDateTime(Joins.Apps.Common.Util.GetCache("ISSUEPACKDATA_TIME"), DateTime.Now.AddMinutes(-20));
            List<Joins.Apps.Models.News10.Cruz3.issue> cc = null;
            try { cc = (List<Joins.Apps.Models.News10.Cruz3.issue>)cruz1; } catch { }
            if (cc != null && cc.Count > 0 && !string.IsNullOrEmpty(cc[0].iss_name)) { returnValue = (cc); }
            else {
                if (memTime < DateTime.Now.AddMinutes(-10)) {//데이터가 없더라도 1시간마다 호출
                    List<Joins.Apps.Models.News10.Cruz3.issue> hotData = Joins.Apps.Core.MemCache.GetMemIssuePackCall();
                    if (hotData != null) { Joins.Apps.Common.Util.SetCache("ISSUEPACKDATA", hotData, 10); }
                    Joins.Apps.Common.Util.SetCache("ISSUEPACKDATA_TIME", DateTime.Now, 10);
                    returnValue = hotData;
                }/*else { Joins.Apps.Common.Util.SetCache("ISSUEPACKDATA_TIME", DateTime.Now, 10); }*/
            }
            return returnValue;
        }
        #endregion

        #region [Weekly 7]

        public static List<Joins.Apps.Models.News10.Cruz3.Article_List> GetMemWeekly7Data() {
            List<Joins.Apps.Models.News10.Cruz3.Article_List> returnValue = new List<Models.News10.Cruz3.Article_List>();
            var cruz1 = Joins.Apps.Common.Util.GetCache("WEEKLYDATA");
            DateTime memTime = JCube.AF.Util.Converts.ToDateTime(Joins.Apps.Common.Util.GetCache("WEEKLY7DATA_TIME"), DateTime.Now.AddHours(-2));
            List<Joins.Apps.Models.News10.Cruz3.Article_List> cc = null;
            try { cc = (List<Joins.Apps.Models.News10.Cruz3.Article_List>)cruz1; } catch { }
            if (cc != null && cc.Count > 0 && !string.IsNullOrEmpty(cc[0].id)) { returnValue = (cc); }
            else {
                if (memTime < DateTime.Now.AddHours(-1)) {//데이터가 없더라도 1시간마다 호출
                    List<Joins.Apps.Models.News10.Cruz3.Article_List> hotData = Joins.Apps.Repository.News10.CardRepository.GetWeekly7Data();
                    if (hotData != null) { Joins.Apps.Common.Util.SetCache("WEEKLYDATA", hotData, 60); }
                    Joins.Apps.Common.Util.SetCache("WEEKLY7DATA_TIME", DateTime.Now, 60);
                    returnValue = hotData;
                }
                /*else { Joins.Apps.Common.Util.SetCache("WEEKLY7DATA_TIME", DateTime.Now, 60); }*/
            }
            return returnValue;
        }
        #endregion

        #region [미세먼지]
        private static Joins.Apps.Models.News10.AirKoreaOutput GetMemAirinfoCall(string area) {
            Joins.Apps.Models.News10.AirKoreaOutput r = null;
            Joins.Apps.Models.News10.AirKoreaInput localAirData = null;
            Joins.Apps.Models.News10.AirKoreaInput ForecastAirData = null;
            string sAreaResult = "",sForecastResult="";
            try { sAreaResult = JCube.AF.Module.PageCall.LoadPage(Joins.Apps.GLOBAL.News10AirKoreaAreaAPI.Replace("@@AREA@@", System.Web.HttpUtility.UrlEncode(area)));} catch { }//지역 미세먼지 정보

            if (!string.IsNullOrEmpty(sAreaResult)) {
                try {
                    localAirData = JCube.AF.Serialize.JSON.JSONTo<Joins.Apps.Models.News10.AirKoreaInput>(sAreaResult);
                } catch (Exception e) { }
                if (localAirData != null && localAirData.list != null && localAirData.list.Count > 0) {
                    r = new Joins.Apps.Models.News10.AirKoreaOutput();
                    foreach (Joins.Apps.Models.News10.AirKoreaInput.list1 item in localAirData.list) {
                        r.admin = area;
                        r.so2value = item.so2Value;
                        r.covalue = item.coValue;
                        r.o3value = item.o3Value;
                        r.no2value = item.no2Value;
                        r.pm10value = item.pm10Value;
                        r.pm25value = item.pm25Value;
                        r.khaivalue = item.khaiValue;
                        r.khaigrade = JCube.AF.Util.Converts.ToInt32(item.khaiGrade, 0);
                        r.so2grade = JCube.AF.Util.Converts.ToInt32(item.so2Grade, 0);
                        r.cograde = JCube.AF.Util.Converts.ToInt32(item.coGrade, 0);
                        r.o3grade = JCube.AF.Util.Converts.ToInt32(item.o3Grade, 0);
                        r.no2grade = JCube.AF.Util.Converts.ToInt32(item.no2Grade, 0);
                        r.pm10grade = JCube.AF.Util.Converts.ToInt32(item.pm10Grade1h, 0);
                        r.pm25grade = JCube.AF.Util.Converts.ToInt32(item.pm25Grade1h, 0);
                        if (r != null && !string.IsNullOrEmpty(r.khaivalue)&& !r.khaivalue.Equals("-")) { break; }
                    }
                    if ( string.IsNullOrEmpty(r.khaivalue) || r.khaivalue.Equals("-")) { r.khaivalue = "-"; }
                }

                #region [미세먼지 내일 예보]
                string sSchDay = DateTime.Now.ToString("yyyy-MM-dd");
                try { sForecastResult = JCube.AF.Module.PageCall.LoadPage(Joins.Apps.GLOBAL.News10AirKoreaForecastAPI.Replace("@@DAY@@", sSchDay));
                } catch { }//미세먼지예보 정보
                if (!string.IsNullOrEmpty(sForecastResult)) {
                    try {
                        ForecastAirData = JCube.AF.Serialize.JSON.JSONTo<Joins.Apps.Models.News10.AirKoreaInput>(sForecastResult);
                    } catch (Exception e) { }
                    if (r != null && ForecastAirData != null && ForecastAirData.list != null && ForecastAirData.list.Count > 0) {
                        foreach (Joins.Apps.Models.News10.AirKoreaInput.list1 item in ForecastAirData.list) {
                            Joins.Apps.Models.News10.AirKoreaOutput iss = new Joins.Apps.Models.News10.AirKoreaOutput();
                            if (item.informCode.Equals("PM10") && item.informData.Equals(DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"))) {
                                if (string.IsNullOrEmpty(r.informoverall)) { r.informoverall = (item.informOverall).Replace("○","").Replace("[미세먼지]", "").Trim(); }// 미세먼지 총평을 사용
                                if (r.informpm10Grade <= 0) {
                                    r.informpm10Grade = AirInfoAreaSplit(item.informGrade,area);
                                }
                            }
                            if (item.informCode.Equals("PM25") && item.informData.Equals(DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"))) {
                                if (r.informpm25Grade <= 0) { r.informpm25Grade = AirInfoAreaSplit(item.informGrade, area); }
                            }
                        }
                    }
                }
                #endregion
            }
            return r;
        }
        public static Joins.Apps.Models.News10.AirKoreaOutput GetMemAirinfoData(string area) {
            var areaList = new Dictionary<string, string>(){
                { "서울", "서울" },
                { "부산", "부산" },
                { "대구", "대구" },
                { "인천", "인천" },
                { "광주", "광주" },
                { "대전", "대전" },
                { "울산", "울산" },
                { "세종", "세종" },
                { "경기", "경기" },
                { "강원", "강원" },
                { "충북", "충북" },
                { "충남", "충남" },
                { "전북", "전북" },
                { "전남", "전남" },
                { "경북", "경북" },
                { "경남", "경남" },
                { "충청북도", "충북" },
                { "충청남도", "충남" },
                { "전라북도", "전북" },
                { "전라남도", "전남" },
                { "경상북도", "경북" },
                { "경상남도", "경남" },
                { "제주", "제주" }
            };

            foreach (KeyValuePair<string, string> area1 in areaList) {
                if (area.IndexOf(area1.Key) > -1) {
                    area = area1.Value;
                    break;
                }
            }

            Joins.Apps.Models.News10.AirKoreaOutput returnValue =new Models.News10.AirKoreaOutput();
            string sAreaKey = "SEOUL";
            if (!string.IsNullOrEmpty(area)) {sAreaKey = JCube.AF.Module.Security.GetMD5Encript(area);}
            var cruz1 = Joins.Apps.Common.Util.GetCache("AIRINFODATA_" + sAreaKey);
            DateTime memTime = JCube.AF.Util.Converts.ToDateTime(Joins.Apps.Common.Util.GetCache("AIRINFODATA_"+ sAreaKey + "_TIME"), DateTime.Now.AddHours(-1));
            Joins.Apps.Models.News10.AirKoreaOutput cc = null;
            try { cc = (Joins.Apps.Models.News10.AirKoreaOutput)cruz1; } catch { }
            if (cc != null && cc.khaigrade>0 ) { returnValue = (cc); }
            else {
                if (memTime < DateTime.Now.AddMinutes(-10)) {//데이터가 없더라도 10분마다 호출
                    Joins.Apps.Models.News10.AirKoreaOutput airData = Joins.Apps.Core.MemCache.GetMemAirinfoCall(area);
                    if (airData != null) { Joins.Apps.Common.Util.SetCache("AIRINFODATA_" + sAreaKey, airData, 30); }
                    Joins.Apps.Common.Util.SetCache("AIRINFODATA_" + sAreaKey + "_TIME", DateTime.Now, 30);
                    returnValue = airData;
                }
            }
            return returnValue;
        }
        #endregion

        #region [여성지 6종]
        private static List<Joins.Apps.Models.News10.Cruz3.MnBSpecial> GetMemMnBSpecialCall() {
            List<Joins.Apps.Models.News10.Cruz3.MnBSpecial> r = null;
            Joins.Apps.Models.News10.MnBSpecialInput boData = null;
            string sResult = "";
            try { sResult = JCube.AF.Module.PageCall.LoadPage("http://mnb.joins.com/service/Content/News10"); } catch { }
            if (!string.IsNullOrEmpty(sResult)) {
                try {
                    boData = JCube.AF.Serialize.JSON.JSONTo<Joins.Apps.Models.News10.MnBSpecialInput>(sResult);
                } catch (Exception e) { }
                if (boData != null && boData.response_code.Equals("1000") ) {
                    r = new List<Joins.Apps.Models.News10.Cruz3.MnBSpecial>();
                    boData.response_data = (boData.response_data).OrderBy(a => Guid.NewGuid()).ToList();
                    foreach (Joins.Apps.Models.News10.MnBSpecialInput.response_data1 item in boData.response_data) {
                        List<Joins.Apps.Models.News10.Cruz.Art_imgs> artimgList = new List<Models.News10.Cruz.Art_imgs>();
                        string image1 = "";
                        artimgList.Add(new Models.News10.Cruz.Art_imgs { h = item.article_cover_image_height, w = item.article_cover_image_width, num = 1, img_url = item.article_cover_image_url });
                        List<Joins.Apps.Models.News10.Cruz.Summaries> summaries = new List<Joins.Apps.Models.News10.Cruz.Summaries>();
                        summaries.Add(new Joins.Apps.Models.News10.Cruz.Summaries { ord = 1, summary = item.article_summary });
                        string sTitle = Joins.Apps.Common.Util.DisplayText(item.article_title).Trim();
                        if (artimgList != null && artimgList.Count > 0) { image1 = artimgList[0].img_url; }
                        Joins.Apps.Models.News10.Cruz3.MnBSpecial a_item = new Joins.Apps.Models.News10.Cruz3.MnBSpecial();
                        a_item.art_imgs = artimgList;
                        a_item.date = item.publish_date;
                        a_item.media = item.media_name;
                        a_item.category = item.category_name;
                        a_item.url = item.article_link;
                        a_item.image = image1;
                        a_item.summaries = summaries;
                        a_item.summary = item.article_summary;
                        a_item.title = sTitle;
                        r.Add(a_item);
                        if (r.Count >= 6) { break; }
                    } 
                }
            }
            return r;
        }
        public static List<Joins.Apps.Models.News10.Cruz3.MnBSpecial> GetMemMnBSpecialData() {
            List<Joins.Apps.Models.News10.Cruz3.MnBSpecial> returnValue = new List<Models.News10.Cruz3.MnBSpecial>();
            var cruz1 = Joins.Apps.Common.Util.GetCache("MNBSPECIALDATA");
            DateTime memTime = JCube.AF.Util.Converts.ToDateTime(Joins.Apps.Common.Util.GetCache("MNBSPECIALDATA_TIME"), DateTime.Now.AddMinutes(-20));
            List<Joins.Apps.Models.News10.Cruz3.MnBSpecial> cc = null;
            try { cc = (List<Joins.Apps.Models.News10.Cruz3.MnBSpecial>)cruz1; } catch { }
            if (cc != null && cc.Count > 0 && !string.IsNullOrEmpty(cc[0].title)) { returnValue = (cc); }
            else {
                if (memTime < DateTime.Now.AddMinutes(-10)) {//데이터가 없더라도 1시간마다 호출
                    List<Joins.Apps.Models.News10.Cruz3.MnBSpecial> hotData = Joins.Apps.Core.MemCache.GetMemMnBSpecialCall();
                    if (hotData != null) { Joins.Apps.Common.Util.SetCache("MNBSPECIALDATA", hotData, 10); }
                    Joins.Apps.Common.Util.SetCache("MNBSPECIALDATA_TIME", DateTime.Now, 10);
                    returnValue = hotData;
                }
            }
            return returnValue;
        }

        #endregion

        #region [썰리]

        public static List<Joins.Apps.Models.Sully.APIProduct> GetMemSSullyData() {
            List<Joins.Apps.Models.Sully.APIProduct> returnValue = new List<Joins.Apps.Models.Sully.APIProduct>();
            var cruz1 = Joins.Apps.Common.Util.GetCache("SSULLYDATA");
            DateTime memTime = JCube.AF.Util.Converts.ToDateTime(Joins.Apps.Common.Util.GetCache("SSULLYDATA_TIME"), DateTime.Now.AddMinutes(-2));
            List<Joins.Apps.Models.Sully.APIProduct> cc = null;
            try { cc = (List<Joins.Apps.Models.Sully.APIProduct>)cruz1; } catch { }
            if (cc != null && cc.Count > 0 && !string.IsNullOrEmpty(cc[0].ptitle)) { returnValue = (cc); }
            else {
                if (memTime < DateTime.Now.AddMinutes(-1)) {//데이터가 없더라도 1분마다 호출
                    Joins.Apps.Models.News10.sullyReturn SsullyData = null;
                    try {
                        string sSSullyData = JCube.AF.Module.PageCall.LoadPage("https://apps-api.joins.com/sully/Main/List");
                        SsullyData = JCube.AF.Serialize.JSON.JSONTo<Joins.Apps.Models.News10.sullyReturn>(sSSullyData);
                    } catch { }

                    if (SsullyData != null && SsullyData.sully != null && SsullyData.sully.Count > 0 && !string.IsNullOrEmpty(SsullyData.sully[0].ptitle)) {
                        Joins.Apps.Common.Util.SetCache("SSULLYDATA", SsullyData.sully, 1);
                        returnValue = SsullyData.sully;
                    }
                    Joins.Apps.Common.Util.SetCache("SSULLYDATA_TIME", DateTime.Now, 1);
                    returnValue = null;
                }
            }
            return returnValue;
        }
        #endregion

        public static int AirInfoAreaSplit(string org, string area) {
            int r = 0;
            if (area.Equals("경기")) { area = "경기남부"; }
            if (area.Equals("강원")) { area = "영서"; }
            string[] areaList = org.Split(",".ToArray());
            if (areaList.Length > 0) {
                foreach (string areaData in areaList) {
                    string[] areaValue = areaData.Split(":".ToArray());
                    if (areaValue.Length > 1 && areaValue[0].Trim() == area) {
                        switch (areaValue[1].Trim()) {
                            case "좋음":
                                    r = 1;
                                break;
                            case "보통":
                                    r = 2;
                                break;
                            case "나쁨":
                                r = 3;
                                break;
                            case "매우나쁨":
                                r = 4;
                                break;
                            case "매우 나쁨":
                                r = 4;
                                break;
                            default:
                                r = 0;
                                break;
                        }
                    }
                }
            }
            return r;
        }
    }
}
