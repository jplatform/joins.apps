﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.News10 {
    public enum BRIEF_TYPE {
        /// <summary>
        /// 하루 종일
        /// </summary>
        all = 0,
        /// <summary>
        /// 아침 브리핑
        /// </summary>
        mornign =1,
        /// <summary>
        /// 오후 브리핑
        /// </summary>
        afternoon = 2,
        /// <summary>
        /// 저녁 브리핑
        /// </summary>
        evening = 3,
        /// <summary>
        /// 밤 브리핑
        /// </summary>
        night = 4,
       
    }
    public class BRIEF_TYPEUtil : JCube.AF.Util.EnumUtils<BRIEF_TYPE> {
        public static string[] ECNames = { "하루 종일", "아침 브리핑", "오후 브리핑", "저녁 브리핑", "밤 브리핑" };
        public static string GetECName(object p) { try { return ECNames[(int)BRIEF_TYPEUtil.GetTypeEnum(p.ToString())]; } catch { return ECNames[0]; } }
        public static string GetECName(int i) { try { return ECNames[i]; } catch { return ECNames[0]; } }
        public static System.Web.UI.WebControls.ListItem[] GetECListItems(bool bAll = false) {
            FieldInfo[] myEnumFields = typeof(BRIEF_TYPE).GetFields();
            int len = 0, tmpi = 0;
            if (bAll) { len = myEnumFields.Length; tmpi = 1; }
            else { len = myEnumFields.Length - 1; tmpi = 0; }
            System.Web.UI.WebControls.ListItem[] litms = new System.Web.UI.WebControls.ListItem[len];

            if (bAll) { litms[0] = new System.Web.UI.WebControls.ListItem("-전체-", ""); }
            foreach (FieldInfo myField in myEnumFields) {
                if (!myField.IsSpecialName && myField.Name.ToLower() != "notset") {
                    int myValue = (int)myField.GetValue(0);
                    litms[tmpi] = new System.Web.UI.WebControls.ListItem(GetECName(myValue), myField.Name);
                    tmpi++;
                }
            }
            return litms;
        }
    }
    public enum WEATHER_TYPE {
        /// <summary>
        /// 보통
        /// </summary>
        N = 0,
        /// <summary>
        /// 비
        /// </summary>
        R = 1,
        /// <summary>
        /// 눈
        /// </summary>
        S = 2,
    }
    public class WEATHER_TYPEUtil : JCube.AF.Util.EnumUtils<WEATHER_TYPE> {
        public static string[] ECNames = { "기본", "비", "눈" };
        public static string GetECName(object p) { try { return ECNames[(int)BRIEF_TYPEUtil.GetTypeEnum(p.ToString())]; } catch { return ECNames[0]; } }
        public static string GetECName(int i) { try { return ECNames[i]; } catch { return ECNames[0]; } }
        public static System.Web.UI.WebControls.ListItem[] GetECListItems(bool bAll = false) {
            FieldInfo[] myEnumFields = typeof(WEATHER_TYPE).GetFields();
            int len = 0, tmpi = 0;
            if (bAll) { len = myEnumFields.Length; tmpi = 1; }
            else { len = myEnumFields.Length - 1; tmpi = 0; }
            System.Web.UI.WebControls.ListItem[] litms = new System.Web.UI.WebControls.ListItem[len];

            if (bAll) { litms[0] = new System.Web.UI.WebControls.ListItem("-전체-", ""); }
            foreach (FieldInfo myField in myEnumFields) {
                if (!myField.IsSpecialName && myField.Name.ToLower() != "notset") {
                    int myValue = (int)myField.GetValue(0);
                    litms[tmpi] = new System.Web.UI.WebControls.ListItem(GetECName(myValue), myField.Name);
                    tmpi++;
                }
            }
            return litms;
        }
    }
    public class ShowCase {
        public int ROWNUM { get; set; }
        public int SEQ { get;set;}
        public string BRIEF_TYPE { get; set; }
        public string USED_YN { get; set; }
        public string BASIC_YN { get; set; }
        public string WEATHER_TYPE { get; set; }
        public string SPECIAL_DAY { get; set; }
        public string IMG { get; set; }
        public string REG_ID { get; set; }
        public string REG_DT { get; set; }
        public string MOD_ID { get; set; }
        public string MOD_DT { get; set; }
    }
}
