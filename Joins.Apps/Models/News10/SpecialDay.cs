﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.News10 {
    public class SpecialDayData {
        public string day { get; set; }
        public string special_type { get; set; }
        //public string service { get; set; }
    }

    public class SpecialDayParameters {
        public string day { get; set; }
        public string service { get; set; }
    }
}
