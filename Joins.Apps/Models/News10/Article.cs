﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.Models.News10 {
    public class ArticleData {
        public string id { get; set; }
        public string title { get; set; }
        public string summary { get; set; }
        public string content { get; set; }
        public string image { get; set; }
        public string writer { get; set; }
        public string date { get; set; }
    }
    
    public class ArticleParameters {
        public int totalID { get; set; }
    }
}