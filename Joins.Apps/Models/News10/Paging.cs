﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.News10 {
    public class PagingParameters {
        public PagingParameters() {
            pageNum = 1;
            pageSize = 10;
        }

        public int pageNum { get; set; }
        public int pageSize { get; set; }
    }
}
