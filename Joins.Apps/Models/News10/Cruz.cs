﻿using Newtonsoft.Json;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using Joins.Apps.Common.JsonConverter;

namespace Joins.Apps.Models.News10 {

    #region [브리핑 cruz 데이터 출력]
    public class Cruz {
        [JsonProperty(PropertyName = "HotKwd")]
        public List<HotKwd> hotkwd { get; set; }
        [JsonProperty(PropertyName = "HotVod")]
        public List<HotVod> hotvod { get; set; }
        [JsonProperty(PropertyName = "News10")]
        public List<News10> news10 { get; set; }
        [JsonProperty(PropertyName = "Special")]
        public List<Special> special { get; set; }
        public class HotKwd {
            public List<Art_imgs> art_imgs { get; set; }
            public List<Art_movs> art_movs { get; set; }
            public string content { get; set; }
            public string date { get; set; }
            public string id { get; set; }
            string _image;
            public string image { get { return _image; } set { _image = string.IsNullOrEmpty(value) ? "" : "http://pds.joins.com" + value; } }
            public string kwd { get; set; }
            public List<Summaries> summaries { get; set; }
            public string summary { get; set; }
            string _title;
            public string title { get { return _title; } set { _title = Joins.Apps.Common.Util.GetRexValue(value); } }
        }
        public class HotVod {
            public List<Art_imgs> art_imgs { get; set; }
            public List<Art_movs> art_movs { get; set; }
            public string content { get; set; }
            public string date { get; set; }
            public string id { get; set; }
            string _image;
           // public string image { get { return _image; } set { _image = string.IsNullOrEmpty(value) ? "" : "http://pds.joins.com" + value; } }
            public string image { get;set; } 
            public List<Summaries> summaries { get; set; }
            public string summary { get; set; }
            string _title;
            public string title { get { return _title; } set { _title = Joins.Apps.Common.Util.GetRexValue(value); } }
        }
        public class News10 {
            public List<Art_imgs> art_imgs { get; set; }
          //  public string art_movs { get; set; }
            public string content { get; set; }
            public string date { get; set; }
            public string id { get; set; }
            string _image;
            public string image { get { return _image; } set {if (string.IsNullOrEmpty(value)) {_image = "";}else {if (value.IndexOf("http://pds.joins.com") >=0) {_image = value;}else {_image = "http://pds.joins.com" + value;}}}
            }
            public List<Summaries> summaries { get; set; }
            public string summary { get; set; }
            string _title;
            public string title { get { return _title; } set { _title= Joins.Apps.Common.Util.GetRexValue(value); } }
            public TTS tts {get {
                    string title1 = Joins.Apps.Common.Util.HTMLRemove(this.title);
                    string title2 = GetTTSWord(title1);
                    string cont1 = Joins.Apps.Common.Util.HTMLRemove(this.content);
                    string cont2 = GetTTSWord(cont1);
                    string summary1 = Joins.Apps.Common.Util.HTMLRemove(this.summary);
                    string summary2 = GetTTSWord(summary1);
                    return new TTS {
                        title = title2,
                        summary = summary2,
                        content = cont2
                    };
                 }
            }
            
        }
        public class News10_NOContentTTS {
            public List<Art_imgs> art_imgs { get; set; }
            //  public string art_movs { get; set; }
            public string content { get; set; }
            public string date { get; set; }
            public string id { get; set; }
            string _image;
            public string image {
                get { return _image; }
                set { if (string.IsNullOrEmpty(value)) { _image = ""; } else { if (value.IndexOf("http://pds.joins.com") >= 0) { _image = value; } else { _image = "http://pds.joins.com" + value; } } }
            }
            public List<Summaries> summaries { get; set; }
            public string summary { get; set; }
            string _title;
            public string title { get { return _title; } set { _title = Joins.Apps.Common.Util.GetRexValue(value); } }
            public TTS tts {
                get {
                    string title1 = Joins.Apps.Common.Util.HTMLRemove(this.title);
                    string title2 = GetTTSWord(title1);
                    string summary1 = Joins.Apps.Common.Util.HTMLRemove(this.summary);
                    string summary2 = GetTTSWord(summary1);
                    return new TTS {
                        title = title2,
                        summary = summary2,
                        content = ""
                    };
                }
            }

        }

        public class News10_NOTTS {
            public List<Art_imgs> art_imgs { get; set; }
          //  public string content { get; set; }
            public string date { get; set; }
            public string id { get; set; }
            string _image;
            public string image {
                get { return _image; }
                set { if (string.IsNullOrEmpty(value)) { _image = ""; } else { if (value.IndexOf("http://pds.joins.com") >= 0) { _image = value; } else { _image = "http://pds.joins.com" + value; } } }
            }
            public List<Summaries> summaries { get; set; }
            public string summary { get; set; }
            string _title;
            public string title { get { return _title; } set { _title = Joins.Apps.Common.Util.GetRexValue(value); } }
        }

        public class Special {
            public List<Art_imgs> art_imgs { get; set; }
            public List<Art_movs> art_movs { get; set; }
            public string category { get; set; }
            string _content ;
            public string content { get { return _content; } set {
                    if (value.IndexOf("━") == 0) { _content = value.Replace("━ ", "").Replace("━", ""); }
                    else { _content = value; }
                }
            }
            public string date { get; set; }
            public string id { get; set; }
            string _image;
            public string image {
                get { return _image; }
                set {
                    if (!string.IsNullOrEmpty(value)) {
                        if (value.IndexOf("http://") >= 0 || value.IndexOf("https://") >= 0) { _image = value; }
                        else { _image = "http://pds.joins.com" + value; }
                    }
                    else { _image = ""; }
                }
            }
          //  public string image { get { return _image; } set { _image = string.IsNullOrEmpty(value) ? "" : "http://pds.joins.com" + value; } }
            public List<Summaries> summaries { get; set; }
            public string summary { get; set; }
            string _title;
            public string title { get { return _title; } set { _title = (value); } }
        }
        public class Art_imgs {
            public int h { get; set; }
            string _img_url;
            public string img_url { get { return _img_url; }  set {
                    if (!string.IsNullOrEmpty(value)) {
                        if (value.IndexOf("http://") >= 0 || value.IndexOf("https://") >= 0) { _img_url = value; }
                        else { _img_url = "http://pds.joins.com" + value; }
                    }
                    else { _img_url = ""; }
                }
            }
            public int num { get; set; }
            public int w { get; set; }
        }
        public class Art_movs {
            public string movie_type { get; set; }
            public string movie_url { get; set; }
        }
        public class Summaries {
            public int ord { get; set; }
            public string summary { get; set; }
         //   string _summary;
          //  public string summary { get { return _summary; } set { _summary = (value).Replace("<BR>"," ").Replace("<BR/>", " ").Replace("&lt;BR&gt;", " ").Replace("&lt;BR/&gt;", " "); } }
        }
        public class TTS {
            public string title { get; set; }
            public string summary { get; set; }
            public string content { get; set; }
        }

        public static string GetTTSWord(string cont) {
            //   cont = "'신기록 행진' 방탄소년단, 美 '빌보드 200' 50위…148계단 올라";
            List<string[]> LReplaceWordList = Joins.Apps.Common.Util.GetTTSReplace();//치환 대상 단어
            foreach (string[] sRe in LReplaceWordList) { cont = cont.Replace(sRe[0], sRe[1]); }
            try { Joins.Apps.Common.Util.GetCodeListForHanjatoHangul(); cont = Joins.Apps.Common.Util.toKorean(cont); } catch { }
            try { cont = GetRexProcess(cont); } catch (Exception em) { Joins.Apps.Common.Logger.LogWriteError("TTS 변환 오류", em.Message); }
            return cont;
        }
        /// <summary>
        /// 정규식을 통한 변경
        /// </summary>
        /// <param name="cont"></param>
        /// <returns></returns>
        public static string GetRexProcess(string cont) {
            //       cont = "'신기록 행진' 방탄소년단, 美 '빌보드 200' 50위…148계단 올라";
            //괄호안 문자 제거
            string pattern = @"(\[|\(|（)(.*?)(\）|\]|\))";
            cont = Regex.Replace(cont, pattern, string.Empty);

            //물결(~)처리
            var wRegex = new Regex(@"([0-9])([~∼])([0-9])");
            cont = wRegex.Replace(cont, "$1에서$3");
            // ton 처리
            var tRegex = new Regex(@"([0-9]|[만]|[천])([t])([^a-z])");
            cont = tRegex.Replace(cont, "$1$2on$3");
            //((<?CAP>[0-9])|[만]|[천])([t])([^a-z])

            #region [각종 숫자+세는단위 변경]
            // 1.맨앞에 위치한 경우
            cont = GetFirstWordUnitReplace(cont, @"([1-9]{1,1}[0-9]?)([명|갑|벌|잔|채|개|장|권|병|살|시|달|해|곳|척])", 1);
            cont = GetFirstWordUnitReplace(cont, @"([1-9]{1,1}[0-9]?)(\s?)([군][데]|[그][릇]|[가][지]|[시][간]|[켤][레]|[송][이]|[마][리]|[사][람]|[봉][지]|[번][째])", 2);

            //각종 숫자+세는단위변경(맨앞 위치제외)
            cont = GetNumUnitReplace(cont, @"([^0-9|^,+?*/\\])([1-9]{1,1}[0-9]?)([명|갑|벌|잔|채|개|장|권|병|살|시|달|해|곳|척])", 1);
            cont = GetNumUnitReplace(cont, @"([^0-9|^,+?*/\\])([1-9]{1,1}[0-9]?)(\s?)([군][데]|[그][릇]|[가][지]|[시][간]|[켤][레]|[송][이]|[마][리]|[사][람]|[봉][지]|[번][째])", 2);
            #endregion

            // 119,112,113,114
            MatchCollection ematchList = Regex.Matches(cont, @"[^0-9][1][1][9|2|3|4][가-힣]");
            foreach (Match match in ematchList) {
                var val = match.Value;
                if (!string.IsNullOrEmpty(val)) {
                    var eRegex = new Regex(@"([^0-9])([1])([1])([9|2|3|4])([^대|분|명|갑|벌|잔|채|개|장|부|권|병|살|시|달|번|해|곳|척])");
                    val = eRegex.Replace(val, "$1 1 1 $4$5");
                    cont = cont.Replace(match.Value, val);
                }
            }
            MatchCollection ematchList1 = Regex.Matches(cont, @"[1][1][9|2|3|4][가-힣]");
            foreach (Match match in ematchList1) {
                var val = match.Value;
                if (cont.IndexOf(val) == 0) {
                    if (!string.IsNullOrEmpty(val)) {
                        var eRegex = new Regex(@"([1])([1])([9|2|3|4])([^대|분|명|갑|벌|잔|채|개|장|부|권|병|살|시|달|번|해|곳|척])");
                        val = eRegex.Replace(val, "1 1 $3$4");
                        cont = cont.Replace(match.Value, val);
                    }
                }
            }
            //문장마지막 한칸 띄어읽기
            foreach (Match match in Regex.Matches(cont, @"[가-힣][.][^\s]")) { cont = cont.Replace(match.Value, (match.Value).Replace(".", ". ")); }

            /*대문자+대문자 띄어읽기로 변경*/
            string pattern1 = @"[A-Z][A-Z]+";
            int wi = 0;
            while (wi < 100) {
                MatchCollection matchList = Regex.Matches(cont, pattern1);
                if (matchList.Count > 0) {
                    foreach (Match match in matchList) {
                        if (!string.IsNullOrEmpty(match.Value)) {
                            string[] strReplaceWord = (match.Value).ToCharArray().Select(c => c.ToString()).ToArray();
                            cont = cont.Replace(match.Value, String.Join(" ", strReplaceWord));
                        }
                    }
                }
                else { break; }
                wi++;
            }
            // 정규식에해당하는 특수기호 빼고 모두 제거
            //  var Rgx1 = new Regex(@"[^\w*‘’?!.,＼／~：；""''‘’“”…ㆍ· ]", RegexOptions.IgnoreCase);
            //   cont = Rgx1.Replace(cont, "");
            cont = cont.Replace("━", "");
            return cont;
        }
        /// <summary>
        /// 숫자세는 단위 변경(맨앞에 위치한 경우)
        /// </summary>
        /// <param name="cont"></param>
        /// <param name="numPatten"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        public static string GetFirstWordUnitReplace(string cont, string numPatten, int len) {
            foreach (Match match in Regex.Matches(cont, numPatten)) {
                string val = match.Value;
                string sUnit = (val).Substring(val.Length - len, len);
                string sSecPos = "";
                string sFstPos = "";
                if (cont.IndexOf(val) == 0) {
                    int inum = JCube.AF.Util.Converts.ToInt32((val).Substring(0, (val).Length - len), 0);
                    if (inum > 0) {
                        int iSecPos = (inum / 10);
                        if ((iSecPos) > 0) {
                            sSecPos = (iSecPos.ToString()).Replace("1", "열").Replace("2", "스물").Replace("3", "서른").Replace("4", "마흔").Replace("5", "쉬흔").Replace("6", "예순").Replace("7", "일흔").Replace("8", "여든").Replace("9", "아흔");
                        }
                        int iFstPos = 0;
                        iFstPos = (inum % 10);
                        if ((iFstPos) > 0) {
                            sFstPos = (iFstPos.ToString()).Replace("1", "한").Replace("2", "두").Replace("3", "세").Replace("4", "네").Replace("5", "다섯").Replace("6", "여섯").Replace("7", "일곱").Replace("8", "여덟").Replace("9", "아홉");
                        }
                    }
                    string nReplace = sSecPos + sFstPos + sUnit;
                    var regex = new Regex(val);
                    cont = regex.Replace(cont, nReplace, 1);
                }
            }
            return cont;
        }

        /// <summary>
        /// 숫자세는 단위 변경(맨앞 위치제외)
        /// </summary>
        /// <param name="cont"></param>
        /// <param name="numPatten"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        public static string GetNumUnitReplace(string cont, string numPatten, int len) {
            foreach (Match match in Regex.Matches(cont, numPatten)) {
                string val = match.Value;
                string sFir = (val).Substring(0, 1);
                string sUnit = (val).Substring(val.Length - len, len);
                string sSecPos = "";
                string sFstPos = "";

                int inum = JCube.AF.Util.Converts.ToInt32((val).Substring(1, (val).Length - len - 1), 0);
                if (inum > 0) {
                    int iSecPos = (inum / 10);
                    if ((iSecPos) > 0) {
                        sSecPos = (iSecPos.ToString()).Replace("1", "열").Replace("2", "스물").Replace("3", "서른").Replace("4", "마흔").Replace("5", "쉬흔").Replace("6", "예순").Replace("7", "일흔").Replace("8", "여든").Replace("9", "아흔");
                    }
                    int iFstPos = 0;
                    iFstPos = (inum % 10);
                    if ((iFstPos) > 0) {
                        sFstPos = (iFstPos.ToString()).Replace("1", "한").Replace("2", "두").Replace("3", "세").Replace("4", "네").Replace("5", "다섯").Replace("6", "여섯").Replace("7", "일곱").Replace("8", "여덟").Replace("9", "아홉");
                    }
                }
                string nReplace = sFir + sSecPos + sFstPos + sUnit;
                var regex = new Regex(val);
                cont = regex.Replace(cont, (nReplace));
            }
            return cont;
        }

        //private string GetRexValue(string v) {

        //   // var Rgx = new Regex(@"\[\w*\]", RegexOptions.IgnoreCase);
        //   // return Rgx.Replace(v, "");
        //    string pattern = @"\[\w*\]";
        //    return Regex.Replace(v, pattern, string.Empty);
        //}
    }

    public class Cruz2 {
        public List<News10> news10 { get; set; }
        public class News10 {
            public List<Cruz.Art_imgs> art_imgs { get; set; }
            //  public string art_movs { get; set; }
            public string content { get; set; }
            public string date { get; set; }
            public string id { get; set; }
            string _image;
            public string image {
                get { return _image; }
                set {
                    if (!string.IsNullOrEmpty(value)) {
                        if (value.IndexOf("http://") >= 0 || value.IndexOf("https://") >= 0) { _image = value; }
                        else { _image = "http://pds.joins.com" + value; }
                    }
                    else { _image = ""; }
                }
            }

      //      public string image { get { return _image; } set { _image = string.IsNullOrEmpty(value) ? "" : "http://pds.joins.com" + value; } }
            public List<Cruz.Summaries> summaries { get; set; }
            public string summary { get; set; }
            string _title;
            public string title { get { return _title; } set { _title = Joins.Apps.Common.Util.GetRexValue(value); } }
        }
    }
    public class Cruz3 {
        public class issue {
            public int iss_seq { get; set; }
            public string iss_name { get; set; }
            public string iss_img { get; set; }
            public int article_cnt { get; set; } = 0;
            public List<Article_List> article_list { get; set; }
        }
        public class Article_List {
            public List<Cruz.Art_imgs> art_imgs { get; set; }
            //  public string art_movs { get; set; }
            public string content { get; set; }
            public string date { get; set; }
            public string id { get; set; }
            string _image;
            public string image {
                get { return _image; }
                set {
                    if (!string.IsNullOrEmpty(value)) {
                        if (value.IndexOf("http://") >= 0 || value.IndexOf("https://") >= 0) { _image = value; }
                        else { _image = "http://pds.joins.com" + value; }
                    }
                    else { _image = ""; }
                }
            }
            //public string image { get { return _image; } set { _image = string.IsNullOrEmpty(value) ? "" : "http://pds.joins.com" + value; } }
            public List<Cruz.Summaries> summaries { get; set; }
            public string summary { get; set; }
            string _title;
            public string title { get { return _title; } set { _title = value; } }
        }

        public class MnBSpecial {
            public List<Cruz.Art_imgs> art_imgs { get; set; }
            public string media { get; set; }
            public string category { get; set; }
            public string date { get; set; }
            public string url { get; set; }
            string _image;
            public string image {
                get { return _image; }
                set {
                    if (!string.IsNullOrEmpty(value)) {
                        if (value.IndexOf("http://") >= 0 || value.IndexOf("https://") >= 0) { _image = value; }
                        else { _image = "http://pds.joins.com" + value; }
                    }
                    else { _image = ""; }
                }
            }
            //  public string image { get { return _image; } set { _image = string.IsNullOrEmpty(value) ? "" : "http://pds.joins.com" + value; } }
            public List<Cruz.Summaries> summaries { get; set; }
            public string summary { get; set; }
            string _title;
            public string title { get { return _title; } set { _title = (value); } }
        }
    }
    #endregion
    #region [브리핑 cruz 데이터 입력]
    //public class CruzData {
    //    public string SEQ { get; set; }
    //    public string STAT_DT { get; set; }
    //    public string STAT_TM { get; set; }
    //    public string DIV { get; set; }
    //    public string RNK { get; set; }
    //    public string TOTAL_ID { get; set; }
    //    public string SOURCE_CODE { get; set; }
    //  //  public string PNT_PV { get; set; }
    //  //  public string PNT_LK { get; set; }
    //  //  public string PNT_SH { get; set; }
    //  //  public string PNT_RP { get; set; }
    //    public string POINTS { get; set; }
    //    public string SERVICE_DT { get; set; }
    //    public string ARTICLE_TITLE { get; set; }
    //  //  public string ARTICLE_REPS { get; set; }
    //    public string ARTICLE_MOVS { get; set; }
    //    public string ARTICLE_IMGS { get; set; }
    //    public string ARTICLE_SUMMARIES { get; set; }
    //    public string ARTICLE_SUMMARY { get; set; }
    //  //  public string ARTICLE_KWDS { get; set; }
    //    public string ARTICLE_CONTENT { get; set; }
    //}
    #endregion
    #region [메가박스 BoxOffice]
    public class BoxOffice {
        [XmlRootAttribute("P351Response",Namespace = "http://www.megabox.co.kr/")]
      //  [XmlRootAttribute("P351EntityResponse")]
        public class P351Response {
            [JsonProperty(PropertyName = "BoxOffice")]
            [DataMember, XmlElementAttribute("PARTNERSHIP")]
            public PARTNERSHIP partnership { get; set; }
        }
        [XmlRootAttribute("PARTNERSHIP")]
        public class PARTNERSHIP {
            [JsonProperty(PropertyName = "Header")]
            [DataMember, XmlElementAttribute("Header")]
            public Header header { get; set; }
            [JsonProperty(PropertyName = "Body")]
            [DataMember, XmlElementAttribute("Body")]
            public Body1 body { get; set; }
        }
        [XmlRootAttribute("Header")]
        public class Header{
         //   public string ClientName { get; set; }
            public string ResultCode { get; set; }
            public string ResultMessage { get; set; }
            public int ResultCount { get; set; }
            string _ResultDate = DateTime.Now.ToString();
            public string ResultDate { get { return _ResultDate; } }
        }
        [XmlRootAttribute("Body")]
        public class Body1 {
            [JsonProperty(PropertyName = "Data")]
            [DataMember, XmlElementAttribute("P351Entity")]
            public List<P351Entity> p351Entity { get; set; }
        }
        [XmlRootAttribute("P351Entity")]
        public class P351Entity {
            public string RANK { get; set; }
            public string PERCENTAGE { get; set; }
            public string MOVIECODE { get; set; }
            public string KOREANTITLE { get; set; }
            [JsonConverter(typeof(JsonJoinDateFormatCoverter), "yyyy-MM-dd")]
            public string RELEASEDATE { get; set; }
            [JsonConverter(typeof(JsonJoinDateFormatCoverter), "yyyy-MM-dd")]
            public string STARTDATE { get; set; }
            [JsonConverter(typeof(JsonJoinDateFormatCoverter), "yyyy-MM-dd")]
            public string ENDDATE { get; set; }
            public string RUNNINGTIME { get; set; }
            public string INTERSTINGCNT { get; set; }
            public string STARSCORE { get; set; }
            public string KOFCODE { get; set; }
            public string IMAGEURL1 { get; set; }
            public string IMAGEURL2 { get; set; }
            public string IMAGEURL3 { get; set; }
        }
    }
    #endregion

    #region [평창 스페셜카드]
    public class PyeongchangInput {
        public class article_list {
            [DataMember, XmlElementAttribute("article")]
            public List<article1> article { get; set; }
        }
        public class article1 {
            public string total_id { get; set; }
            public string title { get; set; }
            public string image_pc { get; set; }
            public string image_mobile { get; set; }
            public string link { get; set; }
            public string content { get; set; }
        }
        public class briefinput {
            public string brief { get; set; }
            public link1 link { get; set; }
            public string image { get; set; }
        }
        public class link1 {
            public string url { get; set; }
        }
    }
    public class PyeongchangOutput {
        public string dt { get; set; }
        public List<Data> data { get; set; }
        public class Data {
            public string title { get; set; }
            public string id { get; set; }
            public string image { get; set; }
        }
    }
    #endregion
    #region [ JTBC Youtube channel list]
    public class JtbcVodListInput {
        public string kind { get; set; }
        public List<items1> items {get;set;}
        public class items1 {
            public string kind { get; set; }
            public id1 id { get; set; }
        }
        public class id1 {
            public string kind { get; set; }
            public string videoId { get; set; }
        }
    }
    public class JtbcVodInput {
        public string kind { get; set; }
        public List<items1> items { get; set; }
        public class items1 {
            public string kind { get; set; }
            public string id { get; set; }
            public snippet1 snippet { get; set; }
        }
        public class snippet1 {
            public string publishedAt { get; set; }
            public string title { get; set; }
            public string description { get; set; }
            public thumbnails1 thumbnails { get; set; }
            public class thumbnails1 {
                public medium1 medium { get;set;}
            }
        }
        public class medium1 {
            public string url { get; set; }
            public int width { get; set; }
            public int height { get; set; }
        }
    }
    #endregion
    #region [이슈패키지]
    public class IssuePackList {
        public int iss_seq { get; set; }
        public string iss_url { get; set; }
        public string iss_title { get; set; }
        public string edit_iss_title { get; set; }
        public string iss_img { get; set; }
        public string edit_iss_img { get; set; }
        public string s_date { get; set; }
        public List<article_list1> article_list { get; set; }
        public class article_list1 {
            public string total_id { get; set; }
            public string ctg { get; set; }
            public string categorykey { get; set; }
            public string title { get; set; }
            public string summary { get; set; }
            public string thumbnail { get; set; }
            public string service_day { get; set; }
            public string serivce_time { get; set; }

        }
    }
    #endregion
    #region [대기정보 -환경관리공단]
    public class AirKoreaInput {
        public List<list1> list { get; set; }
        public class list1 {
            public string stationName { get; set; }//중구
            public string mangName { get; set; }//도시대기
            public string dataTime { get; set; }//2018-05-11 13:00
            public string so2Value { get; set; } = "-";//0.002 아황산가스농도 -- 사용
            public string coValue { get; set; } = "-";//0.5 일산화 농도 - 사용
            public string o3Value { get; set; } = "-";//0.052 오존농도 - 사용
            public string no2Value { get; set; } = "-";//0.039 이산화질소 - 사용
            public string pm10Value { get; set; } = "-";//49</미세먼지농도 - 사용
                                                        //   public string pm10Value24 { get; set; }//52 24시간예측이동농도
            public string pm25Value { get; set; } = "-";//29 초미세먼지농도 - 사용
                                                        //    public string pm25Value24 { get; set; }//32 24시간 예측이동농도
            public string khaiValue { get; set; } = "-";//92 통합대기환경수치 - 사용

            public string khaiGrade { get; set; } = "0";//2 통합대기환경지수 - 사용
            public string so2Grade { get; set; } = "0";//1 아황산가스지수 - 사용
            public string coGrade { get; set; } = "0";//1 일산화탄소지수 - 사용
            public string o3Grade { get; set; } = "0";//2 오존지수 - 사용
            public string no2Grade { get; set; } = "0";//2 이산화질소 지수 - 사용
                                                       //    public string pm10Grade { get; set; }//2 미세먼지지수 24시간
                                                       //    public string pm25Grade { get; set; }//2 초미세먼지 24시간
            public string pm10Grade1h { get; set; } = "0";//2 미세먼지 한시간지수 - 사용
            public string pm25Grade1h { get; set; } = "0";//2 미세먼지 한시간지수 - 사용

            public string informCode { get; set; }//미세먼지 코드(미세먼지,초미세먼지)
            public string informData { get; set; }// 날짜정보
            public string informOverall { get; set; }//내일 예보
            public string informGrade { get; set; }//지역별 등급
            public string informCause { get; set; }//내일 예보 원인
            
        }

    }

    public class AirKoreaOutput {
        public string admin { get; set; }
        public string datatime { get; set; }//2018-05-11 13:00
        public string khaivalue { get; set; }//92 통합대기환경수치 - 사용
        public int khaigrade { get; set; }//2 통합대기환경지수 - 사용
        public string so2value { get; set; }//0.002 아황산가스농도 -- 사용
        public int so2grade { get; set; }//1 아황산가스지수 - 사용
        public string covalue { get; set; }//0.5 일산화 농도 - 사용
        public int cograde { get; set; }//1 일산화탄소지수 - 사용
        public string o3value { get; set; }//0.052 오존농도 - 사용
        public int o3grade { get; set; }//2 오존지수 - 사용
        public string no2value { get; set; }//0.039 이산화질소 - 사용
        public int no2grade { get; set; }//2 이산화질소 지수 - 사용
        public string pm10value { get; set; }//49</미세먼지농도 - 사용
        public int pm10grade { get; set; }//2 미세먼지 한시간지수 - 사용
        public string pm25value { get; set; }//29 초미세먼지농도 - 사용
        public int pm25grade { get; set; }//2 미세먼지 한시간지수 - 사용

        public string informoverall { get; set; }//내일 예보
        public int informpm25Grade { get; set; }//pm2.5 지역별 등급
        public int informpm10Grade { get; set; }//pm10 지역별 등급
    }
    #endregion

    #region [여성지 6종]
    public class MnBSpecialInput {
        public string response_code { get; set; }
        public string response_data_count { get; set; }
        public List<response_data1> response_data { get; set; }
        public class response_data1 {
            public string media_name { get; set; }
            public string article_title { get; set; }
            public string category_name { get; set; }
            public string article_summary { get; set; }
            public string article_cover_image_url { get; set; }
            public int article_cover_image_width { get; set; }
            public int article_cover_image_height { get; set; }
            public string article_link { get; set; }
            public string publish_date { get; set; }
        }

    }
    #endregion

    public class sullyReturn {
        public List<Joins.Apps.Models.Sully.APIProduct> sully { get; set; }
    }
}
