﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.News10 {
    public class Event {
        public int ROWNUM { get; set; }
        public int seq { get; set; }
        public string used_yn { get; set; }
        public string title { get; set; }
        public string summary { get; set; }
        public string event_type { get; set; }
        public string event_cont { get; set; }
        public string img { get; set; }
        public string bg_img { get; set; }
        public string event_sdt { get; set; }
        public string event_edt { get; set; }
        public string reg_dt { get; set; }
        public string mod_dt { get; set; }
    }
    public class EventData {
        public string title { get; set; }
        public string summary { get; set; }
        public string event_type { get; set; }
        public string event_cont { get; set; }
        public string image { get; set; }
        public string bg_image { get; set; }
        public string event_sdt { get; set; }
        public string event_edt { get; set; }
    }
}
