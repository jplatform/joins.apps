﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.News10 {
    public class PushListData {
        public string id { get; set; }
        public string token { get; set; }
    }
    public class NotiPushListData {
        public string devType { get; set; }
        public string token { get; set; }
        public string message { get; set; }
    }
    public class PushListParameters {
        public string deviceType { get; set; }
        public string pushType { get; set; }
        public string pushMessage { get; set; }
        public int pushCount { get; set; }
    }
    public class PushListParameters2 {
        public string deviceType { get; set; }
        public string pushType { get; set; }
        public string pushMessage { get; set; }
        public int pushCount { get; set; }
    }

    public class PushLogListData {
        public string id { get; set; }
        public string type { get; set; }
        public string message { get; set; }
        public string date { get; set; }
    }

    public class PushLogListParameters : PagingParameters {
        public string pushType { get; set; }
        public string startDate { get; set; }
    }

    public class PushLogUpdateParameters {
        public string pushType { get; set; }
        public string pushMessage { get; set; }
        public int pushCount { get; set; }
    }
    public class NotiPush {
        public int ROWNUM { get; set; }
        public int SEQ { get; set; }
        public string USED_YN { get; set; }
        public string PUSH_TYPE { get; set; }
        public string OS_DIV { get; set; }
        public string PUSH_MSG { get; set; }
        public string PUSH_TIME { get; set; }
        public string REG_ID { get; set; }
        public string REG_DT { get; set; }
        public string MOD_ID { get; set; }
        public string MOD_DT { get; set; }
    }
}
