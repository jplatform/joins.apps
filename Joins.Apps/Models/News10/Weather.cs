﻿using System.Collections.Generic;

namespace Joins.Apps.Models.News10 {
    public class WeatherData {
        public string admin { get; set; }
        public string condition { get; set; }
        public string conditionText { get; set; }
        public string temp { get; set; }
        public string tempMax { get; set; }
        public string tempMin { get; set; }
        public string tempDif { get; set; }
        public string pm10 { get; set; }
        public string pm10Grade { get; set; }

        public List<WeatherForecastData> Forecast { get; set; }
        public WeatherData() {
            admin = "서울";
            condition = "1";
            conditionText = "";
            temp = "-";
        }
    }

    public class WeatherForecastData {
        public string time { get; set; }
        public string condition { get; set; }
        public string conditionText { get; set; }
        public string temp { get; set; }
        public string rain { get; set; }
    }

    public class WeatherParameters {
        public string admin { get; set; }
    }
}
