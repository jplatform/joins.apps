﻿using System.Collections.Generic;

namespace Joins.Apps.Models.News10 {
    public class Today {
        public CoverInfo Coverinfo { get; set; }
        public HotKeyInfo Hotkeyinfo { get; set; }
        public string endingMessage { get; set; }
        public WeatherData Weather { get; set; }
        public List<SpecialDayData> Anniversary { get; set; }
        public News10.AirKoreaOutput Airinfo { get; set; }
        public Today() {Weather = new WeatherData();}
    }
    public class CoverInfo {
        public string coverimg { get; set; }
        public string msg { get; set; }
    }
    public class HotKeyInfo {
        public string img { get; set; }
        public string msg { get; set; }
        public string type { get; set; }
        public string link { get; set; }
    }
}
