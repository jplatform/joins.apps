﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.Models.News10 {
    public class Result_CardInfo {
        public int order { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string ename { get; set; }
    }
    public class CardList {
        public string id { get; set; }
        public string name { get; set; }
        public string ename { get; set; }
        public string setting_yn { get; set; }
        public string visible_yn { get; set; }
    }
    public class CardListUpdateParam {
        public string deviceID { get; set; }
        public string cardID { get; set; }
        public string usedYn { get; set; }
    }
    public class CardInfo {
        public int ROWNUM { get; set; }
        public int SEQ { get; set; }
        public int CARD_ORDER { get; set; }
        public string USED_YN { get; set; }
        public string CARD_ID { get; set; }
        public string CARD_NAME { get; set; }
        public string CARD_ENG_NAME { get; set; }
        public string  VERSION { get; set; }
        public string MAX_VERSION { get; set; }
        public string REG_DT { get; set; }
    }

    public class Card {
        public List<ArticleData> History { get; set; }
        public List<ArticleData> Column { get; set; }
        public List<ArticleData> Newsflash { get; set; }
        public List<EventData> Event { get; set; }
    }
    public class ExceptCard {
        public int ROWNUM { get; set; }
        public int seq { get; set; }
        public string total_id { get; set; }
        public string art_title { get; set; }
        public string reg_dt { get; set; }
        public string reg_id { get; set; }
        public string used_yn { get; set; }
        public string mod_id { get; set; }
        public string mod_dt { get; set; }
    }
}