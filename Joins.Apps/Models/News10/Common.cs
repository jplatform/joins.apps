﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.News10 {
    public class returnData<T> {
        public string r_code { get; set; }
        public string r_msg { get; set; }
        public string r_dt { get; set; }
        public int r_tcnt { get; set; }
        public Joins.Apps.Models.News10.Result_CardInfo cardinfo = new Result_CardInfo();
        public T data { get; set; }

    }
    public class returnData2<T> {
        public string r_code { get; set; }
        public string r_msg { get; set; }
        public string r_dt { get; set; }
        public int r_tcnt { get; set; }
        public T News10 { get; set; }

    }
    /// <summary>
    /// 컨텐츠 통합 리턴데이터
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class returnALLData {
        public string r_code { get; set; }
        public string r_msg { get; set; }
        public string r_dt { get; set; }
        public List<Contents> contents { get; set; }
      

    }

    public class Contents {
        public string r_code { get; set; }
        public string r_msg { get; set; }
        public string r_dt { get; set; }
        public int r_tcnt { get; set; }
        public Joins.Apps.Models.News10.Result_CardInfo cardinfo = new Result_CardInfo();
        public IEnumerable<object> data { get; set; }
    }
    public class AppInfoData {
        public string type { get; set; }
        public string version { get; set; }
        public string minorVersion { get; set; }
        public string ver_notice { get; set; }
        public string update { get; set; }
        public string market_info { get; set; }

    }
    public class AppDeviceData {
        public string type { get; set; }
        public string token { get; set; }
        public string gender { get; set; }
        public string birthday { get; set; }
        public List<AppDevicePushData> Push { get; set; }
    }

    public class AppDevicePushData {
        public string type { get; set; }
        public string flag { get; set; }
        public string time { get; set; }
    }

    public class AppDeviceParameters {
        public string deviceID { get; set; }
    }

    public class AppDeviceUpdateParameters {
        public string deviceID { get; set; }
        public string deviceType { get; set; }
        public string deviceToken { get; set; }
        public string deviceGender { get; set; }
        public string deviceBirthday { get; set; }
        public string deviceVersion { get; set; }
        public string deviceInfo { get; set; }
        public string AppVersion { get; set; }
    }

    public class AppDevicePushUpdateParameters {
        public string deviceID { get; set; }
        public string pushType { get; set; }
        public string pushFlag { get; set; }
        public string pushTime { get; set; }
    }
    public class AppApiLogParameters {

        public string deviceID { get; set; }
        public string type { get; set; }
        public string apiurl { get; set; }
        public string resultMsg { get; set; }
    }
    public class ArticleLogParameters {
        public string deviceID { get; set; }
        public int div { get; set; }
        public string cardID { get; set; }
        public int totalID { get; set; }
    }
    public class ArticleLikeData {
        public int total_likecnt { get; set; }
        public int likecnt { get; set; }
    }
    public class ArticleLikeParam {
        public string deviceID { get; set; }
        public int totalID { get; set; }
        public string cardID { get; set; } 
        public int likeCnt { get; set; } = 0;
    }
    public class BriefRestoreData {
        public string used_yn { get; set; }
        public string stat_dt { get; set; }
        public string reg_dt { get; set; }
        public string mod_dt { get; set; }
    }
}
