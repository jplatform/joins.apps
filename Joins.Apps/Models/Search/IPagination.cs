﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Search {
    public interface IPagination {
        /// <summary>
        ///     페이지 번호
        /// </summary>
        int Page { get; set; }

        /// <summary>
        ///     한 페이지 당 노출 되는 Row 수
        /// </summary>
        int PageSize { get; set; }

        /// <summary>
        ///     총 갯수 필요 여부
        /// </summary>
        bool IsNeedTotalCount { get; set; }
    }
}
