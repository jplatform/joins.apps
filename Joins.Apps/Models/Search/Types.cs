﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Search {
    /// <summary>
    ///     정렬 타입
    /// </summary>
    public enum SortType {
        /// <summary>
        ///     최신순
        /// </summary>
        New = 0,

        /// <summary>
        ///     정확도
        /// </summary>
        Accuracy,

        /// <summary>
        ///     조회순
        /// </summary>
        View,
    }
    public enum SearchCategoryType {
        /// <summary>
        ///     통합 검색
        /// </summary>
        UnifiedSearch = 0,

        /// <summary>
        ///     뉴스 - 중앙일보, 중앙선데이, 전체뉴스
        /// </summary>
        News,

        /// <summary>
        ///     뉴스 - 중앙일보
        /// </summary>
        JoongangNews,

        /// <summary>
        ///     뉴스 - 중앙선데이
        /// </summary>
        JoongangSundayNews,

        /// <summary>
        ///     뉴스 - 전체뉴스
        /// </summary>
        TotalNews,

        /// <summary>
        ///     뉴스 - 관련뉴스
        /// </summary>
        RelationNews,

        /// <summary>
        ///     이미지
        /// </summary>
        Image,

        /// <summary>
        ///     동영상
        /// </summary>
        Video,

        /// <summary>
        ///     조인스
        /// </summary>
        Joins,

        /// <summary>
        ///     인물
        /// </summary>
        People,

        /// <summary>
        ///     J플러스
        /// </summary>
        Jplus,

        /// <summary>
        ///     블로그
        /// </summary>
        Blog,

        /// <summary>
        ///     이슈 - 모바일
        /// </summary>
        Issue,

        /// <summary>
        ///     기자 - 모바일
        /// </summary>
        Reporter,

        /// <summary>
        ///     이슈페이지 - 기사 리스트
        /// </summary>
        IssueNews,

        /// <summary>
        ///     중앙일보 기사만
        /// </summary>
        OnlyJoongangNews,
    }
    /// <summary>
    ///     기간 타입
    /// </summary>
    public enum PeriodType {
        /// <summary>
        ///     전체
        /// </summary>
        All = 0,

        /// <summary>
        ///     1일
        /// </summary>
        OneDay,

        /// <summary>
        ///     1주
        /// </summary>
        OneWeek,

        /// <summary>
        ///     1개월
        /// </summary>
        OneMonth,

        /// <summary>
        ///     직접입력
        /// </summary>
        DirectInput,
    }
    /// <summary>
    ///     범위 타입
    /// </summary>
    public enum ScopeType {
        /// <summary>
        ///     전체
        /// </summary>
        All = 0,

        /// <summary>
        ///     제목 + 내용
        /// </summary>
        TitleContent,

        /// <summary>
        ///     제목
        /// </summary>
        Title,

        /// <summary>
        ///     내용
        /// </summary>
        Content,

        /// <summary>
        ///     기자명
        /// </summary>
        Reporter,

        /// <summary>
        ///     키워드
        /// </summary>
        Keyword,

        /// <summary>
        ///     제목 + 키워드 + 기자명
        /// </summary>
        TitleKeywordReporter,
    }
    /// <summary>
    ///     이미지형태 타입
    /// </summary>
    public enum ImageType {
        /// <summary>
        ///     전체
        /// </summary>
        All = 0,

        /// <summary>
        ///     기사이미지
        /// </summary>
        Article,

        /// <summary>
        ///     포토갤러리
        /// </summary>
        Photo,

        /// <summary>
        ///     화보
        /// </summary>
        Gallery,
    }
    /// <summary>
    ///     J플러스 타입
    /// </summary>
    public enum JplusType {
        /// <summary>
        ///     전체
        /// </summary>
        All = 0,

        /// <summary>
        ///     필진이름
        /// </summary>
        WriterName,

        /// <summary>
        ///     필진정보(직업)
        /// </summary>
        WriterJob,

        /// <summary>
        ///     태그
        /// </summary>
        Tag,
    }
    public enum BlogType {
        /// <summary>
        ///     전체
        /// </summary>
        All = 0,

        /// <summary>
        ///     이미지
        /// </summary>
        Image,

        /// <summary>
        ///     이름/별명
        /// </summary>
        NickName,

        /// <summary>
        ///     태그
        /// </summary>
        Tag,
    }
    /// <summary>
    ///     이미지 검색 타입
    /// </summary>
    public enum ImageSearchType {
        /// <summary>
        ///     리스트형
        /// </summary>
        Image = 0,

        /// <summary>
        ///     이미지형
        /// </summary>
        List,
    }
    /// <summary>
    ///     이슈홈 - 이슈 목록 카테고리 타입
    /// </summary>
    public enum IssueCategoryType {
        /// <summary>
        ///     전체
        /// </summary>
        All = 0,

        /// <summary>
        ///     가
        /// </summary>
        Ga,

        /// <summary>
        ///     나
        /// </summary>
        Na,

        /// <summary>
        ///     다
        /// </summary>
        Da,

        /// <summary>
        ///     라
        /// </summary>
        Ra,

        /// <summary>
        ///     마
        /// </summary>
        Ma,

        /// <summary>
        ///     바
        /// </summary>
        Ba,

        /// <summary>
        ///     사
        /// </summary>
        Sa,

        /// <summary>
        ///     아
        /// </summary>
        Ah,

        /// <summary>
        ///     자
        /// </summary>
        Ja,

        /// <summary>
        ///     차
        /// </summary>
        Cha,

        /// <summary>
        ///     카
        /// </summary>
        Ka,

        /// <summary>
        ///     타
        /// </summary>
        Ta,

        /// <summary>
        ///     파
        /// </summary>
        Pa,

        /// <summary>
        ///     하
        /// </summary>
        Ha,

        /// <summary>
        ///     그외
        /// </summary>
        Etc,
    }
    /// <summary>
    ///     매체 그룹 타입
    /// </summary>
    public enum SourceGroupType {
        /// <summary>
        ///     전체
        /// </summary>
        All = 0,

        /// <summary>
        ///     중앙일보
        /// </summary>
        Joongang,

        /// <summary>
        ///     일간스포츠
        /// </summary>
        IlganSports,

        /// <summary>
        ///     JTBC
        /// </summary>
        Jtbc,

        /// <summary>
        ///     중앙선데이
        /// </summary>
        JoongangSunday,

        /// <summary>
        ///     중앙데일리
        /// </summary>
        JoongangDaily,

        /// <summary>
        ///     온라인 중앙일보
        /// </summary>
        JoongangOnline,

        /// <summary>
        ///     기타
        /// </summary>
        Etc,
    }
    public enum DetailSearchFieldType {
        Title = 0,
        SubTitle,
        Content,
        Reporter,
        Keyword,
    }
    /// <summary>
    ///     링크 대상 타입
    /// </summary>
    public enum TargetType {
        /// <summary>
        ///     본창
        /// </summary>
        Self = 0,

        /// <summary>
        ///     부모창
        /// </summary>
        Parent,

        /// <summary>
        ///     새창
        /// </summary>
        Blank,
    }
    public enum ArticleType {
        Text = 0,
        Photo,
        Video,
    }
    public enum RemarkType {
        None = 0,
        Best,
        Recommend
    }
    public enum SearchFieldType {
        Title = 0,
        Reporter,
    }
}
