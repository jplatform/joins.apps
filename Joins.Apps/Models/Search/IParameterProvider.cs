﻿using System.Collections.Generic;

namespace Joins.Apps.Models.Search {
    public interface IParameterProvider {
        Dictionary<string, string> ToParameters();
    }
}
