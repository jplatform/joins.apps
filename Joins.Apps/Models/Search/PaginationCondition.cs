﻿using System.ComponentModel.DataAnnotations;

namespace Joins.Apps.Models.Search {
    public class PaginationCondition : IPagination {
        public PaginationCondition() {
            Page = 1;
            PageSize = 20;
            IsNeedTotalCount = true;
        }

        [Range(1, int.MaxValue)]
        public int Page { get; set; }

        [Range(1, 50)]
        public int PageSize { get; set; }

        public bool IsNeedTotalCount { get; set; }
    }
}
