﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Joins.Apps.Common.Utilities;

namespace Joins.Apps.Models.Search {
    public abstract class BaseResult {
        public IEnumerable<Issue> Keywords { get; set; }
        public IEnumerable<Keyword> RelationalKeywords { get; set; }
        public IEnumerable<PopularSearchWord> PopularSearchWords { get; set; }
        public int TotalCount { get; set; }
    }

    public class UnifiedSearchResult : BaseResult {
        public IEnumerable<ShortCut> ShortCuts { get; set; }
        public IEnumerable<People> Peoples { get; set; }
        public int PeopleTotalCount { get; set; }
        public IEnumerable<Keyword> Issues { get; set; }
        public Dictionary<SearchCategoryType, IEnumerable<SearchItem>> SearchResultDictionary { get; set; }
        public Dictionary<SearchCategoryType, int> SearchTotalCountDictionary { get; set; }
        public IEnumerable<Writer> Reporters { get; set; }
    }
    public class NewsResult : BaseResult {
        public IEnumerable<Keyword> Issues { get; set; }
        public Dictionary<SearchCategoryType, IEnumerable<SearchItem>> SearchResultDictionary { get; set; }
        public IEnumerable<Source> Sources { get; set; }
        public IEnumerable<Service> Services { get; set; }
        public IEnumerable<Writer> Reporters { get; set; }
        public Dictionary<SearchCategoryType, int> SearchTotalCountDictionary { get; set; }
    }
    public class DefaultResult : BaseResult {
        public IEnumerable<Keyword> Issues { get; set; }
        public IEnumerable<SearchItem> SearchItems { get; set; }
        public IEnumerable<Writer> Reporters { get; set; }
        public IEnumerable<Source> Sources { get; set; }
        public IEnumerable<Service> Services { get; set; }
    }
    public class Source {
        public string Code { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
    }
    public class Service {
        public string Code { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
    }
    public class Issue {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Thumbnail { get; set; }
        public IEnumerable<Article> Articles { get; set; }
        public IEnumerable<Reporter> Reporters { get; set; }
        public SearchedCondition SearchedCondition { get; set; }

        public bool IsValid() {
            return !(Id == default(int) && string.IsNullOrEmpty(Title));
        }
    }
    public class Keyword {
        public int Id { get; set; }
        public string Word { get; set; }
    }
    public class PopularSearchWord {
        public int Rank { get; set; }
        public string Word { get; set; }
    }
    public class ShortCut {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Summary { get; set; }
        public string Thumbnail { get; set; }
    }
    public class People {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NameByChinese { get; set; }
        public string NameByEnglish { get; set; }
        public string Thumbnail { get; set; }
        public string Job { get; set; }
        public string BirthPlace { get; set; }
        public DateTime BirthDate { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
        public string Agency { get; set; }
        public IEnumerable<string> Awards { get; set; }
        public IEnumerable<string> Careers { get; set; }
    }
    public class SearchItem {
        public string SectionName { get; set; }
        public int Id { get; set; }
        public int RelationId { get; set; }
        public int MediaId { get; set; }
        public int PublishId { get; set; }
        public string Title { get; set; }
        public string MobileTitle { get; set; }
        public string Thumbnail { get; set; }
        public string Source { get; set; }
        public string Summary { get; set; }
        public bool IsPaid { get; set; }
        public Writer Writer { get; set; }
        public DateTime RegistedDateTime { get; set; }
        public IEnumerable<Keyword> Keywords { get; set; }
        public IEnumerable<SearchItem> RelationalItems { get; set; }
        public int ImageWidth { get; set; }
        public int ImageHeight { get; set; }
        public string OuterUrl { get; set; }
        public string UserId { get; set; }
        public string ArticleType { get; set; }
        public string SourceCode { get; set; }
        public int DuplicateCount { get; set; }
        public string MultiType { get; set; }
    }
    public class Writer {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Department { get; set; }
        public string Thumbnail { get; set; }
        public int Count { get; set; }
    }

    public class Article {
        public Article() {
            Issues = new List<Issue>();
            Reporters = new List<Reporter>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Thumbnail { get; set; }
        public string Summary { get; set; }
        public string Link { get; set; }
        public string MobileLink { get; set; }
        public TargetType Target { get; set; }
        public string TypeSetting { get; set; }
        public IEnumerable<Issue> Issues { get; set; }
        public ArticleType Type { get; set; }
        public string Source { get; set; }
        public string Keyword { get; set; }
        public IEnumerable<Reporter> Reporters { get; set; }
        public string Category { get; set; }
        public DateTime RegistedDate { get; set; }
        public string CategoryLink { get; set; }
        public string FirstLink { get; set; }
        public string CategoryKey { get; set; }
        public string SubImage { get; set; }
        public string FullTitle { get; set; }
        public bool IsFresh { get; set; }
        public bool IsExclusive { get; set; }
        public int SomeCount { get; set; }
        public RemarkType Remark { get; set; }
        public int ClickCount { get; set; }
        public int ReplyCount { get; set; }
        public string InfomationText { get; set; }
        public int JoinId { get; set; }

        public bool IsValid() {
            return !(
                        Id == default(int) &&
                        string.IsNullOrEmpty(Title) &&
                        string.IsNullOrEmpty(SubTitle) &&
                        string.IsNullOrEmpty(Thumbnail) &&
                        string.IsNullOrEmpty(Summary) &&
                        string.IsNullOrEmpty(Link) &&
                        Target == default(TargetType) &&
                        TypeSetting == default(string) &&
                        !Issues.IsAny() &&
                        Type == default(ArticleType) &&
                        string.IsNullOrEmpty(Source) &&
                        string.IsNullOrEmpty(Keyword) &&
                        !Reporters.IsAny() &&
                        string.IsNullOrEmpty(Category) &&
                        RegistedDate == default(DateTime) &&
                        string.IsNullOrEmpty(CategoryLink) &&
                        string.IsNullOrEmpty(FirstLink) &&
                        string.IsNullOrEmpty(CategoryKey) &&
                        string.IsNullOrEmpty(SubImage) &&
                        string.IsNullOrEmpty(FullTitle) &&
                        IsFresh == default(bool) &&
                        IsExclusive == default(bool) &&
                        SomeCount == default(int) &&
                        Remark == default(RemarkType) &&
                        ClickCount == default(int) &&
                        ReplyCount == default(int) &&
                        string.IsNullOrEmpty(InfomationText)
                    );
        }
    }
    public class Reporter {
        public int Id { get; set; }
        public string Position { get; set; }
        public string Name { get; set; }
        public string Profile { get; set; }
        public string Description { get; set; }
        public SearchedCondition SearchedCondition { get; set; }
        public IEnumerable<Article> Articles { get; set; }
        public int Count { get; set; }
        public Issue Issue { get; set; }
        public string Html { get; set; }
        public string Title { get; set; }
    }
    public class SearchedCondition {
        public string Keyword { get; set; }

        public IEnumerable<string> MasterCodes { get; set; }

        public SearchFieldType Field { get; set; }
    }
}
