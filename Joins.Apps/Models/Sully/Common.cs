﻿using Joins.Apps.Repository.Common.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Sully {
   public class UrlInfo {
        public string url { get; set; }
        public string title { get; set; }
        public string img { get; set; }
        public string description { get; set; }
        public string domain { get; set; }

    }
    public class UrlInfoParam {
        public string url { get; set; }
    }
    public class ImgFileInfo {
        public int seq { get; set; }
        public string url { get; set; }
        public int h { get; set; }
        public int w { get; set; }
        public int size { get; set; }
        public string tag { get; set; }
        public string desc { get; set; }
        public string editor { get; set; }
    }
   
    public class ImgData {
        [DbColumn("rownum")]
        public int ROWNUM { get; set; }
        [DbColumn("seq")]
        public int SEQ { get; set; }
        [DbColumn("used_yn")]
        public string USED_YN { get; set; }
        [DbColumn("img_url")]
        public string IMG_URL { get; set; }
        [DbColumn("img_h")]
        public int IMG_H { get; set; }
        [DbColumn("img_w")]
        public int IMG_W { get; set; }
        [DbColumn("img_size")]
        public int IMG_SIZE { get; set; }
        [DbColumn("img_desc")]
        public string IMG_DESC { get; set; }
        [DbColumn("img_tag")]
        public string IMG_TAG{ get; set; }
        [DbColumn("reg_id")]
        public string REG_ID { get; set; }
        [DbColumn("reg_dt")]
        public string REG_DT { get; set; }
        [DbColumn("character_type")]
        public int CHARACTER_TYPE { get; set; }
        [DbColumn(Ignore = true)]
        public string CHARACTER_TYPE_NAME { get; set; }
    }

    public class StickerData {
        public string Sticker_Name { get; set; }
        public string Sticker_URL { get; set; }
    }
    public class LogParam {

        public string devid { get; set; }
        public string sec { get; set; }
        public int pseq { get; set; }
        public string share { get; set; }
        public string ip { get; set; }
        public string type { get; set; }
        public int step { get; set; }
        public string refer { get; set; }
    }
}
