﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Sully {
    public class PushParam {
        public int pgi { get; set; } = 1;
        public int ps { get; set; } = 20;
        public int seq { get; set; } = 0;
        public string type { get; set; } = "";
        public string status { get; set; } = "";
        public string order { get; set; } = "";
        public string sdt { get; set; } = DateTime.Now.AddMonths(-1).ToString("yyyy-MM-dd");
        public string edt { get; set; } = DateTime.Now.ToString("yyyy-MM-dd");
    }
    public class PushEditParam {
        public int seq { get; set; }
        public string p_status { get; set; }
        public string p_type { get; set; }
        public string p_title { get; set; }
        public string p_content { get; set; }
        public string p_img { get; set; }
        public string push_dt { get; set; }
        public string p_os { get; set; }
    }
    public class Push {
        /// <summary>
        /// 일련번호
        /// </summary>
        public int rownum { get; set; }
        /// <summary>
        /// 일련번호
        /// </summary>
        public int seq { get; set; }
        /// <summary>
        /// 푸시상태(R:대기,S:성공,C:취소
        /// </summary>
        public string p_status { get; set; }
        /// <summary>
        /// 푸시타입(0:댓글,1:문의,2:컨텐츠,3:공지사항,4:일반알림
        /// </summary>
        public string p_type { get; set; }
        /// <summary>
        /// 회원번호
        /// </summary>
        public string mem_seq { get; set; }
        /// <summary>
        /// 타이틀
        /// </summary>
        public string p_title { get; set; }
        /// <summary>
        /// 내용
        /// </summary>
        public string p_content { get; set; }
        /// <summary>
        /// 이미지
        /// </summary>
        public string p_img { get; set; }
        /// <summary>
        /// 예약 발송일시
        /// </summary>
        public string push_dt { get; set; }
        /// <summary>
        /// 등록일시
        /// </summary>
        public string reg_dt { get; set; }
        /// <summary>
        /// 발송카운트
        /// </summary>
        public int p_cnt { get; set; }
        /// <summary>
        /// 발송대상 운영체제
        /// </summary>
        public string p_os { get; set; }
    }
}
