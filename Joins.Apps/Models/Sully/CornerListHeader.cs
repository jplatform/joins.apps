﻿using Joins.Apps.Repository.Common.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Sully
{
    public class CornerListHeader
    {
        [DbColumn("pd_type")]
        public int? PdType { get; set; }
        [DbColumn("link_type")]
        public string LinkType { get; set; }
        [DbColumn("link_value")]
        public string LinkValue { get; set; }
        [DbColumn("img_url")]
        public string ImageUrl { get; set; }
        [DbColumn("upload_dt")]
        public DateTime? UploadDateTime { get; set; }
        [DbColumn("seq")]
        public int? LinkDataSeq { get; set; }
        [DbColumn("title")]
        public string LinkDataTitle { get; set; }
    }
}
