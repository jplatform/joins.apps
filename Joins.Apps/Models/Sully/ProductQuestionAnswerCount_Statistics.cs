﻿using Joins.Apps.Repository.Common.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Joins.Apps.Common.Utilities.ExcelExportHelper;

namespace Joins.Apps.Models.Sully
{
    public class ProductQuestionAnswerCount_Statistics
    {
        public Product ProductInfo { get; set; }
        public IEnumerable<AnswerCount> AnswerInfo { get; set; }

        public class Product
        {
            [DbColumn("pd_seq")]
            public int Seq { get; set; }
            [DbColumn("pd_title")]
            public string Title { get; set; }
            [DbColumn("pd_service_dt")]
            public DateTime PublishDateTime { get; set; }
        }

        public class AnswerCount
        {
            [DbColumn("total_answer_count")]
            [ExcelHeaderTItle(Title = "응답자 수")]
            public int TotalAnswerCount { get; set; }
        }
    }
}
