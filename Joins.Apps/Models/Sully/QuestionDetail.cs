﻿using Joins.Apps.Repository.Common.Attributes;
using System;
using System.Collections.Generic;

namespace Joins.Apps.Models.Sully
{
    public class QuestionDetail
    {
        /// <summary>
        /// 문의 글 목록
        /// </summary>
       public IEnumerable<QuestionDetailItem> Items { get; set; }
        /// <summary>
        /// 회원 정보
        /// </summary>
        public QuestionDetailMember MemberInfo { get; set; }
    }

    public class QuestionDetailMember
    {
        /// <summary>
        /// 회원고유번호
        /// </summary>
        [DbColumn("mem_seq")]
        public int Seq { get; set; }
        /// <summary>
        /// 회원 메일주소
        /// </summary>
        [DbColumn("mem_email")]
        public string Email { get; set; }
        /// <summary>
        /// 회원 프로필 이미지 주소
        /// </summary>
        [DbColumn("mem_profile_img")]
        public string ProfileImage { get; set; }
        /// <summary>
        /// 회원 별명
        /// </summary>
        [DbColumn("mem_nick_nm")]
        public string NickName { get; set; }
        /// <summary>
        /// 회원 로그인 종류
        /// </summary>
        [DbColumn("mem_type")]
        public string LoginType { get; set; }
        /// <summary>
        /// 회원 최종 로그인 시간
        /// </summary>
        [DbColumn("mem_latest_dt")]
        public DateTime LastLoginDatetime { get; set; }
        /// <summary>
        /// 관리자 미확인 신규 문의 글 수
        /// </summary>
        [DbColumn("admin_unread_cnt")]
        public int AdminUnreadCount { get; set; }
    }

    public class QuestionDetailItem
    {
        /// <summary>
        /// 일련번호
        /// </summary>
        [DbColumn("seq")]
        public int Seq { get; set; }
        /// <summary>
        /// 문의내용
        /// </summary>
        [DbColumn("content")]
        public string Content { get; set; }
        /// <summary>
        /// 등록일시
        /// </summary>
        [DbColumn("reg_dt")]
        public DateTime RegDateTime { get; set; }
        /// <summary>
        /// 푸시 발송완료 여부
        /// </summary>
        [DbColumn("push_yn")]
        public string PushYN { get; set; }
        /// <summary>
        /// 댓글목록
        /// </summary>
        [DbColumn(Ignore = true)]
        public IEnumerable<QuestionDetailReply> Reply { get; set; }
    }

    public class QuestionDetailReply
    {
        /// <summary>
        /// 일련번호
        /// </summary>
        [DbColumn("seq")]
        public int Seq { get; set; }
        /// <summary>
        /// 문의글 고유번호
        /// </summary>
        [DbColumn("question_seq")]
        public int QuestionSeq { get; set; }
        /// <summary>
        /// 푸시여부
        /// </summary>
        [DbColumn("push_yn")]
        public string PushYN { get; set; }
        /// <summary>
        /// 등록일시
        /// </summary>
        [DbColumn("reg_dt")]
        public DateTime RegDateTime { get; set; }
        
        /// <summary>
        /// 답변 종류 (0 : 텍스트 / 1 : 외부링크 / 2 : 내부링크)
        /// </summary>
        [DbColumn("reply_type")]
        public int ReplyType { get; set; }
        /// <summary>
        /// 답변내용 (텍스트)
        /// </summary>
        [DbColumn("reply_content")]
        public string ReplyContent { get; set; }

        /// <summary>
        /// 외부링크 주소
        /// </summary>
        [DbColumn("link_external_url")]
        public string LinkExternalUrl { get; set; }
        /// <summary>
        /// 외부링크 도메인
        /// </summary>
        [DbColumn("link_external_domain")]
        public string LinkExternalDomain { get; set; }
        /// <summary>
        /// 외부링크 썸네일
        /// </summary>
        [DbColumn("link_external_thumbnail")]
        public string LinkExternalThumbnail { get; set; }
        /// <summary>
        /// 외부링크 타이틀
        /// </summary>
        [DbColumn("link_external_title")]
        public string LinkExternalTitle { get; set; }

        /// <summary>
        /// 내부링크 프로덕트 고유번호
        /// </summary>
        [DbColumn("link_internal_seq")]
        public int LinkInternalSeq { get; set; }
        /// <summary>
        /// 내부링크 프로덕트 제목
        /// </summary>
        [DbColumn("link_internal_title")]
        public string LinkInternalTitle { get; set; }
        /// <summary>
        /// 내부링크 프로덕트 이미지
        /// </summary>
        [DbColumn("link_internal_image")]
        public string LinkInternalImage { get; set; }
        /// <summary>
        /// 내부링크 프로덕트 카테고리명
        /// </summary>
        [DbColumn("link_internal_category_name")]
        public string LinkInternalCategoryName { get; set; }
        /// <summary>
        /// 내부링크 프로덕트 서비스 노출일시
        /// </summary>
        [DbColumn("link_internal_service_dt")]
        public DateTime LinkInternalServiceDatetime { get; set; }
    }
}
