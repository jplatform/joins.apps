﻿using Joins.Apps.Common.JsonConverter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Sully {
    #region [댓글 관리]
    public class CommentListParam {
        public int pgi { get; set; } = 1;
        public int ps { get; set; } = 20;
        public int pdseq { get; set; } = 0;
        public int mseq { get; set; } = 0;
        public string status { get; set; } = "";
        public string search { get; set; } = "";
        public string order { get; set; } = "";
        public string sdt { get; set; } = DateTime.Now.AddDays(-2).ToString("yyyy-MM-dd");
        public string edt { get; set; } = DateTime.Now.ToString("yyyy-MM-dd");
    }
    /// <summary>
    /// 댓글 리스트
    /// </summary>
    public class CommentList {
        public int rownum { get; set; }
        public int cmt_seq { get; set; }
        public int pd_seq { get; set; }
        public string pd_title { get; set; }
        public int mem_seq { get; set; }
        public string mem_nick_nm { get; set; }
        public string mem_type { get; set; }
        public string cmt_status { get; set; }
        public string cmt_content { get; set; }
        public int cmt_like_cnt { get; set; }
        public string reg_ip { get; set; }
        public DateTime reg_dt { get; set; }
        public string mod_seq { get; set; }
        public DateTime? mod_dt { get; set; }
    }
    #endregion

    #region [댓글사용자,IP 차단관리]
    public class BannedListParam {
        public int pgi { get; set; } = 1;
        public int ps { get; set; } = 20;
        public int mseq { get; set; } = 0;
        public string status { get; set; } = "";
        public string search { get; set; } = "";
        public string sdt { get; set; } = DateTime.Now.AddMonths(-1).ToString("yyyy-MM-dd");
        public string edt { get; set; } = DateTime.Now.ToString("yyyy-MM-dd");
    }
    public class BannedList {
        public int rownum { get; set; }
        public int seq { get; set; }
        public string bnd_type { get; set; }
        public int bnd_days { get; set; }
        public int mem_seq { get; set; }
        public string mem_nick_nm { get; set; }
        public string mem_type { get; set; }
        public string bnd_yn { get; set; }
        public string bnd_ip { get; set; }
        public string bnd_desc { get; set; }
        public DateTime reg_dt { get; set; }
        public DateTime? mod_dt { get; set; }
    }
    public class BlockWordList {
        public int ROWNUM { get; set; }
        public int SEQ { get; set; }
        public string USED_YN { get; set; }
        public string WORD { get; set; }
        public DateTime REG_DT { get; set; }
    }
    #endregion

    public class CommentReportList
    {
        /// <summary>
        /// 행 번호
        /// </summary>
        public int rownum { get; set; }
        /// <summary>
        /// 프로덕트 고유번호
        /// </summary>
        public int pd_seq { get; set; }
        /// <summary>
        /// 프로덕트 제목
        /// </summary>
        public string pd_title { get; set; }
        /// <summary>
        /// 댓글 고유번호
        /// </summary>
        public int cmt_seq { get; set; }
        /// <summary>
        /// 댓글 작성자 닉네임
        /// </summary>
        public string mem_nick_nm { get; set; }
        /// <summary>
        /// 댓글 내용
        /// </summary>
        public string cmt_content { get; set; }
        /// <summary>
        /// 신고 횟수
        /// </summary>
        public int report_cnt { get; set; }
        /// <summary>
        /// 댓글 등록일시
        /// </summary>
        public DateTime reg_dt { get; set; }
    }

    public class CommentReportListParam
    {
        public int pgi { get; set; } = 1;
        public int ps { get; set; } = 20;
        public int total_count { get; set; } = 0;
    }

    public class CommentReportInfo
    {
        /// <summary>
        /// 신고당한 댓글 정보
        /// </summary>
        public Comment ReportCommentInfo { get; set; }
        /// <summary>
        /// 신고 목록
        /// </summary>
        public IEnumerable<Report> ReportList { get; set; }

        public class Comment
        {
            /// <summary>
            /// 프로덕트 고유번호
            /// </summary>
            public int pd_seq { get; set; }
            /// <summary>
            /// 프로덕트 제목
            /// </summary>
            public string pd_title { get; set; }
            /// <summary>
            /// 댓글 고유번호
            /// </summary>
            public int cmt_seq { get; set; }
            /// <summary>
            /// 댓글 내용
            /// </summary>
            public string cmt_content { get; set; }
            /// <summary>
            /// 댓글 작성일시
            /// </summary>
            [JsonConverter(typeof(JsonDateFormatConverter), "{0:yyyy.MM.dd HH:mm}")]
            public DateTime reg_dt { get; set; }
        }

        public class Report
        {
            /// <summary>
            /// 행번호
            /// </summary>
            public int rownum { get; set; }
            /// <summary>
            /// 신고 회원고유번호
            /// </summary>
            public int mem_seq { get; set; }
            /// <summary>
            /// 신고 회원 닉네임
            /// </summary>
            public string report_mem_nick_nm { get; set; }
            /// <summary>
            /// 신고회원 로그인 타입
            /// </summary>
            public string report_mem_type { get; set; }
            /// <summary>
            /// 신고회원 썸네일
            /// </summary>
            public string report_mem_image { get; set; }
            /// <summary>
            /// 신고사유
            /// </summary>
            public string report_comment { get; set; }
            /// <summary>
            /// 처리상태
            /// </summary>
            public string report_status { get; set; }
            /// <summary>
            /// 처리상태 텍스트
            /// </summary>
            public string report_status_text { get; set; }
            /// <summary>
            /// 신고일시
            /// </summary>
            [JsonConverter(typeof(JsonDateFormatConverter), "{0:yyyy.MM.dd HH:mm}")]
            public DateTime report_reg_dt { get; set; }
        }
    }
}