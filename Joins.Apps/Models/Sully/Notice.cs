﻿using Joins.Apps.Repository.Common.Attributes;
using System;

namespace Joins.Apps.Models.Sully
{
    public class Notice
    {
        /// <summary>
        /// 고유번호
        /// </summary>
        [DbColumn("seq")]
        public int Seq { get; set; }
        /// <summary>
        /// 제목
        /// </summary>
        [DbColumn("title")]
        public string Title { get; set; }
        /// <summary>
        /// 내용
        /// </summary>
        [DbColumn("content")]
        public string Content { get; set; }
        /// <summary>
        /// 등록일시
        /// </summary>
        [DbColumn("reg_dt")]
        public DateTime? RegDate { get; set; }
    }
}
