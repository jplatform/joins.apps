﻿using Joins.Apps.Common.JsonConverter;
using Joins.Apps.Repository.Common.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Sully
{
    public class ShowcaseSearchProduct
    {
        [DbColumn("pd_seq")]
        [JsonProperty("seq")]
        public int PD_SEQ { get; set; }
        [DbColumn("pd_title")]
        [JsonProperty("title")]
        public string PD_TITLE { get; set; }
        [DbColumn("pd_img_url")]
        [JsonProperty("img_url")]
        public string PD_IMG_URL { get; set; }
        [DbColumn("pd_provider_url")]
        [JsonProperty("provider_img_url")]
        public string PD_PROVIDER_URL { get; set; }
        [DbColumn("pd_category_nm")]
        [JsonProperty("category_name")]
        public string PD_CATEGORY_NM { get; set; }
        [DbColumn("pd_corner_type")]
        [JsonProperty("corner_type")]
        public string PD_CORNER_TYPE { get; set; }
        [DbColumn("pd_service_dt")]
        [JsonProperty("service_date")]
        [JsonConverter(typeof(JsonDateFormatConverter), "{0:yyyy.MM.dd HH:mm:ss}")]
        public DateTime PD_SERVICE_DT { get; set; }
    }
}
