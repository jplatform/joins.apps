﻿using Joins.Apps.Common.JsonConverter;
using Joins.Apps.Repository.Common.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Sully
{
    public class ShowcaseSearchNotice
    {
        [DbColumn("seq")]
        [JsonProperty("seq")]
        public int SEQ { get; set; }
        [DbColumn("title")]
        [JsonProperty("title")]
        public string TITLE { get; set; }
        [DbColumn("reg_dt")]
        [JsonProperty("reg_date")]
        [JsonConverter(typeof(JsonDateFormatConverter), "{0:yyyy.MM.dd HH:mm:ss}")]
        public DateTime REG_DT { get; set; }
    }
}
