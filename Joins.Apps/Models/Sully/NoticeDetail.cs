﻿using Joins.Apps.Repository.Common.Attributes;
using System;

namespace Joins.Apps.Models.Sully
{
    public class NoticeDetail
    {
        /// <summary>
        /// 일련번호
        /// </summary>
        [DbColumn("seq")]
        public int Seq { get; set; }
        /// <summary>
        /// 제목
        /// </summary>
        [DbColumn("title")]
        public string Title { get; set; }
        /// <summary>
        /// 내용
        /// </summary>
        [DbColumn("content")]
        public string Content { get; set; }
        /// <summary>
        /// 사용여부
        /// </summary>
        [DbColumn("use_yn")]
        public string UseYN { get; set; }
        /// <summary>
        /// 푸시사용여부
        /// </summary>
        [DbColumn("push_yn")]
        public string PushYN { get; set; }
        /// <summary>
        /// 등록일시
        /// </summary>
        [DbColumn("reg_dt")]
        public DateTime RegDateTime { get; set; }
        /// <summary>
        /// 수정일시
        /// </summary>
        [DbColumn("mod_dt")]
        public DateTime ModifyDateTime { get; set; }
        /// <summary>
        /// 푸시발송용 제목
        /// </summary>
        [DbColumn("push_title")]
        public string PushTitle { get; set; }
    }
}
