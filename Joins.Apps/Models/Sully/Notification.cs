﻿using Joins.Apps.Repository.Common.Attributes;
using System;

namespace Joins.Apps.Models.Sully
{
    public class Notification
    {
        /// <summary>
        /// 알림종류 코드
        /// </summary>
        [DbColumn("noti_cd")]
        public string NotifyCode { get; set; }
        /// <summary>
        /// 대상 링크 고유번호
        /// </summary>
        [DbColumn("link_seq")]
        public int LinkSeq { get; set; }
        /// <summary>
        /// 대상 세부링크 고유번호
        /// </summary>
        [DbColumn("LINK_SUB_SEQ")]
        public int? LinkSubSeq { get; set; }
        /// <summary>
        /// 읽음여부
        /// </summary>
        [DbColumn("read_yn")]
        public string ReadYN { get; set; }
        /// <summary>
        /// 삭제여부
        /// </summary>
        [DbColumn("del_yn")]
        public string DelYN { get; set; }
        /// <summary>
        /// 등록일시
        /// </summary>
        [DbColumn("reg_dt")]
        public DateTime? RegDateTime { get; set; }
        /// <summary>
        /// 알림내용
        /// </summary>
        [DbColumn("noti_message")]
        public string Message { get; set; }
        /// <summary>
        /// 썸네일 이미지 주소
        /// </summary>
        [DbColumn("thumbnail_url")]
        public string ThumbnailUrl { get; set; }
    }
}
