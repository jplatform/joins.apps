﻿using Joins.Apps.Repository.Common.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Sully
{
    public class SSullyMainLayoutCMS
    {
        [DbColumn("layout_type")]
        [JsonProperty("layout_type")]
        public string LayoutType { get; set; }
        [DbColumn("product_count")]
        [JsonProperty("product_count")]
        public int? ProductCount { get; set; }
        [DbColumn("ord_num")]
        [JsonProperty("order_number")]
        public int OrderNumber { get; set; }
    }

    public class SSullyMainLayoutBannerCMS
    {
        [DbColumn("rownum")]
        public string Rownum { get; set; }
        [DbColumn("pd_seq")]
        public string PDSeq { get; set; }
        [DbColumn("pd_title")]
        public string PDTitle { get; set; }
        [DbColumn("pd_service_status")]
        public string PDServiceStatus { get; set; }
        [DbColumn("pd_service_dt")]
        public DateTime PDServiceDT { get; set; }
        [DbColumn("background_color")]
        public string BackgroundColor { get; set; }
        [DbColumn("image_url")]
        public string ImageUrl { get; set; }
        [DbColumn("reservation_dt")]
        public DateTime ReservationDT { get; set; }
        [DbColumn("reservation_end_dt")]
        public DateTime ReservationEndDT { get; set; }
        [DbColumn("show")]
        public string Show { get; set; }
    }
}
