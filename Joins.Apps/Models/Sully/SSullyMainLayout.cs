﻿using Joins.Apps.Repository.Common.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Sully
{
    public class SSullyMainLayout
    {
        [DbColumn("layout_type")]
        public string LayoutType { get; set; }
        [DbColumn("pd_seq")]
        public int? PdSeq { get; set; }
        [DbColumn("product_count")]
        public int? ProductCount { get; set; }
        [DbColumn("background_color")]
        public string BackgroundColor { get; set; }
        [DbColumn("image_url")]
        public string ImageUrl { get; set; }
        [DbColumn("ord_num")]
        public int OrderNumber { get; set; }
    }
}
