﻿using Joins.Apps.Common.JsonConverter;
using Joins.Apps.Repository.Common.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Sully
{
    public class SearchRanking
    {
        [JsonProperty("ranking")]
        [DbColumn("ranking")]
        public int Ranking { get; set; }
        [JsonProperty("word")]
        [DbColumn("word")]
        public string Word { get; set; }
        [JsonProperty("count")]
        [DbColumn("search_cnt")]
        public int SearchCount { get; set; }
        [JsonIgnore]
        [DbColumn("analyze_dt")]
        public DateTime AnalyzeDate { get; set; }
    }
}
