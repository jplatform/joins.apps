﻿using Joins.Apps.Repository.Common.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Sully
{
    [Obsolete]
    public class SSullyWidgetPosition
    {
        /// <summary>
        /// 위젯이름
        /// </summary>
        [JsonIgnore]
        [DbColumn("widget_name")]
        public string WidgetName { get; set; }
        /// <summary>
        /// 리스트 노출 위치
        /// </summary>
        [JsonProperty("position_number")]
        [DbColumn("position")]
        public int Position { get; set; }
        /// <summary>
        /// 사용여부
        /// </summary>
        [JsonProperty("visible")]
        [DbColumn("use_yn")]
        public string UseYN { get; set; }
        /// <summary>
        /// 링크 종류 ( 0 : 프로덕트 / 1 : 공지사항)
        /// </summary>
        [JsonIgnore]
        [DbColumn("link_type")]
        public int LinkType { get; set; }
        /// <summary>
        /// 링크 종류 ( 0 : 프로덕트 / 1 : 공지사항)
        /// </summary>
        [JsonProperty("link_type_text")]
        [DbColumn("link_type_desc")]
        public string LinkTypeText { get; set; }
        /// <summary>
        /// 링크 내부 데이터 고유번호
        /// </summary>
        [JsonProperty("link_seq")]
        [DbColumn("link_seq")]
        public int LinkSeq { get; set; }
        /// <summary>
        /// 이미지 경로
        /// </summary>
        [JsonProperty("image_url")]
        [DbColumn("image_url")]
        public string ImageUrl { get; set; }
        /// <summary>
        /// 배경색
        /// </summary>
        [JsonProperty("color")]
        [DbColumn("color")]
        public string Color { get; set; }
        /// <summary>
        /// 프로덕트 제목
        /// </summary>
        [JsonIgnore]
        [DbColumn("product_title")]
        public string ProductTitle { get; set; }
    }
}
