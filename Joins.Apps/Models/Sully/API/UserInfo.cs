﻿using Joins.Apps.Common.JsonConverter;
using Joins.Apps.Repository.Common.Attributes;
using Newtonsoft.Json;
using System;

namespace Joins.Apps.Models.Sully.API
{
    public class UserInfo
    {
        /// <summary>
        /// 회원 고유번호
        /// </summary>
        [JsonProperty("mem_seq")]
        [DbColumn("mem_seq")]
        public int MemSeq { get; set; }
        /// <summary>
        /// 이메일주소
        /// </summary>
        [JsonProperty("email")]
        [DbColumn("mem_email")]
        public string Email { get; set; }
        /// <summary>
        /// 프로필 이미지 주소
        /// </summary>
        [JsonProperty("profile_img_url")]
        [DbColumn("mem_profile_img")]
        public string ProfileImgUrl { get; set; }
        /// <summary>
        /// 닉네임
        /// </summary>
        [JsonProperty("nick_name")]
        [DbColumn("mem_nick_nm")]
        public string NickName { get; set; }
        /// <summary>
        /// 성별(M/F)
        /// </summary>
        [JsonProperty("gender")]
        [DbColumn("mem_gender")]
        public string Gender { get; set; }
        /// <summary>
        /// 연령대
        /// </summary>
        [JsonProperty("age")]
        [DbColumn("mem_age")]
        public string Age { get; set; }
        /// <summary>
        /// 푸시 수신여부
        /// </summary>
        [JsonProperty("enable_push")]
        [DbColumn("mem_app_push")]
        public string EnablePush { get; set; }
        /// <summary>
        /// 가입일시
        /// </summary>
        [JsonProperty("reg_dt")]
        [DbColumn("mem_reg_dt")]
        [JsonConverter(typeof(JsonDateFormatConverter), "{0:yyyy.MM.dd}")]
        public DateTime? RegDT { get; set; }
        /// <summary>
        /// 최종로그인 일시
        /// </summary>
        [JsonProperty("latest_login_dt")]
        [DbColumn("mem_latest_dt")]
        [JsonConverter(typeof(JsonDateFormatConverter), "{0:yyyy.MM.dd}")]
        public DateTime? LatestLoginDT { get; set; }
        /// <summary>
        /// SNS 이름
        /// </summary>
        [JsonProperty("name")]
        [DbColumn("mem_name")]
        public string Name { get; set; }
        /// <summary>
        /// 디바이스 푸시 키
        /// </summary>
        [JsonProperty("device_push_key")]
        [DbColumn("device_push_key")]
        public string DevicePushKey { get; set; }
        /// <summary>
        /// Firebase Messaging Token 값
        /// </summary>
        [JsonProperty("fcm_token")]
        [DbColumn("fcm_token")]
        public string FCMToken { get; set; }
        /// <summary>
        /// UID
        /// </summary>
        [JsonProperty("uid")]
        [DbColumn("mem_uid")]
        public string Uid { get; set; }
        /// <summary>
        /// 로그인 타입
        /// </summary>
        [JsonProperty("login_type")]
        [DbColumn("mem_type")]
        public string LoginType { get; set; }
        /// <summary>
        /// 신규회원 여부(Y/N)
        /// </summary>
        [JsonProperty("walkthrough_completed")]
        [DbColumn("walkthrough_completed_yn")]
        public string WalkthroughCompleted { get; set; }
        /// <summary>
        /// 프로모션 레이어 출력여부
        /// </summary>
        [JsonProperty("show_promotion")]
        [DbColumn("show_promotion_yn")]
        public string ShowPromotion { get; set; }

        [JsonProperty("promotion_code")]
        [DbColumn("promotion_code")]
        public string PromotionCode { get; set; }
    }
}
