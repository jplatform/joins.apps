﻿using Joins.Apps.Common.JsonConverter;
using Joins.Apps.Repository.Common.Attributes;
using Newtonsoft.Json;
using System;

namespace Joins.Apps.Models.Sully.API
{
    public class Comment
    {
        public class ItemList
        {
            /// <summary>
            /// 일련번호
            /// </summary>
            [JsonProperty("cmt_seq")]
            [DbColumn("CMT_SEQ")]
            public int CMT_SEQ { get; set; }
            /// <summary>
            /// 기사번호
            /// </summary>
            [JsonIgnore]
            [DbColumn("PD_SEQ")]
            public int PD_SEQ { get; set; }
            /// <summary>
            /// 회원번호
            /// </summary>
            [JsonProperty("m_seq")]
            [DbColumn("MEM_SEQ")]
            public int MEM_SEQ { get; set; }
            /// <summary>
            /// 댓글 상태
            /// </summary>
            [JsonProperty("status")]
            [DbColumn("CMT_STATUS")]
            public string CMT_STATUS { get; set; }
            /// <summary>
            /// 댓글내용
            /// </summary>
            [JsonProperty("content")]
            [DbColumn("CMT_CONTENT")]
            public string CMT_CONTENT { get; set; }
            /// <summary>
            /// 공감지수
            /// </summary>
            [JsonProperty("like_cnt")]
            [DbColumn("CMT_LIKE_CNT")]
            public int CMT_LIKE_CNT { get; set; }
            /// <summary>
            /// 등록IP
            /// </summary>
            [JsonIgnore]
            [DbColumn("REG_IP")]
            public string REG_IP { get; set; }
            /// <summary>
            /// 등록일시
            /// </summary>
            [JsonProperty("reg_dt")]
            [DbColumn("REG_DT")]
            [JsonConverter(typeof(JsonDateDisplayConverter))]
            public DateTime? REG_DT { get; set; }
            /// <summary>
            /// 수정자
            /// </summary>
            [JsonIgnore]
            [DbColumn("MOD_SEQ")]
            public string MOD_SEQ { get; set; }
            /// <summary>
            /// 수정일시
            /// </summary>
            [JsonIgnore]
            [DbColumn("MOD_DT")]
            public DateTime? MOD_DT { get; set; }
            /// <summary>
            /// 별명
            /// </summary>
            [JsonProperty("nick_nm")]
            [DbColumn("MEM_NICK_NM")]
            public string MEM_NICK_NM { get; set; }
            /// <summary>
            /// 프로필 이미지
            /// </summary>
            [JsonProperty("profile_img")]
            [DbColumn("PROFILE_IMG")]
            public string PROFILE_IMG { get; set; }
            /// <summary>
            /// 좋아요선택 여부
            /// </summary>
            [JsonProperty("like_yn")]
            [DbColumn("LIKE_YN")]
            public string LIKE_YN { get; set; }
        }

        /// <summary>
        /// 댓글입력 파라메터
        /// </summary>
        public class Insert
        {
            /// <summary>
            /// 기사번호
            /// </summary>
            public int pd_seq { get; set; }
            /// <summary>
            /// 댓글내용
            /// </summary>
            public string content { get; set; }
            /// <summary>
            /// 페이지당 리스트 수
            /// </summary>
            public int page_size { get; set; }
            /// <summary>
            /// 정렬방식
            /// </summary>
            public string order_by { get; set; } = "new";
        }

        /// <summary>
        /// 댓글삭제 파라메터
        /// </summary>
        public class Delete
        {
            /// <summary>
            /// 댓글번호
            /// </summary>
            public int cmt_seq { get; set; }
            /// <summary>
            /// 기사번호
            /// </summary>
            public int pd_seq { get; set; }
            /// <summary>
            /// 페이지당 리스트 수
            /// </summary>
            public int page_size { get; set; }
            /// <summary>
            /// 정렬방식
            /// </summary>
            public string order_by { get; set; } = "new";
        }

        /// <summary>
        /// 공감/비공감 파라메터
        /// </summary>
        public class Like
        {
            /// <summary>
            /// 댓글번호
            /// </summary>
            public int cmt_seq { get; set; }
            /// <summary>
            /// 댓글내용
            /// </summary>
            public string like_yn { get; set; }
        }

        /// <summary>
        /// 신고하기파라메터
        /// </summary>
        public class Report
        {
            /// <summary>
            /// 댓글번호
            /// </summary>
            public int cmt_seq { get; set; }
            /// <summary>
            /// 신고코드
            /// </summary>
            public string code { get; set; }
            /// <summary>
            /// 신고내용
            /// </summary>
            public string content { get; set; }
        }
    }
}