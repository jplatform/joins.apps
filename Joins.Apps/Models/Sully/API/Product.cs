﻿using Joins.Apps.Common.JsonConverter;
using Joins.Apps.Repository.Common.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Joins.Apps.Models.Sully.API
{
    public class Product
    {
        [JsonProperty("rownum")]
        [DbColumn("rownum")]
        public int RowNum { get; set; }
        /// <summary>
        /// 일련번호
        /// </summary>
        [JsonProperty("seq")]
        [DbColumn("seq")]
        public int Seq { get; set; }
        /// <summary>
        /// 카테고리 코드
        /// </summary>
        [JsonProperty("category")]
        [DbColumn("category")]
        public string Category { get; set; }
        /// <summary>
        /// 코너 종류 코드
        /// </summary>
        [JsonProperty("corner_type")]
        [DbColumn("corner_type")]
        public int CornerType { get; set; }
        /// <summary>
        /// 제목
        /// </summary>
        [JsonProperty("title")]
        [DbColumn("title")]
        public string Title { get; set; }
        /// <summary>
        /// 태그 DB매핑용
        /// </summary>
        [JsonIgnore]
        [DbColumn("tag")]
        public string TagFromDB { get; set; }
        /// <summary>
        /// 태그 JSON 출력용
        /// </summary>
        [JsonProperty("tag")]
        [DbColumn(Ignore = true)]
        public IEnumerable<string> TagToJson { get; set; }
        /// <summary>
        /// 대표 이미지
        /// </summary>
        [JsonProperty("image_url")]
        [DbColumn("image_url")]
        public string ImageUrl { get; set; }
        /// <summary>
        /// 공유용 이미지
        /// </summary>
        [JsonProperty("share_image_url")]
        [DbColumn("share_image_url")]
        public string ShareImageUrl { get; set; }
        /// <summary>
        /// 서비스 노출 시작일
        /// </summary>
        [JsonProperty("service_date")]
        [DbColumn("service_date")]
        [JsonConverter(typeof(JsonDateFormatConverter), "{0:yyyy.MM.dd}")]
        public DateTime? ServiceDate { get; set; }
        /// <summary>
        /// 제공업체
        /// </summary>
        [JsonProperty("provider_url")]
        [DbColumn("provider_url")]
        public string ProviderURL { get; set; }
        /// <summary>
        /// New 아이콘 표시 여부
        /// </summary>
        [JsonProperty("is_new_icon")]
        [DbColumn("is_new_icon")]
        public string IsNewIcon { get; set; }
        /// <summary>
        /// 광고 컨텐트 여부
        /// </summary>
        [JsonProperty("is_ad_product")]
        [DbColumn("is_ad_product")]
        public string IsADProduct { get; set; }
        
        /// <summary>
        /// 썰리퀴즈 이벤트 상태 ( 0 : 이벤트아님 / 1 : 대기 / 2 : 진행중 / 3 : 종료)
        /// </summary>
        [JsonProperty("quiz_event_status")]
        [DbColumn("quiz_event_status")]
        public string QuizEventStatus { get; set; }
        /// <summary>
        /// 퀴즈 참여여부
        /// </summary>
        [JsonProperty("quiz_event_participation")]
        [DbColumn("quiz_event_participation")]
        public string QuizEventParticipationYN { get; set; }
    }
}
