﻿using Joins.Apps.Repository.Common.Attributes;
using Newtonsoft.Json;

namespace Joins.Apps.Models.Sully.API
{
    public class ContentSetting
    {
        /// <summary>
        /// 회원 고유번호
        /// </summary>
        [JsonProperty("mem_seq")]
        [DbColumn("mem_seq")]
        public int MemSeq { get; set; }
        /// <summary>
        /// 애니메이션 재생속도 단계
        /// </summary>
        [JsonProperty("animation")]
        [DbColumn("animation_step")]
        public int Animation { get; set; }
        /// <summary>
        /// 글자 크기 단계
        /// </summary>
        [JsonProperty("font")]
        [DbColumn("font_step")]
        public int Font { get; set; }
        /// <summary>
        /// 애니메이션 사용여부
        /// </summary>
        [JsonProperty("animation_yn")]
        [DbColumn("animation_yn")]
        public string AnimationYN { get; set; }
    }
}
