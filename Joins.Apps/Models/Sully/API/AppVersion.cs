﻿using Joins.Apps.Common.JsonConverter;
using Joins.Apps.Repository.Common.Attributes;
using Newtonsoft.Json;
using System;

namespace Joins.Apps.Models.Sully.API
{
    public class AppVersion
    {
        [JsonIgnore]
        [DbColumn("os_type")]
        public string OSType { get; set; }
        [JsonProperty("version")]
        [DbColumn("app_version")]
        public string Version { get; set; }
        [JsonProperty("required_yn")]
        [DbColumn("required_yn")]
        public string RequiredYN { get; set; }
        [JsonProperty("update_date")]
        [DbColumn("update_dt")]
        [JsonConverter(typeof(JsonDateFormatConverter), "{0:yyyy.MM.dd HH:mm:ss}")]
        public DateTime UpdateDate { get; set; }
    }
}
