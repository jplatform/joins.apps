﻿using Joins.Apps.Repository.Common.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Sully.API
{
    public class UserCategory
    {
        /// <summary>
        /// 회원 고유번호
        /// </summary>
        [JsonIgnore]
        [DbColumn("mem_seq")]
        public int MemSeq { get; set; }
        /// <summary>
        /// 카테고리코드
        /// </summary>
        [JsonProperty("code")]
        [DbColumn("pd_category")]
        public string Category { get; set; }
    }
}
