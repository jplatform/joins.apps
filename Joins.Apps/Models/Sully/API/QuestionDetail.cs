﻿using Joins.Apps.Common.JsonConverter;
using Joins.Apps.Repository.Common.Attributes;
using Newtonsoft.Json;
using System;

namespace Joins.Apps.Models.Sully.API
{
    public enum QuestionDetailType
    {
        TEXT,
        INTERNAL_LINK,
        EXTERNAL_LINK
    }

    public class QuestionDetail
    {
        [JsonProperty("seq")]
        [DbColumn("seq")]
        public int Seq { get; set; }

        [JsonProperty("admin_yn")]
        [DbColumn("admin_yn")]
        public string AdminYN { get; set; }

        [JsonProperty("reg_dt")]
        [DbColumn("reg_dt")]
        [JsonConverter(typeof(JsonDateDisplayConverter))]
        public DateTime RegDateTime { get; set; }

        [DbColumn("reply_type")]
        [JsonIgnore]
        public int ReplyType { get; set; }
        [DbColumn(Ignore = true)]
        [JsonProperty("item_type")]
        public QuestionDetailType ItemType { get; set; }

        [JsonProperty("message")]
        [DbColumn("content")]
        public string Content { get; set; }

        [JsonProperty("link_external_url")]
        [DbColumn("link_external_url")]
        public string LinkExternalUrl { get; set; }
        [JsonProperty("link_external_domain")]
        [DbColumn("link_external_domain")]
        public string LinkExternalDomain { get; set; }
        [JsonProperty("link_external_thumbnail")]
        [DbColumn("link_external_thumbnail")]
        public string LinkExternalThumbnail { get; set; }
        [JsonProperty("link_external_title")]
        [DbColumn("link_external_title")]
        public string LinkExternalTitle { get; set; }

        [JsonProperty("link_internal_seq")]
        [DbColumn("link_internal_seq")]
        public int? LinkInternalSeq { get; set; }
        [JsonProperty("link_internal_category")]
        [DbColumn("link_internal_category")]
        public string LinkInternalCategory { get; set; }
        [JsonProperty("link_internal_corner_type")]
        [DbColumn("link_internal_corner_type")]
        public int? LinkInternalCornerType { get; set; }
        [JsonProperty("link_internal_thumbnail")]
        [DbColumn("link_internal_thumbnail")]
        public string LinkInternalThumbnail { get; set; }
        [JsonProperty("link_internal_title")]
        [DbColumn("link_internal_title")]
        public string LinkInternalTitle { get; set; }
        [JsonProperty("link_internal_service_dt")]
        [DbColumn("link_internal_service_dt")]
        public string LinkInternalServiceDt { get; set; }
        [JsonProperty("link_internal_is_ad")]
        [DbColumn("link_internal_is_ad")]
        public string LinkInternalIsAd { get; set; }
        [JsonProperty("link_internal_url")]
        [DbColumn(Ignore = true)]
        public string LinkInternalUrl { get; set; }

    }
}