﻿using Joins.Apps.Common.JsonConverter;
using Joins.Apps.Repository.Common.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Joins.Apps.Models.Sully.API
{
    public class NotificationList
    {
        /// <summary>
        /// 알림종류 코드
        /// </summary>
        [JsonProperty("notification_code")]
        [DbColumn("noti_cd")]
        public string NotifyCode { get; set; }
        /// <summary>
        /// 대상 링크 고유번호
        /// </summary>
        [JsonProperty("seq")]
        [DbColumn("link_seq")]
        public int LinkSeq { get; set; }
        /// <summary>
        /// 대상 세부링크 고유번호
        /// </summary>
        [JsonProperty("sub_seq")]
        [DbColumn("LINK_SUB_SEQ")]
        public int? LinkSubSeq { get; set; }
        /// <summary>
        /// 읽음여부
        /// </summary>
        [JsonProperty("read_yn")]
        [DbColumn("read_yn")]
        public string ReadYN { get; set; }
        /// <summary>
        /// 삭제여부
        /// </summary>
        [JsonIgnore]
        [DbColumn("del_yn")]
        public string DelYN { get; set; }
        /// <summary>
        /// 등록일시
        /// </summary>
        [JsonProperty("reg_datetime")]
        [DbColumn("reg_dt")]
        [JsonConverter(typeof(JsonDateFormatConverter), "{0:yyyy.MM.dd}")]
        public DateTime? RegDateTime { get; set; }
        /// <summary>
        /// 알림내용
        /// </summary>
        [JsonProperty("message")]
        [DbColumn("noti_message")]
        public string Message { get; set; }
        /// <summary>
        /// 썸네일 이미지 주소
        /// </summary>
        [JsonProperty("thumbnail_url")]
        [DbColumn("thumbnail_url")]
        public string ThumbnailUrl { get; set; }

        [JsonIgnore]
        [DbColumn("custom_value")]
        public string CustomString { get; set; }
        /// <summary>
        /// 추가 데이터
        /// </summary>
        [JsonProperty("custom_data")]
        public Dictionary<string, string> Custom { get; set; }
    }
}
