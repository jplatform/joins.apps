﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Sully.API {
   public class PushSend {
        /// <summary>
        /// 일련번호
        /// </summary>
        public int seq { get; set; }
        /// <summary>
        /// 푸시타입(0:댓글,1:문의,2:컨텐츠,3:공지사항,4:일반알림
        /// </summary>
        public string p_type { get; set; }
        /// <summary>
        /// 회원번호
        /// </summary>
        public string mem_seq { get; set; }
        /// <summary>
        /// 타이틀
        /// </summary>
        public string p_title { get; set; }
        /// <summary>
        /// 내용
        /// </summary>
        public string p_content { get; set; }
        /// <summary>
        /// 이미지
        /// </summary>
        public string p_img { get; set; }
        /// <summary>
        /// 키1
        /// </summary>
        public string p_key1 { get; set; }
        /// <summary>
        /// 키2
        /// </summary>
        public string p_key2 { get; set; }
        /// <summary>
        /// 키3
        /// </summary>
        public string p_key3 { get; set; }
        /// <summary>
        /// push key
        /// </summary>
        public string fcm_token { get; set; }
        /// <summary>
        /// 디바이스 타입(I:IOS,A:android
        /// </summary>
        public string device_type { get; set; }
    }
}
