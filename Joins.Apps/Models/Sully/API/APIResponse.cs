﻿using Newtonsoft.Json;

namespace Joins.Apps.Models.Sully.API
{
    public enum APIErrorResponseType
    {
        /// <summary>
        /// 유효하지 않은 요청입니다.
        /// </summary>
        INVALID_REQUEST,
        /// <summary>
        /// 요청 작업 처리 완료
        /// </summary>
        SUCCESS,
        /// <summary>
        /// 요청 작업 처리 실패
        /// </summary>
        FAIL,
        /// <summary>
        /// MIME 다중 파트 콘텐츠가 아닙니다
        /// </summary>
        INVALID_MIME_MULTIPART_CONTENT,
        /// <summary>
        /// 존재하지 않는 회원 정보 입니다.
        /// </summary>
        USER_NOT_FOUND
    }

    public class APIResponse
    {
        [JsonProperty("ResponseType")]
        public string ResponseType { get; set; }
        [JsonProperty("ResponseCode")]
        public int ResponseCode { get; set; }
        [JsonProperty("Message")]
        public string Message { get; set; }
        [JsonProperty("Result")]
        public object Result { get; set; }

        /// <summary>
        /// 처리 성공 응답 메세지 인스턴스를 반환합니다.
        /// </summary>
        /// <param name="result">전달할 데이터</param>
        /// <returns></returns>
        public static APIResponse OK(object result)
        {
            var code = -1;

            GetErrorInfo(APIErrorResponseType.SUCCESS, out code);

            return new APIResponse
            {
                ResponseType = "OK",
                ResponseCode = code,
                Message = null,
                Result = result
            };
        }

        /// <summary>
        /// 처리 실패 응답 메세지 인스턴스를 반환합니다.
        /// </summary>
        /// <param name="type">오류 형식</param>
        /// <param name="additional_data"></param>
        /// <returns></returns>
        public static APIResponse USER_NOT_FOUND()
        {
            var code = -1;
            var message = string.Empty;

            GetErrorInfo(APIErrorResponseType.USER_NOT_FOUND, out code, out message);

            return new APIResponse
            {
                ResponseType = "ERROR",
                ResponseCode = code,
                Message = message,
                Result = null
            };
        }

        /// <summary>
        /// 처리 실패 응답 메세지 인스턴스를 반환합니다.
        /// </summary>
        /// <param name="message">메세지</param>
        /// <param name="additional_data">전달할 데이터</param>
        /// <returns></returns>
        public static APIResponse FAIL(string message, object additional_data = null)
        {
            var code = -1;

            GetErrorInfo(APIErrorResponseType.FAIL, out code);

            return new APIResponse
            {
                ResponseType = "FAIL",
                ResponseCode = code,
                Message = message,
                Result = additional_data
            };
        }

        /// <summary>
        /// 오류 응답 메세지 인스턴스를 반환합니다.
        /// </summary>
        /// <param name="type">오류 종류</param>
        /// <returns></returns>
        public static APIResponse ERROR(APIErrorResponseType type)
        {
            var code = -1;
            var message = string.Empty;

            GetErrorInfo(type, out code, out message);

            return new APIResponse
            {
                ResponseType = "ERROR",
                ResponseCode = code,
                Message = message,
                Result = null
            };
        }

        private static void GetErrorInfo(APIErrorResponseType type, out int code)
        {
            string message = string.Empty;
            GetErrorInfo(type, out code, out message);
        }

        private static void GetErrorInfo(APIErrorResponseType type, out int code, out string message)
        {
            code = -1;
            message = "알수없는 오류입니다.";
            switch (type)
            {
                case APIErrorResponseType.INVALID_REQUEST:
                    code = 9999;
                    message = "유효하지 않은 요청입니다.";
                    break;
                case APIErrorResponseType.SUCCESS:
                    code = 0;
                    message = null;
                    break;
                case APIErrorResponseType.FAIL:
                    code = -1;
                    message = null;
                    break;
                case APIErrorResponseType.INVALID_MIME_MULTIPART_CONTENT:
                    code = 9998;
                    message = "MIME 다중 파트 콘텐츠가 아닙니다";
                    break;
                case APIErrorResponseType.USER_NOT_FOUND:
                    code = 9997;
                    message = "회원정보를 찾을 수 없습니다";
                    break;
            }
        }
    }
}
