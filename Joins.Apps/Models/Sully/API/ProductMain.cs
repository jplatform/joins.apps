﻿using Joins.Apps.Common.JsonConverter;
using Joins.Apps.Repository.Common.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Joins.Apps.Models.Sully.API
{
    public class ProductMain
    {
        /// <summary>
        /// 아이템 형식
        /// </summary>
        [JsonIgnore]
        [DbColumn("item_type")]
        public string ItemType { get; set; }
        /// <summary>
        /// 일련번호
        /// </summary>
        [JsonProperty("seq")]
        [DbColumn("seq")]
        public int Seq { get; set; }
        /// <summary>
        /// 카테고리 코드
        /// </summary>
        [JsonProperty("category")]
        [DbColumn("category")]
        public string Category { get; set; }
        /// <summary>
        /// 코너 종류 코드
        /// </summary>
        [JsonProperty("corner_type")]
        [DbColumn("corner_type")]
        public int CornerType { get; set; }
        /// <summary>
        /// 제목
        /// </summary>
        [JsonProperty("title")]
        [DbColumn("title")]
        public string Title { get; set; }
        /// <summary>
        /// 태그 DB매핑용
        /// </summary>
        [JsonIgnore]
        [DbColumn("tag")]
        public string TagFromDB { get; set; }
        /// <summary>
        /// 태그 JSON 출력용
        /// </summary>
        [JsonProperty("tag")]
        [DbColumn(Ignore = true)]
        public IEnumerable<string> TagToJson { get; set; }
        /// <summary>
        /// 대표 이미지
        /// </summary>
        [JsonProperty("image_url")]
        [DbColumn("image_url")]
        public string ImageUrl { get; set; }
        /// <summary>
        /// 공유용 이미지
        /// </summary>
        [JsonProperty("share_image_url")]
        [DbColumn("share_image_url")]
        public string ShareImageUrl { get; set; }
        /// <summary>
        /// 서비스 노출 시작일
        /// </summary>
        [JsonProperty("service_date")]
        [DbColumn("service_date")]
        [JsonConverter(typeof(JsonDateFormatConverter), "{0:yyyy.MM.dd}")]
        public DateTime? ServiceDate { get; set; }
        /// <summary>
        /// 제공업체
        /// </summary>
        [JsonProperty("provider_url")]
        [DbColumn("provider_url")]
        public string ProviderURL { get; set; }
        /// <summary>
        /// 라벨 이미지 종류 (1 : 없음 / 2 : 코너 라벨이미지 / 3 : 직접업로드)
        /// </summary>
        [JsonIgnore]
        [DbColumn("provider")]
        public string ProviderType { get; set; }
        /// <summary>
        /// New 아이콘 표시 여부
        /// </summary>
        [JsonProperty("is_new_icon")]
        [DbColumn("is_new_icon")]
        public string IsNewIcon { get; set; }
        /// <summary>
        /// 광고 컨텐트 여부
        /// </summary>
        [JsonProperty("is_ad_product")]
        [DbColumn("is_ad_product")]
        public string IsADProduct { get; set; }
    }
}
