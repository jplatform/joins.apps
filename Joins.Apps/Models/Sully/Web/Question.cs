﻿using Joins.Apps.Repository.Common.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Sully.Web
{
    public class Question
    {
        [DbColumn("seq")]
        public int Seq { get; set; }
        [DbColumn("admin_yn")]
        public string AdminYN { get; set; }
        [DbColumn("reg_dt")]
        public DateTime RegDateTime { get; set; }
        [DbColumn("reply_type")]
        public int ReplyType { get; set; }
        [DbColumn("content")]
        public string Message { get; set; }
        [DbColumn("link_external_url")]
        public string LinkExternalUrl { get; set; }
        [DbColumn("link_external_domain")]
        public string LinkExternalDomain { get; set; }
        [DbColumn("link_external_thumbnail")]
        public string LinkExternalThumbnail { get; set; }
        [DbColumn("link_external_title")]
        public string LinkExternalTitle { get; set; }
        [DbColumn("link_internal_seq")]
        public string LinkInternalSeq { get; set; }
        [DbColumn("link_internal_thumbnail")]
        public string LinkInernalThumbnail { get; set; }
        [DbColumn("link_internal_service_dt")]
        public DateTime? LinkInternalServiceDateTime { get; set; }
        [DbColumn("link_internal_title")]
        public string LinkInternalTitle{ get; set; }
    }
}
