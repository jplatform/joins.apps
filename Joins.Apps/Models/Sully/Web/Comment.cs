﻿using Joins.Apps.Repository.Common.Attributes;
using System;

namespace Joins.Apps.Models.Sully.Web
{
    public class Comment
    {
        /// <summary>
        /// 글 고유번호
        /// </summary>
        [DbColumn("pd_seq")]
        public int PDSeq { get; set; }
        /// <summary>
        /// 코멘트 고유번호
        /// </summary>
        [DbColumn("comment_seq")]
        public int CommentSeq { get; set; }
        /// <summary>
        /// 내용
        /// </summary>
        [DbColumn("content")]
        public string Content { get; set; }
        /// <summary>
        /// 코멘트 상태(Y:사용, N:삭제, S:숨김)
        /// </summary>
        [DbColumn("comment_status")]
        public string CommentStatus { get; set; }
        /// <summary>
        /// 공감 수
        /// </summary>
        [DbColumn("like_count")]
        public int LikeCount { get; set; }
        /// <summary>
        /// 공감 여부
        /// </summary>
        [DbColumn("like_yn")]
        public string LikeYN { get; set; }
        /// <summary>
        /// 작성자 닉네임
        /// </summary>
        [DbColumn("member_nickname")]
        public string MemberNickName { get; set; }
        /// <summary>
        /// 작성자 프로필 이미지 주소
        /// </summary>
        [DbColumn("member_profile_image_url")]
        public string MemberProfileImageUrl { get; set; }
        /// <summary>
        /// 코멘트 작성일시
        /// </summary>
        [DbColumn("write_datetime")]
        public DateTime WriteDateTime { get; set; }
        /// <summary>
        /// 내 코멘트 여부
        /// </summary>
        [DbColumn("my_comment")]
        public string MyComment { get; set; }
    }
}
