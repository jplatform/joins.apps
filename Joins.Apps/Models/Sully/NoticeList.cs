﻿using Joins.Apps.Repository.Common.Attributes;
using System;

namespace Joins.Apps.Models.Sully
{
    public class NoticeList
    {
        /// <summary>
        /// 순서
        /// </summary>
        [DbColumn("rownum")]
        public int RowNum { get; set; }
        /// <summary>
        /// 일련번호
        /// </summary>
        [DbColumn("seq")]
        public int Seq { get; set; }
        /// <summary>
        /// 제목
        /// </summary>
        [DbColumn("title")]
        public string Title { get; set; }
        /// <summary>
        /// 사용여부
        /// </summary>
        [DbColumn("use_yn")]
        public string UseYn { get; set; }
        /// <summary>
        /// 푸시 발송여부
        /// </summary>
        [DbColumn("push_yn")]
        public string PushYn { get; set; }
        /// <summary>
        /// 등록일시
        /// </summary>
        [DbColumn("reg_dt")]
        public DateTime? RegDate { get; set; }
        /// <summary>
        /// 수정일시
        /// </summary>
        [DbColumn("mod_dt")]
        public DateTime? ModifyDate { get; set; }
    }
}
