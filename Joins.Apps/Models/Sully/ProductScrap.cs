﻿using Joins.Apps.Common.JsonConverter;
using Joins.Apps.Repository.Common.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Sully
{
    public class ProductScrap
    {
        /// <summary>
        /// 프로덕트 고유번호
        /// </summary>
        [DbColumn("pd_seq")]
        [JsonProperty("seq")]
        public int ProductSeq { get; set; }
        /// <summary>
        /// 프로덕트 제목
        /// </summary>
        [DbColumn("pd_title")]
        [JsonProperty("title")]
        public string ProductTitle { get; set; }
        /// <summary>
        /// 프로덕트 출고일
        /// </summary>
        [DbColumn("pd_service_dt")]
        [JsonProperty("service_date")]
        [JsonConverter(typeof(JsonDateFormatConverter), "{0:yyyy.MM.dd}")]
        public DateTime? PublishDate { get; set; }
        /// <summary>
        /// 프로덕트 썸네일
        /// </summary>
        [DbColumn("pd_img_url")]
        [JsonProperty("thumbnail")]
        public string Thumbnail { get; set; }
        /// <summary>
        /// 카테고리
        /// </summary>
        [DbColumn("pd_category")]
        [JsonProperty("category")]
        public string Category { get; set; }
        /// <summary>
        /// 코너종류
        /// </summary>
        [DbColumn("pd_type")]
        [JsonProperty("corner_type")]
        public int CornerType { get; set; }
        /// <summary>
        /// 광고여부
        /// </summary>
        [JsonProperty("is_ad_product")]
        [DbColumn("is_ad_product")]
        public string IsADProduct { get; set; }
        /// <summary>
        /// 라벨이미지 (광고인경우에만 노출)
        /// </summary>
        [JsonProperty("provider_url")]
        [DbColumn("PROVIDER_URL")]
        public string ProviderUrl { get; set; }
    }
}
