﻿using Joins.Apps.Common.JsonConverter;
using Joins.Apps.Repository.Common.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Sully
{
    public class AppVersion
    {
        [DbColumn("os_type")]
        public string OS { get; set; }
        [DbColumn("app_version")]
        public string Version { get; set; }
        [DbColumn("update_dt")]
        [JsonConverter(typeof(JsonDateFormatConverter), "{0:yyyy.MM.dd HH:mm:ss}")]
        public DateTime? ModifyDate { get; set; }
        [DbColumn("required_yn")]
        public string RequiredYN { get; set; }
    }
}
