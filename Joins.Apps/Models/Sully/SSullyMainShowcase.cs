﻿using Joins.Apps.Common.JsonConverter;
using Joins.Apps.Repository.Common.Attributes;
using Newtonsoft.Json;
using System;

namespace Joins.Apps.Models.Sully
{
    public class SSullyMainShowcase
    {
        [DbColumn("showcase_type")]
        public int ShowcaseType { get; set; }
        [DbColumn("link_data")]
        public string LinkData { get; set; }
        [DbColumn("title")]
        public string Title { get; set; }
        [DbColumn("thumbnail")]
        public string Thumbnail { get; set; }
        [DbColumn("label")]
        public string Label { get; set; }
        [DbColumn("display_date")]
        public DateTime DisplayDate { get; set; }
        [DbColumn("product_corner_type")]
        public int ProductCornerType { get; set; }
        [DbColumn("product_is_ad_product")]
        public string ProductIsAd { get; set; }
    }
}
