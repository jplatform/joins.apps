﻿using Joins.Apps.Common.JsonConverter;
using Joins.Apps.Repository.Common.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Sully
{
    public class SSullyMainWeeklyRank
    {
        /// <summary>
        /// 랭킹
        /// </summary>
        [JsonProperty("rank")]
        [DbColumn("ranking")]
        public int Rank { get; set; }
        /// <summary>
        /// 기사 고유번호
        /// </summary>
        [JsonProperty("seq")]
        [DbColumn("pd_seq")]
        public int Seq { get; set; }
        /// <summary>
        /// 코너 종류
        /// </summary>
        [JsonProperty("corner_type")]
        [DbColumn("pd_type")]
        public int CornerType { get; set; }
        /// <summary>
        /// 코너 이름
        /// </summary>
        [JsonProperty("corner_name")]
        [DbColumn("CORNER_NAME")]
        public string CornerName { get; set; }
        /// <summary>
        /// 기사 제목
        /// </summary>
        [JsonProperty("title")]
        [DbColumn("pd_title")]
        public string Title { get; set; }
        /// <summary>
        /// 썸네일 주소
        /// </summary>
        [JsonProperty("thumbnail")]
        [DbColumn("pd_img_url")]
        public string Thumbnail { get; set; }
        /// <summary>
        /// 서비스 시작일
        /// </summary>
        [JsonProperty("service_date")]
        [DbColumn("pd_service_dt")]
        [JsonConverter(typeof(JsonDateFormatConverter), "{0:yyyy.MM.dd}")]
        public DateTime ServiceDate { get; set; }
        /// <summary>
        /// 광고 컨텐트 여부
        /// </summary>
        [JsonProperty("is_ad_product")]
        [DbColumn("is_ad_product")]
        public string IsADProduct { get; set; }
        /// <summary>
        /// 연령대 그룹
        /// </summary>
        [JsonIgnore]
        [DbColumn("AGE_GROUP")]
        public int AgeGroup { get; set; }
    }
}
