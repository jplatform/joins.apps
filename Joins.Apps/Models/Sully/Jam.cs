﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Sully
{
    public class Jam
    {
        public class login
        {
            public int code { get; set; }
            public string message { get; set; }
            public loginData data { get; set; }
        }
        public class loginData
        {
            [JsonProperty(PropertyName = "user-token")]
            public string usertoken { get; set; }
        }
        public class userinfo
        {
            public int code { get; set; }
            public string message { get; set; }
            public userinfoData data { get; set; }
        }
        public class userinfoData
        {
            public string userName { get; set; }
            public string userId { get; set; }
            public string workPartSeq { get; set; }
            public string workPartName { get; set; }
        }
    }
}