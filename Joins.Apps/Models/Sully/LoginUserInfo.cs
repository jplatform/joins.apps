﻿using Joins.Apps.Repository.Common.Attributes;
using System;
using System.Collections.Generic;

namespace Joins.Apps.Models.Sully
{
    public class LoginUserInfo
    {
        /// <summary>
        /// 회원 고유번호
        /// </summary>
        [DbColumn("mem_seq")]
        public int MemSeq { get; set; }
        /// <summary>
        /// 이메일주소
        /// </summary>
        [DbColumn("mem_email")]
        public string Email { get; set; }
        /// <summary>
        /// 프로필 이미지 주소
        /// </summary>
        [DbColumn("mem_profile_img")]
        public string ProfileImgUrl { get; set; }
        /// <summary>
        /// 닉네임
        /// </summary>
        [DbColumn("mem_nick_nm")]
        public string NickName { get; set; }
        /// <summary>
        /// 성별(M/F)
        /// </summary>
        [DbColumn("mem_gender")]
        public string Gender { get; set; }
        /// <summary>
        /// 연령대
        /// </summary>
        [DbColumn("mem_age")]
        public string Age { get; set; }
        /// <summary>
        /// 푸시 수신여부
        /// </summary>
        [DbColumn("mem_app_push")]
        public string EnablePush { get; set; }
        /// <summary>
        /// 가입일시
        /// </summary>
        [DbColumn("mem_reg_dt")]
        public DateTime? RegDT { get; set; }
        /// <summary>
        /// 최종로그인 일시
        /// </summary>
        [DbColumn("mem_latest_dt")]
        public DateTime? LatestLoginDT { get; set; }
        /// <summary>
        /// SNS 이름
        /// </summary>
        [DbColumn("mem_name")]
        public string Name { get; set; }
        /// <summary>
        /// UID
        /// </summary>
        [DbColumn("mem_uid")]
        public string Uid { get; set; }
        /// <summary>
        /// 로그인 타입
        /// </summary>
        [DbColumn("mem_type")]
        public string LoginType { get; set; }
        /// <summary>
        /// 신규회원 여부(Y/N)
        /// </summary>
        [DbColumn("walkthrough_completed_yn")]
        public string WalkthroughCompleted { get; set; }
        
        [DbColumn(Ignore = true)]
        public IEnumerable<UserCategory> Category { get; set; }

        public class UserCategory
        {
            [DbColumn("pd_category")]
            public string CategoryCode { get; set; }
            [DbColumn("category_nm")]
            public string CategoryName { get; set; }
        }
    }
}
