﻿using Joins.Apps.Repository.Common.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Sully
{
    public class ProductMain
    {
        /// <summary>
        /// 아이템 형식
        /// </summary>
        [DbColumn("item_type")]
        public string ItemType { get; set; }
        /// <summary>
        /// 일련번호
        /// </summary>
        [DbColumn("seq")]
        public int Seq { get; set; }
        /// <summary>
        /// 카테고리 코드
        /// </summary>
        [DbColumn("category")]
        public string Category { get; set; }
        /// <summary>
        /// 코너 종류 코드
        /// </summary>
        [DbColumn("corner_type")]
        public int CornerType { get; set; }
        /// <summary>
        /// 제목
        /// </summary>
        [DbColumn("title")]
        public string Title { get; set; }
        /// <summary>
        /// 태그 DB매핑용
        /// </summary>
        [DbColumn("tag")]
        public string TagFromDB { get; set; }
        /// <summary>
        /// 태그 JSON 출력용
        /// </summary>
        [DbColumn(Ignore = true)]
        public IEnumerable<string> TagToJson { get; set; }
        /// <summary>
        /// 대표 이미지
        /// </summary>
        [DbColumn("image_url")]
        public string ImageUrl { get; set; }
        /// <summary>
        /// 공유용 이미지
        /// </summary>
        [DbColumn("share_image_url")]
        public string ShareImageUrl { get; set; }
        /// <summary>
        /// 서비스 노출 시작일
        /// </summary>
        [DbColumn("service_date")]
        public DateTime? ServiceDate { get; set; }
        /// <summary>
        /// 제공업체
        /// </summary>
        [DbColumn("provider_url")]
        public string ProviderURL { get; set; }
        /// <summary>
        /// New 아이콘 표시 여부
        /// </summary>
        [DbColumn("is_new_icon")]
        public string IsNewIcon { get; set; }
        /// <summary>
        /// 광고 컨텐트 여부
        /// </summary>
        [DbColumn("is_ad_product")]
        public string IsADProduct { get; set; }
    }
}
