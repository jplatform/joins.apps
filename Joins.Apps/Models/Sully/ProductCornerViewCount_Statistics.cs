﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Joins.Apps.Common.Utilities.ExcelExportHelper;

namespace Joins.Apps.Models.Sully
{
    public class ProductCornerViewCount_Statistics
    {
        [ExcelHeaderTItle(Title = "출고일시")]
        public string pd_service_dt { get; set; }
        [ExcelHeaderTItle(Title = "프로덕트 번호")]
        public int pd_seq { get; set; }
        [ExcelHeaderTItle(Title = "프로덕트 제목")]
        public string pd_title { get; set; }
        [ExcelHeaderTItle(Title = "조회 수")]
        public int view_cnt { get; set; }
        [ExcelHeaderTItle(Ignore = true)]
        public string code_nm { get; set; }
    }
}
