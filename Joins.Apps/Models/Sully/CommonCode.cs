﻿using Joins.Apps.Common.JsonConverter;
using Joins.Apps.Repository.Common.Attributes;
using Newtonsoft.Json;
using System;

namespace Joins.Apps.Models.Sully
{
    public class CommonCode
    {
        /// <summary>
        /// 그룹 코드
        /// </summary>
        [JsonIgnore]
        [DbColumn("group_cd")]
        public string GroupCode { get; set; }
        /// <summary>
        /// 그룹 이름
        /// </summary>
        [JsonIgnore]
        [DbColumn("group_nm")]
        public string GroupName { get; set; }
        /// <summary>
        /// 코드
        /// </summary>
        [JsonProperty("code")]
        [DbColumn("code")]
        public string Code { get; set; }
        /// <summary>
        /// 코드명
        /// </summary>
        [JsonProperty("code_name")]
        [DbColumn("code_nm")]
        public string CodeName { get; set; }
        /// <summary>
        /// 사용여부
        /// </summary>
        [JsonIgnore]
        [DbColumn("use_yn")]
        public string UseYN { get; set; }
        /// <summary>
        /// 정렬순서
        /// </summary>
        [JsonIgnore]
        [DbColumn("ord_num")]
        public int OrderNumber { get; set; }
        /// <summary>
        /// 등록일시
        /// </summary>
        [JsonIgnore]
        [DbColumn("reg_dt")]
        [JsonConverter(typeof(JsonDateFormatConverter), "{0:yyyy.MM.dd}")]
        public DateTime? RegDateTime { get; set; }
    }
}