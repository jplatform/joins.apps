﻿using Joins.Apps.Repository.Common.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Joins.Apps.Models.Sully {
    public enum Pd_ServiceStatus {
        /// <summary>
        /// 대기
        /// </summary>
        R = 0,
        /// <summary>
        /// 예약대기중
        /// </summary>
        A = 1,
        /// <summary>
        /// 서비스중
        /// </summary>
        L = 2,
        /// <summary>
        /// 서비스중지
        /// </summary>
        S = 3,
        /// <summary>
        /// 종료
        /// </summary>
        E = 4
    }
    public class Pd_ServiceStatusUtil : JCube.AF.Util.EnumUtils<Pd_ServiceStatus> {
        public static string[] ECNames = { "대기", "예약대기중", "서비스중", "서비스중지", "종료" };
        public static string GetECName(object p) { try { return ECNames[(int)Pd_ServiceStatusUtil.GetTypeEnum(p.ToString())]; } catch { return ECNames[0]; } }
        public static string GetECName(int i) { try { return ECNames[i]; } catch { return ECNames[0]; } }
        public static System.Web.UI.WebControls.ListItem[] GetECListItems(bool bAll = false) {
            FieldInfo[] myEnumFields = typeof(Pd_ServiceStatus).GetFields();
            int len = 0, tmpi = 0;
            if (bAll) { len = myEnumFields.Length; tmpi = 1; }
            else { len = myEnumFields.Length - 1; tmpi = 0; }
            System.Web.UI.WebControls.ListItem[] litms = new System.Web.UI.WebControls.ListItem[len];

            if (bAll) { litms[0] = new System.Web.UI.WebControls.ListItem("-전체-", ""); }
            foreach (FieldInfo myField in myEnumFields) {
                if (!myField.IsSpecialName && myField.Name.ToLower() != "notset") {
                    int myValue = (int)myField.GetValue(0);
                    litms[tmpi] = new System.Web.UI.WebControls.ListItem(GetECName(myValue), myField.Name);
                    tmpi++;
                }
            }
            return litms;
        }
    }

    [Serializable]
    [XmlRoot("urlset", Namespace = "http://www.sitemaps.org/schemas/sitemap/0.9")]
    public class ProductSitemap
    {
        [XmlElement("url")]
        public List<ProductSitemapItem> url { get; set; }
    }

    public class ProductSitemapItem
    {
        /// <summary>
        /// 고유번호
        /// </summary>
        [DbColumn("loc")]
        [XmlElement("loc")]
        public string Loc { get; set; }
        /// <summary>
        /// 서비스일시
        /// </summary>
        [DbColumn("lastmod")]
        [XmlElement("lastmod")]
        public string Lastmod { get; set; }
    }

    public class ProductRSS {
        /// <summary>
        /// 고유번호
        /// </summary>
        [DbColumn("seq")]
        public int Seq { get; set; }
        /// <summary>
        /// 코너종류
        /// </summary>
        [DbColumn("product_type")]
        public int ProductType { get; set; }
        /// <summary>
        /// 제목
        /// </summary>
        [DbColumn("title")]
        public string Title { get; set; }
        /// <summary>
        /// 내용
        /// </summary>
        [DbColumn("content")]
        public string Content { get; set; }
        /// <summary>
        /// 서비스일시
        /// </summary>
        [DbColumn("service_dt")]
        public DateTime ServiceDate { get; set; }
        /// <summary>
        /// 카테고리명
        /// </summary>
        [DbColumn("category_nm")]
        public string CategoryName { get; set; }
        /// <summary>
        /// 이미지 주소
        /// </summary>
        [DbColumn("image_url")]
        public string ImageUrl { get; set; }
        /// <summary>
        /// 이미지 크기
        /// </summary>
        [DbColumn(Ignore = true)]
        public long ImageLength { get; set; }
    }
    public class Product {
        public int ROWNUM { get; set; }
        public int PDC_SEQ { get; set; }
        public int PD_SEQ { get; set; }
        public string PD_USED_YN { get; set; }
        public int PD_TYPE { get; set; }
        public string PD_CATEGORY { get; set; }
        public string PD_TAG { get; set; }
        public string PD_TITLE { get; set; }
        public string PD_IMG_URL { get; set; }
        public string PD_SERVICE_STATUS { get; set; }
        public string PD_SERVICE_DT { get; set; }
        public string PD_EDITOR_NM { get; set; }
        public string PD_REG_ID { get; set; }
        public string PD_REG_DT { get; set; }
        public string PD_MOD_ID { get; set; }
        public string PD_MOD_DT { get; set; }
        public string PD_SHARE_IMG_URL { get; set; }
        public string PD_SHARE_HONEYSCREEN_IMG_URL { get; set; }
        public string PD_SUMMARY { get; set; }
        public int CR_SEQ_01 { get; set; }
        public string CR_NAME_01 { get; set; }
        public string CR_IMG_01 { get; set; }
        public int CR_SEQ_02 { get; set; }
        public string CR_NAME_02 { get; set; }
        public string CR_IMG_02 { get; set; }
        public int PD_RELATE_SEQ { get; set; }
        public string PD_PROVIDER { get; set; } = "2";
        public string PD_PROVIDER_URL { get; set; }
        public string CATE_NM { get; set; }
        public string CORNER_NM { get; set; }
        public string PUSH_YN { get; set; }
        public string IS_AD_PRODUCT { get; set; }
        public bool IsShowIcon { get; set; } = false;
        public string PD_SHARE_DESCRIPTION { get; set; }


        /// <summary>
        /// 썰리퀴즈 이벤트 여부
        /// </summary>
        public string QUIZ_EVENT_YN { get; set; }
        /// <summary>
        /// 썰리퀴즈 이벤트 시작일
        /// </summary>
        public DateTime? QUIZ_EVENT_START_DT { get; set; }
        /// <summary>
        /// 썰리퀴즈 이벤트 종료일
        /// </summary>
        public DateTime? QUIZ_EVENT_END_DT { get; set; }
        /// <summary>
        /// 썰리퀴즈 당첨자 발표일
        /// </summary>
        public DateTime? QUIZ_EVENT_NOTICE_DT { get; set; }
        /// <summary>
        /// 썰리퀴즈 이벤트 팝업이미지
        /// </summary>
        public string QUIZ_EVENT_POPUP_IMG_URL { get; set; }
        /// <summary>
        /// 썰리퀴즈 경품이미지
        /// </summary>
        public string QUIZ_EVENT_PRIZE_IMG_URL { get; set; }
        /// <summary>
        /// 썰리퀴즈 이벤트 상태 (0 : 대상아님 / 1 : 대기중 / 2 : 진행중 / 3 : 종료)
        /// </summary>
        public int QUIZ_EVENT_STATUS { get; set; }
        /// <summary>
        /// 썰리퀴즈 이벤트 응모 유의사항
        /// </summary>
        public string QUIZ_EVENT_INFO_DESC { get; set; }
        /// <summary>
        /// 퀴즈 참여여부
        /// </summary>
        public string QUIZ_EVENT_PARTICIPATION { get; set; }
        /// <summary>
        /// 총 좋아요 수
        /// </summary>
        public int LIKE_CNT { get; set; }
        /// <summary>
        /// 좋아요 여부
        /// </summary>
        public string LIKE_YN { get; set; }
        /// <summary>
        /// 스크랩 여부
        /// </summary>
        public string SCRAP_YN { get; set; }
    }

    public class ProductPushInfo
    {
        [DbColumn("push_status")]
        public string Status { get; set; }
        [DbColumn("push_title")]
        public string Title { get; set; }
        [DbColumn("push_content")]
        public string Content { get; set; }

        [DbColumn(Ignore = true)]
        public IEnumerable<ProductPushCategory> CategoryList { get; set; }

        public class ProductPushCategory
        {
            [DbColumn("pd_category")]
            public string Category { get; set; }
        }
    }
    public class ProductListParam {
        public int pgi { get; set; } = 1;
        public int ps { get; set; } = 20;
        public string ptype { get; set; } = string.Empty;
        public string pcate { get; set; } = string.Empty;
        public string ptag { get; set; } = string.Empty;
        public string ptitle { get; set; } = string.Empty;
        public string pst { get; set; } = string.Empty;
        public string s_sdt { get; set; } = string.Empty;
        public string s_edt { get; set; } = string.Empty;
        public string r_sdt { get; set; } = string.Empty;
        public string r_edt { get; set; } = string.Empty;
        public string only { get; set; } = string.Empty;
    }
    public class SCProductListParam {
        public int pgi { get; set; } = 1;
        public int ps { get; set; } = 10;
        public int? ptype { get; set; } = null;
        public string pcate { get; set; } = string.Empty;
        public string ptag { get; set; } = string.Empty;
        public string ptitle { get; set; } = string.Empty;
        public string lastdt { get; set; } = string.Empty;
        public string order { get; set; } = string.Empty;
        public string search_word { get; set; } = string.Empty;
    }
    public class ProductParam {
        public int PD_SEQ { get; set; }
		public string PD_TYPE { get; set; }
		public string PD_CATEGORY { get; set; }
		public string PD_TAG { get; set; }
        public string PD_TITLE { get; set; }
        public string PD_IMG_URL { get; set; }
        public string PD_SHARE_IMG_URL { get; set; }
        public string PD_EDITOR_NM { get; set; }
        public int CR_SEQ_01 { get; set; }
        public int CR_SEQ_02 { get; set; }
        public string CR_NAME_01 { get; set; }
        public string CR_NAME_02 { get; set; }
        public string CR_IMG_01 { get; set; }
        public string CR_IMG_02 { get; set; }
        public string PD_PROVIDER { get; set; }
        public string PD_PROVIDER_URL { get; set; }
        public string IS_AD_PRODUCT { get; set; }
        public string PD_SHARE_DESCRIPTION { get; set; }
        public string PD_SHARE_HONEYSCREEN_IMG_URL { get; set; }
        public string QUIZ_EVENT_YN { get; set; }
        public string QUIZ_EVENT_POPUP_IMAGE_URL { get; set; }
        public string QUIZ_EVENT_PRIZE_IMAGE_URL { get; set; }
        public DateTime? QUIZ_EVENT_RANGE_START_DATE { get; set; }
        public DateTime? QUIZ_EVENT_RANGE_END_DATE { get; set; }
        public DateTime? QUIZ_EVENT_NOTICE_DATE { get; set; }
        public string QUIZ_EVENT_INFO_DESC { get; set; }

        public string PUSH_YN { get; set; }
        public string PUSH_TITLE { get; set; }
        public string PUSH_CONTENT { get; set; }
        public IEnumerable<string> PUSH_CATEGORY { get; set; }
        public string PUSH_CATEGORY_XML { get; set; }
    }
    public class ProductContent {
        public int PDC_SEQ { get; set; }
        public int PD_SEQ { get; set; }
        public string PD_TITLE { get; set; }
        public string PD_SERVICE_STATUS { get; set; }
        public string PD_SERVICE_DT { get; set; }
        public string PDC_ORG_CONTENT { get; set; }
        public string PDC_CONTENT { get; set; }
        public string REG_ID { get; set; }
        public string REG_DT { get; set; }
        public string MOD_ID { get; set; }
        public string MOD_DT { get; set; }
        public string TEMP_YN { get; set; }
        public string TEMP_PDC_ORG_CONTENT { get; set; }
        public string TEMP_PDC_CONTENT { get; set; }
        public string TEMP_REG_DT { get; set; }
    }
    public class ProductContentParam {
        public string PD_TEMP { get; set; }
        public int PDC_SEQ { get; set; }
        public int PD_SEQ { get; set; }
        public string PDC_ORG_CONTENT { get; set; }
        public string PDC_CONTENT { get; set; }
    }

    public class APIProduct {
        [DbColumn("pd_seq")]
        public int pseq { get; set; }
        [DbColumn("pd_type")]
        public string ptype { get; set; }
        [DbColumn("pd_category")]
        public string category { get; set; }
        [DbColumn("pd_category_nm")]
        public string categorynm { get; set; }
        [DbColumn("pd_tag")]
        public string tag { get; set; }
        [DbColumn("pd_title")]
        public string ptitle { get; set; }
        [DbColumn("pd_img_url")]
        public string img { get; set; }
        [DbColumn("pd_service_dt")]
        public string servicedt { get; set; }
        [DbColumn("pd_editor_nm")]
        public string editor { get; set; }
        [DbColumn("pd_reg_dt")]
        public string regdt { get; set; }
        [DbColumn(Ignore = true)]
        public string viewurl { get; set; }
        [DbColumn(Ignore = true)]
        public string shareurl { get; set; }
    }
    public class ProductQuestion {
        [JsonProperty("pd_seq")]
        [DbColumn("pd_seq")]
        public int PdSeq { get; set; }
        [JsonProperty("question_seq")]
        [DbColumn("question_seq")]
        public int QuestionSeq { get; set; }
        [JsonProperty("title")]
        [DbColumn("title")]
        public string Title { get; set; }
        [JsonProperty("explanation")]
        [DbColumn("question_desc")]
        public string Explanation { get; set; }
        [JsonProperty("order_number")]
        [DbColumn("ord_num")]
        public int OrderNumber { get; set; }
        [JsonProperty("items")]
        public IEnumerable<Item> Items { get; set; }

        public class Item
        {
            [JsonProperty("pd_seq")]
            [DbColumn("pd_seq")]
            public int PdSeq { get; set; }
            [JsonProperty("question_seq")]
            [DbColumn("question_seq")]
            public int QuestionSeq { get; set; }
            [JsonProperty("item_seq")]
            [DbColumn("item_seq")]
            public int ItemSeq { get; set; }
            [JsonProperty("title")]
            [DbColumn("title")]
            public string Title { get; set; }
            [JsonProperty("correct_yn")]
            [DbColumn("correct_yn")]
            public string CorrectYn { get; set; }
            [JsonProperty("order_number")]
            [DbColumn("ord_num")]
            public int OrderNumber { get; set; }
        }
    }

    public class ProductQuestionApplyResult
    {
        [JsonProperty("total_count")]
        [DbColumn("total_count")]
        public int TotalCount { get; set; }
        [JsonProperty("total_correct_count")]
        [DbColumn("total_correct_count")]
        public int TotalCorrectCount { get; set; }
        [JsonProperty("correct_ratio")]
        [DbColumn("correct_ratio")]
        public int CorrectRatio { get; set; }
        [JsonProperty("grade")]
        [DbColumn("grade")]
        public string Grade { get; set; }
        [JsonProperty("result_share_url")]
        [DbColumn("result_share_url")]
        public string ResultShareUrl { get; set; }
        [JsonProperty("answer_guid")]
        [DbColumn("answer_guid")]
        public string AnswerGuid { get; set; }
    }

    public class ProductQuestionShareDataResult
    {
        [DbColumn("total_count")]
        public int TotalCount { get; set; }
        [DbColumn("correct_count")]
        public int CorrectCount { get; set; }
        [DbColumn("grade")]
        public string Grade { get; set; }
    }

    public class ProductQuestionInfo
    {
        [JsonProperty("seq")]
        [DbColumn("question_seq")]
        public int QuestionSeq { get; set; }
        [JsonProperty("title")]
        [DbColumn("title")]
        public string Title { get; set; }
        [JsonProperty("current_page")]
        [DbColumn("page")]
        public int CurrentPage { get; set; }
        [JsonProperty("total_page")]
        [DbColumn("total_page")]
        public int TotalPage { get; set; }
        [JsonProperty("completed")]
        [DbColumn(Ignore = true)]
        public string Completed { get; set; } = "N";
        [JsonProperty("poll_items")]
        public IEnumerable<ProductQuestionItem> Items { get; set; }

        public class ProductQuestionItem
        {
            [JsonProperty("seq")]
            [DbColumn("item_seq")]
            public int ItemSeq { get; set; }
            [JsonProperty("title")]
            [DbColumn("title")]
            public string Title { get; set; }
            [JsonProperty("selected")]
            [DbColumn(Ignore = true)]
            public string Selected { get; set; } = "N";
        }
    }

    public class ProductQuestionResult
    {
        [JsonProperty("seq")]
        [DbColumn("question_seq")]
        public int QuestionSeq { get; set; }
        [JsonProperty("description")]
        [DbColumn("question_desc")]
        public string QuestionDescription { get; set; }
        [JsonProperty("correct_seq")]
        [DbColumn("correct_item_seq")]
        public int ItemSeq { get; set; }
    }

    public class EventQuizProductInfo
    {
        [DbColumn("pd_seq")]
        public int Seq { get; set; }
        [DbColumn("background_color")]
        public string BackgroundColor { get; set; }
        [DbColumn("image_url")]
        public string ImageUrl { get; set; }
    }
}