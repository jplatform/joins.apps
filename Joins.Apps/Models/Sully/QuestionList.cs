﻿using Joins.Apps.Repository.Common.Attributes;
using System;

namespace Joins.Apps.Models.Sully
{
    public class QuestionList
    {
        [DbColumn("rownum")]
        public int RowNum { get; set; }
        [DbColumn("mem_seq")]
        public int MemberSeq { get; set; }
        [DbColumn("mem_uid")]
        public string MemberUID { get; set; }
        [DbColumn("mem_email")]
        public string MemberEmail { get; set; }
        [DbColumn("mem_nm")]
        public string MemberNickName { get; set; }
        [DbColumn("mem_type")]
        public string LoginType { get; set; }
        [DbColumn("question_dt")]
        public DateTime QuestionDateTime { get; set; }
        [DbColumn("answer_dt")]
        public DateTime? AnswerDateTime { get; set; }
        [DbColumn("is_show_new_flag")]
        public bool IsShowNewFlag { get; set; }
    }
}
