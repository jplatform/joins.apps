﻿using Joins.Apps.Repository.Common.Attributes;
using System;
using System.Collections.Generic;
using static Joins.Apps.Common.Utilities.ExcelExportHelper;

namespace Joins.Apps.Models.Sully
{
    public class ProductQuizEventApplyAllList
    {
        public Product ProductInfo { get; set; }
        public IEnumerable<ApplyUser> List { get; set; }

        public class Product
        {
            [DbColumn("pd_seq")]
            public int Seq { get; set; }
            [DbColumn("pd_title")]
            public string Title { get; set; }
            [DbColumn("pd_service_dt")]
            public DateTime PublishDateTime { get; set; }
        }

        public class ApplyUser
        {
            [DbColumn("rownum")]
            [ExcelHeaderTItle(Title = "번호")]
            public int? RowNum { get; set; }
            [DbColumn("mem_seq")]
            [ExcelHeaderTItle(Title = "로그인 회원 고유번호")]
            public int? MemberSeq { get; set; }
            [DbColumn("user_nm")]
            [ExcelHeaderTItle(Title = "응모자 이름")]
            public string UserName { get; set; }
            [DbColumn("phone")]
            [ExcelHeaderTItle(Title = "응모자 연락처")]
            public string Phone { get; set; }
            [DbColumn("email")]
            [ExcelHeaderTItle(Title = "응모자 메일주소")]
            public string Email { get; set; }
            [DbColumn("require_agree_yn")]
            [ExcelHeaderTItle(Title = "필수항목 동의여부")]
            public string RequiredAgreeYN { get; set; }
            [DbColumn("optional_agree_yn")]
            [ExcelHeaderTItle(Title = "선택항목 동의여부")]
            public string OptionalAgreeYN { get; set; }
            [DbColumn("apply_position")]
            [ExcelHeaderTItle(Title = "응모위치")]
            public string ApplyPosition { get; set; }
            [DbColumn("total_question_count")]
            [ExcelHeaderTItle(Title = "총 문항 수")]
            public int TotalQuestionCount { get; set; }
            [DbColumn("correct_count")]
            [ExcelHeaderTItle(Title = "정답 수")]
            public int CorrectCount { get; set; }
            [DbColumn("apply_dt")]
            [ExcelHeaderTItle(Title = "응모일시", DisplayFormat = "{0:yyyy-MM-dd HH:mm:ss}")]
            public DateTime? ApplyDateTime { get; set; }
            [DbColumn("destruction_dt")]
            [ExcelHeaderTItle(Title = "개인정보파기 예정일", DisplayFormat = "{0:yyyy-MM-dd HH:mm:ss}")]
            public DateTime? DestructionDate { get; set; }
        }
    }
}
