﻿using Joins.Apps.Repository.Common.Attributes;
using System;

namespace Joins.Apps.Models.Sully
{
    public class SSullyMainShowcaseCMS
    {
        [DbColumn("ord_num")]
        public int OrderNumber { get; set; }
        [DbColumn("showcase_type")]
        public string ShowcaseType { get; set; }
        [DbColumn("showcase_data")]
        public string ShowcaseData { get; set; }
        [DbColumn("reg_dt")]
        public DateTime? RegDateTime { get; set; }
        [DbColumn("reservation_dt")]
        public DateTime? ReservationStartDateTime { get; set; }
        [DbColumn("reservation_end_dt")]
        public DateTime? ReservationEndDateTime { get; set; }
        [DbColumn("custom_title")]
        public string CustomTitle { get; set; }
        [DbColumn("custom_thumbnail")]
        public string CustomThumbnail { get; set; }
        [DbColumn("custom_label_image_url")]
        public string CustomLabelImageUrl { get; set; }
        [DbColumn("original_title")]
        public string OriginalTitle { get; set; }
        [DbColumn("original_thumbnail")]
        public string OriginalThumbnail { get; set; }
        [DbColumn("original_label_image_url")]
        public string OriginalLabelImageUrl { get; set; }
        [DbColumn("description_txt")]
        public string DescriptionText { get; set; }
    }
}
