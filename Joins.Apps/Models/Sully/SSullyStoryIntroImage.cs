﻿using Joins.Apps.Repository.Common.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.Sully
{
    public class SSullyStoryIntroImage
    {
        [DbColumn("rownum")]
        public int RowNum { get; set; }
        [DbColumn("seq")]
        public int Seq { get; set; }
        [DbColumn("image_url")]
        public string ImageURL { get; set; }
        [DbColumn("background_color")]
        public string BackgroundColor { get; set; }
        [DbColumn("use_yn")]
        public string UseYN { get; set; }
        [DbColumn("reg_dt")]
        public DateTime RegDateTime { get; set; }
    }
}
