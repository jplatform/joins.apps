﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models.NewsLine {
    public class ArticleData {
        public string id { get; set; }
        public string title { get; set; }
    //    public string summary { get; set; }
     //   public string content { get; set; }
     //   public string image { get; set; }
      //  public string writer { get; set; }
        public string date { get; set; }
    }
    public class TopicInputData {
        public string topic { get; set; }
        public int article_cnt { get; set; }
        public string topic_date { get; set; }

    }
    public class TopicData {
        public int seq { get; set; }
        public string topic_title { get; set; }
        public string topic_date { get; set; }
        public int topic_naver_article_cnt { get; set; }
        public string topic_naver_date { get; set; }
        public string topic_article_data { get; set; }
        public string reg_date { get; set; }
    }
    public class TopicItemData {
        public object topic_data { get; set; }
        public object topic_article_data { get; set; }
    }
    public class TopicArticleData {

        public int topic_seq { get; set; }
        public int total_id { get; set; }
        public string article_title { get; set; }
        public string article_date { get; set; }
        public string reg_date { get; set; }
        public int pre_topic_seq { get; set; }
    }
}
