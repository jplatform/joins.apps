﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins.Apps.Models {
    public class Page<TModel> {
        /// <summary>
        /// 현재 페이지 번호 (기본값 1)
        /// </summary>
        public int PageNum { get; set; } = 1;
        /// <summary>
        /// 총 아이템 갯수 (기본값 0)
        /// </summary>
        public int TotalItemCount { get; set; } = 0;
        /// <summary>
        /// 총 페이지 수 (기본값 0)
        /// </summary>
        public int TotalPage { get; set; } = 0;
        /// <summary>
        /// 화면에 표시할 아이템 갯수 (기본값 10)
        /// </summary>
        public int DisplayItemCount { get; set; } = 10;
        /// <summary>
        /// 화면에 표시할 페이지 수 (기본값 10)
        /// </summary>
        public int DisplayPageCount { get; set; } = 10;
        /// <summary>
        /// 데이터
        /// </summary>
        public IEnumerable<TModel> Data { get; set; }
    }
    /// <summary>
    ///  API 결과
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public class result<TModel> {
        /// <summary>
        /// 성공여부 (기본값 1:성공)
        /// </summary>
        public int r { get; set; } = 1;
        /// <summary>
        /// 총 갯수 (기본값 0)
        /// </summary>
        public int t_cnt { get; set; } = 0;
        /// <summary>
        /// 메시지
        /// </summary>
        public string msg { get; set; } = "";
        /// <summary>
        /// 데이터
        /// </summary>
        public IEnumerable<TModel> Data { get; set; }
    }
}
