﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.API {
    public static class MessageConfig {
        public const string CHECK_ALREADY_PROCEED = "이미 수행한 작업입니다.";
        public const string BAD_PARAMETER = "적합한 요청이 아닙니다.";
        public const string INVALID_OPERATION = "작업을 수행 할 수 없습니다.";
    }
}