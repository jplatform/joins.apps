﻿using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.API {
    public class FilterConfig {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
