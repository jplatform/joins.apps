﻿using System.Net;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Net.Http;

namespace Joins.Apps.API.Filters {
    public class UnhandledExceptionFilter : ExceptionFilterAttribute {
        public override void OnException(HttpActionExecutedContext context) {
            //Logger.Instance.Error(context.Exception);
            Joins.Apps.Common.Logger.LogWriteError("Exception Error",context.Request.RequestUri+"\t"+ context.Exception.ToString());
            throw new HttpResponseException(context.Request.CreateResponse(HttpStatusCode.InternalServerError, new HttpError(MessageConfig.INVALID_OPERATION)));
        }
    }
}