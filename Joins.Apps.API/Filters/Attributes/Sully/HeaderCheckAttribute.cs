﻿using Joins.Apps.Common.Sully;
using Joins.Apps.Models.Sully.API;
using Joins.Apps.Repository.Sully.API;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Joins.Apps.API.Filters.Attributes.Sully
{
    /// <summary>
    /// 헤더의 공통 필수 정보 체크 속성입니다.
    /// 메서드 또는 클래스에 지정할 수 있습니다.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    public class HeaderCheckAttribute : ActionFilterAttribute
    {
        private bool checkActiveUser;

        /// <summary>
        /// 헤더의 공통 필수 정보 체크 속성을 지정합니다.
        /// </summary>
        /// <param name="checkActiveUser">활동중인 회원인지 검사할지 여부</param>
        public HeaderCheckAttribute(bool checkActiveUser = true)
        {
            this.checkActiveUser = checkActiveUser;
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!HeaderValidate(actionContext))
            {
                actionContext.Response = actionContext.Request.CreateResponse(APIResponse.ERROR(APIErrorResponseType.INVALID_REQUEST));
                return;
            }

            if (this.checkActiveUser)
            {
                var hasUserInfo = UserRepository.HasActiveUserInfo(APIHeaderUtil.MemberLoginType, APIHeaderUtil.MemberAuthUniqueID);
                if (!hasUserInfo)
                {
                    actionContext.Response = actionContext.Request.CreateResponse(APIResponse.FAIL("회원정보를 찾을 수 없습니다."));
                    return;
                }
            }
        }

        /// <summary>
        /// 헤더의 필수 요소값을 검사합니다.
        /// </summary>
        /// <param name="actionContext"></param>
        /// <returns></returns>
        private bool HeaderValidate(HttpActionContext actionContext)
        {
            try
            {
                var verifyCheck = !string.IsNullOrEmpty(APIHeaderUtil.DeviceID)
                        && !string.IsNullOrEmpty(APIHeaderUtil.DeviceType)
                        && !string.IsNullOrEmpty(APIHeaderUtil.MemberLoginType)
                        && !string.IsNullOrEmpty(APIHeaderUtil.MemberAuthUniqueID);

                return verifyCheck;
            }
            catch
            {
                return false;
            }
        }

        //public async override Task OnActionExecutedAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        //{
        //    var is_valid = false;
        //    var login_type = APIHeaderUtil.MemberLoginType;

        //    // 구글, 페이스북 idToken 유효성 검증
        //    if ("K".Equals(login_type))
        //    {
        //        // 카카오 token 유효성 검증
        //        is_valid = await VerifyKakaoToken();
        //    }
        //    else if ("N".Equals(login_type))
        //    {
        //        // 네이버 token 유효성 검증
        //        is_valid = await VerifyKakaoToken();
        //    }
        //    else if (new[] { "G", "F" }.Contains(login_type))
        //    {
        //        is_valid = await VerifyFirebaseToken();
        //    }

        //    actionExecutedContext.Response.Content.Headers.Add("token_verify", is_valid ? "Y" : "N");
        //}

        //public async Task<bool> VerifyKakaoToken()
        //{
        //    var result = false;
        //    var token = APIHeaderUtil.MemberAuthToken;

        //    return result;
        //}

        //public async Task<bool> VerifyNaverToken()
        //{
        //    var result = false;
        //    var token = APIHeaderUtil.MemberAuthToken;

        //    return result;
        //}

        ///// <summary>
        ///// Firebase idToken의 유효성을 검증합니다.
        ///// </summary>
        ///// <param name="idToken">idToken</param>
        ///// <returns>유효성 검증여부</returns>
        //public async Task<bool> VerifyFirebaseToken()
        //{
        //    var firebaseProjectId = "ssully-a9fee";
        //    var token = APIHeaderUtil.MemberAuthIdToken;
        //    try
        //    {
        //        // 1. Get Google signing keys
        //        var client = new HttpClient();
        //        client.BaseAddress = new Uri("https://www.googleapis.com/robot/v1/metadata/");
        //        var response = await client.GetAsync("x509/securetoken@system.gserviceaccount.com");

        //        if (!response.IsSuccessStatusCode)
        //            return false;

        //        var x509Data = await response.Content.ReadAsAsync<Dictionary<string, string>>();
        //        var keys = x509Data.Values.Select(x => new X509SecurityKey(new X509Certificate2(Encoding.UTF8.GetBytes(x)))).ToArray();

        //        var tokenHandler = new JwtSecurityTokenHandler();
        //        SecurityToken validatedToken;

        //        // Set the expected properties of the JWT token in the TokenValidationParameters
        //        var principal = tokenHandler.ValidateToken(token, new TokenValidationParameters()
        //        {
        //            ValidAudience = firebaseProjectId,
        //            ValidIssuer = "https://securetoken.google.com/" + firebaseProjectId,
        //            ValidateIssuerSigningKey = true,
        //            IssuerSigningKeys = keys,
        //            ValidateLifetime = true
        //        }, out validatedToken);

        //        var jwt = (JwtSecurityToken)validatedToken;

        //        return APIHeaderUtil.MemberAuthUniqueID.Equals(jwt.Subject);
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}
    }
}