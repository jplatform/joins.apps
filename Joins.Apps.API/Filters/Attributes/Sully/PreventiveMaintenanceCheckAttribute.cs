﻿using Joins.Apps.Models.Sully.API;
using System;
using System.Configuration;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Joins.Apps.API.Filters.Attributes.Sully
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    public class PreventiveMaintenanceCheckAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var is_mode = "Y".Equals(ConfigurationManager.AppSettings["SSully_PreventiveMaintenanceMode"], StringComparison.OrdinalIgnoreCase);

            if (is_mode)
            {
                var start_date = Convert.ToDateTime(ConfigurationManager.AppSettings["SSully_PreventiveMaintenanceStartDate"]);
                var end_date = Convert.ToDateTime(ConfigurationManager.AppSettings["SSully_PreventiveMaintenanceEndDate"]);
                var now_date = DateTime.Now;

                if (now_date.CompareTo(start_date) >= 0 && now_date.CompareTo(end_date) <= 0)
                {
                    var msg = string.Format("서비스 점검을 진행 중입니다.\n이용에 불편을 드려 죄송합니다.\n점검일자 : {0:yyyy.MM.dd(ddd) HH:mm} ~ {1:HH:mm}", start_date, end_date);
                    actionContext.Response = actionContext.Request.CreateResponse(APIResponse.FAIL(msg));
                    return;
                }
            }
        }
    }
}