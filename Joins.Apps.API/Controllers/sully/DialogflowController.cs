﻿using Joins.Apps.API.Filters.Attributes.Sully;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Joins.Apps.API.Controllers.sully
{
    [PreventiveMaintenanceCheck]
    [RoutePrefix("ssully/dialogflow")]
    public class DialogflowController : ApiController
    {
        [Route("")]
        public object Get()
        {
            return NotFound();
        }
    }
}
