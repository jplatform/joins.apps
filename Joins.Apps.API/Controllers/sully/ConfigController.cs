﻿using Joins.Apps.API.Filters.Attributes.Sully;
using Joins.Apps.API.Models.Sully.ConfigController;
using Joins.Apps.Common.Sully;
using Joins.Apps.Models.Sully.API;
using Joins.Apps.Repository.Sully.API;
using System;
using System.Web.Http;

namespace Joins.Apps.API.Controllers.sully
{
    [PreventiveMaintenanceCheck]
    [HeaderCheck]
    [RoutePrefix("ssully/config")]
    public class ConfigController : ApiController
    {
        /// <summary>
        /// 콘텐트 설정을 변경합니다.
        /// </summary>
        /// <param name="animation">애니메이션 재생속도 단계</param>
        /// <param name="font">글자크기 단계</param>
        /// <param name="animation_yn">애니메이션 사용여부</param>
        /// <returns></returns>
        [HttpGet]
        [Route("content")]
        public APIResponse SetContentConfig(int? animation = null, int? font = null, string animation_yn = "Y")
        {
            var result = ConfigRepository.SetContentSetting(APIHeaderUtil.MemberLoginType, APIHeaderUtil.MemberAuthUniqueID, animation, font, animation_yn);

            if (result.IsSuccess)
                return APIResponse.OK("Y");

            return APIResponse.FAIL(result.ResultMessage);
        }

        /// <summary>
        /// 알림 수신여부를 설정합니다.
        /// </summary>
        /// <param name="enable">수신여부 (Y/N)</param>
        /// <returns></returns>
        [HttpGet]
        [Route("push")]
        public APIResponse Push(string enable)
        {
            var result = ConfigRepository.SetPushStatus(APIHeaderUtil.MemberLoginType, APIHeaderUtil.MemberAuthUniqueID, enable.ToUpper());

            if (result.IsSuccess)
                return APIResponse.OK("Y");

            return APIResponse.FAIL(result.ResultMessage);
        }

        /// <summary>
        /// 디바이스 푸시 키를 변경합니다.
        /// </summary>
        /// <param name="post">POST 데이터</param>
        /// <returns></returns>
        [HttpPost]
        [Route("push_key")]
        public APIResponse SetDevicePushKey([FromBody] PushKey data)
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var device_type = APIHeaderUtil.DeviceType;
            var device_id = APIHeaderUtil.DeviceID;
            var device_push_key = string.IsNullOrEmpty(data.Device_push_key) ? null : data.Device_push_key;
            var fcm_token = string.IsNullOrWhiteSpace(data.FCM_Token) ? null : data.FCM_Token;

            if (string.IsNullOrWhiteSpace(fcm_token))
            {
                if ("I".Equals(device_type, StringComparison.OrdinalIgnoreCase))
                    fcm_token = ApnsToFCMConverter.ConvertAPNSToFCM(device_push_key);
                else if ("A".Equals(device_type, StringComparison.OrdinalIgnoreCase))
                    fcm_token = device_push_key;
            }

            var result = ConfigRepository.SetDevicePushKey(login_type, uid, device_id, device_type, device_push_key, fcm_token);

            if (result.IsSuccess)
                return APIResponse.OK("Y");

            return APIResponse.FAIL(result.ResultMessage);
        }
    }
}
