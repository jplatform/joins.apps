﻿using Joins.Apps.API.Filters.Attributes.Sully;
using Joins.Apps.Models.Sully.API;
using Joins.Apps.Repository.Sully;
using System.Linq;
using System.Web.Http;

namespace Joins.Apps.API.Controllers.sully
{
    [PreventiveMaintenanceCheck]
    [RoutePrefix("ssully/code")]
    public class CodeController : ApiController
    {
        /// <summary>
        /// 공통 코드 목록을 반환합니다.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public APIResponse Get()
        {
            var category = CodeRepository.GetUseCategoryList();

            if (category != null)
            {
                var groupCategory = category.GroupBy(x => new { x.GroupCode, x.GroupName }, (key, items) => new
                {
                    group_code = key.GroupCode,
                    group_name = key.GroupName,
                    items = items
                });

                return APIResponse.OK(new { Category = groupCategory });
            }

            return APIResponse.FAIL("카테고리가 없습니다.");
        }
    }
}