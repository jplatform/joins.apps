﻿using System.Web.Http;
using Joins.Apps.Repository.Sully.API;
using Joins.Apps.Models.Sully.API;
using Joins.Apps.API.Filters.Attributes.Sully;
using Joins.Apps.Common.Sully;
using System.Collections.Generic;
using System.Web;
using System.Linq;

namespace Joins.Apps.API.Controllers.sully
{
    [PreventiveMaintenanceCheck]
    [RoutePrefix("ssully/comment")]
    public class CommentController : ApiController
    {
        /// <summary>
        /// 댓글 목록을 검색합니다.
        /// </summary>
        /// <param name="pd_seq">기사 고유번호</param>
        /// <param name="last_seq">불러올 댓글 위치 고유번호</param>
        /// <param name="target_seq">검색할 댓글의 고유번호</param>
        /// <param name="page_size">검색할 갯수</param>
        /// <param name="order_by">정렬방식</param>
        /// <param name="total_count">총 댓글 갯수</param>
        /// <returns>댓글 목록</returns>
        [NonAction]
        private IEnumerable<Comment.ItemList> GetList(int pd_seq, int last_seq, int? target_seq, int page_size, string order_by, out int total_count, out bool is_last_page)
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var total_last_seq = 0;

            var data = CommentRepository.GetCommentList(login_type, uid, pd_seq, last_seq, target_seq, page_size, order_by, out total_count, out total_last_seq);

            is_last_page = true;
            if (data != null)
                is_last_page = data.Where(x => x.CMT_SEQ == total_last_seq).Count() > 0;

            return data;
        }

        /// <summary>
        /// 댓글 목록을 검색합니다.
        /// </summary>
        /// <param name="pd_seq">기사 고유번호</param>
        /// <param name="last_seq">불러올 댓글 위치 고유번호</param>
        /// <param name="target_seq">검색할 댓글의 고유번호</param>
        /// <param name="page_size">검색할 갯수</param>
        /// <param name="order_by">정렬방식 ("like" : 좋아요순 / new : 작성 최신순)</param>
        /// <returns></returns>
        [HeaderCheck]
        [HttpGet]
        [Route("list")]
        public APIResponse GetCommentList(int pd_seq, int last_seq = 0, int? target_seq = null, int page_size = 10, string order_by = "new")
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var total_count = 0;
            var is_last_page = false;

            if (string.IsNullOrEmpty(order_by))
                order_by = "new";

            var result = GetList(pd_seq, last_seq, target_seq, page_size, order_by, out total_count, out is_last_page);

            return APIResponse.OK(new
            {
                List = result ?? new List<Comment.ItemList>(),
                TotalCount = total_count,
                PageSize = page_size,
                IsLastPage = is_last_page ? "Y" : "N"
            });
        }

        [HttpGet]
        [Route("count")]
        public APIResponse GetCommentCount(int seq)
        {
            var comment_count = CommentRepository.GetProductCommentCount(seq);

            return APIResponse.OK(new
            {
                seq = seq,
                comment_count = comment_count
            });
        }

        /// <summary>
        /// 댓글을 작성합니다.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HeaderCheck]
        [HttpPost]
        [Route("insert")]
        public APIResponse PostInsert([FromBody] Joins.Apps.Models.Sully.API.Comment.Insert data)
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var ip = Common.Util.GetClientIPAddress();

            if (string.IsNullOrEmpty(data.content))
                return APIResponse.FAIL("내용을 입력해 주시기 바랍니다.");

            if (data.content.Trim().Length > 500)
                return APIResponse.FAIL("500자 이하로 입력해 주시기 바랍니다.");

            var result = CommentRepository.SetComment(login_type, uid, data.pd_seq, data.content, ip);

            if (result.IsSuccess)
            {
                var total_count = 0;
                var is_last_page = false;

                if (string.IsNullOrEmpty(data.order_by))
                    data.order_by = "new";

                var list = GetList(data.pd_seq, 0, null, data.page_size, data.order_by, out total_count, out is_last_page);

                return APIResponse.OK(new
                {
                    Success = "Y",
                    List = list ?? new List<Comment.ItemList>(),
                    TotalCount = total_count,
                    PageSize = data.page_size,
                    IsLastPage = is_last_page ? "Y" : "N"
                });
            }

            return APIResponse.FAIL(result.ResultMessage, new { Success = "N" });
        }

        /// <summary>
        /// 댓글을 삭제합니다.
        /// </summary>
        /// <param name="message">메세지</param>
        /// <returns></returns>
        [HeaderCheck]
        [HttpPost]
        [Route("delete")]
        public APIResponse PostDelete([FromBody] Joins.Apps.Models.Sully.API.Comment.Delete data)
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var ip = Common.Util.GetClientIPAddress();
            var result = CommentRepository.SetDelete(login_type, uid, data.cmt_seq, ip);

            if (result.IsSuccess)
            {
                var total_count = 0;
                var is_last_page = false;

                if (string.IsNullOrEmpty(data.order_by))
                    data.order_by = "new";

                var list = GetList(data.pd_seq, 0, null, data.page_size, data.order_by, out total_count, out is_last_page);

                return APIResponse.OK(new
                {
                    Success = "Y",
                    List = list ?? new List<Comment.ItemList>(),
                    TotalCount = total_count,
                    PageSize = data.page_size,
                    IsLastPage = is_last_page ? "Y" : "N"
                });
            }

            return APIResponse.FAIL(result.ResultMessage, new { Success = "N" });
        }

        /// <summary>
        /// 댓글 공감을 등록/해제합니다.
        /// </summary>
        /// <param name="message">메세지</param>
        /// <returns></returns>
        [HeaderCheck]
        [HttpPost]
        [Route("like")]
        public APIResponse PostLike([FromBody] Joins.Apps.Models.Sully.API.Comment.Like data)
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var ip = Common.Util.GetClientIPAddress();
            var result = CommentRepository.SetLike(login_type, uid, data.cmt_seq, data.like_yn, ip);

            if (result.IsSuccess)
                return APIResponse.OK("Y");

            return APIResponse.FAIL(result.ResultMessage);
        }

        /// <summary>
        /// 댓글을 신고합니다.
        /// </summary>
        /// <param name="message">메세지</param>
        /// <returns></returns>
        [HeaderCheck]
        [HttpPost]
        [Route("report")]
        public APIResponse PostReport([FromBody] Joins.Apps.Models.Sully.API.Comment.Report data)
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var ip = Common.Util.GetClientIPAddress();
            var result = CommentRepository.SetReportByUser(login_type, uid, data.cmt_seq, data.code, data.content, ip);

            if (result.IsSuccess)
                return APIResponse.OK("Y");

            return APIResponse.FAIL(result.ResultMessage);
        }
    }
}