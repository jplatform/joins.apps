﻿using Joins.Apps.API.Filters.Attributes.Sully;
using Joins.Apps.Common.Sully;
using Joins.Apps.Common.Utilities;
using Joins.Apps.Models.Sully.API;
using Joins.Apps.Repository.Sully.API;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Joins.Apps.API.Controllers.sully
{
    [PreventiveMaintenanceCheck]
    [HeaderCheck]
    [RoutePrefix("ssully/upload")]
    public class UploadController : ApiController
    {
        /// <summary>
        /// 프로필 이미지를 업로드 합니다.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("profile")]
        public async Task<APIResponse> PostUploadProfileImage()
        {
            var message = await UploadFile("profile", "image/gif", "image/png", "image/jpeg");

            if (message.StatusCode == HttpStatusCode.UnsupportedMediaType)
                return APIResponse.FAIL("이미지만 업로드 가능합니다.");

            if (message.StatusCode == HttpStatusCode.OK)
            {
                var imagePath = await message.Content.ReadAsAsync<List<string>>();
                var login_type = APIHeaderUtil.MemberLoginType;
                var uid = APIHeaderUtil.MemberAuthUniqueID;

                var result = UserRepository.SetProfileUrl(login_type, uid, imagePath.FirstOrDefault());

                if (result.IsSuccess)
                    return APIResponse.OK(new { upload_url = imagePath.FirstOrDefault() });
                else
                    return APIResponse.FAIL(result.ResultMessage);
            }

            return APIResponse.FAIL("프로필 이미지 등록에 실패하였습니다.");
        }

        [NonAction]
        private async Task<HttpResponseMessage> UploadFile(string dirName, params string[] allowContentType)
        {
            List<string> savedFilePath = new List<string>();

            // multipart/form-data 여부 체크
            if (!Request.Content.IsMimeMultipartContent())
                return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);

            var tempDir = Path.Combine(ConfigurationManager.AppSettings["FilePath"], "temp");
            var provider = new MultipartFormDataStreamProvider(tempDir);

            if (!Directory.Exists(tempDir))
                Directory.CreateDirectory(tempDir);

            return await Request.Content.ReadAsMultipartAsync(provider).ContinueWith(t =>
            {
                if (t.IsCanceled || t.IsFaulted)
                    return Request.CreateResponse(HttpStatusCode.InternalServerError);

                foreach (var item in provider.FileData)
                {
                    if (!allowContentType.Contains(item.Headers.ContentType.MediaType))
                    {
                        provider.FileData.ToList().ForEach(x => { File.Delete(x.LocalFileName); });
                        return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
                    }

                    try
                    {
                        // guid도 생성된 무작위 파일명 생성
                        var uploadFileName = Guid.NewGuid() + Path.GetExtension(item.Headers.ContentDisposition.FileName.Replace("\"", ""));
                        var ftpUploadPath = string.Format("service/ssully/{0}/{1:yyyy}/{1:MM}/{1:dd}/", dirName, DateTime.Today);
                        // 임시로 생성된 파일명을 실제 파일명으로 치환
                        File.Move(item.LocalFileName, Path.Combine(tempDir, uploadFileName));

                        var result = FTPUtility.Upload(tempDir, uploadFileName, ftpUploadPath, uploadFileName);
                        
                        File.Delete(Path.Combine(tempDir, uploadFileName));

                        if (!result.IsSuccess)
                            return Request.CreateResponse(HttpStatusCode.InternalServerError);

                        savedFilePath.Add(result.AccessURL);
                    }
                    catch
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError);
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, savedFilePath);
            });
        }
    }
}
