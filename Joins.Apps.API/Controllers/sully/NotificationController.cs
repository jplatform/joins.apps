﻿using Joins.Apps.API.Filters.Attributes.Sully;
using Joins.Apps.Common.Sully;
using Joins.Apps.Models.Sully.API;
using Joins.Apps.Repository.Sully.API;
using System.Collections.Generic;
using System.Web.Http;

namespace Joins.Apps.API.Controllers.sully
{
    [PreventiveMaintenanceCheck]
    [HeaderCheck]
    [RoutePrefix("ssully/notification")]
    public class NotificationController : ApiController
    {
        /// <summary>
        /// 알림목록을 검색합니다.
        /// </summary>
        /// <param name="page">페이지 번호</param>
        /// <param name="page_size">페이지당 리스트 수</param>
        /// <returns></returns>
        [HttpGet]
        [Route("list")]
        public APIResponse GetList(int page = 1, int page_size = 8)
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var total_count = 0;
            var total_page = 0;
            var result = NotificationRepository.GetNotifyList(login_type, uid, page, page_size, out total_count, out total_page);

            return APIResponse.OK(new
            {
                List = result ?? new List<NotificationList>(),
                TotalCount = total_count,
                TotalPage = total_page,
                Page = page,
                PageSize = page_size
            });
        }

        /// <summary>
        /// 미확인 알림 갯수를 검색합니다.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("unconfirmed_count")]
        public APIResponse GetUnconfirmedCount()
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;

            return APIResponse.OK(new
            {
                count = NotificationRepository.GetNewNotifyCount(login_type, uid)
            });
        }
    }
}
