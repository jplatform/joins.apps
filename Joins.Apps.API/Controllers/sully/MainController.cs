﻿using Joins.Apps.API.Filters.Attributes.Sully;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Joins.Apps.API.Controllers.sully
{
    [PreventiveMaintenanceCheck]
    public class MainController : ApiController
    {
        #region 앱 - 정보

        [HttpGet]
        [Route("sully/Main/List")]
        public IHttpActionResult List(int pgi = 1, int ps = 4)
        {
            IEnumerable<Joins.Apps.Models.Sully.APIProduct> result;

            try
            {
                result = Joins.Apps.Repository.Sully.ProductRepository.GetNews10APIProductList(pgi, ps);
            }
            catch
            {
                result = null;
            }

            return Ok(new { sully = result });
        }

        #endregion
    }
}