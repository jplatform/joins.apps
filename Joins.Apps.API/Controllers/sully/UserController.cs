﻿using Joins.Apps.API.Filters.Attributes.Sully;
using Joins.Apps.API.Models.Sully.UserController;
using Joins.Apps.Common;
using Joins.Apps.Common.Sully;
using Joins.Apps.Common.Utilities;
using Joins.Apps.Models.Sully.API;
using Joins.Apps.Repository.Sully.API;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;

namespace Joins.Apps.API.Controllers.sully
{
    [PreventiveMaintenanceCheck]
    [RoutePrefix("ssully/user")]
    public class UserController : ApiController
    {
#if DEBUG
        [HttpGet]
        [Route("promotion_remove")]
        public APIResponse RemovePromotion(string uid, string device_id)
        {
            UserRepository.RemovePromotion(uid, device_id);
            return APIResponse.OK(new { result = "Y" });
        }
#endif

        /// <summary>
        /// 탈퇴하지 않은 회원정보를 반환합니다.
        /// </summary>
        /// <param name="login_type"></param>
        /// <param name="uid"></param>
        /// <param name="device_id"></param>
        /// <returns></returns>
        [NonAction]
        private APIResponse GetUserInfoResponse(string login_type, string uid, string device_id)
        {
            var userInfo = UserRepository.GetUserInfo(login_type, uid, device_id);

            if (userInfo != null)
            {
                return APIResponse.OK(new
                {
                    UserInfo = userInfo,
                    Category = UserRepository.GetUserCategory(login_type, uid),
                    ContentSetting = ConfigRepository.GetContentSetting(login_type, uid),
                    UnconfirmedNotificationCount = NotificationRepository.GetNewNotifyCount(login_type, uid)
                });
            }

            return APIResponse.FAIL("회원정보를 찾을 수 없습니다.");
        }

        /// <summary>
        /// 회원정보를 등록합니다.
        /// </summary>
        /// <param name="post">POST 데이터</param>
        /// <returns></returns>
        [HeaderCheck(false)]
        [HttpPost]
        [Route("regist")]
        public APIResponse Regist([FromBody] Regist data)
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var device_id = APIHeaderUtil.DeviceID;
            var device_type = APIHeaderUtil.DeviceType;
            var ip = Common.Util.GetClientIPAddress();

            var email = string.IsNullOrEmpty(data.Email) ? null : data.Email.Trim();
            var name = string.IsNullOrEmpty(data.Name) ? null : data.Name.Trim();
            var profile_url = string.IsNullOrEmpty(data.Profile_Url) ? null : data.Profile_Url.Trim();
            var device_push_key = string.IsNullOrEmpty(data.Device_Push_Key) ? null : data.Device_Push_Key.Trim();
            var fcm_token = string.IsNullOrWhiteSpace(data.FCM_Token) ? null : data.FCM_Token;

            if (string.IsNullOrWhiteSpace(fcm_token))
            {
                if ("I".Equals(device_type, StringComparison.OrdinalIgnoreCase))
                    fcm_token = ApnsToFCMConverter.ConvertAPNSToFCM(device_push_key);
                else if ("A".Equals(device_type, StringComparison.OrdinalIgnoreCase))
                    fcm_token = device_push_key;
            }

            var result = UserRepository.SetLoginUserInfo(login_type.ToUpper(), email, name, profile_url, uid, device_id, device_push_key, fcm_token, device_type, ip);

            if (result.IsSuccess)
                return GetUserInfoResponse(login_type, uid, device_id);

            return APIResponse.FAIL(result.ResultMessage);
        }

        /// <summary>
        /// 회원정보를 검색합니다.
        /// </summary>
        /// <returns></returns>
        [HeaderCheck]
        [HttpGet]
        [Route("info")]
        public APIResponse GetUserInfo()
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var device_id = APIHeaderUtil.DeviceID;
            var isActiveUser = UserRepository.HasActiveUserInfo(login_type, uid);

            if (isActiveUser)
                return GetUserInfoResponse(login_type, uid, device_id);

            return APIResponse.FAIL("등록된 사용자가 존재하지 않습니다");
        }

        /// <summary>
        /// 회원정보를 설정합니다.
        /// </summary>
        /// <param name="data">POST 데이터</param>
        /// <returns></returns>
        [HeaderCheck]
        [HttpPost]
        [Route("info")]
        public async Task<APIResponse> SetUserInfo()
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var device_id = APIHeaderUtil.DeviceID;

            // 파라메터 매핑 및 프로필 이미지 업로드
            var data = new Info();
            var message = await UploadMultipartFormData((formData => {
                // 파일 업로드 전 사전 
                var dictionary = JsonConvert.SerializeObject(formData.AllKeys.ToDictionary(k => k, v => formData[v]));
                data = JsonConvert.DeserializeObject<Info>(dictionary);

                // 별명 유효성 검증
                var nickNameResult = CheckNickname(data.Nick_name);
                if (nickNameResult != null)
                    return APIResponse.FAIL(nickNameResult.Message, new { fail_type = "nick_name", result = GetUserInfoResponse(login_type, uid, device_id).Result  });

                if (!UserRepository.HasActiveUserInfo(login_type, uid))
                    return APIResponse.FAIL("등록된 사용자가 존재하지 않습니다", new { fail_type = "info", result = GetUserInfoResponse(login_type, uid, device_id).Result });

                return null;
            }), "profile", "image/gif", "image/png", "image/jpeg");

            // 폼데이터 검증 실패
            if (message.StatusCode == HttpStatusCode.BadRequest)
                return await message.Content.ReadAsAsync<APIResponse>();

            // 이미지 형식 미지원
            if (message.StatusCode == HttpStatusCode.UnsupportedMediaType)
                return APIResponse.FAIL("업로드할 수 없는 이미지 형식 입니다.", new { fail_type = "image", result = GetUserInfoResponse(login_type, uid, device_id).Result });

            if (message.StatusCode == HttpStatusCode.OK)
            {
                var imagePath = await message.Content.ReadAsAsync<List<string>>();

                var nick_name = string.IsNullOrEmpty(data.Nick_name) ? null : data.Nick_name;
                var gender = string.IsNullOrEmpty(data.Gender) ? null : data.Gender;
                var age = data.Age;
                var profile = imagePath.FirstOrDefault();
                var category_xml = string.Empty;

                // 관심 카테고리 등록
                if (!string.IsNullOrEmpty(data.Category))
                    category_xml = string.Format("<ROOT>{0}</ROOT>", string.Join(string.Empty, data.Category.Split(',').Where(x => !string.IsNullOrEmpty(x)).Select(x => string.Format("<CATEGORY>{0}</CATEGORY>", x.Trim())).ToArray()));

                var imageResult = await message.Content.ReadAsAsync<List<string>>();

                // 회원정보 변경
                var result = UserRepository.SetUserInfo(login_type, uid, nick_name, gender, age, imageResult.FirstOrDefault(), category_xml);

                if (result.IsSuccess)
                    return APIResponse.OK(new { fail_type = string.Empty, result = GetUserInfoResponse(login_type, uid, device_id).Result });
                else
                    return APIResponse.FAIL(result.ResultMessage, new { fail_type = "info", result = GetUserInfoResponse(login_type, uid, device_id).Result });
            }

            return APIResponse.FAIL("이미지 업로드 중 오류가 발생하였습니다.", new { fail_type = "image", result = GetUserInfoResponse(login_type, uid, device_id).Result });
        }

        /// <summary>
        /// 로그아웃을 처리합니다.
        /// </summary>
        /// <returns></returns>
        [HeaderCheck]
        [HttpGet]
        [Route("logout")]
        public APIResponse Logout()
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var device_id = APIHeaderUtil.DeviceID;
            var device_type = APIHeaderUtil.DeviceType;

            var result = UserRepository.SetLogout(login_type, uid, device_id, device_type);

            if (result.IsSuccess)
                return APIResponse.OK("Y");

            return APIResponse.FAIL(result.ResultMessage);
        }

        /// <summary>
        /// 회원탈퇴를 처리합니다.
        /// </summary>
        /// <returns></returns>
        [HeaderCheck]
        [HttpGet]
        [Route("withdraw")]
        public APIResponse Withdraw()
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;

            var result = UserRepository.SetWithdraw(login_type, uid);

            if (result.IsSuccess)
                return APIResponse.OK("Y");

            return APIResponse.FAIL(result.ResultMessage);
        }

        /// <summary>
        /// 워크쓰루에서 필수정보를 설정합니다.
        /// </summary>
        /// <param name="data">파라메터 데이터</param>
        /// <returns></returns>
        [HeaderCheck]
        [HttpPost]
        [Route("walkthrough/nickname")]
        public APIResponse SetWalkthroughNickName([FromBody] Walkthrough data)
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var nickname = data.NickName;

            var nickname_result = CheckNickname(nickname);
            if (nickname_result != null)
                return nickname_result;

            var result = UserRepository.SetWalkthroughNickName(login_type, uid, nickname);

            if (result.IsSuccess)
                return APIResponse.OK("Y");

            return APIResponse.FAIL(result.ResultMessage);
        }

        /// <summary>
        /// 사용가능한 닉네임인지 여부를 체크합니다.
        /// </summary>
        /// <param name="nickname"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("check_nickname")]
        public APIResponse GetNicknameCheck(string nickname)
        {
            var nickname_result = CheckNickname(nickname);
            if (nickname_result != null)
                return nickname_result;

            return APIResponse.OK("Y");
        }

        /// <summary>
        /// 워크쓰루에서 개인추가정보를 설정합니다
        /// </summary>
        /// <param name="data">파라메터 데이터</param>
        /// <returns></returns>
        [HeaderCheck]
        [HttpPost]
        [Route("walkthrough/optional")]
        public APIResponse SetWalkthroughOptionalInfo([FromBody] Walkthrough data)
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;

            var gender = string.IsNullOrEmpty(data.Gender) ? null : data.Gender.ToUpper();
            var pushYN = string.IsNullOrEmpty(data.PushYN) ? null : data.PushYN.ToUpper();
            var age = data.Age;
            var category_xml = string.Empty;

            if (!string.IsNullOrEmpty(data.Category))
                category_xml = string.Format("<ROOT>{0}</ROOT>", string.Join(string.Empty, data.Category.Split(',').Where(x => !string.IsNullOrEmpty(x)).Select(x => string.Format("<CATEGORY>{0}</CATEGORY>", x.Trim())).ToArray()));

            var result = UserRepository.SetWalkthroughOptionalInfo(login_type, uid, gender, age, pushYN, category_xml);

            if (result.IsSuccess)
                return APIResponse.OK("Y");

            return APIResponse.FAIL(result.ResultMessage);
        }

        /// <summary>
        /// 닉네임 사용가능여부를 검사합니다.
        /// </summary>
        /// <param name="nickName"></param>
        /// <returns></returns>
        [NonAction]
        private APIResponse CheckNickname(string nickName)
        {
            if (string.IsNullOrEmpty(nickName) || string.IsNullOrEmpty(nickName.Trim()))
                return APIResponse.FAIL("닉네임을 입력해주세요.");

            // 닉네임은 영한숫자로만 가능 (최대 10자)
            if (!Regex.IsMatch(nickName, "^[0-9a-zA-Zㄱ-힣]{1,10}$"))
                return APIResponse.FAIL("닉네임은 영문, 한글, 숫자로 최대 10자까지 가능합니다.");

            var result = UserRepository.CheckDeniedNickname(nickName);

            if (!result.IsSuccess)
                return APIResponse.FAIL(result.ResultMessage);

            return null;
        }

        /// <summary>
        /// 파일 업로드후 업로드 경로를 반환합니다.
        /// </summary>
        /// <param name="formData">폼 데이터</param>
        /// <param name="dirName">업로드 디렉토리명</param>
        /// <param name="allowContentType">허용 파일종류</param>
        /// <returns></returns>
        [NonAction]
        private async Task<HttpResponseMessage> UploadMultipartFormData(Func<NameValueCollection, APIResponse> checkFormData, string dirName, params string[] allowContentType)
        {
            List<string> savedFilePath = new List<string>();

            // multipart/form-data 여부 체크
            if (!Request.Content.IsMimeMultipartContent())
                return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);

            var tempDir = Path.Combine(ConfigurationManager.AppSettings["FilePath"], "temp");
            var provider = new MultipartFormDataStreamProvider(tempDir);

            if (!Directory.Exists(tempDir))
                Directory.CreateDirectory(tempDir);

            return await Request.Content.ReadAsMultipartAsync(provider).ContinueWith(t =>
            {
                if (t.IsCanceled || t.IsFaulted)
                    return Request.CreateResponse(HttpStatusCode.InternalServerError);

                // 폼 데이터 JSON으로 변환
                var checkFormDataResult = checkFormData(provider.FormData);

                if (checkFormDataResult != null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, checkFormDataResult);

                // 업로드 파일
                foreach (var item in provider.FileData)
                {
                    if (!allowContentType.Contains(item.Headers.ContentType.MediaType))
                    {
                        provider.FileData.ToList().ForEach(x => { File.Delete(x.LocalFileName); });
                        return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
                    }

                    try
                    {
                        // guid도 생성된 무작위 파일명 생성
                        var uploadFileName = Guid.NewGuid() + Path.GetExtension(item.Headers.ContentDisposition.FileName.Replace("\"", ""));
                        var ftpUploadPath = string.Format("service/ssully/{0}/{1:yyyy}/{1:MM}/{1:dd}/", dirName, DateTime.Today);
                        // 임시로 생성된 파일명을 실제 파일명으로 치환
                        File.Move(item.LocalFileName, Path.Combine(tempDir, uploadFileName));

                        var result = FTPUtility.Upload(tempDir, uploadFileName, ftpUploadPath, uploadFileName);

                        File.Delete(Path.Combine(tempDir, uploadFileName));

                        if (!result.IsSuccess)
                            return Request.CreateResponse(HttpStatusCode.InternalServerError);

                        savedFilePath.Add(result.AccessURL);
                    }
                    catch
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError);
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, savedFilePath);
            });
        }


        #region 명랑핫도그 관련 (임시)

        /// <summary>
        /// 추천인코드와 함께 워크쓰루에서 개인추가정보를 설정합니다
        /// </summary>
        /// <param name="post">POST 데이터</param>
        /// <returns></returns>
        [HttpPost]
        [Route("walkthrough/optional_tmp")]
        public APIResponse RegistRecommend([FromBody] Walkthrough data)
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var device_id = APIHeaderUtil.DeviceID;
            var device_type = APIHeaderUtil.DeviceType;

            var gender = string.IsNullOrEmpty(data.Gender) ? null : data.Gender.ToUpper();
            var pushYN = string.IsNullOrEmpty(data.PushYN) ? null : data.PushYN.ToUpper();
            var age = data.Age;
            var category_xml = string.Empty;

            if (!string.IsNullOrEmpty(data.Category))
                category_xml = string.Format("<ROOT>{0}</ROOT>", string.Join(string.Empty, data.Category.Split(',').Where(x => !string.IsNullOrEmpty(x)).Select(x => string.Format("<CATEGORY>{0}</CATEGORY>", x.Trim())).ToArray()));

            var result = UserRepository.SetWalkthroughOptionalInfoTmp(login_type, uid, gender, age, pushYN, category_xml, data.Code, device_type, device_id);

            if (result.IsSuccess)
                return APIResponse.OK("Y");

            return APIResponse.FAIL(result.ResultMessage);
        }

        [HttpPost]
        [Route("recommend_code/confirm")]
        public APIResponse SetRecommentCodeConfirm()
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var device_id = APIHeaderUtil.DeviceID;
            var device_type = APIHeaderUtil.DeviceType;
            var ip = Common.Util.GetClientIPAddress();

            var result = UserRepository.SetUSP_API_SetEventMemberRecommentPromotion_201811(login_type, uid, device_type, device_id, ip);

            if (result.IsSuccess)
                return APIResponse.OK("Y");

            return APIResponse.FAIL(result.ResultMessage);
        }

        #endregion
    }
}