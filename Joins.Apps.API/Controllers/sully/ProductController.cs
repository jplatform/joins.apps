﻿using Joins.Apps.API.Filters.Attributes.Sully;
using Joins.Apps.API.Models.Sully.ProductController;
using Joins.Apps.API.Models.Sully.ProductController.Parameter;
using Joins.Apps.Common.Sully;
using Joins.Apps.Models.Sully.API;
using Joins.Apps.Repository.Sully.API;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Xml;

namespace Joins.Apps.API.Controllers.sully
{
    [PreventiveMaintenanceCheck]
    [RoutePrefix("ssully")]
    public class ProductController : ApiController
    {
        /// <summary>
        /// 프로덕트 조회수 랭킹 수집
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("product/ranking/scheduler")]
        public APIResponse SetProductViewRanking()
        {
            Joins.Apps.Repository.Sully.ProductRepository.SetProductViewRanking();
            return APIResponse.OK(string.Empty);
        }

        /// <summary>
        /// 썰리퀴즈 이벤트 응모자 개인정보 보관기한 초과 데이터 파기처리
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("product/quiz/destruction_userinfo")]
        public APIResponse DestructionProductQuizEventApplyUserInfo() {
            Joins.Apps.Repository.Sully.ProductRepository.DestructionProductQuizEventApplyUserInfo();
            return APIResponse.OK(string.Empty);
        }
        
        /// <summary>
        /// 메인 리스트를 검색합니다.
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="page"></param>
        /// <param name="page_size"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("product/list/main")]
        [Route("product/{mode}/list/main")]
        public APIResponse GetProductMainList(string mode = "", int page = 1, int page_size = 10)
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var device_id = APIHeaderUtil.DeviceID;
            var is_debug = "debug".Equals(mode, StringComparison.OrdinalIgnoreCase);
            var total_count = 0;
            var total_page = 0;
            var layout = Repository.Sully.SSullyMainRepository.GetMainLayout();
            var result = new List<ProductListMain.MainItem>();
            var firstPageProductSize = layout.Where(x => "product".Equals(x.LayoutType, StringComparison.OrdinalIgnoreCase)).Sum(x => x.ProductCount) ?? 0;

            if (page > 1)
            {
                var skip_size = firstPageProductSize + ((page - 2) * page_size);

                if (page_size - firstPageProductSize > 0)
                    skip_size = skip_size + page_size - firstPageProductSize;

                var data = ProductRepository.GetProductListToMainV2(skip_size, page_size, 1, is_debug, login_type, uid, out total_count);

                result.Add(new ProductListMain.MainItem { ItemType = "product", ProductData = data });
            }
            else
            {
                foreach (var x in layout)
                {
                    var item_type = x.LayoutType.ToLower();

                    if ("product".Equals(item_type))
                    {
                        // 일반형
                        var skip_count = layout.Where(r => "product".Equals(r.LayoutType) && r.OrderNumber < x.OrderNumber).Sum(r => r.ProductCount) ?? 0;
                        var product = ProductRepository.GetProductListToMainV2(skip_count, x.ProductCount.Value, 1, is_debug, login_type, uid, out total_count);

                        if (product != null)
                        {
                            var data = product.Select(item =>
                            {
                                if (!"3".Equals(item.ProviderType))
                                    item.ProviderURL = null;

                                return item;
                            });

                            result.Add(new ProductListMain.MainItem { ItemType = item_type, ProductData = data });
                        }
                    }
                    else if ("product_story".Equals(item_type))
                    {
                        // 스토리썰리
                        var product = ProductRepository.GetProductListToMainV2(0, 5, 5, is_debug, login_type, uid);

                        if (product != null)
                        {
                            var data = product.Select(item =>
                            {
                                item.ProviderURL = null;
                                return item;
                            });

                            result.Add(new ProductListMain.MainItem { ItemType = item_type, ProductData = data });
                        }
                    }
                    else if ("product_pedia".Equals(item_type))
                    {
                        // 썰리피디아
                        var product = ProductRepository.GetProductListToMainV2(0, 3, 2, is_debug, login_type, uid);

                        if (product != null)
                        {
                            var data = product.Select(item =>
                            {
                                item.ProviderURL = null;
                                return item;
                            });

                            result.Add(new ProductListMain.MainItem { ItemType = item_type, ProductData = data });
                        }
                    }
                    else if ("product_tour".Equals(item_type))
                    {
                        // 여기가봤썰
                        var product = ProductRepository.GetProductListToMainV2(0, 1, 6, is_debug, login_type, uid).FirstOrDefault();

                        if (product != null)
                        {
                            product.ProviderURL = null;

                            var content = Joins.Apps.Repository.Sully.ProductRepository.GetServiceProductContentInfo(product.Seq, is_debug, login_type, uid);
                            var data = ProductListMain.ProductMainTour.CopyTo(product);

                            if (content != null && !string.IsNullOrWhiteSpace(content.PDC_ORG_CONTENT))
                            {
                                var doc = new HtmlAgilityPack.HtmlDocument();
                                doc.LoadHtml(content.PDC_ORG_CONTENT);

                                var nodes = doc.DocumentNode.SelectNodes("//div[contains(@ssully_type, 'image_type4')]/div[contains(@class, 'image')]/img");
                                if (nodes != null && nodes.Count() > 0)
                                    data.ProductTourImages = nodes.Select(r => r.GetAttributeValue("src", string.Empty)).Where(r => !string.IsNullOrWhiteSpace(r));
                            }

                            result.Add(new ProductListMain.MainItem { ItemType = item_type, ProductTourData = data });
                        }
                    }
                    else if ("product_presso".Equals(item_type))
                    {
                        // 썰리프레소
                        var product = ProductRepository.GetProductListToMainV2(0, 1, 8, is_debug, login_type, uid);

                        if (product != null)
                        {
                            var data = product.Select(item =>
                            {
                                item.ProviderURL = null;
                                return item;
                            });

                            result.Add(new ProductListMain.MainItem { ItemType = item_type, ProductData = data });
                        }
                    }
                    else if ("weekly_rank".Equals(item_type))
                    {
                        // 주간 랭킹
                        var data = Repository.Sully.SSullyMainRepository.GetWeeklyRank();
                        var member = UserRepository.GetUserInfo(login_type, uid, device_id);
                        var member_age = 0;

                        if (member != null && int.TryParse(member.Age, out member_age) && member_age > 40)
                            member_age = 40;

                        result.Add(new ProductListMain.MainItem
                        {
                            ItemType = item_type,
                            WeeklyRank = data.GroupBy(rank => new { rank.AgeGroup }, (key, items) => new
                            {
                                age_group = key.AgeGroup,
                                selected = key.AgeGroup == member_age ? "Y" : "N",
                                data = items
                            }).OrderBy(rank => rank.age_group)
                        });
                    }
                    else if ("banner".Equals(item_type))
                    {
                        // 배너
                        var product = Repository.Sully.ProductRepository.GetProductInfo(x.PdSeq.Value);

                        if (product != null)
                        {
                            var data = new ProductListMain.Banner
                            {
                                ImageUrl = x.ImageUrl,
                                LinkSeq = x.PdSeq.Value,
                                LinkType = "product",
                                Color = x.BackgroundColor,
                                CornerType = product.PD_TYPE,
                                IsADProduct = product.IS_AD_PRODUCT
                            };

                            result.Add(new ProductListMain.MainItem { ItemType = item_type, Banner = data });
                        }
                    }
                }

                // 마지막은 첫페이지의 일반형 프로덕트의 페이지 갯수만큼 채우기
                var remain_count = page_size - firstPageProductSize;
                if (remain_count > 0)
                {
                    var data = ProductRepository.GetProductListToMainV2(page_size - remain_count, remain_count, 1, is_debug, login_type, uid, out total_count);

                    result.Add(new ProductListMain.MainItem { ItemType = "product", ProductData = data });
                }
            }

            // total_page (첫페이지의 일반형 프로덕트를 제외한 나머지 전체를 페이징후 첫페이지 추가)
            var except_first_page_total_count = total_count - firstPageProductSize;
            total_page = (except_first_page_total_count / page_size) + (except_first_page_total_count % page_size > 0 ? 1 : 0) + 1;

            return APIResponse.OK(new ProductListMain
            {
                List = result,
                Page = page,
                PageSize = page_size,
                Showcase = Repository.Sully.SSullyMainRepository.GetServiceShowcase(false, login_type, uid)?.Select(x => new
                {
                    type = x.ShowcaseType,
                    title = x.Title,
                    thumbnail = x.Thumbnail,
                    label = x.Label,
                    display_date = string.Format("{0:yyyy-MM-dd}", x.DisplayDate),
                    product = x.ShowcaseType == 1 ? new
                    {
                        seq = x.LinkData,
                        is_ad_product = x.ProductIsAd ?? "N",
                        corner_type = x.ProductCornerType
                    } : null,
                    notice = x.ShowcaseType == 2 ? new
                    {
                        seq = x.LinkData
                    } : null,
                    link = x.ShowcaseType == 3 ? new
                    {
                        url = x.LinkData
                    } : null
                }),
                TotalCount = total_count,
                TotalPage = total_page
            });
        }

        /// <summary>
        /// 기사 목록을 검색합니다.
        /// </summary>
        /// <param name="mode">디버그여부</param>
        /// <param name="page">페이지 번호</param>
        /// <param name="page_size">페이지당 리스트 수</param>
        /// <param name="category">검색할 카테고리</param>
        /// <param name="corner_type">코너 종류</param>
        /// <returns></returns>
        [HttpGet]
        [Route("product/list")]
        [Route("product/{mode}/list")]
        public APIResponse GetProductList(string mode = "", int page = 1, int page_size = 10, string category = null, int? corner_type = null, string order = "latest")
        {
            var total_count = 0;
            var total_page = 0;
            var is_debug = "debug".Equals(mode, StringComparison.OrdinalIgnoreCase);
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var ip = Common.Util.GetClientIPAddress();

            var list = ProductRepository.GetProductList(page, page_size, category, null, corner_type, null, order, is_debug, login_type, uid, ip, out total_count, out total_page);

            return APIResponse.OK(new
            {
                List = list,
                TotalCount = total_count,
                TotalPage = total_page,
                Page = page,
                PageSize = page_size
            });
        }

        /// <summary>
        /// 기사 목록을 검색합니다.
        /// </summary>
        /// <param name="mode">디버그여부</param>
        /// <param name="page">페이지 번호</param>
        /// <param name="page_size">페이지당 리스트 수</param>
        /// <param name="category">검색할 카테고리</param>
        /// <param name="corner_type">코너 종류</param>
        /// <returns></returns>
        [HttpGet]
        [Route("v2/product/list")]
        [Route("v2/product/{mode}/list")]
        public APIResponse GetProductListV2(string mode = "", int page = 1, int page_size = 10, string category = null, int? corner_type = null, string order = "latest")
        {
            var total_count = 0;
            var total_page = 0;
            var is_debug = "debug".Equals(mode, StringComparison.OrdinalIgnoreCase);
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var ip = Common.Util.GetClientIPAddress();

            var list = ProductRepository.GetProductList(page, page_size, category, null, corner_type, null, order, is_debug, login_type, uid, ip, out total_count, out total_page);

            if (corner_type.HasValue && corner_type.Value == 7)
            {
                var header = Repository.Sully.ProductRepository.GetCornerListHeader(corner_type.Value);
                return APIResponse.OK(new
                {
                    List = list,
                    Header = page == 1 && header != null ? new {
                        link_type = header.LinkType,
                        link_value = header.LinkValue,
                        image_url = header.ImageUrl
                    } : null,
                    TotalCount = total_count,
                    TotalPage = total_page,
                    Page = page,
                    PageSize = page_size
                });
            }

            return APIResponse.OK(new
            {
                List = list,
                TotalCount = total_count,
                TotalPage = total_page,
                Page = page,
                PageSize = page_size
            });
        }

        /// <summary>
        /// 기사를 검색합니다.
        /// </summary>
        /// <param name="data">파라메터</param>
        /// <returns></returns>
        [HttpPost]
        [Route("product/search")]
        public APIResponse GetProductSearch([FromBody] Search data)
        {
            var total_count = 0;
            var total_page = 0;
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var ip = Common.Util.GetClientIPAddress();
            var search_xml = string.IsNullOrEmpty(data.Search_text) ? null : string.Format("<ROOT>{0}</ROOT>", data.Search_text.Trim().Split(' ').Where(x => !string.IsNullOrEmpty(x)).Select(x => string.Format("<WORD>{0}</WORD>", x)).Aggregate((prev, next) => prev + next));
            var list = ProductRepository.GetProductList(data.Page, data.Page_Size, data.Category, null, null, search_xml, data.Order, false, login_type, uid, ip, out total_count, out total_page);

            return APIResponse.OK(new
            {
                List = list,
                TotalCount = total_count,
                TotalPage = total_page,
                Page = data.Page,
                PageSize = data.Page_Size
            });
        }

        /// <summary>
        /// 기사 상세내용을 검색합니다.
        /// </summary>
        /// <param name="seq">기사 고유번호</param>
        /// <param name="mode">디버그여부</param>
        /// <returns></returns>
        [HttpGet]
        [Route("product/detail/{seq:int}")]
        [Route("product/{mode}/detail/{seq:int}")]
        public APIResponse GetProductDetail(int seq, string mode = "")
        {
            var is_debug = "debug".Equals(mode, StringComparison.OrdinalIgnoreCase);
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;

            if (seq > 0 && !is_debug)
            {
                try
                {
                    var host = string.Empty;
                    var url = string.Empty;
                    var ip = Common.Util.GetClientIPAddress();
                    var agent = HttpContext.Current.Request.UserAgent;
                    if (HttpContext.Current.Request.UrlReferrer != null)
                    {
                        host = HttpContext.Current.Request.UrlReferrer.GetLeftPart(UriPartial.Authority);
                        url = HttpContext.Current.Request.UrlReferrer.GetLeftPart(UriPartial.Query);
                    }

                    Joins.Apps.Repository.Sully.LogRepository.SetProductViewLog(seq, ip, agent, host, url, login_type, uid);
                }
                catch (Exception ex) { }
            }

            var content = Joins.Apps.Repository.Sully.ProductRepository.GetServiceProductContentInfo(seq, is_debug, login_type, uid);

            if (content != null)
            {
                var product = Joins.Apps.Repository.Sully.ProductRepository.GetProductInfo(seq, login_type, uid);
                var relateProduct = Joins.Apps.Repository.Sully.ProductRepository.GetServiceProductRelateList(seq);
                var result = ContentOptimizer.GetContent(product, content.PDC_ORG_CONTENT, relateProduct, true, true);
                var share_url_base = new Uri(ConfigurationManager.AppSettings["SSully_Url"]);

                result.ShareURL = new Uri(share_url_base, string.Format("/view/{0}?share=y", result.Product.Seq));

                if (result != null)
                    return APIResponse.OK(result);
            }

            return APIResponse.FAIL("데이터가 없습니다.");
        }

        /// <summary>
        /// 썰리퀴즈 응답
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        [HeaderCheck]
        [HttpPost]
        [Route("product/detail/question/apply")]
        public APIResponse SetProductQuestionApply(QuestionAnswer parameter)
        {
            var resultMessage = string.Empty;
            var doc = new XmlDocument();
            var root = doc.CreateElement("ROOT");
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var ip = Common.Util.GetClientIPAddress();

            var questions = parameter.question_seq.Split(',');
            var items = parameter.item_seq.Split(',');

            if (questions.Length != items.Length)
                return APIResponse.FAIL("풀지않은 퀴즈가 있습니다.");

            for (var i = 0; i < questions.Length; i++)
            {
                var question = doc.CreateElement("QUESTION");
                question.SetAttribute("SEQ", questions[i]);
                question.SetAttribute("ITEM_SEQ", items[i]);

                root.AppendChild(question);
            }

            doc.AppendChild(root);

            var result = ProductRepository.SetProductDetailQuestionApply(login_type, uid, parameter.seq, doc.OuterXml, ip, out resultMessage);

            if (result != null && string.IsNullOrEmpty(resultMessage))
                return APIResponse.OK(result);

            return APIResponse.FAIL(resultMessage);
        }

        /// <summary>
        /// 프로덕트를 좋아요 처리 합니다.
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        [HeaderCheck]
        [HttpPost]
        [Route("product/like/{seq:int}")]
        public APIResponse SetProductLike(int seq)
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var like_yn = "N";
            var total_like_cnt = 0;

            var result = Joins.Apps.Repository.Sully.ProductRepository.SetProductLike(login_type, uid, seq, out like_yn, out total_like_cnt);

            return APIResponse.OK(new {
                result = result.IsSuccess ? "Y" : "N",
                msg = result.IsSuccess ? string.Empty : result.ResultMessage,
                like_yn = like_yn,
                total_like_cnt = total_like_cnt
            });
        }

        /// <summary>
        /// 프로덕트를 스크랩 처리 합니다.
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        [HeaderCheck]
        [HttpPost]
        [Route("product/scrap/{seq:int}")]
        public APIResponse SetProductScrap(int seq)
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var scrap_yn = "N";

            var result = Joins.Apps.Repository.Sully.ProductRepository.SetProductScrap(login_type, uid, seq, out scrap_yn);

            return APIResponse.OK(new
            {
                result = result.IsSuccess ? "Y" : "N",
                msg = result.IsSuccess ? string.Empty : result.ResultMessage,
                scrap_yn = scrap_yn
            });
        }
    }
}
