﻿using Joins.Apps.API.Filters.Attributes.Sully;
using Joins.Apps.API.Models.Sully.QuestionController;
using Joins.Apps.Common.Sully;
using Joins.Apps.Models.Sully.API;
using Joins.Apps.Repository.Sully.API;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Joins.Apps.API.Controllers.sully
{
    [PreventiveMaintenanceCheck]
    [HeaderCheck]
    [RoutePrefix("ssully/question")]
    public class QuestionController : ApiController
    {
        [NonAction]
        private IEnumerable<QuestionDetail> GetDetailList(int? seq, int page, int page_size, out int total_count, out int total_page, out int target_page)
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;

            var result = QuestionRepository.GetDetail(login_type, uid, seq, page, page_size, out total_count, out total_page, out target_page);

            if (result == null)
                return null;

            return QuestionRepository.GetDetail(login_type, uid, seq, page, page_size, out total_count, out total_page, out target_page).Select(x =>
            {
                if (x.ItemType == QuestionDetailType.INTERNAL_LINK)
                    x.LinkInternalUrl = string.Format("https://ssully.joins.com/view/{0}", x.LinkInternalSeq);
                return x;
            });
        }

        /// <summary>
        /// 문의/제보 내역을 검색합니다.
        /// </summary>
        /// <param name="page">페이지 번호</param>
        /// <param name="page_size">페이지당 리스트 수</param>
        /// <param name="seq">한번에 불러올 게시글 번호</param>
        /// <returns></returns>
        [HttpGet]
        [Route("detail")]
        public APIResponse GetDetail(int page = 1, int page_size = 10, int? seq = null)
        {
            var total_count = 0;
            var total_page = 0;
            var target_page = 0;
            var result = GetDetailList(seq, page, page_size, out total_count, out total_page, out target_page);

            return APIResponse.OK(new
            {
                List = result ?? new List<QuestionDetail>(),
                TotalCount = total_count,
                TotalPage = total_page,
                Page = target_page > 0 ? target_page : page,
                PageSize = page_size
            });
        }

        /// <summary>
        /// 문의/제보 글을 작성합니다.
        /// </summary>
        /// <param name="message">메세지</param>
        /// <returns></returns>
        [HttpPost]
        [Route("insert")]
        public APIResponse PostInsert([FromBody] Insert data)
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;

            if (string.IsNullOrEmpty(data.Message))
                return APIResponse.FAIL("메세지를 입력해 주시기 바랍니다.", new { Success = "N" });

            var result = QuestionRepository.SetReply(login_type, uid, data.Message);

            if (result.IsSuccess)
            {
                var total_count = 0;
                var total_page = 0;
                var target_page = 0;
                var list = GetDetailList(null ,1, data.Page_Size, out total_count, out total_page, out target_page);

                return APIResponse.OK(new {
                    Success = "Y",
                    List = list ?? new List<QuestionDetail>(),
                    TotalCount = total_count,
                    TotalPage = total_page,
                    Page = 1,
                    PageSize = data.Page_Size
                });
            }

            return APIResponse.FAIL(result.ResultMessage, new { Success = "N" });
        }
    }
}