﻿using Joins.Apps.API.Filters.Attributes.Sully;
using Joins.Apps.Common.Sully;
using Joins.Apps.Models.Sully;
using Joins.Apps.Models.Sully.API;
using System.Collections.Generic;
using System.Web.Http;

namespace Joins.Apps.API.Controllers.sully
{
    [PreventiveMaintenanceCheck]
    [HeaderCheck]
    [RoutePrefix("ssully/scrap")]
    public class ScrapController : ApiController
    {
        [HttpGet]
        [Route("list")]
        public APIResponse GetScrapList(int page, int page_size)
        {
            var login_type = APIHeaderUtil.MemberLoginType;
            var uid = APIHeaderUtil.MemberAuthUniqueID;
            var total_count = 0;
            var total_page = 0;
            var result = Joins.Apps.Repository.Sully.ScrapRepository.GetScrapList(login_type, uid, page, page_size, out total_count, out total_page) ?? new List<ProductScrap>();

            return APIResponse.OK(new
            {
                List = result,
                TotalCount = total_count,
                TotalPage = total_page,
                Page = page,
                PageSize = page_size
            });
        }
    }
}
