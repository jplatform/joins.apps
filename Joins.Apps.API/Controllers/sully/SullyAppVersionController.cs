﻿using Joins.Apps.API.Filters.Attributes.Sully;
using Joins.Apps.Models.Sully.API;
using Joins.Apps.Repository.Sully.API;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Joins.Apps.API.Controllers.sully
{
    [PreventiveMaintenanceCheck]
    [RoutePrefix("ssully/app")]
    public class SullyAppVersionController : ApiController
    {
        [HttpGet]
        [Route("version/check")]
        public APIResponse Update()
        {
            var googleURL = "https://play.google.com/store/apps/details?id=com.joins.joongang.ssully";
            var iosURL = "https://itunes.apple.com/lookup?id=1335446055";

            var androidTask = Task.Run(async () => {
                try
                {
                    var html = new HtmlAgilityPack.HtmlWeb();
                    var htmlDoc = await html.LoadFromWebAsync(googleURL);

                    var version = htmlDoc.DocumentNode.SelectSingleNode("(//span[@class='htlgb'])[7]");
                    if (version != null)
                        return version.InnerText.Trim();
                }
                catch { }

                return string.Empty;
            });

            var iosTask = Task.Run(async () => {
                try
                {
                    var result = string.Empty;
                    using (var client = new HttpClient())
                        result = await client.GetStringAsync(iosURL);

                    var json = JObject.Parse(result);

                    var info = json["results"].FirstOrDefault();
                    if (info != null)
                        return info.Value<string>("version");
                }
                catch { }

                return string.Empty;
            });

            Task.WaitAll(androidTask, iosTask);

            return APIResponse.OK(new {
                android = androidTask.Result,
                ios = iosTask.Result
            });
        }

        [HttpGet]
        [Route("version")]
        public APIResponse Version()
        {
            var result = AppVersionRepository.GetAppVersion();

            if (result != null)
            {
                var android = result.Where(x => "A".Equals(x.OSType, System.StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                var ios = result.Where(x => "I".Equals(x.OSType, System.StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

                return APIResponse.OK(new { android = android, ios = ios });
            }

            return APIResponse.OK(null);
        }
    }
}
