﻿using Joins.Apps.API.Filters.Attributes.Sully;
using Joins.Apps.Models.Sully;
using Joins.Apps.Models.Sully.API;
using Joins.Apps.Repository.Sully;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Joins.Apps.API.Controllers.sully
{
    [PreventiveMaintenanceCheck]
    [RoutePrefix("ssully/ranking")]
    public class RankingController : ApiController
    {
        [Route("search")]
        public APIResponse GetRanking()
        {
            var result = SearchRankingRepository.GetSearchRanking();
            var analyze_date = string.Format("{0:yyyy.MM.dd}", DateTime.Today);

            if (result != null)
                analyze_date = string.Format("{0:yyyy.MM.dd}", result.FirstOrDefault().AnalyzeDate);

            return APIResponse.OK(new
            {
                analyze_date = analyze_date,
                data = result
            });
        }
    }
}
