﻿using Joins.Apps.API.Filters.Attributes.Sully;
using Joins.Apps.Common;
using Joins.Apps.Models.Sully.API;
using Joins.Apps.Repository.Sully.API;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Joins.Apps.API.Controllers.sully
{
    [PreventiveMaintenanceCheck]
    [RoutePrefix("ssully/push")]
    public class SullyPushController : ApiController
    {
        #region 푸시 전송

        [HttpGet]
        [Route("send")]
        public APIResponse Send()
        {
            try
            {
                var list = PushRepository.GetPushSendList();
                var dry_run = false;
                var priority = "high";
                var stopwatch = new Stopwatch();

                if (list != null && list.Count() > 0)
                {
                    // 테이블의 SEQ로 GROUP BY한 후 ANDROID, IOS별로 리스트 분리
                    var groups = list.GroupBy(x => x.seq, (key, entity) => new
                    {
                        seq = key,
                        android_items = entity.Where(x => "A".Equals(x.device_type, StringComparison.OrdinalIgnoreCase)),
                        ios_items = entity.Where(x => "I".Equals(x.device_type, StringComparison.OrdinalIgnoreCase))
                    });

                    NotificationRepository.SetSystemNotifyData();

                    // 푸시 발송
                    foreach (var group in groups)
                    {
                        var tasks = new List<Task>();
                        var android_result = new List<FirebaseMessagingOptimizedResult>();
                        var ios_result = new List<FirebaseMessagingOptimizedResult>();

                        // 수행시간 측정 시작
                        stopwatch.Restart();

                        // Android 비동기 발송 (1000개씩 묶어서 한번에 발송)
                        tasks.Add(Task.Factory.StartNew(() =>
                        {
                            var page_size = 1000;
                            var total_count = group.android_items.Count();
                            var max_page = (total_count / page_size) + (total_count % page_size > 0 ? 1 : 0);

                            for (var i = 0; i < max_page; i++)
                            {
                                var page_data = group.android_items.Skip(i * page_size).Take(page_size);

                                Task.Factory.StartNew((obj) => {
                                    var items = obj as IEnumerable<PushSend>;
                                    var item = items.FirstOrDefault();
                                    var registration_ids = items.Select(x => x.fcm_token).ToArray();

                                    var data = new
                                    {
                                        registration_ids = registration_ids,
                                        priority = priority,
                                        dry_run = dry_run,
                                        data = new
                                        {
                                            title = item.p_title,
                                            m = item.p_content,
                                            type = item.p_type,
                                            mem_seq = item.mem_seq,
                                            key1 = item.p_key1,
                                            key2 = item.p_key2,
                                            key3 = item.p_key3,
                                            thumb = item.p_img,
                                            bimg = item.p_img
                                        }
                                    };

                                    // 푸시 발송요청 후 결과 저장
                                    var send_result = SendFCM(data);
                                    var fcm_result = ConvertFCMResult(registration_ids, send_result);

                                    android_result.AddRange(fcm_result);
                                }, page_data, TaskCreationOptions.AttachedToParent);
                            }
                        }));


                        // iOS 비동기 발송 (1000개씩 묶어서 한번에 발송)
                        tasks.Add(Task.Factory.StartNew(() =>
                        {
                            var page_size = 1000;
                            var total_count = group.ios_items.Count();
                            var max_page = (total_count / page_size) + (total_count % page_size > 0 ? 1 : 0);

                            for (var i = 0; i < max_page; i++)
                            {
                                var page_data = group.ios_items.Skip(i * page_size).Take(page_size);

                                Task.Factory.StartNew((obj) => {
                                    var items = obj as IEnumerable<PushSend>;
                                    var item = items.FirstOrDefault();
                                    var registration_ids = items.Select(x => x.fcm_token).ToArray();

                                    var data = new
                                    {
                                        registration_ids = registration_ids,
                                        priority = priority,
                                        dry_run = dry_run,
                                        notification = new
                                        {
                                            title = item.p_title,
                                            body = item.p_content
                                        },
                                        data = new
                                        {
                                            type = item.p_type,
                                            mem_seq = item.mem_seq,
                                            key1 = item.p_key1,
                                            key2 = item.p_key2,
                                            key3 = item.p_key3,
                                            thumb = item.p_img,
                                            bimg = item.p_img
                                        }
                                    };

                                    // 푸시 발송요청 후 결과 저장
                                    var send_result = SendFCM(data);
                                    var fcm_result = ConvertFCMResult(registration_ids, send_result);

                                    ios_result.AddRange(fcm_result);
                                }, page_data, TaskCreationOptions.AttachedToParent);
                            }
                        }));

                        Task.WaitAll(tasks.ToArray());

                        // 수행시간 측정 종료
                        stopwatch.Stop();

                        /*
                         * <root>
                         *      <report>
                         *          <item device="A" success="1" remove="2" change="3"/>
                         *          <item device="I" success="1" remove="2" change="3"/>
                         *      </report>
                         *      <remove_items>
                         *          <item><![CDATA[삭제될 토큰값]]></item>
                         *      </remove_items>
                         *      <change_items>
                         *          <item>
                         *              <registration_id><![CDATA[기존 토큰값]]></registration_id>
                         *              <new_registration_id><![CDATA[변경될 토큰값]]></new_registration_id>
                         *          </item>
                         *      </change_items>
                         * </root>
                         */

                        var xml = new StringBuilder();

                        xml.AppendFormat("<root>");

                        // 수행시간
                        xml.AppendFormat(@"<stopwatch duration=""{0}"" />", stopwatch.Elapsed);

                        // 전체 결과
                        xml.AppendFormat("<report>");
                        xml.AppendFormat(@"<item device=""A"" success=""{0}"" remove=""{1}"" change=""{2}""/>", android_result.Count(x => x.is_success), android_result.Count(x => x.is_remove), android_result.Count(x => x.is_change));
                        xml.AppendFormat(@"<item device=""I"" success=""{0}"" remove=""{1}"" change=""{2}""/>", ios_result.Count(x => x.is_success), ios_result.Count(x => x.is_remove), ios_result.Count(x => x.is_change));
                        xml.AppendFormat("</report>");
                        // #전체 결과
                        
                        // 삭제 대상 토큰 목록
                        xml.AppendFormat("<remove_items>");
                        xml.Append(string.Join(string.Empty, android_result.Where(x => x.is_remove).Select(x => string.Format(@"<item><![CDATA[{0}]]></item>", x.registration_id))));
                        xml.Append(string.Join(string.Empty, ios_result.Where(x => x.is_remove).Select(x => string.Format(@"<item><![CDATA[{0}]]></item>", x.registration_id))));
                        xml.AppendFormat("</remove_items>");
                        // #삭제 대상 토큰 목록
                        
                        // 변경 대상 토큰 목록
                        xml.AppendFormat("<change_items>");
                        xml.Append(string.Join(string.Empty, android_result.Where(x => x.is_change).Select(x => {
                            var sub_node = string.Format("<registration_id><![CDATA[{0}]]></registration_id><new_registration_id><![CDATA[{1}]]></new_registration_id>", x.registration_id, x.new_registration_id);
                            return string.Format(@"<item>{0}</item>", sub_node);
                        })));
                        xml.Append(string.Join(string.Empty, ios_result.Where(x => x.is_change).Select(x => {
                            var sub_node = string.Format("<registration_id><![CDATA[{0}]]></registration_id><new_registration_id><![CDATA[{1}]]></new_registration_id>", x.registration_id, x.new_registration_id);
                            return string.Format(@"<item>{0}</item>", sub_node);
                        })));
                        xml.AppendFormat("</change_items>");
                        // #변경 대상 토큰 목록

                        xml.AppendFormat("</root>");

                        //푸시카운트 업데이트
                        PushRepository.SetPushSendResult(group.seq, Convert.ToString(xml));
                    }
                }

                return APIResponse.OK(null);
            }
            catch (Exception ex)
            {
                Logger.LogWriteError("SSully Push Error", Request.RequestUri + "\t" + ex.ToString());
                return APIResponse.FAIL("", ex);
            }
        }

        #endregion


        #region 푸시 호출

        [NonAction]
        private IEnumerable<FirebaseMessagingOptimizedResult> ConvertFCMResult(string[] registration_ids, FirebaseMessagingResult data)
        {
            var result = new List<FirebaseMessagingOptimizedResult>();

            for (var i = 0; i < data.Results.Count(); i++)
            {
                var element = data.Results.ElementAt(i);
                var registration_id = registration_ids[i];
                var new_registration_id = element.ContainsKey("message_id") && element.ContainsKey("registration_id") ? element["registration_id"] : null;
                var is_change = element.ContainsKey("message_id") && element.ContainsKey("registration_id");
                var is_remove = element.ContainsKey("error") && ("InvalidRegistration".Equals(element["error"], StringComparison.OrdinalIgnoreCase) || "NotRegistered".Equals(element["error"], StringComparison.OrdinalIgnoreCase));

                result.Add(new FirebaseMessagingOptimizedResult
                {
                    // 푸시를 발송한 대상 토큰
                    registration_id = registration_id,
                    // 서버로부터 받은 변경된 토큰 값 (삭제대상인 경우 null 처리함)
                    new_registration_id = is_remove ? null : new_registration_id,
                    // 삭제 대상 여부
                    is_remove = is_remove,
                    // 수정 대상 여부 (삭제대상이면서 수정대상인경우 삭제대상으로 처리 -- 그럴리는 없겠지만)
                    is_change = !is_remove && is_change,
                    // 발송성공 대상 여부
                    is_success = !is_remove
                });
            }

            return result;
        }

        private FirebaseMessagingResult SendFCM(object content)
        {
            var url = "https://fcm.googleapis.com/fcm/send";
            var authorization_key = "AAAAujQP2fw:APA91bFktdKJp44L23ijaJVyTtceZeSztSvNR5-qJ-2J7gg-OszTjv92HC2LtIJDjiKjSLp6id5eUhkYHamIzIvMnId24y1ab6m_xezz_mizVx2t6CoBn4RyDnb0AVJhB-Ce7BAdDY-0";
            var sender = "799737371132";

            using (var client = new HttpClient())
            {
                var data = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri(url),
                    Version = HttpVersion.Version11,
                    Content = new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json")
                };
                data.Headers.TryAddWithoutValidation("Authorization", string.Format("key={0}", authorization_key));
                data.Headers.TryAddWithoutValidation("Sender", sender);
                var result = client.SendAsync(data).Result.Content.ReadAsStringAsync().Result;

                return JsonConvert.DeserializeObject<FirebaseMessagingResult>(result);
            }
        }

        public class FirebaseMessagingResult
        {
            [JsonProperty("multicast_id")]
            public string MulticastID { get; set; }
            [JsonProperty("success")]
            public int Success { get; set; }
            [JsonProperty("failure")]
            public int Failure { get; set; }
            [JsonProperty("canonical_ids")]
            public int CanonicalIDs { get; set; }
            [JsonProperty("results")]
            public IEnumerable<Dictionary<string, string>> Results { get; set; }
        }

        public class FirebaseMessagingOptimizedResult
        {
            /// <summary>
            /// 푸시를 발송한 대상 토큰을 가져오거나 설정합니다.
            /// </summary>
            public string registration_id { get; set; }
            /// <summary>
            /// 서버로부터 받은 변경된 토큰 값을 가져오거나 설정합니다.
            /// </summary>
            public string new_registration_id { get; set; }
            /// <summary>
            /// 수정 대상 여부를 가져오거나 설정합니다.
            /// </summary>
            public bool is_change { get; set; }
            /// <summary>
            /// 삭제 대상 여부를 가져오거나 설정합니다.
            /// </summary>
            public bool is_remove { get; set; }
            /// <summary>
            /// 발송 성공여부를 가져오거나 설정합니다.
            /// </summary>
            public bool is_success { get; set; }
        }

        #endregion
    }
}