﻿using HtmlAgilityPack;
using Joins.Apps.Models.NewsLine;
using Joins.Apps.Models.Search;
using Joins.Apps.Repository.Search;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Http;
//using System.Web.Mvc;

namespace Joins.Apps.API.Controllers.NewsLine
{
    public class NaverController : ApiController{
        [HttpGet]
        [Route("Naver/topiclist")]
        public IHttpActionResult topiclist(int pgi=1,int pgs=5,string sw="") {
           var result= Joins.Apps.Repository.NewsLine.ArticleRepository.SetTopicList(pgi,pgs,0,sw);
           return Ok(result);
        }
        [HttpGet]
        [Route("Naver/topicitem")]
        public IHttpActionResult topiclist(int seq=0) {
            Joins.Apps.Models.NewsLine.TopicItemData itemdata = new Joins.Apps.Models.NewsLine.TopicItemData();
            var result = Joins.Apps.Repository.NewsLine.ArticleRepository.SetTopicList(1, 1, seq, "");
            if (result != null && result.Count > 0) {
                itemdata.topic_article_data = JObject.Parse(result.FirstOrDefault().topic_article_data);
               // itemdata.topic_article_data = JObject.Parse(JCube.AF.IO.File.LoadFileText(Joins.Apps.GLOBAL.LogPath + "newsline\\" + result.FirstOrDefault().seq + ".json"));

               // itemdata.topic_data = (result.FirstOrDefault().topic_article_data);
              //  itemdata.topic_article_data = (Repository.NewsLine.ArticleRepository.LoadFileText(Joins.Apps.GLOBAL.LogPath + "newsline\\" + result.FirstOrDefault().seq + ".json"));
            }
            return Ok(itemdata);
        }
        [HttpGet]
        [Route("Naver/topic")]
        public IHttpActionResult topic() {
            //네이버 뉴스 토픽 크롤링
            var html = @"https://news.naver.com/main/main.nhn?mode=LSD&mid=shm&sid1=100";
            HtmlWeb web = new HtmlWeb();
            web.OverrideEncoding = System.Text.Encoding.GetEncoding(51949);
            var htmlDoc = web.Load(html);
            List<string> topiclist = new List<string>();
            string topictime = "";
          //  try {
                //뉴스토픽 시간
            var topictime1 = htmlDoc.DocumentNode.SelectSingleNode("//span[contains(@class, 'newstopic_time')]");
            if(topictime1!=null)
                topictime =topictime1.InnerText.Replace("기준", "").Trim();
            //뉴스토픽 추출
            HtmlAgilityPack.HtmlNodeCollection nodeCol = htmlDoc.DocumentNode.SelectNodes("//ol[contains(@id, 'newstopic_news')]/li");
                if (nodeCol != null && nodeCol.Count > 0) {
                    foreach (HtmlNode li in nodeCol) {
                    //   li.SelectSingleNode("strong");
                        if (li != null && !string.IsNullOrEmpty(li.InnerText)) {
                            topiclist.Add(li.SelectSingleNode("a/strong").InnerText);
                    //    Joins.Apps.Common.Logger.LogWriteAction("topiclist", li.SelectSingleNode("a/strong").InnerText);
                    }
                    }
                }
          //  } catch (Exception e){}
            if (!string.IsNullOrEmpty(topictime)) {
                var result = Joins.Apps.Repository.NewsLine.ArticleRepository.SetTopicList(1, 1, 0, "");
                if (result != null && result.Count > 0) {
                    if (topictime == result.FirstOrDefault().topic_naver_date) {
                        return Ok();
                    }
                }
            }
            Joins.Apps.Common.Logger.LogWriteAction("topictime", topictime);
            //해당 토픽의 일 기사 건수 찾기
          //  List<TopicData> topicDataList = new List<TopicData>();
            foreach (string topic in topiclist) {
                try {
                    var html1 = @"https://search.naver.com/search.naver?where=news&sm=tab_jum&query="+ System.Web.HttpUtility.UrlEncode(topic) + @"&nso=so%3Ar%2Cp%3A1d%2Ca%3Aall";
                    HtmlWeb web1 = new HtmlWeb();
                    web1.OverrideEncoding = System.Text.Encoding.UTF8;
                    var htmlDoc1 = web1.Load(html1);
                    HtmlAgilityPack.HtmlNode nodeCol1 = htmlDoc1.DocumentNode.SelectSingleNode("//div[contains(@class, 'title_desc all_my')]/span");
                    if (nodeCol1 != null ) {
                        int resultcnt = 0;
                       var resultcntdata= nodeCol1.InnerText.Replace("건", "").Trim();
                        if (!string.IsNullOrEmpty(resultcntdata)) {
                           string[] tmp = resultcntdata.Split(" ".ToArray());
                            resultcnt = JCube.AF.Util.Converts.ToInt32(tmp[tmp.Length - 1],0);
                        }
                        //topicDataList.Add(new TopicData() {
                        //    topic = topic,
                        //    article_cnt = resultcnt
                        //});
                        var result = GetJoongangNewsSearch(new TopicInputData() {
                            topic = topic,
                            article_cnt = resultcnt,
                            topic_date = topictime
                        });
                    }
                } catch (Exception e) { }
                //우리 검색에 토픽 던지기
            }
            return Ok();
        }
        /// <summary>
        /// 토픽 중앙일보 검색
        /// </summary>
        /// <param name="topicTitle"></param>
        /// <returns></returns>
        [NonAction]
        public static int GetJoongangNewsSearch(TopicInputData topicdata) {

       //     try {
                SearchRepository schRepository = new SearchRepository(new SearchCondition {
                    Page = 1,
                    PageSize = 10,
                    IsNeedTotalCount = false,
                    Keyword = topicdata.topic,
                    StartSearchDate=DateTime.Now.AddDays(-1),
                    EndSearchDate = DateTime.Now,
                    PeriodType = PeriodType.DirectInput,
                    SearchCategoryType = SearchCategoryType.News,
                    SortType = SortType.New,
                    ScopeType = ScopeType.All
                    //ScopeType = ScopeType.All,
                    //SourceCode = "j9"
                });
                DefaultResult searchJoongangNewsData = schRepository.GetSearchResult();
                if (!searchJoongangNewsData.SearchItems.Any())
                    throw new ArgumentException();
              //  var historyRgx = new Regex(@"[가-힣]{2}\s*\d{4}.?\d{2}.?\d{2}\s*", RegexOptions.IgnoreCase);

                List<ArticleData> returnJoongangNewsData = new List<ArticleData>();
                var xml = new StringBuilder();
                xml.AppendFormat("<ROOT>");
                foreach (SearchItem item in searchJoongangNewsData.SearchItems) {
                    if (item.MobileTitle.IndexOf("오늘]")>=0|| item.MobileTitle.IndexOf("미세먼지 ]") >= 0) {continue;}
                    string sTitle = Joins.Apps.Common.Util.InsertText(Joins.Apps.Common.Util.HTMLRemove(item.MobileTitle));
                    
              //  Joins.Apps.Common.Logger.LogWriteAction("sTitle", sTitle);
                var xmlItem = new StringBuilder();
                        xmlItem.AppendFormat("<ITEM><TID><![CDATA[" + item.Id.ToString() + "]]></TID><ART_TITLE><![CDATA[" + sTitle + "]]></ART_TITLE><ART_DATE><![CDATA[" + Joins.Apps.Common.Util.DateTimeConvert(item.RegistedDateTime.ToString()) + "]]></ART_DATE></ITEM>");
                    xml.AppendFormat(xmlItem.ToString());
                }
                xml.AppendFormat("</ROOT>");
                if (xml.Length > 0) {
                    int r = Joins.Apps.Repository.NewsLine.ArticleRepository.SetTopicInput(topicdata, xml.ToString());
                    // returnJoongangNewsData = returnJoongangNewsData.OrderBy(a => Guid.NewGuid()).ToList();
                    return r;
                }
          //  } catch (ArgumentException) {
                return 0;
         //   }
            return 0;
        }
    }
}

