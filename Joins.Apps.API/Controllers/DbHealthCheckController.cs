﻿using Joins.Apps.Repository.Common;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Joins.Apps.API.Controllers
{
    public class DbHealthCheckController : ApiController
    {
        [HttpGet]
        [Route("healthcheck/db")]
        public async Task<IHttpActionResult> CheckDB()
        {
            var news10Fail = false;
            var target = string.Empty;
            var news10Task = Task.Run(() => news10Fail = CommonRepository.IsNews10DbLive());

            Task.WaitAll(news10Task);

            if (!news10Fail)
            {
                target = "news10";
                var url = string.Format("https://app.joins.com/sms/?phone=01094130984,01026174627,01034759209&callback=0220311805&msg=[{0} / {1}] DB 연결 오류 발생 (3회)", System.Web.HttpContext.Current.Server.MachineName, target);

                using (var client = new HttpClient())
                {
                    await client.GetAsync(url);
                }

                // 01094130984,01026174627,01034759209,01045153100,01087425986,01033555206,01089598379
                // 01094130984 조영곤
                // 01026174627 송관
                // 01034759209 김재일
                // 01045153100 고정훈과장
                // 01087425986 이승철차장
                // 01033555206 박미희과장
                // 01089598379 최민석
                //https://app.joins.com/sms/?phone=01089598379,&callback=0220311805&msg=[가판대 미등록]
            }

            return Ok(new { result = target });
        }
    }
}
