﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Joins.Apps.API.Controllers.Briefing
{
    public class ListController : ApiController{
        #region 카드 리스트
        //[HttpGet]
        //[Route("Briefing/List/Genie")]
        //public IHttpActionResult Genie(int gen = 1, int age =3) {
        //    string sCruz = Joins.Apps.Core.MemCache.GetMemCruzCall(age,gen);
        //    List<Joins.Apps.Models.News10.Cruz.News10> returnValue = null;
        //    try {
        //        Joins.Apps.Common.Logger.LogWriteGroup("News10_Genie","gen:"+ gen+ " age:"+age);
        //        Joins.Apps.Models.News10.Cruz cruz= JsonConvert.DeserializeObject<Joins.Apps.Models.News10.Cruz>(sCruz);
        //        returnValue = cruz.news10;
        //    } catch (ArgumentException) {returnValue = null;}

        //    return Ok(new { News10 = returnValue });
        //}
        [HttpGet]
        [Route("Briefing/List/Genie")]
        public IHttpActionResult Genie(int gen = 1, int age = 3) {
            return Ok();
            List<Joins.Apps.Models.News10.Cruz.News10> returnValue = null;
            try {
               Joins.Apps.Common.Logger.LogWriteGroup("News10_Genie", "gen:" + gen + " age:" + age);
                List<Joins.Apps.Models.News10.Cruz.News10> CruzNew10DataList = Joins.Apps.Core.MemCache.GetMemDBCruzNews10Data(age, gen);
                returnValue = CruzNew10DataList;
            } catch (ArgumentException) { returnValue = null; }

            return Ok(new { News10 = returnValue });
        }
        [HttpGet]
        [Route("Briefing/List/Bixby")]
        public IHttpActionResult Bixby(int gen = 1, int age = 3) {
            return Ok();
            List<Joins.Apps.Models.News10.Cruz.News10> returnValue = null;
            try {
                Joins.Apps.Common.Logger.LogWriteGroup("News10_Bixby", "gen:" + gen + " age:" + age);
                List<Joins.Apps.Models.News10.Cruz.News10> CruzNew10DataList = Joins.Apps.Core.MemCache.GetMemDBCruzNews10BixbyData(age, gen);
                returnValue = CruzNew10DataList;
            } catch (ArgumentException) { returnValue = null; }

            return Ok(new { News10 = returnValue });
        }
        [HttpGet]
        [Route("Briefing/List/Bixby2")]
        public IHttpActionResult Bixby2(int gen = 1, int age = 3) {
            return Ok();
            Joins.Apps.Models.News10.returnData2<List<Joins.Apps.Models.News10.Cruz.News10>> r = new Joins.Apps.Models.News10.returnData2<List<Joins.Apps.Models.News10.Cruz.News10>>();
            List<Joins.Apps.Models.News10.Cruz.News10> returnValue = null;
            try {
                Joins.Apps.Common.Logger.LogWriteGroup("News10_Bixby2", "gen:" + gen + " age:" + age);
                List<Joins.Apps.Models.News10.Cruz.News10> CruzNew10DataList = Joins.Apps.Core.MemCache.GetMemDBCruzNews10BixbyData(age, gen);
              //  List<Joins.Apps.Models.News10.Cruz.News10_NOContentTTS> CruzNew10DataList = Joins.Apps.Core.MemCache.GetMemDBCruzNews10BixbyData2(age, gen);
                returnValue = CruzNew10DataList;
            } catch (ArgumentException) { returnValue = null; }

        //    return Ok(new { News10 = returnValue });

            if (returnValue != null && returnValue.Count > 5 && !string.IsNullOrEmpty(returnValue[0].id)) {
                r.r_code = "1";
                r.r_msg = "성공";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = returnValue.Count;
               
                r.News10 = returnValue;
            }
            else {
                r.r_code = "0";
                r.r_msg = "실패";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = 0;
            }
            return Ok(r);
        }
        [HttpGet]
        [Route("Briefing/List/Bixby3")]
        public IHttpActionResult Bixby3(int gen = 1, int age = 3) {
            return Ok();
            Joins.Apps.Models.News10.returnData2<List<Joins.Apps.Models.News10.Cruz.News10_NOContentTTS>> r = new Joins.Apps.Models.News10.returnData2<List<Joins.Apps.Models.News10.Cruz.News10_NOContentTTS>>();
            List<Joins.Apps.Models.News10.Cruz.News10_NOContentTTS> returnValue = null;
            try {
                Joins.Apps.Common.Logger.LogWriteGroup("News10_Bixby3", "gen:" + gen + " age:" + age);
                
                List<Joins.Apps.Models.News10.Cruz.News10_NOContentTTS> CruzNew10DataList = Joins.Apps.Core.MemCache.GetMemDBCruzNews10BixbyData2(age, gen);
               // List<Joins.Apps.Models.News10.Cruz.News10_NOContentTTS> CruzNew10DataList = Joins.Apps.Repository.News10.CruzRepository.GetCruzNews10NoContentTTSList(gen, age, true);
                returnValue = CruzNew10DataList;
            } catch (ArgumentException) { returnValue = null; }

            //    return Ok(new { News10 = returnValue });

            if (returnValue != null && returnValue.Count > 5 && !string.IsNullOrEmpty(returnValue[0].id)) {
                r.r_code = "1";
                r.r_msg = "성공";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = returnValue.Count;

                r.News10 = returnValue;
            }
            else {
                r.r_code = "0";
                r.r_msg = "실패";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = 0;
            }
            return Ok(r);
        }
        //[HttpGet]
        //[Route("Briefing/List/Bixby3")]
        //public IHttpActionResult Bixby3(int gen = 1, int age = 3) {
        //    Joins.Apps.Models.News10.returnData2<List<Joins.Apps.Models.News10.Cruz.News10_NOContentTTS>> r = new Joins.Apps.Models.News10.returnData2<List<Joins.Apps.Models.News10.Cruz.News10_NOContentTTS>>();
        //    List<Joins.Apps.Models.News10.Cruz.News10_NOContentTTS> returnValue = null;
        //    try {
        //        Joins.Apps.Common.Logger.LogWriteGroup("News10_Bixby3", "gen:" + gen + " age:" + age);
        //        //  List<Joins.Apps.Models.News10.Cruz.News10> CruzNew10DataList = Joins.Apps.Core.MemCache.GetMemDBCruzNews10BixbyData(age, gen);
        //        List<Joins.Apps.Models.News10.Cruz.News10_NOContentTTS> CruzNew10DataList = Joins.Apps.Repository.News10.CruzRepository.GetCruzNews10NoContentTTSList(age, gen);//news10 브리핑
        //        returnValue = CruzNew10DataList;
        //    } catch (ArgumentException) { returnValue = null; }

        //    //    return Ok(new { News10 = returnValue });

        //    if (returnValue != null && returnValue.Count > 5 && !string.IsNullOrEmpty(returnValue[0].id)) {
        //        r.r_code = "1";
        //        r.r_msg = "성공";
        //        r.r_dt = DateTime.Now.ToString();
        //        r.r_tcnt = returnValue.Count;

        //        r.News10 = returnValue;
        //    }
        //    else {
        //        r.r_code = "0";
        //        r.r_msg = "실패";
        //        r.r_dt = DateTime.Now.ToString();
        //        r.r_tcnt = 0;
        //    }
        //    return Ok(r);
        //}
        //[HttpGet]
        //[Route("Briefing/List/SmartTv")]
        //public IHttpActionResult SmartTv(int gen = 1, int age = 3) {
        //    string sCruz = Joins.Apps.Core.MemCache.GetMemCruzCall(age, gen);
        //    List<Joins.Apps.Models.News10.Cruz2.News10> returnValue = null;
        //    try {
        //        Joins.Apps.Common.Logger.LogWriteGroup("News10_SmartTv", "gen:" + gen + " age:" + age);
        //        Joins.Apps.Models.News10.Cruz2 cruz = JsonConvert.DeserializeObject<Joins.Apps.Models.News10.Cruz2>(sCruz);
        //        returnValue = cruz.news10;
        //    } catch (ArgumentException) { returnValue = null; }

        //    return Ok(new { News10 = returnValue });
        //}
        [HttpGet]
        [Route("Briefing/List/SmartTv")]
        public IHttpActionResult SmartTv(int gen = 1, int age = 3) {
            return Ok();
            List<Joins.Apps.Models.News10.Cruz2.News10> returnValue = null;
            try {
                Joins.Apps.Common.Logger.LogWriteGroup("News10_SmartTv", "gen:" + gen + " age:" + age);
                List<Joins.Apps.Models.News10.Cruz2.News10> CruzNew10DataList = Joins.Apps.Core.MemCache.GetMemDBCruzNews10Data2(age, gen);
                returnValue = CruzNew10DataList;
            } catch (ArgumentException) { returnValue = null; }

            return Ok(new { News10 = returnValue });
        }
        #endregion
    }
}
