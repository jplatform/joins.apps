﻿using Joins.Apps.Models.News10;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace Joins.Apps.API.Controllers.News10
{
    public class CruzController : ApiController{
        //[HttpGet]
        //[Route("News10/Cruz")]
        //public IHttpActionResult Index(int gen = 1, int age = 3, string deviceID = "") {

        //    Joins.Apps.Models.News10.Cruz returnValue = Joins.Apps.Core.MemCache.GetMemCruzData(age, gen);
        //    return Ok(returnValue);
        //}
        /// <summary>
        /// 현재 브리핑
        /// </summary>
        /// <param name="gen"></param>
        /// <param name="age"></param>
        /// <param name="deviceID"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("News10/Brief")]
        public IHttpActionResult Brief(int gen = 1, int age = 3, string deviceID = "") {
            return Ok();
            Joins.Apps.Models.News10.returnData<List<Joins.Apps.Models.News10.Cruz.News10>> r = new returnData<List<Joins.Apps.Models.News10.Cruz.News10>>();
            Joins.Apps.Models.News10.Cruz returnValue = new Joins.Apps.Models.News10.Cruz();
            List<Joins.Apps.Models.News10.Cruz.News10> CruzNew10DataList = Joins.Apps.Core.MemCache.GetMemDBCruzNews10Data(age, gen);//news10 브리핑

            Joins.Apps.Models.News10.CardInfo cardinfo = Joins.Apps.Core.MemCache.GetMemCardInfo("1");
            if (CruzNew10DataList != null && CruzNew10DataList.Count > 5 && !string.IsNullOrEmpty(CruzNew10DataList[0].id)) {
                r.r_code = "1";
                r.r_msg = "성공";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = CruzNew10DataList.Count;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
                r.data = CruzNew10DataList;
            }else {
                r.r_code = "0";
                r.r_msg = "실패";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = 0;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
            }
            return Ok(r);
        }
        [HttpGet]
        [Route("News10/Brief/Now")]
        public IHttpActionResult BriefTest(int gen = 1, int age = 3, string deviceID = "") {
            return Ok();
            Joins.Apps.Models.News10.returnData<List<Joins.Apps.Models.News10.Cruz.News10>> r = new returnData<List<Joins.Apps.Models.News10.Cruz.News10>>();
            Joins.Apps.Models.News10.Cruz returnValue = new Joins.Apps.Models.News10.Cruz();
            List<Joins.Apps.Models.News10.Cruz.News10> CruzNew10DataList = Joins.Apps.Repository.News10.CruzRepository.GetCruzNews10ListTest(gen, age);//news10 브리핑
            Joins.Apps.Models.News10.CardInfo cardinfo = Joins.Apps.Core.MemCache.GetMemCardInfo("1");
            if (CruzNew10DataList != null && CruzNew10DataList.Count > 5 && !string.IsNullOrEmpty(CruzNew10DataList[0].id)) {
                r.r_code = "1";
                r.r_msg = "성공";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = CruzNew10DataList.Count;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
                r.data = CruzNew10DataList;
            }
            else {
                r.r_code = "0";
                r.r_msg = "실패";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = 0;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
            }
            return Ok(r);
        }

        [HttpGet]
        [Route("News10/Brief2")]
        public IHttpActionResult Brief2(int gen = 1, int age = 3, string deviceID = "") {
            return Ok();
            Joins.Apps.Models.News10.returnData<List<Joins.Apps.Models.News10.Cruz.News10_NOTTS>> r = new returnData<List<Joins.Apps.Models.News10.Cruz.News10_NOTTS>>();
            Joins.Apps.Models.News10.Cruz returnValue = new Joins.Apps.Models.News10.Cruz();
          //  List<Joins.Apps.Models.News10.Cruz.News10_NOTTS> CruzNew10DataList = Joins.Apps.Core.MemCache.GetMemDBCruzNews10NoTTSData(age, gen);//news10 브리핑
            List<Joins.Apps.Models.News10.Cruz.News10_NOTTS> CruzNew10DataList = Joins.Apps.Repository.News10.CruzRepository.GetCruzNews10NoTTSList(age, gen);//news10 브리핑
            Joins.Apps.Models.News10.CardInfo cardinfo = Joins.Apps.Core.MemCache.GetMemCardInfo("1");
            if (CruzNew10DataList != null && CruzNew10DataList.Count > 5 && !string.IsNullOrEmpty(CruzNew10DataList[0].id)) {
                r.r_code = "1";
                r.r_msg = "성공";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = CruzNew10DataList.Count;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
                r.data = CruzNew10DataList;
            }
            else {
                r.r_code = "0";
                r.r_msg = "실패";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = 0;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
            }
            return Ok(r);
        }
        [HttpGet]
        [Route("News10/Brief/Tts")]
        public IHttpActionResult Tts(int gen = 1, int age = 3, string deviceID = "") {
            Joins.Apps.Models.News10.returnData<List<Joins.Apps.Models.News10.Cruz.TTS>> r = new returnData<List<Joins.Apps.Models.News10.Cruz.TTS>>();
            Joins.Apps.Models.News10.Cruz returnValue = new Joins.Apps.Models.News10.Cruz();
         //   List<Joins.Apps.Models.News10.Cruz.TTS> CruzNew10DataList = Joins.Apps.Repository.News10.CruzRepository.GetCruzNews10OnlyTTSList(age, gen);//news10 브리핑
            List<Joins.Apps.Models.News10.Cruz.TTS> CruzNew10DataList = Joins.Apps.Core.MemCache.GetMemDBCruzNews10OnlyTTSData(age, gen);//news10 브리핑
            
            Joins.Apps.Models.News10.CardInfo cardinfo = Joins.Apps.Core.MemCache.GetMemCardInfo("1");
            if (CruzNew10DataList != null && CruzNew10DataList.Count > 5 && !string.IsNullOrEmpty(CruzNew10DataList[0].title)) {
                r.r_code = "1";
                r.r_msg = "성공";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = CruzNew10DataList.Count;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
                r.data = CruzNew10DataList;
            }
            else {
                r.r_code = "0";
                r.r_msg = "실패";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = 0;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
            }
            return Ok(r);
        }
        [HttpGet]
        [Route("News10/Cruz")]
        public IHttpActionResult Index(int gen = 1, int age = 3, string deviceID = "",int cardVersion=0) {
            return Ok();
            Joins.Apps.Models.News10.Cruz returnValue = new Joins.Apps.Models.News10.Cruz();
            List<Joins.Apps.Models.News10.Cruz.Special> CruzSpecialDataList = Joins.Apps.Core.MemCache.GetMemDBCruzSpecialData(age, gen, cardVersion);//스페셜
            if (cardVersion >= 10) { returnValue.news10 = null; }
            else {
                List<Joins.Apps.Models.News10.Cruz.News10> CruzNew10DataList = Joins.Apps.Core.MemCache.GetMemDBCruzNews10Data(age, gen);//news10 브리핑
                returnValue.news10 = CruzNew10DataList;
            }
            if (cardVersion >= 10 && CruzSpecialDataList != null) {CruzSpecialDataList = CruzSpecialDataList.OrderBy(a => Guid.NewGuid()).ToList(); }
            List<Joins.Apps.Models.News10.Cruz.HotKwd> CruzkeywodaList = Joins.Apps.Core.MemCache.GetMemDBCruzkeywordData();//키워드 이슈
            returnValue.special = CruzSpecialDataList;
            returnValue.hotkwd = CruzkeywodaList;
            returnValue.hotvod = null;
            return Ok(returnValue);
        }

        [HttpGet]
        [Route("News10/Cruz/Test")]
        public IHttpActionResult Test(int gen = 1, int age = 3, string deviceID = "",int cardVersion=0) {
            Joins.Apps.Models.News10.Cruz returnValue = new Joins.Apps.Models.News10.Cruz();
            List<Joins.Apps.Models.News10.Cruz.News10> CruzNew10DataList = Joins.Apps.Repository.News10.CruzRepository.GetCruzNews10ListTest(gen, age);//news10 브리핑
            List<Joins.Apps.Models.News10.Cruz.Special> CruzSpecialDataList = Joins.Apps.Repository.News10.CruzRepository.GetCruzSpecialList(gen, age, 0);
            List<Joins.Apps.Models.News10.Cruz.HotKwd> CruzkeywodaList = Joins.Apps.Core.MemCache.GetMemDBCruzkeywordData();//키워드 이슈
        //    Joins.Apps.Models.News10.Cruz OldCruz = Joins.Apps.Core.MemCache.GetMemCruzData(age, gen);//이슈와 vod
            returnValue.hotkwd = null;
            returnValue.hotvod = null;
          //  if (OldCruz != null && OldCruz.hotkwd != null && OldCruz.hotkwd.Count > 0) { returnValue.hotkwd = OldCruz.hotkwd; }
          //  if (OldCruz != null && OldCruz.hotvod != null && OldCruz.hotvod.Count > 0) { returnValue.hotvod = OldCruz.hotvod; }
            returnValue.news10 = CruzNew10DataList;
            returnValue.special = CruzSpecialDataList;
            return Ok(returnValue);
        }

        
        [HttpGet]
        [Route("News10/Cruz/Score")]
        public IHttpActionResult Score(int gen = 1, int age = 3, string deviceID = "") {
            string returnValue = Joins.Apps.Core.MemCache.GetMemCruzScoreData();
            return Ok(Newtonsoft.Json.Linq.JObject.Parse(returnValue));
        }
        [HttpGet]
        [Route("News10/Cruz/BoxOffice")]
        public IHttpActionResult BoxOffice(int gen = 1, int age = 3, string deviceID = "") {
            Joins.Apps.Models.News10.BoxOffice.P351Response result = new Joins.Apps.Models.News10.BoxOffice.P351Response();
         //   Joins.Apps.Models.News10.BoxOffice.P351Response returnValue = Joins.Apps.Core.MemCache.GetMemCruzBoxOffice();
            //return Ok(returnValue);

            return Ok(result);
        }
        //[HttpGet]
        //[Route("News10/Cruz/Pyeongchang2")]
        //public IHttpActionResult Pyeongchang2(string deviceID = "") {
        //    Joins.Apps.Models.News10.PyeongchangOutput returnValue = Joins.Apps.Core.MemCache.GetMemCruzPyeongchang();
        //    returnValue.data = returnValue.data.OrderBy(a => Guid.NewGuid()).ToList();
        //    return Ok(new { Pyeongchang = returnValue });
        //}
        [HttpGet]
        [Route("News10/Cruz/Pyeongchang")]
        public IHttpActionResult Pyeongchang(string deviceID = "") {
            Joins.Apps.Models.News10.PyeongchangOutput returnValue = Joins.Apps.Core.MemCache.GetMemPyeongchangBrief();
            returnValue.data = returnValue.data.OrderBy(a => Guid.NewGuid()).ToList();
            return Ok(new { Pyeongchang = returnValue });
        }
        [HttpGet]
        [Route("News10/Cruz/JtbcVod")]
        public IHttpActionResult JtbcVod(string deviceID = "") {
            List<Joins.Apps.Models.News10.Cruz.HotVod> returnValue = Joins.Apps.Core.MemCache.GetMemJtbcVod();
            returnValue = returnValue.OrderBy(a => Guid.NewGuid()).ToList();
            return Ok(new { HotVod = returnValue });
        }
        [HttpGet]
        [Route("News10/Cruz/JtbcVod2")]
        public IHttpActionResult JtbcVod2(string deviceID = "") {
            List<Joins.Apps.Models.News10.Cruz.HotVod> jtbcvodData = Joins.Apps.Core.MemCache.GetMemJtbcVod();

            Joins.Apps.Models.News10.Cruz.HotVod jtbc1min = Joins.Apps.Core.MemCache.GetMemJTBC1MinData();
            jtbcvodData = jtbcvodData.OrderBy(a => Guid.NewGuid()).ToList();
          //  jtbc1min = null;
            if (jtbc1min != null && !string.IsNullOrEmpty(jtbc1min.title)) { jtbcvodData.Insert(0, jtbc1min); }
          //  return Ok(new { HotVod = returnValue });

            Joins.Apps.Models.News10.returnData<List<Joins.Apps.Models.News10.Cruz.HotVod>> r = new returnData<List<Joins.Apps.Models.News10.Cruz.HotVod>>();
            Joins.Apps.Models.News10.CardInfo cardinfo = Joins.Apps.Core.MemCache.GetMemCardInfo("31");
            if (jtbcvodData != null && jtbcvodData.Count > 0 && !string.IsNullOrEmpty(jtbcvodData[0].title)) {
                r.r_code = "1";
                r.r_msg = "성공";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = jtbcvodData.Count;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
                r.data = jtbcvodData;
            }
            else {
                r.r_code = "0";
                r.r_msg = "실패";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = 0;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
            }
            return Ok(r);
        }
        #region[이슈패키지]
        [HttpGet]
        [Route("News10/Cruz/IssuePack")]
        public IHttpActionResult IssuePack(string deviceID = "") {
            List<Joins.Apps.Models.News10.Cruz3.issue> issData = Joins.Apps.Core.MemCache.GetMemIssuePackData();
            Joins.Apps.Models.News10.returnData<List<Joins.Apps.Models.News10.Cruz3.issue>> r = new returnData<List<Joins.Apps.Models.News10.Cruz3.issue>>();
            Joins.Apps.Models.News10.CardInfo cardinfo = Joins.Apps.Core.MemCache.GetMemCardInfo("36");
            if (issData != null && issData.Count > 0 && !string.IsNullOrEmpty(issData[0].iss_name)) {
                r.r_code = "1";
                r.r_msg = "성공";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = issData.Count;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
                r.data = issData;
            }
            else {
                r.r_code = "0";
                r.r_msg = "실패";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = 0;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
            }
            return Ok(r);
        }
        #endregion

        #region[Mnb Magazine special]
        [HttpGet]
        [Route("News10/Cruz/MnbSpecial")]
        public IHttpActionResult MnbSpecial(string deviceID = "") {
            List<Joins.Apps.Models.News10.Cruz3.MnBSpecial> issData = Joins.Apps.Core.MemCache.GetMemMnBSpecialData();
            Joins.Apps.Models.News10.returnData<List<Joins.Apps.Models.News10.Cruz3.MnBSpecial>> r = new returnData<List<Joins.Apps.Models.News10.Cruz3.MnBSpecial>>();
            Joins.Apps.Models.News10.CardInfo cardinfo = Joins.Apps.Core.MemCache.GetMemCardInfo("39");
            if (issData != null && issData.Count > 0 && !string.IsNullOrEmpty(issData[0].title)) {
                r.r_code = "1";
                r.r_msg = "성공";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = issData.Count;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
                r.data = issData;
            }
            else {
                r.r_code = "0";
                r.r_msg = "실패";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = 0;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
            }
            return Ok(r);
        }
        #endregion

        
    }
}
