﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Joins.Apps.Models.News10;

namespace Joins.Apps.API.Controllers.News10
{
    public class CommonController : ApiController{
        #region 앱 - 정보

        [HttpGet]
        [Route("news10/Common/AppInfo")]
        public IHttpActionResult AppInfo(string deviceType = null) {
            return Ok();
            List<Joins.Apps.Models.News10.AppInfoData> returnValue = null;
            try {
                returnValue = Joins.Apps.Repository.News10.CommonRepository.AppInfo(deviceType);
                if (returnValue.Count == 0)
                    throw new ArgumentException();
            } catch (ArgumentException) {
                returnValue = null;
            }

            return Ok(new { AppInfo = returnValue });
        }

        #endregion

        #region 앱 - 디바이스 - 조회

        [HttpGet]
        [Route("news10/Common/AppDevice")]
        public IHttpActionResult AppDevice([FromUri] AppDeviceParameters parameters) {
            return Ok();
            AppDeviceData returnValue = null;

            try {
                if (string.IsNullOrEmpty(parameters.deviceID))
                    throw new ArgumentException();
                returnValue = Joins.Apps.Repository.News10.CommonRepository.AppDevice(parameters);
            } catch (ArgumentException) {
                returnValue = null;
            }

            return Ok(returnValue);
        }

        #endregion

        #region 앱 - 디바이스 - 업데이트

        [HttpPost]
        [Route("news10/Common/AppDeviceUpdate")]
        public IHttpActionResult AppDeviceUpdate([FromBody] AppDeviceUpdateParameters parameters) {
            return Ok();
            bool returnValue = true;

            try {
                if (string.IsNullOrEmpty(parameters.deviceID) || string.IsNullOrEmpty(parameters.deviceType))
                    throw new ArgumentException();
                returnValue = Joins.Apps.Repository.News10.CommonRepository.AppDeviceUpdate(parameters);
            } catch (ArgumentException) {
                returnValue = false;
            }

            return Ok(new { result = returnValue });
        }

        #endregion

        #region 앱 - 디바이스 - 부가정보 - 업데이트

        [HttpPost]
        [Route("news10/Common/AppDeviceAdditionUpdate")]
        public IHttpActionResult AppDeviceAdditionUpdate([FromBody] AppDeviceUpdateParameters parameters) {
            return Ok();
            bool returnValue = true;

            try {
                if (string.IsNullOrEmpty(parameters.deviceID) || string.IsNullOrEmpty(parameters.deviceType))
                    throw new ArgumentException();
                returnValue = Joins.Apps.Repository.News10.CommonRepository.AppDeviceAdditionUpdate(parameters);
            } catch (ArgumentException) {
                returnValue = false;
            }

            return Ok(new { result = returnValue });
        }

        #endregion

        #region 앱 - 디바이스 - 푸시 - 업데이트

        [HttpPost]
        [Route("news10/Common/AppDevicePushUpdate")]
        public IHttpActionResult AppDevicePushUpdate([FromBody] AppDevicePushUpdateParameters parameters) {
            return Ok();
            bool returnValue = true;

            try {
                if (string.IsNullOrEmpty(parameters.deviceID) || string.IsNullOrEmpty(parameters.pushType) || string.IsNullOrEmpty(parameters.pushFlag))
                    throw new ArgumentException();

                if (parameters.pushType == "briefing") {
                    if (!string.IsNullOrEmpty(parameters.pushFlag) && (parameters.pushFlag).Equals("N")) {
                        parameters.pushType = "morning";
                        parameters.pushTime = "06:00";//06:00
                        Joins.Apps.Repository.News10.CommonRepository.AppDevicePushUpdate(parameters);
                        parameters.pushType = "afternoon";
                        parameters.pushTime = "12:00";
                        Joins.Apps.Repository.News10.CommonRepository.AppDevicePushUpdate(parameters);
                        parameters.pushType = "evening";
                        parameters.pushTime = "18:00";//18:00
                        Joins.Apps.Repository.News10.CommonRepository.AppDevicePushUpdate(parameters);
                        parameters.pushType = "night";
                        parameters.pushTime = "00:00";
                        Joins.Apps.Repository.News10.CommonRepository.AppDevicePushUpdate(parameters);
                        parameters.pushType = "noti";
                        parameters.pushTime = "";
                        Joins.Apps.Repository.News10.CommonRepository.AppDevicePushUpdate(parameters);
                    }else {
                        parameters.pushType = "morning";
                        parameters.pushTime = "06:00";//06:00
                        Joins.Apps.Repository.News10.CommonRepository.AppDevicePushUpdate(parameters);
                        parameters.pushType = "afternoon";
                        parameters.pushTime = "12:00";
                        Joins.Apps.Repository.News10.CommonRepository.AppDevicePushUpdate(parameters);
                        parameters.pushType = "evening";
                        parameters.pushTime = "18:00";//18:00
                        Joins.Apps.Repository.News10.CommonRepository.AppDevicePushUpdate(parameters);
                        parameters.pushType = "night";
                        parameters.pushTime = "00:00";
                        parameters.pushFlag = "N";
                        Joins.Apps.Repository.News10.CommonRepository.AppDevicePushUpdate(parameters);
                        parameters.pushType = "noti";
                        parameters.pushTime = "";
                        parameters.pushFlag = "Y";
                        Joins.Apps.Repository.News10.CommonRepository.AppDevicePushUpdate(parameters);
                    }
                }
                else {
                    if (string.IsNullOrEmpty(parameters.pushTime)) {
                        switch (parameters.pushType) {
                            case "morning":
                                parameters.pushTime = "06:00";//06:00
                                break;
                            case "afternoon":
                                parameters.pushTime = "12:00";
                                break;
                            case "evening":
                                parameters.pushTime = "18:00";//18:00
                                break;
                            case "night":
                                parameters.pushTime = "00:00";
                                break;
                        }
                    }
                    returnValue = Joins.Apps.Repository.News10.CommonRepository.AppDevicePushUpdate(parameters);
                }
            } catch (ArgumentException) {
                returnValue = false;
            }

            return Ok(new { result = returnValue });
        }

        #endregion

        #region 앱 - api 호출 Log
        [HttpPost]
        [Route("news10/Common/AppApiLog")]
        public IHttpActionResult AppApiLog([FromBody] AppApiLogParameters parameters) {
            return Ok();
            bool returnValue = true;
            try {
                if (string.IsNullOrEmpty(parameters.deviceID) || string.IsNullOrEmpty(parameters.apiurl))
                    throw new ArgumentException();
                returnValue = Joins.Apps.Repository.News10.CommonRepository.AppApiLogInert(parameters);
            } catch (ArgumentException) {
                returnValue = false;
            }

            return Ok(new { result = returnValue });
        }
        [HttpPost]
        [Route("news10/Common/AppLog")]
        public IHttpActionResult AppLog([FromBody] ArticleLogParameters parameters) {
            return Ok();
            bool returnValue = true;
            try {
                if (string.IsNullOrEmpty(parameters.deviceID) || string.IsNullOrEmpty(parameters.cardID))
                    throw new ArgumentException();
                returnValue = Joins.Apps.Repository.News10.CommonRepository.ArticleLogInert(parameters);
            } catch (ArgumentException) {returnValue = false;}
            return Ok(new { result = returnValue });
        }
        #endregion

        #region [좋아요]
        [HttpGet]
        [Route("news10/Common/ArticleLikeInfo")]
        public IHttpActionResult ArticleLikeInfo(int totalID,string deviceID,string cardID="") {
            return Ok();
            Joins.Apps.Models.News10.ArticleLikeData returnValue = Joins.Apps.Repository.News10.CommonRepository.GetArticleLikeInfo(totalID, deviceID);
          
            Joins.Apps.Models.News10.returnData<Joins.Apps.Models.News10.ArticleLikeData> r = new returnData<Joins.Apps.Models.News10.ArticleLikeData>();
            if (returnValue != null && returnValue.total_likecnt >= 0) {
                r.r_code = "1";
                r.r_msg = "성공";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = 1;
                r.cardinfo = null;
                r.data = returnValue;
            }
            else {
                r.r_code = "0";
                r.r_msg = "실패";
                r.r_dt = DateTime.Now.ToString();
                r.cardinfo = null;
                r.r_tcnt = 0;               
            }
            return Ok(r);
        }
        [HttpPost]
        [Route("news10/Common/ArticleLikeUpdate")]
        public IHttpActionResult ArticleLikeUpdate([FromBody] ArticleLikeParam parameters) {
            return Ok();
            bool returnValue = true;
            returnValue = Joins.Apps.Repository.News10.CommonRepository.SetArticleLikeUpdate(parameters);

            return Ok(new { result = returnValue });
        }
            #endregion
        }
}
