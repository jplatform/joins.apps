﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Joins.Apps.Models.News10;

namespace Joins.Apps.API.Controllers.News10
{
    public class EtcController : ApiController{
        #region 오늘

        [HttpGet]
        [Route("news10/Etc/Today")]
        public IHttpActionResult Today([FromUri] WeatherParameters parametersWeather, [FromUri] SpecialDayParameters parametersSpecialDay,string deviceID="") {
            return Ok();
            Today returnValue = new Today();
            WeatherData weather= new WeatherData();
            CoverInfo coverinfo =null;
            List<Joins.Apps.Models.News10.ShowCase> showcaseImgList = Joins.Apps.Repository.News10.ShowCaseRepository.GetShowCaseInfo();
            List<string> areaList = new List<string>() { "서울", "부산", "대구", "인천", "광주", "대전", "울산", "세종", "경기", "강원", "제주","충북","충남","전북","전남" ,"경북","경남", "충청북도", "충청남도", "전라북도", "전라남도", "경상북도", "경상남도" };
            string sArea = "서울";
            if (parametersWeather == null) { parametersWeather = new WeatherParameters { admin = "서울" }; }
            if (!string.IsNullOrEmpty(parametersWeather.admin)) {
                foreach (string area in areaList) { if (parametersWeather.admin.IndexOf(area) >= 0) { sArea = area; } }
            }
            //날씨
            try {
              //  if (parametersWeather == null || parametersWeather.admin == null)
               //     throw new ArgumentException();

                weather = Joins.Apps.Repository.News10.EtcRepository.Weather(sArea);
                //if (string.IsNullOrEmpty(weather.admin))
                //    throw new ArgumentException();
            } catch (ArgumentException) {}
            if (weather != null) {
                returnValue.Weather = weather;
            }
            string sMessage = "",sBrief= "";
            DateTime today = DateTime.Now;
            //if (6 <= today.Hour && today.Hour < 12) {sBrief = " 모닝브리핑을 마치겠습니다.";}
            //else if (12 <= today.Hour && today.Hour < 18) {sBrief = " 애프터눈브리핑을 마치겠습니다.";}
            //else if (18 <= today.Hour && today.Hour <= 23) { sBrief = " 이브닝브리핑을 마치겠습니다."; }
            //else if (0 <= today.Hour && today.Hour < 6) { sBrief = " 나이트브리핑을 마치겠습니다."; }

            if (weather != null && !string.IsNullOrEmpty(weather.admin)) {
                bool bEmRain = false;
                string sToday = today.ToString("M월 dd일");
                string sRainMessage = "";
                if (weather.condition == "5" || weather.condition == "9" || weather.condition == "10") {bEmRain = true;}
                if (weather.Forecast != null && weather.Forecast.Count > 0) {
                    foreach (WeatherForecastData forecast in weather.Forecast) {
                        if (Convert.ToInt16(forecast.rain) >= 60) {
                            if (forecast.condition == "5" || forecast.condition == "9" || forecast.condition == "10") { bEmRain = true; }
                            break;
                        }
                    }
                }
              if (bEmRain && (today.Hour >= 6 && today.Hour <= 23)) {sRainMessage = "비 소식이 있으니 외출하실 때 우산을 준비하세요. ";}
                string ttemp = (weather.temp).Replace("-", "영하");
                if (string.IsNullOrEmpty(weather.pm10Grade)) {
                    sMessage = "현재 " + sArea + " 기온은 " + ttemp + "도입니다. " + sRainMessage;}
                else {
                    if (string.IsNullOrEmpty(weather.temp)) {
                        sMessage = "현재 " + sArea + " 미세먼지 농도는 " + weather.pm10Grade + "입니다. " + sRainMessage;
                    }else {
                       
                        sMessage = "현재 " + sArea + " 기온은 " + ttemp + "도이며 미세먼지 농도는 " + weather.pm10Grade + "입니다. " + sRainMessage;
                    }
                }
            }else {sMessage = sBrief;}
            returnValue.endingMessage = sMessage;
            /*
                맑음 (대체로 맑음)
                구름 조금
                구름 많음
                흐림 (대체로 흐림)
                흐리고 비 5
                흐리고 눈 6
                비온후 갬
                소나기
                비 또는 눈 9
                눈 또는 비 10
                천둥번개
                안개
                1 2 3 4 5 6 7 8 9 10 11
             */
            bool bRain = false;
            bool bSnow = false;
            if (weather != null) {
                switch (weather.condition) {
                    case "5":
                        bRain = true;
                        break;
                    case "6":
                        bSnow = true;
                        break;
                }
            }
            string sCimg = "";
            if (showcaseImgList.Count > 0) {

                    foreach (Joins.Apps.Models.News10.ShowCase showimg in showcaseImgList) {
                        if (string.IsNullOrEmpty(showimg.IMG)) { continue; }
                        if (!string.IsNullOrEmpty(showimg.SPECIAL_DAY)) {
                                sCimg = showimg.IMG;
                                break;
                        }
                        if ((showimg.BASIC_YN).Equals("Y")) { sCimg = showimg.IMG; }
                    }

                    foreach (Joins.Apps.Models.News10.ShowCase showimg in showcaseImgList) {
                        if (string.IsNullOrEmpty(showimg.IMG)) { continue; }
                        if (bRain) {
                            if (showimg.WEATHER_TYPE == "1") {
                                sCimg = showimg.IMG;
                                if (!(showimg.BRIEF_TYPE).Equals("0")) {break; }//현재 브리핑 날씨이면 중지
                        }
                        }
                        if (bSnow) {
                            if (showimg.WEATHER_TYPE == "2") {
                                sCimg = showimg.IMG;
                                if (!(showimg.BRIEF_TYPE).Equals("0")) { break; }//현재 브리핑 날씨이면 중지
                        }
                        }
                   }
            }
         
            coverinfo = new CoverInfo(){ coverimg = sCimg ,msg=""};
            returnValue.Coverinfo = coverinfo;

            //기념일
            try {
                if (parametersSpecialDay == null || parametersSpecialDay.day == null) {
                    parametersSpecialDay = new SpecialDayParameters() {
                        day = DateTime.Today.ToString("yyyy-MM-dd"),
                       // service = "Y"
                    };
                }

                returnValue.Anniversary = Joins.Apps.Repository.News10.EtcRepository.SpecialDay(parametersSpecialDay);
                if (returnValue.Anniversary.Count == 0)
                    throw new ArgumentException();
            } catch (ArgumentException) {
                returnValue.Anniversary = null;
            }
            //  DateTime.Now.ToString("yyyy-MM-dd")
            //     "2017-02-09"

            //   JCube.AF.Util.Converts.ToDateTime("2017-02-09")
            DateTime dday = JCube.AF.Util.Converts.ToDateTime("2018-02-09");
            DateTime nowday = JCube.AF.Util.Converts.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"));
            int dd = DateTime.Compare(nowday, dday);
            string sMsg = "";
            if (dd < 0) {
                TimeSpan ts = dday - nowday;
                sMsg = "D-" + ts.Days;
            }
            //HotKeyInfo hotKeyInfo = new HotKeyInfo{
            //    img = "http://images.joins.com/News10/cover/badge_trend.png",
            //    msg = sMsg,
            //    type="0",
            //    link ="39"
            //};
           // returnValue.Hotkeyinfo = hotKeyInfo;
            #region 미세먼지
            Joins.Apps.Models.News10.AirKoreaOutput airinfo= Joins.Apps.Core.MemCache.GetMemAirinfoData(sArea);
            if(airinfo != null){returnValue.Airinfo = airinfo;}
            #endregion
            return Ok(returnValue);
        }

        #endregion
      
    }
}
