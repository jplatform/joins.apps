﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using Joins.Apps.Models.News10;
using Joins.Apps.Repository.Search;
using Joins.Apps.Models.Search;

namespace Joins.Apps.API.Controllers.News10
{
    public class CardController : ApiController{

        #region 카드 리스트
        [HttpGet]
        [Route("News10/Card/CardList")]
        public IHttpActionResult CardList(int cardVersion = 1,string deviceID="") {
            return Ok();
            List<Joins.Apps.Models.News10.CardList> returnValue = null;
            try {
                //returnValue = Joins.Apps.Repository.News10.CardRepository.CardList(cardVersion);
                returnValue = Joins.Apps.Repository.News10.CardRepository.MyCardList(cardVersion, deviceID);
                if (returnValue.Count == 0)
                    throw new ArgumentException();
            } catch (ArgumentException) {
                returnValue = null;
            }

            return Ok(new { CardList = returnValue });
        }
        [HttpPost]
        [Route("News10/Card/CardListUpdate")]
        public IHttpActionResult MyCardListUpdate(CardListUpdateParam param) {
            bool returnValue = false;
            try {
                if (string.IsNullOrEmpty(param.deviceID) || string.IsNullOrEmpty(param.cardID) || string.IsNullOrEmpty(param.usedYn)) { }
                else {returnValue = Joins.Apps.Repository.News10.CardRepository.SetMyCardListYN(param.deviceID, param.cardID, param.usedYn);}
            } catch (ArgumentException) {}

            return Ok(new { CardList = returnValue });
        }
        #endregion

        #region 카드 - 기사 (역사, 칼럼, 속보)
        [HttpGet]
        [Route("News10/Card/Article")]
        public IHttpActionResult Article(string deviceID = "") {
            Joins.Apps.Models.News10.Card returnValue = new Joins.Apps.Models.News10.Card();

            //역사
            //try {

            //     SearchRepository schRepository =new SearchRepository(new SearchCondition {
            //         Page = 1,
            //         PageSize = 10,
            //         IsNeedTotalCount = false,
            //         Keyword = DateTime.Today.ToString("MM월dd일"),
            //         SearchCategoryType = SearchCategoryType.IssueNews,
            //         SortType = SortType.New,
            //         ScopeType = ScopeType.Keyword,
            //         SourceCode = "j9"
            //     });
            //    DefaultResult searchHistory = schRepository.GetSearchResult();
            //     //searchHistory
            //     if (!searchHistory.SearchItems.Any())
            //         throw new ArgumentException();

            //     var historyRgx = new Regex(@"[가-힣]{2}\s*\d{4}.?\d{2}.?\d{2}\s*", RegexOptions.IgnoreCase);

            //     List<ArticleData> returnHistory = new List<ArticleData>();
            //     foreach (SearchItem item in searchHistory.SearchItems) {
            //       //  if (item.Id == 20904210) { continue; }
            //         string historySummary = item.Summary;
            //         if (item.Id > 0) {
            //             ArticleData historyContent = Joins.Apps.Repository.News10.ArticleRepository.View(new ArticleParameters { totalID = item.Id });
            //             if (string.IsNullOrEmpty(historySummary)) {historySummary = historyContent.content;}
            //         }
            //         if (!string.IsNullOrEmpty(historySummary)) {
            //             historySummary = historyRgx.Replace(historySummary, "").TrimStart();
            //         }
            //         if (string.IsNullOrEmpty(item.Thumbnail)) { continue; }
            //         string sTitle =Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(item.MobileTitle));
            //         if (sTitle.IndexOf(DateTime.Today.ToString("dd")) < 0) { continue; }
            //         returnHistory.Add(new ArticleData() {
            //             id = item.Id.ToString(),
            //             title = sTitle,
            //             summary = Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(historySummary)),
            //             image = Joins.Apps.Common.Util.ImagePath(item.Thumbnail),
            //             writer = (item.Writer == null) ? string.Empty : Joins.Apps.Common.Util.DisplayText(item.Writer.Name),
            //             date = Joins.Apps.Common.Util.DateTimeConvert(item.RegistedDateTime.ToString())
            //         });
            //     }
            //     returnHistory = returnHistory.OrderBy(a => Guid.NewGuid()).ToList();
            //     returnValue.History = returnHistory;
            // } catch (ArgumentException) {
            //     returnValue.History = null;
            // }
            returnValue.History = GetHistory();
            //returnValue.History = null;

            //칼럼
            //try {
            //    List<Joins.Apps.Models.News10.ArticleData> columnList = Joins.Apps.Repository.News10.CardRepository.Column();
            //    if (!columnList.Any())
            //        throw new ArgumentException();

            //    var columnRgx = new Regex(@"^\[.*?\]", RegexOptions.IgnoreCase);

            //    var returnColumn = new List<Joins.Apps.Models.News10.ArticleData>();
            //    foreach (Joins.Apps.Models.News10.ArticleData item in columnList) {
            //        string columnTitle = item.title;
            //        if (!string.IsNullOrEmpty(item.id)) {
            //            Joins.Apps.Models.News10.ArticleData columnArticle = Joins.Apps.Repository.News10.ArticleRepository.View(new Joins.Apps.Models.News10.ArticleParameters { totalID = Convert.ToInt32(item.id) });
            //            if (!string.IsNullOrEmpty(columnArticle.title)) {
            //                columnTitle = columnArticle.title;
            //            }
            //        }
            //        if (!string.IsNullOrEmpty(columnTitle)) {
            //            columnTitle = columnRgx.Replace(columnTitle, "").TrimStart();
            //        }
            //        if (!string.IsNullOrEmpty(item.id) && !string.IsNullOrEmpty(columnTitle)) {
            //            returnColumn.Add(new Joins.Apps.Models.News10.ArticleData {
            //                id = item.id,
            //                title = Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(columnTitle)),
            //                summary = Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(item.summary)),
            //                image = item.image,
            //                writer = Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(item.writer)),
            //                date = item.date
            //            });
            //        }
            //    }
            //    returnValue.Column = returnColumn;
            //} catch (ArgumentException) {
            //    returnValue.Column = null;
            //}
            returnValue.Column = GetColumn();
            //속보
            //try {
            //    List<Joins.Apps.Models.News10.ArticleData> returnNewsflash = Joins.Apps.Repository.News10.CardRepository.Newsflash();
            //    if (!returnNewsflash.Any())
            //        throw new ArgumentException();

            //    returnValue.Newsflash = returnNewsflash;
            //} catch (ArgumentException) {
            //    returnValue.Newsflash = null;
            //}

            //이벤트
            try {
                List<Joins.Apps.Models.News10.EventData> returnEvent= Joins.Apps.Repository.News10.EventRepository.GetEvent(1,5,"Y");
             //   if (!returnEvent.Any())
              //      throw new ArgumentException();

                returnValue.Event = returnEvent;
            } catch (ArgumentException) {
                returnValue.Event = null;
            }
            
            return Ok(returnValue);
        }

        #endregion

        #region 운세

        [HttpGet]
        [Route("News10/Card/Fortune")]
        public IHttpActionResult Fortune(string deviceid = "") {
            List<Joins.Apps.Models.News10.FortuneData> returnValue = null;
            try {
                returnValue = Joins.Apps.Repository.News10.CardRepository.Fortune();
                if (returnValue.Count == 0)
                    throw new ArgumentException();
            } catch (ArgumentException) {
                returnValue = null;
            }

            return Ok(new { Fortune = returnValue });
        }
        [HttpGet]
        [Route("news10/Card/Fortune2")]
        public IHttpActionResult Fortune2(string deviceid = "") {
            List<Joins.Apps.Models.News10.FortuneData2> returnValue = null;

            try {
                returnValue = Joins.Apps.Repository.News10.CardRepository.Fortune2();
                if (returnValue.Count == 0)
                    throw new ArgumentException();
            } catch (ArgumentException) {
                returnValue = null;
            }

            return Ok(new { Fortune = returnValue });
        }
        #endregion
        #region [미리보는 오늘]

        [HttpGet]
        [Route("News10/Card/PreviewToday")]
        public IHttpActionResult PreviewToday(string deviceID = "") {
            List<Joins.Apps.Models.News10.Cruz.News10> PreviewData = Joins.Apps.Core.MemCache.GetMemPreviewTodayData();//news10 브리핑
            Joins.Apps.Models.News10.returnData < List<Joins.Apps.Models.News10.Cruz.News10>> r = new returnData<List<Joins.Apps.Models.News10.Cruz.News10>>();
            Joins.Apps.Models.News10.CardInfo cardinfo = Joins.Apps.Core.MemCache.GetMemCardInfo("32");
            if (PreviewData != null && PreviewData.Count>0&& !string.IsNullOrEmpty(PreviewData[0].id)) {
                r.r_code = "1";
                r.r_msg = "성공";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = 1;
                r.cardinfo = new Result_CardInfo {
                    id= cardinfo.CARD_ID,
                    name= cardinfo.CARD_NAME,
                    ename= cardinfo.CARD_ENG_NAME
                };
                r.data = PreviewData;
            }else {
                r.r_code = "0";
                r.r_msg = "실패";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = 0;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
            }
            return Ok( r );
        }

        #endregion

        #region[연재]
        [HttpGet]
        [Route("News10/Card/hotSeries")]
        public IHttpActionResult hotSeries(string deviceID = "") {
            List<Joins.Apps.Models.News10.Cruz3.Article_List> PreviewData = Joins.Apps.Core.MemCache.GetMemHotSeriesData();//news10 브리핑
            Joins.Apps.Models.News10.returnData<List<Joins.Apps.Models.News10.Cruz3.Article_List>> r = new returnData<List<Joins.Apps.Models.News10.Cruz3.Article_List>>();
            Joins.Apps.Models.News10.CardInfo cardinfo = Joins.Apps.Core.MemCache.GetMemCardInfo("34");
            if (PreviewData != null && PreviewData.Count > 0 && !string.IsNullOrEmpty(PreviewData[0].id)) {
                r.r_code = "1";
                r.r_msg = "성공";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = PreviewData.Count;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
                r.data = PreviewData;
            }
            else {
                r.r_code = "0";
                r.r_msg = "실패";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = 0;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
            }
            return Ok(r);
        }
        #endregion
       
        #region[브리핑카드 -관련기사리스트]
        [HttpGet]
        [Route("News10/Card/RelArticle")]
        public IHttpActionResult RelArticle(int tid,string deviceID = "") {
            List<Joins.Apps.Models.News10.Cruz3.Article_List> AData = Joins.Apps.Repository.News10.CardRepository.GetRelativeArticleData(tid, deviceID);//news10 브리핑
            Joins.Apps.Models.News10.returnData<List<Joins.Apps.Models.News10.Cruz3.Article_List>> r = new returnData<List<Joins.Apps.Models.News10.Cruz3.Article_List>>();
            Joins.Apps.Models.News10.CardInfo cardinfo = Joins.Apps.Core.MemCache.GetMemCardInfo("1");
            if (AData != null && AData.Count > 0 && !string.IsNullOrEmpty(AData[0].id)) {
                r.r_code = "1";
                r.r_msg = "성공";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = AData.Count;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
                r.data = AData;
            }
            else {
                r.r_code = "0";
                r.r_msg = "실패";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = 0;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
            }
            return Ok(r);
        }
        #endregion

        #region[주간랭킹 ]
        [HttpGet]
        [Route("News10/Card/Weekly7")]
        public IHttpActionResult Weekly7( string deviceID = "") {
            List<Joins.Apps.Models.News10.Cruz3.Article_List> AData = Joins.Apps.Core.MemCache.GetMemWeekly7Data();//news10 브리핑
            Joins.Apps.Models.News10.returnData<List<Joins.Apps.Models.News10.Cruz3.Article_List>> r = new returnData<List<Joins.Apps.Models.News10.Cruz3.Article_List>>();
            Joins.Apps.Models.News10.CardInfo cardinfo = Joins.Apps.Core.MemCache.GetMemCardInfo("37");
            if (AData != null && AData.Count > 0 && !string.IsNullOrEmpty(AData[0].id)) {
                r.r_code = "1";
                r.r_msg = "성공";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = AData.Count;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
                r.data = AData;
            }
            else {
                r.r_code = "0";
                r.r_msg = "실패";
                r.r_dt = DateTime.Now.ToString();
                r.r_tcnt = 0;
                r.cardinfo = new Result_CardInfo {
                    id = cardinfo.CARD_ID,
                    name = cardinfo.CARD_NAME,
                    ename = cardinfo.CARD_ENG_NAME
                };
            }
            return Ok(r);
        }
        #endregion

        public static List<Joins.Apps.Models.News10.ArticleData> GetHistory() {

            try {

                SearchRepository schRepository = new SearchRepository(new SearchCondition {
                    Page = 1,
                    PageSize = 10,
                    IsNeedTotalCount = false,
                    Keyword = DateTime.Today.ToString("MM월dd일"),
                    SearchCategoryType = SearchCategoryType.IssueNews,
                    SortType = SortType.New,
                    ScopeType = ScopeType.Keyword,
                    SourceCode = "j9"
                });
                DefaultResult searchHistory = schRepository.GetSearchResult();
                //searchHistory
                if (!searchHistory.SearchItems.Any())
                    throw new ArgumentException();

                var historyRgx = new Regex(@"[가-힣]{2}\s*\d{4}.?\d{2}.?\d{2}\s*", RegexOptions.IgnoreCase);

                List<ArticleData> returnHistory = new List<ArticleData>();
                foreach (SearchItem item in searchHistory.SearchItems) {
                    //  if (item.Id == 20904210) { continue; }
                    string historySummary = item.Summary;
                    if (item.Id > 0) {
                        ArticleData historyContent = Joins.Apps.Repository.News10.ArticleRepository.View(new ArticleParameters { totalID = item.Id });
                        if (string.IsNullOrEmpty(historySummary)) { historySummary = historyContent.content; }
                    }
                    if (!string.IsNullOrEmpty(historySummary)) {
                        historySummary = historyRgx.Replace(historySummary, "").TrimStart();
                    }
                    if (string.IsNullOrEmpty(item.Thumbnail)) { continue; }
                    string sTitle = Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(item.MobileTitle));
                    if (sTitle.IndexOf(DateTime.Today.ToString("dd")) < 0) { continue; }
                    returnHistory.Add(new ArticleData() {
                        id = item.Id.ToString(),
                        title = sTitle,
                        summary = Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(historySummary)),
                        image = Joins.Apps.Common.Util.ImagePath(item.Thumbnail),
                        writer = (item.Writer == null) ? string.Empty : Joins.Apps.Common.Util.DisplayText(item.Writer.Name),
                        date = Joins.Apps.Common.Util.DateTimeConvert(item.RegistedDateTime.ToString())
                    });
                }
                returnHistory = returnHistory.OrderBy(a => Guid.NewGuid()).ToList();
                return returnHistory;
            } catch (ArgumentException) {
                return null;
            }
        }

        public static List<Joins.Apps.Models.News10.ArticleData> GetColumn() {
            try {
                List<Joins.Apps.Models.News10.ArticleData> columnList = Joins.Apps.Repository.News10.CardRepository.Column();
                if (!columnList.Any())
                    throw new ArgumentException();

                var columnRgx = new Regex(@"^\[.*?\]", RegexOptions.IgnoreCase);

                var returnColumn = new List<Joins.Apps.Models.News10.ArticleData>();
                foreach (Joins.Apps.Models.News10.ArticleData item in columnList) {
                    string columnTitle = item.title;
                    if (!string.IsNullOrEmpty(item.id)) {
                        Joins.Apps.Models.News10.ArticleData columnArticle = Joins.Apps.Repository.News10.ArticleRepository.View(new Joins.Apps.Models.News10.ArticleParameters { totalID = Convert.ToInt32(item.id) });
                        if (!string.IsNullOrEmpty(columnArticle.title)) {
                            columnTitle = columnArticle.title;
                        }
                    }
                    if (!string.IsNullOrEmpty(columnTitle)) {
                        columnTitle = columnRgx.Replace(columnTitle, "").TrimStart();
                    }
                    if (!string.IsNullOrEmpty(item.id) && !string.IsNullOrEmpty(columnTitle)) {
                        returnColumn.Add(new Joins.Apps.Models.News10.ArticleData {
                            id = item.id,
                            title = Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(columnTitle)),
                            summary = Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(item.summary)),
                            image = item.image,
                            writer = Joins.Apps.Common.Util.DisplayText(Joins.Apps.Common.Util.HTMLRemove(item.writer)),
                            date = item.date
                        });
                    }
                }
                return  returnColumn;
            } catch (ArgumentException) { return null; }
        }
    }
}
