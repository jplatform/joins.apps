﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Joins.Apps.Models.News10;
using System.IO;
using System.Text;
using System.Web;

namespace Joins.Apps.API.Controllers.News10
{
    public class PushController : ApiController
    {
        #region 푸시 전송
        [HttpGet]
        [Route("news10/Push/Send")]
        public IHttpActionResult Send(string pushType = "", string pushMessage = "") {
            var parameters = new PushListParameters();

            //상시
            if (!string.IsNullOrEmpty(pushType) && !string.IsNullOrEmpty(pushMessage)) {
                parameters.pushType = pushType;
                parameters.pushMessage = pushMessage;
            }

            //계획
            else {
                DateTime CurrentDT = DateTime.Now;
                int CurrentHour = Convert.ToInt16(CurrentDT.ToString("HH"));
                int CurrentMin = Convert.ToInt16(CurrentDT.ToString("mm"));
                // string nowHour = "";

                if ((0 <= CurrentMin && CurrentMin < 20) || (30 <= CurrentMin && CurrentMin < 50)) { }
                else { return Ok( new { result = false }); }
                //전송 시간 체크
                // string nowHour = DateTime.Now.ToString("HH");
                //    if (string.IsNullOrEmpty(nowHour))
                //       throw new ArgumentException();

                if (6 <= CurrentHour && CurrentHour < 12) {
                    parameters.pushType = "morning";
                    parameters.pushMessage = "모닝 브리핑이 도착했습니다.";
                }
                else if (12 <= CurrentHour && CurrentHour < 18) {
                    parameters.pushType = "afternoon";
                    parameters.pushMessage = "애프터눈 브리핑이 도착했습니다.";
                }
                else if (18 <= CurrentHour && CurrentHour <= 23) {
                    parameters.pushType = "evening";
                    parameters.pushMessage = "이브닝 브리핑이 도착했습니다.";
                }
                else if (0 <= CurrentHour && CurrentHour < 6) {
                    parameters.pushType = "night";
                    parameters.pushMessage = "나이트 브리핑이 도착했습니다.";
                }

                //로그 체크 - 발송 로그가 있으면 중지
                string nowDate = DateTime.Now.ToString("yyyy-MM-dd");
                List<PushLogListData> log = Joins.Apps.Repository.News10.PushRepository.LogList(new PushLogListParameters() { pageNum = 1, pageSize = 1, pushType = parameters.pushType, startDate = nowDate });
                if (log.Any()) {
                    return Ok(new { result = false });
                    //throw new ArgumentException();
                }
            }

            parameters.pushMessage = HttpUtility.UrlEncode(parameters.pushMessage);

            bool returnValue = true;

            try {
                //Android
                parameters.deviceType = "A";
                parameters.pushCount += List(parameters, Joins.Apps.Repository.News10.PushRepository.List(parameters));

                //IOS
              //  parameters.deviceType = "I";
             //   parameters.pushCount += List(parameters, Joins.Apps.Repository.News10.PushRepository.List(parameters));

                //로그
                Joins.Apps.Repository.News10.PushRepository.LogUpdate(new PushLogUpdateParameters() { pushType = parameters.pushType, pushMessage = HttpUtility.UrlDecode(parameters.pushMessage), pushCount = parameters.pushCount });
            } catch (ArgumentException) {
                returnValue = false;
            }

            return Ok(new { result = returnValue });
        }
        #endregion

        #region [일반알림푸시전송]
        [HttpGet]
        [Route("news10/Push/NSend")]
        public IHttpActionResult NotiSend() {
            var parameters = new PushListParameters2();
            parameters.pushType = "noti";
            //로그 체크 - 발송 로그가 있으면 중지
            string nowDate = DateTime.Now.ToString("yyyy-MM-dd");
            List<PushLogListData> log = Joins.Apps.Repository.News10.PushRepository.NotiLogList(new PushLogListParameters() { pageNum = 1, pageSize = 1, pushType = parameters.pushType, startDate = nowDate });
            if (log.Any())
                throw new ArgumentException();


            //parameters.pushMessage = HttpUtility.UrlEncode(parameters.pushMessage);

            bool returnValue = true;

            try {
                //Android
                parameters.deviceType = "A";
                parameters.pushCount += NotiList(parameters, Joins.Apps.Repository.News10.PushRepository.NotiList(parameters));

                //IOS
                parameters.deviceType = "I";
                parameters.pushCount += NotiList(parameters, Joins.Apps.Repository.News10.PushRepository.NotiList(parameters));
                //로그
                if (parameters.pushCount > 0) {
                    Joins.Apps.Repository.News10.PushRepository.NotiLogUpdate(new PushLogUpdateParameters() { pushType = parameters.pushType, pushMessage = HttpUtility.UrlDecode(parameters.pushMessage), pushCount = parameters.pushCount });
                }
            } catch (ArgumentException) {
                returnValue = false;
            }

            return Ok(new { result = returnValue });
        }
        #endregion

        #region[FCM 푸시 전송]
        [HttpGet]
        [Route("news10/Push/SendFCM")]
        public IHttpActionResult SendFCM(string pushType = "", string pushMessage = "") {
            var parameters = new PushListParameters();

            //상시
            if (!string.IsNullOrEmpty(pushType) && !string.IsNullOrEmpty(pushMessage)) {
                parameters.pushType = pushType;
                parameters.pushMessage = pushMessage;
            }

            //계획
            else {
                DateTime CurrentDT = DateTime.Now;
                int CurrentHour = Convert.ToInt16(CurrentDT.ToString("HH"));
                int CurrentMin = Convert.ToInt16(CurrentDT.ToString("mm"));
                // string nowHour = "";

                if ((0 <= CurrentMin && CurrentMin < 20) || (30 <= CurrentMin && CurrentMin < 50)) { }
                else { return Ok(new { result = false }); }
                //전송 시간 체크
                // string nowHour = DateTime.Now.ToString("HH");
                //    if (string.IsNullOrEmpty(nowHour))
                //       throw new ArgumentException();

                if (6 <= CurrentHour && CurrentHour < 12) {
                    parameters.pushType = "morning";
                    parameters.pushMessage = "모닝 브리핑이 도착했습니다.";
                }
                else if (12 <= CurrentHour && CurrentHour < 18) {
                    parameters.pushType = "afternoon";
                    parameters.pushMessage = "애프터눈 브리핑이 도착했습니다.";
                }
                else if (18 <= CurrentHour && CurrentHour <= 23) {
                    parameters.pushType = "evening";
                    parameters.pushMessage = "이브닝 브리핑이 도착했습니다.";
                }
                else if (0 <= CurrentHour && CurrentHour < 6) {
                    parameters.pushType = "night";
                    parameters.pushMessage = "나이트 브리핑이 도착했습니다.";
                }

                //로그 체크 - 발송 로그가 있으면 중지
                string nowDate = DateTime.Now.ToString("yyyy-MM-dd");
                List<PushLogListData> log = Joins.Apps.Repository.News10.PushRepository.LogList(new PushLogListParameters() { pageNum = 1, pageSize = 1, pushType = parameters.pushType+"_F", startDate = nowDate });
                if (log.Any()) {
                    return Ok(new { result = false });
                }
            }

            parameters.pushMessage = HttpUtility.UrlEncode(parameters.pushMessage);

            bool returnValue = true;

            try {
                //Android
                parameters.deviceType = "A";
                parameters.pushCount += List_FCM(parameters, Joins.Apps.Repository.News10.PushRepository.List_FCM(parameters));

                //IOS
                parameters.deviceType = "I";
                parameters.pushCount += List_FCM(parameters, Joins.Apps.Repository.News10.PushRepository.List_FCM(parameters));

                //로그
                Joins.Apps.Repository.News10.PushRepository.LogUpdate(new PushLogUpdateParameters() { pushType = parameters.pushType+"_F", pushMessage = HttpUtility.UrlDecode(parameters.pushMessage), pushCount = parameters.pushCount });
            } catch (ArgumentException) {
                returnValue = false;
            }

            return Ok(new { result = returnValue });
        }
        #endregion

        #region 푸시 리스트
        public int List_FCM(PushListParameters parameters, List<PushListData> list) {
            string tokenArray = string.Empty;
            int tokenCount = 0;
            int pushCount = 0;
            foreach (PushListData item in list) {
                pushCount++;
                tokenCount++;
                tokenArray += item.token + ",";

                if (parameters.deviceType == "A") {
                    if (tokenCount > 500) {
                        Call_FCM(parameters.deviceType, parameters.pushMessage, tokenArray);
                        tokenArray = string.Empty;
                        tokenCount = 0;
                    }
                }else if (parameters.deviceType == "I") {
                    Call_FCM(parameters.deviceType, parameters.pushMessage, tokenArray);
                    tokenArray = string.Empty;
                }
            }

            if (!string.IsNullOrEmpty(tokenArray)) {
                Call_FCM(parameters.deviceType, parameters.pushMessage, tokenArray);
            }
            return pushCount;
        }

        public int List(PushListParameters parameters, List<PushListData> list) {
            string tokenArray = string.Empty;
            int tokenCount = 0;
            int pushCount = 0;
            foreach (PushListData item in list) {
                pushCount++;
                tokenCount++;
                tokenArray += item.token + ",";

                if (parameters.deviceType == "A") {
                    if (tokenCount > 500) {
                        Call(parameters.deviceType, parameters.pushMessage, tokenArray);
                        tokenArray = string.Empty;
                        tokenCount = 0;
                    }
                }
                else if (parameters.deviceType == "I") {
                    Call(parameters.deviceType, parameters.pushMessage, tokenArray);
                    tokenArray = string.Empty;
                }
            }

            if (!string.IsNullOrEmpty(tokenArray)) {
                Call(parameters.deviceType, parameters.pushMessage, tokenArray);
            }
            return pushCount;
        }

        public int NotiList(PushListParameters2 parameters, List<NotiPushListData> list) {
            string tokenArray = string.Empty;
            int tokenCount = 0;
            int pushCount = 0;
            string sMsg = "";
            foreach (NotiPushListData item in list) {
                pushCount++;
                tokenCount++;
                tokenArray += item.token + ",";
                parameters.pushMessage = HttpUtility.UrlEncode(item.message);
                if (parameters.deviceType == "A") {
                    if (!string.IsNullOrEmpty(sMsg) && sMsg != item.message) {
                        Call_FCM(parameters.deviceType, parameters.pushMessage, tokenArray);
                        tokenArray = string.Empty;
                        tokenCount = 0;
                    }
                    if (tokenCount > 500) {
                        Call_FCM(parameters.deviceType, parameters.pushMessage, tokenArray);
                        tokenArray = string.Empty;
                        tokenCount = 0;
                    }
                }
                else if (parameters.deviceType == "I") {
                    Call_FCM(parameters.deviceType, parameters.pushMessage, tokenArray);
                    tokenArray = string.Empty;
                }
                sMsg = item.message;
            }
            if (!string.IsNullOrEmpty(tokenArray)) { Call(parameters.deviceType, parameters.pushMessage, tokenArray); }

            return pushCount;
        }
        #endregion

        #region 푸시 호출
        public void Call_FCM(string dtype, string msg, string tokenArray) {
            string pushPath = "http://sender.push.labs.joins.com/_apns/mynews/push_fcm.php";
            string pushData = string.Format("os={0}&token={1}&m={2}", dtype, tokenArray, msg);
            byte[] byteArray = Encoding.UTF8.GetBytes(pushData);

            WebRequest request = WebRequest.Create(pushPath);
            request.Method = "POST";
            request.ContentLength = byteArray.Length;
            request.ContentType = "application/x-www-form-urlencoded";
            request.Timeout = 180000;

            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
        } 
        public void Call(string dtype,string msg, string tokenArray) {
            string pushPath = "http://sender.push.labs.joins.com/_apns/mynews/push.php";
            string pushData = string.Format("os={0}&token={1}&m={2}", dtype, tokenArray, msg);
            byte[] byteArray = Encoding.UTF8.GetBytes(pushData);

            WebRequest request = WebRequest.Create(pushPath);
            request.Method = "POST";
            request.ContentLength = byteArray.Length;
            request.ContentType = "application/x-www-form-urlencoded";
            request.Timeout = 180000;

            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
        }
        #endregion
    }
}
