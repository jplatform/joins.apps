﻿using Joins.Apps.Models.News10;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Joins.Apps.API.Controllers.News10
{
    public class ContentsController : ApiController {
        [HttpGet]
        [Route("News10/Contents")]
        public IHttpActionResult Contents(int gen = 1, int age = 3, string deviceID = "", int cardVersion = 12) {
            Joins.Apps.Models.News10.returnALLData returnAll = new Joins.Apps.Models.News10.returnALLData();
            return Ok(returnAll);
            returnAll.contents = new List<Contents>();

            #region [카드 정보]
            List<Joins.Apps.Models.News10.CardList> CardInfoList=Joins.Apps.Repository.News10.CardRepository.MyCardList(cardVersion, deviceID);
            Hashtable hCardInfoList = new Hashtable();
            if (CardInfoList.Count > 0) {
                int iCardorder = 1;
                foreach (Joins.Apps.Models.News10.CardList cardinfo in CardInfoList) {
                    if (!cardinfo.id.Equals("8") && !cardinfo.id.Equals("9") && !cardinfo.id.Equals("10") && !cardinfo.id.Equals("38")) {
                        if (!hCardInfoList.ContainsKey(cardinfo.id)) { hCardInfoList.Add(cardinfo.id, new Result_CardInfo() { order = iCardorder, id = cardinfo.id, name = cardinfo.name, ename = cardinfo.ename }); iCardorder++; }
                    }
                }
            }
            #endregion

            #region [브리핑]
            Contents ContentsBrief = new Contents();
            if (hCardInfoList.ContainsKey("1")) {
                List<Joins.Apps.Models.News10.Cruz.News10> CruzNew10DataList = Joins.Apps.Core.MemCache.GetMemDBCruzNews10Data(age, gen);//news10 브리핑
                if (CruzNew10DataList != null && (CruzNew10DataList).Count > 5 && !string.IsNullOrEmpty((CruzNew10DataList)[0].id)) {
                    ContentsBrief.r_code = "1";
                    ContentsBrief.r_msg = "성공";
                    ContentsBrief.r_tcnt = CruzNew10DataList.Count;
                    ContentsBrief.cardinfo = (Result_CardInfo)hCardInfoList["1"];
                    ContentsBrief.data = CruzNew10DataList;
                }else {
                    ContentsBrief.r_code = "0";
                    ContentsBrief.r_msg = "실패";
                    ContentsBrief.r_tcnt = 0;
                    ContentsBrief.cardinfo = (Result_CardInfo)hCardInfoList["1"];
                }
                returnAll.contents.Add(ContentsBrief);
            }
            #endregion

            #region [미리보는 오늘]
            if (hCardInfoList.ContainsKey("32")) {
                Contents ContentData = new Contents();
                List<Joins.Apps.Models.News10.Cruz.News10> PreviewData = Joins.Apps.Core.MemCache.GetMemPreviewTodayData();//news10 브리핑
                if (PreviewData != null && !string.IsNullOrEmpty(PreviewData[0].id)) {
                    ContentData.r_code = "1";
                    ContentData.r_msg = "성공";
                    ContentData.r_tcnt = PreviewData.Count;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["32"];
                    ContentData.data = PreviewData;
                }
                else {
                    ContentData.r_code = "0";
                    ContentData.r_msg = "실패";
                    ContentData.r_tcnt = 0;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["32"];

                }
                returnAll.contents.Add(ContentData);
            }
            #endregion

            #region [실시간 키워드 이슈]
            if (hCardInfoList.ContainsKey("22")) {
                Contents ContentData = new Contents();
                List<Joins.Apps.Models.News10.Cruz.HotKwd> CruzkeywodaList = Joins.Apps.Core.MemCache.GetMemDBCruzkeywordData();//키워드 이슈
                if (CruzkeywodaList != null && !string.IsNullOrEmpty(CruzkeywodaList[0].id)) {
                    ContentData.r_code = "1";
                    ContentData.r_msg = "성공";
                    ContentData.r_tcnt = CruzkeywodaList.Count;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["22"];
                    ContentData.data = CruzkeywodaList;
                }else {
                    ContentData.r_code = "0";
                    ContentData.r_msg = "실패";
                    ContentData.r_tcnt = 0;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["22"];
                  
                }
                returnAll.contents.Add(ContentData);
            }
            #endregion

            #region [섹션모음]
            if (hCardInfoList.ContainsKey("35")) {
                Contents ContentData = new Contents();
                List<Joins.Apps.Models.News10.Cruz.Special> CruzSpecialDataList = Joins.Apps.Core.MemCache.GetMemDBCruzSpecialData(age, gen, cardVersion);//스페셜
                if (cardVersion >= 10 && CruzSpecialDataList != null) { CruzSpecialDataList = CruzSpecialDataList.OrderBy(a => Guid.NewGuid()).ToList(); }
                if (CruzSpecialDataList != null && !string.IsNullOrEmpty(CruzSpecialDataList[0].id)) {
                    ContentData.r_code = "1";
                    ContentData.r_msg = "성공";
                    ContentData.r_tcnt = CruzSpecialDataList.Count;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["35"];
                    ContentData.data = CruzSpecialDataList;
                }else {
                    ContentData.r_code = "0";
                    ContentData.r_msg = "실패";
                    ContentData.r_tcnt = 0;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["35"];
                }
                returnAll.contents.Add(ContentData);
            }
            #endregion

            #region [추천 연재]
            if (hCardInfoList.ContainsKey("34")) {
                Contents ContentData = new Contents();
                List<Joins.Apps.Models.News10.Cruz3.Article_List> HotSeriesData = Joins.Apps.Core.MemCache.GetMemHotSeriesData();
                if (HotSeriesData != null && !string.IsNullOrEmpty(HotSeriesData[0].id)) {
                    ContentData.r_code = "1";
                    ContentData.r_msg = "성공";
                    ContentData.r_tcnt = HotSeriesData.Count;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["34"];
                    ContentData.data = HotSeriesData;
                }else {
                    ContentData.r_code = "0";
                    ContentData.r_msg = "실패";
                    ContentData.r_tcnt = 0;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["34"];
                }
                returnAll.contents.Add(ContentData);
            }
            #endregion

            #region [운세]
            if (hCardInfoList.ContainsKey("4")) {
                Contents ContentData = new Contents();
                List<Joins.Apps.Models.News10.FortuneData2> FortuneData = Joins.Apps.Repository.News10.CardRepository.Fortune2();
                    if (FortuneData != null && !string.IsNullOrEmpty(FortuneData[0].birthyear)) {
                    ContentData.r_code = "1";
                    ContentData.r_msg = "성공";
                    ContentData.r_tcnt = FortuneData.Count;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["4"];
                    ContentData.data = FortuneData;
                }
                else {
                    ContentData.r_code = "0";
                    ContentData.r_msg = "실패";
                    ContentData.r_tcnt = 0;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["4"];
                }
                returnAll.contents.Add(ContentData);
            }
            #endregion

            #region [이슈패키지]
            if (hCardInfoList.ContainsKey("36")) {
                Contents ContentData = new Contents();
                List<Joins.Apps.Models.News10.Cruz3.issue> issData = Joins.Apps.Core.MemCache.GetMemIssuePackData();
                if (issData != null && issData[0].iss_seq > 0) {
                    ContentData.r_code = "1";
                    ContentData.r_msg = "성공";
                    ContentData.r_tcnt = issData.Count;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["36"];
                    ContentData.data = issData;
                }else {
                    ContentData.r_code = "0";
                    ContentData.r_msg = "실패";
                    ContentData.r_tcnt = 0;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["36"];
                }
                returnAll.contents.Add(ContentData);
            }
            #endregion


            #region [역사속 오늘]
            if (hCardInfoList.ContainsKey("25")) {
                Contents ContentData = new Contents();
                List<Joins.Apps.Models.News10.ArticleData> HistoryData = Joins.Apps.API.Controllers.News10.CardController.GetHistory();
              
                if (HistoryData != null && !string.IsNullOrEmpty(HistoryData[0].id)) {
                    ContentData.r_code = "1";
                    ContentData.r_msg = "성공";
                    ContentData.r_tcnt = HistoryData.Count;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["25"];
                    ContentData.data = HistoryData;
                }
                else {
                    ContentData.r_code = "0";
                    ContentData.r_msg = "실패";
                    ContentData.r_tcnt = 0;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["25"];
                }
                returnAll.contents.Add(ContentData);
            }
            #endregion

            #region [오피니언]
            if (hCardInfoList.ContainsKey("23")) {
                Contents ContentData = new Contents();
                List<Joins.Apps.Models.News10.ArticleData> ColumnData = Joins.Apps.API.Controllers.News10.CardController.GetColumn();
                if (ColumnData != null && !string.IsNullOrEmpty(ColumnData[0].id)) {
                    ContentData.r_code = "1";
                    ContentData.r_msg = "성공";
                    ContentData.r_tcnt = ColumnData.Count;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["23"];
                    ContentData.data = ColumnData;
                }
                else {
                    ContentData.r_code = "0";
                    ContentData.r_msg = "실패";
                    ContentData.r_tcnt = 0;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["23"];
                }
                returnAll.contents.Add(ContentData);
            }
            #endregion

            #region [주간 공감]
            if (hCardInfoList.ContainsKey("37")) {
                Contents ContentData = new Contents();
                List<Joins.Apps.Models.News10.Cruz3.Article_List> Weekly7Data = Joins.Apps.Core.MemCache.GetMemWeekly7Data();
                if (Weekly7Data != null &&!string.IsNullOrEmpty(Weekly7Data[0].id)) {
                    ContentData.r_code = "1";
                    ContentData.r_msg = "성공";
                    ContentData.r_tcnt = Weekly7Data.Count;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["37"];
                    ContentData.data = Weekly7Data;
                }else {
                    ContentData.r_code = "0";
                    ContentData.r_msg = "실패";
                    ContentData.r_tcnt = 0;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["37"];
                }
                returnAll.contents.Add(ContentData);
            }
            #endregion

            #region [MnB Special]
            if (hCardInfoList.ContainsKey("39")) {
                Contents ContentData = new Contents();
                List<Joins.Apps.Models.News10.Cruz3.MnBSpecial> MnbSpecialData = Joins.Apps.Core.MemCache.GetMemMnBSpecialData();
                if (MnbSpecialData != null && !string.IsNullOrEmpty(MnbSpecialData[0].title)) {
                    ContentData.r_code = "1";
                    ContentData.r_msg = "성공";
                    ContentData.r_tcnt = MnbSpecialData.Count;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["39"];
                    ContentData.data = MnbSpecialData;
                }else {
                    ContentData.r_code = "0";
                    ContentData.r_msg = "실패";
                    ContentData.r_tcnt = 0;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["39"];
                }
                returnAll.contents.Add(ContentData);
            }
            #endregion

            #region [박스 오피스]
            if (hCardInfoList.ContainsKey("28")) {
                Contents ContentData = new Contents();
                Joins.Apps.Models.News10.BoxOffice.P351Response BoxOfficeData = Joins.Apps.Core.MemCache.GetMemCruzBoxOffice();
                
                if (BoxOfficeData != null && BoxOfficeData.partnership != null && BoxOfficeData.partnership.header != null && BoxOfficeData.partnership.body != null && BoxOfficeData.partnership.header.ResultCode != null && BoxOfficeData.partnership.header.ResultCode.Equals("0000")) {
                    ContentData.r_code = "1";
                    ContentData.r_msg = "성공";
                    ContentData.r_tcnt = BoxOfficeData.partnership.body.p351Entity.Count;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["28"];
                    ContentData.data = BoxOfficeData.partnership.body.p351Entity;
                }else {
                    ContentData.r_code = "0";
                    ContentData.r_msg = "실패";
                    ContentData.r_tcnt = 0;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["28"];
                }
                returnAll.contents.Add(ContentData);
            }
            #endregion

            #region [JTBC 영상]
            if (hCardInfoList.ContainsKey("31")) {
                Contents ContentData = new Contents();
                List<Joins.Apps.Models.News10.Cruz.HotVod> jtbcvodData = Joins.Apps.Core.MemCache.GetMemJtbcVod();

                Joins.Apps.Models.News10.Cruz.HotVod jtbc1min = Joins.Apps.Core.MemCache.GetMemJTBC1MinData();
                jtbcvodData = jtbcvodData.OrderBy(a => Guid.NewGuid()).ToList();
                //  jtbc1min = null;
                if (jtbc1min != null && !string.IsNullOrEmpty(jtbc1min.title)) { jtbcvodData.Insert(0, jtbc1min); }

                if (jtbcvodData != null && jtbcvodData.Count>0 && !string.IsNullOrEmpty(jtbcvodData[0].title)) {
                    ContentData.r_code = "1";
                    ContentData.r_msg = "성공";
                    ContentData.r_tcnt = jtbcvodData.Count;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["31"];
                    ContentData.data = jtbcvodData;
                }else {
                    ContentData.r_code = "0";
                    ContentData.r_msg = "실패";
                    ContentData.r_tcnt = 0;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["31"];
                }
                returnAll.contents.Add(ContentData);
            }
            #endregion

            #region [썰리]
            if (hCardInfoList.ContainsKey("29")) {
                Contents ContentData = new Contents();
                List<Joins.Apps.Models.Sully.APIProduct> SsullyData = Joins.Apps.Core.MemCache.GetMemSSullyData();

                if (SsullyData != null && SsullyData.Count > 0 && !string.IsNullOrEmpty(SsullyData[0].ptitle)) {
                    ContentData.r_code = "1";
                    ContentData.r_msg = "성공";
                    ContentData.r_tcnt = SsullyData.Count;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["29"];
                    ContentData.data = SsullyData;
                }else {
                    ContentData.r_code = "0";
                    ContentData.r_msg = "실패";
                    ContentData.r_tcnt = 0;
                    ContentData.cardinfo = (Result_CardInfo)hCardInfoList["29"];
                }
                returnAll.contents.Add(ContentData);
            }
            #endregion




            if (ContentsBrief != null && ContentsBrief.r_code !=null && ContentsBrief.r_code.Equals("1")) {
                returnAll.r_code = "1";
                returnAll.r_msg = "성공";
                returnAll.r_dt = DateTime.Now.ToString();
            }else {
                returnAll.r_code = "0";
                returnAll.r_msg = "실패";
                returnAll.r_dt = DateTime.Now.ToString();
                returnAll.contents = null;
            }

            return Ok(returnAll);
        }
    }
}
