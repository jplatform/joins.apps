﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Joins.Apps.API {
    public class WebApiApplication : System.Web.HttpApplication {
        protected void Application_Start() {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
        //    FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        //    BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        public static string LERM = "";
        protected virtual void Application_Error(object sender, EventArgs e) {
            Exception em = Server.GetLastError().GetBaseException();
            string tpg = "", rpg = "";
            try {
                tpg = System.Web.HttpContext.Current.Request.Url.ToString();
                rpg = System.Web.HttpContext.Current.Request.UrlReferrer.ToString();
            } catch { }

            try {
                string msg = "";
                if (LERM == (tpg + em.Message)) { msg = ("# [" + tpg + "] # " + em.Message); }
                else { msg = ("# [" + rpg + " → " + tpg + "] # " + em.Message + "\r\n" + em.StackTrace); }
                (new JCube.AF.Module.LogWriter(Joins.Apps.GLOBAL.LogPath + "System", "", JCube.AF.Module.LogWriter.LogUnit.Day)).WriteLog(msg);
                LERM = (tpg + em.Message);
            } catch { }
        }
    }
}
