﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.API.Models.Sully.QuestionController
{
    public class Insert
    {
        /// <summary>
        /// 작성 메세지
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 페이지당 리스트 수
        /// </summary>
        public int Page_Size { get; set; }
    }
}