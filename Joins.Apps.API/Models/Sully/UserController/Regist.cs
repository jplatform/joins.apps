﻿namespace Joins.Apps.API.Models.Sully.UserController
{
    public class Regist
    {
        /// <summary>
        /// 이메일주소
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// SNS 이름
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// SNS 프로필 이미지 URL
        /// </summary>
        public string Profile_Url { get; set; }
        /// <summary>
        /// 디바이스 푸시 키
        /// </summary>
        public string Device_Push_Key { get; set; }
        /// <summary>
        /// 파이어베이스 푸시 토큰
        /// </summary>
        public string FCM_Token { get; set; }
    }
}