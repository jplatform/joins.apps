﻿namespace Joins.Apps.API.Models.Sully.UserController
{
    public class Walkthrough
    {
        /// <summary>
        /// 닉네임
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// 성별(F / M)
        /// </summary>
        public string Gender { get; set; }
        /// <summary>
        /// 연령대
        /// </summary>
        public int? Age { get; set; }
        /// <summary>
        /// 푸시수신여부(Y/N)
        /// </summary>
        public string PushYN { get; set; }
        /// <summary>
        /// 관심카테고리
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// 추천코드 (임시용)
        /// </summary>
        public string Code { get; set; }
    }
}