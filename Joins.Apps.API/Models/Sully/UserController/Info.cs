﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.API.Models.Sully.UserController
{
    public class Info
    {
        /// <summary>
        /// 별명
        /// </summary>
        public string Nick_name { get; set; }
        /// <summary>
        /// 성별
        /// </summary>
        public string Gender { get; set; }
        /// <summary>
        /// 연령대
        /// </summary>
        public int? Age { get; set; }
        /// <summary>
        /// 카테고리
        /// </summary>
        public string Category { get; set; }
    }
}