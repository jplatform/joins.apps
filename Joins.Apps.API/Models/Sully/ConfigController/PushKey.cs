﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.API.Models.Sully.ConfigController
{
    public class PushKey
    {
        /// <summary>
        /// 디바이스 푸시 키
        /// </summary>
        public string Device_push_key { get; set; }
        /// <summary>
        /// 파이어베이스 푸시 토큰
        /// </summary>
        public string FCM_Token { get; set; }
    }
}