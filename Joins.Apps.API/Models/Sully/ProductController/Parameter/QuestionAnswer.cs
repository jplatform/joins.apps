﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.API.Models.Sully.ProductController.Parameter
{
    public class QuestionAnswer
    {
        public int seq { get; set; }
        public string question_seq { get; set; }
        public string item_seq { get; set; }
    }
}