﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.API.Models.Sully.ProductController.Parameter
{
    public class Search
    {
        /// <summary>
        /// 페이지 번호
        /// </summary>
        public int Page { get; set; } = 1;
        /// <summary>
        /// 페이지 당 리스트 수
        /// </summary>
        public int Page_Size { get; set; } = 10;
        /// <summary>
        /// 정렬방식 (latest : 최신순 / popularity : 인기순)
        /// </summary>
        public string Order { get; set; } = "latest";
        /// <summary>
        /// 카테고리 코드
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// 검색어 (공백으로 구분)
        /// </summary>
        public string Search_text { get; set; }
    }
}