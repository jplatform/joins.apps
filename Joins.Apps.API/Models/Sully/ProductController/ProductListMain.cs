﻿using Joins.Apps.Models.Sully.API;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Joins.Apps.API.Models.Sully.ProductController
{
    public class ProductListMain
    {
        /// <summary>
        /// 리스트
        /// </summary>
        public IEnumerable<MainItem> List { get; set; }
        /// <summary>
        /// 기사 총 갯수
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// 총 페이지 수
        /// </summary>
        public int TotalPage { get; set; }
        /// <summary>
        /// 현재 페이지
        /// </summary>
        public int Page { get; set; }
        /// <summary>
        /// 페이지당 리스트 수
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// 쇼케이스
        /// </summary>
        public IEnumerable<object> Showcase { get; set; }

        public class MainItem
        {
            /// <summary>
            /// 아이템 형식
            /// </summary>
            [JsonProperty("item_type")]
            public string ItemType { get; set; }
            /// <summary>
            /// 프로덕트 데이터
            /// </summary>
            [JsonProperty("product_data")]
            public IEnumerable<ProductMain> ProductData { get; set; }
            /// <summary>
            /// 여기가봤썰 데이터
            /// </summary>
            [JsonProperty("product_tour_data")]
            public ProductMainTour ProductTourData { get; set; }
            /// <summary>
            /// 위클리 랭킹 데이터
            /// </summary>
            [JsonProperty("weekly_rank")]
            public IEnumerable WeeklyRank { get; set; }
            /// <summary>
            /// 배너
            /// </summary>
            [JsonProperty("banner")]
            public Banner Banner { get; set; }
        }

        public class ProductMainTour : ProductMain
        {
            /// <summary>
            /// 여기가봤썰 이미지
            /// </summary>
            [JsonProperty("product_tour_images")]
            public IEnumerable<string> ProductTourImages { get; set; }

            public static ProductMainTour CopyTo(ProductMain data)
            {
                var sourceType = data.GetType();
                var target = new ProductMainTour();

                foreach (var sourceProperty in sourceType.GetProperties())
                {
                    var destinationProperty = target.GetType().GetProperty(sourceProperty.Name);

                    if (destinationProperty != null)
                        destinationProperty.SetValue(target, sourceProperty.GetValue(data));
                }

                return target;
            }
        }

        public class Banner
        {
            /// <summary>
            /// 이미지 경로
            /// </summary>
            [JsonProperty("image_url")]
            public string ImageUrl { get; set; }
            /// <summary>
            /// 링크 종류
            /// </summary>
            [JsonProperty("link_type")]
            public string LinkType { get; set; }
            /// <summary>
            /// 링크 고유번호
            /// </summary>
            [JsonProperty("link_seq")]
            public int LinkSeq { get; set; }
            /// <summary>
            /// 프로덕트 코너타입
            /// </summary>
            [JsonProperty("corner_type")]
            public int? CornerType { get; set; }
            /// <summary>
            /// 배경색
            /// </summary>
            [JsonProperty("color")]
            public string Color { get; set; }
            /// <summary>
            /// 광고 컨텐트 여부
            /// </summary>
            [JsonProperty("is_ad_product")]
            public string IsADProduct { get; set; }
        }
    }
}