﻿var Pager = new function () {
    this.PagingHtml = function (pageNum, tot,itemcnt, funcName) {
        var displayItemCount = itemcnt;
        var displayPageCount = 5;
        var lastPage = parseInt(Math.ceil(tot / parseFloat(displayItemCount)));
        var currentPage = Math.max(1, Math.min(lastPage, pageNum));
        var visibleFirstPage = parseInt(Math.floor((currentPage - 1) / parseFloat(displayPageCount))) * displayPageCount + 1;
        var visibleLastPage = visibleFirstPage + displayPageCount - 1;
        visibleLastPage = Math.max(Math.min(visibleLastPage, lastPage), 1);
        //    console.log("pageNum", pageNum, "tot", tot, "lastPage", lastPage, "currentPage", currentPage, "visibleFirstPage", visibleFirstPage, "visibleLastPage", visibleLastPage);

        var html ="";
        html = '<ul class="pagination  pagination-sm">';
        if (currentPage <= displayPageCount){
            html+='    <li class="paginate_button disabled"><a href="javascript:void(0)">처음</a></li>';
            html+='    <li class="paginate_button disabled"><a href="javascript:void(0)">이전</a></li>';
        }else{
            html += '    <li class="paginate_button"><a onclick="' + funcName + '(\'' + 1 + '\')' + '">처음</a></li>';
            html += '    <li class="paginate_button"><a onclick="' + funcName + '(\'' + (visibleFirstPage - 1) + '\')' + '">이전</a></li>';
        }
        for (var i = visibleFirstPage; i <= visibleLastPage; i++) {
            html += '    <li class="paginate_button' + ((currentPage == i) ? " active" : "") + '"><a onclick="' + funcName + '(\'' + i + '\')' + '">' + i + '</a></li>';
        }
        if (visibleLastPage >= lastPage) {
            html += '    <li class="paginate_button disabled"><a href="javascript:void(0)">다음</a></li>';
            html += '    <li class="paginate_button disabled"><a href="javascript:void(0)">끝</a></li>';
        }else {
            html += '    <li class="paginate_button"><a onclick="' + funcName + '(\'' + (visibleLastPage + 1) + '\')' + '">다음</a></li>';
            html += '    <li class="paginate_button"><a onclick="' + funcName + '(\'' + (lastPage + 1) + '\')' + '">끝</a></li>';
        }
        html += '</ul>';
        return html;
    };
};
function IsStringEmpty(str) { if (str == undefined) { return false; } return (str.replace(/\s/g, "") == ""); }
var JCookie = new function () {
    this.SetCookie = function (key, val, expdays, dom) {
        var TODAY;
        if (typeof expdays == 'number' || typeof expdays == 'string') {
            TODAY = new Date();
            TODAY.setDate(TODAY.getDate() + expdays);
        } else {
            TODAY = expdays;
        }

        document.cookie = (key + '=' + escape(val) + ';path=/;expires=' + TODAY.toGMTString() + ';' + ((dom != undefined && dom.length > 0) ? ('domain=' + dom + ';') : ''));
    }

    this.GetCookie = function (key) {
        if (IsStringEmpty(key)) { return ''; }
        var sidx = document.cookie.indexOf(key + '=');
        if (sidx >= 0) {
            sidx += (key.length + 1);
            var eidx = document.cookie.indexOf(";", sidx);
            if (eidx < 0) { eidx = document.cookie.length; }
            return unescape(document.cookie.substring(sidx, eidx));
        }
        else { return ''; }
    };
    this.GetCookie2 = function (pkey, key) {	/* 쿠키열 중에 subkey의 값을 구한다 */
        if (IsStringEmpty(key)) { return ''; }

        var pval = this.GetCookie(pkey);

        if (IsStringEmpty(pval)) { return ''; }
        var sidx = pval.indexOf(key + '=');

        if (sidx >= 0) {
            sidx += (key.length + 1);
            var eidx = pval.indexOf("&", sidx);

            if (eidx < 0) { eidx = pval.length; }
            return unescape(pval.substring(sidx, eidx));
        }
        else { return ''; }
    };
    this.GetCookieArray = function (key1, key2) {
        if (IsStringEmpty(key1) || IsStringEmpty(key2)) { return ''; }
        var key = ('[' + key1 + '][' + key2 + ']');
        return this.GetCookie(key);
    };
}
