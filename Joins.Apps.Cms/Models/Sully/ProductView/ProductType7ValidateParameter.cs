﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Joins.Apps.Cms.Models.Sully.ProductView
{
    [XmlType(TypeName = "QUESTION")]
    public class ProductType7ValidateParameter
    {
        /// <summary>
        /// 고유번호
        /// </summary>
        [XmlAttribute(AttributeName = "SEQ")]
        public int Seq { get; set; }

        /// <summary>
        /// 질문 문항
        /// </summary>
        [XmlIgnore]
        public string Title { get; set; }
        [XmlElement(ElementName = "TITLE")]
        public XmlNode[] TitleCDATA
        {
            get
            {
                return new XmlNode[] { new XmlDocument().CreateCDataSection(Title) };
            }
            set
            {
                if (value == null)
                    return;

                if (value.Length != 1)
                    throw new InvalidOperationException(String.Format("Invalid array length {0}", value.Length));

                Title = value[0].Value;
            }
        }

        /// <summary>
        /// 정답 해설
        /// </summary>
        [XmlIgnore]
        public string Description { get; set; }
        [XmlElement(ElementName = "DESCRIPTION")]
        public XmlNode[] DescriptionCDATA
        {
            get
            {
                return new XmlNode[] { new XmlDocument().CreateCDataSection(Description) };
            }
            set
            {
                if (value == null)
                    return;

                if (value.Length != 1)
                    throw new InvalidOperationException(String.Format("Invalid array length {0}", value.Length));

                Description = value[0].Value;
            }
        }

        /// <summary>
        /// 순서
        /// </summary>
        [XmlAttribute(AttributeName = "ORDER")]
        public int Order { get; set; }

        /// <summary>
        /// 보기항목
        /// </summary>
        [XmlArray(ElementName = "ITEMS")]
        [XmlArrayItem(ElementName = "ITEM")]
        public Item[] Items { get; set; }

        public class Item
        {
            /// <summary>
            /// 고유번호
            /// </summary>
            [XmlAttribute(AttributeName = "SEQ")]
            public int Seq { get; set; }

            /// <summary>
            /// 보기 제목
            /// </summary>
            [XmlIgnore]
            public string Title { get; set; }
            [XmlElement(ElementName = "TITLE")]
            public XmlNode[] TitleCDATA
            {
                get
                {
                    return new XmlNode[] { new XmlDocument().CreateCDataSection(Title) };
                }
                set
                {
                    if (value == null)
                        return;

                    if (value.Length != 1)
                        throw new InvalidOperationException(String.Format("Invalid array length {0}", value.Length));

                    Title = value[0].Value;
                }
            }

            /// <summary>
            /// 정답여부
            /// </summary>
            [XmlAttribute(AttributeName = "CORRECT")]
            public string Correct { get; set; }

            /// <summary>
            /// 순서
            /// </summary>
            [XmlAttribute(AttributeName = "ORDER")]
            public int Order { get; set; }
        }

        public string ToXmlString()
        {
            var x = new XmlSerializer(typeof(ProductType7ValidateParameter));
            var xml = string.Empty;
            using (var sww = new StringWriter())
            {
                var ns = new XmlSerializerNamespaces();
                ns.Add("", "");

                using (var writer = XmlWriter.Create(sww, new XmlWriterSettings
                {
                    OmitXmlDeclaration = true
                }))
                {
                    x.Serialize(writer, this, ns);
                    xml = sww.ToString(); // Your XML
                }
            }

            return xml;
        }
    }
}