﻿using Joins.Apps.Models.Sully;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.Cms.Models.Sully.ProductView
{
    public class PreviewModel
    {
        /// <summary>
        /// 기사정보
        /// </summary>
        public Product Product { get; set; }
        /// <summary>
        /// 기사 내용
        /// </summary>
        public string ContentHtml { get; set; }
        /// <summary>
        /// 추천 기사
        /// </summary>
        public IEnumerable<Product> RecommendProduct { get; set; }
        /// <summary>
        /// 공유용 URL 접근여부
        /// </summary>
        public bool IsShareLink { get; set; }
    }
}