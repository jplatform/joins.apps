﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.Cms.Models.Sully.ProductView
{
    public class SSullyProductContentEdit
    {
        public Apps.Models.Sully.Product Product { get; set; }
        public Apps.Models.Sully.ProductContent Content { get; set; }
    }
}