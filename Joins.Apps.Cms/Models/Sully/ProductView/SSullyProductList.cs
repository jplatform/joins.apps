﻿using Joins.Apps.Models.Sully;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.Cms.Models.Sully.ProductView
{
    public class SSullyProductList
    {
        public Joins.Apps.Models.Page<Joins.Apps.Models.Sully.Product> Entity { get; set; }
        public IEnumerable<CommonCode> Category { get; set; }
        public IEnumerable<CommonCode> Corner { get; set; }
    }
}