﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.Cms.Models.Sully
{
    public class ProductEdit
    {
        public Joins.Apps.Models.Sully.Product Product { get; set; }
        public Joins.Apps.Models.Sully.ProductPushInfo PushInfo { get; set; }
        public Joins.Apps.Cms.Models.Auth LoginInfo { get; set; }
        public IEnumerable<Apps.Models.Sully.CommonCode> Categories { get; set; }
        public IEnumerable<Apps.Models.Sully.CommonCode> Corners { get; set; }
    }
}