﻿using Joins.Apps.Models;
using Joins.Apps.Models.Sully;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.Cms.Models.Sully.Question
{
    public class Detail
    {
        public Page<QuestionDetailItem> Pages { get; set; }
        public QuestionDetailMember Member { get; set; }
    }
}