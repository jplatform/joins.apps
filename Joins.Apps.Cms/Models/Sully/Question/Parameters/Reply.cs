﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Joins.Apps.Cms.Models.Sully.Question.Parameters
{
    public class Reply
    {
        /// <summary>
        /// 답글 번호
        /// </summary>
        public int QuestionSeq { get; set; }
        /// <summary>
        /// 답변 종류 (1 : 텍스트 / 2 : 외부링크 / 3 : 내부링크)
        /// </summary>
        public int ReplyType { get; set; }

        /// <summary>
        /// 답변
        /// </summary>
        public string ReplyContent { get; set; }
        /// <summary>
        /// 내부링크 프로덕트 고유번호
        /// </summary>
        public int? LinkInternalSeq { get; set; }

        /// <summary>
        /// 외부링크 주소
        /// </summary>
        public string LinkExternalUrl { get; set; }
        /// <summary>
        /// 외부링크 도메인
        /// </summary>
        public string LinkExternalDomain { get; set; }
        /// <summary>
        /// 외부링크 제목
        /// </summary>
        public string LinkExternalTitle { get; set; }
        /// <summary>
        /// 외부링크 썸네일 주소
        /// </summary>
        public string LinkExternalThumbnail { get; set; }
    }
}