﻿using Joins.Apps.Common.JsonConverter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.Cms.Models.Sully.Main
{
    /*
new
            {
                showcase_type = type,
                link_key = data,
                custom_data = new {
                    title = custom_title ?? string.Empty,
                    thumbnail = custom_thumbnail ?? string.Empty,
                    label_image_url = custom_label_image_url ?? string.Empty,
                    reservation_start_dt = custom_reservation_start_dt != null ? string.Format("{0:yyyy-MM-dd HH:mm}", custom_reservation_start_dt) : string.Empty
                },
                original_data = new {
                    title = original_title ?? string.Empty,
                    thumbnail = original_thumbnail ?? string.Empty,
                    label_image_url = original_label_image_url ?? string.Empty,
                    reservation_start_dt = original_reservation_start_dt != null ? string.Format("{0:yyyy-MM-dd HH:mm}", original_reservation_start_dt) : string.Empty
                },
                description = description,
                custom_yn = custom_yn
            };
     */
    public class ShowcaseModel
    {
        /// <summary>
        /// 쇼케이스 종류 (1 : 프로덕트 / 2 : 공지사항 / 3 : 외부링크)
        /// </summary>
        [JsonProperty("showcase_type")]
        public string ShowcaseType { get; set; }
        /// <summary>
        /// 쇼케이스 링크 데이터
        /// </summary>
        [JsonProperty("link_key")]
        public string LinkKey { get; set; }
        /// <summary>
        /// 커스텀 데이터
        /// </summary>
        [JsonProperty("custom_data")]
        public Data CustomData { get; set; }
        /// <summary>
        /// 원본 데이터
        /// </summary>
        [JsonProperty("original_data")]
        public Data OriginalData { get; set; }
        /// <summary>
        /// 기타내용
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 커스텀 여부
        /// </summary>
        [JsonProperty("custom_yn")]
        public string CustomYN { get; set; }

        public class Data
        {
            /// <summary>
            /// 제목
            /// </summary>
            [JsonProperty("title")]
            public string Title { get; set; }
            /// <summary>
            /// 썸네일
            /// </summary>
            [JsonProperty("thumbnail")]
            public string Thumbnail { get; set; }
            /// <summary>
            /// 라벨 이미지
            /// </summary>
            [JsonProperty("label_image_url")]
            public string LabelImage { get; set; }
            /// <summary>
            /// 노출 시작일
            /// </summary>
            [JsonProperty("reservation_start_dt")]
            [JsonConverter(typeof(JsonDateFormatConverter), "{0:yyyy-MM-dd HH:mm}")]
            public DateTime? ReservationStartDateTime { get; set; }
            /// <summary>
            /// 노출 종료일
            /// </summary>
            [JsonProperty("reservation_end_dt")]
            [JsonConverter(typeof(JsonDateFormatConverter), "{0:yyyy-MM-dd HH:mm}")]
            public DateTime? ReservationEndDateTime { get; set; }
            /// <summary>
            /// 노출 종료여부
            /// </summary>
            [JsonProperty("expired")]
            public string ExpiredYN { get; set; }
        }
    }
}