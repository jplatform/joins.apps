﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.Cms.Models.Sully.Main.Parameter
{
    public class Layout
    {
        public int Order_Number { get; set; }
        public string Layout_Type { get; set; }
        public int? Product_Count { get; set; }
        public int? Product_Seq { get; set; }
        public string Image_Url { get; set; }
        public string Background_Color { get; set; }
    }
}