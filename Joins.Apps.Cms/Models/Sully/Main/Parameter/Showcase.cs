﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.Cms.Models.Sully.Main.Parameter
{
    public class Showcase
    {
        public int Idx { get; set; }
        public string Showcase_Type { get; set; }
        public string Link_Key { get; set; }
        public ItemData Data { get; set; }

        public class ItemData
        {
            public string Title { get; set; }
            public string Thumbnail { get; set; }
            public string Label_image_url { get; set; }
            public DateTime? Reservation_start_dt { get; set; }
            public DateTime? Reservation_end_dt { get; set; }
        }
    }
}