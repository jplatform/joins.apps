﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.Cms.Models.Sully.Main
{
    public class SSullyMainShowcaseInfo
    {
        public int PdSeq { get; set; }
        public string UploadFile { get; set; }
        public int OrdNumber { get; set; }
    }

    public class SSullyMainShowcaseSaveInfo
    {
        public IEnumerable<Entity> Parameters { get; set; }

        public class Entity
        {
            public int Seq { get; set; }
            public string Thumbnail { get; set; }
            public HttpPostedFileBase UploadThumbnail { get; set; }
        }
    }
}