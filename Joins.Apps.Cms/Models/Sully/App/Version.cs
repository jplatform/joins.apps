﻿using Joins.Apps.Models.Sully;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.Cms.Models.Sully.App
{
    public class Version
    {
        public AppVersion Android { get; set; }
        public AppVersion IOS { get; set; }
    }
}