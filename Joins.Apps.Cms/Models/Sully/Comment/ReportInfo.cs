﻿using Joins.Apps.Models;
using Joins.Apps.Models.Sully;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace Joins.Apps.Cms.Models.Sully.Comment
{
    public class ReportInfo
    {
        public CommentReportInfo.Comment Comment { get; set; }
        public Page<CommentReportInfo.Report> Report { get; set; }
        public NameValueCollection PageParam { get; set; }
    }

    public class ReportInfoParam
    {
        public int page { get; set; } = 1;
        public int page_size { get; set; } = 5;
        public int cmt_seq { get; set; } = 0;

        public NameValueCollection ToNameValueCollection()
        {
            return new NameValueCollection {
                { "page_size", Convert.ToString(this.page_size) },
                { "cmt_seq", Convert.ToString(this.cmt_seq)  }
            };
        }
    }
}