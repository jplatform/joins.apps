﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.Cms.Models.Sully.Notice
{
    public class Insert
    {
        /// <summary>
        /// 수정할 공지사항 고유번호
        /// </summary>
        public int? Seq { get; set; }
        /// <summary>
        /// 제목
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 내용
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 노출여부
        /// </summary>
        public string UseYN { get; set; }
        /// <summary>
        /// 푸시발송여부
        /// </summary>
        public string PushYN { get; set; }
        /// <summary>
        /// 푸시발송용 제목
        /// </summary>
        public string PushTitle { get; set; }
    }
}