﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.Cms.Models.Sully.Notice
{
    public class List
    {
        /// <summary>
        /// 페이지 번호
        /// </summary>
        public int Page { get; set; } = 1;
        /// <summary>
        /// 검색종류 (1 : 제목 / 2 : 내용)
        /// </summary>
        public string SearchType { get; set; }
        /// <summary>
        /// 검색어
        /// </summary>
        public string SearchValue { get; set; }
        /// <summary>
        /// 사용여부
        /// </summary>
        public string UseYN { get; set; }
    }
}