﻿using Joins.Apps.Models.Sully;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.Cms.Models.Sully.Notice
{
    public class Detail
    {
        public int? Seq { get; set; }
        public bool IsEditMode { get; set; }
        public NoticeDetail Data { get; set; }
    }
}