﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.Cms.Models {
    public class Auth {
        public string mid { get; set; }
        public string mname { get; set; }
        public string group { get; set; }
        public string auth { get; set; }
        public string key { get; set; }
    }
    public class loginChk {
        public bool relogin { get; set; }
        public string group { get; set; }
        public string msg { get; set; }
    }
    public class LoginInfo {
        public string mID { get; set; }
        public string mToken { get; set; }
    }
}