﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.Cms.Controllers
{
    public class MainController : Controller{
        // GET: Main
        [Joins.Apps.Cms.Core.Attribute.LoginCheck(true)]
        public ActionResult Index() { return View(); }
    }
}