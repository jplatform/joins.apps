﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Joins.Apps.Cms.Controllers
{
    public class AuthController : Controller
    {
        [Joins.Apps.Cms.Core.Attribute.LoginCheck(false)]
        public ActionResult Index() {
            Joins.Apps.Cms.Models.Auth mem = Joins.Apps.Cms.Core.LoginAuth.LoadLoginInfo();
            if (mem != null && !string.IsNullOrEmpty(mem.mid)) { return RedirectToAction("Index", "Main"); }
            Joins.Apps.Cms.Models.loginChk loginchk = new Joins.Apps.Cms.Models.loginChk();
            System.Web.HttpContext hc = System.Web.HttpContext.Current;
            if (hc.Request.Cookies["JoinsAppsLoginGroup"] != null) {
                loginchk.group= JCube.AF.Util.Converts.ToString(hc.Request.Cookies["JoinsAppsLoginGroup"].Value, "");
              
            }
            loginchk.relogin = true;
            ViewBag.showMessage = false;
            return View(loginchk);
        }
        // GET: Auth
        [HttpPost]
        [Joins.Apps.Cms.Core.Attribute.LoginCheck(false)]
        public ActionResult Index(string mid, string pwd, string grp="news10",string returnUrl = "") {


            Joins.Apps.Cms.Models.loginChk loginchk = new Joins.Apps.Cms.Models.loginChk();
            if (!string.IsNullOrEmpty(mid) && !string.IsNullOrEmpty(pwd) && !string.IsNullOrEmpty(grp)) {
                SetLoginCookie("", grp);
                //sullry jam login
                if (grp.Equals("sully")) {//sully
                    loginchk.group = "sully";
                    int SullyLoginTryCnt = (JCube.AF.Util.Converts.ToInt32(Session["SullyLoginTry"], 0) + 1);
                    if (SullyLoginTryCnt <= 5) {

#if DEBUG
                        if (!Request.Url.Authority.Equals("apps-cms.joins.com", StringComparison.OrdinalIgnoreCase))
                        {
                            mid = "cms_test";
                            FormsAuthentication.SetAuthCookie(mid + "|" + new Random().Next(1, Int32.MaxValue), false, "/");
                            Joins.Apps.Cms.Core.LoginAuth.GetLoginSave(mid, new Models.Auth { mid = mid, mname = "CMS 테스트", group = "sully" });
                            Joins.Apps.Common.Logger.LogWriteAction(mid, "Sully 로그인 성공");
                            Session["SullyLoginTry"] = 0;

                            if (string.IsNullOrEmpty(returnUrl)) { return RedirectToAction("Index", "Main"); }
                            return Redirect(returnUrl);
                        }
#endif

                        Joins.Apps.Cms.Core.LoginAuth loginauth = new Joins.Apps.Cms.Core.LoginAuth();
                        Joins.Apps.Models.Sully.Jam.login jamlogin = loginauth.GetJAMLogin(mid, pwd);
                        if (jamlogin != null ) {
                            if (jamlogin.code == 200) {
                                Joins.Apps.Models.Sully.Jam.userinfo jamuserinfo = loginauth.GetJAMUserInfo(mid, jamlogin.data.usertoken);
                                if (jamuserinfo != null && jamuserinfo.code == 200) {
                                    if (jamuserinfo.code == 200) {
                                        Joins.Apps.Cms.Models.Auth auth = new Models.Auth();
                                        auth.mid = mid;
                                        auth.mname = jamuserinfo.data.userName;
                                        auth.group = "sully";
                                        FormsAuthentication.SetAuthCookie(mid+"|"+ jamlogin.data.usertoken, false, "/");
                                        Joins.Apps.Cms.Core.LoginAuth.GetLoginSave(mid, auth);
                                        Session["SullyLoginTry"] = 0;
                                        Joins.Apps.Common.Logger.LogWriteAction(mid, "Sully 로그인 성공");
                                        if (string.IsNullOrEmpty(returnUrl)) { return RedirectToAction("Index", "Main"); }
                                        return Redirect(returnUrl);
                                    }
                                }
                                else {
                                    loginchk.relogin = true;
                                    loginchk.msg = ("잠시 후 다시 시도해 주시기 바랍니다.");
                                }
                            }else {
                                string msg = "잠시 후 다시 시도해 주시기 바랍니다.";
                                if (jamlogin.code == 505) {
                                    msg = "JAM 비밀번호가 최종 변경일로부터 1개월이 경과 됐습니다. JAM에서 비밀번호를 변경해 주십시오.";
                                }
                                loginchk.relogin = true;
                                loginchk.msg = msg;
                            }
                        }else {
                            Session["SullyLoginTry"] = SullyLoginTryCnt;
                            loginchk.relogin = true;
                            Joins.Apps.Common.Logger.LogWriteAction(mid, "sully 로그인 실패, Count :" + SullyLoginTryCnt);
                            loginchk.msg = (SullyLoginTryCnt + "번째 로그인에 실패했습니다.");
                        }
                    }else { loginchk.relogin = false; loginchk.msg = ("더 이상 로그인 할 수 없습니다. 관리자에게 문의 하세요."); }
                    // jamlogin.data.usertoken
                }
                else {//news10 login
                    loginchk.group = "news10";
                    int News10LoginTryCnt = (JCube.AF.Util.Converts.ToInt32(Session["News10LoginTry"], 0) + 1);
                    if (News10LoginTryCnt <= 5) {
                        string smid = JCube.AF.Util.Converts.ToString(ConfigurationManager.AppSettings["LoginId"], "");
                        string smpw = JCube.AF.Util.Converts.ToString(ConfigurationManager.AppSettings["LoginPW"], "");
                        if (mid == smid && pwd == smpw) {
                            Joins.Apps.Cms.Models.Auth auth = new Models.Auth();
                            auth.mid = mid;
                            auth.mname = "관리자";
                            auth.group = "news10";
                            FormsAuthentication.SetAuthCookie(mid, false, "/");
                            Joins.Apps.Cms.Core.LoginAuth.GetLoginSave(mid, auth);
                            Session["News10LoginTry"] = 0;
                            Joins.Apps.Common.Logger.LogWriteAction(mid, "news10 로그인 성공");
                            if (string.IsNullOrEmpty(returnUrl)) { return RedirectToAction("Index", "Main"); }
                            return Redirect(returnUrl);
                        }else {
                            Session["News10LoginTry"] = News10LoginTryCnt;
                            loginchk.relogin = true;
                            Joins.Apps.Common.Logger.LogWriteAction(mid, "news10 로그인 실패, Count :" + News10LoginTryCnt);
                            loginchk.msg = (News10LoginTryCnt + "번째 로그인에 실패했습니다.");
                        }

                    }else { loginchk.relogin = false; loginchk.msg = ("더 이상 로그인 할 수 없습니다. 관리자에게 문의 하세요."); }
                }
            }
            ViewBag.showMessage = true;
            return View(loginchk);
        }
        [Joins.Apps.Cms.Core.Attribute.LoginCheck(false)]
        public ActionResult Logout() {
            string ruid = Joins.Apps.Cms.Core.LoginAuth.GetLoginID();
            Joins.Apps.Cms.Models.Auth admmember = Joins.Apps.Cms.Core.LoginAuth.LoadLoginInfo();
            if(admmember != null) {
                if (admmember.group.Equals("sully")) {
                    Joins.Apps.Cms.Core.LoginAuth loginauth = new Joins.Apps.Cms.Core.LoginAuth();
                    loginauth.GetJAMUserLogout(admmember.mid);
                }
            }
            Joins.Apps.Cms.Core.LoginAuth.GetLoginSave(ruid, null);


            FormsAuthentication.SetAuthCookie("", true, "/");
            //      Session["JMnetCommCmsLogin"] = null;
            //return RedirectToAction("login", "Login");
            return Redirect("/");
        }
        private void SetLoginCookie(string userid,string group) {

            System.Web.HttpContext hc = System.Web.HttpContext.Current;
            hc.Response.Cookies.Clear();
            if (!string.IsNullOrEmpty(userid)) {
                System.Web.HttpCookie newCookie1 = new System.Web.HttpCookie("JoinsAppsLoginID");
                newCookie1.Value = userid;
                hc.Response.Cookies.Add(newCookie1);
            }
            if (!string.IsNullOrEmpty(group)) {
                System.Web.HttpCookie newCookie2 = new System.Web.HttpCookie("JoinsAppsLoginGroup");
                newCookie2.Value = group;
                hc.Response.Cookies.Add(newCookie2);
            }
        }
    }
}