﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.Cms.Controllers.Sully
{
    [RoutePrefix("Sully/Particle")]
    [Joins.Apps.Cms.Core.Attribute.AuthGroupCheck("sully")]
    public class ParticleController : Controller
    {
        // GET: Particle
        [Route("")]
        public ActionResult Index(int pgi=1,int ps=20,string uyn="",string ptag="",string r_sdt="", string r_edt = "", string only="") {
            var entity = new Joins.Apps.Models.Page<Joins.Apps.Models.Sully.ImgData> { PageNum = pgi };
            string mid = "";
            var tot_cnt = 0;
            if (!string.IsNullOrEmpty(only) && only == "Y") {
                Joins.Apps.Cms.Models.Auth admember = Joins.Apps.Cms.Core.LoginAuth.LoadLoginInfo();
                mid = admember.mid;
            }
            entity.Data = Joins.Apps.Repository.Sully.CommonRepository.GetParticleImgList(pgi,ps, uyn, ptag, r_sdt, r_edt, mid, out tot_cnt);
            entity.TotalItemCount = tot_cnt;
            entity.DisplayItemCount = ps;
            return View("~/Views/sully/Particle/Index.cshtml", entity);
        }
        [Route("CharacterList")]
        public ActionResult CharacterList(int pgi = 1, int ps = 20, string uyn = "", string ptag = "", string r_sdt = "", string r_edt = "", string only = "", int? character_type = null) {
            var entity = new Joins.Apps.Models.Page<Joins.Apps.Models.Sully.ImgData> { PageNum = pgi };
            string mid = "";
            var tot_cnt = 0;
            if (!string.IsNullOrEmpty(only) && only == "Y") {
                Joins.Apps.Cms.Models.Auth admember = Joins.Apps.Cms.Core.LoginAuth.LoadLoginInfo();
                mid = admember.mid;
            }
            entity.Data = Joins.Apps.Repository.Sully.CommonRepository.GetCharacterImgList(pgi, ps, uyn, ptag, r_sdt, r_edt, mid, character_type, out tot_cnt);
            entity.TotalItemCount = tot_cnt;
            entity.DisplayItemCount = ps;
            return View("~/Views/sully/Particle/CharacterList.cshtml", entity);
        }
    }
}