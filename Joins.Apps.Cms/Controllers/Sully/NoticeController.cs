﻿using Joins.Apps.Cms.Core.Attribute;
using Joins.Apps.Cms.Models.Sully.Notice;
using Joins.Apps.Common.Utilities;
using Joins.Apps.Models;
using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Sully;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.Cms.Controllers.Sully
{
    [RoutePrefix("Sully/Notice")]
    [AuthGroupCheck("sully")]
    public class NoticeController : Controller
    {
        /// <summary>
        /// 목록을 검색합니다.
        /// </summary>
        /// <param name="data">파라메터</param>
        /// <returns></returns>
        [Route("list")]
        public ActionResult GetList(List data)
        {
            var entity = new Page<NoticeList> { PageNum = data.Page };
            var total_count = 0;
            var total_page = 0;

            entity.Data = NoticeRepository.GetList(data.Page, entity.DisplayItemCount, data.SearchType, data.SearchValue, data.UseYN, out total_page, out total_count);
            entity.TotalItemCount = total_count;
            entity.TotalPage = total_page;

            ViewBag.Data = data;

            return View("~/views/sully/notice/list.cshtml", entity);
        }

        /// <summary>
        /// 상세내용을 확인합니다.
        /// </summary>
        /// <param name="id">글 고유번호 nullable</param>
        /// <returns></returns>
        [Route("detail/{id:int?}")]
        public ActionResult GetDetail(int? id)
        {
            var entity = new Detail { Seq = id, IsEditMode = id.HasValue, Data = null };

            if (entity.IsEditMode)
                entity.Data = NoticeRepository.GetDetail(id.Value);

            if (entity.IsEditMode && entity.Data == null)
            {
                entity.Seq = null;
                entity.IsEditMode = false;
            }

            return View("~/views/sully/notice/detail.cshtml", entity);
        }

        /// <summary>
        /// 공지사항을 등록하거나 수정합니다.
        /// </summary>
        /// <param name="data">파라메터</param>
        /// <returns></returns>
        [HttpPost]
        [Route("detail")]
        [ValidateInput(false)]
        public ActionResult PostDetail(Insert data)
        {
            if (!"Y".Equals(data.PushYN, StringComparison.OrdinalIgnoreCase))
                data.PushTitle = string.Empty;

            if (string.IsNullOrWhiteSpace(data.PushTitle))
                data.PushTitle = null;

            NoticeRepository.SetDetail(data.Seq, data.Title, data.Content, data.UseYN, data.PushYN, data.PushTitle);
            return Redirect("List");
        }

        /// <summary>
        /// 이미지를 업로드 합니다.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("upload/image")]
        public ActionResult UploadImage()
        {
            var url = string.Empty;
            var message = string.Empty;
            var allowFile = new string[] { "image/gif", "image/png", "image/jpeg" };

            try
            {
                if (!Request.Files.AllKeys.Any())
                {
                    url = string.Empty;
                    message = "업로드할 파일이 존재하지 않습니다.";
                }

                var file = Request.Files[0];
                if (!allowFile.Contains(file.ContentType))
                {
                    url = string.Empty;
                    message = "업로드할 수 없는 파일입니다.";
                }

                // guid로 생성된 무작위 파일명 생성
                var uploadFileName = Guid.NewGuid() + Path.GetExtension(file.FileName.Replace("\"", ""));
                var ftpUploadPath = string.Format("service/ssully/notice/{0:yyyy}/{0:MM}/{0:dd}/", DateTime.Today);

                var constructorInfo = typeof(HttpPostedFile).GetConstructors(BindingFlags.NonPublic | BindingFlags.Instance)[0];
                var fileObject = (HttpPostedFile)constructorInfo.Invoke(new object[] { file.FileName, file.ContentType, file.InputStream });

                var result = FTPUtility.Upload(fileObject, ftpUploadPath, uploadFileName, true);

                if (result.IsSuccess)
                {
                    url = result.AccessURL;
                    message = string.Empty;
                }
                else
                {
                    url = string.Empty;
                    message = "파일 업로드 중 오류가 발생하였습니다.";
                }
            }
            catch
            {
                url = string.Empty;
                message = "파일 업로드 중 오류가 발생하였습니다.";
            }

            return Content(string.Format(@"<script ""text/javascript"">window.parent.CKEDITOR.tools.callFunction({0}, ""{1}"", ""{2}"");</script>", Request["CKEditorFuncNum"], url, message));
        }
    }
}