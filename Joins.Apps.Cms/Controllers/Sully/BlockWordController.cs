﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.Cms.Controllers.Sully
{
    [Joins.Apps.Cms.Core.Attribute.AuthGroupCheck("sully")]
    [RoutePrefix("Sully/BlockWord")]
    public class BlockWordController : Controller
    {
        [Route("Index")]
        public ActionResult Index(int pgi = 1, int ps = 50, string uyn = "", string search = "")
        {
            var tot_cnt = 0;
            var entity = new Joins.Apps.Models.Page<Joins.Apps.Models.Sully.BlockWordList> { PageNum = pgi };
            entity.Data = Joins.Apps.Repository.Sully.BlockWordRepository.GetBlockWordList(pgi, ps, uyn, search, out tot_cnt);
            entity.TotalItemCount = tot_cnt;
            entity.DisplayItemCount = ps;
            return View("~/Views/sully/BlockWord/Index.cshtml", entity);
        }
        [Route("status")]
        public ActionResult Status(int seq, string st)
        {
            var result = Joins.Apps.Repository.Sully.BlockWordRepository.SetBlockWordStatus(seq, st);
            return Json(new { result = result.IsSuccess, message = result.ResultMessage });
        }
        [HttpPost]
        [Route("regist")]
        public ActionResult Regist(string b_word, string b_desc)
        {
            var result = Joins.Apps.Repository.Sully.BlockWordRepository.SetBlockWord(b_word, b_desc);
            return Json(new { result = result.IsSuccess, message = result.ResultMessage });
        }
    }
}