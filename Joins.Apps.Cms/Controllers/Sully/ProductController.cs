﻿using HtmlAgilityPack;
using Joins.Apps.Cms.Core;
using Joins.Apps.Cms.Core.Attribute;
using Joins.Apps.Cms.Models.Sully.ProductView;
using Joins.Apps.Cms.Models.Sully.ProductView.Parameters;
using Joins.Apps.Common.Sully;
using Joins.Apps.Common.Utilities;
using Joins.Apps.Models;
using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Sully;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Joins.Apps.Cms.Controllers.sully
{
    [AuthGroupCheck("sully")]
    [RoutePrefix("Sully/Product")]
    public class ProductController : Controller
    {
        [NonAction]
        private SSullyProductList GetProductList(ProductListParam param, int? ps = null)
        {
            var tot_cnt = 0;
            string mid = (!string.IsNullOrEmpty(param.only) && param.only == "Y") ? LoginAuth.LoadLoginInfo().mid : string.Empty;

            if (ps.HasValue)
                param.ps = ps.Value;

            var entity = new Page<Joins.Apps.Models.Sully.Product> { PageNum = param.pgi };
            entity.Data = ProductRepository.GetCMSProductList(param, mid, out tot_cnt);
            entity.TotalItemCount = tot_cnt;
            entity.DisplayItemCount = param.ps;

            return new SSullyProductList
            {
                Entity = entity,
                Category = CodeRepository.GetUseCategoryList("C0001"),
                Corner = CodeRepository.GetUseCategoryList("C0004")
            };
        }

        [Route("search")]
        [HttpPost]
        public ActionResult GetProductList(string search_title, int page = 1, int page_size = 10)
        {
            var total_count = 0;
            var data = ProductRepository.GetCMSProductList(new ProductListParam {
                 pgi = page,
                 ps = page_size,
                 pst = "L",
                 s_edt = string.Format("{0:yyyy-MM-dd}", DateTime.Today),
                ptitle = search_title
            }, string.Empty, out total_count);

            return Json(new { list = data.Select(x => new {
                seq = x.PD_SEQ,
                title = x.PD_TITLE,
                image = x.PD_IMG_URL,
                service_date = x.PD_SERVICE_DT,
                category_name = x.CATE_NM,
                url = ConfigurationManager.AppSettings["SSully_Url"] + "/View/" + x.PD_SEQ
            }), total_count = total_count });
        }

        #region [프로덕트 관리]

        /// <summary>
        /// 프로덕트 목록
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("")]
        public ActionResult Index(Joins.Apps.Models.Sully.ProductListParam param)
        {
            return View("~/Views/sully/Product/Index.cshtml", GetProductList(param));
        }

        /// <summary>
        /// 프로덕트 팝업 목록
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("PopProductList")]
        public ActionResult PopProductList(Joins.Apps.Models.Sully.ProductListParam param)
        {
            return View("~/Views/sully/Product/PopProductList.cshtml", GetProductList(param, 5));
        }

        /// <summary>
        /// 수정/작성
        /// </summary>
        /// <param name="pno"></param>
        /// <returns></returns>
        [Route("Edit")]
        public ActionResult Edit(int? pno)
        {
            var loginInfo = Joins.Apps.Cms.Core.LoginAuth.LoadLoginInfo();

            return View("~/Views/sully/Product/Edit.cshtml", new Joins.Apps.Cms.Models.Sully.ProductEdit
            {
                LoginInfo = loginInfo,
                Categories = CodeRepository.GetUseCategoryList("C0001"),
                Corners = CodeRepository.GetUseCategoryList("C0004"),
                Product = pno.HasValue ? ProductRepository.GetProductInfo(pno.Value) : new Apps.Models.Sully.Product(),
                PushInfo = pno.HasValue ? ProductRepository.GetProductPushInfo(pno.Value) : null
            });
        }

        /// <summary>
        /// 수정/작성 처리
        /// </summary>
        /// <param name="param">파라메터</param>
        /// <param name="file">대표 이미지</param>
        /// <param name="share_file">공유 이미지</param>
        /// <param name="provider_file">제공매체 이미지</param>
        /// <param name="pgi">페이지 번호</param>
        /// <param name="only">내프로덕트만 검색 여부</param>
        /// <param name="popup">팝업여부</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        [Route("Edit")]
        public ActionResult Edit(ProductParam param, HttpPostedFileBase file, HttpPostedFileBase share_file, HttpPostedFileBase share_honeyscreen_file, HttpPostedFileBase provider_file, HttpPostedFileBase quiz_event_popup_file, HttpPostedFileBase quiz_event_prize_file, int pgi = 1, string only = "", string popup = "N")
        {
            if (param.PD_SEQ >= 0 && !string.IsNullOrEmpty(param.PD_TITLE))
            {
                bool bFileSave = true;
                var uploadMaxSize = 1 * 1024 * 1024;
                var allowExtensions = new string[] { ".jpg", ".png", ".gif" };
                var admember = Joins.Apps.Cms.Core.LoginAuth.LoadLoginInfo();
                var tempPath = string.Format("{0}ssully\\{1:yyyy}\\{1:MM}\\", ConfigurationManager.AppSettings["FilePath"], DateTime.Today);
                // 허용된 파일 확장자여부 확인
                Func<string, bool> checkExtension = (ext => !string.IsNullOrEmpty(ext) && allowExtensions.Contains(ext.ToLower()));
                // 최대 업로드 사이즈 확인
                Func<int, bool> checkFileSize = (length => length <= uploadMaxSize);
                // FTP 업로드 경로
                Func<string, string> uploadFTPPath = (ext => string.Format("service/ssully/pd/{0:yyyy}/{0:MM}/{0:dd}/", DateTime.Now));
                // 서버에 저장할 파일명 (임시, FTP)
                Func<string, string> uploadFileName = (ext => string.Format("{0:yyyyMMddHHmmss}{1:00000}{2}", DateTime.Now, new Random().Next(10000, 99999), ext));
                Func<string, ContentResult> GetErrorScript = (s => Content(string.Format("<script language='javascript' type='text/javascript'>alert('{0}');history.back(-1);</script>", s)));

                #region 대표이미지 업로드

                // 대표 이미지 FTP 업로드
                if (file != null)
                {
                    var ext = Path.GetExtension(file.FileName);
                    var saveFileName = uploadFileName(ext);
                    var uploadFTPFilePath = uploadFTPPath(ext);
                    var tempFileName = Path.Combine(tempPath, saveFileName);

                    // 파일 확장자 체크
                    if (!checkExtension(ext))
                        return GetErrorScript("허용된 파일 형태가 아닌 파일이 있습니다.");

                    // 파일 용량 체크
                    if (!checkFileSize(file.ContentLength))
                        return GetErrorScript("파일 용량은 1MB 이하로 제한되어있습니다. 파일용량이 초과된 파일이 있습니다.");

                    if (!Directory.Exists(tempPath))
                        Directory.CreateDirectory(tempPath);

                    file.SaveAs(tempFileName);

                    var ftpResult = FTPUtility.Upload(tempPath, saveFileName, uploadFTPFilePath, saveFileName, true);

                    param.PD_IMG_URL = ftpResult.AccessURL;
                    bFileSave = bFileSave && ftpResult.IsSuccess;

                    System.IO.File.Delete(tempFileName);
                }

                #endregion

                #region 허니스크린용 이미지 업로드

                // 공유이미지 FTP 업로드
                if (share_honeyscreen_file != null)
                {
                    var ext = Path.GetExtension(share_honeyscreen_file.FileName);
                    var saveFileName = uploadFileName(ext);
                    var uploadFTPFilePath = uploadFTPPath(ext);
                    var tempFileName = Path.Combine(tempPath, saveFileName);

                    // 파일 확장자 체크
                    if (!checkExtension(ext))
                        return Json(new { result = false, message = "허용된 파일 형태가 아닌 파일이 있습니다." });

                    // 파일 용량 체크
                    if (!checkFileSize(share_honeyscreen_file.ContentLength))
                        return Json(new { result = false, message = "파일 용량은 1MB 이하로 제한되어있습니다. 파일용량이 초과된 파일이 있습니다." });

                    if (!Directory.Exists(tempPath))
                        Directory.CreateDirectory(tempPath);

                    share_honeyscreen_file.SaveAs(tempFileName);

                    var ftpResult = FTPUtility.Upload(tempPath, saveFileName, uploadFTPFilePath, saveFileName, true);

                    param.PD_SHARE_HONEYSCREEN_IMG_URL = ftpResult.AccessURL;
                    bFileSave = bFileSave && ftpResult.IsSuccess;

                    System.IO.File.Delete(tempFileName);
                }

                #endregion

                #region SNS 이미지 업로드

                // 공유이미지 FTP 업로드
                if (share_file != null)
                {
                    var ext = Path.GetExtension(share_file.FileName);
                    var saveFileName = uploadFileName(ext);
                    var uploadFTPFilePath = uploadFTPPath(ext);
                    var tempFileName = Path.Combine(tempPath, saveFileName);

                    // 파일 확장자 체크
                    if (!checkExtension(ext))
                        return GetErrorScript("허용된 파일 형태가 아닌 파일이 있습니다.");

                    // 파일 용량 체크
                    if (!checkFileSize(share_file.ContentLength))
                        return GetErrorScript("파일 용량은 1MB 이하로 제한되어있습니다. 파일용량이 초과된 파일이 있습니다.");

                    if (!Directory.Exists(tempPath))
                        Directory.CreateDirectory(tempPath);

                    share_file.SaveAs(tempFileName);

                    var ftpResult = FTPUtility.Upload(tempPath, saveFileName, uploadFTPFilePath, saveFileName, true);

                    param.PD_SHARE_IMG_URL = ftpResult.AccessURL;
                    bFileSave = bFileSave && ftpResult.IsSuccess;

                    System.IO.File.Delete(tempFileName);
                }

                #endregion

                #region 제공매체(콜라보) 이미지 업로드
                
                // 콜라보 이미지 FTP 업로드
                if (provider_file != null)
                {
                    var ext = Path.GetExtension(provider_file.FileName);
                    var saveFileName = uploadFileName(ext);
                    var uploadFTPFilePath = uploadFTPPath(ext);
                    var tempFileName = Path.Combine(tempPath, saveFileName);

                    // 파일 확장자 체크
                    if (!checkExtension(ext))
                        return GetErrorScript("허용된 파일 형태가 아닌 파일이 있습니다.");

                    // 파일 용량 체크
                    if (!checkFileSize(provider_file.ContentLength))
                        return GetErrorScript("파일 용량은 1MB 이하로 제한되어있습니다. 파일용량이 초과된 파일이 있습니다.");

                    if (!Directory.Exists(tempPath))
                        Directory.CreateDirectory(tempPath);

                    provider_file.SaveAs(tempFileName);

                    var ftpResult = FTPUtility.Upload(tempPath, saveFileName, uploadFTPFilePath, saveFileName, true);

                    param.PD_PROVIDER_URL = ftpResult.AccessURL;
                    bFileSave = bFileSave && ftpResult.IsSuccess;

                    System.IO.File.Delete(tempFileName);
                }

                #endregion

                #region 썰리퀴즈 이벤트 관련

                if ("Y".Equals(param.QUIZ_EVENT_YN, StringComparison.OrdinalIgnoreCase))
                {
                    if (!param.QUIZ_EVENT_NOTICE_DATE.HasValue)
                        return GetErrorScript("이벤트 발표예정일이 입력되지 않았거나 올바르지 않습니다.");
                    if (!param.QUIZ_EVENT_RANGE_START_DATE.HasValue)
                        return GetErrorScript("이벤트 시작일을 입력되지 않았거나 올바르지 않습니다.");
                    if (!param.QUIZ_EVENT_RANGE_END_DATE.HasValue)
                        return GetErrorScript("이벤트 종료일을 입력되지 않았거나 올바르지 않습니다.");
                    if (quiz_event_popup_file == null && string.IsNullOrWhiteSpace(param.QUIZ_EVENT_POPUP_IMAGE_URL))
                        return GetErrorScript("이벤트 팝업 이미지를 선택해 주시기 바랍니다.");
                    if (quiz_event_prize_file == null && string.IsNullOrWhiteSpace(param.QUIZ_EVENT_PRIZE_IMAGE_URL))
                        return GetErrorScript("이벤트 경품 이미지를 선택해 주시기 바랍니다.");

                    // 썰리퀴즈 이벤트 팝업 이미지 FTP 업로드
                    if (quiz_event_popup_file != null)
                    {
                        var ext = Path.GetExtension(quiz_event_popup_file.FileName);
                        var saveFileName = uploadFileName(ext);
                        var uploadFTPFilePath = uploadFTPPath(ext);
                        var tempFileName = Path.Combine(tempPath, saveFileName);

                        // 파일 확장자 체크
                        if (!checkExtension(ext))
                            return GetErrorScript("허용된 파일 형태가 아닌 파일이 있습니다.");

                        // 파일 용량 체크
                        if (!checkFileSize(quiz_event_popup_file.ContentLength))
                            return GetErrorScript("파일 용량은 1MB 이하로 제한되어있습니다. 파일용량이 초과된 파일이 있습니다.");

                        if (!Directory.Exists(tempPath))
                            Directory.CreateDirectory(tempPath);

                        quiz_event_popup_file.SaveAs(tempFileName);

                        var ftpResult = FTPUtility.Upload(tempPath, saveFileName, uploadFTPFilePath, saveFileName, true);

                        param.QUIZ_EVENT_POPUP_IMAGE_URL = ftpResult.AccessURL;
                        bFileSave = bFileSave && ftpResult.IsSuccess;

                        System.IO.File.Delete(tempFileName);
                    }

                    // 썰리퀴즈 이벤트 경품 이미지 FTP 업로드
                    if (quiz_event_prize_file != null)
                    {
                        var ext = Path.GetExtension(quiz_event_prize_file.FileName);
                        var saveFileName = uploadFileName(ext);
                        var uploadFTPFilePath = uploadFTPPath(ext);
                        var tempFileName = Path.Combine(tempPath, saveFileName);

                        // 파일 확장자 체크
                        if (!checkExtension(ext))
                            return GetErrorScript("허용된 파일 형태가 아닌 파일이 있습니다.");

                        // 파일 용량 체크
                        if (!checkFileSize(quiz_event_prize_file.ContentLength))
                            return GetErrorScript("파일 용량은 1MB 이하로 제한되어있습니다. 파일용량이 초과된 파일이 있습니다.");

                        if (!Directory.Exists(tempPath))
                            Directory.CreateDirectory(tempPath);

                        quiz_event_prize_file.SaveAs(tempFileName);

                        var ftpResult = FTPUtility.Upload(tempPath, saveFileName, uploadFTPFilePath, saveFileName, true);

                        param.QUIZ_EVENT_PRIZE_IMAGE_URL = ftpResult.AccessURL;
                        bFileSave = bFileSave && ftpResult.IsSuccess;

                        System.IO.File.Delete(tempFileName);
                    }
                }
                else
                {
                    param.QUIZ_EVENT_YN = "N";
                    param.QUIZ_EVENT_POPUP_IMAGE_URL = null;
                    param.QUIZ_EVENT_PRIZE_IMAGE_URL = null;
                    param.QUIZ_EVENT_NOTICE_DATE = null;
                    param.QUIZ_EVENT_RANGE_END_DATE = null;
                    param.QUIZ_EVENT_RANGE_START_DATE = null;
                }

                #endregion


                if (bFileSave)
                {
                    if (param.PUSH_CATEGORY != null && param.PUSH_CATEGORY.Count() > 0)
                        param.PUSH_CATEGORY_XML = string.Format("<ROOT>{0}</ROOT>", param.PUSH_CATEGORY.Select(x => string.Format("<ITEM>{0}</ITEM>", x)).Aggregate((prev, next) => string.Concat(prev, next)));

                    int r = Joins.Apps.Repository.Sully.ProductRepository.SetCMSProductEdit(param, admember.mid);
                    string msg = "";
                    if (r > 0)
                    {
                        if (param.PD_SEQ > 0) { msg = "수정"; }
                        else { msg = "등록"; }
                        return Content("<script language='javascript' type='text/javascript'>alert('프로덕트가 " + msg + " 되었습니다.');location.href='/sully/Product/Edit?pno=" + r + "&pgi=" + pgi + "&only=" + only + "&popup=" + popup + "';</script>");
                    }
                }
            }

            return Content("<script language='javascript' type='text/javascript'>alert('잠시 후 다시 시도해 주시기 바랍니다.');history.back(-1);</script>");
        }

        /// <summary>
        /// 출고상태 변경
        /// </summary>
        /// <param name="pseq"></param>
        /// <param name="st"></param>
        /// <param name="serviceTime"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("PDStatus")]
        public ActionResult PDStatus(int pseq, string st, string serviceTime = "")
        {// 프로덕트 상태변경
            var status = string.Empty;
            var stMsg = string.Empty;
            var admember = LoginAuth.LoadLoginInfo();

            //CL:즉시출고 RL:예약출고,S:중지,E:중지
            switch (st)
            {
                case "CL":
                    status = "L";
                    stMsg = "출고되었습니다.";
                    serviceTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
                    break;
                case "RL":
                    stMsg = serviceTime + "으로 예약 출고 되었습니다.";
                    status = "L";
                    break;
                case "S":
                    stMsg = "중지 되었습니다.";
                    status = st;
                    serviceTime = "";
                    break;
                case "E":
                    stMsg = "종료 되었습니다.";
                    status = st;
                    serviceTime = "";
                    break;
                default:
                    status = "";
                    break;
            }

            if (string.IsNullOrEmpty(status))
                return Json(new { result = false, message = "입력정보가 올바르지 않습니다." });

            var result = ProductRepository.SetCMSProductStatus(pseq, status, serviceTime, admember.mid);

            if (result)
                return Json(new { result = true, message = stMsg });

            else { return Json(new { result = false, message = "잠시 후 다시 시도해 주시기 바랍니다" }); }

        }

        [Route("PDRelateEdit")]
        public ActionResult PDRelateEdit(int pseq, int rseq, string st)
        {
            // 관련 프로덕트 상태변경
            var stMsg = string.Empty;
            var admember = Joins.Apps.Cms.Core.LoginAuth.LoadLoginInfo();

            switch (st)
            {
                case "Y":
                    stMsg = "등록 되었습니다.";
                    break;
                case "N":
                    stMsg = "삭제 되었습니다.";
                    break;
                default:
                    stMsg = "등록 되었습니다.";
                    break;
            }
            
            var result = ProductRepository.SetCMSProductRelateEdit(pseq, rseq, st, admember.mid);

            if (result)
                return Json(new { result = true, message = stMsg });

            return Json(new { result = false, message = "잠시 후 다시 시도해 주시기 바랍니다" });
        }

        [HttpGet]
        [Route("download/excel/{seq:int}")]
        public ActionResult DownloadProductQuizEventApplyExcelList(int seq)
        {
            var data = ProductRepository.GetCMSProductQuizEventApplyAllList(seq);

            if (data == null || data.ProductInfo == null)
                return Content(string.Format("<script language='javascript' type='text/javascript'>alert('프로덕트 정보를 찾을 수 없습니다.');history.back(-1);</script>"));

            var title = "썰리퀴즈 이벤트 응모현황";
            var subTitle = string.Format("{0} (고유번호 : {1} / 출고일시 : {2:yyyy-MM-dd HH:mm:ss})", data.ProductInfo.Title, data.ProductInfo.Seq, data.ProductInfo.PublishDateTime);

            using (var helper = new ExcelExportHelper())
            {
                var dataTable = helper.ConvertDataTable(data.List);
                var bytes = helper.AddWorkSheetData(dataTable, title, subTitle, title).GetExcelBytes(); ;

                return File(bytes, ExcelExportHelper.ExcelContentType, "썰리퀴즈_이벤트_응모현황.xlsx");
            }
        }

        #endregion

        #region [프로덕트 내용 관리]
        [Route("ContentEdit")]
        public ActionResult ContentEdit(int? pno)
        {
            if (!pno.HasValue || pno.Value <= 0)
                return Redirect("/sully/Product/Edit");

            var admember = LoginAuth.LoadLoginInfo();

            return View("~/Views/sully/Product/ContentEdit.cshtml", new SSullyProductContentEdit
            {
                Product = ProductRepository.GetProductInfo(pno.Value),
                Content = ProductRepository.GetCMSProductContentInfo(pno.Value, admember.mid)
            });
        }

        [HttpPost]
        [ValidateInput(false)]
        [Route("ContentEdit")]
        public ActionResult ContentEdit(Joins.Apps.Models.Sully.ProductContentParam param, int pgi = 1, string only = "", string popup = "N")
        {
            if (param.PD_SEQ <= 0 || string.IsNullOrEmpty(param.PDC_ORG_CONTENT))
                return Content("<script language='javascript' type='text/javascript'>alert('입력정보가 올바르지 않습니다.');history.back(-1);</script>");

            var result = false;
            var data = new ProductContent();
            var admember = LoginAuth.LoadLoginInfo();
            var product = ProductRepository.GetProductInfo(param.PD_SEQ);
            
            if ("Y".Equals(param.PD_TEMP, StringComparison.OrdinalIgnoreCase))
                result = ProductRepository.SetCMSTempProductContent(param, admember.mid);
            else
                result = ProductRepository.SetCMSProductContent(param, admember.mid);

            if (result)
            {
                string msgType = ("Y".Equals(param.PD_TEMP, StringComparison.OrdinalIgnoreCase)) ? "임시저장" : "실반영";
                return Content("<script language='javascript' type='text/javascript'>alert('" + msgType + " 되었습니다.');location.href='/sully/Product/ContentEdit?pno=" + param.PD_SEQ + "&pgi=" + pgi + "&only=" + only + "&popup=" + popup + "';</script>");
            }

            return Content("<script language='javascript' type='text/javascript'>alert('잠시 후 다시 시도해 주시기 바랍니다');history.back(-1);</script>");
        }

        [HttpPost]
        [ValidateInput(false)]
        [Route("ContentEdit/Validate/Type7")]
        public ActionResult ContentEditPreValidate(int seq, IEnumerable<ProductType7ValidateParameter> parameter)
        {
            var xml = string.Empty;
            using (var stringWriter = new StringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(stringWriter, new XmlWriterSettings { OmitXmlDeclaration = true }))
                {
                    var serializer = new XmlSerializer(parameter.GetType(), new XmlRootAttribute("ROOT"));
                    serializer.Serialize(xmlWriter, parameter);
                    xml = stringWriter.ToString();
                }
            }

            var result = ProductRepository.SetProductPoll(seq, xml);

            return Content(Newtonsoft.Json.JsonConvert.SerializeObject(result), "application/json");
        }
        
        [Route("preview/{seq:int}")]
        [ValidateInput(false)]
        public ActionResult Preview(int seq, string html)
        {
            var tot_cnt = 0;
            var admember = LoginAuth.LoadLoginInfo();
            var product = ProductRepository.GetProductInfo(seq);
            var recommentProduct = ProductRepository.GetCMSProductRelateList(seq, out tot_cnt);

            HttpContext.Response.AddHeader("X-XSS-Protection", "0");

            if (string.IsNullOrWhiteSpace(html))
                html = ProductRepository.GetCMSProductContentInfo(seq, admember.mid).PDC_ORG_CONTENT;

            var model = new PreviewModel
            {
                Product = product,
                ContentHtml = ContentOptimizer.GetContentHtml(product, recommentProduct, html),
                RecommendProduct = recommentProduct,
                IsShareLink = "Y".Equals(Request["share"], StringComparison.OrdinalIgnoreCase)
            };

            if (product.PD_TYPE == 4 || product.PD_TYPE == 6)
                return View("~/views/sully/product/preview/viewtype4.cshtml", model);
            else if (product.PD_TYPE == 5)
                return View("~/views/sully/product/preview/viewtype5.cshtml", model);
            else if (product.PD_TYPE == 7)
                return View("~/views/sully/product/preview/viewtype7.cshtml", model);
            else if (product.PD_TYPE == 8)
                return View("~/views/sully/product/preview/viewtype8.cshtml", model);

            return View("~/views/sully/product/preview/viewtype1.cshtml", model);
        }

        #endregion

        #region 이미지 업로드

        /// <summary>
        /// 작성 또는 수정 처리
        /// </summary>
        /// <param name="story">엔티티</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        [Route("CharacterUpload")]
        public ActionResult CharacterUpload(string img_tag, int img_type, IEnumerable<HttpPostedFileBase> files)
        {
            int maxsize = 1 * 1024 * 1024;
            bool bFileSize = true, bFileExt = true, bFileInfo = true;
            string sd = ("\\" + (DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") + "\\"));
            string fp = (System.Web.Configuration.WebConfigurationManager.AppSettings["FilePath"] + "ssully\\" + sd);
            foreach (var file in files)
            {
                if (file != null)
                {
                    /*파일 확장자 체크*/
                    string fpn = (fp + file.FileName);
                    string ext = System.IO.Path.GetExtension(fpn);
                    if (string.IsNullOrEmpty(ext) || "|.jpg|.png|.gif".IndexOf(ext.ToLower()) < 0)
                    {
                        bFileExt = false;
                        break;
                    }

                    /*파일 용량 체크*/
                    if (file.ContentLength > maxsize)
                    {
                        bFileSize = false;
                        break;
                    }

                }
            }

            if (!bFileExt)
                return Json(new { result = false, message = "허용된 파일 형태가 아닌 파일이 있습니다." });

            if (!bFileSize)
                return Json(new { result = false, message = "파일 용량은 1MB 이하로 제한되어있습니다. 파일용량이 초과된 파일이 있습니다." });

            var bFileSave = true;
            var icnt = 0;
            var admember = Joins.Apps.Cms.Core.LoginAuth.LoadLoginInfo();

            foreach (var file in files)
            {
                if (file != null)
                {
                    var saveFileName = string.Empty;
                    var fpn = string.Empty;

                    try
                    {
                        icnt++;
                        Random random = new Random();
                        fpn = (fp + file.FileName);
                        string ext = System.IO.Path.GetExtension(fpn);
                        string fsave = (random.Next(10000, 99999)).ToString("00000");
                        saveFileName = (DateTime.Now.ToString("yyyyMMddHHmmss") + fsave + ext);
                        fpn = (fp + saveFileName);
                        if (JCube.AF.IO.Dir.CheckAndCreate(fp)) { file.SaveAs(fpn); }
                        string surlfath = "service/ssully/ch/" + DateTime.Now.ToString("yyyy") + "/" + DateTime.Now.ToString("MM") + "/";
                        /*FTP 전송 S*/
                        var result = FTPUtility.Upload(fp, saveFileName, surlfath, saveFileName, true);
                        /*FTP 전송 E*/
                        if (result.IsSuccess)
                        {
                            var r = CommonRepository.SetCharacterFileInfo(new ImgFileInfo { url = result.AccessURL, editor = admember.mid, tag = img_tag }, img_type);

                            if (!r)
                                bFileInfo = false;
                        }
                        else
                        {
                            bFileSave = false;
                        }
                    }
                    catch (Exception em)
                    {
                        Joins.Apps.Common.Logger.LogWriteSystemError("파일 업로드 오류", em, "파일 업로드 오류. 파일 경로:" + fp + ",업로드 파일명:" + file.FileName + ",변경 파일명:" + saveFileName); bFileSave = false;
                    }

                    FileInfo f = new System.IO.FileInfo(fpn);

                    if (f.Exists)
                        f.Delete();
                }
            }

            if (!bFileSave || !bFileInfo)
                return Json(new { result = false, message = "파일중 업로드에 실패한 파일이 있습니다." });
            
            return Json(new { result = true, message = "파일이 업로드되었습니다." });
        }

        /// <summary>
        /// 작성 또는 수정 처리
        /// </summary>
        /// <param name="story">엔티티</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        [Route("ImageUpload")]
        public ActionResult ImageUpload(string img_tag, string img_desc, IEnumerable<HttpPostedFileBase> files)
        {
            int maxsize = 5 * 1024 * 1024;
            bool bFileSize = true, bFileExt = true, bFileInfo = true;
            // string saveFileName = "";
            string sd = ("\\" + (DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") + "\\"));
            string fp = (System.Web.Configuration.WebConfigurationManager.AppSettings["FilePath"] + "ssully\\" + sd);

            foreach (var file in files)
            {
                if (file != null)
                {
                    /*파일 확장자 체크*/
                    string fpn = (fp + file.FileName);
                    string ext = System.IO.Path.GetExtension(fpn);
                    if (string.IsNullOrEmpty(ext) || "|.jpg|.png|.gif".IndexOf(ext.ToLower()) < 0) { bFileExt = false; break; }
                    /*파일 용량 체크*/
                    if (file.ContentLength > maxsize) { bFileSize = false; break; }

                }
            }

            if (!bFileExt)
                return Json(new { result = false, message = "허용된 파일 형태가 아닌 파일이 있습니다." });

            if (!bFileSize)
                return Json(new { result = false, message = "파일 용량은 5MB 이하로 제한되어있습니다. 파일용량이 초과된 파일이 있습니다." });

            var bFileSave = true;
            var admember = Joins.Apps.Cms.Core.LoginAuth.LoadLoginInfo();
            var icnt = 0;

            foreach (var file in files)
            {
                if (file != null)
                {
                    string saveFileName = "", fpn = "";
                    try
                    {
                        icnt++;
                        Random random = new Random();
                        fpn = (fp + file.FileName);
                        string ext = System.IO.Path.GetExtension(fpn);
                        string fsave = (random.Next(10000, 99999)).ToString("00000");
                        saveFileName = (DateTime.Now.ToString("yyyyMMddHHmmss") + fsave + ext);
                        fpn = (fp + saveFileName);
                        if (JCube.AF.IO.Dir.CheckAndCreate(fp)) { file.SaveAs(fpn); }
                        string surlfath = "service/ssully/pd/" + DateTime.Now.ToString("yyyy") + "/" + DateTime.Now.ToString("MM") + "/" + DateTime.Now.ToString("dd") + "/";
                        /*FTP 전송 S*/
                        var result = FTPUtility.Upload(fp, saveFileName, surlfath, saveFileName, true);
                        /*FTP 전송 E*/

                        /*파일 정보 저장 s*/
                        if (result.IsSuccess)
                        {
                            Bitmap img = new Bitmap(fpn);//이미지 정보 추출
                            int imageHeight = img.Height;
                            int imageWidth = img.Width;
                            img.Dispose();
                            var imgfileinfo = new Apps.Models.Sully.ImgFileInfo
                            {
                                url = result.AccessURL,
                                editor = admember.mid,
                                size = file.ContentLength / 1024,
                                h = imageHeight,
                                w = imageWidth,
                                tag = img_tag,
                                desc = img_desc
                            };

                            bool r = CommonRepository.SetImgFileInfo(imgfileinfo);
                            if (!r)
                                bFileInfo = false;
                        }
                        else
                        {
                            bFileSave = false;
                        }
                    }
                    catch (Exception em)
                    {
                        Joins.Apps.Common.Logger.LogWriteSystemError("파일 업로드 오류", em, "파일 업로드 오류. 파일 경로:" + fp + ",업로드 파일명:" + file.FileName + ",변경 파일명:" + saveFileName); bFileSave = false;
                    }

                    var f = new System.IO.FileInfo(fpn);

                    if (f.Exists)
                        f.Delete();
                }
            }

            if (!bFileSave || !bFileInfo)
                return Json(new { result = false, message = "파일중 업로드에 실패한 파일이 있습니다." });

            return Json(new { result = true, message = "파일이 업로드되었습니다." });
        }

        #endregion

        [Route("module/type1/image/list")]
        public ActionResult GetModuleType1_ImageList(int page = 1, int page_size = 10, string tag = null, string start_date = null, string end_date = null, string search_my_image = "N")
        {
            var total_count = 0;
            var mid = ("Y".Equals(search_my_image, StringComparison.OrdinalIgnoreCase)) ? LoginAuth.LoadLoginInfo().mid : string.Empty;
            var model = new Page<ImgData> { PageNum = page, DisplayPageCount = 5 };
            model.Data = CommonRepository.GetParticleImgList(page, page_size, "Y", tag, start_date, end_date, mid, out total_count);
            model.TotalItemCount = total_count;
            model.DisplayItemCount = page_size;

            return PartialView("~/views/sully/product/module/item/type1/image_item.cshtml", model);
        }

        [Route("module/type5/image/list")]
        public ActionResult GetModuleType5_ImageList(int page = 1, int page_size = 10, string tag = null, string start_date = null, string end_date = null, string search_my_image = "N")
        {
            var total_count = 0;
            var mid = ("Y".Equals(search_my_image, StringComparison.OrdinalIgnoreCase)) ? LoginAuth.LoadLoginInfo().mid : string.Empty;
            var model = new Page<ImgData> { PageNum = page, DisplayPageCount = 5 };
            model.Data = CommonRepository.GetParticleImgList(page, page_size, "Y", tag, start_date, end_date, mid, out total_count);
            model.DisplayPageCount = 5;
            model.TotalItemCount = total_count;
            model.DisplayItemCount = page_size;

            return PartialView("~/views/sully/product/module/item/type5/image_item.cshtml", model);
        }

        [Route("module/type1/sticker/list")]
        public ActionResult GetModuleSticker(int page = 1, int page_size = 10)
        {
            int total_count = 0;
            var model = new Page<StickerData> { PageNum = page, DisplayPageCount = 5 };
            model.Data = CommonRepository.GetStickerImgList(page, page_size, null, out total_count);
            model.TotalItemCount = total_count;
            model.DisplayItemCount = page_size;

            return PartialView("~/views/sully/product/module/item/type1/sticker_item.cshtml", model);
        }

        [Route("module/type5/image/intro/list")]
        public ActionResult GetStoryIntroImage(int page=1, int page_size = 10)
        {
            var total_count = 0;
            var total_page = 0;
            var model = new Page<SSullyStoryIntroImage> { PageNum = page, DisplayPageCount = 5 };
            model.Data = ProductRepository.GetCMSProductSSullyStoryIntroImageList(page, page_size, out total_count, out total_page);
            model.TotalItemCount = total_count;
            model.TotalPage = total_page;
            model.DisplayItemCount = page_size;

            return PartialView("~/views/sully/product/module/item/type5/intro_item.cshtml", model);
        }
        
        [Route("module/urllink/info")]
        [HttpPost]
        public async Task<ActionResult> GetModuleLink(string url)
        {
            var doc = new HtmlDocument();
            using (var client = new HttpClient())
            {
                doc.LoadHtml(await client.GetStringAsync(url));
            }
            var result = new Dictionary<string, string>();

            foreach (var node in doc.DocumentNode.SelectNodes("//meta/@property/@content"))
            {
                var key = node.GetAttributeValue("property", string.Empty);
                var value = node.GetAttributeValue("content", string.Empty);

                if (!result.ContainsKey(key))
                {
                    result.Add(key, Server.HtmlDecode(value));

                    if ("og:url".Equals(key, StringComparison.OrdinalIgnoreCase))
                    {
                        var uri = new Uri(value);
                        result.Add("custom:domain", uri.GetLeftPart(UriPartial.Scheme | UriPartial.Authority));
                    }
                }
            }

            return Json(result);
        }

        [Route("module/imageupload/upload")]
        public ActionResult SetUploadImage(HttpPostedFileBase file)
        {
            var uploadMaxSize = 5 * 1024 * 1024;
            var allowExtensions = new string[] { ".jpg", ".png", ".gif" };
            var admember = Joins.Apps.Cms.Core.LoginAuth.LoadLoginInfo();
            var tempPath = string.Format("{0}ssully\\{1:yyyy}\\{1:MM}\\", ConfigurationManager.AppSettings["FilePath"], DateTime.Today);
            // 허용된 파일 확장자여부 확인
            Func<string, bool> checkExtension = (ext => !string.IsNullOrEmpty(ext) && allowExtensions.Contains(ext.ToLower()));
            // 최대 업로드 사이즈 확인
            Func<int, bool> checkFileSize = (length => length <= uploadMaxSize);
            // FTP 업로드 경로
            Func<string, string> uploadFTPPath = (ext => string.Format("service/ssully/pd/{0:yyyy}/{0:MM}/{0:dd}/", DateTime.Now));
            // 서버에 저장할 파일명 (임시, FTP)
            Func<string, string> uploadFileName = (ext => string.Format("{0:yyyyMMddHHmmss}{1:00000}{2}", DateTime.Now, new Random().Next(10000, 99999), ext));

            // 대표 이미지 FTP 업로드
            if (file != null)
            {
                var ext = Path.GetExtension(file.FileName);
                var saveFileName = uploadFileName(ext);
                var uploadFTPFilePath = uploadFTPPath(ext);
                var tempFileName = Path.Combine(tempPath, saveFileName);

                // 파일 확장자 체크
                if (!checkExtension(ext))
                    return Json(new { result = false, message = "허용된 파일 형태가 아닌 파일이 있습니다." });

                // 파일 용량 체크
                //if (!checkFileSize(file.ContentLength))
                //    return Json(new { result = false, message = "파일 용량은 1MB 이하로 제한되어있습니다. 파일용량이 초과된 파일이 있습니다." });

                if (!Directory.Exists(tempPath))
                    Directory.CreateDirectory(tempPath);

                file.SaveAs(tempFileName);

                var ftpResult = FTPUtility.Upload(tempPath, saveFileName, uploadFTPFilePath, saveFileName, true);

                var url = ftpResult.AccessURL;
                var result = ftpResult.IsSuccess;

                System.IO.File.Delete(tempFileName);

                if (ftpResult.IsSuccess)
                    return Json(new { result = true, Url = ftpResult.AccessURL, FileName = file.FileName });
            }

            return Json(new { result = false, message = "이미지를 업로드 할 수 없습니다." });
        }

        [Route("quiz/page/{seq:int}")]
        public ActionResult GetQuizInfo(int seq, int page)
        {
            var data = ProductRepository.GetProductQuizInfo(seq, page);
            return Content(Newtonsoft.Json.JsonConvert.SerializeObject(data), "application/json");
        }

        [Route("quiz/answer/{seq:int}")]
        public ActionResult GetQuizAnswerInfo(int seq, int question_seq, int item_seq)
        {
            var data = ProductRepository.GetProductQuizResult(seq, question_seq, item_seq);
            return Content(Newtonsoft.Json.JsonConvert.SerializeObject(data), "application/json");
        }

        [Route("quiz/answer/apply/{seq:int}")]
        public ActionResult SetQuizApply(int seq, IEnumerable<QuizAnswerApply> data)
        {
            var ip = Common.Util.GetClientIPAddress();
            var resultMessage = string.Empty;
            var xml = string.Format("<ROOT>{0}</ROOT>", string.Join(string.Empty, data.Select(x => string.Format(@"<QUESTION SEQ=""{0}"" ITEM_SEQ=""{1}""/>", x.QuestionSeq, x.ItemSeq))));
            var result = ProductRepository.SetProductDetailQuestionApply(seq, null, null, xml, ip, out resultMessage);

            if (!string.IsNullOrEmpty(resultMessage))
            {
                return Json(new
                {
                    result = "N",
                    message = resultMessage
                });
            }

            return Json(new
            {
                result = "Y",
                message = string.Empty,
                correct_ratio = result.CorrectRatio,
                grade = result.Grade,
                correct_count = result.TotalCorrectCount,
                total_count = result.TotalCount
            });
        }
    }
}