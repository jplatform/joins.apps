﻿using HtmlAgilityPack;
using Joins.Apps.Cms.Core.Attribute;
using Joins.Apps.Cms.Models.Sully.Main;
using Joins.Apps.Cms.Models.Sully.Main.Parameter;
using Joins.Apps.Common.Utilities;
using Joins.Apps.Models;
using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Sully;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.Cms.Controllers.Sully
{
    [AuthGroupCheck("sully")]
    [RoutePrefix("sully/main")]
    public class SSullyMainController : Controller
    {
        [Route("layout")]
        public ActionResult GetLayout()
        {
            return View("~/views/sully/main/layout.cshtml");
        }

        private ContentResult JsonContent (object obj)
        {
            return Content(JsonConvert.SerializeObject(obj), "application/json");
        }

        #region 랭킹 API

        /// <summary>
        /// 주간랭킹을 현시간 기준으로 재수집 합니다.
        /// </summary>
        /// <returns></returns>
        [HttpGet, AjaxOnly]
        [Route("api/rank/refresh")]
        public ActionResult SetAPIRankRefresh()
        {
            ProductRepository.SetProductViewRanking();
            return JsonContent(new { result = "Y" });
        }

        #endregion

        #region 레이아웃 API

        /// <summary>
        /// 레이아웃을 가져옵니다.
        /// </summary>
        /// <returns></returns>
        [HttpGet, AjaxOnly]
        [Route("api/layout")]
        public ActionResult GetAPILayout()
        {
            return PartialView("~/views/sully/main/partial/layout.cshtml", SSullyMainRepository.GetCMSMainLayout());
        }

        /// <summary>
        /// 레이아웃을 저장합니다.
        /// </summary>
        /// <returns></returns>
        [HttpPost, AjaxOnly]
        [Route("api/layout")]
        public ActionResult SetAPILayout(IEnumerable<Layout> param)
        {
            var xml_root_format = @"<ROOT>{0}</ROOT>";  
            var xml_format = @"<ITEM PRODUCT_COUNT=""{0}"" ORDER_NUMBER=""{1}"" LAYOUT_TYPE=""{2}"" />";
            var xml_list = param.Select(x => string.Format(xml_format, x.Product_Count, x.Order_Number, x.Layout_Type));
            var xml_string = string.Format(xml_root_format, xml_list.Aggregate((current, next) => string.Concat(current, next)));

            var result = SSullyMainRepository.SetCMSMainLayout(xml_string);

            return JsonContent(new { result = result });
        }

        /// <summary>
        /// 프로덕트를 검색합니다.
        /// </summary>
        /// <returns></returns>
        [HttpPost, AjaxOnly]
        [Route("api/layout/banner/search")]
        public ActionResult SearchAPIProduct(string search_title)
        {
            var total_count = 0;
            var param = new ProductListParam { pgi = 1, ps = 10, ptitle = search_title };
            var data = ProductRepository.GetCMSProductList(param, string.Empty, out total_count);

            return JsonContent(new
            {
                list = data.Select(x => new {
                    seq = x.PD_SEQ,
                    title = x.PD_TITLE,
                    image = x.PD_IMG_URL,
                    service_date = x.PD_SERVICE_DT,
                    category_name = x.CATE_NM,
                    url = ConfigurationManager.AppSettings["SSully_Url"] + "/View/" + x.PD_SEQ
                }),
                total_count = total_count
            });
        }

        /// <summary>
        /// 배너를 등록합니다.
        /// </summary>
        /// <param name="pd_seq"></param>
        /// <param name="background_color"></param>
        /// <param name="image_url"></param>
        /// <param name="reservation_dt"></param>
        /// <returns></returns>
        [HttpPost, AjaxOnly]
        [Route("api/layout/banner")]
        public ActionResult SetBanner(int pd_seq, string background_color, string image_url, string reservation_start_dt, string reservation_end_dt)
        {
            var reservation_start_datetime = DateTime.Now;
            var reservation_end_datetime = DateTime.Now;

            if (!DateTime.TryParse(reservation_start_dt, out reservation_start_datetime))
                return JsonContent(new { result = false, message = "입력된 노출일시는 올바른 날짜형식이 아닙니다." });
            if (!DateTime.TryParse(reservation_end_dt, out reservation_end_datetime))
                return JsonContent(new { result = false, message = "입력된 노출일시는 올바른 날짜형식이 아닙니다." });

            SSullyMainRepository.SetCMSMainLayoutBanner(pd_seq, background_color, image_url, reservation_start_datetime, reservation_end_datetime);

            return JsonContent(new { result = true, message = string.Empty });
        }

        /// <summary>
        /// 배너를 삭제합니다.
        /// </summary>
        /// <param name="pd_seq"></param>
        /// <param name="image_url"></param>
        /// <returns></returns>
        [HttpPost, AjaxOnly]
        [Route("api/layout/banner/remove")]
        public ActionResult RemoveBanner(int pd_seq, string image_url)
        {
            var result = SSullyMainRepository.RemoveCMSMainLayoutBanner(pd_seq, image_url);

            if (result)
                return JsonContent(new { result = true, message = string.Empty });

            return JsonContent(new { result = false, message = "처리중 오류가 발생하였습니다." });
        }

        /// <summary>
        /// 배너 목록을 검색합니다.
        /// </summary>
        /// <param name="page">페이지</param>
        /// <returns></returns>
        [Route("api/layout/banner/list")]
        public ActionResult GetBannerList(int page)
        {
            var total_page = 0;
            var total_count = 0;
            var page_size = 10;
            var data = SSullyMainRepository.GetCMSMainLayoutBannerList(page, page_size, out total_count, out total_page);
            var model = new Page<SSullyMainLayoutBannerCMS>
            {
                PageNum = page,
                DisplayItemCount = page_size,
                TotalItemCount = total_count,
                TotalPage = total_page,
                Data = data
            };

            return PartialView("~/views/sully/main/partial/layout/banner_list.cshtml", model);
        }

        #endregion

        #region 쇼케이스 API

        /// <summary>
        /// 쇼케이스를 가져옵니다.
        /// </summary>
        /// <returns></returns>
        [HttpGet, AjaxOnly]
        [Route("api/showcase")]
        public ActionResult GetAPIShowcase()
        {
            var data = SSullyMainRepository.GetCMSShowcase();
            return JsonContent(data.Select(x => {
                if ("3".Equals(x.ShowcaseType))
                    x.DescriptionText = string.Format("[URL] {0}", x.ShowcaseData);

                var custom_yn = "N";
                var expired = "N";

                if (!x.ShowcaseType.Equals("3"))
                {
                    if (!string.IsNullOrWhiteSpace(x.CustomTitle) || !string.IsNullOrWhiteSpace(x.CustomThumbnail) || !string.IsNullOrWhiteSpace(x.CustomLabelImageUrl))
                        custom_yn = "Y";
                }

                if (x.ShowcaseType.Equals("2"))
                    x.OriginalThumbnail = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mP8/x8AAwMCAO+ip1sAAAAASUVORK5CYII=";

                if (x.ReservationEndDateTime.HasValue && x.ReservationEndDateTime.Value.CompareTo(DateTime.Now) < 0)
                    expired = "Y";

                return new ShowcaseModel
                {
                    ShowcaseType = x.ShowcaseType,
                    LinkKey = x.ShowcaseData,
                    Description = x.DescriptionText,
                    CustomYN = custom_yn,
                    OriginalData = new ShowcaseModel.Data
                    {
                        LabelImage = x.OriginalLabelImageUrl,
                        Thumbnail = x.OriginalThumbnail,
                        Title = x.OriginalTitle,
                        ReservationStartDateTime = x.ReservationStartDateTime,
                        ReservationEndDateTime = x.ReservationEndDateTime,
                        ExpiredYN = expired
                    },
                    CustomData = new ShowcaseModel.Data
                    {
                        LabelImage = x.CustomLabelImageUrl,
                        Thumbnail = x.CustomThumbnail,
                        Title = x.CustomTitle,
                        ReservationStartDateTime = x.ReservationStartDateTime,
                        ReservationEndDateTime = x.ReservationEndDateTime,
                        ExpiredYN = expired
                    }
                };
                //return GenerateShowcaseData(x.ShowcaseType, x.ShowcaseData, x.CustomTitle, x.OriginalTitle, x.CustomThumbnail, x.OriginalThumbnail, x.CustomLabelImageUrl, x.OriginalLabelImageUrl, x.DescriptionText, x.ReservationDateTime, x.ReservationDateTime, custom_yn);
            }));
        }

        /// <summary>
        /// 쇼케이스를 저장합니다.
        /// </summary>
        /// <returns></returns>
        [HttpPost, AjaxOnly]
        [Route("api/showcase")]
        public ActionResult SetAPIShowcase(IEnumerable<Showcase> data)
        {
            if (data == null || data.Count() == 0)
                return Json(new { result = "N", message = "1개 이상의 쇼케이스를 등록해 주셔야 합니다." });

            var items = data.Select(x => {
                var itemXml = new StringBuilder();

                itemXml.Append("<ITEM>");
                itemXml.AppendFormat("<ORD_NUM>{0}</ORD_NUM>", x.Idx);
                itemXml.AppendFormat("<SHOWCASE_TYPE>{0}</SHOWCASE_TYPE>", x.Showcase_Type);
                itemXml.AppendFormat("<SHOWCASE_DATA><![CDATA[{0}]]></SHOWCASE_DATA>", x.Link_Key);
                if (x.Data != null)
                {
                    if (!string.IsNullOrWhiteSpace(x.Data.Title))
                        itemXml.AppendFormat("<TITLE><![CDATA[{0}]]></TITLE>", x.Data.Title);

                    if (!string.IsNullOrWhiteSpace(x.Data.Thumbnail))
                        itemXml.AppendFormat("<THUMBNAIL><![CDATA[{0}]]></THUMBNAIL>", x.Data.Thumbnail);

                    if (!string.IsNullOrWhiteSpace(x.Data.Label_image_url))
                        itemXml.AppendFormat("<LABEL_IMAGE_URL><![CDATA[{0}]]></LABEL_IMAGE_URL>", x.Data.Label_image_url);

                    if (x.Data.Reservation_start_dt.HasValue)
                        itemXml.AppendFormat("<RESERVATION_START_DT><![CDATA[{0:yyyy-MM-dd HH:mm:ss}]]></RESERVATION_START_DT>", x.Data.Reservation_start_dt);
                    else
                        itemXml.AppendFormat("<RESERVATION_START_DT><![CDATA[{0:yyyy-MM-dd HH:mm:ss}]]></RESERVATION_START_DT>", DateTime.Today);

                    if (x.Data.Reservation_end_dt.HasValue)
                        itemXml.AppendFormat("<RESERVATION_END_DT><![CDATA[{0:yyyy-MM-dd HH:mm:ss}]]></RESERVATION_END_DT>", x.Data.Reservation_end_dt);
                    else
                        itemXml.AppendFormat("<RESERVATION_END_DT></RESERVATION_END_DT>");
                }
                itemXml.Append("</ITEM>");

                return Convert.ToString(itemXml);
            });

            var xml = string.Format("<ROOT>{0}</ROOT>", string.Join(string.Empty, items));
            var result = SSullyMainRepository.SetCMSShowcase(xml);

            return JsonContent(new { result = result ? "Y" : "N" });
        }

        /// <summary>
        /// 쇼케이스에 추가할 데이터를 검색합니다.
        /// </summary>
        /// <param name="type">1 : 프로덕트 / 2 : 공지사항 / 3 : 외부url</param>
        /// <param name="search_value">검색어</param>
        /// <returns></returns>
        [HttpPost, AjaxOnly]
        [Route("api/showcase/search")]
        public ActionResult SearchShowcaseData(string type, string search_value)
        {
            if ("1".Equals(type))
            {
                // 프로덕트 검색
                var data = SSullyMainRepository.SearchProduct(search_value);
                if (data == null)
                    return JsonContent(new { result = "N", msg = "검색 결과가 없습니다." });

                return JsonContent(new
                {
                    result = "Y",
                    data = data.Select(x =>
                    {
                        var seq = Convert.ToString(x.PD_SEQ);
                        var title = x.PD_TITLE;
                        var thumbnail = x.PD_IMG_URL;
                        var label_image_url = x.PD_PROVIDER_URL;
                        var description_text = string.Format("[프로덕트] {0:yyyy-MM-dd HH:mm} / {1}", x.PD_SERVICE_DT, x.PD_CATEGORY_NM.Replace("/", "_"));

                        return new ShowcaseModel
                        {
                            ShowcaseType = type,
                            LinkKey = seq,
                            Description = description_text,
                            CustomYN = "N",
                            OriginalData = new ShowcaseModel.Data
                            {
                                LabelImage = label_image_url,
                                Thumbnail = thumbnail,
                                Title = title,
                                ReservationStartDateTime = null,
                                ReservationEndDateTime = null,
                                ExpiredYN = "N"
                            },
                            CustomData = new ShowcaseModel.Data
                            {
                                LabelImage = string.Empty,
                                Thumbnail = string.Empty,
                                Title = string.Empty,
                                ReservationStartDateTime = null,
                                ReservationEndDateTime = null,
                                ExpiredYN = "N"
                            }
                        };
                    })
                });
            }
            else if ("2".Equals(type))
            {
                // 공지사항 검색
                var data = SSullyMainRepository.SearchNotice(search_value);
                var thumbnail = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mP8/x8AAwMCAO+ip1sAAAAASUVORK5CYII=";

                if (data == null)
                    return JsonContent(new { result = "N", msg = "검색 결과가 없습니다." });

                return JsonContent(new
                {
                    result = "Y",
                    data = data.Select(x =>
                    {
                        var seq = Convert.ToString(x.SEQ);
                        var title = x.TITLE;
                        var description_text = string.Format("[공지사항] {0:yyyy-MM-dd HH:mm:ss}", x.REG_DT);

                        return new ShowcaseModel
                        {
                            ShowcaseType = type,
                            LinkKey = seq,
                            Description = description_text,
                            CustomYN = "N",
                            OriginalData = new ShowcaseModel.Data
                            {
                                LabelImage = string.Empty,
                                Thumbnail = thumbnail,
                                Title = title,
                                ReservationStartDateTime = null,
                                ReservationEndDateTime = null,
                                ExpiredYN = "N"
                            },
                            CustomData = new ShowcaseModel.Data
                            {
                                LabelImage = string.Empty,
                                Thumbnail = string.Empty,
                                Title = string.Empty,
                                ReservationStartDateTime = null,
                                ReservationEndDateTime = null,
                                ExpiredYN = "N"
                            }
                        };
                    })
                });
            }
            else if ("3".Equals(type))
            {
                // URL 검색
                var data = new List<Dictionary<string, string>>();
                using (var client = new HttpClient())
                {
                    var doc = new HtmlDocument();
                    doc.LoadHtml(client.GetStringAsync(search_value).GetAwaiter().GetResult());

                    var result = new Dictionary<string, string>();
                    foreach (var node in doc.DocumentNode.SelectNodes("//meta/@property/@content"))
                    {
                        var key = node.GetAttributeValue("property", string.Empty);
                        var value = node.GetAttributeValue("content", string.Empty);

                        result[key] = value;

                        if ("og:url".Equals(key, StringComparison.OrdinalIgnoreCase))
                            result["custom:domain"] = string.Format("[URL] {0}", new Uri(value).GetLeftPart(UriPartial.Scheme | UriPartial.Authority));
                    }

                    data.Add(result);
                }

                if (data?.Count() == 0)
                    return JsonContent(new { result = "N", msg = "검색 결과가 없습니다." });

                return JsonContent(new
                {
                    result = "Y",
                    data = data.Select(x =>
                    {
                        var link = x["og:url"];
                        var title = Server.HtmlDecode(x["og:title"]);
                        var thumbnail = x["og:image"];
                        var description_text = x["custom:domain"];

                        return new ShowcaseModel
                        {
                            ShowcaseType = type,
                            LinkKey = link,
                            Description = description_text,
                            CustomYN = "N",
                            OriginalData = new ShowcaseModel.Data
                            {
                                LabelImage = string.Empty,
                                Thumbnail = thumbnail,
                                Title = title,
                                ReservationStartDateTime = null,
                                ReservationEndDateTime = null,
                                ExpiredYN = "N"
                            },
                            CustomData = new ShowcaseModel.Data
                            {
                                LabelImage = string.Empty,
                                Thumbnail = thumbnail,
                                Title = title,
                                ReservationStartDateTime = null,
                                ReservationEndDateTime = null,
                                ExpiredYN = "N"
                            }
                        };
                    })
                });
            }

            return JsonContent(new { result = "N", msg = "요청 정보가 올바르지 않습니다." });
        }

        #endregion

        #region 코너 리스트 헤더 관리

        [HttpGet]
        [Route("api/corner/header")]
        public ActionResult GetCornerListHeader(int pd_type)
        {
            var data = ProductRepository.GetCornerListHeader(pd_type);
            var hasData = data != null;

            return JsonContent(new {
                result = hasData ? "Y" : "N",
                header = hasData ? new {
                    pd_type = data.PdType,
                    link_type = data.LinkType,
                    link_value = data.LinkValue,
                    image_url = data.ImageUrl,
                    upload_datetime = string.Format("{0:yyyy-mm-dd HH:mm:ss}", data.UploadDateTime),
                    data_seq = data.LinkDataSeq,
                    data_title = data.LinkDataTitle
                } : null
            });
        }

        [HttpPost]
        [Route("api/corner/header/search")]
        public ActionResult GetCornerNoticeSearch(string search_value)
        {
            var data = SSullyMainRepository.SearchNotice(search_value);

            return PartialView("~/views/sully/main/partial/cornernoticelist.cshtml", data);
        }

        [HttpPost, AjaxOnly]
        [Route("api/corner/header")]
        public ActionResult SetCornerListHeader(CornerListHeader data)
        {
            if (!data.PdType.HasValue)
                return JsonContent(new { result = "N", msg = "코너종류 코드를 알 수 없습니다." });
            if (string.IsNullOrWhiteSpace(data.ImageUrl))
                return JsonContent(new { result = "N", msg = "헤더 이미지주소를 알 수 없습니다." });

            ProductRepository.SetCMSCornerListHeader(data.PdType.Value, data.LinkType, data.LinkValue, data.ImageUrl);

            return JsonContent(new { result = "Y", msg = string.Empty });
        }


        #endregion

        #region 이미지 업로드 API

        /// <summary>
        /// 이미지를 업로드 합니다.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost, AjaxOnly]
        [Route("api/image")]
        public ActionResult SaveWidgerPosition(HttpPostedFileBase file)
        {
            var uploadMaxSize = 4 * 1024 * 1024;
            var allowExtensions = new string[] { ".jpg", ".png", ".gif" };
            var tempPath = string.Format("{0}ssully\\{1:yyyy}\\{1:MM}\\", ConfigurationManager.AppSettings["FilePath"], DateTime.Today);
            // 허용된 파일 확장자여부 확인
            Func<string, bool> checkExtension = (ext => !string.IsNullOrEmpty(ext) && allowExtensions.Contains(ext.ToLower()));
            // 최대 업로드 사이즈 확인
            Func<int, bool> checkFileSize = (length => length <= uploadMaxSize);
            // FTP 업로드 경로
            Func<string, string> uploadFTPPath = (ext => string.Format("service/ssully/pd/{0:yyyy}/{0:MM}/{0:dd}/", DateTime.Now));
            // 서버에 저장할 파일명 (임시, FTP)
            Func<string, string> uploadFileName = (ext => string.Format("{0:yyyyMMddHHmmss}{1:00000}{2}", DateTime.Now, new Random().Next(10000, 99999), ext));
            
            if (file != null)
            {
                var ext = Path.GetExtension(file.FileName);
                var saveFileName = uploadFileName(ext);
                var uploadFTPFilePath = uploadFTPPath(ext);
                var tempFileName = Path.Combine(tempPath, saveFileName);

                // 파일 확장자 체크
                if (!checkExtension(ext))
                    return Json(new { result = false, image_url = string.Empty, message = "허용된 파일 형태가 아닌 파일이 있습니다." });

                // 파일 용량 체크
                if (!checkFileSize(file.ContentLength))
                    return Json(new { result = false, image_url = string.Empty, message = "파일 용량은 4MB 이하로 제한되어있습니다. 파일용량이 초과된 파일이 있습니다." });

                if (!Directory.Exists(tempPath))
                    Directory.CreateDirectory(tempPath);

                file.SaveAs(tempFileName);

                var ftpResult = FTPUtility.Upload(tempPath, saveFileName, uploadFTPFilePath, saveFileName, true);

                System.IO.File.Delete(tempFileName);

                if (!ftpResult.IsSuccess)
                    return Json(new { result = false, message = "파일 업로드 중 오류가 발생하였습니다." });

                return Json(new { result = true, image_url = ftpResult.AccessURL, message = string.Empty });
            }

            return Json(new
            {
                result = false,
                image_url = string.Empty,
                message = "파일을 업로드 할 수 없습니다."
            });
        }

        #endregion
    }
}