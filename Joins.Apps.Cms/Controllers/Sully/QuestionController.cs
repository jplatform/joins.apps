﻿using HtmlAgilityPack;
using Joins.Apps.Cms.Core.Attribute;
using Joins.Apps.Cms.Models.Sully.Question;
using Joins.Apps.Cms.Models.Sully.Question.Parameters;
using Joins.Apps.Models;
using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Sully;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Joins.Apps.Cms.Controllers.Sully
{
    [AuthGroupCheck("sully")]
    [RoutePrefix("Sully/Question")]
    public class QuestionController : Controller
    {
        [Route("")]
        public ActionResult GetIndex(int page = 1, int page_size = 10)
        {
            var entity = new Page<QuestionList> { PageNum = page, DisplayItemCount = page_size };

            var total_count = 0;
            var total_page = 0;
            entity.Data = QuestionRepository.GetQuestionList(page, page_size, out total_count, out total_page);
            entity.TotalItemCount = total_count;

            return View("~/Views/sully/Question/Index.cshtml", entity);
        }
        
        [Route("{seq:int}")]
        public ActionResult GetDetail(int seq, int page = 1, int page_size = 10)
        {
            var total_count = 0;
            var total_page = 0;
            var data = QuestionRepository.GetQuestionDetail(seq, page, page_size, out total_count, out total_page);

            var entity = new Detail
            {
                Pages = new Page<QuestionDetailItem>
                {
                    PageNum = page,
                    DisplayItemCount = page_size,
                    TotalItemCount = total_count,
                    TotalPage = total_page,
                    Data = data.Items
                },
                Member = data.MemberInfo
            };

            return View("~/Views/sully/Question/Detail.cshtml", entity);
        }
        
        [Route("api/reply")]
        [HttpPost]
        [AjaxOnly]
        public ActionResult PostReply([System.Web.Http.FromBody] Reply data)
        {
            var result_msg = string.Empty;
            var result = QuestionRepository.SetQuestionReply(data.QuestionSeq, data.ReplyType, data.ReplyContent, data.LinkInternalSeq, data.LinkExternalUrl, data.LinkExternalDomain, data.LinkExternalThumbnail, data.LinkExternalTitle, out result_msg);

            return Json(new { result = result ? "Y" : "N", result_msg = result_msg });
        }

        [Route("api/reply/remove/{question_seq:int}/{reply_seq:int}")]
        [HttpPost]
        [AjaxOnly]
        public ActionResult RemoveReply(int reply_seq, int question_seq)
        {
            var result_msg = string.Empty;
            var result = QuestionRepository.RemoveQuestionReply(question_seq, reply_seq, out result_msg);

            return Json(new { result = result ? "Y" : "N", result_msg = result_msg });
        }

        [Route("api/reply/read/{mem_seq:int}")]
        [HttpPost]
        [AjaxOnly]
        public ActionResult SetReadFlag(int mem_seq)
        {
            var result = QuestionRepository.SetQuestionReadFlag(mem_seq);

            return Json(new { result = result ? "Y" : "N", result_msg = string.Empty });
        }

        [Route("api/reply/push/{question_seq:int}/{reply_seq:int}")]
        [HttpPost]
        [AjaxOnly]
        public ActionResult SetPush(int question_seq, int reply_seq)
        {
            var result_msg = string.Empty;
            var result = QuestionRepository.SetQuestionPush(question_seq, reply_seq, out result_msg);

            return Json(new { result = result ? "Y" : "N", result_msg = result_msg });
        }

        [Route("api/reply/exeternal_url")]
        [HttpPost]
        public async Task<ActionResult> GetModuleLink(string url)
        {
            var doc = new HtmlDocument();
            using (var client = new HttpClient())
            {
                doc.LoadHtml(await client.GetStringAsync(url));
            }
            var result = new Dictionary<string, string>();

            foreach (var node in doc.DocumentNode.SelectNodes("//meta/@property/@content"))
            {
                var key = node.GetAttributeValue("property", string.Empty);
                var value = node.GetAttributeValue("content", string.Empty);

                if (!result.ContainsKey(key))
                {
                    result.Add(key, Server.HtmlDecode(value));

                    if ("og:url".Equals(key, StringComparison.OrdinalIgnoreCase))
                    {
                        var uri = new Uri(value);
                        result.Add("custom:domain", uri.GetLeftPart(UriPartial.Scheme | UriPartial.Authority));
                    }
                }
            }

            return Json(result);
        }
    }
}