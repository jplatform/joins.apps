﻿using Joins.Apps.Common.Sully;
using Joins.Apps.Common.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;


namespace Joins.Apps.Cms.Controllers.Sully
{
    [RoutePrefix("Sully/Common")]
    [Joins.Apps.Cms.Core.Attribute.AuthGroupCheck("sully")]
    public class CommonController : Controller
    {
        #region [파티클 상태 변경]
        [HttpPost]
        [Route("ImgStatus")]
        public ActionResult ImgStatus(int seq, string yn)
        {// 프로덕트 이미지 상태변경
            bool r = false;
            string stMsg = "";

            if (yn == "Y")
            {
                stMsg = "사용으로 변경 되었습니다.";
            }
            else { stMsg = "종료로 변경 되었습니다."; }

            if (string.IsNullOrEmpty(yn)) { return Json(new { result = false, message = "입력정보가 올바르지 않습니다." }); }

            r = Joins.Apps.Repository.Sully.CommonRepository.SetProductImgStatus(seq, yn);

            if (r)
            {
                return Json(new { result = true, message = stMsg });
            }
            else { return Json(new { result = false, message = "잠시 후 다시 시도해 주시기 바랍니다" }); }

        }
        [HttpPost]
        [Route("CharacterStatus")]
        public ActionResult CharacterStatus(int seq, string yn)
        {// 캐릭터 이미지 상태변경
            bool r = false;
            string stMsg = "";
            if (yn == "Y")
            {
                stMsg = "사용으로 변경 되었습니다.";
            }
            else { stMsg = "종료로 변경 되었습니다."; }

            if (string.IsNullOrEmpty(yn)) { return Json(new { result = false, message = "입력정보가 올바르지 않습니다." }); }

            r = Joins.Apps.Repository.Sully.CommonRepository.SetProductCharacterStatus(seq, yn);

            if (r)
            {
                return Json(new { result = true, message = stMsg });
            }
            else { return Json(new { result = false, message = "잠시 후 다시 시도해 주시기 바랍니다" }); }

        }
        #endregion
        [HttpPost]
        [Route("ContConvert")]
        [ValidateInput(false)]
        public ActionResult ContConvert(string sOrgData, int pno)
        {
            //프로덕트 내용 
            var total_count = 0;
            var product = Joins.Apps.Repository.Sully.ProductRepository.GetProductInfo(pno);
            var recommend = Joins.Apps.Repository.Sully.ProductRepository.GetCMSProductRelateList(pno, out total_count);
            return Json(new
            {
                result = true,
                Data = ContentOptimizer.GetContentHtml(product, recommend, sOrgData)
            });
        }

        [HttpPost]
        [Route("imageupload")]
        public ActionResult SetUploadImage()
        {
            var file = Request.Files?.Count > 0 ? Request.Files[0] : null;
            return SetUploadImage(file, string.Empty);
        }
        [HttpPost]
        [Route("imageupload/ckeditor")]
        public ActionResult SetUploadImageEditor()
        {
            var file = Request.Files?.Count > 0 ? Request.Files[0] : null;
            return SetUploadImage(file, "ckeditor");
        }

        [NonAction]
        private JsonResult GetUploadImageResultEditor(bool is_success, string url = "", string fileName = "", string err_msg = "")
        {
            if (is_success)
            {
                if (string.IsNullOrWhiteSpace(err_msg))
                    return Json(new { uploaded = 1, fileName = fileName, url = url });

                return Json(new { uploaded = 1, fileName = fileName, url = url, error = new { message = err_msg } });
            }

            return Json(new { uploaded = 0, error = new { message = err_msg } });
        }

        [NonAction]
        private ActionResult SetUploadImage(HttpPostedFileBase file, string type = "")
        {
            var uploadMaxSize = 5 * 1024 * 1024;
            var allowExtensions = new string[] { ".jpg", ".png", ".gif" };
            var admember = Joins.Apps.Cms.Core.LoginAuth.LoadLoginInfo();
            var tempPath = string.Format("{0}ssully\\{1:yyyy}\\{1:MM}\\", ConfigurationManager.AppSettings["FilePath"], DateTime.Today);
            // 허용된 파일 확장자여부 확인
            Func<string, bool> checkExtension = (ext => !string.IsNullOrEmpty(ext) && allowExtensions.Contains(ext.ToLower()));
            // 최대 업로드 사이즈 확인
            Func<int, bool> checkFileSize = (length => length <= uploadMaxSize);
            // FTP 업로드 경로
            Func<string, string> uploadFTPPath = (ext => string.Format("service/ssully/pd/{0:yyyy}/{0:MM}/{0:dd}/", DateTime.Now));
            // 서버에 저장할 파일명 (임시, FTP)
            Func<string, string> uploadFileName = (ext => string.Format("{0:yyyyMMddHHmmss}{1:00000}{2}", DateTime.Now, new Random().Next(10000, 99999), ext));

            // 대표 이미지 FTP 업로드
            if (file != null)
            {
                var ext = Path.GetExtension(file.FileName);
                var saveFileName = uploadFileName(ext);
                var uploadFTPFilePath = uploadFTPPath(ext);
                var tempFileName = Path.Combine(tempPath, saveFileName);

                // 파일 확장자 체크
                if (!checkExtension(ext))
                {
                    if ("ckeditor".Equals(type, StringComparison.OrdinalIgnoreCase))
                        return GetUploadImageResultEditor(false, err_msg: "허용된 파일 형태가 아닌 파일이 있습니다");

                    return Json(new { result = false, message = "허용된 파일 형태가 아닌 파일이 있습니다." });
                }

                // 파일 용량 체크
                //if (!checkFileSize(file.ContentLength))
                //    return Json(new { result = false, message = "파일 용량은 1MB 이하로 제한되어있습니다. 파일용량이 초과된 파일이 있습니다." });

                if (!Directory.Exists(tempPath))
                    Directory.CreateDirectory(tempPath);

                file.SaveAs(tempFileName);

                var ftpResult = FTPUtility.Upload(tempPath, saveFileName, uploadFTPFilePath, saveFileName, true);

                var url = ftpResult.AccessURL;
                var result = ftpResult.IsSuccess;

                System.IO.File.Delete(tempFileName);

                if (ftpResult.IsSuccess)
                {
                    if ("ckeditor".Equals(type, StringComparison.OrdinalIgnoreCase))
                        return GetUploadImageResultEditor(true, url: ftpResult.AccessURL, fileName: file.FileName);

                    return Json(new { result = true, message = string.Empty, url = ftpResult.AccessURL, file_name = file.FileName });
                }
            }

            if ("ckeditor".Equals(type, StringComparison.OrdinalIgnoreCase))
                return GetUploadImageResultEditor(false, err_msg: "이미지를 업로드 할 수 없습니다.");

            return Json(new { result = false, message = "이미지를 업로드 할 수 없습니다." });
        }
    }
}