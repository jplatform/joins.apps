﻿using Joins.Apps.Cms.Core.Attribute;
using Joins.Apps.Common.Utilities;
using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Sully;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.Cms.Controllers.Sully
{
    [AuthGroupCheck("sully")]
    [RoutePrefix("sully/statistics")]
    public class StatisticsController : Controller
    {
        [Route("")]
        public ActionResult Index()
        {
            return View("~/Views/sully/statistics/index.cshtml");
        }

        [Route("statistics_001")]
        public ActionResult ExcelProductCornerViewCount()
        {
            var item = StatisticsRepository.GetProductCornerViewCount();
            byte[] excelBytes = null;

            if (item != null) {
                using (var helper = new ExcelExportHelper()) {
                    var data = item.GroupBy(x => new { x.code_nm }, (key, items) => new { code_nm = key.code_nm, items = items });

                    data.All(x => {
                        helper.AddWorkSheetData(helper.ConvertDataTable(x.items), "", "", x.code_nm);
                        return true;
                    });
                    excelBytes = helper.GetExcelBytes();
                }
            }

            return File(excelBytes, ExcelExportHelper.ExcelContentType, "프로덕트_코너별_유입량.xlsx");
        }

        [Route("statistics_002")]
        public ActionResult ExcelProductQuestionAnswerCount(int? statistics_002_pd_seq, DateTime? statistics_002_search_date)
        {
            if (!statistics_002_pd_seq.HasValue || !statistics_002_search_date.HasValue)
                return Content("<script>alert('검색 조건이 올바르지 않습니다.\\n확인 후 다시 시도해 주시기 바랍니다.');history.go(-1);</script>");

            var item = StatisticsRepository.GetQuestionAnswerCount(statistics_002_pd_seq.Value, statistics_002_search_date.Value);
            byte[] excelBytes = null;

            if (item != null)
            {
                using (var helper = new ExcelExportHelper())
                {
                    var title = "썰리퀴즈 응답자 수";
                    var subTitle = string.Format("{0} (고유번호 : {1} / 출고일시 : {2:yyyy-MM-dd HH:mm:ss})", item.ProductInfo.Title, item.ProductInfo.Seq, item.ProductInfo.PublishDateTime);

                    helper.AddWorkSheetData(helper.ConvertDataTable(item.AnswerInfo), title, subTitle, title);

                    excelBytes = helper.GetExcelBytes();
                }
            }

            if (excelBytes != null)
                return File(excelBytes, ExcelExportHelper.ExcelContentType, "썰리퀴즈_응답자_수.xlsx");

            return Content("<script>alert('요청하신 데이터가 검색되지 않았습니다.\\n확인 후 다시 시도해 주시기 바랍니다.');history.go(-1);</script>");
        }
    }
}