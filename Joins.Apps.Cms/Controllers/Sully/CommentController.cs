﻿using Joins.Apps.Cms.Core.Attribute;
using Joins.Apps.Cms.Models.Sully.Comment;
using Joins.Apps.Models;
using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Sully;
using System.Web.Mvc;

namespace Joins.Apps.Cms.Controllers.Sully
{
    [RoutePrefix("Sully/Comment")]
    [AuthGroupCheck("sully")]
    public class CommentController : Controller
    {
        [NonAction]
        private Page<CommentList> getCommentList(CommentListParam param)
        {
            var tot_cnt = 0;
            var entity = new Page<CommentList> { PageNum = param.pgi };

            entity.Data = CommentRepository.GetCommentList(param, out tot_cnt);
            entity.TotalItemCount = tot_cnt;
            entity.DisplayItemCount = param.ps;

            return entity;
        }

        /// <summary>
        /// 댓글 목록 페이지
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("")]
        public ActionResult Index(CommentListParam param)
        {
            var entity = getCommentList(param);
            ViewBag.Data = param;

            return View("~/views/sully/comment/index.cshtml", entity);
        }

        /// <summary>
        /// 댓글 목록페이지 팝업
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("PopList")]
        public ActionResult PopList(CommentListParam param)
        {
            param.ps = 20;

            var entity = getCommentList(param);
            ViewBag.Data = param;

            return View("~/views/sully/comment/poplist.cshtml", entity);
        }

        /// <summary>
        /// 댓글 입력차단 목록
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("BannedList")]
        public ActionResult BannedList(BannedListParam param)
        {
            var tot_cnt = 0;
            var entity = new Page<BannedList> { PageNum = param.pgi };
            entity.Data = CommentRepository.GetBannedList(param, out tot_cnt);
            entity.TotalItemCount = tot_cnt;
            entity.DisplayItemCount = param.ps;
            ViewBag.Data = param;

            return View("~/views/sully/comment/bannedlist.cshtml", entity);
        }
        /// <summary>
        /// 댓글 상태 변경 (숨김/삭제)
        /// </summary>
        /// <param name="cmtseq"></param>
        /// <param name="st"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CommentStatus")]
        public ActionResult CommentStatus(int cmtseq, string st)
        {
            var result = CommentRepository.SetCommetStatus(cmtseq, st);
            return Json(new
            {
                result = result.IsSuccess,
                message = result.ResultMessage
            });
        }


        /// <summary>
        /// 작성자 또는 IP 차단
        /// </summary>
        /// <param name="b_mseq"></param>
        /// <param name="b_ip"></param>
        /// <param name="b_days"></param>
        /// <param name="b_desc"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Banned")]
        public ActionResult Banned(int b_mseq, string b_ip, int b_days, string b_desc)
        {
            var result = CommentRepository.SetBanned(b_mseq, b_ip, b_days, b_desc);
            return Json(new
            {
                result = result.IsSuccess,
                message = result.ResultMessage
            });
        }
        
        /// <summary>
        /// 댓글 입력차단 관리
        /// </summary>
        /// <param name="seq"></param>
        /// <param name="st"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("BannedStatus")]
        public ActionResult BannedStatus(int seq, string st)
        {
            var result = CommentRepository.SetBannedStatus(seq, st);
            return Json(new
            {
                result = result.IsSuccess,
                message = result.ResultMessage
            });
        }

        #region 댓글 신고
        
        /// <summary>
        /// 댓글신고 관리 목록
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("Report")]
        public ActionResult ReportList(CommentReportListParam param)
        {
            var model = new Page<CommentReportList> { PageNum = param.pgi };
            model.Data = CommentRepository.GetReportList(param);
            model.TotalItemCount = param.total_count;
            model.DisplayItemCount = param.ps;
            ViewBag.Data = param;

            return View("~/views/sully/comment/reportlist.cshtml", model);
        }

        /// <summary>
        /// 댓글신고 관리 상세정보
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("report/info")]
        public ActionResult ReportCommentInfo(ReportInfoParam param)
        {
            var total_count = 0;
            var entity = CommentRepository.GetReportInfo(param.page, param.page_size, param.cmt_seq, out total_count);

            var model = new ReportInfo
            {
                Comment = entity.ReportCommentInfo,
                Report = new Page<CommentReportInfo.Report>
                {
                    PageNum = param.page,
                    DisplayItemCount = param.page_size,
                    TotalItemCount = total_count,
                    Data = entity.ReportList
                },
                PageParam = param.ToNameValueCollection()
            };

            return PartialView("~/views/sully/comment/partial/reportcommentinfo.cshtml", model);
        }

        /// <summary>
        /// 댓글신고 수락
        /// </summary>
        /// <param name="cmt_seq"></param>
        /// <returns></returns>
        [Route("report/accept")]
        public ActionResult ReportAccept(int cmt_seq)
        {
            var result = CommentRepository.SetCommetStatus(cmt_seq, "S");

            if (result.IsSuccess)
                CommentRepository.SetReportAccept(cmt_seq);

            return Json(new
            {
                result = result.IsSuccess,
                message = result.ResultMessage
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 댓글신고 반려
        /// </summary>
        /// <param name="cmt_seq"></param>
        /// <returns></returns>
        [Route("report/reject")]
        public ActionResult ReportReject(int cmt_seq)
        {
            CommentRepository.SetReportReject(cmt_seq);

            return Json(new
            {
                result = true,
                message = string.Empty
            }, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}