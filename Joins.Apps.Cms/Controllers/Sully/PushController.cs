﻿using Joins.Apps.Common.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.Cms.Controllers.Sully
{
    [RoutePrefix("Sully/Push")]
    [Joins.Apps.Cms.Core.Attribute.AuthGroupCheck("sully")]
    public class PushController : Controller
    {
        #region [일반 푸시 관리]
        [Route("")]
        public ActionResult Index(Joins.Apps.Models.Sully.PushParam param) {
            var tot_cnt = 0;
            // Joins.Apps.Models.Sully.ProductListParam param = new Apps.Models.Sully.ProductListParam();
            var entity = new Joins.Apps.Models.Page<Joins.Apps.Models.Sully.Push> { PageNum = param.pgi };
            param.type = "4";
            entity.Data = Joins.Apps.Repository.Sully.PushRepository.GetPushList(param, out tot_cnt);
            entity.TotalItemCount = tot_cnt;
            entity.DisplayItemCount = param.ps;
            ViewBag.Data = param;
            return View("~/Views/sully/Push/Index.cshtml", entity);
        }
        //[Route("Edit")]
        //public ActionResult Edit() {
        //    return View("~/Views/sully/Push/Edit.cshtml", null);
        //}
        [Route("Edit")]
        public ActionResult Edit(Joins.Apps.Models.Sully.PushParam param) {
            var tot_cnt = 0;
            Joins.Apps.Models.Sully.Push returnItem = new Joins.Apps.Models.Sully.Push();
            if (param.seq > 0) {
                Joins.Apps.Models.Sully.PushParam param1 = new Apps.Models.Sully.PushParam();
                param1.type = "4";
                param1.pgi = 1;
                param1.ps = 1;
                param1.seq = param.seq;
                param1.sdt = param.sdt;
                param1.edt = param.edt;
                IEnumerable<Joins.Apps.Models.Sully.Push> IEnumerableList = Joins.Apps.Repository.Sully.PushRepository.GetPushList(param1, out tot_cnt);
                if (IEnumerableList.Count<Joins.Apps.Models.Sully.Push>() > 0 && tot_cnt > 0) {
                    foreach (Joins.Apps.Models.Sully.Push item in IEnumerableList) { returnItem = item; }
                }
            }
            ViewBag.Data = param;
            return View("~/Views/sully/Push/Edit.cshtml", returnItem);
        }
        [HttpPost]
        [ValidateInput(false)]
        [Route("Edit")]
        public ActionResult Edit(Joins.Apps.Models.Sully.PushEditParam param, HttpPostedFileBase file, int pgi = 1, string order = "", string status = "") {
            int maxsize = 1 * 1024 * 1024;
            //   var data = new Joins.Apps.Models.Sully.Product();
            if (param.seq >= 0 && !string.IsNullOrEmpty(param.p_title)) {
                string sd = ("\\" + (DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") + "\\"));
                string fp = (System.Web.Configuration.WebConfigurationManager.AppSettings["FilePath"] + "ssully\\ph\\" + sd);
                Joins.Apps.Cms.Models.Auth admember = Joins.Apps.Cms.Core.LoginAuth.LoadLoginInfo();
                bool bFileSave = true;
                if (file != null) {
                    string saveFileName = "";
                    /*파일 확장자 체크*/
                    string fpn = (fp + file.FileName);
                    string ext = System.IO.Path.GetExtension(fpn);
                    if (string.IsNullOrEmpty(ext) || "|.jpg|.png|.gif".IndexOf(ext.ToLower()) < 0) { return Json(new { result = false, message = "허용된 파일 형태가 아닌 파일이 있습니다." }); }
                    /*파일 용량 체크*/
                    if (file.ContentLength > maxsize) { return Json(new { result = false, message = "파일 용량은 1MB 이하로 제한되어있습니다. 파일용량이 초과된 파일이 있습니다." }); }
                    Random random = new Random();
                    string fsave = (random.Next(10000, 99999)).ToString("00000");
                    saveFileName = (DateTime.Now.ToString("yyyyMMddHHmmss") + fsave + ext);
                    fpn = (fp + saveFileName);
                    if (JCube.AF.IO.Dir.CheckAndCreate(fp)) { file.SaveAs(fpn); }

                    string surlfath = "service/ssully/pd/" + DateTime.Now.ToString("yyyy") + "/" + DateTime.Now.ToString("MM") + "/" + DateTime.Now.ToString("dd") + "/";
                    /*FTP 전송 S*/
                    var result = FTPUtility.Upload(fp, saveFileName, surlfath, saveFileName, true);
                    /*FTP 전송 E*/
                    param.p_img = result.AccessURL;
                    bFileSave = result.IsSuccess;
                    FileInfo f = new System.IO.FileInfo(fpn);
                    if (f.Exists) { f.Delete(); }
                }
                if (bFileSave) {
                    if (string.IsNullOrEmpty(param.p_type)) { param.p_type = "4"; }

                    param.p_os = string.IsNullOrEmpty(param.p_os) ? null : param.p_os;

                    bool r = Joins.Apps.Repository.Sully.PushRepository.SetPushEdit(param);
                    string msg = "";
                    if (r ) {
                        if (param.seq > 0) { msg = "수정";
                            return Content("<script language='javascript' type='text/javascript'>alert('일반푸시가 " + msg + " 되었습니다.');location.href='/sully/Push/Edit?seq=" + param.seq + "&pgi=" + pgi + "&status=" + status + "&order=" + order + "';</script>");
                        }else { msg = "등록";
                            return Content("<script language='javascript' type='text/javascript'>alert('일반푸시가 " + msg + " 되었습니다.');location.href='/sully/Push/';</script>");
                        }
                    }
                }
            }
            return Content("<script language='javascript' type='text/javascript'>alert('잠시 후 다시 시도해 주시기 바랍니다.');history.back(-1);</script>");
        }
        #endregion
    }
}