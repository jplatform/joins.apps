﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.UI.HtmlControls;

namespace Joins.Apps.Cms.Controllers.Sully.API
{
    [RoutePrefix("Api/Sully/Particle")]
    public class ParticleController : ApiController{
        [HttpPost]
        [Route("UrlInfo")]
        public IHttpActionResult UrlInfo([FromBody] Joins.Apps.Models.Sully.UrlInfoParam param) {

            Joins.Apps.Models.Sully.UrlInfo returnValue = new Joins.Apps.Models.Sully.UrlInfo();
            try {
                string sUrl = param.url;
                if (!string.IsNullOrEmpty(sUrl)) {
                    string shtml = GetPageLoad(sUrl);
                    List<HtmlMeta> htmlmetas = HtmlMetaParser.Parse(shtml);
                    if (htmlmetas.Count > 0) {
                        foreach (HtmlMeta htmlmeta in htmlmetas) {
                            string metatitle = htmlmeta.Name;
                            switch (metatitle) {
                                case "og:title":
                                    returnValue.title = htmlmeta.Content;
                                    break;
                                case "og:description":
                                    returnValue.description = htmlmeta.Content;
                                    break;
                                case "og:image":
                                    returnValue.img = htmlmeta.Content;
                                    break;
                                case "og:url":
                                    returnValue.url = htmlmeta.Content;
                                    returnValue.domain = GetDamain(htmlmeta.Content);
                                    break;
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                returnValue = null;
            }

            return Ok(new { UrlInfo = returnValue });
        }
        [HttpGet]
        [Route("ImgList")]
        public IHttpActionResult ImgList(int pgi=1,int ps=12,string t="",string sdt="",string edt="",string my="Y") {
            int tot_cnt = 0;
            string editor = "";
            var entity = new Joins.Apps.Models.result<Joins.Apps.Models.Sully.ImgData> ();
            if (!string.IsNullOrEmpty(my)&&my.Equals("Y")) {
                Joins.Apps.Cms.Models.Auth meminfo= Joins.Apps.Cms.Core.LoginAuth.LoadLoginInfo();
                editor = meminfo.mid;
            }
            entity.Data =Joins.Apps.Repository.Sully.CommonRepository.GetParticleImgList(pgi, ps, "Y",t, sdt, edt, editor, out tot_cnt);
            entity.t_cnt = tot_cnt;
            if (tot_cnt > 0) { entity.r = 1; entity.msg = ""; }
            else {
                entity.r = 0;
                entity.msg = "데이터 없음";
            }
            return Ok(new { result = entity });
        }

        [HttpGet]
        [Route("CharacterImgList")]
        public IHttpActionResult CharacterImgList(int pgi = 1, int ps = 24, string t = "", string my = "Y", int? character_type = null)
        {
            int tot_cnt = 0;
            string editor = "";
            var entity = new Joins.Apps.Models.result<Joins.Apps.Models.Sully.ImgData>();
            if (!string.IsNullOrEmpty(my) && my.Equals("Y"))
            {
                Joins.Apps.Cms.Models.Auth meminfo = Joins.Apps.Cms.Core.LoginAuth.LoadLoginInfo();
                editor = meminfo.mid;
            }
            entity.Data = Joins.Apps.Repository.Sully.CommonRepository.GetCharacterImgList(pgi, ps, "Y", t, "", "", editor, character_type, out tot_cnt);
            entity.t_cnt = tot_cnt;
            if (tot_cnt > 0) { entity.r = 1; entity.msg = ""; }
            else
            {
                entity.r = 0;
                entity.msg = "데이터 없음";
            }
            return Ok(new { result = entity });
        }
        [HttpGet]
        [Route("Sticker")]
        public IHttpActionResult StickerList(int pgi = 1, int ps = 12, string t = "") {
            int tot_cnt = 0;
            var entity = new Joins.Apps.Models.result<Joins.Apps.Models.Sully.StickerData>();
           
            entity.Data = Joins.Apps.Repository.Sully.CommonRepository.GetStickerImgList(pgi, ps, t, out tot_cnt);
            entity.t_cnt = tot_cnt;
            if (tot_cnt > 0) { entity.r = 1; entity.msg = ""; }
            else {
                entity.r = 0;
                entity.msg = "데이터 없음";
            }
            return Ok(new { result = entity });
        }
        #region [page meta 정보 추출]
        private string GetPageLoad(string srcURL) {
            string rtn = "";
            try {
                HttpWebRequest req = (HttpWebRequest)System.Net.WebRequest.Create(srcURL);
                //  System.Net.WebRequest req = System.Net.WebRequest.Create(srcURL);
                req.Timeout = 60 * 1000;
                req.UserAgent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";
                req.PreAuthenticate = true;
                req.Credentials = CredentialCache.DefaultCredentials;
                using (System.Net.WebResponse res = req.GetResponse()) {
                    req.Timeout = 60 * 1000;  // 밀리초
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(res.GetResponseStream(), System.Text.Encoding.GetEncoding("UTF-8"))) {
                        rtn = sr.ReadToEnd().ToString();
                        sr.Close();
                    }
                }
            } catch (Exception ex) { return ex.ToString(); }
            return rtn;
        }

        private class HtmlMetaParser {
            public enum RobotHtmlMeta {
                None = 0, NoIndex, NoFollow,
                NoIndexNoFollow
            };

            public static List<HtmlMeta> Parse(string htmldata) {
                Regex metaregex =
                    new Regex(@"<meta\s*(?:(?:\b(\w|-)+\b\s*(?:=\s*(?:""[^""]*""|'" +
                              @"[^']*'|[^""'<> ]+)\s*)?)*)/?\s*>",
                              RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);

                List<HtmlMeta> MetaList = new List<HtmlMeta>();
                foreach (Match metamatch in metaregex.Matches(htmldata)) {
                    HtmlMeta mymeta = new HtmlMeta();

                    Regex submetaregex =
                        new Regex(@"(?<name>\b(\w|-)+\b)\" +
                                  @"s*=\s*(""(?<value>" +
                                  @"[^""]*)""|'(?<value>[^']*)'" +
                                  @"|(?<value>[^""'<> ]+)\s*)+",
                                  RegexOptions.IgnoreCase |
                                  RegexOptions.ExplicitCapture);

                    foreach (Match submetamatch in
                             submetaregex.Matches(metamatch.Value.ToString())) {
                        if ("http-equiv" ==
                              submetamatch.Groups["name"].ToString().ToLower())
                            mymeta.HttpEquiv =
                              submetamatch.Groups["value"].ToString();

                        if (("name" ==
                             submetamatch.Groups["name"].ToString().ToLower())
                             && (mymeta.HttpEquiv == String.Empty))
                            mymeta.Name = submetamatch.Groups["value"].ToString();

                        if ("scheme" ==
                            submetamatch.Groups["name"].ToString().ToLower())
                            mymeta.Scheme = submetamatch.Groups["value"].ToString();

                        if ("content" ==
                            submetamatch.Groups["name"].ToString().ToLower()) {
                            mymeta.Content = submetamatch.Groups["value"].ToString();
                            MetaList.Add(mymeta);
                        }
                        if ("property" ==
                          submetamatch.Groups["name"].ToString().ToLower()) {
                            mymeta.Name = submetamatch.Groups["value"].ToString();
                            MetaList.Add(mymeta);
                        }
                    }
                }
                return MetaList;
            }

            public static RobotHtmlMeta ParseRobotMetaTags(string htmldata) {
                List<HtmlMeta> MetaList = HtmlMetaParser.Parse(htmldata);

                RobotHtmlMeta result = RobotHtmlMeta.None;
                foreach (HtmlMeta meta in MetaList) {
                    if (meta.Name.ToLower().IndexOf("robots") != -1 ||
                            meta.Name.ToLower().IndexOf("robot") != -1) {
                        string content = meta.Content.ToLower();
                        if (content.IndexOf("noindex") != -1 &&
                            content.IndexOf("nofollow") != -1) {
                            result = RobotHtmlMeta.NoIndexNoFollow;
                            break;
                        }
                        if (content.IndexOf("noindex") != -1) {
                            result = RobotHtmlMeta.NoIndex;
                            break;
                        }
                        if (content.IndexOf("nofollow") != -1) {
                            result = RobotHtmlMeta.NoFollow;
                            break;
                        }
                    }
                }
                return result;
            }
        }

        private string GetDamain(string url) {
            string r = "";
            if (url.IndexOf("http://") >= 0 || url.IndexOf("https://") >= 0) {
               url = url.Replace("http://","").Replace("https://", "");
                if (url.IndexOf("/") >= 0) {
                    r = url.Substring(0, url.IndexOf("/"));
                }else {
                    r = url;
                }
            }
            return r;
        }
        #endregion

        [HttpGet]
        [Route("RelateList")]
        public IHttpActionResult RelateList(int? pno) {

            int tot_cnt1 = 0;
          
            var entity = new Joins.Apps.Models.result<Joins.Apps.Models.Sully.Product>();
          
            entity.Data = Joins.Apps.Repository.Sully.ProductRepository.GetCMSProductRelateList(pno.Value, out tot_cnt1);
            entity.t_cnt = tot_cnt1;
            if (tot_cnt1 > 0) { entity.r = 1; entity.msg = ""; }
            else {
                entity.r = 0;
                entity.msg = "데이터 없음";
            }
            return Ok(new { result = entity });
        }
    }
}
