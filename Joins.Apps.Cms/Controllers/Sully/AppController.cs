﻿using Joins.Apps.Cms.Core.Attribute;
using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Sully;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.Cms.Controllers.Sully
{
    [AuthGroupCheck("sully")]
    [RoutePrefix("sully/app")]
    public class AppController : Controller
    {
        [Route("version")]
        public ActionResult Version()
        {
            var result = AppRepository.GetVersionInfo();
            var model = new Models.Sully.App.Version();

            if (result != null)
            {
                model.Android = result.FirstOrDefault(x => "A".Equals(x.OS, StringComparison.OrdinalIgnoreCase));
                model.IOS = result.FirstOrDefault(x => "I".Equals(x.OS, StringComparison.OrdinalIgnoreCase));
            }

            model.Android = model.Android ?? new AppVersion { OS = "A", RequiredYN = "N", ModifyDate = null, Version = string.Empty };
            model.IOS = model.IOS ?? new AppVersion { OS = "I", RequiredYN = "N", ModifyDate = null, Version = string.Empty };
            
            return View("~/views/sully/app/version.cshtml", model);
        }

        [Route("version")]
        [HttpPost]
        public JsonResult Version(AppVersion param)
        {
            var result = AppRepository.SetVersionInfo(param);

            if (result != null)
            {
                return Json(new
                {
                    result = "Y",
                    os = result.OS,
                    version = result.Version,
                    update_dt = string.Format("{0:yyyy-MM-dd HH:mm:ss}", result.ModifyDate),
                    required_yn = result.RequiredYN
                });
            }

            return Json(new
            {
                result = "N"
            });
        }
        
        [Route("store_version")]
        public ContentResult StoreVersion()
        {
            using (var client = new HttpClient())
            {
                return Content(client.GetStringAsync("https://apps-api.joins.com/ssully/app/version/check").GetAwaiter().GetResult(), "application/json");
            }
        }
    }
}