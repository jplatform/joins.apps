﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.Cms.Controllers.News10
{
    [Joins.Apps.Cms.Core.Attribute.AuthGroupCheck("news10")]
    public class ShowCaseController : Controller {
        //  public List<List<Joins.Apps.Models.News10.ShowCase>> ShowTodayList1 = null;
        // GET: ShowCase
        public ActionResult Index(int pgi = 1, int ps = 10, string uyn = "") {
            var tot_cnt = 0;
            var entity = new Joins.Apps.Models.Page<Joins.Apps.Models.News10.ShowCase> { PageNum = pgi };
            Joins.Apps.Models.News10.ShowCase s = new Joins.Apps.Models.News10.ShowCase() {
                SEQ = 0,
                BRIEF_TYPE = "",
                USED_YN = uyn,
                BASIC_YN = "",
                WEATHER_TYPE = "",
                SPECIAL_DAY = ""
            };
            entity.Data = Joins.Apps.Repository.News10.ShowCaseRepository.GetShowCaseList(pgi, ps, s, out tot_cnt);
            entity.TotalItemCount = tot_cnt;
            entity.DisplayItemCount = ps;
            ViewBag.ShowTodayList = Joins.Apps.Repository.News10.ShowCaseRepository.GetShowCaseToday();

            return View(entity);
        }

        public ActionResult Edit(int? sno) {
            var data = new Joins.Apps.Models.News10.ShowCase();
            var tot_cnt = 0;
            if (sno.HasValue) {
                Joins.Apps.Models.News10.ShowCase s = new Joins.Apps.Models.News10.ShowCase() {
                    SEQ = sno.Value,
                    BRIEF_TYPE = "",
                    USED_YN = "",
                    BASIC_YN = "",
                    WEATHER_TYPE = "",
                    SPECIAL_DAY = ""
                };
                IEnumerable<Joins.Apps.Models.News10.ShowCase> show=Joins.Apps.Repository.News10.ShowCaseRepository.GetShowCaseList(1, 1, s, out tot_cnt);
                data = show.First<Joins.Apps.Models.News10.ShowCase>();
            }
            return View(data);
        }
        /// <summary>
        /// 작성 또는 수정 처리
        /// </summary>
        /// <param name="story">엔티티</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(Joins.Apps.Models.News10.ShowCase exc, HttpPostedFileBase file,int pgi){
            int r = 0;
            bool bImgUpload = true;
            string imgurl = "";
            if (exc != null) {
                if (file != null) {
                    string sfilepath = Joins.Apps.Common.Util.imgUpload(file, "");
                    if (sfilepath.Equals("t")) {// 허용된 확장자가 아닐때
                        return Content("<script language='javascript' type='text/javascript'>alert('허용된 파일 형태가 아닙니다.');history.back(-1);</script>");
                    }
                    else if (sfilepath.Equals("s")) {//용량초과
                        return Content("<script language='javascript' type='text/javascript'>alert('파일 용량은 1MB 이하로 제한되어있습니다 .');history.back(-1);</script>");
                    }
                    else if (sfilepath.Equals("e")) {//업로드 에러
                        return Content("<script language='javascript' type='text/javascript'>alert('파일 업로드중 에러가 발생하였습니다.');history.back(-1);</script>");
                    }
                    else {
                        string surlfath = "/News10/cover/" + DateTime.Now.ToString("yyyy") + "/"; //+ DateTime.Now.ToString("MM") + "/");
                        JCube.AF.Module.FTPConnectInfo ftpinfo = new JCube.AF.Module.FTPConnectInfo();
                        ftpinfo.Addr = "ftp.images.joins.com";
                        ftpinfo.BasePath = "/";
                        ftpinfo.Port = "8021";
                        ftpinfo.UID = "uximg";
                        ftpinfo.UPW = "ux0lalwl!@";
                        ftpinfo.UsePassive = true;
                        var fileName = Path.GetFileName(sfilepath);
                        sfilepath = sfilepath.Replace(fileName, "");
                        //string imgurl = Joins.Apps.Common.Util.UploadFileToFtp(sfilepath);
                        bool rr = JCube.AF.Module.FTP.FTPUploadTo(ftpinfo, sfilepath, fileName, surlfath, fileName, false);
                        if (!rr) { bImgUpload = false; return Content("<script language='javascript' type='text/javascript'>alert('파일 업로드중 에러가 발생하였습니다.');history.back(-1);</script>"); }
                        else { imgurl = "http://images.joins.com" + surlfath + fileName; }
                        FileInfo f = new System.IO.FileInfo(sfilepath + fileName);
                        if (f.Exists) { f.Delete(); }
                        //   bool rr = false;

                    }
                }
                if (bImgUpload) {
                    Joins.Apps.Cms.Models.Auth admmember = Joins.Apps.Cms.Core.LoginAuth.LoadLoginInfo();
                    if (exc.WEATHER_TYPE == "0") { exc.BASIC_YN = "Y"; }
                    else { exc.BASIC_YN = "N"; }
                    if (!string.IsNullOrEmpty(exc.SPECIAL_DAY)) { exc.BASIC_YN = "N"; exc.WEATHER_TYPE = "0"; }
                    exc.REG_ID = admmember.mid;
                    exc.IMG = imgurl;
                    r = Joins.Apps.Repository.News10.ShowCaseRepository.SetShowCaseItemEdit(exc);
                    if (r <= 0) { return Content("<script language='javascript' type='text/javascript'>alert('잠시후 다시 시도해 주시기 바랍니다.');history.back(-1);</script>"); }
                    else {
                        string m = exc.SEQ > 0 ? "수정" : "등록";
                        return Content("<script language='javascript' type='text/javascript'>alert('" + m + " 되었습니다.');location.href='/ShowCase/Edit?sno=" + r + "&pgi=" + pgi + "';</script>");
                    }
                }
            }
            else {
                //입력정보가 올바르지 않습니다.
                return Content("<script language='javascript' type='text/javascript'>alert('입력정보가 올바르지 않습니다.');history.back(-1);</script>");
            }
            ////string msg = "";
            ////if (r == 2) { msg = "이미 제외된 기사입니다."; }
            ////else { msg = "잠시 후 다시 시도해 주시기 바랍니다."; }
            ////return Json(new {
            ////    result = false,
            ////    message = msg
            ////});
            //var data = new Joins.Apps.Models.News10.ShowCase();
            //var tot_cnt = 0;
            //if (r > 0) {
            //    Joins.Apps.Models.News10.ShowCase s = new Joins.Apps.Models.News10.ShowCase() {
            //        SEQ = r,
            //        BRIEF_TYPE = "",
            //        USED_YN = "",
            //        BASIC_YN = "",
            //        WEATHER_TYPE = "",
            //        SPECIAL_DAY = ""
            //    };
            //    IEnumerable<Joins.Apps.Models.News10.ShowCase> show = Joins.Apps.Repository.News10.ShowCaseRepository.GetShowCaseList(1, 1, s, out tot_cnt);
            //    data = show.First<Joins.Apps.Models.News10.ShowCase>();
            //}
            // Joins.Apps.Common.Util.MSGShow("수정/저장 되었습니다.", "/ShowCase/");
            //   return View();
            return Content("<script language='javascript' type='text/javascript'>alert('잠시후 다시 시도해 주시기 바랍니다.');history.back(-1);</script>");

        }
    }
}