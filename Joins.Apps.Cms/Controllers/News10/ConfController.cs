﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.Cms.Controllers.News10
{
    [Joins.Apps.Cms.Core.Attribute.AuthGroupCheck("news10")]
    public class ConfController : Controller
    {
        // GET: Conf
        public ActionResult Index(){
            return View();
        }
        public ActionResult Apis() {
            return View();
        }
        public ActionResult BriefRestore() {
            Joins.Apps.Models.News10.BriefRestoreData viewData=Joins.Apps.Repository.News10.CommonRepository.GetBriefRestore();
            return View(viewData);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult BriefRestoreEdit(string yn="") {
            bool r = Joins.Apps.Repository.News10.CommonRepository.SetBriefRestore(yn);
            string msg = "";
            if (r) { return Json(new { result = true, message = "등록되었습니다." }); }
            else { msg = "잠시 후 다시 시도해 주시기 바랍니다."; }
            return Json(new {
                result = false,
                message = msg
            });
        }

    }
}