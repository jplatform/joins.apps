﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.Cms.Controllers.News10
{
    [Joins.Apps.Cms.Core.Attribute.AuthGroupCheck("news10")]
    public class PushController : Controller
    {
        [HttpGet]
        public ActionResult PushList(int pgi = 1, int ps = 20, string uyn="") {
            var tot_cnt = 0;
            var entity = new Joins.Apps.Models.Page<Joins.Apps.Models.News10.NotiPush> { PageNum = pgi };
            entity.Data = Joins.Apps.Repository.News10.PushRepository.GetNotiPushList(pgi, ps, 0,uyn,out tot_cnt);
            entity.TotalItemCount = tot_cnt;
            entity.DisplayItemCount = ps;
            return View(entity);
        }
        public ActionResult PushEdit(int? sno) {
            var data = new Joins.Apps.Models.News10.NotiPush();
            var tot_cnt = 0;
            if (sno.HasValue) {
                IEnumerable<Joins.Apps.Models.News10.NotiPush> show = Joins.Apps.Repository.News10.PushRepository.GetNotiPushList(1, 1, sno.Value,"", out tot_cnt);
                data = show.First<Joins.Apps.Models.News10.NotiPush>();
            }
            return View(data);
        }
        /// <summary>
        /// 작성 또는 수정 처리
        /// </summary>
        /// <param name="story">엔티티</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PushEdit(Joins.Apps.Models.News10.NotiPush exc) {
            int r = 0;
            if (exc != null) {
                Joins.Apps.Cms.Models.Auth admmember = Joins.Apps.Cms.Core.LoginAuth.LoadLoginInfo();
                exc.REG_ID = admmember.mid;
                exc.PUSH_TYPE = "0";
               // exc.OS_DIV = "ALL";
                r = Joins.Apps.Repository.News10.PushRepository.SetNotiPushEdit(exc);
                if (r>0) { return Json(new { result = true, message = "" }); }
            }
            string msg = "잠시 후 다시 시도해 주시기 바랍니다.";
         //   if (r == 2) { msg = "이미 제외된 기사입니다."; }
         //   else {msg = "잠시 후 다시 시도해 주시기 바랍니다.";}
            return Json(new {
                result = false,
                message = msg
            });
        }
    }
}