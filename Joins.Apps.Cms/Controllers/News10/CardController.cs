﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.Cms.Controllers.News10
{
    [Joins.Apps.Cms.Core.Attribute.AuthGroupCheck("news10")]
    public class CardController : Controller
    {
        [HttpGet]
        public ActionResult CardList(int pgi=1,int ps=1000 , string uyn = "") {
            var tot_cnt = 0;
            var entity = new Joins.Apps.Models.Page<Joins.Apps.Models.News10.CardInfo> { PageNum = pgi };
            entity.Data = Joins.Apps.Repository.News10.CardRepository.GetCmsCardList(pgi,ps,uyn, out tot_cnt);
            entity.TotalItemCount = tot_cnt;
            entity.DisplayItemCount = ps;
            return View(entity);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CardListYN(int seq,string uyn) {
            int r = 0;
            if (seq>0&&!string.IsNullOrEmpty(uyn)) {r = Joins.Apps.Repository.News10.CardRepository.SetCardListYN(seq, uyn);}
            string msg = "";
            if (r > 0) { return Json(new { result = true, message = "" }); }
            else {
                msg = "잠시 후 다시 시도해 주시기 바랍니다.";
                return Json(new {
                    result = false,
                    message = msg
                });
            }
        }
        [HttpGet]
        public ActionResult ExceptArticleList(int pgi = 1, int ps = 20, int tid = 0,string uyn="", string sdt = "", string edt = "") {
            var tot_cnt = 0;
            var entity = new Joins.Apps.Models.Page<Joins.Apps.Models.News10.ExceptCard> { PageNum = pgi };
            entity.Data = Joins.Apps.Repository.News10.CardRepository.GetExceptArticleList(pgi, ps, tid,uyn,sdt,edt,out tot_cnt);
            entity.TotalItemCount = tot_cnt;
            entity.DisplayItemCount = ps;
            return View(entity);
        }
        public ActionResult ExceptArticleEdit(int? sno) {
            var data = new Joins.Apps.Models.News10.ExceptCard();

            if (sno.HasValue) {
                data = Joins.Apps.Repository.News10.CardRepository.GetExceptArticleItem(sno.Value);
            }
            return View(data);
        }
        /// <summary>
        /// 작성 또는 수정 처리
        /// </summary>
        /// <param name="story">엔티티</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ExceptArticleEdit(Joins.Apps.Models.News10.ExceptCard exc) {
            int r = 0;
            if (exc != null) {
                Joins.Apps.Cms.Models.Auth admmember = Joins.Apps.Cms.Core.LoginAuth.LoadLoginInfo();
                exc.reg_id = admmember.mid;
                r = Joins.Apps.Repository.News10.CardRepository.SetExceptArticleItemEdit(exc);
                if (r==1) { return Json(new { result = true, message = "" }); }
            }
            string msg = "";
            if (r == 2) { msg = "이미 제외된 기사입니다."; }
            else {msg = "잠시 후 다시 시도해 주시기 바랍니다.";}
            return Json(new {
                result = false,
                message = msg
            });
        }
    }
}