﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.Cms.Controllers.News10
{
    [Joins.Apps.Cms.Core.Attribute.AuthGroupCheck("news10")]
    public class EventController : Controller{
        // GET: Event
        public ActionResult Index(int pgi = 1, int ps = 10, string uyn = "") {
            var tot_cnt = 0;
            var entity = new Joins.Apps.Models.Page<Joins.Apps.Models.News10.Event> { PageNum = pgi };

            entity.Data = Joins.Apps.Repository.News10.EventRepository.GetCMSEvent(pgi, ps,0,uyn, out tot_cnt);
            entity.TotalItemCount = tot_cnt;
            entity.DisplayItemCount = ps;
          //  ViewBag.ShowTodayList = Joins.Apps.Repository.News10.ShowCaseRepository.GetShowCaseToday();

            return View(entity);
        }
        public ActionResult Edit(int? sno) {
            var data = new Joins.Apps.Models.News10.Event();
            var tot_cnt = 0;
            if (sno.HasValue) {
                IEnumerable<Joins.Apps.Models.News10.Event> show = Joins.Apps.Repository.News10.EventRepository.GetCMSEvent(1, 1, sno.Value, "", out tot_cnt);
                data = show.First<Joins.Apps.Models.News10.Event>();
            }
            return View(data);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ImageUpload( HttpPostedFileBase file) {
            bool bImgUpload = false;
            string smsg = "잠시 후 다시 시도해 주시기 바랍니다.";
            if (file != null) {
                string sfilepath = Joins.Apps.Common.Util.imgUpload(file, "");
                if (sfilepath.Equals("t")) {// 허용된 확장자가 아닐때
                    bImgUpload = false;
                    smsg = "허용된 파일 형태가 아닙니다.";
                    return Content("<script language='javascript' type='text/javascript'>alert('허용된 파일 형태가 아닙니다.');history.back(-1);</script>");
                }else if (sfilepath.Equals("s")) {//용량초과
                    bImgUpload = false;
                    smsg = "파일 용량은 1MB 이하로 제한되어있습니다 .";
                }else if (sfilepath.Equals("e")) {//업로드 에러
                    bImgUpload = false;
                    smsg = "파일 업로드중 에러가 발생하였습니다.";
                }else {
                    string surlfath = "/News10/event/" + DateTime.Now.ToString("yyyy") + "/"; //+ DateTime.Now.ToString("MM") + "/");
                    JCube.AF.Module.FTPConnectInfo ftpinfo = new JCube.AF.Module.FTPConnectInfo();
                    ftpinfo.Addr = "ftp.images.joins.com";
                    ftpinfo.BasePath = "/";
                    ftpinfo.Port = "8021";
                    ftpinfo.UID = "uximg";
                    ftpinfo.UPW = "ux0lalwl!@";
                    ftpinfo.UsePassive = true;
                    var fileName = Path.GetFileName(sfilepath);
                    sfilepath = sfilepath.Replace(fileName, "");
                    //string imgurl = Joins.Apps.Common.Util.UploadFileToFtp(sfilepath);
                    bool rr = JCube.AF.Module.FTP.FTPUploadTo(ftpinfo, sfilepath, fileName, surlfath, fileName, false);
                    if (!rr) { bImgUpload = true; smsg = "파일 업로드중 에러가 발생하였습니다."; }
                    else { smsg = "http://images.joins.com" + surlfath + fileName; }
                    FileInfo f = new System.IO.FileInfo(sfilepath + fileName);
                    if (f.Exists) { f.Delete(); }
                    //   bool rr = false;

                }
            }
            return View(new {result= bImgUpload, msg= smsg });
        }
        /// <summary>
        /// 작성 또는 수정 처리
        /// </summary>
        /// <param name="pgi">페이지</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(Joins.Apps.Models.News10.Event exc, HttpPostedFileBase file, HttpPostedFileBase file2, int pgi) {
            int r = 0;
            bool bImgUpload = true;
            string imgurl = "", imgurl1="";
            if (exc != null) {
                var fileName = "";
                if (file != null) {
                   string sfilepath = Joins.Apps.Common.Util.imgUpload(file, "");
                    if (sfilepath.Equals("t")) {// 허용된 확장자가 아닐때
                        return Content("<script language='javascript' type='text/javascript'>alert('허용된 파일 형태가 아닙니다.');history.back(-1);</script>");
                    }else if (sfilepath.Equals("s")) {//용량초과
                        return Content("<script language='javascript' type='text/javascript'>alert('파일 용량은 1MB 이하로 제한되어있습니다 .');history.back(-1);</script>");
                    }else if (sfilepath.Equals("e")) {//업로드 에러
                        return Content("<script language='javascript' type='text/javascript'>alert('파일 업로드중 에러가 발생하였습니다.');history.back(-1);</script>");
                    }else {
                        string surlfath = "/News10/event/" + DateTime.Now.ToString("yyyy") + "/"; //+ DateTime.Now.ToString("MM") + "/");
                        JCube.AF.Module.FTPConnectInfo ftpinfo = new JCube.AF.Module.FTPConnectInfo();
                        ftpinfo.Addr = "ftp.images.joins.com";
                        ftpinfo.BasePath = "/";
                        ftpinfo.Port = "8021";
                        ftpinfo.UID = "uximg";
                        ftpinfo.UPW = "ux0lalwl!@";
                        ftpinfo.UsePassive = true;
                        fileName = Path.GetFileName(sfilepath);
                        sfilepath = sfilepath.Replace(fileName, "");
                        //string imgurl = Joins.Apps.Common.Util.UploadFileToFtp(sfilepath);
                        bool rr = JCube.AF.Module.FTP.FTPUploadTo(ftpinfo, sfilepath, fileName, surlfath, fileName, false);
                        if (!rr) { bImgUpload = false; return Content("<script language='javascript' type='text/javascript'>alert('파일 업로드중 에러가 발생하였습니다.');history.back(-1);</script>"); }
                        else { imgurl = "http://images.joins.com" + surlfath + fileName; }
                        FileInfo f = new System.IO.FileInfo(sfilepath + fileName);
                        if (f.Exists) { f.Delete(); }
                        //   bool rr = false;

                    }
                }
                if (file2 != null) {
                    string filename2 = "";
                    if (!string.IsNullOrEmpty(fileName)) {
                        int iex= fileName.LastIndexOf(".");
                       string f1= fileName.Substring(0,  iex);
                        string f2 = fileName.Substring(iex, fileName.Length - iex);
                        filename2 = f1+"_bg" + f2;
                    }
                    string sfilepath = Joins.Apps.Common.Util.imgUpload(file2, filename2);
                    if (sfilepath.Equals("t")) {// 허용된 확장자가 아닐때
                        return Content("<script language='javascript' type='text/javascript'>alert('허용된 파일 형태가 아닙니다.');history.back(-1);</script>");
                    }
                    else if (sfilepath.Equals("s")) {//용량초과
                        return Content("<script language='javascript' type='text/javascript'>alert('파일 용량은 1MB 이하로 제한되어있습니다 .');history.back(-1);</script>");
                    }
                    else if (sfilepath.Equals("e")) {//업로드 에러
                        return Content("<script language='javascript' type='text/javascript'>alert('파일 업로드중 에러가 발생하였습니다.');history.back(-1);</script>");
                    }
                    else {
                        string surlfath = "/News10/event/" + DateTime.Now.ToString("yyyy") + "/"; //+ DateTime.Now.ToString("MM") + "/");
                        JCube.AF.Module.FTPConnectInfo ftpinfo = new JCube.AF.Module.FTPConnectInfo();
                        ftpinfo.Addr = "ftp.images.joins.com";
                        ftpinfo.BasePath = "/";
                        ftpinfo.Port = "8021";
                        ftpinfo.UID = "uximg";
                        ftpinfo.UPW = "ux0lalwl!@";
                        ftpinfo.UsePassive = true;
                        var fileName1 = Path.GetFileName(sfilepath);
                        sfilepath = sfilepath.Replace(fileName1, "");
                        //string imgurl = Joins.Apps.Common.Util.UploadFileToFtp(sfilepath);
                        bool rr = JCube.AF.Module.FTP.FTPUploadTo(ftpinfo, sfilepath, fileName1, surlfath, fileName1, false);
                        if (!rr) { bImgUpload = false; return Content("<script language='javascript' type='text/javascript'>alert('파일 업로드중 에러가 발생하였습니다.');history.back(-1);</script>"); }
                        else { imgurl1 = "http://images.joins.com" + surlfath + fileName1; }
                        FileInfo f = new System.IO.FileInfo(sfilepath + fileName1);
                        if (f.Exists) { f.Delete(); }
                        //   bool rr = false;

                    }
                }
                if (bImgUpload) {
                    if (!string.IsNullOrEmpty(imgurl)) { exc.img = imgurl; }
                    if (!string.IsNullOrEmpty(imgurl1)) { exc.bg_img = imgurl1; }
                    if (string.IsNullOrEmpty(exc.img)) { return Content("<script language='javascript' type='text/javascript'>alert('등록된 이미지 정보가 없습니다.');history.back(-1);</script>"); }

                    r = Joins.Apps.Repository.News10.EventRepository.SetEventItemEdit(exc);
                    if (r <= 0) { return Content("<script language='javascript' type='text/javascript'>alert('잠시후 다시 시도해 주시기 바랍니다.');history.back(-1);</script>"); }
                    else {
                        string m = exc.seq > 0 ? "수정" : "등록";
                        return Content("<script language='javascript' type='text/javascript'>alert('" + m + " 되었습니다.');location.href='/Event/Edit?sno=" + r + "&pgi=" + pgi + "';</script>");
                    }
                }
            }else {
                //입력정보가 올바르지 않습니다.
                return Content("<script language='javascript' type='text/javascript'>alert('입력정보가 올바르지 않습니다.');history.back(-1);</script>");
            }
            return Content("<script language='javascript' type='text/javascript'>alert('잠시후 다시 시도해 주시기 바랍니다.');history.back(-1);</script>");

        }
    }
}