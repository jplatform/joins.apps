﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.Cms.Core.Extentions
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString Nl2Br(this HtmlHelper htmlHelper, string text)
        {
            return MvcHtmlString.Create(Regex.Replace(text, "\n", "<br/>\n"));
        }
    }
}