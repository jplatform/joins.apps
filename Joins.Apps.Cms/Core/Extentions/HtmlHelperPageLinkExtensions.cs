﻿using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.Cms.Core.Extentions
{
    public static class HtmlHelperPageLinkExtensions
    {
        /// <summary>
        /// 링크정보를 생성합니다.
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="link">링크 URL</param>
        /// <param name="keys">수집할 파라메터</param>
        /// <returns></returns>
        public static IHtmlString GenerateLink(this HtmlHelper helper, string link, params string[] keys)
        {
            return GenerateLink(helper, link, null, keys);
        }

        /// <summary>
        /// 링크정보를 생성합니다.
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="link">링크 URL</param>
        /// <param name="addData">추가할 파라메터 정보</param>
        /// <param name="keys">수집할 파라메터</param>
        /// <returns></returns>
        public static IHtmlString GenerateLink(this HtmlHelper helper, string link, NameValueCollection addData, params string[] keys)
        {
            var request = helper.ViewContext.HttpContext.Request;
            var requestQuery = keys.Select(key => string.Format("{0}={1}", key, HttpUtility.UrlEncode(request[key])));
            var linkParameter = string.Empty;

            if (addData != null)
                linkParameter = addData.AllKeys.Select(key => string.Format("{0}={1}", key, HttpUtility.UrlEncode(addData[key]))).Concat(requestQuery).Aggregate((prev, next) => string.Concat(prev, "&", next));
            else
                linkParameter = requestQuery.Aggregate((prev, next) => string.Concat(prev, "&", next));

            return helper.Raw(string.Format("{0}?{1}", link, linkParameter));
        }
    }
}