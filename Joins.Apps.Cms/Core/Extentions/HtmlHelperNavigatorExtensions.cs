﻿using System;
using System.Text;
using System.Web.Mvc;

namespace Joins.Apps.Cms.Core.Extentions
{
    public static class HtmlHelperNavigatorExtensions
    {
        /// <summary>
        /// 페이지에 현재 위치를 표시합니다.
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="title">페이지 제목</param>
        /// <param name="description">설명</param>
        /// <param name="navigator">내비게이터</param>
        /// <returns></returns>
        public static MvcHtmlString Navigator(this HtmlHelper helper, string title, string description, NavigatorInfo[] navigator)
        {
            var sb = new StringBuilder();
            sb.AppendLine(@"<section class=""content-header"">");
            sb.AppendLine(string.Format(@"    <h1>{0}<small>{1}</small></h1>", title, description));
            sb.AppendLine(@"    <ol class=""breadcrumb"">");
            sb.AppendLine(@"        <li><a href = ""/Main/""><i class=""fa fa-home""></i> Home</a></li>");

            foreach (var item in navigator)
            {
                //var link = !string.IsNullOrEmpty(item.Path) ? string.Format(@"<a href = ""{0}"">{1}</a>", item.Path, item.Name) : string.Format("{0}", item.Name);
                var link = string.Format("{0}", item.Name);

                if (item.Active)
                    sb.AppendLine(string.Format(@"        <li class=""active"">{0}</li>", link));
                else
                    sb.AppendLine(string.Format(@"        <li>{0}</li>", link));
            }

            sb.AppendLine(@"    </ol>");
            sb.AppendLine(@"</section>");

            return MvcHtmlString.Create(Convert.ToString(sb));
        }

        public class NavigatorInfo
        {
            /// <summary>
            /// 이름
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// 현재 페이지 여부
            /// </summary>
            public bool Active { get; set; }
        }
    }
}