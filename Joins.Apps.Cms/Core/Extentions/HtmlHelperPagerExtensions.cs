﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.Cms.Core.Extentions
{
    public static class HtmlHelperPagerExtensions
    {
        /// <summary>
        /// 페이지 HTML을 생성합니다.
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="page">페이지 모델</param>
        /// <returns></returns>
        public static MvcHtmlString Pager<TModel>(this HtmlHelper helper, Joins.Apps.Models.Page<TModel> page)
        {
            return Pager(helper, page, null);
        }

        /// <summary>
        /// 페이지 HTML을 생성합니다.
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="page">페이지 모델</param>
        /// <param name="options">페이징 환경설정</param>
        /// <returns></returns>
        public static MvcHtmlString Pager<TModel>(this HtmlHelper helper, Joins.Apps.Models.Page<TModel> page, PagerOptions options)
        {
            var totalItemCount = page.TotalItemCount;
            var displayItemCount = page.DisplayItemCount;
            var pageNum = page.PageNum;
            var displayPageCount = page.DisplayPageCount;

            var html = new StringBuilder();
            var lastPage = Convert.ToInt32(Math.Ceiling(totalItemCount / (float)displayItemCount));
            var currentPage = Math.Max(1, Math.Min(lastPage, pageNum));
            var visibleFirstPage = Convert.ToInt32(Math.Floor((currentPage - 1) / (float)displayPageCount)) * displayPageCount + 1;
            var visibleLastPage = visibleFirstPage + displayPageCount - 1;

            visibleLastPage = Math.Max(Math.Min(visibleLastPage, lastPage), 1);

            html.AppendLine(@"<ul class=""pagination"">");

            if (currentPage <= displayPageCount)
            {
                html.AppendLine(@"    <li class=""paginate_button disabled""><a href=""javascript:void(0)"">처음</a></li>");
                html.AppendLine(@"    <li class=""paginate_button disabled""><a href=""javascript:void(0)"">이전</a></li>");
            }
            else
            {
                html.AppendLine(string.Format(@"    <li class=""paginate_button""><a href=""?{0}"" data-page=""1"">처음</a></li>", GetQuery(1, options)));
                html.AppendLine(string.Format(@"    <li class=""paginate_button""><a href=""?{0}"" data-page=""{1}"">이전</a></li>", GetQuery(visibleFirstPage - 1, options), visibleFirstPage - 1));
            }
            

            for (var i = visibleFirstPage; i <= visibleLastPage; i++)
                html.AppendLine(string.Format(@"    <li class=""paginate_button{0}""><a href=""?{1}"" data-page=""{2}"">{3}</a></li>", ((currentPage == i) ? " active" : ""), GetQuery(i, options), i, i));

            if (visibleLastPage >= lastPage)
            {
                html.AppendLine(@"    <li class=""paginate_button disabled""><a href=""javascript:void(0)"">다음</a></li>");
                html.AppendLine(@"    <li class=""paginate_button disabled""><a href=""javascript:void(0)"">끝</a></li>");
            }
            else
            {
                html.AppendLine(string.Format(@"    <li class=""paginate_button""><a href=""?{0}"" data-page=""{1}"">다음</a></li>", GetQuery(visibleLastPage + 1, options), visibleLastPage + 1));
                html.AppendLine(string.Format(@"    <li class=""paginate_button""><a href=""?{0}"" data-page=""{1}"">끝</a></li>", GetQuery(lastPage, options), lastPage));
            }
            
            html.AppendLine(@"</ul>");

            return MvcHtmlString.Create(Convert.ToString(html));
        }

        /// <summary>
        /// 사용자 파라메터와 조합하여 QueryString을 반환합니다.
        /// </summary>
        /// <param name="page">생성할 페이지 번호</param>
        /// <param name="options">환경설정</param>
        /// <returns>QueryString</returns>
        private static string GetQuery(int page, PagerOptions options)
        {
            var query = string.Empty;
            var hasOption = options != null;
            var pageParam = hasOption ? options.PageParameterName : "pgi";

            if (hasOption && options.Parameters != null)
                query = string.Join("&", options.Parameters.AllKeys.Where(x => !options.PageParameterName.Equals(x, StringComparison.OrdinalIgnoreCase)).Select(p => string.Concat(p, "=", HttpUtility.UrlEncode(options.Parameters[p]))).ToArray());

            return string.IsNullOrEmpty(query) ? string.Format("{0}={1}", pageParam, page) : string.Format("{0}&{1}={2}", query, pageParam, page);
        }

        /// <summary>
        /// 페이지 HTML을 생성하기 위한 환경설정 정보입니다.
        /// </summary>
        public class PagerOptions
        {
            /// <summary>
            /// 페이지 파라메터 이름을 가져오거나 설정합니다.
            /// </summary>
            public string PageParameterName { get; set; } = "pgi";
            /// <summary>
            /// 페이지 링크에 추가될 파라메터 목록 입니다.
            /// </summary>
            public NameValueCollection Parameters { get; set; }
        }
    }
}