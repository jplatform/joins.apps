﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.Cms.Core.ActionFilter {
    public class LoginCheckActionFilter : ActionFilterAttribute, IActionFilter {
        HttpRequestBase request;
        public override void OnActionExecuting(ActionExecutingContext context) {
            request = context.RequestContext.HttpContext.Request;
            if (IsRequiredLoginCheck(context)) {
                Joins.Apps.Cms.Models.Auth admember = Joins.Apps.Cms.Core.LoginAuth.LoadLoginInfo();
                 System.Uri uri = request.Url;
                if (admember == null || string.IsNullOrEmpty(admember.mid)) {
                    // System.Uri uri = request.Url;
                    if (uri != null) { context.HttpContext.Response.Redirect("/Auth/?returnUrl=" + HttpUtility.UrlEncode(request.Url.PathAndQuery)); }
                    else {context.HttpContext.Response.Redirect("/Auth/");}
                }else {
                    if (!uri.AbsolutePath.Equals("/")) {
                        if(!IsAuthGroupCheck(context, admember.group)) { context.HttpContext.Response.Redirect("/"); }
                        //if (admember.group == "sully") { if (uri.AbsolutePath.IndexOf("sully") < 0) { context.HttpContext.Response.Redirect("/"); } }
                        //else { if (uri.AbsolutePath.IndexOf("sully") >= 0) { context.HttpContext.Response.Redirect("/"); } }
                    }
                }
                //context.HttpContext.Response.Redirect("/?returnUrl=" + HttpUtility.UrlEncode(request.Url.PathAndQuery));
            }
            base.OnActionExecuting(context);
        }
        private bool IsRequiredLoginCheck(ActionExecutingContext context) {
            //var referrer = string.Empty;
            //if (request.UrlReferrer != null)
            //    referrer = request.UrlReferrer.AbsolutePath;

            var hasAction = GetAttributes(context.ActionDescriptor).Any(x => ((Joins.Apps.Cms.Core.Attribute.LoginCheckAttribute)x).Enable == false);
            var hasController = GetAttributes(context.ActionDescriptor.ControllerDescriptor).Any(x => ((Joins.Apps.Cms.Core.Attribute.LoginCheckAttribute)x).Enable == false);

            return !hasAction && !hasController;
        }

        private IEnumerable<object> GetAttributes(ControllerDescriptor desc) {
            var attr = typeof(Joins.Apps.Cms.Core.Attribute.LoginCheckAttribute);
            return desc.GetCustomAttributes(attr, false).ToList();
        }

        private IEnumerable<object> GetAttributes(ActionDescriptor desc) {
            var attr = typeof(Joins.Apps.Cms.Core.Attribute.LoginCheckAttribute);
            return desc.GetCustomAttributes(attr, false).ToList();
        }
        private bool IsAuthGroupCheck(ActionExecutingContext context,string mid) {
            var hasAction = GetAttributesGroup(context.ActionDescriptor).Any(x => JCube.AF.Util.Converts.ToString(((Joins.Apps.Cms.Core.Attribute.AuthGroupCheckAttribute)x).Group,"news10") == mid);
            var hasController = GetAttributesGroup(context.ActionDescriptor.ControllerDescriptor).Any(x => JCube.AF.Util.Converts.ToString(((Joins.Apps.Cms.Core.Attribute.AuthGroupCheckAttribute)x).Group, "news10") == mid);

            return hasAction || hasController;
        }

        private IEnumerable<object> GetAttributesGroup(ControllerDescriptor desc) {
            var attr = typeof(Joins.Apps.Cms.Core.Attribute.AuthGroupCheckAttribute);
            return desc.GetCustomAttributes(attr, false).ToList();
        }

        private IEnumerable<object> GetAttributesGroup(ActionDescriptor desc) {
            var attr = typeof(Joins.Apps.Cms.Core.Attribute.AuthGroupCheckAttribute);
            return desc.GetCustomAttributes(attr, false).ToList();
        }
    }
}