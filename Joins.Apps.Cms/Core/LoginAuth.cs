﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Joins.Apps.Cms.Core {
    public class LoginAuth {
        public static string GetLoginID() {
            Joins.Apps.Cms.Models.LoginInfo logininfo = GetLoginInfo();
            return logininfo.mID;
        }
        public static Joins.Apps.Cms.Models.LoginInfo GetLoginInfo() {
            Joins.Apps.Cms.Models.LoginInfo logininfo = new Models.LoginInfo();
            string AuthInfo = JCube.AF.Util.Converts.ToString(System.Web.HttpContext.Current.User.Identity.Name, "");
            string[] AuthInfoList = AuthInfo.Split("|".ToArray());
            if (AuthInfo != null) {
                if (AuthInfoList.Length > 1) {
                    logininfo.mID = AuthInfoList[0];
                    logininfo.mToken = AuthInfoList[1];
                }else { logininfo.mID = AuthInfoList[0]; }
            }
            return logininfo;
        }
        public static void GetLoginSave(string admid, Joins.Apps.Cms.Models.Auth adm) {
            if (adm == null) { System.Web.HttpContext.Current.Application["JoinsAppsCmsLogin_" + admid] = null; }
            else { System.Web.HttpContext.Current.Application["JoinsAppsCmsLogin_" + admid] = adm; }
        }
        public static Joins.Apps.Cms.Models.Auth LoadLoginInfo() {
            Joins.Apps.Cms.Models.LoginInfo logininfo = Joins.Apps.Cms.Core.LoginAuth.GetLoginInfo();
            Joins.Apps.Cms.Models.Auth ui = new Joins.Apps.Cms.Models.Auth();
            if (logininfo!=null && !string.IsNullOrEmpty(logininfo.mID)) {
                if (null != System.Web.HttpContext.Current.Application["JoinsAppsCmsLogin_" + logininfo.mID]) {
                    ui = (Joins.Apps.Cms.Models.Auth)System.Web.HttpContext.Current.Application["JoinsAppsCmsLogin_" + logininfo.mID]; }
                else {
                    if (!string.IsNullOrEmpty(logininfo.mToken)) {
                        Joins.Apps.Cms.Core.LoginAuth loginauth = new Joins.Apps.Cms.Core.LoginAuth();
                        Joins.Apps.Models.Sully.Jam.userinfo jamuserinfo = loginauth.GetJAMUserInfo(logininfo.mID, logininfo.mToken);
                        if (jamuserinfo != null && jamuserinfo.code == 200) {
                            ui.mid = logininfo.mID;
                            ui.mname = jamuserinfo.data.userName;
                            ui.group = "sully";
                        }
                    }else {
                        ui.mid = logininfo.mID;
                        ui.mname = "관리자";
                        ui.group = "news10";
                    }
                    if (ui != null && !string.IsNullOrEmpty(ui.mid)){Joins.Apps.Cms.Core.LoginAuth.GetLoginSave(ui.mid,ui);}
                }
            }
            return ui;
        }
        public Joins.Apps.Models.Sully.Jam.login GetJAMLogin(string userid,string userpw) {
            Joins.Apps.Models.Sully.Jam.login jamLogin = null;
            try {
                var webRequest = System.Net.WebRequest.Create(Joins.Apps.GLOBAL.JAMLoginAPIURL);
                if (webRequest != null) {
                    webRequest.Method = System.Net.WebRequestMethods.Http.Post;
                    webRequest.Timeout = 12000;
                    webRequest.ContentType = "application/json";
                    webRequest.Headers.Add("user-id", userid);
                    webRequest.Headers.Add("user-password", userpw);
                    webRequest.Headers.Add("user-device-key", "3");
                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream()) {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s)) {
                            string jsonResponse = sr.ReadToEnd();
                             jamLogin = JsonConvert.DeserializeObject<Joins.Apps.Models.Sully.Jam.login>(jsonResponse);
                            //  Console.WriteLine(String.Format("Response: {0}", jsonResponse));
                        }
                    }
                }
            } catch (Exception ex) {
             //   Console.WriteLine(ex.ToString());
            }
            return jamLogin;
        }
        public Joins.Apps.Models.Sully.Jam.userinfo GetJAMUserInfo(string userid, string usertoken) {
            Joins.Apps.Models.Sully.Jam.userinfo jamuserinfo = null;
            try {
                var webRequest = System.Net.WebRequest.Create(Joins.Apps.GLOBAL.JAMUserInfoAPIURL);
                if (webRequest != null) {
                    webRequest.Method = "GET";
                    webRequest.Timeout = 12000;
                    webRequest.ContentType = "application/json";
                    webRequest.Headers.Add("user-id", userid);
                    webRequest.Headers.Add("user-token", usertoken);
                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream()) {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s)) {
                            string jsonResponse = sr.ReadToEnd();
                            jamuserinfo = JsonConvert.DeserializeObject<Joins.Apps.Models.Sully.Jam.userinfo>(jsonResponse);
                            //  Console.WriteLine(String.Format("Response: {0}", jsonResponse));
                        }
                    }
                }
            } catch (Exception ex) {
                //   Console.WriteLine(ex.ToString());
            }
         
            return jamuserinfo;
        }
        ///app/auth/logout.json
        ///
        public bool GetJAMUserLogout(string userid) {
            Joins.Apps.Models.Sully.Jam.userinfo jamuserinfo = null;
            try {
                var webRequest = System.Net.WebRequest.Create(Joins.Apps.GLOBAL.JAMLogoutAPIURL);
                if (webRequest != null) {
                    webRequest.Method = "GET";
                    webRequest.Timeout = 12000;
                    webRequest.ContentType = "application/json";
                    webRequest.Headers.Add("user-id", userid);
                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream()) {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s)) {
                            string jsonResponse = sr.ReadToEnd();
                            jamuserinfo = JsonConvert.DeserializeObject<Joins.Apps.Models.Sully.Jam.userinfo>(jsonResponse);
                            //  Console.WriteLine(String.Format("Response: {0}", jsonResponse));
                        }
                    }
                }
            } catch (Exception ex) {
                //   Console.WriteLine(ex.ToString());
            }
            if (jamuserinfo == null) {return false;}
            return true;
        }
    }
}