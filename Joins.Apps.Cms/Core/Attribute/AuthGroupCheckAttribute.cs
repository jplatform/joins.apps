﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.Cms.Core.Attribute {
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    public class AuthGroupCheckAttribute : System.Attribute {
        /// <summary>
        /// 로그인여부 체크 활성화 유무를 가져오거나 설정합니다.
        /// </summary>
        public string Group { get; set; }
        /// <summary>
        /// 로그인여부 체크 활성화 여부를 지정합니다.
        /// </summary>
        /// <param name="enable">활성화 유무</param>
        public AuthGroupCheckAttribute(string Group = "news10") {
            this.Group = Group;
        }
    }
}