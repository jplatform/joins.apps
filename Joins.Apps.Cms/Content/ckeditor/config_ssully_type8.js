/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
CKEDITOR.editorConfig = function (config) {
    config.allowedContent = true;
    config.removeFormatAttributes = '';

    config.toolbarGroups =[
        //{ name: 'clipboard', groups: ['clipboard', 'undo'] },
        //{ name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
        //{ name: 'colors', groups: ['colors'] },
            { name: 'links', groups: ['links']
    },
        //{ name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing'] },
        //{ name: 'forms', groups: ['forms'] },
        //{ name: 'paragraph', groups: ['list', 'blocks', 'align', 'bidi', 'paragraph'] },
        //{ name: 'insert', groups: ['insert'] },
            { name: 'document', groups: ['custom', 'mode']},
        //'/',
        //{ name: 'styles', groups: ['styles'] },
        //'/',
        //{ name: 'tools', groups: ['tools'] },
        //{ name: 'others', groups: ['others'] },
        //{ name: 'about', groups: ['about'] }
];

    config.removeButtons = 'NewPage,About,Iframe,PageBreak,Flash,BidiRtl,BidiLtr,Language,CreateDiv,HiddenField,ImageButton,Button,Select,Textarea,TextField,Radio,Checkbox,Form,Scayt,SelectAll,Replace,Find,Print,Preview,Save,Anchor,Smiley,SpecialChar,CopyFormatting,RemoveFormat,Blockquote,Maximize,Image';
    config.contentsCss = ['https://static.joins.com/html/mobile/ssuly/html/css/renewal/style_corner.css', 'https://fonts.googleapis.com/css?family=Noto+Sans+KR:300,400,500,600', '/content/ckeditor/contents_ssully_type8.css'];
    config.bodyClass = 'presso-article';
    config.enterMode = CKEDITOR.ENTER_BR;
    config.fontNames = 'Noto Sans KR';
    config.startupOutlineBlocks = true;
    config.resize_enabled = true;
    config.extraPlugins = 'easykeymap,image2';
    config.filebrowserImageUploadUrl = '/sully/common/imageupload/ckeditor';
};