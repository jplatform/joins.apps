﻿CKEDITOR.plugins.add('ssully_page_break', {
    icons: 'ssully_page_break',
    init: function (editor) {
        editor.addCommand('ssully_page_break', new CKEDITOR.dialogCommand('ssully_page_break_dialog'));
        editor.ui.addButton('ssully_page_break', {
            label: 'Page Break',
            command: 'ssully_page_break',
            toolbar: 'ssully'
        });

        CKEDITOR.dialog.add('ssully_page_break_dialog', this.path + 'dialogs/ssully_page_break.js');
    }
});