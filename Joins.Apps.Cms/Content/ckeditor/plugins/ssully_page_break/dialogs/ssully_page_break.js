﻿CKEDITOR.dialog.add('ssully_page_break_dialog', function (editor) {
    return {
        title: '썰리 페이지 분리',
        minWidth: 400,
        minHeight: 200,
        contents: [
            {
                id: 'tab-basic',
                label: 'Basic Settings',
                elements: [
                    {
                        type: 'text',
                        id: 'name',
                        label: '페이지 버튼 명 (빈란으로 둘 경우 기본 값은 <span style="color: #f00;">그래서? 그래서?</span> 입니다)',
                        setup: function (element) {
                            this.setValue($.trim(element.getText()));
                        },

                        commit: function (element) {
                            element.setText($.trim(this.getValue()));
                            
                        }
                    }
                ]
            }
        ],

        onShow: function () {
            var selection = editor.getSelection();
            var element = selection.getStartElement();

            if (element)
                element = element.getAscendant('div', true);

            if (!element || element.getName() != 'div') {
                element = editor.document.createElement('div');
                element.setAttribute('ssully_page_break', '')
                    .setStyle('border-top', '1px dashed #f00')
                    .setStyle('border-bottom', '1px dashed #f00')
                    .setStyle('text-align', 'center')
                    .setStyle('margin', '30px 0')
                    .setStyle('color', '#f00');
                this.element = element;
                this.setupContent(this.element);

                this.insertMode = true;
            }
            else
                this.insertMode = false;

            this.element = element;
            if (!this.insertMode)
                this.setupContent(this.element);
        },


        onOk: function () {
            var dialog = this;
            var element = this.element;
            this.commitContent(element);

            if (this.insertMode)
                editor.insertElement(element);
        }
    };
});