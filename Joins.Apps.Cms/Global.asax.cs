﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Joins.Apps.Cms
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
       //     FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        //    BundleConfig.RegisterBundles(BundleTable.Bundles);
            RegisterGlobalFilter(GlobalFilters.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
        private static void RegisterGlobalFilter(GlobalFilterCollection filter) {
            filter.Add(new Joins.Apps.Cms.Core.ActionFilter.LoginCheckActionFilter());
        }
    }
}
