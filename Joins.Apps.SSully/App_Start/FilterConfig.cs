﻿using Joins.Apps.SSully.Attributes;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.SSully
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new RequireSSLAttribute());
            filters.Add(new LoginCheckAttribute());
        }
    }
}
