﻿using Joins.Apps.SSully.Helpers;
using System.Linq;
using System.Web.Mvc;

namespace Joins.Apps.SSully.Attributes
{
    public class LoginCheckAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var attribute = filterContext.ActionDescriptor.GetCustomAttributes(typeof(LoginAttribute), true).FirstOrDefault() as LoginAttribute;
            var isAjaxRequest = filterContext.RequestContext.HttpContext.Request.IsAjaxRequest();
            
            if (attribute != null)
            {
                // 로그인 필수 여부
                if (!LoginHelper.IsLogin && attribute.RequiredLogin)
                {
                    if (isAjaxRequest)
                    {
                        filterContext.Result = new JsonResult {
                            Data = new {
                                ssully_login_result = "1000"
                            },
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                            ContentType = "application/json"
                        };
                        return;
                    }

                    filterContext.Result = new ContentResult { Content = @"<html><head><script type=""text/javascript"">alert('로그인이 필요합니다.'); location.replace(""/User/Login"");</script></head><body></body></html>" };
                    return;
                }
            }

            if (LoginHelper.IsLogin)
            {
                // 로그인 데이터 변조 여부 확인
                if (!LoginHelper.IsValidateLoginData)
                {
                    if (isAjaxRequest)
                    {
                        filterContext.HttpContext.Response.AddHeader("ssully_login_ajax_result", "2000");
                        return;
                    }

                    LoginHelper.RemoveLoginCookie();
                    filterContext.Result = new RedirectResult("/");
                    return;
                }

                //워크쓰루 완료 여부
                var checkWalkthrough = attribute == null || (attribute != null && attribute.CheckWalkThroughCompleted);
                if (checkWalkthrough && string.IsNullOrWhiteSpace(LoginHelper.LoginCookie.Name))
                {
                    if (isAjaxRequest)
                    {
                        filterContext.HttpContext.Response.AddHeader("ssully_login_ajax_result", "2001");
                        return;
                    }

                    filterContext.Result = new RedirectResult("/User/Regist");
                    return;
                }
            }
        }
    }
}