﻿using System;
using System.Web.Mvc;

namespace Joins.Apps.SSully.Attributes
{
    public class RequireSSLAttribute : RequireHttpsAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
                throw new ArgumentNullException("filterContext");

            if (filterContext.HttpContext.Request.IsLocal || filterContext.HttpContext.Request.Url.Host.Contains("dev.ssully.joins.com"))
                return;

            if (!filterContext.HttpContext.Request.IsSecureConnection)
            {
                HandleNonHttpsRequest(filterContext);
                return;
            }
        }
    }
}