﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.SSully.Attributes
{
    public class LoginAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// 워크쓰루 완료 여부 체크
        /// </summary>
        public bool CheckWalkThroughCompleted { get; set; } = true;
        /// <summary>
        /// 로그인 필수여부
        /// </summary>
        public bool RequiredLogin { get; set; } = false;
    }
}