﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.SSully.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    public class PreventiveMaintenanceCheckAttribute : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var is_mode = "Y".Equals(ConfigurationManager.AppSettings["SSully_PreventiveMaintenanceMode"], StringComparison.OrdinalIgnoreCase);

            if (is_mode)
            {
                var start_date = Convert.ToDateTime(ConfigurationManager.AppSettings["SSully_PreventiveMaintenanceStartDate"]);
                var end_date = Convert.ToDateTime(ConfigurationManager.AppSettings["SSully_PreventiveMaintenanceEndDate"]);
                var now_date = DateTime.Now;

                if (now_date.CompareTo(start_date) >= 0 && now_date.CompareTo(end_date) <= 0)
                {
                    filterContext.Result = new ViewResult
                    {
                        ViewName = "~/views/error/preventivemaintenance.cshtml",
                        ViewData = new ViewDataDictionary
                        {
                            //점검 일자 : 2019.01.13(일) 02:00 - 04:00
                            Model = string.Format("{0:yyyy.MM.dd(ddd) HH:mm} ~ {1:HH:mm}", start_date, end_date)
                        }
                    };
                }
            }
        }
    }
}