﻿using Joins.Apps.Repository.Sully;
using Joins.Apps.SSully.Attributes;
using Joins.Apps.SSully.Helpers;
using Joins.Apps.SSully.Models.Login;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.SSully.Controllers
{
    [PreventiveMaintenanceCheck]
    [RoutePrefix("login")]
    public class LoginController : Controller
    {
        [NonAction]
        private ActionResult RedirectErrorPage(bool isPopup)
        {
            return RedirectPage(isPopup, "/error/sorry");
        }

        [NonAction]
        private ActionResult RedirectPage(bool isPopup, string returnUrl)
        {
            if (isPopup)
                return Content(string.Format("<html><head><script>opener.location.replace('{0}'); self.close();</script></head></html>", returnUrl));

            return Content(string.Format("<html><head><script>location.replace('{0}');</script></head></html>", returnUrl));
        }

        [NonAction]
        private ActionResult LoginUserDataResult(bool is_popup, LoginProfile sns_profile, string return_url)
        {
            var ip_address = Common.Util.GetClientIPAddress();
            var result = UserRepository.SetLoginUserInfo(sns_profile.Type, sns_profile.UID, sns_profile.Email, sns_profile.Name, sns_profile.ProfileImage, ip_address);

            if (result.IsSuccess)
            {
                LoginHelper.SetLoginCookie(UserRepository.GetUserInfo(sns_profile.Type, sns_profile.UID));
                return RedirectPage(is_popup, return_url);
            }

            return RedirectErrorPage(is_popup);
        }

        /// <summary>
        /// 네이버 로그인
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [Route("naver")]
        public ActionResult NaverLogin(string returnUrl)
        {
            var clientId = ConfigurationManager.AppSettings["Login_Naver_ClientID"];
            var callbackUrl = HttpUtility.UrlEncode(Request.Url.GetLeftPart(UriPartial.Authority) + ConfigurationManager.AppSettings["Login_Naver_Callback"]);
            var state = Convert.ToString(Guid.NewGuid());

            LoginHelper.SaveCustomData(new TempData { State = state, ReturnURL = returnUrl });

            return Redirect(string.Format("https://nid.naver.com/oauth2.0/authorize?response_type=code&client_id={0}&redirect_uri={1}&state={2}", clientId, callbackUrl, state));
        }

        /// <summary>
        /// 네이버 로그인 콜백
        /// </summary>
        /// <param name="code"></param>
        /// <param name="state"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [Route("callback/naver")]
        [HttpGet]
        public async Task<ActionResult> NaverLoginCallback(string code, string state, string returnUrl)
        {
            var clientId = ConfigurationManager.AppSettings["Login_Naver_ClientID"];
            var clientSecret = ConfigurationManager.AppSettings["Login_Naver_Secret"];
            var tokenUrl = string.Format("https://nid.naver.com/oauth2.0/token?grant_type=authorization_code&client_id={0}&client_secret={1}&code={2}&state={3}", clientId, clientSecret, code, state);
            var profileUrl = "https://openapi.naver.com/v1/nid/me";
            var tempData = LoginHelper.LoadCustomData();

            if (tempData == null)
                return RedirectErrorPage(true);

            LoginHelper.RemoveCustomData();

            if (!state.Equals(tempData.State, StringComparison.OrdinalIgnoreCase))
                return RedirectErrorPage(true);

            // 접근 토큰 획득
            var tokenResult = await LoginHelper.GetAsyncUrlResult<JObject>(tokenUrl, null, new NameValueCollection
            {
                { "X-Naver-Client-Id", clientId },
                { "X-Naver-Client-Secret", clientSecret }
            });

            // 사용자 정보 획득
            if (tokenResult != null && tokenResult["access_token"] != null)
            {
                var profileResult = await LoginHelper.GetAsyncUrlResult<JObject>(profileUrl, null, new NameValueCollection {
                    { "Authorization",  string.Format("Bearer {0}", tokenResult["access_token"].Value<string>()) }
                });

                if (profileResult != null && profileResult["response"] != null)
                {
                    var res = profileResult["response"];
                    var uid = res["id"]?.Value<string>();

                    if (string.IsNullOrWhiteSpace(uid))
                        return RedirectErrorPage(true);

                    var profile = new LoginProfile
                    {
                        Type = "N",
                        UID = uid,
                        Name = res["name"]?.Value<string>(),
                        Email = res["email"]?.Value<string>(),
                        ProfileImage = res["profile_image"]?.Value<string>()
                    };

                    return LoginUserDataResult(true, profile, tempData.ReturnURL);
                }
            }

            return RedirectErrorPage(true);
        }

        /// <summary>
        /// 카카오 로그인
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [Route("kakao")]
        public ActionResult KakaoLogin(string returnUrl)
        {
            var apiKey = ConfigurationManager.AppSettings["Login_Kakao_RestAPIKey"];
            var callbackUrl = HttpUtility.UrlEncode(Request.Url.GetLeftPart(UriPartial.Authority) + ConfigurationManager.AppSettings["Login_Kakao_Callback"]);
            
            LoginHelper.SaveCustomData(new TempData { ReturnURL = returnUrl });

            return Redirect(string.Format("https://kauth.kakao.com/oauth/authorize?client_id={0}&redirect_uri={1}&response_type=code", apiKey, callbackUrl));
        }

        /// <summary>
        /// 카카오 로그인 콜백
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [Route("callback/kakao")]
        [HttpGet]
        public async Task<ActionResult> KakaoLoginCallback(string code)
        {
            var tempData = LoginHelper.LoadCustomData();
            var apiKey = ConfigurationManager.AppSettings["Login_Kakao_RestAPIKey"];
            var callbackUrl = Request.Url.GetLeftPart(UriPartial.Authority) + ConfigurationManager.AppSettings["Login_Kakao_Callback"];

            LoginHelper.RemoveCustomData();

            if (tempData == null)
                return RedirectErrorPage(true);

            // 토큰 획득
            var tokenUrl = "https://kauth.kakao.com/oauth/token";
            var profileUrl = "https://kapi.kakao.com/v2/user/me?secure_resource=true";
            var tokenResult = await LoginHelper.PostAsyncUrlResult<JObject>(tokenUrl, null, new FormUrlEncodedContent(new[] {
                    new KeyValuePair<string, string>("grant_type", "authorization_code"),
                    new KeyValuePair<string, string>("client_id", apiKey),
                    new KeyValuePair<string, string>("redirect_uri", callbackUrl),
                    new KeyValuePair<string, string>("code", code)
                }));

            // 프로필 획득
            if (tokenResult != null && tokenResult["access_token"] != null)
            {
                var header = new NameValueCollection {
                    { "Authorization", string.Format("Bearer {0}", tokenResult["access_token"].Value<string>()) }
                };
                var profileResult = await LoginHelper.GetAsyncUrlResult<JObject>(profileUrl, null, header);

                if (profileResult != null)
                {
                    var uid = profileResult?.Value<string>("id");

                    if (string.IsNullOrWhiteSpace(uid))
                        return RedirectErrorPage(true);

                    var profile = new LoginProfile
                    {
                        Type = "K",
                        UID = uid,
                        Email = profileResult["kakao_account"]?.Value<string>("email"),
                        Name = profileResult["properties"]?.Value<string>("nickname"),
                        ProfileImage = profileResult["properties"]?.Value<string>("profile_image")
                    };

                    if (string.IsNullOrEmpty(profile.UID))
                        return RedirectErrorPage(true);

                    return LoginUserDataResult(true, profile, tempData.ReturnURL);
                }
            }

            return RedirectErrorPage(true);
        }

        /// <summary>
        /// 파이어베이스 로그인 인증 데이터 콜백
        /// </summary>
        /// <param name="isPopup"></param>
        /// <param name="returnUrl"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        [Route("callback/firebase")]
        public async Task<ActionResult> FirebaseLoginCallback(string isPopup, string returnUrl, LoginProfile profile)
        {
            if ("F".Equals(profile.Type, StringComparison.OrdinalIgnoreCase))
            {
                var json = await LoginHelper.GetAsyncUrlResult<JObject>("https://graph.facebook.com/v2.12/me?fields=email&access_token=" + profile.Token, "application/json");
                profile.Email = json?.Value<string>("email");
            }
            else if ("G".Equals(profile.Type, StringComparison.OrdinalIgnoreCase))
            {
                var json = await LoginHelper.GetAsyncUrlResult<JObject>("https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + profile.Token, "application/json");
                profile.Email = json?.Value<string>("email");
            }

            return LoginUserDataResult("Y".Equals(isPopup, StringComparison.OrdinalIgnoreCase), profile, returnUrl);
        }
    }
}