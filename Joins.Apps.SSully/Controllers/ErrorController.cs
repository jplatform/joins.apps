﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.SSully.Controllers
{
    [RoutePrefix("error")]
    public class ErrorController : Controller
    {
        [Route("sorry")]
        public ActionResult Sorry()
        {
            return View();
        }
        [Route("notfound")]
        public ActionResult NotFound()
        {
            return View();
        }
    }
}