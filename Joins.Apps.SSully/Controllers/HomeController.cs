﻿using Joins.Apps.Common.Sully;
using Joins.Apps.Models;
using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Sully;
using Joins.Apps.SSully.Attributes;
using Joins.Apps.SSully.Helpers;
using Joins.Apps.SSully.Models.Home;
using Joins.Apps.SSully.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Joins.Apps.SSully.Controllers
{
    [PreventiveMaintenanceCheck]
    [RoutePrefix("")]
    public class HomeController : Controller 
    {
        #region 형식별 리스트 검색
        
        [NonAction]
        private ProductList getPageData(SCProductListParam param)
        {
            var login_type = LoginHelper.LoginType;
            var uid = LoginHelper.UID;
            var totalCount = 0;
            var is_first_page = param.pgi == 1;
            var category = (!string.IsNullOrWhiteSpace(param.pcate)) ? CodeRepository.GetCodeInfo("C0001", param.pcate) : null;
            var corner = param.ptype.HasValue ? CodeRepository.GetCodeInfo("C0004", Convert.ToString(param.ptype)) : null;
            var header = param.ptype.HasValue && param.ptype.Value == 7 ? ProductRepository.GetCornerListHeader(param.ptype.Value) : null;

            if (!string.IsNullOrWhiteSpace(param.pcate) && category == null)
                return null;

            if (param.ptype.HasValue && corner == null)
                return null;

            var ip = Common.Util.GetClientIPAddress();
            var list = ProductRepository.GetServiceProductList(param, login_type, uid, ip, out totalCount);

            return new ProductList
            {
                Result = new Page<Product>
                {
                    PageNum = param.pgi,
                    Data = list,
                    TotalItemCount = totalCount,
                    DisplayItemCount = param.ps,
                    TotalPage = totalCount / param.ps + (totalCount % param.ps > 0 ? 1 : 0)
                },
                CornerListHeader = header,
                CategoryCode = category,
                CornerCode = corner,
                Parameters = param
            };
        }

        [NonAction]
        private MainList getMainPageData(SCProductListParam param, bool is_restore)
        {
            var total_count = 0;
            var total_page = 0;
            var layout = Repository.Sully.SSullyMainRepository.GetMainLayout();
            var result = new List<MainList.MainItem>();
            var firstPageProductSize = layout.Where(x => "product".Equals(x.LayoutType, StringComparison.OrdinalIgnoreCase)).Sum(x => x.ProductCount) ?? 0;

            if (param.pgi > 1 || is_restore)
            {
                var skip_size = is_restore ? 10 : (param.pgi - 1) * param.ps;
                var data = Joins.Apps.Repository.Sully.API.ProductRepository.GetProductListToMainV2(skip_size, param.ps, 1, false, null, null, out total_count);

                if (data != null)
                    result.Add(new MainList.MainItem { ItemType = "product", ProductData = data });
            }
            else
            {
                foreach (var x in layout)
                {
                    var item_type = x.LayoutType.ToLower();

                    if ("product".Equals(item_type))
                    {
                        // 일반형
                        var skip_count = layout.Where(r => "product".Equals(r.LayoutType) && r.OrderNumber < x.OrderNumber).Sum(r => r.ProductCount) ?? 0;
                        var data = Joins.Apps.Repository.Sully.API.ProductRepository.GetProductListToMainV2(skip_count, x.ProductCount.Value, 1, false, null, null, out total_count);

                        result.Add(new MainList.MainItem { ItemType = item_type, ProductData = data });
                    }
                    else if ("product_story".Equals(item_type))
                    {
                        // 스토리썰리
                        var data = Joins.Apps.Repository.Sully.API.ProductRepository.GetProductListToMainV2(0, 5, 5, false, null, null);

                        result.Add(new MainList.MainItem { ItemType = item_type, ProductData = data });
                    }
                    else if ("product_pedia".Equals(item_type))
                    {
                        // 썰리피디아
                        var data = Joins.Apps.Repository.Sully.API.ProductRepository.GetProductListToMainV2(0, 3, 2, false, null, null);

                        result.Add(new MainList.MainItem { ItemType = item_type, ProductData = data });
                    }
                    else if ("product_tour".Equals(item_type))
                    {
                        // 여기가봤썰
                        var product = Joins.Apps.Repository.Sully.API.ProductRepository.GetProductListToMainV2(0, 1, 6, false, null, null).FirstOrDefault();

                        if (product != null)
                        {
                            var content = Joins.Apps.Repository.Sully.ProductRepository.GetServiceProductContentInfo(product.Seq, false, null, null);

                            if (content == null || string.IsNullOrWhiteSpace(content.PDC_ORG_CONTENT))
                                return null;

                            var doc = new HtmlAgilityPack.HtmlDocument();
                            doc.LoadHtml(content.PDC_ORG_CONTENT);
                            var nodes = doc.DocumentNode.SelectNodes("//div[contains(@ssully_type, 'image_type4')]/div[contains(@class, 'image')]/img");
                            var data = MainList.ProductMainTour.CopyTo(product);
                            data.ProductTourImages = nodes.Select(r => r.GetAttributeValue("src", string.Empty)).Where(r => !string.IsNullOrWhiteSpace(r));
                            result.Add(new MainList.MainItem { ItemType = item_type, ProductTourData = data });
                        }
                    }
                    else if ("product_presso".Equals(item_type))
                    {
                        // 썰리프레소
                        var data = Joins.Apps.Repository.Sully.API.ProductRepository.GetProductListToMainV2(0, 1, 8, false, null, null);

                        result.Add(new MainList.MainItem { ItemType = item_type, ProductData = data });
                    }
                    else if ("weekly_rank".Equals(item_type))
                    {
                        // 주간 랭킹
                        var login_type = LoginHelper.LoginType;
                        var uid = LoginHelper.UID;
                        var data = SSullyMainRepository.GetWeeklyRank();
                        var member = UserRepository.GetUserInfo(login_type, uid);

                        result.Add(new MainList.MainItem
                        {
                            ItemType = item_type,
                            WeeklyRank = data.GroupBy(rank => new { rank.AgeGroup }, (key, items) => new MainList.Ranking
                            {
                                AgeGroup = key.AgeGroup,
                                Selected = member != null ? Convert.ToString(key.AgeGroup).Equals(member.Age) : key.AgeGroup == 0,
                                Rank = items
                            })
                        });
                    }
                    else if ("banner".Equals(item_type))
                    {
                        // 배너
                        var product = Repository.Sully.ProductRepository.GetProductInfo(x.PdSeq.Value);

                        if (product != null)
                        {
                            var data = new MainList.Banner
                            {
                                ImageUrl = x.ImageUrl,
                                LinkSeq = x.PdSeq.Value,
                                LinkType = "product",
                                Color = x.BackgroundColor,
                                CornerType = product.PD_TYPE,
                                IsADProduct = product.IS_AD_PRODUCT
                            };

                            result.Add(new MainList.MainItem { ItemType = item_type, Banner = data });
                        }
                    }
                }

                // 마지막은 첫페이지의 일반형 프로덕트의 페이지 갯수만큼 채우기
                var remain_count = param.ps - firstPageProductSize;
                if (remain_count > 0)
                {
                    var data = Joins.Apps.Repository.Sully.API.ProductRepository.GetProductListToMainV2(10 - remain_count, remain_count, 1, false, null, null, out total_count);

                    result.Add(new MainList.MainItem { ItemType = "product", ProductData = data });
                }
            }

            // total_page (첫페이지의 일반형 프로덕트를 제외한 나머지 전체를 페이징후 첫페이지 추가)
            var except_first_page_total_count = total_count - firstPageProductSize;
            total_page = (except_first_page_total_count / param.ps) + (except_first_page_total_count % param.ps > 0 ? 1 : 0) + 1;

            return new MainList
            {
                List = result,
                Page = param.pgi,
                PageSize = param.ps,
                Showcase = Repository.Sully.SSullyMainRepository.GetServiceShowcase(false, null, null),
                TotalCount = total_count,
                TotalPage = total_page,
                Parameters = param
            };
        }

        #endregion

        /// <summary>
        /// 기사목록
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("")]
        public ActionResult Index(SCProductListParam param)
        {
            // 카테고리 목록 페이지
            if (!string.IsNullOrEmpty(param.pcate))
            {
                var model = getPageData(param);

                if (model != null)
                    return View("categorylist", model);

                return Redirect("/");
            }

            // 코너 목록 페이지
            if (param.ptype.HasValue)
            {
                var model = getPageData(param);

                if (model != null)
                    return View("cornerlist", model);

                return Redirect("/");
            }

            // 메인 페이지
            return View("mainlist", getMainPageData(param, false));
        }

        [Route("lnb")]
        public ActionResult GetLeftNavigationBar()
        {
            return PartialView("~/views/home/partial/lnb.cshtml", new Lnb
            {
                IsLogin = LoginHelper.IsLogin,
                LoginInfo = LoginHelper.LoginCookie
            });
        }

        [HttpGet]
        [Route("search")]
        public ActionResult Search()
        {
            return View();
        }
        [HttpPost]
        [Route("search")]
        public ActionResult SearchAjax(int page, int page_size, string search)
        {
            var search_xml = string.IsNullOrEmpty(search) ? null : string.Format("<ROOT>{0}</ROOT>", search.Trim().Split(' ').Where(x => !string.IsNullOrEmpty(x)).Select(x => string.Format("<WORD>{0}</WORD>", x)).Aggregate((prev, next) => prev + next));
            var total_count = 0;
            var ip = Common.Util.GetClientIPAddress();
            var data = ProductRepository.GetServiceProductList(new SCProductListParam {
                pgi = page,
                ps = page_size,
                search_word = search_xml
            }, null, null, ip, out total_count);

            return Json(new
            {
                total_count = total_count,
                list = data?.Select(x => new
                {
                    seq = x.PD_SEQ,
                    title = x.PD_TITLE,
                    service_datetime = string.Format("{0:yyyy.MM.dd}", Convert.ToDateTime(x.PD_SERVICE_DT)),
                    thumbnail_image_url = x.PD_IMG_URL,
                    label_image_url = x.PD_PROVIDER_URL
                })
            }, JsonRequestBehavior.AllowGet);
            //return PartialView("~/views/home/partial/searchlist.cshtml", data);
        }


        /// <summary>
        /// 기사목록 다음페이지
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("more")]
        public ActionResult More(SCProductListParam param, string is_restore = "N")
        {
            // 카테고리 리스트
            if (!string.IsNullOrWhiteSpace(param.pcate))
            {
                var page = param.pgi;
                var page_size = param.ps;

                if ("Y".Equals(is_restore, StringComparison.OrdinalIgnoreCase))
                {
                    param.pgi = 1;
                    param.ps = page * page_size;
                }

                var data = getPageData(param);

                param.ps = page_size;
                param.pgi = page;

                return PartialView("~/views/home/partial/categorylist_more.cshtml", data);
            }

            // 코너 리스트
            if (param.ptype.HasValue)
            {
                return PartialView("~/views/home/partial/cornerlist_more.cshtml", getPageData(param));
            }

            // 메인 리스트
            return PartialView("~/views/home/partial/mainitem/product_more.cshtml", getMainPageData(param, false));
        }

        /// <summary>
        /// 기사목록 다음페이지
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [Route("restore")]
        public ActionResult Restore(SCProductListParam param)
        {
            // 카테고리 리스트
            if (!string.IsNullOrEmpty(param.pcate))
                return PartialView("~/views/home/partial/categorylist_more.cshtml", getPageData(param));

            // 코너 리스트
            if (param.ptype.HasValue)
                return PartialView("~/views/home/partial/cornerlist_more.cshtml", getPageData(param));

            // 메인 리스트
            return PartialView("~/views/home/partial/mainitem/product_more.cshtml", getMainPageData(param, true));
        }

        /// <summary>
        /// 썰리소개 페이지
        /// </summary>
        /// <returns></returns>
        [Route("about")]
        public ActionResult About()
        {
            return View();
        }

        /// <summary>
        /// 마켓으로 이동
        /// </summary>
        /// <returns></returns>
        [Route("store")]
        public ActionResult Store(string referrer)
        {
            var agent = Request.UserAgent.ToLower();

            if (agent.Contains("ios") || agent.Contains("iphone") || agent.Contains("ipad"))
                return Redirect("itms-apps://itunes.apple.com/us/app/apple-store/id1335446055?mt=8");
            else if (agent.Contains("android")) {
                if (string.IsNullOrWhiteSpace(referrer))
                    return Redirect("market://details?id=com.joins.joongang.ssully&referrer=utm_source%3D1boon%26utm_medium%3Dbanner%26utm_campaign%3Dandroid_download");

                return Redirect(string.Format("market://details?id=com.joins.joongang.ssully&referrer={0}", Server.UrlEncode(referrer)));
            }
            
            return Redirect("/about" + Request.Url.Query);
        }

        /// <summary>
        /// 미리보기 전용 URL 뷰
        /// </summary>
        /// <param name="enc_key"></param>
        /// <returns></returns>
        [Route("_/{enc_key}")]
        public ActionResult Detail(string enc_key)
        {
            var seq = ContentOptimizer.GetPreviewProductSeq(enc_key);

            if (!seq.HasValue)
                return RedirectToActionPermanent("");

            var product = ProductRepository.GetProductInfo(seq.Value);
            
            // 출고된 프로덕트인 경우 공식 URL로 이동 (/view/{seq})
            if ("L".Equals(product.PD_SERVICE_STATUS, StringComparison.OrdinalIgnoreCase) && Convert.ToDateTime(product.PD_SERVICE_DT) <= DateTime.Now)
                return RedirectToActionPermanent(Convert.ToString(product.PD_SEQ), "view");

            return View("/views/view/view.cshtml", new View
            {
                Content = ProductRepository.GetCMSProductContentInfo(seq.Value, null),
                Product = product,
                RecommendProduct = ProductRepository.GetServiceProductRelateList(seq.Value),
                IsShareLink = "Y".Equals(Request["share"], StringComparison.OrdinalIgnoreCase),
                IsNews10 = false,
                IsStatusRestore = false,
                ShareImage = string.IsNullOrEmpty(product.PD_SHARE_IMG_URL) ? product.PD_IMG_URL : product.PD_SHARE_IMG_URL,
                IsPreview = true
            });
        }
    }
}