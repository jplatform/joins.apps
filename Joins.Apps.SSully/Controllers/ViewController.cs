﻿using Joins.Apps.Common.Sully;
using Joins.Apps.Common.Utilities;
using Joins.Apps.Repository.Sully;
using Joins.Apps.SSully.Attributes;
using Joins.Apps.SSully.Helpers;
using Joins.Apps.SSully.Models.View;
using Joins.Apps.SSully.Models.View.Parameters;
using NReco.ImageGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.SSully.Controllers
{
    [PreventiveMaintenanceCheck]
    [RoutePrefix("View")]
    public class ViewController : Controller
    {
        /// <summary>
        /// 상세보기 데이터를 검색하고 결과를 반환합니다.
        /// </summary>
        /// <param name="seq">글 번호</param>
        /// <param name="isPageStatusRestore">페이지 위치 복원기능 사용여부</param>
        /// <param name="isNews10">뉴스10 서비스용 여부</param>
        /// <returns></returns>
        [NonAction]
        public ActionResult getDetailDataResult(int seq, bool isPageStatusRestore, bool isNews10)
        {
            var login_type = LoginHelper.LoginType;
            var uid = LoginHelper.UID;
            var product = ProductRepository.GetProductInfo(seq, login_type, uid);

            if (product == null || product.PD_SEQ == 0)
                return Redirect("/");

            var is_public_date = DateTime.Now.CompareTo(Convert.ToDateTime(product.PD_SERVICE_DT)) >= 0;
            var is_public = "L".Equals(product.PD_SERVICE_STATUS, StringComparison.OrdinalIgnoreCase);
            if (!is_public || !is_public_date)
                return Redirect("/");

            // 광고프로덕트지만 파라메터가 없는 경우
            if ("Y".Equals(product.IS_AD_PRODUCT, StringComparison.OrdinalIgnoreCase) && !"Y".Equals(Request.QueryString["ad"], StringComparison.OrdinalIgnoreCase))
            {
                var param = Request.QueryString.ToString();
                param = string.IsNullOrEmpty(param) ? string.Format("ad=y", param) : string.Format("{0}&ad=y", param);

                return Redirect(string.Format("{0}?{1}", Request.Url.AbsolutePath, param));
            }

            var result = new View
            {
                Content = ProductRepository.GetServiceProductContentInfo(seq, false, null, null),
                EventQuizProduct = ProductRepository.GetEventQuizProductData(),
                Product = product,
                CommentCount = Joins.Apps.Repository.Sully.API.CommentRepository.GetProductCommentCount(seq),
                RecommendProduct = isNews10 ? null : ProductRepository.GetServiceProductRelateList(seq),
                IsShareLink = "Y".Equals(Request["share"], StringComparison.OrdinalIgnoreCase),
                IsNews10 = isNews10,
                IsStatusRestore = isPageStatusRestore,
                ShareImage = string.IsNullOrEmpty(product.PD_SHARE_IMG_URL) ? product.PD_IMG_URL : product.PD_SHARE_IMG_URL,
                IsPreview = false
            };

            if (!string.IsNullOrEmpty(Request["guid"]) && product.PD_TYPE == 7)
                result.ShareImage = string.Format("{0}/view/quiz/result/image/{1}/{2}", Request.Url.GetLeftPart(UriPartial.Scheme | UriPartial.Authority), seq, Request["guid"]);

            if (result.Product.PD_SEQ > 0)
            {
                try
                {
                    var host = string.Empty;
                    var url = string.Empty;
                    var ip = Common.Util.GetClientIPAddress();
                    var agent = Request.UserAgent;

                    if (Request.UrlReferrer != null)
                    {
                        host = Request.UrlReferrer.GetLeftPart(UriPartial.Authority);
                        url = Request.UrlReferrer.GetLeftPart(UriPartial.Query);
                    }

                    LogRepository.SetProductViewLog(seq, ip, agent, host, url, login_type, uid);
                }
                catch { }

                return View("View", result);
            }

            return View("NotFound", result.IsNews10);
        }

        /// <summary>
        /// 퀴즈 결과 공유이미지 생성
        /// </summary>
        /// <param name="seq"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        [Route("quiz/result/image/{seq:int}/{guid}")]
        public ActionResult QuizShareResult(int seq, string guid)
        {
            var data = ProductRepository.GetQuestionShareData(seq, guid);
            var product = ProductRepository.GetProductInfo(seq);

            if (data == null)
            {
                using (var client = new HttpClient())
                    return File(client.GetByteArrayAsync("https://images.joins.com/ssully/common/share_thum.png").GetAwaiter().GetResult(), "image/png");
            }

            var html = System.IO.File.ReadAllText(Server.MapPath("/Template/ProductQuizShareCard.html"));
            html = html.Replace("#{Grade}", data.Grade).Replace("#{TotalCount}", Convert.ToString(data.TotalCount)).Replace("#{CorrectCount}", Convert.ToString(data.CorrectCount));

            var converter = new HtmlToImageConverter { Width = 1200, Height = 637 };
            return File(converter.GenerateImage(html, ImageFormat.Png), "image/png");
        }

        /// <summary>
        /// 상세보기 (페이지 위치 복원)
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        [Route("{seq}")]
        public ActionResult Detail(int seq)
        {
            // 구글 애널리틱스 로고 대소문자 가리므로 PascalCase로 통일하기 위함
            if (!Request.Url.AbsolutePath.StartsWith("/View"))
            {
                if (Request.QueryString.Count == 0)
                    return RedirectPermanent(string.Format("/View/{0}", seq));
                else
                    return RedirectPermanent(string.Format("/View/{0}?{1}", seq, Request.QueryString));
            }

            return getDetailDataResult(seq, true, false);
        }

        /// <summary>
        /// 상세보기 (페이지 위치 복원 기능 없음)
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        [Route("First/{seq}")]
        public ActionResult DetailNoStateRestore(int seq)
        {
            // 구글 애널리틱스 로고 대소문자 가리므로 PascalCase로 통일하기 위함
            if (!Request.Url.AbsolutePath.StartsWith("/View/First"))
            {
                if (Request.QueryString.Count == 0)
                    return RedirectPermanent(string.Format("/View/First/{0}", seq));
                else
                    return RedirectPermanent(string.Format("/View/First/{0}?{1}", seq, Request.QueryString));
            }

            return getDetailDataResult(seq, false, false);
        }

        /// <summary>
        /// 상세보기 (뉴스10앱내 서비스용)
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        [Route("Apps/{seq}")]
        public ActionResult DetailNews10Apps(int seq)
        {
            // 구글 애널리틱스 로고 대소문자 가리므로 PascalCase로 통일하기 위함
            if (!Request.Url.AbsolutePath.StartsWith("/View/Apps"))
            {
                if (Request.QueryString.Count == 0)
                    return RedirectPermanent(string.Format("/View/Apps/{0}", seq));
                else
                    return RedirectPermanent(string.Format("/View/Apps/{0}?{1}", seq, Request.QueryString));
            }

            return getDetailDataResult(seq, true, true);
        }

        /// <summary>
        /// 썰리퀴즈 문항정보를 가져옵니다.
        /// </summary>
        /// <param name="seq">프로덕트 번호</param>
        /// <param name="page">페이지(문항) 번호</param>
        /// <returns></returns>
        [Route("quiz/page/{seq:int}")]
        public ActionResult GetQuizInfo(int seq, int page)
        {
            var data = ProductRepository.GetProductQuizInfo(seq, page);
            return Content(Newtonsoft.Json.JsonConvert.SerializeObject(data), "application/json");
        }

        /// <summary>
        /// 퀴즈 문항에 대한 풀이 결과를 가져옵니다.
        /// </summary>
        /// <param name="seq">프로덕트 번호</param>
        /// <param name="question_seq">문항 고유번호</param>
        /// <param name="item_seq">사용자가 선택한 보기 고유번호</param>
        /// <returns></returns>
        [Route("quiz/answer/{seq:int}")]
        public ActionResult GetQuizAnswerInfo(int seq, int question_seq, int item_seq)
        {
            var data = ProductRepository.GetProductQuizResult(seq, question_seq, item_seq);
            return Content(Newtonsoft.Json.JsonConvert.SerializeObject(data), "application/json");
        }

        /// <summary>
        /// 퀴즈 프로덕트에 대한 응답 정보를 등록합니다.
        /// </summary>
        /// <param name="seq">프로덕트 번호</param>
        /// <param name="data">문항마다 선택한 보기 번호에 대한 데이터</param>
        /// <returns></returns>
        [Route("quiz/answer/apply/{seq:int}")]
        public ActionResult SetQuizApply(int seq, IEnumerable<QuizAnswerApply> data)
        {
            var login_type = LoginHelper.LoginType;
            var uid = LoginHelper.UID;
            var ip = Common.Util.GetClientIPAddress();
            var resultMessage = string.Empty;
            var xml = string.Format("<ROOT>{0}</ROOT>", string.Join(string.Empty, data.Select(x => string.Format(@"<QUESTION SEQ=""{0}"" ITEM_SEQ=""{1}""/>", x.QuestionSeq, x.ItemSeq))));
            var result = ProductRepository.SetProductDetailQuestionApply(seq, login_type, uid, xml, ip, out resultMessage);

            if (!string.IsNullOrEmpty(resultMessage))
            {
                return Json(new
                {
                    result = "N",
                    message = resultMessage
                });
            }

            return Json(new
            {
                result = "Y",
                message = string.Empty,
                correct_ratio = result.CorrectRatio,
                grade = result.Grade,
                correct_count = result.TotalCorrectCount,
                total_count = result.TotalCount,
                result_share_url = result.ResultShareUrl,
                answer_guid = result.AnswerGuid
            });
        }

        /// <summary>
        /// 썰리퀴즈 경품 이벤트에 대한 사용자정보를 응모합니다.
        /// </summary>
        /// <param name="data">파라메터 데이터</param>
        /// <returns></returns>
        [HttpPost]
        [Route("quiz/event/apply")]
        public ActionResult SetQuizEventApply(QuizEventApply data)
        {
            var allowPosition = new[] { "W", "A", "I" };
            var login_type = string.Empty;
            var uid = string.Empty;

            if ("W".Equals(data.ApplyPosition, StringComparison.OrdinalIgnoreCase))
            {
                login_type = LoginHelper.LoginType;
                uid = LoginHelper.UID;
            }
            else
            {
                login_type = Common.Sully.APIHeaderUtil.MemberLoginType;
                uid = Common.Sully.APIHeaderUtil.MemberAuthUniqueID;
            }

            var is_app = "A".Equals(data.ApplyPosition, StringComparison.OrdinalIgnoreCase) || "I".Equals(data.ApplyPosition, StringComparison.OrdinalIgnoreCase);

            if (is_app && string.IsNullOrWhiteSpace(login_type) && string.IsNullOrWhiteSpace(uid))
                return Json(new { result = "N", msg = "잘못된 접근입니다." });
            if (string.IsNullOrWhiteSpace(data.AnswerGUID) || string.IsNullOrWhiteSpace(data.ApplyPosition) || !allowPosition.Contains(data.ApplyPosition))
                return Json(new { result = "N", msg = "잘못된 접근입니다." });
            if (string.IsNullOrWhiteSpace(data.Email))
                return Json(new { result = "N", msg = "이메일을 입력해 주시기 바랍니다." });
            if (string.IsNullOrWhiteSpace(data.Phone))
                return Json(new { result = "N", msg = "연락처를 입력해 주시기 바랍니다." });
            if (string.IsNullOrWhiteSpace(data.UserName))
                return Json(new { result = "N", msg = "이름을 입력해 주시기 바랍니다." });
            if (!"Y".Equals(data.RequireAgreeYN, StringComparison.OrdinalIgnoreCase))
                return Json(new { result = "N", msg = "필수동의사항에 동의해 주셔야 응모가 가능합니다." });

            // 응모 처리
            var require_agree_checked = "Y".Equals(data.RequireAgreeYN, StringComparison.OrdinalIgnoreCase);
            var optional_agree_checked = "Y".Equals(data.RequireOptionalYN, StringComparison.OrdinalIgnoreCase);
            var msg = string.Empty;
            var result = ProductRepository.SetProductQuizEventApply(data.Seq, login_type, uid, data.AnswerGUID, data.UserName, data.Phone, data.Email, require_agree_checked, optional_agree_checked, data.ApplyPosition, out msg);

            return Json(new { result = result ? "Y" : "N", msg = msg });
        }

        /// <summary>
        /// 모바일앱용 퀴즈 이벤트 응모페이지를 연결합니다.
        /// </summary>
        /// <param name="seq"></param>
        /// <param name="guid"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("quiz/event/apply_form/{seq}")]
        public ActionResult GetQuizEventFormApp(int seq, string guid)
        {
            var login_type = Common.Sully.APIHeaderUtil.MemberLoginType;
            var uid = Common.Sully.APIHeaderUtil.MemberAuthUniqueID;
            var position = Common.Sully.APIHeaderUtil.DeviceType;

            if (string.IsNullOrWhiteSpace(login_type) || string.IsNullOrWhiteSpace(uid) || string.IsNullOrWhiteSpace(position))
                return Redirect(string.Format("ssully://?action=ssully_quiz_event_apply&process=N&msg={0}", Server.UrlEncode("잘못된 접근입니다.")));

            return View(new QuizApplyFormApp
            {
                Product = ProductRepository.GetProductInfo(seq),
                Guid = guid,
                LoginType = Common.Sully.APIHeaderUtil.MemberLoginType,
                PD_SEQ = seq,
                Position = position,
                UID = Common.Sully.APIHeaderUtil.MemberAuthUniqueID
            });
        }

        /// <summary>
        /// 댓글 페이지를 불러오거나 데이터를 검색합니다.
        /// </summary>
        /// <param name="seq">프로덕트 고유번호</param>
        /// <param name="comment_seq">한번에 불러올 코멘트 고유번호</param>
        /// <param name="page">페이지 번호</param>
        /// <param name="page_size">페이지당 리스트 수</param>
        /// <param name="order_by">정렬 방식 (new / like)</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{seq:int}/Comment")]
        public ActionResult CommentList(int seq, int? comment_seq, int? page, int page_size = 10, string order_by = "new")
        {
            var login_type = LoginHelper.LoginType;
            var uid = LoginHelper.UID;
            var total_count = 0;
            var total_page = 0;
            var current_page = 0;

            var result = Joins.Apps.Repository.Sully.Web.CommentRepository.GetCommentList(page, page_size, seq, comment_seq, login_type, uid, order_by, out total_count, out total_page, out current_page);

            if (Request.IsAjaxRequest())
            {
                return Json(new
                {
                    list = result != null ? result.Select(x => new
                    {
                        seq = x.PDSeq,
                        comment_seq = x.CommentSeq,
                        content = Server.HtmlEncode(x.Content).Replace("\n", "</br>"),
                        comment_status = x.CommentStatus,
                        like_count = x.LikeCount,
                        like_yn = x.LikeYN,
                        member_nick_name = x.MemberNickName,
                        member_profile_image_url = x.MemberProfileImageUrl,
                        write_datetime = DateTimeDisplayConverter.ConvertDateDuration(x.WriteDateTime),
                        my = x.MyComment
                    }) : null,
                    total_count = total_count,
                    total_page = total_page,
                    current_page = current_page
                }, JsonRequestBehavior.AllowGet);
            }

            return View(new CommentList
            {
                PD_SEQ = seq,
                COMMENT_SEQ = comment_seq
            });
        }

        /// <summary>
        /// 댓글을 입력합니다.
        /// </summary>
        /// <param name="seq"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        [Login(RequiredLogin = true)]
        [ValidateInput(false)]
        [Route("{seq:int}/Api/Comment/Insert")]
        public ActionResult CommentInsert(int seq, string content)
        {
            var login_type = LoginHelper.LoginType;
            var uid = LoginHelper.UID;
            var ip = Common.Util.GetClientIPAddress();

            var result = Joins.Apps.Repository.Sully.Web.CommentRepository.SetCommentInsert(login_type, uid, seq, content, ip);

            return Json(new
            {
                result = result.IsSuccess ? "Y" : "N",
                msg = result.IsSuccess ? string.Empty : result.ResultMessage
            });
        }

        /// <summary>
        /// 댓글을 삭제합니다.
        /// </summary>
        /// <param name="seq"></param>
        /// <param name="comment_seq"></param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        [Login(RequiredLogin = true)]
        [Route("{seq:int}/Api/Comment/Delete/{comment_seq:int}")]
        public ActionResult CommentDelete(int seq, int comment_seq)
        {
            var login_type = LoginHelper.LoginType;
            var uid = LoginHelper.UID;

            var result = Joins.Apps.Repository.Sully.Web.CommentRepository.SetCommentRemove(login_type, uid, seq, comment_seq);
            return Json(new
            {
                result = result.IsSuccess ? "Y" : "N",
                msg = result.IsSuccess ? string.Empty : result.ResultMessage
            });
        }

        /// <summary>
        /// 좋아요 버튼 토글
        /// </summary>
        /// <param name="seq"></param>
        /// <param name="comment_seq"></param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        [Login(RequiredLogin = true)]
        [Route("{seq:int}/Api/Comment/Like/{comment_seq:int}")]
        public ActionResult CommentLike(int seq, int comment_seq, string like_yn)
        {
            var login_type = LoginHelper.LoginType;
            var uid = LoginHelper.UID;
            var ip = Common.Util.GetClientIPAddress();
            var total_like_cnt = 0;
            var result = Joins.Apps.Repository.Sully.Web.CommentRepository.SetCommentLikeToggle(login_type, uid, comment_seq, like_yn, ip, out total_like_cnt);

            return Json(new
            {
                result = result.IsSuccess ? "Y" : "N",
                msg = result.IsSuccess ? string.Empty : result.ResultMessage,
                like_cnt = total_like_cnt
            });
        }

        [Login(RequiredLogin = true)]
        [Route("{seq:int}/Comment/Report/{comment_seq:int}")]
        public ActionResult CommentReport(int seq, int comment_seq, string report_code = "", string report_msg = "")
        {
            if (Request.IsAjaxRequest())
            {
                var login_type = LoginHelper.LoginType;
                var uid = LoginHelper.UID;
                var ip = Common.Util.GetClientIPAddress();

                var result = Joins.Apps.Repository.Sully.Web.CommentRepository.SetReportByUser(login_type, uid, comment_seq, report_code, report_msg, ip);

                return Json(new
                {
                    result = result.IsSuccess ? "Y" : "N",
                    msg = result.IsSuccess ? string.Empty : result.ResultMessage
                });
            }

            return View(new CommentReport
            {
                PD_SEQ = seq,
                COMMENT_SEQ = comment_seq
            });
        }

        [HttpPost]
        [AjaxOnly]
        [Login(RequiredLogin = true)]
        [Route("{seq:int}/Api/Like")]
        public ActionResult SetLike(int seq)
        {
            var login_type = LoginHelper.LoginType;
            var uid = LoginHelper.UID;
            var like_yn = string.Empty;
            var total_like_cnt = 0;

            var result = ProductRepository.SetProductLike(login_type, uid, seq, out like_yn, out total_like_cnt);

            return Json(new
            {
                result = result.IsSuccess ? "Y" : "N",
                like_yn = result.IsSuccess ? like_yn : "N",
                total_like_cnt = result.IsSuccess ? total_like_cnt : 0
            });
        }

        [HttpPost]
        [AjaxOnly]
        [Login(RequiredLogin = true)]
        [Route("{seq:int}/Api/Scrap")]
        public ActionResult SetScrap(int seq)
        {
            var login_type = LoginHelper.LoginType;
            var uid = LoginHelper.UID;
            var scrap_yn = string.Empty;

            var result = ProductRepository.SetProductScrap(login_type, uid, seq, out scrap_yn);

            return Json(new
            {
                result = result.IsSuccess ? "Y" : "N",
                scrap_yn = result.IsSuccess ? scrap_yn : "N",
            });
        }
    }
}