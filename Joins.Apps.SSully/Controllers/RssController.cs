﻿using Joins.Apps.Common.Sully;
using Joins.Apps.Models.Sully;
using Joins.Apps.Repository.Sully;
using Joins.Apps.SSully.Models.Rss;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Joins.Apps.SSully.Controllers
{
    [RoutePrefix("rss")]
    public class RssController : Controller
    {
        /// <summary>
        /// 사이트맵 XML을 생성합니다.
        /// </summary>
        /// <returns></returns>
        [Route("sitemap")]
        public ActionResult Sitemap()
        {
            var xml = string.Empty;
            var data = ProductRepository.GetSitemapProductList();

            var xmlSerializer = new XmlSerializer(data.GetType());

            using (var stream = new MemoryStream())
            {
                using (XmlTextWriter writer = new XmlTextWriter(stream, Encoding.UTF8))
                {
                    var ns = new XmlSerializerNamespaces();
                    ns.Add(string.Empty, "http://www.sitemaps.org/schemas/sitemap/0.9");
                    xmlSerializer.Serialize(writer, data, ns);

                    stream.Seek(0, SeekOrigin.Begin);

                    using (var reader = new StreamReader(stream, Encoding.UTF8))
                        xml = reader.ReadToEnd();
                }
            }

            return Content(xml, "text/xml");
        }

        /// <summary>
        /// 플립보드용 RSS를 생성합니다.
        /// </summary>
        /// <returns></returns>
        [Route("flipboard")]
        public ActionResult Rss()
        {
            var data = ProductRepository.GetRssProductList(30);
            var items = data.Select(x => {
                var url = string.Format("https://ssully.joins.com/view/{0}?share=y&ch=flipboard", x.Seq);

                var item = new SyndicationItem {
                    Title = SyndicationContent.CreateXhtmlContent(x.Title),
                    Content = SyndicationContent.CreateXhtmlContent(string.IsNullOrWhiteSpace(x.ImageUrl) ? x.Content : string.Format(@"<img src=""{0}""/>{1} <a href=""{2}"" target=""_blank"">▶ 보다 친절하고 자세한 내용은 썰리에서 읽어보세요 ◀</a>", x.ImageUrl, x.Content, url)),
                    Id = url,
                    PublishDate = DateTimeOffset.Parse(string.Format("{0:yyyy-MM-dd HH:mm:ss}", x.ServiceDate)),
                };

                item.AddPermalink(new Uri(url));
                item.Authors.Add(new SyndicationPerson("썰리"));
                item.Categories.Add(new SyndicationCategory(x.CategoryName));

                if (!string.IsNullOrWhiteSpace(x.ImageUrl))
                    item.ElementExtensions.Add(new XElement("enclosure", new XAttribute("type", "image/jpeg"), new XAttribute("url", x.ImageUrl), new XAttribute("length", x.ImageLength)).CreateReader());

                return item;
            });
            
            SyndicationFeed feed = new SyndicationFeed("썰리 - 썰로 푸는 이슈 정리", "아무리 어려운 뉴스라도 '썰리'가 설명∙정리 해 드립니다.", new Uri("https://ssully.joins.com"), items);
            feed.Language = "ko-kr";

            return new RssResult(feed);
        }
    }
}