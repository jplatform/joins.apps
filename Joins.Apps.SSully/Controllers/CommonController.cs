﻿using Joins.Apps.Common;
using Joins.Apps.Common.Sully;
using Joins.Apps.Repository.Sully;
using Joins.Apps.SSully.Attributes;
using Joins.Apps.SSully.Helpers;
using Joins.Apps.SSully.Models.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml.Linq;

namespace Joins.Apps.SSully.Controllers
{
    [PreventiveMaintenanceCheck]
    [RoutePrefix("Common")]
    public class CommonController : Controller
    {
        [NonAction]
        private Notice GetNoticeData(int? seq)
        {
            return new Notice { Data = NoticeRepository.GetServiceList(), RequestSeq = seq };
        }
        /// <summary>
        /// [앱뷰] 공지사항
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        [Route("App/Notice")]
        [Route("App/Notice/{seq:int}")]
        public ActionResult NoticeApp(int? seq)
        {
            return View(GetNoticeData(seq));
        }
        /// <summary>
        /// 공지사항
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        [Route("Notice")]
        [Route("Notice/{seq:int}")]
        public ActionResult Notice(int? seq)
        {
            return View(GetNoticeData(seq));
        }



        /// <summary>
        /// [앱뷰] 이용약관
        /// </summary>
        /// <returns></returns>
        [Route("App/TermsOfUse")]
        public ActionResult TermsOfUseApp()
        {
            return View();
        }
        /// <summary>
        /// 이용약관
        /// </summary>
        /// <returns></returns>
        [Route("TermsOfUse")]
        public ActionResult TermsOfUse()
        {
            return View();
        }



        /// <summary>
        /// [앱뷰] 개인정보취급방침
        /// </summary>
        /// <returns></returns>
        [Route("App/PrivacyPolicy")]
        public ActionResult PrivacyPolicyApp()
        {
            return View();
        }
        /// <summary>
        /// 개인정보취급방침
        /// </summary>
        /// <returns></returns>
        [Route("PrivacyPolicy")]
        public ActionResult PrivacyPolicy()
        {
            return View();
        }



        /// <summary>
        /// 문의/제안
        /// </summary>
        /// <returns></returns>
        [Route("QA")]
        [Login(RequiredLogin = true)]
        public ActionResult QA(int? target_seq)
        {
            return View(new Question { Seq = target_seq });
        }

        [AjaxOnly]
        [Route("QA/List")]
        [Login(RequiredLogin = true)]
        public ActionResult QAList(int? target_seq, int? page, int page_size = 10)
        {
            var login_type = LoginHelper.LoginType;
            var uid = LoginHelper.UID;
            var total_count = 0;
            var total_page = 0;
            var current_page = 0;

            var result = Joins.Apps.Repository.Sully.Web.QuestionRepository.GetQuestionList(login_type, uid, page, page_size, target_seq, out total_count, out total_page, out current_page);

            return Json(new
            {
                list = result != null ? result.Select(x => new
                {
                    seq = x.Seq,
                    admin_yn = x.AdminYN,
                    external_link = x.ReplyType == 2 ? new
                    {
                        domain = x.LinkExternalDomain,
                        thumbnail = x.LinkExternalThumbnail,
                        title = x.LinkExternalTitle,
                        url = x.LinkExternalUrl
                    } : null,
                    internal_link = x.ReplyType == 3 ? new
                    {
                        thumbnail = x.LinkInernalThumbnail,
                        seq = x.LinkInternalSeq,
                        title = x.LinkInternalTitle,
                        publish_date = string.Format("{0:yyyy-MM-dd}", x.LinkInternalServiceDateTime)
                    } : null,
                    message = Server.HtmlEncode(string.IsNullOrWhiteSpace(x.Message) ? "" : x.Message).Replace("\n", "</br>"),
                    write_duration = DateTimeDisplayConverter.ConvertDateDuration(x.RegDateTime),
                    message_type = x.ReplyType
                }) : null,
                total_count = total_count,
                total_page = total_page,
                current_page = current_page
            }, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        [HttpPost]
        [Login(RequiredLogin = true)]
        [Route("QA/Insert")]
        public ActionResult QAInsert(string  content)
        {
            var login_type = LoginHelper.LoginType;
            var uid = LoginHelper.UID;
            var ip = Util.GetClientIPAddress();

            if (string.IsNullOrWhiteSpace(content))
                return Json(new { result = "N", msg = "내용을 입력해 주세요" });

            var result = Joins.Apps.Repository.Sully.Web.QuestionRepository.SetReply(login_type, uid, content);

            return Json(new { result = result.IsSuccess ? "Y" : "N", msg = result.ResultMessage });
        }



        /// <summary>
        /// 허니스크린 RSS
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("rss_hs")]
        public XDocument rss_hs()
        {
            var tot_cnt = 0;
            var param = new Joins.Apps.Models.Sully.SCProductListParam { pgi = 1, ps = 10 };
            var ip = Common.Util.GetClientIPAddress();
            var reList = ProductRepository.GetServiceProductList(param, null, null, ip, out tot_cnt).ToList();

            Response.AddHeader("Content-Type", "text/xml");

            var sb = new StringBuilder();
            sb.AppendLine(@"<rss xmlns:content=""http://purl.org/rss/1.0/modules/content/"" xmlns:dc=""http://purl.org/dc/elements/1.1/"" xmlns:media=""http://search.yahoo.com/mrss/"" xmlns:atom=""http://www.w3.org/2005/Atom"" xmlns:georss=""http://www.georss.org/georss"" version=""1.0"">");
            sb.AppendLine(@"<channel>");
            sb.AppendLine(@"<title><![CDATA[썰리 - 썰로 푸는 이슈 정리]]> </title>");
            sb.AppendLine(@"<link><![CDATA[https://ssully.joins.com]]></link>");
            sb.AppendLine(@"<description><![CDATA[아무리 어려운 뉴스라도 ‘썰리’가 설명∙정리 해 드립니다.]]></description>");
            sb.AppendLine(@"<lastBuildDate>" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @" +0900 </lastBuildDate>");
            sb.AppendLine(@"<atom:link rel=""hub"" href=""http://pubsubhubbub.appspot.com"" />");

            if (reList.Count > 0)
            {
                var sProductImg = string.Empty;
                foreach (var product in reList)
                {
                    sProductImg = product.PD_SHARE_HONEYSCREEN_IMG_URL;

                    if (string.IsNullOrWhiteSpace(sProductImg))
                        sProductImg = product.PD_SHARE_IMG_URL;

                    if (string.IsNullOrWhiteSpace(sProductImg))
                        sProductImg = product.PD_IMG_URL;

                    sb.AppendLine(@"    <item>");
                    sb.AppendLine(@"        <guid isPermaLink=""false""> " + product.PD_SEQ + @" </guid>");
                    sb.AppendLine(@"        <title><![CDATA[" + product.PD_TITLE + @"]]></title>");
                    sb.AppendLine(@"        <link><![CDATA[https://ssully.joins.com/View/" + product.PD_SEQ + @"?share=y&ch=hs]]></link>");
                    sb.AppendLine(@"        <dc:creator><![CDATA[썰리]]></dc:creator>");
                    sb.AppendLine(@"        <content:encoded><![CDATA[" + sProductImg + @"]]></content:encoded>");
                    sb.AppendLine(@"        <pubDate> " + product.PD_SERVICE_DT + @" +0900 </pubDate>");
                    sb.AppendLine(@"    </item>");
                }
            }

            sb.AppendLine(@"</channel>");
            sb.AppendLine(@"</rss>");

            return XDocument.Parse(sb.ToString());
        }

        [HttpGet]
        [Route("Image")]
        public async Task<ActionResult> GetImageUrl(string url)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync(url);
                var stream = await result.Content.ReadAsStreamAsync();
                return new FileStreamResult(stream, result.Content.Headers.ContentType.MediaType);
            }
        }
    }
}