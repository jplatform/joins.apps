﻿using Joins.Apps.Common.Utilities;
using Joins.Apps.Repository.Sully;
using Joins.Apps.SSully.Attributes;
using Joins.Apps.SSully.Helpers;
using Joins.Apps.SSully.Models.User.Parameters;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Joins.Apps.SSully.Controllers
{
    [PreventiveMaintenanceCheck]
    [RoutePrefix("User")]
    public class UserController : Controller
    {
        [Route("Login")]
        public ActionResult Login()
        {
            if (LoginHelper.IsLogin)
                return Redirect("/User/Logout");

            return View();
        }

        [Login(RequiredLogin = true)]
        [Route("Logout")]
        public ActionResult Logout()
        {
            LoginHelper.RemoveLoginCookie();
            return Redirect("/");
        }

        [Login(RequiredLogin = true)]
        [Route("Withdraw")]
        public ActionResult Withdraw()
        {
            var login_type = LoginHelper.LoginType;
            var uid = LoginHelper.UID;

            var result = UserRepository.SetUserWithdraw(login_type, uid);

            if (result.IsSuccess)
            {
                LoginHelper.RemoveLoginCookie();
                return Redirect("/");
            }

            return Content(string.Format("<html><head><script>alert('{0}'); location.href='/';</script></head></html>", result.ResultMessage));
        }

        #region 회원정보 수정

        [HttpGet]
        [Route("Info")]
        [Login(RequiredLogin = true)]
        public ActionResult Info()
        {
            var login_type = LoginHelper.LoginType;
            var uid = LoginHelper.UID;
            var model = UserRepository.GetUserInfo(login_type, uid);

            return View(model);
        }

        [HttpPost]
        [AjaxOnly]
        [Route("Info")]
        [Login(RequiredLogin = true)]
        public ActionResult PostInfo(PostInfo data) {
            var login_type = LoginHelper.LoginType;
            var uid = LoginHelper.UID;
            var category_xml = string.Empty;

            if (data.Category != null)
                category_xml = string.Format("<ROOT>{0}</ROOT>", string.Join(string.Empty, data.Category.Where(x => !string.IsNullOrEmpty(x)).Select(x => string.Format("<CATEGORY>{0}</CATEGORY>", x.Trim())).ToArray()));

            var result = UserRepository.SetUserModifyInfo(login_type, uid, data.ProfileImageUrl, data.Name, data.Gender, data.Age, category_xml);
            
            if (result.IsSuccess)
            {
                var user = UserRepository.GetUserInfo(login_type, uid);

                LoginHelper.SetLoginCookie(user);

                return Json(new
                {
                    result = "Y",
                    msg = string.Empty,
                    info = new
                    {
                        name = user.NickName,
                        profile_image = user.ProfileImgUrl
                    }
                }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { result = "N", msg = result.ResultMessage }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("Info/Upload")]
        [Login(RequiredLogin = true)]
        public ActionResult UploadImage(HttpPostedFileBase profile_image)
        {
            var allowExtensions = new string[] { ".jpg", ".jpeg", ".png", ".gif" };
            var tempPath = string.Format("{0}ssully\\{1:yyyy}\\{1:MM}\\", ConfigurationManager.AppSettings["FilePath"], DateTime.Today);
            // 허용된 파일 확장자여부 확인
            Func<string, bool> checkExtension = (ext => !string.IsNullOrEmpty(ext) && allowExtensions.Contains(ext.ToLower()));
            // FTP 업로드 경로
            Func<string, string> uploadFTPPath = (ext => string.Format("service/ssully/profile/{0:yyyy}/{0:MM}/{0:dd}/", DateTime.Now));
            // 서버에 저장할 파일명 (임시, FTP)
            Func<string, string> uploadFileName = (ext => string.Format("{0:yyyyMMddHHmmss}{1:00000}{2}", DateTime.Now, new Random().Next(10000, 99999), ext));

            if (profile_image != null)
            {
                var ext = Path.GetExtension(profile_image.FileName);
                var saveFileName = uploadFileName(ext);
                var uploadFTPFilePath = uploadFTPPath(ext);
                var tempFileName = Path.Combine(tempPath, saveFileName);

                // 파일 확장자 체크
                if (!checkExtension(ext))
                    return Json(new { result = "N", image_url = string.Empty, msg = "허용된 파일 형태가 아닌 파일이 있습니다." });

                if (!Directory.Exists(tempPath))
                    Directory.CreateDirectory(tempPath);

                profile_image.SaveAs(tempFileName);

                var ftpResult = FTPUtility.Upload(tempPath, saveFileName, uploadFTPFilePath, saveFileName, true);

                System.IO.File.Delete(tempFileName);

                if (!ftpResult.IsSuccess)
                    return Json(new { result = "N", image_url = string.Empty, msg = "파일 업로드 중 오류가 발생하였습니다." });

                return Json(new { result = "Y", image_url = ftpResult.AccessURL, msg = string.Empty });
            }

            return Json(new { result = "N", image_url = string.Empty, msg = "파일을 업로드 할 수 없습니다." });
        }

        [HttpPost]
        [AjaxOnly]
        [Route("Info/NickName")]
        [Login(RequiredLogin = true)]
        public ActionResult ValidNickName(string nick_name)
        {
            if (string.IsNullOrWhiteSpace(nick_name))
                return Json(new { result = "N", msg = "닉네임을 입력해 주세요." }, JsonRequestBehavior.AllowGet);

            // 닉네임은 영한숫자로만 가능 (최대 10자)
            if (!Regex.IsMatch(nick_name, "^[0-9a-zA-Zㄱ-힣]{1,10}$"))
                return Json(new { result = "N", msg = "닉네임은 영문, 한글, 숫자로 최대 10자까지 가능합니다." }, JsonRequestBehavior.AllowGet);

            var result = UserRepository.GetValidateUserNickName(nick_name);

            return Json(new { result = result.IsSuccess ? "Y" : "N", msg = result.ResultMessage }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region 회원가입

        [HttpGet]
        [Route("Regist")]
        [Login(RequiredLogin = true, CheckWalkThroughCompleted = false)]
        public ActionResult Regist()
        {
            if (LoginHelper.IsLogin)
                return View(LoginHelper.LoginCookie);

            return Content("<html><head><script>alert('잘못된 접근입니다. 처음부터 다시 진행해 주시기 바랍니다.'); location.href='/';</script></head></html>");
        }
        
        [HttpPost]
        [AjaxOnly]
        [Route("Regist")]
        [Login(RequiredLogin = true, CheckWalkThroughCompleted = false)]
        public ActionResult PostRegist(PostRegist data)
        {
            var login_type = LoginHelper.LoginType;
            var uid = LoginHelper.UID;

            var category_xml = string.Empty;

            if (data.Category != null)
                category_xml = string.Format("<ROOT>{0}</ROOT>", string.Join(string.Empty, data.Category.Where(x => !string.IsNullOrEmpty(x)).Select(x => string.Format("<CATEGORY>{0}</CATEGORY>", x.Trim())).ToArray()));

            var result = UserRepository.SetUserRegistInfo(login_type, uid, data.Gender, data.Age, category_xml);

            LoginHelper.SetLoginCookie(UserRepository.GetUserInfo(login_type, uid));

            return Json(new { result = result.IsSuccess ? "Y" : "N", msg = result.ResultMessage }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("Regist/NickName")]
        [Login(RequiredLogin = true, CheckWalkThroughCompleted = false)]
        public ActionResult RegistNickName(string nick_name)
        {
            if (string.IsNullOrWhiteSpace(nick_name))
                return Json(new { result = "N", msg = "닉네임을 입력해 주세요." }, JsonRequestBehavior.AllowGet);

            // 닉네임은 영한숫자로만 가능 (최대 10자)
            if (!Regex.IsMatch(nick_name, "^[0-9a-zA-Zㄱ-힣]{1,10}$"))
                return Json(new { result = "N", msg = "닉네임은 영문, 한글, 숫자로 최대 10자까지 가능합니다." }, JsonRequestBehavior.AllowGet);

            var result = UserRepository.SetUserNickName(LoginHelper.LoginType, LoginHelper.UID, nick_name);

            return Json(new { result = result.IsSuccess ? "Y" : "N", msg = result.ResultMessage }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        [Route("Notification")]
        [Login(RequiredLogin = true)]
        public ActionResult Notification(int page = 1, int page_size = 10)
        {
            if (Request.IsAjaxRequest())
            {
                var login_type = LoginHelper.LoginType;
                var uid = LoginHelper.UID;
                var total_count = 0;
                var total_page = 0;
                var result = UserRepository.GetNotificationList(login_type, uid, page, page_size, out total_count, out total_page);
                var notification_class = new Func<string, string>(x => {
                    if ("1000".Equals(x))
                        return "warning";
                    else if ("2000".Equals(x))
                        return "notice";
                    else if ("3000".Equals(x))
                        return "reply";
                    else if ("5000".Equals(x))
                        return "like";
                    else if ("6000".Equals(x))
                        return "normal";

                    return string.Empty;
                });

                return Json(new
                {
                    total_count = total_count,
                    total_page = total_page,
                    data = result?.Select(x => new
                    {
                        notification_code = x.NotifyCode,
                        notofication_class_name = notification_class(x.NotifyCode),
                        message = x.Message,
                        read_yn = x.ReadYN,
                        reg_datetime = string.Format("{0:yyyy.MM.dd}", x.RegDateTime),
                        seq = x.LinkSeq,
                        sub_seq = x.LinkSubSeq,
                        thumbnail_url = x.ThumbnailUrl
                    })
                }, JsonRequestBehavior.AllowGet);
            }

            return View();
        }

        [Route("NotifyCount")]
        public ActionResult GetNotifyCount()
        {
            var unreadNotiCount = 0;

            if (LoginHelper.IsLogin)
            {
                var login_type = LoginHelper.LoginType;
                var uid = LoginHelper.UID;

                unreadNotiCount = Repository.Sully.API.NotificationRepository.GetNewNotifyCount(login_type, uid);
            }

            return Json(new { unread_notify_count = unreadNotiCount }, JsonRequestBehavior.AllowGet);
        }

        [Route("Scrap")]
        [Login(RequiredLogin = true)]
        public ActionResult Scrap(int page = 1, int page_size = 10)
        {
            if (Request.IsAjaxRequest())
            {
                var login_type = LoginHelper.LoginType;
                var uid = LoginHelper.UID;
                var total_count = 0;
                var total_page = 0;
                var result = ScrapRepository.GetScrapList(login_type, uid, page, page_size, out total_count, out total_page);

                return Json(new
                {
                    total_count = total_count,
                    total_page = total_page,
                    data = result?.Select(x => new
                    {
                        category = x.Category,
                        corner_type = x.CornerType,
                        is_ad_product = x.IsADProduct,
                        product_seq = x.ProductSeq,
                        product_title = x.ProductTitle,
                        publish_date = string.Format("{0:yyyy.MM.dd}", x.PublishDate),
                        thumbnail_url = x.Thumbnail,
                        provider_url = x.ProviderUrl
                    })
                }, JsonRequestBehavior.AllowGet);
            }

            return View();
        }
    }
}