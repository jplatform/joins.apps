﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.SSully.Models.Login
{
    public class TempData
    {
        public string State { get; set; }
        public string ReturnURL { get; set; }
    }
}