﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.SSully.Models.Login
{
    public class LoginProfile
    {
        /// <summary>
        /// 로그인 형식
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 고유아이디
        /// </summary>
        public string UID { get; set; }
        /// <summary>
        /// 메일주소
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 프로필 이미지주소
        /// </summary>
        public string ProfileImage { get; set; }
        /// <summary>
        /// 이름
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 인증 토큰
        /// </summary>
        public string Token { get; set; }
    }
}