﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.SSully.Models.View
{
    public class CommentReport
    {
        public int PD_SEQ { get; set; }
        public int COMMENT_SEQ { get; set; }
    }
}