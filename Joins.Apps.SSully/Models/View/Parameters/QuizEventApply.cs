﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.SSully.Models.View.Parameters
{
    public class QuizEventApply
    {
        public int Seq { get; set; }
        public string AnswerGUID { get; set; }
        public string UserName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string RequireAgreeYN { get; set; }
        public string RequireOptionalYN { get; set; }
        public string ApplyPosition { get; set; }
    }
}