﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace Joins.Apps.SSully.Models.View.Parameters
{
    public class QuizAnswerApply
    {
        public int QuestionSeq { get; set; }
        public int ItemSeq { get; set; }
    }
}