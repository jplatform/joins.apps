﻿using Joins.Apps.Models.Sully;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.SSully.Models.View
{
    public class QuizApplyFormApp
    {
        /// <summary>
        /// 기사정보
        /// </summary>
        public Product Product { get; set; }
        /// <summary>
        /// 로그인 종류
        /// </summary>
        public string LoginType { get; set; }
        /// <summary>
        /// 유저 고유값
        /// </summary>
        public string UID { get; set; }
        /// <summary>
        /// 프로덕트 고유번호
        /// </summary>
        public int PD_SEQ { get; set; }
        /// <summary>
        /// 퀴즈 응답 고유아이디
        /// </summary>
        public string Guid { get; set; }
        /// <summary>
        /// 접속환경 (W : 웹 / A : 안드로이드 / I : iOS)
        /// </summary>
        public string Position { get; set; }
    }
}