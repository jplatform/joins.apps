﻿using Joins.Apps.Models.Sully;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.SSully.Models.View
{
    public class View
    {
        /// <summary>
        /// 기사정보
        /// </summary>
        public Product Product { get; set; }
        /// <summary>
        /// 이벤트 중인 썰리퀴즈 정보 (메인 레이아웃에 노출되는 데이터 연동)
        /// </summary>
        public EventQuizProductInfo EventQuizProduct { get; set; }
        /// <summary>
        /// 기사 내용
        /// </summary>
        public ProductContent Content { get; set; }
        /// <summary>
        /// 추천 기사
        /// </summary>
        public IEnumerable<Product> RecommendProduct { get; set; }
        /// <summary>
        /// 공유용 URL 접근여부
        /// </summary>
        public bool IsShareLink { get; set; }
        /// <summary>
        /// 뉴스텐 앱 여부
        /// </summary>
        public bool IsNews10 { get; set; }
        /// <summary>
        /// 마지막 위치 복원 기능 여부
        /// </summary>
        public bool IsStatusRestore { get; set; }
        /// <summary>
        /// 공유용 이미지
        /// </summary>
        public string ShareImage { get; set; }
        /// <summary>
        /// 미리보기여부
        /// </summary>
        public bool IsPreview { get; set; }
        /// <summary>
        /// 댓글 갯수
        /// </summary>
        public int? CommentCount { get; set; }
    }
}