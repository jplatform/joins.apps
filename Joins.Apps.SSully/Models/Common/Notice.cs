﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.SSully.Models.Common
{
    public class Notice
    {
        /// <summary>
        /// 검색 데이터
        /// </summary>
        public IEnumerable<Joins.Apps.Models.Sully.Notice> Data { get; set; }
        /// <summary>
        /// 요청 공지사항 고유번호
        /// </summary>
        public int? RequestSeq { get; set; }
    }
}