﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.SSully.Models.User.Parameters
{
    public class PostInfo
    {
        public string ProfileImageUrl { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public int? Age { get; set; }
        public IEnumerable<string> Category { get; set; }
    }
}