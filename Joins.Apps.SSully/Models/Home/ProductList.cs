﻿using Joins.Apps.Models;
using Joins.Apps.Models.Sully;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.SSully.Models.Home
{
    public class ProductList
    {
        /// <summary>
        /// 결과 데이터
        /// </summary>
        public Page<Product> Result { get; set; }
        /// <summary>
        /// 카테고리 코드
        /// </summary>
        public CommonCode CategoryCode { get; set; }
        /// <summary>
        /// 코너 코드
        /// </summary>
        public CommonCode CornerCode { get; set; }
        /// <summary>
        /// 요청 파라메터 데이터
        /// </summary>
        public SCProductListParam Parameters { get; set; }
        /// <summary>
        /// 코너 리스트 헤더
        /// </summary>
        public CornerListHeader CornerListHeader { get; set; }
    }
}