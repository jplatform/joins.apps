﻿using Joins.Apps.Models.Sully.API;
using System.Collections.Generic;

namespace Joins.Apps.SSully.Models.Home
{
    public class MainList
    {
        /// <summary>
        /// 리스트
        /// </summary>
        public IEnumerable<MainItem> List { get; set; }
        /// <summary>
        /// 기사 총 갯수
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// 총 페이지 수
        /// </summary>
        public int TotalPage { get; set; }
        /// <summary>
        /// 현재 페이지
        /// </summary>
        public int Page { get; set; }
        /// <summary>
        /// 페이지당 리스트 수
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// 쇼케이스
        /// </summary>
        public IEnumerable<Apps.Models.Sully.SSullyMainShowcase> Showcase { get; set; }
        /// <summary>
        /// 파라메터
        /// </summary>
        public Apps.Models.Sully.SCProductListParam Parameters { get; set; }

        public class MainItem
        {
            /// <summary>
            /// 아이템 형식
            /// </summary>
            public string ItemType { get; set; }
            /// <summary>
            /// 프로덕트 데이터
            /// </summary>
            public IEnumerable<ProductMain> ProductData { get; set; }
            /// <summary>
            /// 여기가봤썰 데이터
            /// </summary>
            public ProductMainTour ProductTourData { get; set; }
            /// <summary>
            /// 위클리 랭킹 데이터
            /// </summary>
            public IEnumerable<Ranking> WeeklyRank { get; set; }
            /// <summary>
            /// 배너
            /// </summary>
            public Banner Banner { get; set; }
        }

        public class ProductMainTour : ProductMain
        {
            /// <summary>
            /// 여기가봤썰 이미지
            /// </summary>
            public IEnumerable<string> ProductTourImages { get; set; }

            public static ProductMainTour CopyTo(ProductMain data)
            {
                var sourceType = data.GetType();
                var target = new ProductMainTour();

                foreach (var sourceProperty in sourceType.GetProperties())
                {
                    var destinationProperty = target.GetType().GetProperty(sourceProperty.Name);

                    if (destinationProperty != null)
                        destinationProperty.SetValue(target, sourceProperty.GetValue(data));
                }

                return target;
            }
        }

        public class Banner
        {
            /// <summary>
            /// 이미지 경로
            /// </summary>
            public string ImageUrl { get; set; }
            /// <summary>
            /// 링크 종류
            /// </summary>
            public string LinkType { get; set; }
            /// <summary>
            /// 링크 고유번호
            /// </summary>
            public int LinkSeq { get; set; }
            /// <summary>
            /// 프로덕트 코너타입
            /// </summary>
            public int? CornerType { get; set; }
            /// <summary>
            /// 배경색
            /// </summary>
            public string Color { get; set; }
            /// <summary>
            /// 광고 컨텐트 여부
            /// </summary>
            public string IsADProduct { get; set; }
        }

        public class Ranking
        {
            public int AgeGroup { get; set; }
            public bool Selected { get; set; }
            public IEnumerable<Apps.Models.Sully.SSullyMainWeeklyRank> Rank { get; set; }
        }
    }

}