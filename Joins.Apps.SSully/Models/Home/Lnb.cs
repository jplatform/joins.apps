﻿using Joins.Apps.SSully.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.SSully.Models.Home
{
    public class Lnb
    {
        public bool IsLogin { get; set; }
        public LoginCookie LoginInfo { get; set; }
    }
}