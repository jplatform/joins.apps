﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.SSully.Models.Home
{
    public class WidgetPosition
    {
        /// <summary>
        /// 코너 위젯 위치
        /// </summary>
        public int? Corner { get; set; }
        /// <summary>
        /// 위클리 랭킹 위치
        /// </summary>
        public int? WeeklyRank { get; set; }
    }
}