﻿using Joins.Apps.Models.Sully;
using Joins.Apps.SSully.Models.Login;
using Newtonsoft.Json;
using System;
using System.Collections.Specialized;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace Joins.Apps.SSully.Helpers
{
    public class LoginHelper
    {
        private const string ENC_KEY = "!L0ginD&at#a&K(Ey";

        private static void SetCookie(string name, string value, bool is_encrypt)
        {
            try
            {
                if (string.IsNullOrEmpty(value))
                    value = string.Empty;

                if (is_encrypt)
                    value = JCube.AF.Module.Security.JoinsTripleDESEncrypt(value.Trim(), ENC_KEY);
                
                var cookie = new HttpCookie(name)
                {
                    Value = Microsoft.JScript.GlobalObject.escape(value)
                };

                HttpContext.Current.Response.Cookies.Add(cookie);
            }
            catch { }
        }
        private static string GetCookie(string name, bool is_decrypt)
        {
            try
            {
                var cookie = HttpContext.Current.Request.Cookies.Get(name);

                if (cookie == null)
                    return string.Empty;

                var value = cookie.Value;

                if (is_decrypt)
                    value = JCube.AF.Module.Security.JoinsTripleDESDecrypt(value, ENC_KEY).Trim();

                return Microsoft.JScript.GlobalObject.unescape(value);
            }
            catch {
                return string.Empty;
            }
        }
        private static void RemoveCookie(params string[] names)
        {
            foreach (var name in names)
            {
                var cookie = HttpContext.Current.Request.Cookies.Get(name);

                if (cookie != null)
                {
                    cookie.Expires = DateTime.Today.AddYears(-1);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
            }
        }


        /// <summary>
        /// 페이지 이동간 보관할 데이터를 쿠키에 암호화하여 저장합니다.
        /// </summary>
        /// <param name="data">저장할 데이터</param>
        public static void SaveCustomData(TempData data)
        {
            SetCookie("ssully_temp", JsonConvert.SerializeObject(data), true);
        }

        /// <summary>
        /// 페이지 이동간 보관한 데이터를 가져옵니다.
        /// </summary>
        /// <returns></returns>
        public static TempData LoadCustomData()
        {
            var cookie = GetCookie("ssully_temp", true);

            if (string.IsNullOrWhiteSpace(cookie))
                return null;

            return JsonConvert.DeserializeObject<TempData>(cookie);
        }

        /// <summary>
        /// 페이지 이동간 보관한 데이터를 삭제합니다.
        /// </summary>
        public static void RemoveCustomData()
        {
            RemoveCookie("ssully_temp");
        }

        /// <summary>
        /// 로그인 쿠키를 생성합니다.
        /// </summary>
        /// <param name="data"></param>
        public static void SetLoginCookie(LoginUserInfo data)
        {
            if (data != null)
            {
                SetCookie("ssully_type", data.LoginType, false);
                SetCookie("ssully_uid", data.Uid, false);
                SetCookie("ssully_name", HttpUtility.JavaScriptStringEncode(data.NickName), false);
                SetCookie("ssully_email", data.Email, true);
                SetCookie("ssully_profile_url", data.ProfileImgUrl, false);

                var validate_value = string.Format("{0}{1}{2}{3}", data.LoginType, data.Uid, data.NickName, data.Email);
                SetCookie("ssully_stamp", JCube.AF.Module.Security.SHA256Encrypt(validate_value), false);
            }
        }

        public static void RemoveLoginCookie()
        {
            RemoveCookie("ssully_type", "ssully_uid", "ssully_name", "ssully_email", "ssully_profile_url", "ssully_stamp");
        }

        public static string LoginType { get { return IsLogin ? LoginCookie.LoginType : null; } }
        public static string UID { get { return IsLogin ? LoginCookie.UID : null; } }
        public static bool IsValidateLoginData
        {
            get
            {
                if (!IsLogin)
                    return false;

                var validate_value = string.Format("{0}{1}{2}{3}", LoginCookie.LoginType, LoginCookie.UID, LoginCookie.Name, LoginCookie.Email);
                var cookie = GetCookie("ssully_stamp", false);

                return JCube.AF.Module.Security.SHA256Encrypt(validate_value).Equals(cookie);
            }
        }

        public static LoginCookie LoginCookie
        {
            get
            {
                if (!IsLogin)
                    return null;

                return new LoginCookie
                {
                    LoginType = GetCookie("ssully_type", false),
                    UID = GetCookie("ssully_uid", false),
                    Name = GetCookie("ssully_name", false),
                    Email = GetCookie("ssully_email", true),
                    ProfileUrl = GetCookie("ssully_profile_url", false)
                };
            }
        }
        /// <summary>
        /// 로그인 여부를 가져옵니다.
        /// </summary>
        /// <returns></returns>
        public static bool IsLogin
        {
            get
            {
                if (string.IsNullOrWhiteSpace(GetCookie("ssully_uid", false)) || string.IsNullOrWhiteSpace(GetCookie("ssully_type", false)))
                    return false;

                return true;
            }
        }

        /// <summary>
        /// URL 호출 결과를 반환합니다.
        /// </summary>
        /// <typeparam name="T">반환형식</typeparam>
        /// <param name="url">주소</param>
        /// <param name="acceptMediaType">Media Type</param>
        /// <param name="customHeaders">헤더</param>
        /// <returns></returns>
        public static async Task<T> GetAsyncUrlResult<T>(string url, string acceptMediaType, NameValueCollection customHeaders = null)
        {
            using (var client = new HttpClient())
            {
                if (!string.IsNullOrEmpty(acceptMediaType))
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(acceptMediaType));

                if (customHeaders != null)
                {
                    foreach(var key in customHeaders.AllKeys)
                        client.DefaultRequestHeaders.Add(key, customHeaders[key]);
                }

                var responseMessage = await client.GetAsync(url);

                if (responseMessage.StatusCode == System.Net.HttpStatusCode.OK)
                    return await responseMessage.Content.ReadAsAsync<T>();
            }

            return default(T);
        }

        /// <summary>
        /// POST 호출 후 결과를 반환합니다.
        /// </summary>
        /// <typeparam name="T">반환형식</typeparam>
        /// <param name="url">주소</param>
        /// <param name="acceptMediaType">Media Type</param>
        /// <param name="post">POST로 보낼 데이터</param>
        /// <param name="headers">헤더</param>
        /// <returns></returns>
        public static async Task<T> PostAsyncUrlResult<T>(string url, string acceptMediaType, FormUrlEncodedContent post = null, NameValueCollection headers = null)
        {
            using (var client = new HttpClient())
            {
                if (!string.IsNullOrEmpty(acceptMediaType))
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(acceptMediaType));

                if (headers != null)
                {
                    foreach (var key in headers.AllKeys)
                        client.DefaultRequestHeaders.Add(key, headers[key]);
                }

                var responseMessage = await client.PostAsync(url, post);

                if (responseMessage.StatusCode == System.Net.HttpStatusCode.OK)
                    return await responseMessage.Content.ReadAsAsync<T>();
            }

            return default(T);
        }
    }
}