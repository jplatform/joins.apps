﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.SSully.Helpers
{
    public class LoginCookie
    {
        public string LoginType { get; set; }
        public string UID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ProfileUrl { get; set; }
    }
}