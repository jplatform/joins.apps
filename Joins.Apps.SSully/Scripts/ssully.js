﻿function checkTitle() {
    var txtLength = $('.btn_tit').text().length;
    var txtWidth = $('.btn_tit').width() + 26;
    var headingWidth = $('header h2').width();

    if (headingWidth > txtWidth) {
        $('.btn_tit').addClass('short');
    } else {
        $('.btn_tit').removeClass('short')
    }
}




$(window).resize(function () {
    checkTitle();
    var winH = $(window).height(),
		btH = $('.bt').height(),
		hdH = $('.hd').height();
    $('.chat section').css('height', winH - 52);
 //   if ($('.container').hasClass('web-view')) { $('.chat section').css('height', winH - 52 - 20); } else { $('.chat section').css('height', winH - 52); }
    //	$('.write section textarea').css('height', winH- 52 - btH - hdH -58 - 57 );
});

$(function () {
    checkDevice();
    checkTitle();

    // 타이틀 팝업
    $('header .btn_tit').on('click', function () {
        $('.tit_layer').addClass('layeron');
    });
    $('.tit_layer').on('click', function () {
        $('.tit_layer').removeClass('layeron');
    });



    //init
    var imgW = $('dd span img').width(),
		docW = $('.container').width(),
		winH = $(window).height(),
		btH = $('.bt').height(),
		hdH = $('.hd').height();
    docH = $(document).height(),
    findDd = $("section").find("dd"),
    divSec = $("section");

    /*
    var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
    */
    // chat 높이
    $('.chat section').css('height', winH - 52);
//    if ($('.container').hasClass('web-view')) { $('.chat section').css('height', winH - 52 - 20);  } else { $('.chat section').css('height', winH - 52); }
    $('.error section').css('height', winH - 52);
    $('.chat .summary dd img').css('width', 228);
    $('.block, .particle dd').hide();

    // 이미지 레이어
    //$('dd.img span').on('click', function () {
    //    try { if (($(this).parent().parent()).hasClass('summary')) { return; } } catch (e) { }/*3줄요약이미지 제외*/
    //    fn_fullScreen_on();
    //    $('#imgView').addClass('layeron');
    //    var imgUrl = $(this).find("img").attr('src');
    //    var imgtitle = $(this).find("img").attr('title');
    //    var stateObj = { foo: "bar" };
    //    history.pushState(stateObj, "page 2", location.href);
    //    $('#imgView .thumb img').attr('src', imgUrl);
       
    //    if (imgtitle == undefined || imgtitle == '') { $('#imgView').find("div.caption").remove(); } else {
    //        if ($('#imgView .thumb div').is(".caption")) {
    //            $('#imgView .thumb .caption').html("<p>" + imgtitle + "</p>");
    //        } else {
    //            $('#imgView .thumb').append("<div class=\"caption\"><p>" + imgtitle + "</p></div>");
    //        }
    //    }
    //});
    $('dd.img span').on('click', function () {
        try { if (($(this).parent().parent()).hasClass('summary')) { return; } } catch (e) { }/*3줄요약이미지 제외*/
        
        var imgUrl = $(this).find("img").attr('src');
        var imgtitle = $(this).find("img").attr('title');
      //  fn_fullScreen_on();
          fn_layer_img_open(imgUrl, imgtitle);
        //setTimeout(function () {
        //    fn_layer_img_open(imgUrl, imgtitle);
        //}, 100)
       
    });
    function fn_layer_img_open(imgUrl, imgtitle) {
      //  console.log('a');
        $('#imgView').addClass('layeron');
        var stateObj = { foo: "bar" };
        history.pushState(stateObj, "page 2", location.href);
        $('#imgView .thumb img').attr('src', imgUrl);
        $('#imgView .thumb .inner_box').css("max-height", $(window).height()); // 2018-01-22 추가
        if (imgtitle == undefined || imgtitle == '') { $('#imgView').find("div.caption").remove(); } else {
            if ($('#imgView .thumb div').is(".caption")) {
                $('#imgView .thumb .caption').html("<p>" + imgtitle + "</p>");
            } else {
                $('#imgView .thumb').append("<div class=\"caption\"><p>" + imgtitle + "</p></div>");
            }
        }
    }
    window.onpopstate = function (event) {
        //   history.forward();
        if ($('#imgView').find('layeron')) {$('#imgView').removeClass('layeron');}
    };
    $('#imgView').click(function () {
        $(this).toggleClass('hf_hide');
        //$(this).removeClass('layeron');;
    });

    $('.layer_pop .btn_closed').click(function () {
        history.back();
        $('#imgView').removeClass('layeron');
    //   fn_fullScreen_off();
    });

    //간편로그인 열기
    $(".btn_share").on('click', function () {
        setTimeout(function () {
            $("#layer_share").toggleClass("layeron");
            return false;
        });
        $('#layer_share').show();
    });
    $(".layer_sns .btn_close").on('click touchstart', function () {
        $('#layer_share').hide();
        return false;
    });
});

function fn_fullScreen_on() {
    try {
        Mobile.onStartFullScreen();
    } catch (err) { }

    try {
        webkit.messageHandlers.callbackHandler.postMessage("onStartFullScreen");
    } catch (err) { }
}
function fn_fullScreen_off() {
    try {
        Mobile.onFinishFullScreen();
    } catch (err) { }

    try {
        webkit.messageHandlers.callbackHandler.postMessage("onFinishFullScreen");
    } catch (err) { }
}

// 접속 디바이스를 확인합니다(데스크탑:false, 모바일:true)
function checkDevice() {
    if (navigator.userAgent.indexOf('Mobile') != -1) {
        $('body').addClass('mobile');
        //		alert('Mobile 접속');
        return true;
    } else {
        $('body').addClass('pc')
        //		alert('PC 접속')
        return false;
    }
}