﻿
(function ($, window, document) {
    document.domain = 'joins.com';

    var noImage = utils.config('imagePath') + '/mw/article/v_noimg_comment.png';
    var reporterDataUrl = 'https://static.joins.com/joongang_15re/scripts/data/reporter.info.js';
    var loginPageUrl = 'https://my.joins.com/login/login_mobile_web_type2.asp';

    $.fn.comment = function (options) {
        var WEB_PATHS = {
            pc: utils.config('webPcPath'),
            mobile: utils.config('webMobilePath')
        };

        var REFER_URL = {
            article: 'joongang.joins.com/article',
            reporter: 'joongang.joins.com/reporter',
            jtbc: 'joongang.joins.com/jtbc'
        };

        var isLogin = false, // 로그인 상태 여부
            isWrite = true; // 글을 등록할 수 있는지 여부

        var apiPath = utils.config('apiPath');

        var per_page = 3;

        var defaults = {
            id: '',
            language: 'ko',
            page: 'event', //article,repoter,jtbc
            currentPage: 1,
            perPage: per_page,
            enabledPaste: false,
            enabledCopy: false,
            enabledDrag: false,
            enabledSort: true,
            headType: 'tab', //list, write
            bodyType: 'list', //write
            sortType: 'new', //new, good, bad
            referUrl: ''
        },
            config = $.extend(true, defaults, options),
            userInfo = null,
            reporterInfo = {},
            commentLanguage = window.commentLanguage[config.language],
         //   commentHtml = window.commentHtml(config.lang),
            commentDatas = { displayCount: 0, List: [], Count: 0 },
            reporterCookieName = 'reporterInfo';

        (function () {
            var reporterCookieInfo = utils.getCookie(reporterCookieName);

            userInfo = commentUserInfo.getInfo();
            isLogin = commentUserInfo.isLogin();

            if (reporterCookieInfo !== null) {
                reporterCookieInfo = reporterCookieInfo.split('&');

                for (var i = 0,
                        len = reporterCookieInfo.length; i < len; i++) {
                    var minfo = reporterCookieInfo[i].split('='),
                        key = minfo[0],
                        value = minfo[1];

                    if (key === 'REP_NAME') {
                        value = decodeURIComponent(value);
                    }

                    if (key === 'REP_VIEW_IMG') {
                        value = decodeURIComponent(value);
                    }

                    reporterInfo[key] = value;
                }

                if (reporterInfo.JOINS_ID === userInfo.id) { //기자이다!

                    userInfo.profileImage = reporterInfo.REP_VIEW_IMG;
                    userInfo.reporterSEQ = reporterInfo.REP_SEQ;

                } else {
                    //remove cookie
                    utils.removeCookie(reporterCookieName, COOKIE_CONDITION.path, COOKIE_CONDITION.domain);
                }
            } else {
                if (isLogin === true) {
                    getReporterList(function () { //기자정보 서치
                        reporterInfo = fnGetRepoterInfoByJoinsId(userInfo.id);
                        //rInfos = fnGetRepoterInfoByJoinsId('choyg94');//test

                        if (rInfos !== null) {
                            userInfo.profileImage = reporterInfo.REP_VIEW_IMG;
                            userInfo.reporterSEQ = reporterInfo.REP_SEQ;

                            utils.setCookie('reporterInfo', $.param(reporterInfo), 1, COOKIE_CONDITION.path, COOKIE_CONDITION.domain);
                        }
                    });
                }
            }

            init('new');
        })();

        function ajaxPost(obj) {
            utils.ajaxPost({
                url: obj.url,
                data: obj.params,
                success: function (res) {
                    obj.callback && obj.callback(res);
                }
            });
        }

        function submitComment(obj) {
            var params = {
                ParentId: obj.seqId,
                Content: obj.content.replace(/<[^>]*>/g, ' '),
                UserId: userInfo.id,
                UserName: userInfo.name,
                UserImage: userInfo.profileImage,
                UserLoginType: userInfo.type,
                GroupId: 0,
                UsedCache: false,
                ReferUrl: config.referUrl
            };

            if (config.page === 'article') {
                params.TotalId = config.id;
            }

            if (config.page === 'reporter') {
                params.ReporterId = config.id;
            }

            ajaxPost({
                url: apiPath + '/comment',
                params: params,
                callback: function (resData) {
                    var errorText = '';
                    if (resData.IsSuccess) {
                        obj.callback && obj.callback(resData);

                        isWrite = false;

                        setTimeout(function () {
                            isWrite = true;
                        }, 60000);
                    } else {
                        switch (resData.Code) {
                            case 'BadIp':
                                errorText = commentLanguage.errorBadIp;
                                break;
                            case 'BadWord':
                                errorText = resData.Target + commentLanguage.errorBadWord;
                                break;
                            case 'BadUserId':
                                errorText = resData.Target + commentLanguage.errorBadUser;
                                break;
                            default:
                                errorText = commentLanguage.fail;
                        }
                        alert(errorText);
                    }
                }
            });
        }

        function getReporterList(callback) {
            $.getScript(reporterDataUrl, function () {
                try {
                    callback && callback();
                } catch (e) {
                }
            });
        }

        function getCommentList(obj) {

            var url = '';// config.page !== 'jtbc' ? apiPath + '/' + config.page + '/' + config.id + '/comment' : apiPath + '/' + config.page + '/comment';

            if (config.page === 'jtbc' || config.page === 'event') {
                url = apiPath + '/event/comment';
            }
            else {
                url = apiPath + '/' + config.page + '/' + config.id + '/comment';
            }

            var params = {
                page: obj.currentPage || config.currentPage,
                pagesize: obj.perPage || config.perPage,
                sort: config.sortType,
                referurl: config.referUrl
            };

            if (config.page !== 'jtbc') {
                params.UserId = userInfo.id;
            }

            $.ajax({
                url: url,
                contentType: 'text/plain',
                data: params,
                dataType: 'jsonp',
                success: function (res) {
                    obj.callback && obj.callback(res);
                },
                error: function (request, status, error) {
                    if (request.status === 200 && request.responseText === undefined) { }
                }
            });
        }

        function commentVotePost(obj) {
            ajaxPost({
                url: apiPath + '/comment/' + obj.id + '/' + obj.type + '/' + utils.getCookie('PCID'),
                callback: function (res) {
                    obj.callback && obj.callback(res);
                }
            });
        }

        function commentReportPost(obj) {
            ajaxPost({
                url: apiPath + '/comment/' + obj.id + '/report',
                callback: function (res) {
                    if (res.IsSuccess) {
                        alert(commentLanguage.reportTrue);
                        obj.callback && obj.callback();
                    } else {
                        alert(commentLanguage.reportFail);
                    }
                }
            });
        }

        function commentDeletePost(obj) {
            var params = {};

            if (config.page === 'article') {
                params.TotalId = obj.totalid;
            }

            if (config.page === 'reporter') {
                params.ReporterId = obj.reporterid;
            }

            params.Id = obj.id;

            utils.ajaxPost({
                url: apiPath + '/comment/' + obj.id + '/delete',
                data: params,
                success: function () {
                    obj.callback && obj.callback();
                }
            });
        }

        function emptyComment($t) {
            $t.focus();
            alert(commentLanguage.errorEmptyContent);
        }

        function reSetPageValue() {
            config.currentPage = 1;
            config.perPage = per_page;
        }

        function fnResetCmtInput() {
            var txtId = "txtcmt";
            var divId = "div_cmt_write";
            $("#" + txtId).val('');
            $("#" + txtId).css("height", "1px").css("height", (4 + $("#" + txtId).prop("scrollHeight")) + "px");
            $("#" + divId).removeClass();
            $("#" + divId).addClass('cmt_write cmt_focus');
        }

        function updateCommentDatas(data, isReplace) {
            commentDatas.Count = data.Count;
            if (isReplace) {
                commentDatas.displayCount = data.displayCount;
                commentDatas.List = [];
                for (var i = 0, len = data.List.length; i < len; i++) {
                    if (IsActiveComment(data.List[i], data.List)) {
                        commentDatas.List.push(data.List[i]);
                    }
                }
                if ((commentDatas.List.length == 0) && data.List.length > 0) {
                    getCommentList({
                        currentPage: config.currentPage,
                        perPage: per_page + 1,
                        callback: function (datas) {
                            commentDatas.Count = datas.Count;
                            commentDatas.displayCount = datas.displayCount;
                            commentDatas.List = [];
                            for (var i = 0, len = datas.List.length; i < len; i++) {
                                if (IsActiveComment(datas.List[i], datas.List)) {
                                    commentDatas.List.push(datas.List[i]);
                                }
                            }
                        }
                    });
                }
            } else {
                for (var i = 0, len = data.List.length; i < len; i++) {
                    commentDatas.List.push(data.List[i]);
                }
            }
        }

        function IsActiveComment(comment, commentList) {
            if (comment.IsDelete != 'D') { return true; }
            else {
                for (var i = 0; i < commentList.length; i++) {
                    if (commentList[i].Id == comment.Id) continue;
                    if ((comment.Id == commentList[i].ParentId) && (commentList[i].IsDelete == 'A')) {
                        return true;
                    }
                }
                return false;
            }
        }

        function fnCommentItem(commentDatas) {
            var html = '';
            $.each(commentDatas.List, function () {
                var userID = '';
                if (this.UserLoginType.toLowerCase() === 'joins' && this.IsReporter === true) {
                    userID = this.UserName + ' ' + commentLanguage.isReporter;
                } else if (this.UserLoginType.toLowerCase() === 'joins' && this.IsJplusJouralist === true) {
                    userID = this.UserName;
                } else if (this.UserLoginType.toLowerCase() !== 'joins') {
                    userID = this.UserName;
                } else {
                    userID = this.UserId;
                }

                if (this.IsReporter !== true) {
                    if (userID.length > 4) {
                        userID = userID.substring(0, 3) + "****";
                    } else {
                        var len = userID.length / 2;
                        userID = userID.substring(0, len) + "****";
                    }
                }
                var isReply = this.Id !== this.ParentId ? true : false;
                var isReplyClass = '';
                var isRelpyID = '';
                if (isReply) {
                    isReplyClass = 'cmt_replay';
                    if ($("#isReplyUserId").val() !== undefined) {
                        isRelpyID = '<em>@' + $("#isReplyUserId").val() + '</em> ';
                    }
                } else {
                    $("#isReplyUserId").val(userID);
                }
                var isMyWriting = false;
                var isMyWritingHtml = '';
                if (isLogin === false || userInfo.id !== this.UserId) {
                    isMyWriting = false;
                } else {
                    isMyWriting = true;
                    isMyWritingHtml = 'cmt_my';
                }
                var content = '';
                var delFlag = false;
                switch (this.IsDelete) {
                    case "D":
                        delFlag = true;
                        content = commentLanguage.removeMessageUser;
                        break;
                    case "N":
                        delFlag = true;
                        content = commentLanguage.removeMessageAdmin;
                        break;
                    default:
                        content = this.Content;
                        break;
                }
                html += '<li id="li_' + this.Id + '">';
                html += '<!-- 댓글 -->';
                html += '<div class="inbox ' + isReplyClass + ' ' + isMyWritingHtml + '">';
                switch (this.UserLoginType.toLowerCase()) {
                    case "joins":
                        html += '<strong class="i_jb">중앙일보로 로그인</strong>';
                        break;
                    case "facebook":
                        html += '<strong class="i_fb">페이스북으로 로그인</strong>';
                        break;
                    case "twitter":
                        html += '<strong class="i_tw">트위터로 로그인</strong>';
                        break;
                    case "kakao":
                        html += '<strong class="i_kk">카카오톡으로 로그인</strong>';
                        break;
                    default:
                        html += '<strong class="i_jb">중앙일보로 로그인</strong>';
                        break;
                }
                html += '<div class="cmt_box">';
                html += '<dl class="cmt_info">';
                html += '<dt>';
                if (this.IsReporter === true) {
                    html += '<a href="/reporter/' + this.ReporterId + '" target="_self"><em>' + userID + '</em></a>';
                }
                else {
                    html += '<em>' + userID + '</em>';
                }
                html += '<span class="cmt_date">' + this.RegistedDateTime.toDateISO8061().format('yyyy.MM.dd HH:mm') + '</span>';
                html += '<button class="btn_tooltip" type="button"><span>레이어 열기</span></button>';
                html += '<div class="tooltip_box">';
                //html += '<a href="#">작성자의 댓글 모아보기</a>';
                if (isMyWriting) {
                    //if (!delFlag) {
                    html += '<a data-action="del" class="delete" href="#" data-deleteid="' + this.Id + '">삭제</a>';
                    //}
                }
                else {
                    html += '<a class="report" href="#" data-reportid="' + this.Id + '">신고하기</a>';
                }
                html += '</div>';
                html += '</dt>';
                html += '<dd>';
                html += '<p>';
                html += isRelpyID;
                html += content;
                html += '</p>';
                html += '</dd>';
                html += '</dl>';
                html += '<div class="cmt_btn">';
                if (!isReply) {
                    html += '<a href="#" class="btn_reply" data-replyid="' + this.Id + '" data-replyname="' + userID + '">답글달기</a>';
                }
                html += '<a href="#" class="btn_recomm" data-voiteid="' + this.Id + '" title="이 댓글을 공감합니다"><span>공감</span><em id="good_' + this.Id + '" class="good">' + this.GoodCount + '</em></a>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                html += '<!-- /댓글 -->';
                html += '</li>';
            });
            $(".cmt_lst").append(html);
        }

        function fnCommentList(commentDatas, type) {
            var html = '';

            html += '<div class="hd">';
            html += '<h3>댓글 <span id="sp_displayCount" class="displayCount">' + commentDatas.displayCount + '</span>개</h3>';
            html += '<div class="sns">';
            html += '<a><dl>';
            html += '<dt class="hide">간편로그인</dt>';
            html += '<dd class="i_jb" id="login_i_jb">중앙일보</dd><!-- 로그인시 on 클래스 넣어주세요. -->';
            html += '<dd class="i_fb" id="login_i_fb">페이스북</dd><!-- 로그인시 on 클래스 넣어주세요. -->';
            html += '<dd class="i_tw" id="login_i_tw">트위터</dd><!-- 로그인시 on 클래스 넣어주세요. -->';
            html += '<dd class="i_kk" id="login_i_kk">카카오톡</dd><!-- 로그인시 on 클래스 넣어주세요. -->';
            html += '</dl></a>';
            html += '</div>';
            html += '<form>';
            html += '<fieldset>';
            html += '<legend class="hide">댓글 쓰기</legend>';
            html += '<input type="text" id="txtReplyId" style="display:none" />';
            html += '<div class="cmt_write cmt_login" id="div_cmt_write"><!-- 로그인 전 cmt_login, 로그인 후 cmt_focus, 글 입력시 cmt_writing -->';
            html += '<label for="txtcmt" class="txtcmt">댓글을 작성해 주세요</label>';
            html += '<em class="replay" id="emReplyId"></em>';
            html += '<textarea name="txtcmt" id="txtcmt" value="" title="댓글 입력" tabindex="1"></textarea>';
            html += '<button id="btn_submit">등록</button>';
            html += '<div class="check_area" id="div_check_area" style="display:none">';
            html += '<a href="#" class="btn_del">삭제</a>';
            //html += '<span class="check"><input type="checkbox" id="upload" tabindex="2"> <label for="upload">페이스북에 동시 등록</label></span>';
            html += '</div>';
            html += '</div>';
            html += '</fieldset>';
            html += '</form>';
            html += '</div>';
            html += '<div class="bd" id="bd_comment" style="display:none">';
            html += '<h5 class="hide">댓글보기</h5>';
            html += '<ul class="cmt_sort">';
            html += '<li id="li_sort_new"><a href="javascript:void(0)" id="sort_new">최신순</a></li>';
            html += '<li id="li_sort_like"><a href="javascript:void(0)" id="sort_like">추천순</a></li>';
            html += '</ul>';
       //     html += '<a class="my_cmt" id="my_cmt" href="' + utils.config('webMobilePath') + '/mynews/comment" style="display:none">내 댓글 보기</a>';
            html += '<!-- 활성화 on 클래스 부여 -->';
            html += '<ul class="cmt_lst">';
            html += '</ul>';
            if (type !== 'none') {
                var commentListUrl = '',
                    mobilePath = utils.config('webMobilePath');

                if (config.page === 'jtbc') {
                    commentListUrl = mobilePath + '/jtbc/comment?referurl=' + config.referUrl + '&id=' + config.id;
                } else if (config.page === 'event') {
                    commentListUrl = mobilePath + '/event/comment?referurl=' + config.referUrl + '&id=' + config.id;
                } else {
                    commentListUrl = mobilePath + '/' + config.page + '/' + config.id + '/comment';
                }
            //    html += '<a class="more_type" href="' + commentListUrl + '?cloc=joongang|marticle|morecomment"><strong class="displayCount">' + commentDatas.displayCount + '</strong>개의 댓글 전체보기</a>';
            }
            html += '</div>';

            html += '<div id="layer_snslogin" class="fulllayer">';
            html += '<div class="layer_login">';
            html += '<strong class="tit_sub">간편 로그인</strong>';
            html += '<span class="txt">원하시는 계정을 선택해 로그인해 주세요.</span>';
            html += '<button class="btn_close" id="btn_close_comment" type="button">간편 로그인 닫기</button>';
            html += '<ul>';
            html += '<li><a class="i_jb sns_login" href="' + loginPageUrl + '" target="_self">중앙일보</a></li>';
            html += '<li><a class="i_fb sns_login" href="' + loginPageUrl + '" target="_self">페이스북</a></li>';
            html += '<li><a class="i_tw sns_login" href="' + loginPageUrl + '" target="_self">트위터</a></li>';
            html += '<li><a class="i_kk sns_login" href="' + loginPageUrl + '" target="_self">카카오톡</a></li>';
            html += '<li><a class="i_na sns_login" href="' + loginPageUrl + '" target="_self">네이버</a></li>'
            html += '</ul>';
            html += '</div>';
            html += '</div>';

            //html += '<div id="layer_cmtdel" class="fulllayer">';
            //html += '<div class="layer_cmtdel">';
            //html += '<strong class="tit_sub">댓글을 삭제하시겠습니까?</strong>';
            //html += '<button class="btn_close" type="button">댓글 삭제 레이어 닫기</button>';
            //html += '<div class="btn_area">';
            //html += '<button class="cancle" type="button">취소</button>';
            //html += '<button class="send" type="button">확인</button>';
            //html += '</div>';
            //html += '</div>';
            //html += '</div>';

            $("#comment").removeClass();
            $("#comment").addClass('cmt_wrap');
            $("#comment").html(html);
            if (type !== 'none') {
                fnCommentItem(commentDatas);
                $("#bd_comment").show();
            }
            if (config.sortType === "like") {
                $("#li_sort_like").addClass("on");
            } else {
                $("#li_sort_new").addClass("on");
            }
            $('#head').find('.btn_cmt').html('<span><em class="hide">댓글</em>' + commentDatas.displayCount + '</span>');
            $("#cmt_total_count").text('댓글 ' + commentDatas.displayCount + '개');
            // 로그인 처리
            if (isLogin) {
                switch (userInfo.type.toLowerCase()) {
                    case "joins":
                        $("#my_cmt").show();
                        $("#login_i_jb").addClass("on");
                        break;
                    case "facebook":
                        $("#login_i_fb").addClass("on");
                        break;
                    case "twitter":
                        $("#login_i_tw").addClass("on");
                        break;
                    case "kakao":
                        $("#login_i_kk").addClass("on");
                        break;
                }
                $(".cmt_write").removeClass("cmt_login");
                $(".cmt_write").addClass("cmt_focus");
            }

            ////간편로그인 열기
            //$(".cmt_login, .sns a").on('click', function () {
            //    $("#layer_snslogin").toggleClass("layeron");
            //    return false;
            //});
            //간편로그인 열기
            $(".cmt_login, .sns a").on('click', function () {
                setTimeout(function () {
                    $("#layer_snslogin").toggleClass("layeron");
                    return false;
                });
                $('#layer_snslogin').show();
            });
            $(".layer_login .btn_close").on('click touchstart', function () {
                $('#layer_snslogin').hide();
                return false;
            });

            //레이어 닫기
            $(".btn_close").on('click touchstart', function () {
                $(".fulllayer, .tooltip_box").removeClass("layeron");
                return false;
            });
            ////레이어 닫기
            //$("#btn_close_comment").on('click touchstart', function () {
            //    $(".fulllayer, .tooltip_box").removeClass("layeron");
            //    return false;
            //});

            //툴팁 레이어 오픈
            $(".btn_tooltip").each(function () {
                $(this).on('click', function () {
                    $(this).next().toggleClass("layeron");
                    return false;
                });
            });

            ////삭제 레이어 오픈
            //$(".tooltip_box a[data-action='del']").each(function () {
            //	$(this).on('click touchstart', function () {
            //		$("#layer_cmtdel").toggleClass("layeron");
            //		return false;
            //	});
            //});

            //새로고침
            $(".btn_refresh").on('click', function () {
                location.reload();
            });

            ////레이어 외 누르면 레이어 닫힘
            //$(document).on('click touchstart', function (e) {
            //	if (e.target === undefined) return;
            //	var $target = $(e.target);
            //	if ($target.closest(".layer_login, .tooltip_box").length === 0) {
            //		$(this).find(".fulllayer, .tooltip_box").removeClass("layeron");
            //	}
            //});
            //textarea 입력 시
            $("#txtcmt").focus(function () {
                //textarea 에 글을 입력하면 자동 리사이징
                $(this).css("height", "137px");
                //textarea 에 글을 입력하면 스크롤 제거
                $("body").find(".full_cmt .bd").removeClass("bd_scroll");
            });
            //textarea 입력 시
            $("#txtcmt").keyup(function () {
                //textarea 에 글을 입력하면 자동 리사이징
          //      $(this).css("height", "1px").css("height", (4 + $(this).prop("scrollHeight")) + "px");
                //textarea 에 글을 입력하면 스크롤 제거
         //       $("body").find(".full_cmt .bd").removeClass("bd_scroll");
                //textarea 글자 제한
                if ($(this).val().length > 500) {
                    $(this).val($(this).val().substring(0, 500));
                    alert("500자를 초과 입력 할 수 없습니다.");
                }

                if ($(this).val().length !== 0 || $("#txtReplyId").val().length !== 0) {
                    $("#div_check_area").show();
                }

                if ($(this).val().length === 0 && $("#txtReplyId").val().length === 0) {
                    $("#div_check_area").hide();
                    $("body").removeAttr("style");
                    $(".bd").addClass("bd_scroll");
                }
            });

            $("#txtcmt").change(function () {
                //textarea 글자 제한
                if ($(this).val().length > 500) {
                    $(this).val($(this).val().substring(0, 500));
                    alert("500자를 초과 입력 할 수 없습니다.");
                }

                if ($(this).val().length !== 0 || $("#txtReplyId").val().length !== 0) {
                    $("#div_check_area").show();
                }

                if ($(this).val().length === 0 && $("#txtReplyId").val().length === 0) {
                    $("#div_check_area").hide();
                    $("body").removeAttr("style");
                    $(".bd").addClass("bd_scroll");
                }
            });
        }

        function init(sort) {
            if ($("#comment").length === 0) {
                var html = '<div class="comment_area" id="comment"></div>';
                $("body").prepend(html);
            }

            if ($("#isReplyUserId").length === 0) {
                var html = '<input type="text" id="isReplyUserId" style="display:none" />';
                $("#comment").before(html);
            }

            try {
                config.referUrl = config.referUrl !== '' ? config.referUrl : REFER_URL[config.page];
                config.perPage = 5;
                config.sortType = sort;
            } catch (e) { alert('잘못된 호출입니다.'); }

            fnCommentList(commentDatas, 'none');

            getCommentList({
                callback: function (datas) {
                    updateCommentDatas(datas, true);
                    fnCommentList(datas, '');
                }
            });

            //button events
            $("#comment").unbind();

            if (config.enabledCopy === false) {
                $("#comment").on('copy', 'textarea', function () {
                    alert(commentLanguage.errorCopy);
                    return false;
                });
            }

            if (config.enabledPaste === false) {
                $("#comment").on('paste', 'textarea', function () {

                    alert(commentLanguage.errorPast);
                    return false;
                });
            }

            $("#comment").on('click', '.btn_reply', function () {
                var $reply = $(this);
                $("#txtReplyId").val($reply.data('replyid'));
                $("#emReplyId").html($reply.data('replyname'));
                $("#div_check_area").show();
                $("#txtcmt").focus();
                return false;
            }).on('click', '.btn_recomm', function () {
                var $up = $(this);

                commentVotePost({
                    type: 'like',
                    id: $up.data('voiteid'),
                    callback: function (res) {
                        var code = res.Code;

                        if (res.IsSuccess) {
                            var goodCount = parseInt($up.find('.good').text(), 10);
                            $up.find('.good').text(goodCount + 1);
                        } else {

                            switch (code) {
                                case 'Duplicated':
                                    msg = '중복된 참여입니다.';
                                    break;
                                case 'NotPeriod':
                                    msg = '기간이 아닙니다.';
                                    break;
                                default:
                                    msg = '데이타 처리에 문제가 발생하였습니다.';
                                    break;
                            }
                            alert(msg);
                        }
                    }
                });

                return false;
            }).on('click', '.report', function () {
                var $report = $(this);

                var result = confirm(commentLanguage.reportMessage);

                if (result === true) {
                    commentReportPost({ id: $report.data('reportid') });
                }

                return false;
            }).on('click', '.delete', function () {
                var $delete = $(this),
                    deleteId = $delete.data('deleteid');

                var opts = {
                    id: deleteId,
                    callback: function () {
                        alert(commentLanguage.deleteComment);
                        commentDatas.displayCount = commentDatas.displayCount - 1;

                        var count = parseInt($("#sp_displayCount").text(), 10);
                        if (count > 0) {
                            $(".displayCount").text(count - 1);
                            $('#head').find('.btn_cmt').html('<span><em class="hide">댓글</em>' + (count - 1) + '</span>');
                        }
                        if (commentDatas.displayCount == 0) {
                            $('#head').find('.btn_cmt').html('<span><em class="hide">댓글</em>0</span>');
                            fnCommentList(commentDatas, 'none');
                        } else {
                            $("#li_" + deleteId).remove();
                        }
                    }
                };

                if (config.page === 'article') {
                    opts.totalid = $delete.data('targetid');
                }

                if (config.page === 'reporter') {
                    opts.reporterid = $delete.data('targetid');
                }

                commentDeletePost(opts);

                return false;
            }).on('click', '.btn_del', function () {
                $("#txtReplyId").val('');
                $("#emReplyId").html('');
                $("#txtcmt").val('');
                $("#txtcmt").css("height", "1px").css("height", (4 + $("#txtcmt").prop("scrollHeight")) + "px");
                $("#txtcmt").focus();
                $("#div_check_area").hide();
                $("body").removeAttr("style");
                $(".bd").addClass("bd_scroll");

                return false;
            }).on('focus', '#div_cmt_write', function () {
                var $write = $(this);

                $write.removeClass("cmt_focus");
                $write.addClass("cmt_open cmt_writing");

                //$("#div_check_area").show();

                return false;
            }).on('click', '.sns_login', function () {
                commentUserInfo.logout(); //cookie remove
                commentUserInfo.init(); // get cookie
            }).on('click', '#sort_like', function () {
                config.currentPage = 1;
                init('like');
            }).on('click', '#sort_new', function () {
                config.currentPage = 1;
                init('new');
            }).on('submit', 'form', function () {
                var value = $("#txtcmt").val();
                var seq = $("#txtReplyId").val();
                if (isWrite === false) {
                    alert(commentLanguage.errorWrite);
                    return false;
                }
                if (value.length === 0 || value.replace(/(^\s*)|(\s*$)/g, "").length === 0) {
                    emptyComment($("#txtcmt"));
                    return false;
                }
                if (seq === "") {
                    seq = 0;
                }
                submitComment({
                    seqId: seq,
                    content: value,
                    callback: function (resData) {
                        if (resData.IsSuccess == true) {
                            if (seq === 0) {
                                var html = '';
                                var userID = '';
                                if (resData.Item.UserLoginType.toLowerCase() === 'joins' && resData.Item.IsReporter === true) {
                                    userID = resData.Item.UserName + ' ' + commentLanguage.isReporter;
                                } else if (resData.Item.UserLoginType.toLowerCase() === 'joins' && resData.Item.IsJplusJouralist === true) {
                                    userID = resData.Item.UserName;
                                } else if (resData.Item.UserLoginType.toLowerCase() !== 'joins') {
                                    userID = resData.Item.UserName;
                                } else {
                                    userID = resData.Item.UserId;
                                }
                                if (resData.Item.IsReporter !== true) {
                                    if (userID.length > 4) {
                                        userID = userID.substring(0, 3) + "****";
                                    } else {
                                        var len = userID.length / 2;
                                        userID = userID.substring(0, len) + "****";
                                    }
                                }
                                html += '<li id="li_' + resData.Item.Id + '">';
                                html += '<!-- 댓글 -->';
                                html += '<div class="inbox cmt_my">';
                                switch (resData.Item.UserLoginType.toLowerCase()) {
                                    case "joins":
                                        html += '<strong class="i_jb">중앙일보로 로그인</strong>';
                                        break;
                                    case "facebook":
                                        html += '<strong class="i_fb">페이스북으로 로그인</strong>';
                                        break;
                                    case "twitter":
                                        html += '<strong class="i_tw">트위터로 로그인</strong>';
                                        break;
                                    case "kakao":
                                        html += '<strong class="i_kk">카카오톡으로 로그인</strong>';
                                        break;
                                    default:
                                        html += '<strong class="i_jb">중앙일보로 로그인</strong>';
                                        break;
                                }
                                html += '<div class="cmt_box">';
                                html += '<dl class="cmt_info">';
                                html += '<dt>';
                                html += '<em>' + userID + '</em>';
                                html += '<span class="cmt_date">' + resData.Item.RegistedDateTime.toDateISO8061().format('yyyy.MM.dd HH:mm') + '</span>';
                                html += '<button class="btn_tooltip" id="btn_tooltip_' + resData.Item.Id + '" type="button"><span>레이어 열기</span></button>';
                                html += '<div class="tooltip_box">';
                                html += '<a data-action="del" class="delete" href="#" data-deleteid="' + resData.Item.Id + '">삭제</a>';
                                html += '</div>';
                                html += '</dt>';
                                html += '<dd>';
                                html += '<p>';
                                html += resData.Item.Content;
                                html += '</p>';
                                html += '</dd>';
                                html += '</dl>';
                                html += '<div class="cmt_btn">';
                                html += '<a href="#" class="btn_reply" data-replyid="' + resData.Item.Id + '" data-replyname="' + userID + '">답글달기</a>';
                                html += '<a href="#" class="btn_recomm" data-voiteid="' + resData.Item.Id + '" title="이 댓글을 공감합니다"><span>공감</span><em id="good_' + resData.Item.Id + '" class="good">' + resData.Item.GoodCount + '</em></a>';
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += '<!-- /댓글 -->';
                                html += '</li>';
                                $(".cmt_lst").prepend(html);
                                //툴팁 레이어 오픈
                                $("#btn_tooltip_" + resData.Item.Id).on('click', function () {
                                    $("#btn_tooltip_" + resData.Item.Id).next().toggleClass("layeron");
                                    return false;
                                });
                                $("#li_" + resData.Item.Id).on('click', '.delete', function () {
                                    var $delete = $(this),
										deleteId = $delete.data('deleteid');

                                    var opts = {
                                        id: deleteId,
                                        callback: function () {
                                            alert(commentLanguage.deleteComment);
                                            commentDatas.displayCount = commentDatas.displayCount - 1;
                                            var count = parseInt($("#sp_displayCount").text(), 10);
                                            if (count > 0) {
                                                $(".displayCount").text(count - 1);
                                                $('#head').find('.btn_cmt').html('<span><em class="hide">댓글</em>' + (count - 1) + '</span>');
                                            }
                                            if (commentDatas.displayCount == 0) {
                                                $('#head').find('.btn_cmt').html('<span><em class="hide">댓글</em>0</span>');
                                                fnCommentList(commentDatas, 'none');
                                            } else {
                                                $("#li_" + deleteId).remove();
                                            }
                                        }
                                    };

                                    if (config.page === 'article') {
                                        opts.totalid = $delete.data('targetid');
                                    }

                                    if (config.page === 'reporter') {
                                        opts.reporterid = $delete.data('targetid');
                                    }

                                    commentDeletePost(opts);

                                    return false;
                                }).on('click', '.btn_reply', function () {
                                    var $reply = $(this);
                                    $("#txtReplyId").val($reply.data('replyid'));
                                    $("#emReplyId").html($reply.data('replyname'));
                                    $("#txtcmt").focus();
                                    return false;
                                });
                            } else {
                                var html = '';
                                var userID = '';
                                if (resData.Item.UserLoginType.toLowerCase() === 'joins' && resData.Item.IsReporter === true) {
                                    userID = resData.Item.UserName + ' ' + commentLanguage.isReporter;
                                } else if (resData.Item.UserLoginType.toLowerCase() === 'joins' && resData.Item.IsJplusJouralist === true) {
                                    userID = resData.Item.UserName;
                                } else if (resData.Item.UserLoginType.toLowerCase() !== 'joins') {
                                    userID = resData.Item.UserName;
                                } else {
                                    userID = resData.Item.UserId;
                                }
                                if (resData.Item.IsReporter !== true) {
                                    if (userID.length > 4) {
                                        userID = userID.substring(0, 3) + "****";
                                    } else {
                                        var len = userID.length / 2;
                                        userID = userID.substring(0, len) + "****";
                                    }
                                }
                                var isRelpyID = $("#emReplyId").html();
                                html += '<li id="li_' + resData.Item.Id + '">';
                                html += '<!-- 댓글 -->';
                                html += '<div class="inbox cmt_replay cmt_my">';
                                switch (resData.Item.UserLoginType.toLowerCase()) {
                                    case "joins":
                                        html += '<strong class="i_jb">중앙일보로 로그인</strong>';
                                        break;
                                    case "facebook":
                                        html += '<strong class="i_fb">페이스북으로 로그인</strong>';
                                        break;
                                    case "twitter":
                                        html += '<strong class="i_tw">트위터로 로그인</strong>';
                                        break;
                                    case "kakao":
                                        html += '<strong class="i_kk">카카오톡으로 로그인</strong>';
                                        break;
                                    default:
                                        html += '<strong class="i_jb">중앙일보로 로그인</strong>';
                                        break;
                                }
                                html += '<div class="cmt_box">';
                                html += '<dl class="cmt_info">';
                                html += '<dt>';
                                html += '<em>' + userID + '</em>';
                                html += '<span class="cmt_date">' + resData.Item.RegistedDateTime.toDateISO8061().format('yyyy.MM.dd HH:mm') + '</span>';
                                html += '<button class="btn_tooltip" id="btn_tooltip_' + resData.Item.Id + '" type="button"><span>레이어 열기</span></button>';
                                html += '<div class="tooltip_box">';
                                //html += '<a href="#">작성자의 댓글 모아보기</a>';
                                html += '<a data-action="del" class="delete" href="#" data-deleteid="' + resData.Item.Id + '">삭제</a>';
                                html += '</div>';
                                html += '</dt>';
                                html += '<dd>';
                                html += '<p>';
                                html += '<em>' + isRelpyID + '</em>';
                                html += resData.Item.Content;
                                html += '</p>';
                                html += '</dd>';
                                html += '</dl>';
                                html += '<div class="cmt_btn">';
                                html += '<a href="#" class="btn_recomm" data-voiteid="' + resData.Item.Id + '" title="이 댓글을 공감합니다"><span>공감</span><em id="good_' + resData.Item.Id + '" class="good">' + resData.Item.GoodCount + '</em></a>';
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += '<!-- /댓글 -->';
                                html += '</li>';
                                $("#li_" + seq).after(html);
                                //툴팁 레이어 오픈
                                $("#btn_tooltip_" + resData.Item.Id).on('click', function () {
                                    $("#btn_tooltip_" + resData.Item.Id).next().toggleClass("layeron");
                                    return false;
                                });
                                $("#li_" + resData.Item.Id).on('click', '.delete', function () {
                                    var $delete = $(this),
										deleteId = $delete.data('deleteid');

                                    var opts = {
                                        id: deleteId,
                                        callback: function () {
                                            alert(commentLanguage.deleteComment);
                                            commentDatas.displayCount = commentDatas.displayCount - 1;
                                            var count = parseInt($("#sp_displayCount").text(), 10);
                                            if (count > 0) {
                                                $(".displayCount").text(count - 1);
                                            }
                                            if (commentDatas.displayCount == 0) {
                                                fnCommentList(commentDatas, 'none');
                                            } else {
                                                $("#li_" + deleteId).remove();
                                            }
                                        }
                                    };

                                    if (config.page === 'article') {
                                        opts.totalid = $delete.data('targetid');
                                    }

                                    if (config.page === 'reporter') {
                                        opts.reporterid = $delete.data('targetid');
                                    }

                                    commentDeletePost(opts);

                                    return false;
                                });
                            }
                        }
                        $("#txtReplyId").val('');
                        $("#emReplyId").html('');
                        $("#txtcmt").val('');
                        $("#div_check_area").hide();
                        var count = parseInt($("#sp_displayCount").text(), 10);
                        $(".displayCount").text(count + 1);
                        $('#head').find('.btn_cmt').html('<span><em class="hide">댓글</em>' + (count + 1) + '</span>');
                        $("body").removeAttr("style");
                        $("#bd_comment").show();
                        $("#btn_submit").blur();
                        fnResetCmtInput();
                        alert(commentLanguage.success);
                    }
                });

                //GA코드 통합의 댓글 등록 이벤트 추가
                try {
                    ga('total.send', 'event', 'Engaging', 'Comment', document.title, 1);
                }
                catch (e) {
                }

                return false;
            });
        }
        return this;
    };
})(jQuery, window, document);