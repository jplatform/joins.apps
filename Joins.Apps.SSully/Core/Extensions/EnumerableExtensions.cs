﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Joins.Apps.SSully.Core.Extensions
{
    public static class EnumerableExtensions
    {
        public static void ForEach<T> (this IEnumerable<T> list, Action<T> action)
        {
            foreach (var item in list)
                action(item);
        }

        public static IEnumerable<TResult> ForEachPrevNext<T, TResult>(this IEnumerable<T> stream, Func<T, T, T, TResult> selector) where T : class
        {
            var enumerator = stream.GetEnumerator();
            if (enumerator.MoveNext())
            {
                T prev = null;
                T curr = enumerator.Current;

                while (enumerator.MoveNext())
                {
                    var next = enumerator.Current;
                    yield return selector(prev, curr, next);
                    prev = curr;
                    curr = next;
                }

                yield return selector(prev, curr, null);
            }
        }
    }
}